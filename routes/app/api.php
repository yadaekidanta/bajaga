<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\XenditController;
use App\Http\Controllers\Api\RajaOngkirController;

Route::group([
    'domain' => 'https://bajaga.dev',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'rajaongkir'
], function () {
    Route::match(['get', 'post'], '/cost', [RajaOngkirController::class, 'cost']);
    Route::get('/trf', [RajaOngkirController::class, 'trf']);
    Route::get('/province', [RajaOngkirController::class, 'province']);
    Route::get('/city', [RajaOngkirController::class, 'city']);
    Route::get('/subdistrict', [RajaOngkirController::class, 'subdistrict']);
    Route::post("waybill", [RajaOngkirController::class, 'waybill']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'xendit'
], function () {
    Route::post("invoice", [XenditController::class, 'create']);
    Route::post("get_invoice", [XenditController::class, 'getInvoice']);
    Route::post("payout", [XenditController::class, 'createPayout']);
    Route::post("disbursment", [XenditController::class, 'createDisbursment']);
    Route::post("va", [XenditController::class, 'createVirtualAccount']);
});
