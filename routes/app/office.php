<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Office\FaqController;
use App\Http\Controllers\Office\AuthController;
use App\Http\Controllers\Office\SaleController;
use App\Http\Controllers\Office\BannerController;
use App\Http\Controllers\Office\SellerController;
use App\Http\Controllers\Office\AccountController;
use App\Http\Controllers\Office\ExpenseController;
use App\Http\Controllers\Office\FinanceController;
use App\Http\Controllers\Office\ProductController;
use App\Http\Controllers\Office\ProfileController;
use App\Http\Controllers\Office\CustomerController;
use App\Http\Controllers\Office\DiscountController;
use App\Http\Controllers\Office\EmployeeController;
use App\Http\Controllers\Office\PurchaseController;
use App\Http\Controllers\Office\RegionalController;
use App\Http\Controllers\Office\SupplierController;
use App\Http\Controllers\Office\DashboardController;
use App\Http\Controllers\Office\SalePaymentController;
use App\Http\Controllers\Office\SaleReturnController;
use App\Http\Controllers\Office\SaleDetailController;
use App\Http\Controllers\Office\AccountTypeController;
use App\Http\Controllers\Office\BarcodeController;
use App\Http\Controllers\Office\FaqCategoryController;
use App\Http\Controllers\Office\ProductUnitController;
use App\Http\Controllers\Office\ProductBrandController;
use App\Http\Controllers\Office\ProductVarianController;
use App\Http\Controllers\Office\ExpensePaymentController;
use App\Http\Controllers\Office\PurchaseDetailController;
use App\Http\Controllers\Office\PurchaseReturnController;
use App\Http\Controllers\Office\SellingPricingController;
use App\Http\Controllers\Office\ExpenseCategoryController;
use App\Http\Controllers\Office\ProductCategoryController;
use App\Http\Controllers\Office\ProductWarrantyController;
use App\Http\Controllers\Office\PurchasePaymentController;
use App\Http\Controllers\Office\BusinessLocationController;
use App\Http\Controllers\Office\ProductLelangController;
use App\Http\Controllers\Office\PurchaseReturnDetailController;
use App\Http\Controllers\Office\SaleReturnDetailController;
use App\Http\Controllers\Office\PurchaseReturnPaymentController;
use App\Http\Controllers\Office\SaleReturnPaymentController;


Route::group(['domain' => ''], function() {
    Route::get('office/auth',[AuthController::class, 'index'])->name('auth.index');
    Route::prefix('office')->name('office.')->group(function(){
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('getreset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
        });

        Route::middleware(['auth:office'])->group(function(){
            Route::redirect('/', 'dashboard', 301);
            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
            Route::get('test', function(){
                return view('page.office.product.barcode');
            })->name('db.migrate');
            // MASTER
            Route::resource('product', ProductController::class);
            Route::post('barcode/request_download_pdf',[BarcodeController::class, 'request_download_pdf'])->name('barcode.request_download_pdf');
            Route::get('barcode/pdfDownload',[BarcodeController::class, 'pdfDownload'])->name('barcode.pdfDownload');
            Route::resource('barcode', BarcodeController::class);
            Route::post('product/{product}/active',[ProductController::class, 'active'])->name('product.active');
            Route::post('product/{product}/nonActive',[ProductController::class, 'nonActive'])->name('product.nonActive');
            Route::post('productLelang/{productLelang}/active',[ProductLelangController::class, 'active'])->name('productLelang.active');
            Route::post('productLelang/{productLelang}/tidakTerjual',[ProductLelangController::class, 'tidakTerjual'])->name('productLelang.tidakTerjual');
            Route::post('product/get',[ProductController::class, 'get'])->name('product.get');
            Route::post('product/barcode',[ProductController::class, 'barcode'])->name('product.barcode');
            Route::post('product/input_modal',[ProductController::class, 'input_modal'])->name('product.input_modal');
            Route::post('product/list_modal',[ProductController::class, 'list_modal'])->name('product.list_modal');
            Route::get('product/{product}/lelang',[ProductLelangController::class, 'create'])->name('product-lelang.create');
            Route::get('product/{product}/lelang',[ProductController::class, 'lelang'])->name('product.lelang');
            Route::get('product/{product}/create_variations',[ProductVarianController::class, 'create'])->name('product-varian.create');
            Route::post('product-varian/get',[ProductVarianController::class, 'get'])->name('varian.get');
            Route::post('product-varian/get_list',[ProductVarianController::class, 'get_list'])->name('varian.get_list');
            Route::get('product/{product}/{productVarian}/edit',[ProductVarianController::class, 'edit'])->name('product-varian.edit');
            Route::patch('product-varian/{productVarian}/update',[ProductVarianController::class, 'update'])->name('product-varian.update');
            Route::delete('product-varian/{productVarian}/destroy',[ProductVarianController::class, 'destroy'])->name('product-varian.destroy');
            Route::get('product/{product}/variations',[ProductVarianController::class, 'index'])->name('product-varian.index');

            Route::get('bid_detail/{id}',[ProductLelangController::class, 'bid_detail'])->name('productLelang.bid_detail');
            Route::get('bid_send_mail/{id}',[ProductLelangController::class, 'bid_send_mail'])->name('productLelang.bid_send_mail');
            Route::resource('productLelang', ProductLelangController::class);
            // Route::resource('product-varian', ProductVarianController::class);
            Route::post('product-varian/store',[ProductVarianController::class, 'store'])->name('product-varian.store');
            Route::resource('product-warranty', ProductWarrantyController::class);
            Route::post('product-warranty/get',[ProductWarrantyController::class, 'get'])->name('product-warranty.get');
            Route::post('product-warranty/get_list',[ProductWarrantyController::class, 'get_list'])->name('product-warranty.get_list');
            Route::post('product-warranty/input_modal',[ProductWarrantyController::class, 'input_modal'])->name('product-warranty.input_modal');
            Route::post('product-warranty/list_modal',[ProductWarrantyController::class, 'list_modal'])->name('product-warranty.list_modal');
            Route::resource('product-category', ProductCategoryController::class);
            Route::post('product-category/get',[ProductCategoryController::class, 'get'])->name('product-category.get');
            Route::post('product-category/input_modal',[ProductCategoryController::class, 'input_modal'])->name('product-category.input_modal');
            Route::post('product-category/list_modal',[ProductCategoryController::class, 'list_modal'])->name('product-category.list_modal');
            Route::post('product-category/{productCategory}/active',[ProductCategoryController::class, 'active'])->name('product-category.active');
            Route::post('product-category/{productCategory}/nonActive',[ProductCategoryController::class, 'nonActive'])->name('product-category.nonActive');
            Route::post('product-category/get_list',[ProductCategoryController::class, 'get_list'])->name('product-category.get_list');
            Route::post('product-category/get_list_sub',[ProductCategoryController::class, 'get_list_sub'])->name('product-category.get_list_sub');
            Route::resource('product-brand', ProductBrandController::class);
            Route::post('product-brand/get',[ProductBrandController::class, 'get'])->name('product-brand.get');
            Route::post('product-brand/get_list',[ProductBrandController::class, 'get_list'])->name('product-brand.get_list');
            Route::post('product-brand/input_modal',[ProductBrandController::class, 'input_modal'])->name('product-brand.input_modal');
            Route::post('product-brand/list_modal',[ProductBrandController::class, 'list_modal'])->name('product-brand.list_modal');
            Route::resource('product-unit', ProductUnitController::class);
            Route::post('product-unit/get',[ProductUnitController::class, 'get'])->name('product-unit.get');
            Route::post('product-unit/get_list',[ProductUnitController::class, 'get_list'])->name('product-unit.get_list');
            Route::post('product-unit/input_modal',[ProductUnitController::class, 'input_modal'])->name('product-unit.input_modal');
            Route::post('product-unit/list_modal',[ProductUnitController::class, 'list_modal'])->name('product-unit.list_modal');

            Route::resource('selling-price', SellingPricingController::class);

            Route::resource('business-location', BusinessLocationController::class);
            Route::post('business-location/get_list',[BusinessLocationController::class, 'get_list'])->name('business-location.get_list');
            Route::resource('banner', BannerController::class);
            Route::post('banner/active/{banner}', [BannerController::class, 'active'])->name('banner.active');
            Route::post('banner/nonActive/{banner}', [BannerController::class, 'nonActive'])->name('banner.nonActive');
            Route::delete('banner/{banner}', [BannerController::class, 'destroy'])->name('banner.destroy');
            Route::resource('faq-category', FaqCategoryController::class);
            Route::resource('faq', FaqController::class);
            Route::resource('discount', DiscountController::class);

            // CRM
            Route::resource('customer', CustomerController::class);
            Route::post('customer/get',[CustomerController::class, 'get'])->name('customer.get');
            Route::post('customer/get_list',[CustomerController::class, 'get_list'])->name('customer.get_list');
            Route::resource('seller', SellerController::class);
            Route::get('seller/{seller}/download',[SellerController::class, 'download'])->name('seller.download');
            Route::post('seller/{seller}/active',[SellerController::class, 'active'])->name('seller.active');
            Route::post('seller/create-reason',[SellerController::class, 'create_reason'])->name('seller.create_reason');
            Route::post('seller/send-reason',[SellerController::class, 'send_reason'])->name('seller.send_reason');
            Route::resource('supplier', SupplierController::class);
            Route::post('supplier/getList',[SupplierController::class, 'getList'])->name('supplier.getList');
            Route::post('supplier/get',[SupplierController::class, 'get'])->name('supplier.get');
            Route::get('regional/province',[RegionalController::class, 'province'])->name('regional.province');
            Route::get('regional/city',[RegionalController::class, 'city'])->name('regional.city');

            // HRM
            Route::resource('employee', EmployeeController::class);

            // Finance
            // Route::resource('balance_sheet', FinanceController::class);

             // FINANCE
            Route::resource('account', AccountController::class);
            Route::post('account/get',[AccountController::class, 'get'])->name('account.get');
            Route::post('account/get_list',[AccountController::class, 'get_list'])->name('account.get_list');
            Route::post('account/list_modal',[AccountController::class, 'list_modal'])->name('account.list_modal');
            Route::post('account/{account}/active',[AccountController::class, 'active'])->name('account.active');
            Route::post('account/{account}/close',[AccountController::class, 'close'])->name('account.close');
            Route::resource('account-type', AccountTypeController::class);
            Route::get('balance-sheet',[FinanceController::class, 'balance_sheet'])->name('finance.balance_sheet');
            Route::get('trial-balance',[FinanceController::class, 'trial_balance'])->name('finance.trial_balance');
            Route::get('cash-flow',[FinanceController::class, 'cash_flow'])->name('finance.cash_flow');
            Route::get('payment-account-report',[FinanceController::class, 'payment_account_report'])->name('finance.payment_account_report');

            // TRANSACTION
            Route::resource('expense', ExpenseController::class);
            Route::get('expense/{expense}/download',[ExpenseController::class, 'download'])->name('expense.download');
            Route::resource('expense-category', ExpenseCategoryController::class);
            Route::post('expense-category/get_list',[ExpenseCategoryController::class, 'get_list'])->name('expense-category.get_list');
            Route::post('expense-payment/store',[ExpensePaymentController::class, 'store'])->name('expense-payment.store');
            Route::delete('expense-payment/{expensePayment}/destroy',[ExpensePaymentController::class, 'destroy'])->name('expense-payment.destroy');
            // PURCHASE
            Route::resource('purchase', PurchaseController::class);
            Route::post('purchase/{purchase}/process',[PurchaseController::class, 'process'])->name('purchase.process');
            Route::post('purchase/{purchase}/delivered',[PurchaseController::class, 'delivered'])->name('purchase.delivered');
            Route::post('purchase-detail/store',[PurchaseDetailController::class, 'store'])->name('purchase_detail.store');
            Route::delete('purchase-detail/{purchaseDetail}/delete',[PurchaseDetailController::class, 'destroy'])->name('purchase_detail.destroy');
            Route::post('purchase-payment/store',[PurchasePaymentController::class, 'store'])->name('purchase_payment.store');
            Route::delete('purchase-payment/{purchasePayment}/delete',[PurchasePaymentController::class, 'destroy'])->name('purchase_payment.destroy');
            Route::resource('purchase-return', PurchaseReturnController::class);
            Route::post('purchase-return/{purchaseReturn}/process',[PurchaseReturnController::class, 'process'])->name('purchase-return.process');
            Route::post('purchase-return/{purchaseReturn}/delivered',[PurchaseReturnController::class, 'delivered'])->name('purchase-return.delivered');
            Route::post('purchase-return-detail/store',[PurchaseReturnDetailController::class, 'store'])->name('purchase-return_detail.store');
            Route::delete('purchase-return-detail/{purchaseReturnDetail}/delete',[PurchaseReturnDetailController::class, 'destroy'])->name('purchase-return_detail.destroy');
            Route::post('purchase-return-payment/store',[PurchaseReturnPaymentController::class, 'store'])->name('purchase-return_payment.store');
            Route::delete('purchase-return-payment/{purchaseReturnPayment}/delete',[PurchaseReturnPaymentController::class, 'destroy'])->name('purchase-return_payment.destroy');
            // SALE
            Route::resource('sale', SaleController::class);
            Route::post('sale/{sale}/process',[SaleController::class, 'process'])->name('sale.process');
            Route::post('sale/{sale}/delivered',[SaleController::class, 'delivered'])->name('sale.delivered');
            Route::post('sale-detail/store',[SaleDetailController::class, 'store'])->name('sale_detail.store');
            Route::delete('sale-detail/{saleDetail}/delete',[SaleDetailController::class, 'destroy'])->name('sale_detail.destroy');
            Route::post('sale-payment/store',[SalePaymentController::class, 'store'])->name('sale_payment.store');
            Route::delete('sale-payment/{salePayment}/delete',[SalePaymentController::class, 'destroy'])->name('sale_payment.destroy');
            Route::resource('sale-return', SaleReturnController::class);
            Route::post('sale-return/{saleReturn}/process',[SaleReturnController::class, 'process'])->name('sale-return.process');
            Route::post('sale-return/{saleReturn}/delivered',[SaleReturnController::class, 'delivered'])->name('sale-return.delivered');
            Route::post('sale-return-detail/store',[SaleReturnDetailController::class, 'store'])->name('sale-return_detail.store');
            Route::delete('sale-return-detail/{saleReturnDetail}/delete',[SaleReturnDetailController::class, 'destroy'])->name('sale-return_detail.destroy');
            Route::post('sale-return-payment/store',[SaleReturnPaymentController::class, 'store'])->name('sale-return_payment.store');
            Route::delete('sale-return-payment/{saleReturnPayment}/delete',[SaleReturnPaymentController::class, 'destroy'])->name('sale-return_payment.destroy');
            Route::post('sale/shipping', [SaleReturnPaymentController::class, 'add_tracking_number'])->name('store.shipping');
            // REGIONAL
            Route::get('province', [RegionalController::class, 'province'])->name('regional.province');
            Route::get('city', [RegionalController::class, 'city'])->name('regional.city');
            Route::get('subdistrict', [RegionalController::class, 'subdistrict'])->name('regional.subdistrict');
            Route::get('village', [RegionalController::class, 'village'])->name('regional.village');
            Route::post('city/get', [RegionalController::class, 'get_city'])->name('regional.get_city');
            Route::post('subdistrict/get', [RegionalController::class, 'get_subdistrict'])->name('regional.get_subdistrict');
            Route::post('village/get', [RegionalController::class, 'get_village'])->name('regional.get_village');

            // PROFILE
            Route::prefix('profile')->name('profile.')->group(function(){
                Route::get('', [ProfileController::class, 'index'])->name('index');
                Route::get('edit', [ProfileController::class, 'edit'])->name('edit');
                Route::post('cpassword', [ProfileController::class, 'cpassword'])->name('cpassword');
                Route::post('save', [ProfileController::class, 'save'])->name('save');
            });
            Route::get('notice',[AuthController::class, 'verification'])->name('verification.notice');
            Route::get('resend',[AuthController::class, 'resend_mail'])->name('auth.resend.mail');
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');

            Route::get('migrate', function(){
                Artisan::call('migrate');
                return response()->json([
                    'alert' => 'success',
                    'message' => 'DB Migrate!'
                ]);
            })->name('db.migrate');
            Route::get('storage-link', function(){
                Artisan::call('storage:link');
                return response()->json([
                    'alert' => 'success',
                    'message' => 'Storage Linked!'
                ]);
            })->name('storage.link');
            Route::get('db-seed', function(){
                Artisan::call('db:seed');
                return response()->json([
                    'alert' => 'success',
                    'message' => 'DB Seed!'
                ]);
            })->name('db.seed');
        });
    });
});
