<?php

use App\Http\Controllers\Api\XenditController;
use App\Http\Controllers\Office\DiscountController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Web\ProductDiscussController;
use App\Http\Controllers\Web\WebController;
use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\CartController;
use App\Http\Controllers\Web\ChatController;
use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Web\ProfileController;
use App\Http\Controllers\Web\CheckoutController;
use App\Http\Controllers\Web\DiscountController as WebDiscountController;
use App\Http\Controllers\Web\RegionalController;
use App\Http\Controllers\Web\OpenStoreController;
use App\Http\Controllers\Web\TransactionController;
use App\Http\Controllers\Web\UserAddressController;
use App\Http\Controllers\Web\NotificationController;
use App\Http\Controllers\Web\ProductCategoryController;
use App\Http\Controllers\Web\StoreController;
use App\Http\Controllers\Web\UserBankController;
use App\Http\Controllers\Web\UserDisplayController;
use App\Http\Controllers\Web\UserReview;
use App\Http\Controllers\Web\WishListController;

Route::group(['domain' => ''], function() {
    Route::prefix('')->name('web.')->group(function(){
        Route::redirect('/', 'home', 301);
        Route::get('home',[WebController::class, 'index'])->name('home');
        Route::get('about',[WebController::class, 'about'])->name('about');
        Route::get('help',[WebController::class, 'help'])->name('help');
        Route::get('terms',[WebController::class, 'terms'])->name('terms');
        Route::get('privacy',[WebController::class, 'privacy'])->name('privacy');
        Route::post('modal',[WebController::class, 'modal'])->name('modalAlamat');
        Route::get('auction',[WebController::class, 'auction'])->name('auction');
        Route::get('result/{keyword}',[WebController::class, 'result'])->name('result');
        Route::get('search',[WebController::class, 'searching'])->name('search');
        Route::post('search',[WebController::class, 'searching'])->name('searching');
        Route::post('auction',[WebController::class, 'auction_store'])->name('auction.store');
        Route::get('auction/{productLelang}',[WebController::class, 'auction_show'])->name('auction.show');

        
        Route::resource('product', ProductController::class);
        Route::post('products/add_modal', [ProductController::class, 'add_modal'])->name('product.add_modal');
        Route::post('products/update_modal/{product:slug}', [ProductController::class, 'update_modal'])->name('product.update_modal');
        
        Route::get('products/recomendation', [ProductController::class, 'recomendation'])->name('product.recomendation');
        Route::get('products/{product:slug}',[ProductController::class, 'show'])->name('product.show');
        Route::get('catalog/products',[ProductController::class, 'catalog'])->name('catalog.products');
        Route::get('catalog/search/products',[ProductController::class, 'catalog_search'])->name('catalog.search.products');

        Route::get('category/{category:slug}',[ProductCategoryController::class, 'show'])->name('category.show');
        Route::get('subcategory/{subcategory:slug}',[ProductCategoryController::class, 'show_subcategory'])->name('category.show_subcategory');
        Route::get('brand/{brand:slug}',[ProductCategoryController::class, 'show_brand'])->name('category.show_brand');

        Route::post('invoice/callback', [TransactionController::class,'invoice_callback'])->name('invoice.callback');
        Route::post("disbursment/callback", [XenditController::class, 'callbackDisbursment']);

        Route::prefix('auth')->name('auth.')->group(function(){
            Route::get('',[AuthController::class, 'index'])->name('index');
            Route::get('get_register',[AuthController::class, 'get_register'])->name('get_register');
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::post('check',[AuthController::class, 'check_phone'])->name('check');
            Route::get('password_changed',[AuthController::class, 'password_changed'])->name('password_changed');
            Route::post('validate_otp',[AuthController::class, 'do_login_sms'])->name('validate_otp');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('getreset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
            Route::get('verify/{auth:email}',[AuthController::class, 'do_verify'])->name('verify');
            Route::get('deactivated', [AuthController::class, 'deactivated'])->name('deactivated');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('getreset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
        });

        Route::middleware(['auth:web'])->group(function(){
            Route::post('transaction', [TransactionController::class,'transaction'])->name('transaction');
            Route::get('transaction/checkout', [TransactionController::class,'index'])->name('transaction.checkout');
            Route::post('transaction/checkout/getModalUlasan/{sale}', [TransactionController::class,'getModalUlasan'])->name('transaction.checkout.getModalUlasan');
            Route::post('transaction/checkout/invoice', [TransactionController::class,'invoice'])->name('transaction.invoice');
            Route::get('transaction/success', [TransactionController::class,'success'])->name('transaction.success');
            Route::get('transaction/failed', [TransactionController::class,'failed'])->name('transaction.failed');
            Route::get('transaction/detail/{sale}', [TransactionController::class,'detail'])->name('transaction.detail');
            Route::get('shipping/detail/{sale}', [TransactionController::class,'detailShipping'])->name('shipping.detail');
            Route::get('shipping/get_detail/{sale}', [TransactionController::class,'getDetailShipping'])->name('shipping.get_detail');

            Route::post('regional/province',[RegionalController::class, 'province'])->name('regional.province');
            Route::post('regional/city',[RegionalController::class, 'city'])->name('regional.city');
            Route::post('regional/subdistrict',[RegionalController::class, 'subdistrict'])->name('regional.subdistrict');
            Route::get('user-address/getDataAddress', [UserAddressController::class, 'getDataAddress'])->name('user-address.getDataAddress');
            Route::resource('user-address', UserAddressController::class);
            Route::patch('user-address/update/is_use/{userAddress:id}', [UserAddressController::class, 'address_is_use'])->name('user-address.is_use');
            Route::get('user-address/edit/{id}',[UserAddressController::class, 'edit'])->name('user-address.edit');
            Route::post('user-address/edit/modal/{userAddress:id}',[UserAddressController::class, 'edit_modal'])->name('user-address.edit_modal');
            Route::patch('user-address/edit/{user}',[UserAddressController::class, 'update'])->name('user-address.update');
            Route::delete('user-address/destroy/{userAddress:id}',[UserAddressController::class, 'destroy'])->name('user-address.destroy');
            Route::patch('profile/updateAdress/{user}',[ProfileController::class, 'updateAddress'])->name('profile.updateAddress');

            Route::get('invoice', [TransactionController::class,'invoice_detail'])->name('invoice');


            Route::get('counter_notif', [NotificationController::class, 'counter'])->name('counter_notif');
            Route::get('counter_chat', [NotificationController::class, 'counter_chat'])->name('counter_chat');
            Route::resource('open-store', OpenStoreController::class);
            
            Route::get('chat', [ChatController::class, 'index'])->name('chat');
            Route::post('chat', [ChatController::class, 'store'])->name('chat');
            Route::get('chat/{user}', [ChatController::class, 'show'])->name('chat.detail');

            Route::get('store/paystore', [StoreController::class, 'paystore'])->name('store.paystore');
            Route::get('store/paystore/withdraw_more_data', [StoreController::class, 'withdraw_more_data'])->name('store.paystore.withdraw_more_data');
            Route::get('store/paystore/topup_more_data', [StoreController::class, 'topup_more_data'])->name('store.paystore.topup_more_data');
            Route::get('store/favorite', [StoreController::class, 'favorite'])->name('store.favorite');
            Route::post('store/favorite/add', [StoreController::class, 'add_favorite'])->name('store.favorite.add');
            Route::get('store/favorite/follow', [StoreController::class, 'follow'])->name('store.favorite.follow');
            Route::delete('store/favorite/destory/{storeFavorite:id}', [StoreController::class, 'destory_favorite'])->name('store.favorite.destroy');
            Route::delete('store/favorite/unfollow', [StoreController::class, 'unfollow'])->name('store.favorite.unfollow');
            Route::get('store/auction', [StoreController::class, 'auction'])->name('store.auction');
            Route::get('store/settings', [StoreController::class, 'main_setting'])->name('store.settings');
            Route::get('store/{userStore}', [StoreController::class, 'detail'])->name('store');
            Route::get('store/history/transaction', [StoreController::class, 'history_transaction'])->name('store.history.transaction');
            Route::post('store/history/transaction/getDetailModal/{sale}', [StoreController::class, 'history_transaction_getDetailModal'])->name('store.history.transaction.getDetailModal');
            // for user to update data tracking number in sale
            Route::post('store/shipping/add_tracking_number', [StoreController::class, 'add_tracking_number'])->name('store.add_tracking_number');
            Route::post('store/shipping/tracking_number', [StoreController::class, 'tracking_number'])->name('store.tracking_number');
            Route::get('store/review/menu', [StoreController::class, 'review'])->name('store.review.menu');
            Route::get('store/discuss/menu', [StoreController::class, 'discuss'])->name('store.discuss.menu');
            Route::post('store/review/addModalReview/{productReview:id}', [StoreController::class, 'addModalReview'])->name('store.review.addModalReview');
            Route::get('store/settings/profile', [StoreController::class, 'profile_setting'])->name('store.settings.profile');
            Route::patch('store/settings/profile', [StoreController::class, 'profile_setting_update'])->name('store.settings.profile');
            Route::get('store/settings/product', [StoreController::class, 'product_setting'])->name('store.settings.product');
            Route::patch('store/settings/product/price', [StoreController::class, 'product_setting_price'])->name('store.settings.product.price');
            Route::patch('store/settings/product/stock', [StoreController::class, 'product_setting_stock'])->name('store.settings.product.stock');
            Route::get('etalase/{userDisplay}', [UserDisplayController::class, 'catalog'])->name('etalase');
            Route::get('store/settings/product/list_etalase', [UserDisplayController::class, 'index'])->name('store.settings.product.list_etalase');
            Route::get('store/settings/product/list_etalase/getDataEtalase', [UserDisplayController::class, 'getDataEtalase'])->name('store.settings.product.list_etalase.getDataEtalase');
            Route::post('store/settings/product/etalase/add_modal', [UserDisplayController::class, 'add_modal'])->name('store.settings.product.etalase.add_modal');
            Route::post('store/settings/product/price/update_modal/{product}', [UserDisplayController::class, 'update_modal_price'])->name('store.settings.product.price.update_modal');
            Route::post('store/settings/product/stok/update_modal/{product}', [UserDisplayController::class, 'update_modal_stok'])->name('store.settings.product.stok.update_modal');
            Route::post('store/settings/product/etalase/store', [UserDisplayController::class, 'store'])->name('store.settings.product.etalase.store');
            Route::delete('store/settings/product/etalase/destroy/{userDisplay}', [UserDisplayController::class, 'destroy'])->name('store.settings.product.etalase.destroy');
            Route::get('store/settings/shipping/address', [StoreController::class, 'address_store'])->name('store.settings.shipping.address');
            Route::patch('store/settings/shipping/address/update/{id}', [StoreController::class, 'address_store_update'])->name('store.settings.shipping.address.update');
            Route::get('store/settings/shipping/services', [StoreController::class, 'shipping_services'])->name('store.settings.shipping.services');
            Route::get('store/settings/shipping/services/create', [StoreController::class, 'shipping_services_create'])->name('store.settings.shipping.services.create');
            Route::get('store/settings/shipping/services/destroy', [StoreController::class, 'shipping_services_destroy'])->name('store.settings.shipping.services.destroy');
            Route::post('store/settings/oprational', [StoreController::class, 'oprational'])->name('store.settings.oprational');
            Route::resource('product', ProductController::class);

            Route::resource('product-category', ProductCategoryController::class);
            Route::post('product-category/get',[ProductCategoryController::class, 'get'])->name('product-category.get');
            Route::post('product-category/list_modal',[ProductCategoryController::class, 'list_modal'])->name('product-category.list_modal');
            Route::post('product-category/{productCategory}/active',[ProductCategoryController::class, 'active'])->name('product-category.active');
            Route::post('product-category/{productCategory}/nonActive',[ProductCategoryController::class, 'nonActive'])->name('product-category.nonActive');
            Route::post('product-category/get_list',[ProductCategoryController::class, 'get_list'])->name('product-category.get_list');
            Route::post('product-category/get_list_sub',[ProductCategoryController::class, 'get_list_sub'])->name('product-category.get_list_sub');
            
            Route::get('data-user',[ProfileController::class, 'getDataUser']);
            Route::get('profile',[ProfileController::class, 'index'])->name('profile');
            Route::get('profile/getDataBiodata',[ProfileController::class, 'getDataBiodata'])->name('profile.getDataBiodata');
            Route::patch('profile/update/photo/{user}',[ProfileController::class, 'update_photo'])->name('profile.update.photo');
            Route::get('profile/edit/{user}',[ProfileController::class, 'edit'])->name('profile.edit');
            Route::put('profile',[ProfileController::class, 'update'])->name('profile.update');
            Route::patch('profile/update/{user}',[ProfileController::class, 'update'])->name('profile.update');

            Route::get('profile/address',[UserAddressController::class, 'index'])->name('profile.address');
            
            Route::get('profile/account_security',[ProfileController::class, 'account_security'])->name('profile.account_security');
            Route::get('profile/account_security/password',[ProfileController::class, 'account_security_password'])->name('profile.account_security.password');
            Route::post('profile/account_security/password',[ProfileController::class, 'account_security_password_store'])->name('profile.account_security.password');

            Route::get('profile/bank_account',[UserBankController::class, 'index'])->name('profile.bank_account');
            Route::get('profile/bank_account/getDataBankUser',[UserBankController::class, 'getDataBankUser'])->name('profile.bank_account.getDataBankUser');
            Route::get('profile/bank_account/add',[UserBankController::class, 'create'])->name('profile.bank_account.add');
            Route::post('profile/bank_account/add_modal',[UserBankController::class, 'add_modal'])->name('profile.bank_account.add_modal');
            Route::patch('profile/bank_account/is_use',[UserBankController::class, 'is_use'])->name('profile.bank_account.is_use');
            Route::post('profile/bank_account/store',[UserBankController::class, 'store'])->name('profile.bank_account.store');
            Route::post('profile/bank_account/destroy/{userBank}',[UserBankController::class, 'destroy'])->name('profile.bank_account.destroy');

            Route::get('review',[UserReview::class, 'index'])->name('review');
            Route::post('review',[UserReview::class, 'store'])->name('review');

            Route::get('discuss/{product_id}',[ProductDiscussController::class, 'show'])->name('discuss.get');
            Route::get('discuss',[ProductDiscussController::class, 'showAll'])->name('discuss.getAll');
            Route::post('discuss',[ProductDiscussController::class, 'store'])->name('discuss');
            
            Route::get('wishlist',[WishListController::class, 'index'])->name('wishlist');
            Route::post('wishlist.store',[WishListController::class, 'store'])->name('wishlist.store');
            Route::delete('wishlist/destroy/{id}',[WishListController::class, 'destroy'])->name('wishlist.destroy');
            
            Route::get('discount', [WebDiscountController::class, 'show_all'])->name('discount');
            Route::get('discount/{discount:id}', [WebDiscountController::class, 'detail'])->name('discount.detail');

            Route::get('notification', [NotificationController::class, 'index'])->name('notification');
            Route::get('notification/show', [NotificationController::class, 'show'])->name('notification.show');
            Route::get('counter_cart', [CartController::class, 'counter'])->name('counter_cart');
            Route::get('cart', [CartController::class, 'index'])->name('cart');
            Route::get('cart/detail', [CartController::class, 'detail'])->name('cart.detail');
            Route::post('cart/add', [CartController::class, 'store'])->name('cart.add');
            Route::get('cart/delete', [CartController::class, 'delete'])->name('cart.delete');
            Route::patch('cart/decrease/{item}',[CartController::class, 'decreaseQuantity'])->name('cart.decreaseQuantity');
            Route::patch('cart/increase/{item}',[CartController::class, 'increaseQuantity'])->name('cart.increaseQuantity');
            Route::delete('cart/destroy',[CartController::class, 'destroy'])->name('cart.destroy');
            Route::patch('cart/update/{item}',[CartController::class, 'updateQuantity'])->name('cart.updateQuantity');
            Route::get('checkout', [CartController::class,'checkout_show'])->name('checkout.show');
            Route::post('checkout/modalUserAddress', [CartController::class,'modalUserAddress'])->name('checkout.modalUserAddress');
            Route::post('checkout', [CartController::class,'checkout'])->name('checkout');
            Route::get('notice',[AuthController::class, 'verification'])->name('verification.notice');
            Route::get('resend',[AuthController::class, 'resend_mail'])->name('auth.resend.mail');
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');

        });
        
    });
    Route::get('migrate', function(){
        Artisan::call('migrate');
        return response()->json([
            'alert' => 'success',
            'message' => 'DB Migrate!'
        ]);
    })->name('db.migrate');
    Route::get('storage-link', function(){
        Artisan::call('storage:link');
        return response()->json([
            'alert' => 'success',
            'message' => 'Storage Linked!'
        ]);
    })->name('storage.link');
    Route::get('db-seed', function(){
        Artisan::call('db:seed');
        return response()->json([
            'alert' => 'success',
            'message' => 'DB Seed!'
        ]);
    })->name('db.seed');
});
