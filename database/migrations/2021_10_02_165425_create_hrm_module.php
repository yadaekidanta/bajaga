<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrmModule extends Migration
{
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->id();
            $table->string('nik',16)->nullable();
            $table->string('username',30)->nullable();
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('phone',15);
            $table->string('photo')->nullable();
            $table->enum('role',['pegawai','owner']);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
