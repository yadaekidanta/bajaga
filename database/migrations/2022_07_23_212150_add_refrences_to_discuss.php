<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefrencesToDiscuss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discuss', function (Blueprint $table) {
            $table->bigInteger('to_id')->index()->nullable()->after("comment");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discuss', function (Blueprint $table) {
            $table->dropColumn("to_id");
        });
    }
}
