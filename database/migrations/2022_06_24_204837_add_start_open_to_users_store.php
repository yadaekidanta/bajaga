<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddStartOpenToUsersStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_store', function (Blueprint $table) {
            $table->time("end_at")->nullable()->after("verified_at");
            $table->time("start_at")->nullable()->after("verified_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_store', function (Blueprint $table) {
            $table->dropColumn(["start_at", "start_at"]);
        });
    }
}
