<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleModule extends Migration
{
    public function up()
    {
        Schema::create('sale', function (Blueprint $table) {
            $table->id();
            $table->string('code',30);
            $table->integer('store_id')->default(0);
            $table->integer('customer_id')->default(0);
            $table->date('date');
            $table->string('total',30)->default(0);
            $table->string('disc_price',30)->default(0);
            $table->string('shipping_detail')->nullable();
            $table->string('shipping_price',30)->default(0);
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->string('payment_method')->nullable();
            $table->string('payment_channel')->nullable();
            $table->string('courier')->nullable();
            $table->string('no_resi')->default(null);
            $table->string('total_paid',30)->default(0);
            $table->string('grand_total',30)->default(0);
            $table->longText('note')->nullable();
            $table->enum('type',['Offline','Online'])->default('Offline');
            $table->enum('st',['Tertunda','Dipesan','Diterima', 'Dikirim', 'Sampai', 'Selesai', 'Dibatalkan'])->default('Tertunda');
            $table->enum('payment_st',['Belum lunas','Lunas'])->default('Belum lunas');
            $table->integer('created_by');
            $table->timestamps();
        });
        Schema::create('sale_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('sale_id')->default(0);
            $table->integer('product_id')->default(0);
            $table->string('price',20)->default(0);
            $table->string('qty',20)->default(0);
            $table->string('subtotal',20)->default(0);
            $table->string('disc_price',30)->default(0);
            $table->string('shipping_price',30)->default(0);
        });
        Schema::create('sale_payment', function (Blueprint $table) {
            $table->id();
            $table->integer('sale_id')->default(0);
            $table->integer('account_id')->default(0);
            $table->date('date');
            $table->string('total',20)->nullable();
            $table->enum('payment_method',['Bank Transfer','Cash']);
            $table->longText('note')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
        Schema::create('sale_return', function (Blueprint $table) {
            $table->id();
            $table->integer('sale_id')->default(0);
            $table->string('code',30);
            $table->date('date');
            $table->string('total',30)->default(0);
            $table->string('total_paid',30)->default(0);
            $table->string('grand_total',30)->default(0);
            $table->enum('st',['Tertunda','Diproses','Diterima'])->default('Tertunda');
            $table->enum('payment_st',['Belum lunas','Lunas'])->default('Belum lunas');
            $table->integer('created_by');
            $table->timestamps();
        });
        Schema::create('sale_return_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('sale_return_id')->default(0);
            $table->integer('product_id')->default(0);
            $table->string('price',20)->default(0);
            $table->string('qty',20)->default(0);
            $table->string('subtotal',20)->default(0);
        });
        Schema::create('sale_return_payment', function (Blueprint $table) {
            $table->id();
            $table->integer('sale_return_id')->default(0);
            $table->integer('account_id')->default(0);
            $table->date('date');
            $table->string('total',20)->default(0);
            $table->enum('payment_method',['Bank Transfer','Cash']);
            $table->longText('note')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('sale');
        Schema::dropIfExists('sale_detail');
        Schema::dropIfExists('sale_payment');
        Schema::dropIfExists('sale_return');
        Schema::dropIfExists('sale_return_detail');
        Schema::dropIfExists('sale_return_payment');
    }
}
