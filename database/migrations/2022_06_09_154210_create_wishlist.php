<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWishlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wishlist', function (Blueprint $table) {
            $table->id();
            $table->foreignId("users_id");
            $table->foreignId("id_product");
            // $table->primary(["users_id", "id_product"]);
            $table->timestamps();

            $table->foreign("users_id")->references("id")->on("users")->onDelete('cascade');
            $table->foreign("id_product")->references("id")->on("product")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wishlist');
    }
}
