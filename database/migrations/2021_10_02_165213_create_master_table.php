<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterTable extends Migration
{
    public function up()
    {
        Schema::create('faq_category', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::create('faq', function (Blueprint $table) {
            $table->id();
            $table->integer('faq_category_id')->default(0);
            $table->longText('question');
            $table->longText('answer');
            $table->timestamp('created_at');
            $table->integer('created_by')->default(0);
        });
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });
        // Schema::create('personal_access_tokens', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->morphs('tokenable');
        //     $table->string('name');
        //     $table->string('token', 64)->unique();
        //     $table->text('abilities')->nullable();
        //     $table->timestamp('last_used_at')->nullable();
        //     $table->timestamps();
        // });
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string('photo');
            $table->string('url');
            $table->enum('active',['a','n'])->default('n');
        });
        Schema::create('business_location', function (Blueprint $table) {
            $table->id();
            $table->string('code',20);
            $table->string('name');
            $table->longText('address');
            $table->integer('province_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->string('phone',15);
            $table->string('email');
        });
        Schema::create('selling_price_group', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('type',['persentase','harga']);
            $table->string('persentase',3)->nullable()->default(0);
            $table->string('harga',20)->nullable()->default(0);
        });
        Schema::create('discount', function (Blueprint $table) {
            $table->id();
            $table->string('code',30);
            $table->string('name');
            $table->longText('desc');
            $table->enum('type',['persentase','harga']);
            $table->string('persentase',3)->nullable()->default(0);
            $table->string('harga',20)->nullable()->default(0);
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->enum('st',['aktif','kadaluarsa','belum aktif'])->default('belum aktif');
            $table->timestamps();
        });
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('product_id');
            $table->longText('review');
            $table->float('rate',3);
            $table->timestamps();
        });
        Schema::create('discuss', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id');
            $table->integer('user_id');
            $table->longText('comment');
            $table->timestamps();
        });
        // Schema::create('carts', function (Blueprint $table) {
        //     $table->id();
        //     $table->integer('user_id');
        //     $table->integer('product_id');
        //     $table->string('qty');
        // });
        // Schema::create('wishlists', function (Blueprint $table) {
        //     $table->id();
        //     $table->integer('user_id');
        //     $table->integer('product_id');
        // });
    }
    public function down()
    {
        Schema::dropIfExists('faq_category');
        Schema::dropIfExists('faq');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('failed_jobs');
        // Schema::dropIfExists('personal_access_tokens');
        Schema::dropIfExists('banners');
        Schema::dropIfExists('business_location');
        Schema::dropIfExists('selling_price_group');
        Schema::dropIfExists('reviews');
        Schema::dropIfExists('discuss');
        Schema::dropIfExists('carts');
        Schema::dropIfExists('wishlists');
    }
}
