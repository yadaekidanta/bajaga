<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceModule extends Migration
{
    public function up()
    {
        Schema::create('expense_category', function (Blueprint $table) {
            $table->id();
            $table->string('code',20);
            $table->string('name');
        });
        Schema::create('expense', function (Blueprint $table) {
            $table->id();
            $table->string('code',30)->nullable();
            $table->integer('expense_category_id')->default(0);
            $table->timestamp('date')->nullable();
            $table->string('amount',20)->default(0);
            $table->string('document')->nullable();
            $table->longText('note')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
        Schema::create('expense_payment', function (Blueprint $table) {
            $table->id();
            $table->integer('expense_id')->default(0);
            $table->integer('account_id')->default(0);
            $table->date('date')->nullable();
            $table->string('amount',20)->default(0);
            $table->integer('created_by');
            $table->timestamps();
        });
        Schema::create('account_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('account_type_id')->default(0);
        });
        Schema::create('account', function (Blueprint $table) {
            $table->id();
            $table->string('code',20);
            $table->string('name');
            $table->integer('account_type_id')->default(0);
            $table->string('balance',30)->default(0);
            $table->string('label')->nullable();
            $table->string('value')->nullable();
            $table->longText('note')->nullable();
            $table->enum('st',['a','c'])->default('a');
            $table->timestamp('created_at');
            $table->integer('created_by');
        });
    }
    public function down()
    {
        Schema::dropIfExists('expense_category');
        Schema::dropIfExists('expense');
        Schema::dropIfExists('expense_payment');
        Schema::dropIfExists('account_type');
        Schema::dropIfExists('account');
    }
}
