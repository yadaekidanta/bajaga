<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('product_category', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->string('photo')->nullable();
            $table->integer('product_category_id')->default(0);
            $table->enum('show_on_home',['n','y'])->default('n');
        });
        Schema::create('product_unit', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('shortname')->nullable();
        });
        Schema::create('product_brand', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('photo')->nullable();
            $table->longText('note')->nullable();
        });
        Schema::create('product_warranty', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('description');
            $table->string('duration',4);
            $table->enum('duration_type',['Hari','Bulan','Tahun']);
        });
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->integer('store_id')->default(0);
            $table->integer('product_unit_id')->default(0);
            $table->integer('product_brand_id')->default(0);
            $table->integer('product_warranty_id')->default(0);
            $table->integer('product_category_id')->default(0);
            $table->integer('product_subcategory_id')->nullable();
            $table->integer('business_location_id')->default(0);
            $table->string('sku');
            $table->string('name');
            $table->string('slug');
            $table->longText('desc');
            $table->string('video_url')->nullable();
            $table->string('photo')->nullable();
            $table->string('brocure')->nullable();
            $table->string('weight')->default(0);
            $table->string('price',20)->default(0);
            $table->string('stock',5)->default(0);
            $table->string('alert_quantity')->default(0);
            $table->string('custom_field_1')->nullable();
            $table->string('custom_field_2')->nullable();
            $table->string('custom_field_3')->nullable();
            $table->string('custom_field_4')->nullable();
            $table->enum('not_for_selling',['n','y'])->default('n');
            $table->enum('condition',['baru','bekas'])->nullable();
            $table->timestamps();
        });
        Schema::create('product_auction', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->default(0);
            $table->date('start');
            $table->date('end');
            $table->string('price',20)->default(0);
            $table->string('qty',4)->default(0);
            $table->string('qty_return',4)->default(0);
            $table->enum('st',['aktif','belum aktif','tidak terjual']);
        });
    }
    public function down()
    {
        Schema::dropIfExists('product_category');
        Schema::dropIfExists('product_unit');
        Schema::dropIfExists('product_brand');
        Schema::dropIfExists('product_warranty');
        Schema::dropIfExists('product');
        Schema::dropIfExists('product_auction');
    }
}
