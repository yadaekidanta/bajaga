<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmModule extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nik',16)->nullable();
            $table->string('code');
            $table->string('name');
            $table->string('username')->nullable();
            $table->string('phone',15)->nullable();
            $table->string('email')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->enum('type',['Offline','Online'])->default('Offline');
            $table->string('password')->nullable();
            $table->string('avatar')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('users_verify', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('token');
            $table->timestamps();
        });
        Schema::create('users_address', function (Blueprint $table) {
            $table->id();
            $table->integer('users_id');
            $table->string('desc')->nullable();
            $table->string('postcode',6)->nullable();
            $table->longText('address');
            $table->integer('province_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->integer('subdistrict_id')->default(0);
            $table->enum('is_use',['0','1'])->default(0);
            $table->timestamps();
        });
        Schema::create('users_point', function (Blueprint $table) {
            $table->id();
            $table->integer('users_id');
            $table->enum('type',['D','W']);
            $table->string('point',20)->default(0);
            $table->timestamps();
        });
        Schema::create('users_store', function (Blueprint $table) {
            $table->id();
            $table->integer('users_id');
            $table->string('name');
            $table->longtext('desc')->nullable();
            $table->string('photo')->nullable();
            $table->longText('address');
            $table->integer('province_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->integer('subdistrict_id')->default(0);
            $table->string('ktp')->nullable();
            $table->string('selfie_ktp')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->timestamps();
        });
        Schema::create('store_point', function (Blueprint $table) {
            $table->id();
            $table->integer('store_id');
            $table->enum('type',['D','W']);
            $table->string('point',20)->default(0);
            $table->timestamps();
        });
        Schema::create('store_schedules', function (Blueprint $table) {
            $table->id();
            $table->integer('store_id');
            $table->enum('day',["Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu"]);
            $table->string('open',5);
            $table->string('close',5);
            $table->timestamps();
        });
        Schema::create('supplier', function (Blueprint $table) {
            $table->id();
            $table->string('code',15);
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->longText('address');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('users_verify');
        Schema::dropIfExists('users_address');
        Schema::dropIfExists('users_point');
        Schema::dropIfExists('users_store');
        Schema::dropIfExists('store_point');
        Schema::dropIfExists('store_schedules');
        Schema::dropIfExists('supplier');
    }
}
