<?php

namespace Database\Seeders;

use App\Models\User;
use App\Helpers\Helper;
use App\Models\Supplier;
use App\Models\UserStore;
use App\Models\UserAddress;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CrmSeeder extends Seeder
{
    public function run()
    {
        $user = array(
            [
                'name' => 'Lindo Hari Sandi',
                'username' => 'lindo.h.sandi',
                'email' => 'lindo.h.sandi@gmail.com',
                'phone' => '0812666436',
                'phone_verified_at' => date("Y-m-d H:i:s"),
                'email_verified_at' => date("Y-m-d H:i:s"),
                'type' => 'Online',
                'password' => Hash::make('password'),

            ]
        );
        $uaddress = array(
            [
                'users_id' => '1',
                'address' => 'Jalan Talaga Bodas No 29',
                'province_id' => '9',
                'city_id' => '23',
                'subdistrict_id' => '359',
                'is_use' => '1',
                'postcode' => '154125',
            ]
        );
        $ustore = array(
            [
                'users_id' => '1',
                'name' => 'Bajaga Store',
                'address' => 'Jalan Talaga Bodas No 29',
                'province_id' => '9',
                'city_id' => '23',
                'subdistrict_id' => '359',
                'verified_at' => date("Y-m-d H:i:s"),
            ]
        );
        $sup = array(
            [
                'name' => 'Bajaga Store',
                'email' => 'yadaekidanta@gmail.com',
                'phone' => '087748005611',
                'address' => 'Jalan Talaga Bodas No 29',

            ]
        );
        foreach($user AS $u){
            User::create([
                'code' => Helper::IDGenerator(new User, 'code','CUST-'),
                'name' => $u['name'],
                'username' => $u['username'],
                'email' => $u['email'],
                'phone' => $u['phone'],
                'phone_verified_at' => $u['phone_verified_at'],
                'email_verified_at' => $u['email_verified_at'],
                'type' => $u['type'],
                'password' => $u['password'],

            ]);
        }
        foreach($uaddress AS $ua){
            UserAddress::create([
                'users_id' => $ua['users_id'],
                'address' => $ua['address'],
                'province_id' => $ua['province_id'],
                'city_id' => $ua['city_id'],
                'subdistrict_id' => $ua['subdistrict_id'],
                'is_use' => $ua['is_use'],

            ]);
        }
        foreach($ustore AS $us){
            UserStore::create([
                'users_id' => $us['users_id'],
                'name' => $us['name'],
                'address' => $us['address'],
                'province_id' => $us['province_id'],
                'city_id' => $us['city_id'],
                'subdistrict_id' => $us['subdistrict_id'],
                'verified_at' => $us['verified_at'],

            ]);
        }
        foreach($sup AS $s){
            Supplier::create([
                'code' => Helper::IDGenerator(new Supplier, 'code','SUP-'),
                'name' => $s['name'],
                'email' => $s['email'],
                'phone' => $s['phone'],
                'address' => $s['address'],

            ]);
        }
    }
}
