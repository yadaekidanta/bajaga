<?php

namespace Database\Seeders;

use App\Models\Courier;
use Illuminate\Database\Seeder;

class CourierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list_courier = ["jne","pos","tiki","rpx","pandu","wahana","sicepat","jnt","ninja","lion","rex","anteraja"];
        foreach ($list_courier as $courier) {
            Courier::create([
                "name" => $courier
            ]);
        }
    }
}
