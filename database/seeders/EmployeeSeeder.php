<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Employee;

class EmployeeSeeder extends Seeder
{
    public function run()
    {
        $data = array(
            [
                'username' => 'admin',
                'name' => 'Administrator',
                'email' => 'admin@bajaga.com',
                'phone' => '081361432123',
                'role' => 'owner',
                'password' => Hash::make('password'),

            ]
        );
        foreach($data AS $d){
            Employee::create([
                'username' => $d['username'],
                'name' => $d['name'],
                'email' => $d['email'],
                'phone' => $d['phone'],
                'role' => $d['role'],
                'password' => $d['password'],

            ]);
        }
    }
}
