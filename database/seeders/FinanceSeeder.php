<?php

namespace Database\Seeders;

use App\Helpers\Helper;
use App\Models\Account;
use App\Models\AccountType;
use App\Models\ExpenseCategory;
use Illuminate\Database\Seeder;

class FinanceSeeder extends Seeder
{
    public function run()
    {
        $ecat = array(
            [
                'name' => 'Pembelian Spare Part',
            ],
            [
                'name' => 'Biaya Listrik',
            ],
            [
                'name' => 'Biaya Internet',
            ],
            [
                'name' => 'Pembelian Aset Usaha',
            ],
            [
                'name' => 'Bunga Hutang Bank',
            ],
            [
                'name' => 'Biaya Sewa Bangunan',
            ],
            [
                'name' => 'Biaya Lain-lain',
            ],
            [
                'name' => 'Biaya Admin Bank',
            ],
            [
                'name' => 'Pembelian Material Packaging',
            ],
        );
        $atype = array(
            [
                'name' => 'Pendapatan',
                'account_type_id' => '0',
            ],
            [
                'name' => 'Utang',
                'account_type_id' => '0',
            ],
            [
                'name' => 'Modal',
                'account_type_id' => '0',
            ],
            [
                'name' => 'Biaya',
                'account_type_id' => '0',
            ],
            [
                'name' => 'Asset',
                'account_type_id' => '0',
            ],
            [
                'name' => 'Pinjaman Bank',
                'account_type_id' => '2',
            ],
            [
                'name' => 'Cash',
                'account_type_id' => '5',
            ],
            [
                'name' => 'Persediaan Barang',
                'account_type_id' => '5',
            ],
            [
                'name' => 'Piutang Penjualan',
                'account_type_id' => '5',
            ],
            [
                'name' => 'Rekening Bank',
                'account_type_id' => '5',
            ],
            [
                'name' => 'Piutang Ragu-ragu',
                'account_type_id' => '5',
            ],
        );
        $account = array(
            [
                'code' => '5680537455',
                'name' => 'Bank BCA Kemang',
                'account_type_id' => '10',
                'balance' => '100000000',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => '1',
            ],
            [
                'code' => '4140835264',
                'name' => 'Bank BCA Koja',
                'account_type_id' => '10',
                'balance' => '100000000',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => '1',
            ],
            [
                'code' => '708601010841504',
                'name' => 'Bank BRI',
                'account_type_id' => '10',
                'balance' => '100000000',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => '1',
            ],
            [
                'code' => '003631003377',
                'name' => 'Bank Danamon',
                'account_type_id' => '10',
                'balance' => '100000000',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => '1',
            ],
            [
                'code' => '1560010313767',
                'name' => 'Bank Mandiri',
                'account_type_id' => '10',
                'balance' => '100000000',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => '1',
            ],
            [
                'code' => '0001',
                'name' => 'Cash',
                'account_type_id' => '7',
                'balance' => '0',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => '1',
            ],
        );
        foreach($ecat AS $ec){
            ExpenseCategory::create([
                'code' => Helper::IDGenerator(new ExpenseCategory, 'code','BY-'),
                'name' => $ec['name'],
            ]);
        }
        foreach($atype AS $at){
            AccountType::create([
                'name' => $at['name'],
                'account_type_id' => $at['account_type_id'],
            ]);
        }
        foreach($account AS $a){
            Account::create([
                'code' => $a['code'],
                'name' => $a['name'],
                'account_type_id' => $a['account_type_id'],
                'balance' => $a['balance'],
                'created_at' => $a['created_at'],
                'created_by' => $a['created_by'],
            ]);
        }
    }
}
