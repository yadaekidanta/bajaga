<?php

namespace Database\Seeders;

use App\Models\Bank;
use App\Helpers\Helper;
use App\Models\FaqCategory;
use App\Models\ProductUnit;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductWarranty;
use Illuminate\Database\Seeder;
use App\Models\BusinessLocation;

class MasterSeeder extends Seeder
{
    public function run()
    {
        $fcat = array(
            [
                'name' => 'Akun Saya',
            ],
            [
                'name' => 'Pesanan Saya',
            ],
            [
                'name' => 'Pembayaran',
            ],
            [
                'name' => 'Pengiriman',
            ],
            [
                'name' => 'Pengembalian Dana',
            ],
            [
                'name' => 'Komplain Pesanan',
            ],
            [
                'name' => 'Promosi',
            ],
            [
                'name' => 'Lainnya',
            ],
        );
        $pwar = array(
            [
                'name' => 'Garansi Smartphone',
                'description' => 'Garansi berlaku selama 10 Hari sejak tanggal pembelian secara online (Marketplace Tokopedia, Shopee, dan Bukalapak) atau 7 hari untuk pembelian secara offline (Bayar di Tempat). Garansi tidak berlaku apabila kerusakan unit terjadi akibat kesalahan pengguna seperti (terkena cairan, terjatuh, terbakar, melakukan update software dll.) Kami juga menyediakan layanan perpanjang garansi yang dapat anda beli pada etalase toko online atau katalog produk.',
                'duration' => '7',
                'duration_type' => 'Hari',
            ],
        );
        $pbran = array(
            [
                'name' => 'Apple',
                'slug' => 'apple',
            ],
            [
                'name' => 'Samsung',
                'slug' => 'samsung',
            ],
        );
        $punit = array(
            [
                'name' => 'Meter',
                'shortname' => 'M²',
            ],
            [
                'name' => 'Pieces',
                'shortname' => 'PCS',
            ],
        );
        $pcat = array(
            [
                'title' => 'Elektronik',
                'slug' => 'elektronik',
                'product_category_id' => '0',
            ],
            [
                'title' => 'Handphone & Tablet',
                'slug' => 'handphone-tablet',
                'product_category_id' => '0',
            ],
            [
                'title' => 'Android',
                'slug' => 'android',
                'product_category_id' => '2',
            ],
            [
                'title' => 'IOS',
                'slug' => 'ios',
                'product_category_id' => '2',
            ],
        );
        $loc = array(
            [
                'name' => 'HQ Yada Ekidanta',
                'address' => 'Jalan Talaga Bodas No 29',
                'province_id' => '9',
                'city_id' => '23',
                'subdistrict_id' => '359',
                'phone' => '087748005611',
                'email' => 'yadaekidanta@gmail.com',

            ]
        );
        $bank = array(
            [
                "name" => "Bank Central Asia",
                "code" => "BCA",
                "thumbnail" => "banks/bca.png",
                "is_activated" => false
            ],
            [
                "name" => "Bank Negara Indonesia",
                "code" => "BNI",
                "thumbnail" => "banks/bni.png",
                "is_activated" => true
            ],
            [
                "name" => "Bank Mandiri",
                "code" => "MANDIRI",
                "thumbnail" => "banks/mandiri.png",
                "is_activated" => true
            ],
            [
                "name" => "Bank Permata",
                "code" => "PERMATA",
                "thumbnail" => "banks/permata-bank.png",
                "is_activated" => true
            ],
            [
                "name" => "Bank Rakyat Indonesia",
                "code" => "BRI",
                "thumbnail" => "banks/bri.png",
                "is_activated" => true
            ],
            [
                "name" => "Bank CIMB Niaga",
                "code" => "CIMB",
                "thumbnail" => "banks/cimb-niaga.png",
                "is_activated" => false
            ],
            [
                "name" => "Bank Syariah Indonesia",
                "code" => "BSI",
                "thumbnail" => "banks/bank-syariah-indonesia.png",
                "is_activated" => true
            ]
        );
        // foreach($fcat AS $fc){
        //     FaqCategory::create([
        //         'name' => $fc['name'],
        //     ]);
        // }
        // foreach($pcat AS $pc){
        //     ProductCategory::create([
        //         'title' => $pc['title'],
        //         'slug' => $pc['slug'],
        //         'product_category_id' => $pc['product_category_id'],
        //     ]);
        // }
        // foreach($pbran AS $pb){
        //     ProductBrand::create([
        //         'name' => $pb['name'],
        //         'slug' => $pb['slug'],
        //     ]);
        // }
        // foreach($punit AS $pu){
        //     ProductUnit::create([
        //         'name' => $pu['name'],
        //         'shortname' => $pu['shortname'],
        //     ]);
        // }
        // foreach($pwar AS $pw){
        //     ProductWarranty::create([
        //         'name' => $pw['name'],
        //         'description' => $pw['description'],
        //         'duration' => $pw['duration'],
        //         'duration_type' => $pw['duration_type'],
        //     ]);
        // }
        // foreach($loc AS $l){
        //     BusinessLocation::create([
        //         'code' => Helper::IDGenerator(new BusinessLocation, 'code','BJG-'),
        //         'name' => $l['name'],
        //         'address' => $l['address'],
        //         'province_id' => $l['province_id'],
        //         'city_id' => $l['city_id'],
        //         'phone' => $l['phone'],
        //         'email' => $l['email'],
        //     ]);
        // }
        foreach($bank AS $b){
            // dd($b["name"]);
            Bank::create([
                'name' => $b['name'],
                'code' => $b['code'],
                'thumbnail' => $b['thumbnail'],
                'is_activated' => $b['is_activated']
            ]);
        }
    }
}
