<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            RegionalSeeder::class,
            MasterSeeder::class,
            FinanceSeeder::class,
            CrmSeeder::class,
            EmployeeSeeder::class,
            CourierTableSeeder::class,
        ]);
    }
}
