<x-web-layout title="List Produk" keyword="Bajaga Store">
    <div class="container-custom px-3 py-3 bg-white">
        <!-- product recomendation -->
        <div class="d-flex flex-wrap justify-content-between gap-3 mt-2 scrolling-pagination" id="list_result">
            {{-- @include('page.web.mobile.catalog.data') --}}
        </div>
        <div class="ajax-load d-flex justify-content-center w-100 mt-5">
            <div class="spinner-grow text-secondary" role="status">
            </div>
        </div>
        <!-- /product recomendation -->
        <form id="content_filter" class="d-none">
            <input type="hidden" name="keyword" id="input-filter">
        </form>
    </div>

    @section('custom_js')
    <script>
        let urlParams = new URLSearchParams(params)
        let keyword = urlParams.get('keyword')
        if (keyword) {
            // alert()
            $("#input-filter").val(keyword)
            localStorage.setItem("page_infinate", 1)
            load_list(1)
        }else{
            localStorage.setItem("page_infinate", 1)
            load_list(1)
        }
        console.log();

        //function for Scroll Event
        $(document).ready(function(){
            let options = {
                root: null,
                rootMargin: '5px',
                threshold: 0.5
            }

            const observer = new IntersectionObserver(handleIntersect, options)
            observer.observe(document.querySelector(".ajax-load"))
        })
        
        function handleIntersect(entries){
            if (entries[0].isIntersecting) {
                localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                load_more(parseInt(localStorage.getItem("page_infinate")));
            }
        }
    </script>
    @endsection
</x-web-layout>