@foreach ($collections as $item)
    <a href="{{ route('web.product.show', [$item->slug]) }}" class="text-black w-47% element-items">
        <div class="section-card-vertical">
            <div class="container-card shadow">
                <div class="container-img">
                    <img src="{{$item->image}}" alt="">
                </div>
                <div class="p-1 description-product">
                    <div class="d-flex flex-column">
                        <span class="font-sm text-capitalize line-normal">{{$item->name}}</span>
                        <span class="font-medium font-bold">Rp {{number_format($item->price)}}</span>
                        <div class="d-flex align-items-center gap-2">
                            <img class="logo-sm" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                            <div>
                                <span class="font-sm">{{Str::limit($item->product_store->name, 15)}}</span>
                                <span class="font-sm">{{Str::limit($item->product_store->city->name, 15)}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
@endforeach