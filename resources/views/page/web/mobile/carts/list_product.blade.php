@foreach ($product_recomendation as $item)
    <a href="{{ route('web.product.show', [$item->slug]) }}" class="text-black w-47% element-items position-relative">
        <div class="section-card-vertical">
            <div class="container-card shadow">
                <div class="container-img">
                    <img src="{{$item->image}}" alt="">
                </div>
                <div class="description-product p-1">
                    <div class="d-flex flex-column">
                        <span class="font-sm  text-capitalize line-normal">{!! Str::limit($item->name, 18, ' ...') !!}</span>
                        <span class="font-medium  font-bold">Rp {{number_format($item->price)}}</span>
                        <div class="d-flex gap-2 mt-2">
                            <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                            <span style="margin-top: -5px !important;">
                                <span class="text-capitalize line-normal">{{$item->product_store->name}}</span>
                                <span class="font-sm">{{\Str::limit($item->product_store->city->name, 14)}}</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
@endforeach