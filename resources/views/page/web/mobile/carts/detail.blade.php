<x-web-layout title="Keranjang" keyword="Bajaga Store">
    <div class="container-custom">
        <div class="px-3 bg-white pt-2 pb-2" style="margin-top: -1.6em;">
            <div class="d-flex gap-3 align-items-center">
                <input id="check_all" type="checkbox" class="check_all big-checkbox">
                <label for="check_all" class="text-capitalize m-0">Pilih Semua Barang</label>
            </div>
        </div>
    
        <!-- setting address -->
        <div class="py-1 bg-white mt-1">
            <div class="px-3 d-flex align-items-center gap-2" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" viewBox="0 0 24 24"><path d="M15.5 23c0 .552-1.566 1-3.5 1s-3.5-.448-3.5-1 1.566-1 3.5-1 3.5.448 3.5 1zm2.5-17c0 2.972-2.164 5.433-5 5.91v6.09l-2 2v-8.089c-2.836-.477-5-2.938-5-5.911 0-3.314 2.687-6 6-6s6 2.687 6 6zm-2 0c0-2.206-1.794-4-4-4s-4 1.794-4 4 1.794 4 4 4 4-1.794 4-4zm-5.618 2.098c2.339 1.84 5.563-.722 3.858-3.539.313 2.237-1.956 4.03-3.858 3.539z"/></svg>
                <span>Dikirim ke</span>
                <span class="font-bold">{{$user->name}}</span>
                <svg class="unf-icon" viewBox="0 0 24 24" width="14" height="14" fill="var(--color-icon-enabled, #2E3137)" style="display: inline-block; vertical-align: middle;"><path d="M12 15.25a.74.74 0 0 1-.53-.22l-5-5A.75.75 0 0 1 7.53 9L12 13.44 16.47 9a.75.75 0 0 1 1.06 1l-5 5a.74.74 0 0 1-.53.25Z"></path></svg>
            </div>
        </div>
        <!-- /setting address -->
        <hr class="m-0">
        <!-- list -->
        
                    
        <div id="content_list">
            <div class="d-flex flex-column gap-2 justify-content-between mb-2 container-list-item">
                <div id="list_result"></div>
            </div>
        </div>
        <!-- list -->

        <!-- kupon -->
        <div class="px-3 py-3 bg-white mt-1">
            <!-- shipping choice -->
            <div class="w-full opacity-65" id="button-cupon" role="button" data-bs-toggle="" data-bs-target="#canvasCupon" aria-controls="canvasCupon">
                <div class="border rounded p-1 bg-white d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center gap-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#3E74FD" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd"><path d="M0 0h24v24H0z"/><path fill="#3E74FD" fill-rule="nonzero" d="M10.194 2.665a2.783 2.783 0 0 1 3.611 0l.838.713c.446.38 1 .61 1.584.657l1.098.087a2.783 2.783 0 0 1 2.552 2.553l.088 1.097c.047.585.276 1.138.656 1.585l.714.837a2.781 2.781 0 0 1 0 3.61l-.714.838c-.38.447-.61 1-.656 1.585l-.088 1.097a2.783 2.783 0 0 1-2.552 2.553l-1.098.087a2.793 2.793 0 0 0-1.584.656l-.838.714a2.781 2.781 0 0 1-3.61 0l-.838-.714c-.446-.38-1-.61-1.584-.656l-1.097-.087a2.785 2.785 0 0 1-2.554-2.553l-.087-1.097a2.782 2.782 0 0 0-.656-1.585l-.714-.838a2.781 2.781 0 0 1 0-3.61l.714-.837c.38-.447.61-1 .656-1.585l.087-1.097a2.786 2.786 0 0 1 2.554-2.553l1.097-.087a2.79 2.79 0 0 0 1.584-.657zm5.571 5.57a.797.797 0 0 0-1.13 0l-6.4 6.4a.797.797 0 0 0 0 1.13.798.798 0 0 0 1.131.001l6.4-6.4a.8.8 0 0 0 0-1.131zm-1.194 4.908a1.428 1.428 0 1 0 0 2.855 1.428 1.428 0 0 0 0-2.855zM9.43 8a1.429 1.429 0 1 0-.002 2.858A1.429 1.429 0 0 0 9.43 8z"/></g></svg>
                        <span class="font-bold text-cupon">Makin hemat pakai promo</span>
                    </div>

                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 40 40"><g fill="none" fill-rule="evenodd"><circle cx="24" cy="24" r="24"/><path stroke="#6c727c" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" d="M16.53 27.47l7.202-7.202m-7.202-7.2l7.202 7.203"/></g></svg>
                </div>
            </div>
            <!-- shipping choice -->
        </div>
        <!-- /kupon -->

        <!-- product recomendation -->
        <div class="p-2 bg-white d-flex flex-column mt-2">
            <div class="w-full my-1 d-flex justify-content-start">
                <h3 class="text-start">
                    Rekomendasi untukmu
                </h3>
            </div>
            <div class="d-flex flex-wrap gap-3 mt-2 scrolling-pagination">
                @include('page.web.mobile.carts.list_product')
            </div>
            <div class="ajax-load text-center mt-3" style="display:none">
                <p class="text-primary font-bold">Load More Post...</p>
            </div>
        </div>
        <!-- /product recomendation -->
    </div>

    <!-- canvas cupon -->
    <div class="offcanvas offcanvas-end" tabindex="-1" id="canvasCupon" aria-labelledby="canvasCuponLabel">
        <div class="offcanvas-header">
            <h3 id="offcanvasRightLabel">Pilih Promo</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <!-- <form action="">
                <div class="d-flex gap-1 align-items-center">
                    <input type="text" name="code_cupon" class="form-control">
                    <input type="submit" value="Terapkan" class="btn btn-primary">
                </div>
            </form> -->

            <!-- <hr class="my-1"> -->

            <!-- list cupon -->
            <div class="cupon-container">
                @php
                    $i = 0;
                @endphp
                @foreach($cupons as $cupon)
                <div onclick="use_cupon(`{{$cupon->type}}`, `{{$cupon->type == 'harga' ? $cupon->harga : $cupon->persentase}}`, '{{$cupon->name}}', this)" id="cupon-card item-{{++$i}}" class="card my-3 cupon-card" data-target="parent-card">
                    <div data-target="parent-header" class="card-header bg-header-opacity">
                        <h4 class="font-bold">{{$cupon->name}}</h4>
                        <a href="{{route('web.discount.detail', $cupon->id)}}" class="font-sm ">Lihat detail</a>
                    </div>
                    <div data-target="parent-body" class="card-body">
                        <!-- info -->
                        <div class="">
                            {{$cupon->desc}}
                        </div>
                        <div data-target="parent-info" class="d-flex align-items-center gap-2">
                            <i class="icon-clock1"></i>
                            <span>{{$cupon->end_at}}</span>
                        </div>
                        <!-- info -->
                    </div>
                </div>
                @endforeach
            </div>
            <!-- list cupon -->
        </div>
    </div>
    <!-- /canvas cupon -->

    <!-- canvas address -->
    <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h2 class="offcanvas-title font-bold " id="offcanvasBottomLabel">Mau kirim belanjaan kemana?</h2>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body px-0 small">
            <div class="d-flex justify-content-center">
                <span>Biar pengalaman belanjamu lebih baik, pilih alamat dulu</span>
            </div>

            <!-- container list address -->
            <div class="px-3 d-flex align-items-center gap-3 overflow-scroll container-address">
                @foreach($address as $a)
                    <div data-target="parent" data-address="{{$a->id}}" class="card-address rounded {{$a->is_use == 1 ? 'active' : '';}} p-1 mt-2">
                        <div data-target="parent-header">
                            <span class="font-sm">{{$user->name}}</span>
                        </div>
        
                        <div data-target="parent-body" class="d-flex flex-column text-box" data-maxlength="25">
                            <span class="font-sm text-secondary">{{$a->users->phone}}</span>
                            <span class="font-sm text-secondary">{{$a->users->address}}</span>
                            <span class="font-sm text-secondary text-capitalize">{{$a->subdistrict->name}}, {{$a->city->name}}, {{$a->province->name}}, ID {{$a->postcode}}</span> 
                        </div>
        
                    </div>
                @endforeach
            </div>
            <!-- /container list address -->
        </div>
    </div>
    <!-- /canvas address -->

    @section('custom_js')
    <script>
        // payment_content('order_content');
        load_list(1);

        function loadMoreData(page)
        {
            $.ajax({
                url: '/products/recomendation?page=' + page,
                type:'get',
                beforeSend: function()
                {
                    $(".ajax-load").show();
                }
            })
            .done(function(data){
                if(data.html == ""){
                    $('.ajax-load').html("No more Items Found!");
                    return;
                }
                $('.ajax-load').hide();
                $(".scrolling-pagination").append(data.html);
            })
            // Call back function
            .fail(function(jqXHR,ajaxOptions,thrownError){
                alert("Server not responding.....");
            });
        }

        //function for Scroll Event
        var pages = 1;
        $(window).scroll(function(){
            // console.log($(window).scrollTop());
            if($(window).scrollTop() + $(window).height() >= $(document).height()){
                pages++;
                loadMoreData(pages);
            }
        });

        let total_harga = 0;
        let total_harga_tmp = 0;

        $(".text-box span").text(function(index, currentText) {
            var maxLength = $(this).parent().attr('data-maxlength');
            if(currentText.length >= maxLength) {
                return currentText.substr(0, maxLength) + "...";
            } else {
                return currentText
            } 
        });


        function decrementQty()
        {
            $target = $(event.target);
            let count_item = parseInt($target.next().html())
            if ( count_item > 1) {
                $target.next().html(count_item - 1)
            }
        }

        function incrementQty()
        {
            let stock = $("#stock").val()
            // console.log(parseInt(stock) > parseInt($("#qty_count").text()), parseInt(stock), parseInt($("#qty_count").text()));
            if (parseInt(stock) > parseInt($("#qty_count").text())) {
                $.ajax({
                    type: 'PATCH',
                    url: `/cart/increase/${$('#id_product').val()}`,
                    success: function(response){
                        // console.log(response);
                        $target = $(event.target);
                        let count_item = parseInt($target.prev().html())
                        $target.prev().html(count_item + 1)
                    }
                })
            }
        }

        function use_cupon(type, discount, name, element)
        {
            total_harga = total_harga_tmp
            if(element.classList.contains('border')){
                name = "Makin hemat pakai promo"
            }else{
                name = limit_text(name)
                if (type == "harga") {
                    total_harga -= parseInt(discount)
                } else {
                    total_harga -= total_harga * (parseInt(discount) / 100)
                }
            }

            $(".text-cupon").text(`${name}`)
            $("#total_price").html(`Rp ${formatRupiah(total_harga.toString())}`)
        }

        $(".cupon-container").on('click', function (event) {
            
            $target = $(event.target);
            if ($target.parent().parent().parent().hasClass('border border-primary')) {
                $('.cupon-container > *').removeClass('border border-primary')
            }else if($target.parent().parent().hasClass('border border-primary')){
                $('.cupon-container > *').removeClass('border border-primary')
            }else if($target.parent().hasClass('border border-primary')){
                $('.cupon-container > *').removeClass('border border-primary')
            }else{
                $('.cupon-container > *').removeClass('border border-primary')
                if ($target.parent().attr("data-target") == "parent-info") {
                    $target.parent().parent().parent().addClass('border border-primary');
                }else if($target.parent().attr("data-target") == "parent-body"){
                    $target.parent().parent().addClass('border border-primary');
                }else if($target.parent().attr("data-target") == "parent-header"){
                    $target.parent().parent().addClass('border border-primary');
                }else{
                    $target.parent().addClass('border border-primary');
                }
            }
            // console.log($target);
        });

        $(".container-address").on('click', function (event) {
            $target = $(event.target);
            $('.container-address > *').removeClass('active')
            if ($target.parent().attr("data-target") == "parent-header") {
                $target.parent().parent().addClass(' active');
                is_use($target.parent().parent().attr("data-address"))
            }else if($target.parent().attr("data-target") == "parent-body"){
                $target.parent().parent().addClass(' active');
                is_use($target.parent().parent().attr("data-address"))
            }else{
                $target.parent().addClass(' active');
                is_use($target.parent().attr("data-address"))
            }
            // console.log($target);
        });

        function is_use(id, target){
            // console.log($(".is_use:checked").next().val())
            $.ajax({
                type: 'PATCH',
                url: `/user-address/update/${id}`,
                dataType: 'json',
                processData: false,
                success: function(response){
                    if(response.alert == 'success'){
                        Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    }
                }
            })
        }
        
    </script>
    @endsection

</x-web-layout>