@foreach ($store as $toko)
    <div class="mb-3" id="list-product">
        @php
        $carts = \App\Models\Cart::where('store_id',$toko->store_id)->where('user_id',$user_id)->get();
        
        @endphp
        <div class="py-2 bg-white">
            <div class="px-3 d-flex align-items-center gap-3 header-store">
                <input id="check_all_store" type="checkbox" class="check_all_store big-checkbox">
                <div class="d-flex flex-column justify-content-between">
                    <span class="font-bold">{{$carts[0]->cart_store->name}}</span>
                    <span class="font-thin font-sm">{{$carts[0]->cart_store->city->name}}</span>
                </div>
            </div>
            @foreach($carts as $st)
            
            <div class="px-3 d-flex gap-3 mt-2 item-product">
                <input type="hidden" value="{{$st->product->stock}}" id="stock">
                <input type="hidden" value="{{$st->product->price}}" id="price_product_to">
                <input type="hidden" value="{{$st->id}}" id="id_cart">
                <input type="hidden" value="{{$st->product->id}}" id="id_product">
                <input id="check_all_store_item" type="checkbox" class="check_all_store_item big-checkbox">
                <div class="d-flex gap-3">
                    <div class="border w-20 h-20 overflow-hidden align-items-center justify-content-center d-flex">
                        <img src="{{asset($st->product != null ? 'storage/'.$st->product->photo : 'img/product/all-product.png')}}" class="object-fit-cover w-100 h-100" alt="">
                    </div>
                    <div class="d-flex flex-column">
                        <span class="font-medium">{{$st->product->name}}</span>
                        <h3 class="m-0 font-bold">Rp {{number_format($st->product->price)}}</h3>
                    </div>
                </div>
            </div>

            <div class="px-3 d-flex gap-3 mt-3 align-items-center justify-content-end">
                <div class="d-flex gap-4 align-items-center">
                    <!-- counter item -->
                    
                    <div class="d-flex justify-content-between px-1 align-items-center gap-3 border rounded">
                        <i class="icon-line-minus decrement"></i>
                        <span id="qty_count">{{$st->qty}}</span>
                        <i class="icon-line-plus increment"></i>
                    </div>
                    <!-- counter item -->
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endforeach

<script>
    $(".check_all").change(function() {
        if ($(".check_all")[0].checked == true) {
            $(".check_all_store").prop('checked', true)
            $(".check_all_store_item").prop('checked', true)
        } else {
            $(".check_all_store").prop('checked', false)
            $(".check_all_store_item").prop('checked', false)
        }
        reload_price()
        if (getListItemChecked().length == 0) {
            $("#btn-beli").prop("disabled", true)
        }else{
            $("#btn-beli").prop("disabled", false)
        }
    })

    let item_collections = [].slice.call(document.getElementsByClassName('item-product'))
    let is_checked_count_all_item = 0
    function reload_price()
    {
        total_harga = 0
        is_checked_count_all_item = 0
        item_collections.forEach(element => {
            
            if ($(element).children(".check_all_store_item")[0].checked) {
                let price = $(element).children("#price_product_to")[0].value;
                let qty = $(element).next().children(".align-items-center").children(".align-items-center").children("#qty_count")[0].textContent;
                total_harga += (parseInt(qty) * parseInt(price))
                is_checked_count_all_item += 1
                $("#button-cupon").attr("data-bs-toggle", "offcanvas")
                $("#button-cupon").removeClass("opacity-65")
            }
        });
        if (item_collections.length == is_checked_count_all_item) {
            $(".check_all").prop('checked', true)
        }else if(is_checked_count_all_item == 0){
            $("#button-cupon").removeAttr("data-bs-toggle", "offcanvas")
            $("#button-cupon").addClass("opacity-65")
            name = "Makin hemat pakai promo"
            $(".text-cupon").text(`${name}`)
        }
        else{
            $(".check_all").prop('checked', false)
        }
        total_harga_tmp = total_harga
        // console.log(parseInt($("#qty_count").text())+'asd', $("#price_product_to").val());
        $("#total_price").html(`Rp ${formatRupiah(total_harga.toString())}`)
    }

    reload_price()

    // console.log();
    $(".check_all_store_item").change(function(){
        let list_item = [].slice.call($(this).parent().parent().children(".item-product"))
        let is_checked_count = 0
        list_item.forEach(element => {
            if ($(element).children(".check_all_store_item")[0].checked) {
                is_checked_count++
            }
        });
        if (is_checked_count == list_item.length) {            
            $(this).parent().parent().children(".header-store").children(".check_all_store")[0].checked = true;
        }else{
            $(this).parent().parent().children(".header-store").children(".check_all_store")[0].checked = false;
        }
        reload_price()
        if (getListItemChecked().length == 0) {
            $("#btn-beli").prop("disabled", true)
        }else{
            $("#btn-beli").prop("disabled", false)
        }
    })

    $(".check_all_store").change(function(){
        
        if ($(this)[0].checked == true) {
            var arr = [].slice.call($(this).parent().parent().children(".item-product"));
            arr.forEach(element => {
                // console.log(element);
                $(element).children(".check_all_store_item").prop('checked', true)
            });
        } else {
            $(".check_all").prop('checked', false)
            var arr = [].slice.call($(this).parent().parent().children(".item-product"));
            arr.forEach(element => {
                // console.log(element);
                $(element).children(".check_all_store_item").prop('checked', false)
            });
        }
        reload_price()
        if (getListItemChecked().length == 0) {
            $("#btn-beli").prop("disabled", true)
        }else{
            $("#btn-beli").prop("disabled", false)
        }
    })
    $(".increment").click(function() {
         
        let qty = parseInt($(this).prev("#qty_count")[0].textContent)
        let content = $(this).prev("#qty_count")[0]
        // console.log(qty + 1);
        let stock = parseInt($(this).parent().parent().parent().prev().children("#stock")[0].value)
        console.log(qty < stock, qty , stock);
        let id = $(this).parent().parent().parent().prev().children("#id_cart")[0].value
        $.ajax({
            type: "PATCH",
            url: `/cart/increase/${id}`,
            dataType: 'json',
            success: function(response) { 
                if (response.alert == "success") {
                    
                    if (qty < stock) {
                        content.textContent++
                        reload_price()
                    }
                    // console.log(response);
                } else {
                    error_toastr(response.message);
                } 
            }
        });
    })

    function removeElement(root, element)
    {
        let sum_of_product = root.children(".item-product").length
        if (sum_of_product == 1) {
            root.parent().remove()
        }else{
            let content = element.prev()
            let count_content = element
            content.remove()
            count_content.remove()
            // console.log(element);
        }
    }

    function ajaxDecrease(id, callback){
        $.ajax({
            type: "PATCH",
            url: `/cart/decrease/${id}`,
            dataType: 'json',
            success: function(response) { 
                if (response.alert == "success") {
                    callback(true)
                } else {
                    error_toastr(response.message);
                    callback(false)
                } 
            }
        });
    }

    $(".decrement").click(function() {
        let qty = parseInt($(this).next("#qty_count")[0].textContent)
        let content = $(this).next("#qty_count")[0]
        // console.log(qty + 1);
        let stock = parseInt($(this).parent().parent().parent().prev().children("#stock")[0].value)
        let id = $(this).parent().parent().parent().prev().children("#id_cart")[0].value
        let store_container = $(this).parent().parent().parent().parent()
        let item_container = $(this).parent().parent().parent()
        if (qty == 1) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        // do it ajax
                        let decreasing = ajaxDecrease(id, function(cond){
                            if (cond) {
                                removeElement(store_container, item_container)
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )
                                if (item_collections.length == 1) {
                                    total_harga = 0
                                    $("#total_price").html(`Rp ${formatRupiah(total_harga.toString())}`)
                                    $(".check_all").prop('checked', false)
                                } 

                            }
                        })
                    }
            })
        }else{
            ajaxDecrease(id, function(cond){
                if (cond) {
                    content.textContent--
                    reload_price()
                }
            })
        }
    })

    function getListItemChecked(){
        let arrayItems = [];
        item_collections.forEach(element => {
            if ($(element).children(".check_all_store_item")[0].checked) {
                let id = $(element).children("#id_product")[0].value
                arrayItems.push(id)
            }
        });

        return arrayItems
    }

    if (getListItemChecked().length == 0) {
        $("#btn-beli").prop("disabled", true)
    }else{
        $("#btn-beli").prop("disabled", false)
    }

    function beli(tombol, url, method, text_button = null){ 
        let arrayItems = getListItemChecked()
        if (arrayItems.langth == 0) {
            return false
        }

        if (text_button) {
            $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)
        }

        $(tombol).submit(function() {
            return false;
        });
        
        $(tombol).prop("disabled", true);

        $.ajax({
            type: method,
            url: url,
            data: {data: arrayItems},
            dataType: 'json',
            error: function(response) {
                Swal.fire({ 
                    text: response.responseJSON.message,
                    icon: "error", 
                    showDenyButton: true,
                    showCancelButton: false,
                    confirmButtonText: "Tambahkan",
                    denyButtonText: "Tidak"})
                    .then(result => {
                        if (result.isConfirmed) {
                            location.href = "{{route('web.user-address.create')}}"
                        }
                    })
                    $(tombol).submit(function() {
                        return true;
                    });

                    // console.log(arrayItems);
                    
                    $(tombol).prop("disabled", false);
                    if (text_button) {
                        $(tombol).html(text_button)
                    }
            },
            success: function(response){
                // console.log(response);
                if (response.alert == "success") {
                    location.href = response.redirect 
                }

                if (text_button) {
                    $(tombol).html(text_button)
                }
            }
        });
    }
</script>