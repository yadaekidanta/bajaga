<x-web-layout title="Notifikasi" keyword="Bajaga Store">
    <div class="bg-white pt-3 container-custom">
        <!-- latest notification -->
        <div>
            <!-- list notification -->
            <div class="notification-container">
                @foreach($collection as $cole)
                    <div class="d-flex flex-column px-3">
                        <span class="font-thin text-secondary">{{$cole->data["pesan"]}}</span>
                        
                        <!-- date -->
                        <div class="d-flex justify-content-end">
                            <span class="font-sm text-secondary float-end">{{$cole->read_at}}</span>
                        </div>
                        <!-- date -->
                        
                        <hr class="mt-1 mb-0">
                    </div>
                @endforeach
            </div>
            <!-- list notification -->
        </div>
        <!-- /latest notification -->
    </div>
    @section('custom_js')
    <script>
        $(".goBacks").click(function(){
            location.href = '/'
        })
    </script>
    @endsection
</x-web-layout>