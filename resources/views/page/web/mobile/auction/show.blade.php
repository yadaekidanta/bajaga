<x-web-layout title="Produk Lelang" keyword="Bajaga Store">
    <div class="container-custom bg-white pb-4">
        <div id="carousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{$productLelang->product_store->image}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{$productLelang->product_store->image}}" class="d-block w-100" alt="...">
                </div>
            </div>
        </div>

        <div class="px-3 bg-white py-2">
            <div class="font-xl font-bold">Rp. {{number_format($productLelang->price)}}</div><!-- Product Single - Price End -->
            <div class="mt-1">{{$productLelang->product_store->name}}</div>
            <div class="font-sm badge badge-primary mt-1">Bid ({{$bidCount}})</div>
            @auth
            <!-- form bid user -->
            <div class="mt-2">
                <form id="form_input">
                    <div class="d-flex form-group align-items-center gap-2 position-relative">
                        <input type="text" class="form-control" name="bid" id="bid">
                        <input type="hidden" name="product_id" id="product_id" value="{{ $productLelang->product_id }}">
                        <input type="hidden" name="price" id="price" value="{{ $productLelang->price }}">
                        <button id="tombol_kirim" onclick="handle_save('#tombol_kirim','#form_input','{{ route('web.auction.store') }}','POST', 'send');" class="btn btn-primary">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="white" width="14" viewBox="0 0 24 24"><path d="M24 0l-6 22-8.129-7.239 7.802-8.234-10.458 7.227-7.215-1.754 24-12zm-15 16.668v7.332l3.258-4.431-3.258-2.901z"/></svg>
                        </button>
                    </div>
                </form>
            </div>
            <!-- form bid user -->
            @endauth
        </div>
        <div class="px-3 bg-white py-2">
            <div id="list_result"></div>
        </div>
    </div>
    @section('custom_js')
    <script>
        load_list(1);
        setInterval(function(){
            load_list(1);
        }, 10000);

        $('#bid').on('keyup', function(){
            let rupiah = formatRupiah($(this).val())
            $(this).val(rupiah)
        })
    </script>
    @endsection
</x-web-layout>