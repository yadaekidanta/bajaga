<x-web-layout title="Produk Lelang" keyword="Bajaga Store">
    <div class="container-custom px-3 py-3 bg-white">
        <!-- filter -->
        <div class="d-flex gap-3">
            <span class="font-bold">Filter</span>
            <div class="rounded-lg border px-1 d-flex gap-2 align-items-center" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                <span class="status-filter">Semua</span>
                
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                    <defs>
                        <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                    </defs>
                    <g fill="none" fill-rule="evenodd">
                        <path d="M0 24h24V0H0z"/>
                        <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                    </g>
                </svg>

            </div>
        </div>
        <!-- /filter -->

        <!-- product recomendation -->
        <div class="d-flex flex-wrap gap-3 mt-2" id="product">
            @include('page.web.mobile.auction.data_list_auction')
        </div>
        <div class="ajax-load text-center mt-3" style="display:none">
            <p class="text-primary font-bold">Load More Post...</p>
        </div>
        <!-- /product recomendation -->
    </div>

    <!-- canvas filter -->
    <div class="offcanvas offcanvas-bottom h-90" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small">
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-sort-by="*">
                <span class="font-bold font-lg">Semua</span>
                <input type="radio" name="all" class="form-custom-radio" checked>
            </div>
            
            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-sort-by="price_hl">
                <span class="font-bold font-lg">Harga Tertinggi</span>
                <input type="radio" name="price_hl" class="form-custom-radio">
            </div>
            
            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-sort-by="price_lh">
                <span class="font-bold font-lg">Harga Terendah</span>
                <input type="radio" name="price_lh" class="form-custom-radio">
            </div>
            
        </div>
    </div>
    <!-- /canvas filter -->

    @section('custom_js')
    <script>
        function loadMoreData(page)
        {
            // alert()
            $.ajax({
                url: '?page=' + page,
                type:'get',
                beforeSend: function()
                {
                    $(".ajax-load").show();
                }
            })
            .done(function(data){
                if(data.html == ""){
                    $('.ajax-load').html("No more Items Found!");
                    return;
                }
                $('.ajax-load').hide();
                $(".scrolling-pagination").append(data.html);
            })
            // Call back function
            .fail(function(jqXHR,ajaxOptions,thrownError){
                alert("Server not responding.....");
            });
        
        }

        //function for Scroll Event
        var pages = 1;
        $(window).scroll(function(){
            // console.log($(window).scrollTop());
            if($(window).scrollTop() + $(window).height() + 80 >= $(document).height()){
                pages++;
                loadMoreData(pages);
            }
        })

        $('#product').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    return parseFloat($(itemElem).find("#item_price")[0].value)
                },
                price_hl: function( itemElem ) {
                    return parseFloat($(itemElem).find("#item_price")[0].value)
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $(document).ready(function () {
            $(".offcanvas-body").on("click", "div", function(){
                // alert()
                $('.offcanvas-body div input').prop("checked", false)
                $(this).children("input").prop("checked", true)
                // console.log($(this).children("input"));
                $(".status-filter").html($(this).children("span").text())
                var sortByValue = $(this).attr('data-sort-by');
                // console.log(sortByValue);
                $('#product').isotope({ sortBy: sortByValue });
                return false;
            });
        })

        // $(document).ready(function () {
        //     $(".offcanvas-body").on("click", "div", function(){
        //         var filterValue = $(this).attr('data-filter');
        //         $('#list-items').isotope({ transitionDuration: '0.65s',filter: filterValue });
        //         // console.log(filterValue);
        //         $('.offcanvas-body div input').prop("checked", false)
        //         $(this).children("input").prop("checked", true)
        //         $(".status-filter").html($(this).children("span").text())
        //     })
        // })
    </script>
    @endsection
</x-web-layout>