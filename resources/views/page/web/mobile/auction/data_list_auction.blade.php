@foreach ($products as $item)
                <a href="{{ route('web.auction.show', $item->id) }}" class="text-black mx-1">
                    <input type="hidden" id="item_price" value="{{$item->price}}">
                    <div class="section-card-vertical">
                        <div class="container-card shadow">
                            <div class="container-img">
                                <img src="{{$item->product_store->image}}" alt="">
                            </div>
                            <div class="p-1 description-product">
                                <div class="d-flex flex-column">
                                    <span class="font-sm text-capitalize line-normal">{!! Str::limit($item->product_store->name)!!}</span>
                                    <span class="font-medium font-bold">Rp {{number_format($item->price)}}</span>

                                    <div class="d-flex align-items-center gap-2">
                                        <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                        <span class="font-sm">{{$item->product_store->product_store->city->name}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach