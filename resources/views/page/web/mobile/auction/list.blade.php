<div class="d-flex flex-column gap-3 border rounded p-2" style="overflow-y: scroll; height:400px;">
    @foreach ($collection as $data)
        <div class="d-flex gap-3">
            <div class="w-3em h-3em overflow-hidden rounded-circle">
                <img class="w-100 h-100 object-fit-cover" src="{{ $data->users->avatar == null ? 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png' : asset('storage/'.$data->users->avatar) }}">
            </div>
            <div>
                <p class="mb-0 font-bold">{{ $data->users->name }}</p>
                <p class="mb-0">{{ number_format($data->bid) }}</p>
            </div>
        </div>
        <hr class="my-1">
    @endforeach
</div>