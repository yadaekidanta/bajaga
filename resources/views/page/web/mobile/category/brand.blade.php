<x-web-layout title="List Produk" keyword="Bajaga Store">
    <div class="container-custom px-3 py-3 bg-white">
        <!-- filter -->
        <div class="d-flex gap-3">
            <span class="font-bold">Filter</span>
            <div class="rounded-lg border px-1 d-flex gap-2 align-items-center" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                <span class="status-filter">Semua status</span>
                
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                    <defs>
                        <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                    </defs>
                    <g fill="none" fill-rule="evenodd">
                        <path d="M0 24h24V0H0z"/>
                        <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                    </g>
                </svg>

            </div>
        </div>
        <!-- /filter -->

        <!-- product recomendation -->
        <div class="d-flex flex-wrap gap-3 mt-2 scrolling-pagination justify-content-between" id="list_result">
        </div>
        <div class="ajax-load d-flex justify-content-center w-100 mt-5">
            <div class="spinner-grow text-secondary" role="status">
            </div>
        </div>
        <!-- /product recomendation -->
    </div>

    <!-- canvas filter -->
    <div class="offcanvas offcanvas-bottom h-90" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small">
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter="semua">
                <span class="font-bold font-lg">Semua</span>
                <input type="radio" name="all" class="form-custom-radio" checked>
            </div>
            
            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter="max">
                <span class="font-bold font-lg">Harga Tertinggi</span>
                <input type="radio" name="waiting_confirmation" class="form-custom-radio">
            </div>
            
            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter="min">
                <span class="font-bold font-lg">Harga Terandah</span>
                <input type="radio" name="on_progress" class="form-custom-radio">
            </div>
        </div>
    </div>
    <form id="content_filter" class="d-none">
        <input type="hidden" id="filter_input" name="filter">
    </form>
    <!-- /canvas filter -->
    @section('custom_js')
    <script>
        //function for Scroll Event
        localStorage.setItem("page_infinate", 1)
        load_list(1)

        $(document).ready(function(){
            let options = {
                root: null,
                rootMargin: '10px',
                threshold: 0.5
            }

            const observer = new IntersectionObserver(handleIntersect, options)
            observer.observe(document.querySelector(".ajax-load"))
        })
        
        function handleIntersect(entries){
            if (entries[0].isIntersecting) {
                // alert()
                localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                load_more(parseInt(localStorage.getItem("page_infinate")));
            }
        }

        $(document).ready(function () {
            $(".offcanvas-body").on("click", "div", function(){
                var filterValue = $(this).attr('data-filter');
                console.log(filterValue);
                if (filterValue == "semua") {
                    $("#filter_input").val("")
                    load_list(1)
                }else{
                    $("#filter_input").val(filterValue)
                    load_list(1)
                }
                // $('#list_result').isotope({ transitionDuration: '0.65s',filter: filterValue });
                // console.log(filterValue);
                $('.offcanvas-body div input').prop("checked", false)
                $(this).children("input").prop("checked", true)
                $(".status-filter").html($(this).children("span").text())
            })
        })
    </script>
    @endsection
</x-web-layout>