<x-web-layout title="Detail Pengiriman" keyword="Bajaga Store">
    <div class="container-custom bg-white pb-5">
        {{-- {{dd($response["delivery_status"]["status"])}} --}}
        {{-- @if($response["delivery_status"]) --}}
            <!-- hero info -->
            <div class="w-100 px-3 d-flex flex-column align-items-center justify-content-center">
                <div class="w-100 p-3">
                    @if($sale->st == "Tertunda")
                        <img src="{{asset('img/shipping/timeline-1.png')}}" alt="">
                    @elseif($sale->st == "Dipesan")
                        <img src="{{asset('img/shipping/timeline-2.png')}}" alt="">
                    @elseif($sale->st == "Dikirim" && $response["delivery_status"]["status"] == "ON PROCESS")
                        <img src="{{asset('img/shipping/timeline-3.png')}}" alt="">
                    @elseif($sale->st == "Diterima" || $response["delivery_status"]["status"] == "DELIVERED")
                        <img src="{{asset('img/shipping/timeline-4.png')}}" alt="">
                    @endif
                </div>

                @if ($response)
                    @if($response["delivery_status"]["status"] == "DELIVERED")
                    <span class="text-bajaga-primary font-bold">
                        Pesanan Diterima
                    </span>
                    @elseif($response["delivery_status"]["status"] == "ON PROCESS")
                    <span class="text-bajaga-primary font-bold">
                        Sedang Dalam Perjalanan
                    </span>
                    @endif

                    
                    @if($response["manifest"])
                    <hr>
                        <div class="px-3" id="timeline">
                            @for($i = 0; $i < count($response["manifest"]); $i++)
                            <div class="d-flex gap-3 mt-3">
                                <div class="bullet-tracker"></div>
                                <div style="width: 90%;margin-top: -2px;">
                                    <span class="font-bold">{{$response["manifest"][$i]["manifest_description"]}}</span>
                                    <span class="font-sm">( {{$response["manifest"][$i]["city_name"]}} )</span>
                                    <span class="d-block font-sm">
                                        {{$response["manifest"][$i]["manifest_date"]}} {{$response["manifest"][$i]["manifest_time"]}}
                                    </span>
                                </div>
                            </div>
                            @endfor
                            <div class="d-flex gap-3 mt-3">
                                <div class="bullet-tracker"></div>
                                <div style="width: 90%;margin-top: -2px;">
                                    <span class="font-bold">Bejaga - {{$sale->updated_at}}</span>
                                    <span class="d-block font-sm">
                                        Pembayaran sudah Diverifikasi.
                                        <br>
                                        Pembayaran telah diterima Bajaga dan pesanan Anda sudah diteruskan ke penjual.
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    @if($sale->st == "Tertunda")
                        <span class="text-bajaga-primary font-bold">
                            Masih Menunggu Pembayaran
                        </span>
                        <input type="hidden" value="{{$sale->code}}" id="code">
                        <span class=""></span>
                        <span onclick="checkout(this)" class="font-sm mt-1 text-primary text-underline">
                            <ins>Lanjutkan ke pembayaran</ins>
                        </span>
                    @elseif($sale->st == "Dipesan")
                        <span class="text-bajaga-primary font-bold">
                            Pesanan Sedang di Proses Toko
                        </span>
                        <input type="hidden" value="{{$sale->code}}" id="code">
                        <span class=""></span>
                    @endif
                @endif

            </div>
            <!-- hero info -->
        {{-- @else
            <!-- hero info -->
            <div class="w-100 px-3 pt-2 d-flex flex-column align-items-center justify-content-center">
                <div class="alert alert-danger d-flex align-items-center gap-2" role="alert">
                    <span>No resi, invalid, silahkan hubungi penjual untuk update no resi</span>
                    <a href="" class="btn btn-sm btn-outline-light">Chat</a>
                </div>
            </div>
            <!-- hero info -->
        @endif --}}
    </div>

    @section('custom_js')
        <script>
            function checkout(el)
            {
                let container = $(el);
                $(container).html(`<div class="spinner-border spinner-border-sm text-primary spinner" role="status"></div>`);
                var settings = {
                    "url": "http://127.0.0.1:8000/api/xendit/get_invoice",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify({
                        "id": $("#code").val()
                    }),
                };
        
                $.ajax(settings).done(function (response) {
                    // console.log(response);
                    // toggleLoading($(this), false)
                    location.href = response.invoice_url
                    // console.log(response);
                    $(container).html(`<ins>Lanjutkan ke pembayaran</ins>`);
                }).fail( (response) => {
                    $(container).html(`<ins>Lanjutkan ke pembayaran</ins>`);
                    Swal.fire({ text: response , icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                });
            }
        </script>
    @endsection

</x-web-layout>