<x-web-layout title="Kupon Saya" keyword="Bajaga Store">
    <div class="container-custom pb-3 bg-white">
        <!-- banner -->
        <div class="position-relative bg-warning w-100" style="height: 12em">
                {{-- <div class="h-6.5em position-absolute z-30 left-9em top-12px w-9em">
                    <div class="font-lg font-bold">
                        {{$discount->name}}
                    </div>
                    <div class="font-sm mt-1">
                        {{$discount->desc}}
                    </div>
                </div> --}}
                
                <div class="overflow-hidden h-100 w-100">
                    <img src="{{asset('storage/'.$discount->thumbnail)}}" class="w-100 h-100 object-fit-cover" alt="">
                </div>
            <!-- </div> -->   
        </div>
        <!-- banner -->

        <div class="mt-2 px-3">
            <div>
                <h3 class="m-0">{{Str::limit($discount->name, 30)}}</h3>
            </div>
            <div class="mt-1 d-flex justify-content-between align-items-center">
                <div class="d-flex gap-2 align-items-center">
                    <img src="{{asset('img/other/timer.png')}}" class="w-5 h-5" alt="image time">
                    <span class="font-sm">Berlaku hingga</span>
                </div>
                <span class="font-sm badge badge-primary">{{$discount->end_at}}</span>
            </div>
            @if($discount->type == "harga")
            <div class="mt-1 d-flex justify-content-between align-items-center">
                <div class="d-flex gap-2 align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#4C83F2" width="24" height="24"><defs><filter id="a" width="111.1%" height="161.5%" x="-5.6%" y="-30.8%"><feOffset in="SourceAlpha"/><feGaussianBlur stdDeviation="4"/><feColorMatrix result="C" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/><feMerge><feMergeNode in="C"/><feMergeNode in="SourceGraphic"/></feMerge></filter><path id="b" d="M10.68 10.32c0 .37-.27.6-.75.6H9V9.7h.92c.46 0 .75.2.75.6v.02zm4.17.38c.45 0 .8.37.8.88v.01c0 .53-.35.888-.8.888-.44 0-.8-.37-.8-.89v-.01c0-.5.35-.88.8-.88zm2.15.9v-.02c0-1.24-.8-2.02-1.73-2.02-.58 0-.92.3-1.18.6v-.52h-1.37v5.06h1.37v-1.63c.25.3.6.56 1.2.56.92 0 1.72-.77 1.72-2.04zm-6.36 1.95h1.6l-1.2-1.76c.63-.262 1.04-.78 1.04-1.552v-.01c0-.5-.15-.9-.45-1.18-.34-.34-.87-.55-1.64-.55H7.6v5.05H9V12h.6l1.02 1.53zM20 5.16a2 2 0 0 1 2 2v9.778a2.01 2.01 0 0 1-1.37 1.901c-2.37.78-4.92.86-7.33.2l-3.8-1.02a7.611 7.611 0 0 0-4.76.27A1.996 1.996 0 0 1 2 16.44v-9.7a1.99 1.99 0 0 1 1.25-1.849A11.95 11.95 0 0 1 7.74 4a12.11 12.11 0 0 1 3.1.41l3.75 1c1.57.4 3.23.36 4.78-.15.2-.07.42-.1.63-.1z"/></defs><g fill-rule="evenodd" filter="url(#a)"><mask id="c" fill="#fff"><use xlink:href="#b"/></mask><g fill-rule="nonzero"><use fill="#a1a7ab" xlink:href="#b"/><g fill="#03ac0e" mask="url(#c)"><path d="M1 1h22v22H1z"/></g></g></g></svg>
                        <span class="font-sm text-dark">Diskon</span>
                    </div>
                    <span class="font-sm text-dark">{{number_format($discount->harga)}}</span>
                </div>
            @else
                <div class="mt-1 d-flex justify-content-between align-items-center">
                    <div class="d-flex gap-2 align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#4C83F2" width="24" height="24"><defs><filter id="a" width="111.1%" height="161.5%" x="-5.6%" y="-30.8%"><feOffset in="SourceAlpha"/><feGaussianBlur stdDeviation="4"/><feColorMatrix result="C" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/><feMerge><feMergeNode in="C"/><feMergeNode in="SourceGraphic"/></feMerge></filter><path id="b" d="M10.68 10.32c0 .37-.27.6-.75.6H9V9.7h.92c.46 0 .75.2.75.6v.02zm4.17.38c.45 0 .8.37.8.88v.01c0 .53-.35.888-.8.888-.44 0-.8-.37-.8-.89v-.01c0-.5.35-.88.8-.88zm2.15.9v-.02c0-1.24-.8-2.02-1.73-2.02-.58 0-.92.3-1.18.6v-.52h-1.37v5.06h1.37v-1.63c.25.3.6.56 1.2.56.92 0 1.72-.77 1.72-2.04zm-6.36 1.95h1.6l-1.2-1.76c.63-.262 1.04-.78 1.04-1.552v-.01c0-.5-.15-.9-.45-1.18-.34-.34-.87-.55-1.64-.55H7.6v5.05H9V12h.6l1.02 1.53zM20 5.16a2 2 0 0 1 2 2v9.778a2.01 2.01 0 0 1-1.37 1.901c-2.37.78-4.92.86-7.33.2l-3.8-1.02a7.611 7.611 0 0 0-4.76.27A1.996 1.996 0 0 1 2 16.44v-9.7a1.99 1.99 0 0 1 1.25-1.849A11.95 11.95 0 0 1 7.74 4a12.11 12.11 0 0 1 3.1.41l3.75 1c1.57.4 3.23.36 4.78-.15.2-.07.42-.1.63-.1z"/></defs><g fill-rule="evenodd" filter="url(#a)"><mask id="c" fill="#fff"><use xlink:href="#b"/></mask><g fill-rule="nonzero"><use fill="#a1a7ab" xlink:href="#b"/><g fill="#03ac0e" mask="url(#c)"><path d="M1 1h22v22H1z"/></g></g></g></svg>
                        <span class="font-sm text-dark">Diskon</span>
                    </div>
                    <span class="font-sm text-dark">{{$discount->persentase}}%</span>
                </div>
            @endif
        </div>

        <hr>

        <div class="px-3">
            <div class="font-lg font-bold">Deskripsi</div>
            <div class="mt-1">{{$discount->desc}}</div>
        </div>
    </div>

    @section('custom_js')
    <script>
        $(".goBacks").click(function(){
            location.href = '/discount'
        })
    </script>
    @endsection
</x-web-layout>