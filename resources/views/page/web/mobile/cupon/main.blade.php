<x-web-layout title="Kupon Saya" keyword="Bajaga Store">
    <div class="container-custom px-3 py-3 bg-white">
        <!-- filter -->
        {{-- <div class="d-flex gap-3">
            <span class="font-bold">Filter</span>
            <div class="rounded-lg border px-1 d-flex gap-2 align-items-center" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                <span class="status-filter">Semua status</span>
                
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                    <defs>
                        <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                    </defs>
                    <g fill="none" fill-rule="evenodd">
                        <path d="M0 24h24V0H0z"/>
                        <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                    </g>
                </svg>

            </div>
        </div> --}}
        <!-- /filter -->

        <!-- product recomendation -->
        <div class="d-flex flex-wrap gap-3 mt-2 w-100">
            @foreach($collections as $collection)
                <a href="{{route('web.discount.detail', $collection->id)}}" class="card text-dark shadow-cupon w-100">
                    <div class="card-body p-0 position-relative bg-warning">

                        
                        <!-- <div class="position-relative w-100 h-100"> -->
                            <div class="h-100 h-10 position-absolute z-20 -left-12px d-flex align-items-center">
                                <div class="h-5 w-5 bg-white rounded-circle"></div>
                            </div>
        
                            <div class="h-100 h-10 position-absolute z-20 -right-12px d-flex align-items-center">
                                <div class="h-5 w-5 bg-white rounded-circle"></div>
                            </div>
                            {{-- <div class="h-6.5em position-absolute z-30 left-9em top-12px w-9em">
                                <div class="font-lg font-bold">
                                    {{Str::limit($collection->name, 20)}}
                                </div>
                                <div class="font-sm mt-1">
                                    {{Str::limit($collection->desc, 25)}}
                                </div>
                            </div> --}}
                            
                            <div class="overflow-hidden" style="height: 7.7em; width: 100%">
                                <img src="{{asset('storage/'.$collection->thumbnail)}}" class="object-fit-cover w-100 h-100" alt="">
                            </div>
                        <!-- </div> -->
                            
                    </div>
                    <div class="card-footer text-muted">
                        <div>
                            <h3 class="m-0">{{Str::limit($collection->name, 20)}}</h3>
                            <span>{{Str::limit($collection->desc, 45)}}</span>
                        </div>
                        <div class="d-flex justify-content-between"> 
                            <div class="d-flex align-items-center gap-2">
                                <!-- <div class="w-5 h-5"> -->
                                    <img src="{{asset('img/other/timer.png')}}" class="w-5 h-5" alt="image time">
                                <!-- </div> -->
                                <div class="d-flex flex-column">
                                    <span>Berlaku hingga</span>
                                    <span class="-mt-1">{{$collection->end_at}}</span>
                                </div>
                            </div>
                            <div class="d-flex gap-2 ">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#427BFF" width="24" height="24"><defs><filter id="a" width="111.1%" height="161.5%" x="-5.6%" y="-30.8%"><feOffset in="SourceAlpha"/><feGaussianBlur stdDeviation="4"/><feColorMatrix result="C" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/><feMerge><feMergeNode in="C"/><feMergeNode in="SourceGraphic"/></feMerge></filter><path id="b" d="M10.68 10.32c0 .37-.27.6-.75.6H9V9.7h.92c.46 0 .75.2.75.6v.02zm4.17.38c.45 0 .8.37.8.88v.01c0 .53-.35.888-.8.888-.44 0-.8-.37-.8-.89v-.01c0-.5.35-.88.8-.88zm2.15.9v-.02c0-1.24-.8-2.02-1.73-2.02-.58 0-.92.3-1.18.6v-.52h-1.37v5.06h1.37v-1.63c.25.3.6.56 1.2.56.92 0 1.72-.77 1.72-2.04zm-6.36 1.95h1.6l-1.2-1.76c.63-.262 1.04-.78 1.04-1.552v-.01c0-.5-.15-.9-.45-1.18-.34-.34-.87-.55-1.64-.55H7.6v5.05H9V12h.6l1.02 1.53zM20 5.16a2 2 0 0 1 2 2v9.778a2.01 2.01 0 0 1-1.37 1.901c-2.37.78-4.92.86-7.33.2l-3.8-1.02a7.611 7.611 0 0 0-4.76.27A1.996 1.996 0 0 1 2 16.44v-9.7a1.99 1.99 0 0 1 1.25-1.849A11.95 11.95 0 0 1 7.74 4a12.11 12.11 0 0 1 3.1.41l3.75 1c1.57.4 3.23.36 4.78-.15.2-.07.42-.1.63-.1z"/></defs><g fill-rule="evenodd" filter="url(#a)"><mask id="c" fill="#fff"><use xlink:href="#b"/></mask><g fill-rule="nonzero"><use fill="#a1a7ab" xlink:href="#b"/><g fill="#03ac0e" mask="url(#c)"><path d="M1 1h22v22H1z"/></g></g></g></svg>
                                <div class="d-flex flex-column">
                                    @if($collection->type == "harga")
                                        <span>Diskon</span>
                                        <span class="-mt-1 font-bold">Rp{{number_format($collection->harga)}}</span>
                                    @else
                                        <span>Diskon</span>
                                        <span class="-mt-1 font-bold">{{$collection->persentase}}%</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </a>
            @endforeach
        </div>
        <!-- /product recomendation -->
    </div>

    <!-- canvas filter -->
    <div class="offcanvas offcanvas-bottom h-90" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small">
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter="*">
                <span class="font-bold font-lg">Semua Status</span>
                <input type="radio" name="all" class="form-custom-radio" checked>
            </div>
            
            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter=".waiting_confirmation">
                <span class="font-bold font-lg">Menunggu Konfirmasi</span>
                <input type="radio" name="waiting_confirmation" class="form-custom-radio">
            </div>
            
            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter=".on_progress">
                <span class="font-bold font-lg">Diproses</span>
                <input type="radio" name="on_progress" class="form-custom-radio">
            </div>

            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter=".sent">
                <span class="font-bold font-lg">Dikirim</span>
                <input type="radio" name="sent" class="form-custom-radio">
            </div>

            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter=".arrive">
                <span class="font-bold font-lg">Tiba di Tujuan</span>
                <input type="radio" name="arrive" class="form-custom-radio">
            </div>

            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter=".success">
                <span class="font-bold font-lg">Berhasil</span>
                <input type="radio" name="success" class="form-custom-radio">
            </div>

            <hr class="m-0">
            
            <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter=".cancelled">
                <span class="font-bold font-lg">Tidak Berhasil</span>
                <input type="radio" name="cancelled" class="form-custom-radio">
            </div>
            
        </div>
    </div>
    <!-- /canvas filter -->


    @section('custom_js')
    <script>
    </script>
    @endsection
</x-web-layout>