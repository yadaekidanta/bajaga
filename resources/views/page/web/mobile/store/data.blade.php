@foreach($collections as $collection)
    <div class="w-47%">
        <div class="section-card-vertical h-100">
            <div class="container-card shadow">
                <a href="{{route('web.product.show', $collection->slug)}}">
                    <div class="container-img">
                        <img src="{{asset($collection->image)}}" alt="">
                    </div>
                </a>
                <div class="p-1 description-product">
                    <div class="d-flex flex-column">
                        <a href="{{route('web.product.show', $collection->slug)}}" class="text-dark">
                            <span class="font-sm text-capitalize line-normal">{{Str::limit($collection->name, 18)}}</span>
                            <span class="font-medium font-bold d-block">Rp {{number_format($collection->price)}}</span>
                        </a>
                        <!-- <div class="d-flex align-items-center">
                            <i class="icon-star3"></i>
                            <span class="font-sm">5.0 | Terjual 1.5 rb</span>
                        </div> -->
                        <a href="{{route('web.store', $store->id)}}" class="d-flex mt-2 gap-1 text-dark">
                            <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                            <span style="margin-top: -5px !important;">
                                <span class="text-capitalize line-normal">{{$collection->product_store->name}}</span>
                                <span class="font-sm d-block">{{Str::limit($collection->product_store->city->name, 14)}}</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach