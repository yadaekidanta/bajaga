<x-web-layout title="Pengiriman" keyword="Bajaga Store">
    <div class="container-custom bg-white py-3">
        <div class="d-flex gap-3 px-3">
            <div class="w-20 h-20 border">
                <img class="object-fit-cover w-100 h-100" src="{{asset( $store->photo != null ? 'storage/'.$store->photo : 'img/store/default.png')}}" alt="">
            </div>
            <div class="">
                <h3 class="font-bold m-0">{{$store->name}}</h3>
                <div class="d-flex align-items-center mt-2 gap-2">
                    <span class="w-12px h-12px bg-{{Cache::has('is_client_online'. $store->user->id) ? 'success' : 'danger'}} border border-light rounded-circle"></span>
                    {{-- <img class="w-9px " src="{{asset('img/other/indicator.png')}}" alt=""> --}}
                    <span class="text-{{Cache::has('is_client_online'. $store->user->id) ? 'success' : 'danger'}} fs-14px">{{Cache::has('is_client_online'. $store->user->id) ? 'Online' : 'Offline'}}</span>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-evenly mt-2 px-3">
            <div class="d-flex flex-column align-items-center">
                <div class="d-flex gap-1 align-items-center">
                    <i class="icon-star3 text-warning"></i>
                    <span>{{$rating}}</span>
                </div>
                <div class="font-sm">Rating & Ulasan</div>
            </div>
            <div class="sparator"></div>
            <div class="">
                {{$store->start_at ? $store->start_at->format('H:i') : '..:..'}} -  {{$store->end_at ? $store->end_at->format('H:i') : '..:..'}}</span>
                <div class="font-sm">Jam Operasi Toko</div>
            </div>
        </div>
        
        <div class="d-flex align-items-center mt-2 gap-3 px-3">
            @php
                $user = Auth::user();
            @endphp
            @if ($user)
                @if ($store->user->id == $user->id)
                    <a href="{{route('web.product.index')}}" class="btn btn-sm w-100 btn-primary">Lihat Toko</a>
                @else
                    <a href="{{route('web.chat.detail', $store->user->id)}}" class="btn btn-primary btn-sm w-full">Chat</a>
                    <button type="button" id="follow_store" onclick="follow('{{route('web.store.favorite.follow').'?store_id='.$store->id}}', this)" class="btn btn-sm btn-primary {{ $state_follow ?'d-none': ''}} text-sm px-5">Follow</button>
                    <button type="button" id="unfollow_store" onclick="unfollow('{{route('web.store.favorite.unfollow').'?store_id='.$store->id}}', this)" class="btn btn-sm btn-outline-primary {{ $state_follow ? '': 'd-none'}} text-sm px-5">Unfollow</button>
                @endif
            @endif
        </div>

        <div class="mt-2">
            <ul class="nav nav-tabs d-flex align-items-center justify-content-around" id="myTab" role="tablist">
                <li class="nav-item px-2" role="presentation">
                    <button class="nav-link active font-bold padding-tab-navs-icon" id="btn-store" data-bs-toggle="tab" data-bs-target="#store-nav" type="button" role="tab" aria-controls="store-nav" aria-selected="true">
                        <!-- fill -->
                        <svg class="store-fill" xmlns="http://www.w3.org/2000/svg" width="20" height="17.844" viewBox="0 0 20 17.844"><path d="M2.1,10.05A4.072,4.072,0,0,1,2,9.6L3.8,4.3A1.6,1.6,0,0,1,5.7,3.2H18.3a1.955,1.955,0,0,1,1.9,1.3c.6,1.8,1.7,4.9,1.8,5.1a4.331,4.331,0,0,0-.1.5,1.69,1.69,0,0,1-.2.5,3.208,3.208,0,0,1-.3.5l-.4.4a2.716,2.716,0,0,1-1.7.6,2.628,2.628,0,0,1-2.7-2.7,1.421,1.421,0,0,0-.2-.6.56.56,0,0,1-.082-.109c-.033-.053-.056-.091-.118-.091l-.1-.1a.1.1,0,0,0-.1-.1.367.367,0,0,0-.3-.1.355.355,0,0,0-.15.05.355.355,0,0,1-.15.05.1.1,0,0,1-.1.1.349.349,0,0,0-.2.1l-.1.1a.1.1,0,0,0-.1.1.764.764,0,0,0-.2.5A2.628,2.628,0,0,1,12,12,2.476,2.476,0,0,1,9.3,9.3a.754.754,0,0,0-.2-.453C9.07,8.8,9.037,8.755,9,8.7l-.1-.1-.1-.1a.13.13,0,0,1-.1-.05.13.13,0,0,0-.1-.05.1.1,0,0,0-.1-.1.367.367,0,0,0-.3-.1.355.355,0,0,0-.15.05.355.355,0,0,1-.15.05l-.1.1-.1.1a.559.559,0,0,1-.109.082c-.053.033-.091.056-.091.118a1.421,1.421,0,0,0-.2.6A2.628,2.628,0,0,1,4.6,12a2.716,2.716,0,0,1-1.7-.6L2.5,11a3.208,3.208,0,0,1-.3-.5A4.072,4.072,0,0,0,2.1,10.05ZM15.6,12.3A4.983,4.983,0,0,0,19.3,14a4.671,4.671,0,0,0,1.7-.3V19a1.89,1.89,0,0,1-2,2H15V19a1.89,1.89,0,0,0-2-2H11a1.89,1.89,0,0,0-2,2v2H5a1.89,1.89,0,0,1-2-2V13.7a5.079,5.079,0,0,0,1.7.3,4.611,4.611,0,0,0,3.6-1.7A4.385,4.385,0,0,0,12,14,4.611,4.611,0,0,0,15.6,12.3Z" transform="translate(-2 -3.156)" fill="#15314f" fill-rule="evenodd"/></svg>
                        <!-- fill -->

                        <!-- outline -->
                        <svg class="store-outline d-none" width="21" height="19" viewBox="0 0 21 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.9999 7C20.9987 6.89149 20.9819 6.78371 20.9499 6.68L19.1499 1.46C19.0348 1.04894 18.7912 0.685481 18.4548 0.422786C18.1183 0.16009 17.7066 0.0119395 17.2799 1.89363e-10H4.67991C4.26402 -5.82378e-06 3.85926 0.134328 3.5259 0.382995C3.19254 0.631662 2.94843 0.981357 2.82991 1.38L0.99991 6.68C0.98473 6.78613 0.98473 6.89387 0.99991 7C0.998874 7.97573 1.35452 8.9182 1.99991 9.65V16C1.9901 16.2653 2.03512 16.5297 2.13216 16.7768C2.2292 17.0239 2.37617 17.2483 2.56388 17.436C2.75159 17.6237 2.976 17.7707 3.22309 17.8678C3.47018 17.9648 3.73463 18.0098 3.99991 18H17.9999C18.2652 18.0098 18.5296 17.9648 18.7767 17.8678C19.0238 17.7707 19.2482 17.6237 19.4359 17.436C19.6237 17.2483 19.7706 17.0239 19.8677 16.7768C19.9647 16.5297 20.0097 16.2653 19.9999 16V9.65C20.6453 8.9182 21.0009 7.97573 20.9999 7ZM4.67991 2H17.2799H17.2299L18.9999 7.15C18.98 7.68043 18.7502 8.18124 18.3611 8.54225C17.972 8.90325 17.4553 9.09489 16.9249 9.075C16.3945 9.05511 15.8937 8.82532 15.5327 8.43618C15.1717 8.04704 14.98 7.53043 14.9999 7C14.9999 6.73478 14.8946 6.48043 14.707 6.29289C14.5195 6.10536 14.2651 6 13.9999 6C13.7347 6 13.4803 6.10536 13.2928 6.29289C13.1053 6.48043 12.9999 6.73478 12.9999 7C12.9999 7.53043 12.7892 8.03914 12.4141 8.41421C12.0391 8.78929 11.5303 9 10.9999 9C10.7291 9.03417 10.4541 9.00596 10.1959 8.91753C9.93768 8.8291 9.7031 8.6828 9.51011 8.4898C9.31711 8.29681 9.17081 8.06223 9.08238 7.80402C8.99395 7.54581 8.96574 7.27079 8.99991 7C8.99991 6.73478 8.89455 6.48043 8.70702 6.29289C8.51948 6.10536 8.26513 6 7.99991 6C7.73469 6 7.48034 6.10536 7.2928 6.29289C7.10527 6.48043 6.99991 6.73478 6.99991 7C6.99991 7.53043 6.7892 8.03914 6.41412 8.41421C6.03905 8.78929 5.53034 9 4.99991 9C4.49447 9.00142 4.00724 8.81142 3.63619 8.46821C3.26515 8.12499 3.03782 7.65402 2.99991 7.15L4.67991 2ZM17.9999 16H13.9999V14C14.0097 13.7347 13.9647 13.4703 13.8677 13.2232C13.7706 12.9761 13.6237 12.7517 13.4359 12.564C13.2482 12.3763 13.0238 12.2293 12.7767 12.1323C12.5296 12.0352 12.2652 11.9902 11.9999 12H9.99991C9.73463 11.9902 9.47018 12.0352 9.22309 12.1323C8.976 12.2293 8.75159 12.3763 8.56388 12.564C8.37617 12.7517 8.2292 12.9761 8.13216 13.2232C8.03512 13.4703 7.9901 13.7347 7.99991 14V16H3.99991V10.86C4.32618 10.9478 4.66207 10.9948 4.99991 11C5.55269 11.0036 6.10023 10.8927 6.60797 10.6741C7.11572 10.4555 7.57264 10.134 7.94991 9.73C8.33606 10.1499 8.80876 10.4809 9.33536 10.7001C9.86196 10.9194 10.4299 11.0217 10.9999 11C11.5656 11.003 12.1255 10.8861 12.6426 10.6568C13.1597 10.4275 13.6223 10.0912 13.9999 9.67C14.3775 10.0912 14.8401 10.4275 15.3572 10.6568C15.8744 10.8861 16.4342 11.003 16.9999 11C17.3376 10.9925 17.6732 10.9455 17.9999 10.86V16Z" fill="#525867"/></svg>
                        <!-- outline -->

                    </button>
                </li>
                <li class="nav-item px-2" role="presentation">
                    <button class="nav-link font-bold padding-tab-navs-icon" id="btn-etalase" data-bs-toggle="tab" data-bs-target="#etalase-nav" type="button" role="tab" aria-controls="etalase-nav" aria-selected="false">
                        <!-- etalase fill -->
                        <svg class="etalase-fill d-none" xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20"><path d="M16.258,0h-14a2.006,2.006,0,0,0-2,2V18a2.006,2.006,0,0,0,2,2h14a2.006,2.006,0,0,0,2-2V2A2.006,2.006,0,0,0,16.258,0Zm-5,17h-4a1,1,0,0,1,0-2h4a.945.945,0,0,1,1,1A1,1,0,0,1,11.258,17Zm0-6h-4a.945.945,0,0,1-1-1,.945.945,0,0,1,1-1h4a.945.945,0,0,1,1,1A1,1,0,0,1,11.258,11Zm0-6h-4a.945.945,0,0,1-1-1,.945.945,0,0,1,1-1h4a.945.945,0,0,1,1,1A1,1,0,0,1,11.258,5Z" transform="translate(-0.258)" fill="#15314f"/></svg>
                        <!-- etalase fill -->
                        
                        <!-- etalase outline -->
                        <svg class="etalase-outline" width="18" height="20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2 0h14c1.1 0 2 .9 2 2v16c0 1.1-.9 2-2 2H2c-1.1 0-2-.9-2-2V2C0 .9.9 0 2 0zm0 18h14v-4H2v4zm0-6h14V8H2v4zM2 2v4h14V2H2zm5 1h4c.5 0 1 .4 1 1s-.4 1-1 1H7c-.6 0-1-.4-1-1s.4-1 1-1zm0 8h4c.6 0 1-.4 1-1s-.4-1-1-1H7c-.6 0-1 .4-1 1s.4 1 1 1zm4 6H7c-.6 0-1-.4-1-1s.4-1 1-1h4c.6 0 1 .4 1 1s-.4 1-1 1z" fill="#525867"/></svg>
                        <!-- etalase outline -->
                    </button>
                </li>
            </ul>
            <div class="tab-content w-100" id="myTabContent">
                <div class="tab-pane fade show active mt-3" id="store-nav" role="tabpanel" aria-labelledby="store-nav-tab">
                    <!-- terbaru -->
                    <div class="mt-3">
                        <div class="d-flex px-3 justify-content-between align-items-center">
                            <h2 class="font-lg m-0">Terbaru</h2>
                        </div>
    
                        <div class="section-slide mt-2 pe-3">
                            @foreach($collections as $collection)
                            <div class="container-card shadow max-w-10">
                                <a href="{{route('web.product.show', $collection->slug)}}">
                                    <div class="container-img">
                                        <img src="{{asset($collection->image)}}" alt="">
                                    </div>
                                </a>
                                <div class="p-1 description-product">
                                    <div class="d-flex flex-column">
                                        <a href="{{route('web.product.show', $collection->slug)}}" href="" class="d-flex flex-column text-dark">
                                            <span class="font-sm text-capitalize line-normal">{{Str::limit($collection->name, 18)}}</span>
                                            <span class="font-medium font-bold">Rp {{number_format($collection->price)}}</span>
                                            <span class="font-smaller rounded badge-card-product bg-success text-light">New</span>
                                        </a>
                                        <!-- <div class="d-flex align-items-center">
                                            <i class="icon-star3"></i>
                                            <span class="font-sm">5.0 | Terjual 1.5 rb</span>
                                        </div> -->
                                        <a href="{{route('web.store', $store->id)}}" class="d-flex mt-2 gap-1 text-dark">
                                            <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                            <span style="margin-top: -5px !important;">
                                                <span class="text-capitalize line-normal">{{$collection->product_store->name}}</span>
                                                <span class="font-sm">{{Str::limit($collection->product_store->city->name, 14)}}</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /terbaru -->

                    <!-- terlaris -->
                    <div class="">
                        <div class="d-flex px-3 justify-content-between align-items-center">
                            <h2 class="font-lg m-0">Terlaris</h2>
                        </div>
    
                        <div class="section-slide mt-2 pe-3">
                            @foreach($collections as $collection)
                            <div class="container-card shadow max-w-10">
                                <a href="{{route('web.product.show', $collection->slug)}}">
                                    <div class="container-img">
                                        <img src="{{asset($collection->image)}}" alt="">
                                    </div>
                                </a>
                                <div class="p-1 description-product">
                                    <div class="d-flex flex-column">
                                        <a href="{{route('web.product.show', $collection->slug)}}" class="text-dark">
                                            <span class="font-sm text-capitalize line-normal">{{Str::limit($collection->name, 18)}}</span>
                                            <span class="font-medium font-bold">Rp {{number_format($collection->price)}}</span>
                                            <span class="font-smaller rounded badge-card-product bg-danger text-light px-2">Hot</span>
                                        </a>
                                        <!-- <div class="d-flex align-items-center">
                                            <i class="icon-star3"></i>
                                            <span class="font-sm">5.0 | Terjual 1.5 rb</span>
                                        </div> -->
                                        <a href="{{route('web.store', $store->id)}}" class="d-flex mt-2 gap-1 text-dark">
                                            <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                            <span style="margin-top: -5px !important;">
                                                <span class="text-capitalize line-normal">{{$collection->product_store->name}}</span>
                                                <span class="font-sm">{{Str::limit($collection->product_store->city->name, 14)}}</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- terlaris -->
                    
                    <!-- semua produk -->
                    <div class="px-3">
                        <div class="w-full my-1 d-flex justify-content-start">
                            <h2 class="font-lg m-0">Semua Produk</h2>
                        </div>

                        <!-- filter -->
                        <div class="d-flex gap-3 my-3">
                            <span class="font-bold">Filter</span>
                            <div class="rounded-lg border px-1 d-flex gap-2 align-items-center" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                                <span class="status-filter">Semua status</span>
                                
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                        <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                                    </defs>
                                    <g fill="none" fill-rule="evenodd">
                                        <path d="M0 24h24V0H0z"/>
                                        <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                                    </g>
                                </svg>

                            </div>
                        </div>
                        <!-- /filter -->

                        <!-- list produk -->
                        <div id="list_result" class="d-flex flex-wrap justify-content-between gap-3">
                        </div>
                        <div class="ajax-load text-center mt-3" style="display:none">
                            <p class="text-primary font-bold">Load More Product...</p>
                        </div>
                        <!-- list produk -->
                    </div>
                    <!-- semua produk -->
                </div>

                <div class="tab-pane fade show mt-3 px-3" id="etalase-nav" role="tabpanel" aria-labelledby="etalase-nav-tab">
                    <h3 class="m-0">Semua Etalase</h3>

                    <div>
                        @foreach($etalase as $et)
                            <a href="{{route('web.etalase', $et->id)}}?store_id={{$store->id}}" class="d-flex align-items-center text-dark gap-3 mt-2">
                                <div class="w-10 h-10 border">
                                    <img src="{{asset('img/product/'.$et->thumbnail)}}" alt="image etalase">
                                </div>
                                <span class="font-bold">{{$et->name}}</span>
        
                            </a>
                            <hr class="m-0 mt-2">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- sampe mau bikin panel tab di store profile -->

    </div>

    <!-- canvas filter -->
    <div class="offcanvas offcanvas-bottom h-90" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small position-relative pb-5">
            <form id="content_filter">
                <!-- filter sort -->
                <!-- <div>
                    <h3 class="font-bold m-0">Urutkan</h3>
                    <div class="d-flex flex-wrap align-item-center gap-2 mt-1 container-sort-filter">
                        <span data-sort-by="new_item" class="px-2 py-1 d-flex align-item-center justify-center border border-primary text-primary rounded-pill">
                            Terbaru
                        </span>
                        <span data-sort-by="name" class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                            Nama
                        </span>
                        <span data-sort-by="price_hl" class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                            Harga Tertinggi
                        </span>
                        <span data-sort-by="price_lh" class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                            Harga Terendah
                        </span>
                    </div>
                </div> -->
                <!-- /filter sort -->

                <!-- filter Categories -->
                <div class="mt-3">
                    <h3 class="font-bold m-0">Kategori</h3>
                    <div class="d-flex flex-wrap align-item-center gap-2 mt-1 container-categories">
                        @foreach($categories as $category)
                            <span data-filter-by="{{$category->id}}" class="px-2 py-1 d-flex align-item-center justify-content-center border  rounded-pill">
                                {{$category->title}}
                            </span>
                        @endforeach
                    </div>
                </div>
                <!-- /filter Categories -->
                <input type="text" name="keywords" id="keywords" class="d-none">
            </form>
        </div>
    </div>
    <!-- /canvas filter -->

    @section('custom_js')
    <script>
        load_list(1)

        // $(".container-categories").on('click', function (event) {
        //     $target = $(event.target);
        //     $('.container-categories > span').removeClass('border-primary text-primary')
        //     $target.addClass('border-primary text-primary');
        // });

        // $(".container-sort-filter").on('click', function (event) {
        //     $target = $(event.target);
        //     $('.container-sort-filter > span').removeClass('border-primary text-primary')
        //     $target.addClass('border-primary text-primary');
        // });
        
        // $('.minimum-filter').on('keyup', function(){
        //     let rupiah = formatRupiah($(this).val())
        //     $(this).val(rupiah)
        // })

        // $('.maximum-filter').on('keyup', function(){
        //     let rupiah = formatRupiah($(this).val())
        //     $(this).val(rupiah)
        // })

        $(".container-categories").on('click', function (event) {
            $target = $(event.target);
            $('.container-categories > span').removeClass('border-primary text-primary')
            $target.addClass('border-primary text-primary');
            [].slice.call($(this).children()).forEach(element => {
                if ($(element).hasClass('border-primary')) {
                    let dataFilter = $(element).attr('data-filter-by')
                    $("#keywords").val(dataFilter)
                    load_list(1)
                }
            });
            return false;
        });

        //function for Scroll Event
        var pages = 1;
        $(window).scroll(function(){
            // console.log($(window).scrollTop());
            if($(window).scrollTop() + $(window).height() >= $(document).height()){
                pages++;
                load_more(pages);
            }
        });

        // follow and unfollow store
        function follow(url, el){
                // alert(param)
                $(el).prop("disabled", true)
                $.ajax({
                    type: 'GET',
                    url,
                    success: function(response){
                        $(el).prop("disabled", false)
                        // console.log(response);
                        $("#unfollow_store").removeClass("d-none")
                        $("#follow_store").addClass("d-none")
                        // load_list(1)
                    },
                    error: (response) => {
                        $(el).prop("disabled", false)
                        // load_list(1)
                    }
                })
            }
            function unfollow(url, el){
                $(el).prop("disabled", true)
                $.ajax({
                    type: 'DELETE',
                    url,
                    success: function(response){
                        $(el).prop("disabled", false)
                        $("#follow_store").removeClass("d-none")
                        $("#unfollow_store").addClass("d-none")
                    },
                    error: (response) => {
                        $(el).prop("disabled", false)
                        // load_list(1)
                    }
                })
            }
            // follow and unfollow store
    </script>
    @endsection
</x-web-layout>