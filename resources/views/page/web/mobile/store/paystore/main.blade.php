<x-web-layout title="Pengiriman" keyword="Bajaga Store">
    <div class="container-custom bg-white pb-5">
        <!-- hero -->
        <div class="h-7rem">
            <div class="pt-2 pb-2 pt-5 bg-bajaga-primary d-flex align-items-center justify-content-between w-100 px-3">
                <h3 class="font-bold font-lg text-white" id="list_result">Rp {{number_format($balance)}}</h3>
                <div class="d-flex justify-content-around align-items-center flex-column" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                    
                    <svg clip-rule="evenodd" fill="white" class="icon-size-3" fill-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m2.009 11.998c0-5.518 4.48-9.998 9.998-9.998s9.998 4.48 9.998 9.998c0 5.517-4.48 9.997-9.998 9.997s-9.998-4.48-9.998-9.997zm1.5 0c0 4.69 3.808 8.497 8.498 8.497s8.498-3.807 8.498-8.497-3.808-8.498-8.498-8.498-8.498 3.808-8.498 8.498zm4.716 1.528s1.505 1.501 3.259 3.254c.146.147.338.22.53.22s.384-.073.53-.22c1.754-1.752 3.258-3.254 3.258-3.254.145-.145.217-.335.217-.526 0-.192-.074-.384-.221-.53-.292-.293-.766-.295-1.057-.004l-1.977 1.977v-6.693c0-.414-.336-.75-.75-.75s-.75.336-.75.75v6.693l-1.979-1.978c-.289-.289-.761-.287-1.054.006-.147.147-.221.339-.222.53 0 .191.071.38.216.525z" fill-rule="nonzero"/></svg>

                    <span class="font-bold text-white font-xs m-0">Withdraw</span>
                </div>
            </div>

        </div>
        <!-- /hero -->

        <!-- table transaction wd and dp -->
        <div class="pt-2 bg-white pt-2">
            <div class="left-0 bg-white w-100">
                <form id="content_filter" class="d-none">
                    <input type="text" name="keyword" value="data_wd" id="input_filter" class="d-none">
                </form>
                <ul class="nav nav-tabs d-flex align-items-center justify-content-around" id="myTab" role="tablist">
                    <li class="nav-item px-2" role="presentation">
                        <button class="nav-link active font-bold padding-tab-navs-icon" id="btn-tp" data-bs-toggle="tab" data-bs-target="#etalase-nav" type="button" role="tab" aria-controls="etalase-nav" aria-selected="false">
                            <span class="m-0 text-bajaga-primary">TopUp</span>
                        </button>
                    </li>
                    <li class="nav-item px-2" role="presentation">
                        <button class="nav-link font-bold padding-tab-navs-icon" id="btn-wd" data-bs-toggle="tab" data-bs-target="#store-nav" type="button" role="tab" aria-controls="store-nav" aria-selected="true">
                            <span class="m-0">Withdraw</span>
                        </button>
                    </li>
                </ul>
            </div>
            <div class="tab-content w-100 px-3 pt-2" id="myTabContent">
                <div class="tab-pane fade show" id="store-nav" role="tabpanel" aria-labelledby="store-nav-tab">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col" class="text-center">Amount</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Date</th>
                            </tr>
                        </thead>
                        <tbody id="list_result_wd">
                            @php
                                $i = 1
                            @endphp
                            @foreach($withdraw as $wd)
                                <tr>
                                    <th scope="row">{{$i++}}</th>
                                    <td>Rp{{number_format($wd->amount)}}</td>
                                    <td>{{$wd->status}}</td>
                                    <td>{{$wd->created_at}}</td>
                                </tr>
                            @endforeach
                            {{ $withdraw->links() }}
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane fade show active" id="etalase-nav" role="tabpanel" aria-labelledby="etalase-nav-tab">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col" class="text-center">Amount</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $d = 1
                            @endphp
                            @foreach($topup as $tp)
                                <tr>
                                    <th scope="row">{{$d++}}</th>
                                    <td>Rp{{number_format($tp->amount)}}</td>
                                    <td>{{$tp->status}}</td>
                                    <td>{{$tp->created_at}}</td>
                                </tr>
                            @endforeach
                            {{ $topup->links() }}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /table transaction wd and dp -->

    </div>

    <!-- canvas address -->
    <div class="offcanvas offcanvas-bottom h-65" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h2 class="offcanvas-title font-bold " id="offcanvasBottomLabel">Withdraw</h2>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body px-0 small">
            <form id="form_input">
                <!-- container list address -->
                <div class="px-3 d-flex align-items-center gap-3 overflow-scroll container-bank">
                    @foreach($user_banks as $user_bank)
                        <div data-target="parent" data-address="{{$user_bank->id}}" class="card-bank-mini choice-bank rounded-lg {{$user_bank->is_active == 1 ? 'active' : '';}} p-1 mt-2">
                            <input type="hidden" value="{{$user_bank->account_number}}" name="account_number" id="account_number">
                            <div class="d-flex flex-column justify-content-end h-100">
                                <span class="font-sm">{{$user_bank->banks->name}}</span>                            
                                <div data-target="parent-body" class="d-flex flex-column text-box" data-maxlength="25">
                                    <span class="font-sm">{{$user_bank->account_number}}</span>
                                    <span class="font-sm">{{$user_bank->account_holder_name}}</span>
                                </div>
                            </div>
            
                        </div>
                    @endforeach
                    <a href="{{route('web.profile.bank_account.add')}}" class="card-bank-mini hover-alice border-primary d-flex align-items-center justify-content-center rounded bg- p-1 mt-2">
                        
                        <span class="text-primary">Tambah Rekening Bank</span>

                    </a>
                </div>

                <div class="form-group m-0 mt-2 px-3">
                    <label for="amount" class="m-0">Nominal penarikan</label>
                    <input type="text" id="amount" placeholder="10.000" name="amount" class="form-control" required>
                    <span class="text-xs text-secondary">Minimum penarikan Rp10.000</span>
                </div>
            </form>

            <!-- /container list address -->
        </div>
        <div class="offcanvas-footer">
            <div class="bottom-0 left-0 px-3 py-1 w-100">
                <button class="btn btn-primary w-100" onclick="withdraw(this)">Tarik Saldo</button>
            </div>
        </div>
    </div>
    <!-- /canvas address -->

    @section("custom_js")
    <script>
        // var pages = 1;
        // $(window).scroll(function(){
        //     // console.log($(window).scrollTop());
        //     if($(window).scrollTop() + $(window).height() >= $(document).height()){
        //         pages++;
        //         load_more(pages, "");
        //     }
        // });

        $("#btn-wd").click(function (){
            // $("#btn-tp").find("span")[0].classList.toggle("text-white")
            $("#btn-tp").find("span")[0].classList.remove("text-bajaga-primary")
            $(this).find("span")[0].classList.add("text-bajaga-primary")
        })
        
        $("#btn-tp").click(function (){
            $("#btn-wd").find("span")[0].classList.remove("text-bajaga-primary")
            $(this).find("span")[0].classList.add("text-bajaga-primary")
        })
        $(".choice-bank").click(function (){

            let id_bank = $(this).attr("data-address")
            let route = '{{route('web.profile.bank_account.is_use')}}' + '?id=' + id_bank
            let element = $(this)
            // console.log(route);
            // doing ajax
            $.ajax({
                type: 'PATCH',
                url: route,
                dataType: 'json',
                processData: false,
                success: function(response){
                    if(response.alert == 'success'){
                        Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                        $(".container-bank > div").removeClass("active")
                        element.addClass("active")
                    }else{
                        Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    }
                }
            })

        })

        function withdraw(el){
            // SHOW load spinner
            let textButton = $(el).text()
            $(el).prop('disabled', 'disabled')
            $(el).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)

            let user_id = "{{$user->id}}"
            let amount = $("#amount").val()
            // console.log(amount);
            let email = "{{$user->email}}"

            var settings = {
                "url": "/api/xendit/disbursment",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "user_id": user_id,
                    "amount": amount,
                    "description": "Withdraw saldo store",
                    "email_to": [
                        email
                    ]
                }),
            };

            $.ajax(settings).done(function (response) {
                // console.log(response);
                if (response.alert == "success") {
                    Swal.fire({ text: response.message, icon: "success", confirmButtonText: "Ok!",  buttonsStyling: !1, customClass: { confirmButton: "btn btn-primary" }});
                } else {
                    Swal.fire({ text: response.message, icon: "error", confirmButtonText: "Ok, Mengerti!",  buttonsStyling: !1, customClass: { confirmButton: "btn btn-primary" }});
                }
                load_list(1)
                $(el).html(`${textButton}`)
                $(el).prop('disabled', false)
                load_list_local(1, '#list_result_wd', '{{route('web.store.paystore.withdraw_more_data')}}')
            }).fail(response => {
                Swal.fire({ text: response.responseJSON.message, icon: "error", confirmButtonText: "Ok, Mengerti!",  buttonsStyling: !1, customClass: { confirmButton: "btn btn-primary" }});
                // console.log(response);
                $(el).html(`${textButton}`)
                $(el).prop('disabled', false)
                load_list_local(1, '#list_result_wd', '{{route('web.store.paystore.withdraw_more_data')}}')
            });
        }

        function load_list_local(page, conatiner, route){
            $.get(route+'?page=' + page, $('#content_filter').serialize(), function(result) {
                $(conatiner).html(result);
            }, "html");
        }

        $("#amount").on("keyup", function(){
            let rupiah = formatRupiah($(this).val().toString())
            $(this).val(rupiah)
        })

        setInterval(() => {
            load_list(1)
            load_list_local(1, '#list_result_wd', '{{route('web.store.paystore.withdraw_more_data')}}')
        }, 5000);
    </script>
    @endsection
</x-web-layout>