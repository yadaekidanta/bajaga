<x-web-layout title="Toko Favorit" keyword="Bajaga Store">
    <div class="container-custom px-3 pb-10em bg-white pt-2">
        <div class="d-flex flex-column gap-3"
         id="list_result">
            @foreach ($store as $key => $item)
            <div class="h-10em w-100 overflow-hidden position-relative">
                <img src="{{asset('storage/'. ($item->photo ? $item->photo : 'store/default.png'))}}" class="w-100 h-100 object-fit-cover" alt="image-store">

                <div class="p-1 position-absolute bg-dark-transparant w-100 bottom-0 left-0">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex gap-3">
                            <div class="w-2em h-2em overflow-hidden">
                                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->photo ? $item->photo : 'store/default.png'))}}" alt="image store">
                            </div>
                            <div>
                                <span class="d-block font-sm text-white">{{$item->name}}</span>
                            </div>
                        </div>

                        <form id="form_favorite_{{$key}}" class="m-0">
                            <input type="number" class="d-none" value="{{$item->id}}" name="store_id">
                            <!-- icon love favorite -->
                            <!-- no-fill -->
                            <svg role="button" class="favorite-outline" onclick="add('#form_favorite_{{$key}}')" width="24" fill="white" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 21.593c-5.63-5.539-11-10.297-11-14.402 0-3.791 3.068-5.191 5.281-5.191 1.312 0 4.151.501 5.719 4.457 1.59-3.968 4.464-4.447 5.726-4.447 2.54 0 5.274 1.621 5.274 5.181 0 4.069-5.136 8.625-11 14.402m5.726-20.583c-2.203 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248-3.183 0-6.281 2.187-6.281 6.191 0 4.661 5.571 9.429 12 15.809 6.43-6.38 12-11.148 12-15.809 0-4.011-3.095-6.181-6.274-6.181"/></svg>
                            <!-- /no-fill -->
                            <!-- /icon love favorite -->
                        </form>
                    </div>
                </div>
            </div>
            @endforeach

            @foreach ($storeFavorite as $item)
                <hr class="my-2">
                <div class="d-flex justify-content-between">
                    <div class="d-flex gap-3">
                        <div class="w-3em h-3em rounded-circle overflow-hidden">
                            <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->store->photo ? $item->store->photo : 'store/default.png'))}}" alt="image store">
                        </div>
                        <div>
                            <div class="d-flex gap-3 align-items-center">
                                <img class="logo-sm" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                <span class="text-primary">{{$item->store->name}}</span>
                            </div>
                            <span class="font-sm text-secondary">{{$item->store->city->name}}</span>
                        </div>
                    </div>

                    <form id="form_favorite" class="m-0">
                        <!-- fill -->
                        <svg role="button" class="favorite-fill me-1" onclick="destroy('{{route('web.store.favorite.destroy', $item->id)}}')"   fill="#ec7d87" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>
                        <!-- fill -->
                        <!-- /icon love whitelist -->
                    </form>
                </div>
            @endforeach
        </div>
    </div>

    @section('custom_js')
        <script>
            // load_list(1)
            function add(param){
                // alert(param)
                let data = $(param).serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{route('web.store.favorite.add')}}",
                    data: data,
                    success: function(response){
                        load_list(1)
                    },
                    error: (response) => {
                        load_list(1)
                    }
                })
            }
            function destroy(url){
                $.ajax({
                    type: 'DELETE',
                    url,
                    success: function(response){
                        load_list(1)
                    },
                    error: (response) => {
                        load_list(1)
                    }
                })
            }
        </script>
    @endsection
</x-web-layout>