<x-web-layout title="Selamat Datang" keyword="Bajaga Store">
    <img src="{{asset('img/bg/Intersect.png')}}" class="image-fluid mt-3 wave-top" alt="wave">
    
    <div class="px-3">
        <div class="card p-1">
            <div class="d-flex justify-content-around gap-3">
                @if($available == 2)
                    <a href="{{route('web.open-store.index')}}" class="d-flex align-items-center gap-3">
                        <svg class="icon-size-2" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M10 9v-1.098l1.047-4.902h1.905l1.048 4.9v1.098c0 1.067-.933 2.002-2 2.002s-2-.933-2-2zm5 0c0 1.067.934 2 2.001 2s1.999-.833 1.999-1.9v-1.098l-2.996-5.002h-1.943l.939 4.902v1.098zm-10 .068c0 1.067.933 1.932 2 1.932s2-.865 2-1.932v-1.097l.939-4.971h-1.943l-2.996 4.971v1.097zm-4 2.932h22v12h-22v-12zm2 8h18v-6h-18v6zm1-10.932v-1.097l2.887-4.971h-2.014l-4.873 4.971v1.098c0 1.066.933 1.931 2 1.931s2-.865 2-1.932zm15.127-6.068h-2.014l2.887 4.902v1.098c0 1.067.933 2 2 2s2-.865 2-1.932v-1.097l-4.873-4.971zm-.127-3h-14v2h14v-2z"/></svg>
                        <div class="d-flex flex-column text-dark align-items-center">
                            <span class="font-bold font-thin font-sm m-0">Poin Toko</span>
                            <span class="font-bold font-sm -mt-1">{{$point_store}}</span>
                        </div>
                    </a>
                @elseif($available == 0)
                    <a href="{{route('web.auth.index')}}" class="d-flex align-items-center gap-3">
                        <ion-icon name="log-in-outline" style="font-size: 30px;"></ion-icon>
                        <span class="font-bold font-thin font-sm m-0 text-black">Masuk / Daftar</span>
                    </a>
                @else
                    <a href="{{route('web.open-store.index')}}" class="d-flex align-items-center gap-3">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon-size-2" fill="#4879D3" viewBox="0 0 24 24"><path d="M21 13v10h-21v-19h12v2h-10v15h17v-8h2zm3-12h-10.988l4.035 4-6.977 7.07 2.828 2.828 6.977-7.07 4.125 4.172v-11z"/></svg>
                        <div class="d-flex flex-column">
                            <span class="font-bold font-thin font-sm m-0 text-black">Buka Toko</span>
                        </div>
                    </a>
                @endif

                @auth
                <div class="d-flex align-items-center gap-3">
                    <svg class="icon-size-2" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.326 18.266l-4.326-2.314-4.326 2.313.863-4.829-3.537-3.399 4.86-.671 2.14-4.415 2.14 4.415 4.86.671-3.537 3.4.863 4.829z"/></svg>
                    
                    <div class="d-flex flex-column align-items-center">
                        <span class="font-bold font-thin font-sm m-0">Poin User</span>
                        <span class="font-bold font-sm -mt-1">{{$point_user}}</span>
                    </div>
                </div>
                @endauth
            </div>
        </div>
    </div>
    
    <div class="d-flex pl-3 pe-1 align-items-start mt-3 gap-4 overflow-scroll">
        @foreach($subCategory as $subc)
            <a href="{{route('web.category.show_subcategory', $subc->slug)}}" class="w-10 d-flex flex-column align-items-center gap-2 text-dark">
                <img src="{{asset('storage/'.$subc->photo)}}" class="image-fluid min-w-2.5em" alt="image category">
                <span class="font-xs font-bold text-center act-dark" style="line-height: 0.8rem;">{{$subc->title}}</span>
            </a>
        @endforeach
    </div>
    <div class="mt-3">
        <div class="px-3 d-flex align-items-center">
            <h2 class="font-lg m-0">Kategori Pilihan</h2>
        </div>
        <div class="section-category mt-2 pe-2">
            <div class="container-card">
                @for($i = 0; $i < (count($category)-1); $i++)
                <div class="">
                    <a href="{{route('web.category.show', $category[$i]->slug)}}" class="text-dark card-category">
                        <div class="container-img-card-category">
                            <img class="category-img" src="{{asset('storage/'.$category[$i]->photo)}}" alt="">
                        </div>
                        <span>{{$category[$i++]->title}}</span>
                    </a>
                    <a href="{{route('web.category.show', $category[$i]->slug)}}" class="card-category text-dark">
                        <div class="container-img-card-category">
                            <img class="category-img" src="{{asset('storage/'.$category[$i]->photo)}}" alt="">
                        </div>
                        <span>{{$category[$i]->title}}</span>
                    </a>
                </div>
                @endfor
            </div>
        </div>
    </div>
    <div class="">
        <div class="d-flex px-3 align-items-center justify-content-between">
            <h2 class="font-lg m-0">{{Str::title('Produk pilihan untukmu')}}</h2>
            <a href="{{route('web.catalog.products')}}" class="text-sm">Lihat semua</a>
        </div>

        <div class="section-slide mt-2 pe-2">
            @foreach($product_pilihan as $p)
            <div class="container-card text-dark shadow">
                <div class="container-img">
                    <a href="{{route('web.product.show', $p->slug)}}">
                        <img src="{{asset('storage/'.$p->photo)}}" alt="">
                    </a>
                </div>
                <div class="p-1 description-product">
                    <div class="d-flex flex-column">
                        <a href="{{route('web.product.show', $p->slug)}}" class="font-sm 
                        text-capitalize line-normal text-dark "><span class="act-dark">{{Str::limit($p->name, 18)}}</span></a>
                        <a href="{{route('web.product.show', $p->slug)}}" class="font-medium text-dark font-bold">
                            <span class="act-dark">
                                Rp {{number_format($p->price)}}</a>
                            </span>
                        <a href="{{route('web.store', $p->product_store->id)}}" class="text-dark d-flex mt-2 gap-1">
                                <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                <span style="margin-top: -5px !important;">
                                    <span class="text-capitalize line-normal">{{$p->product_store->name}}</span>
                                    <span class="font-sm">{{\Str::limit($p->product_store->city->name, 14)}}</span>
                                </span>
                        </a>
                    </div>

                </div>

            </div>
            @endforeach
        </div>
    </div>
    @if($trending->count() > 0)
    <div class="d-flex px-3 align-items-center justify-content-between">
        <h2 class="font-lg m-0">{{Str::title('Lagi trending nih!')}}</h2>
    </div>

    <div class="section-slide mt-2 py-3 pb-0 bg-bajaga-primary pe-2">
        @foreach($trending as $p)
        <a href="{{route('web.product.show', $p->product->slug)}}" class="container-card text-dark shadow">
            <div class="container-img">
                <img src="{{asset('storage/'.$p->product->photo)}}" alt="">
            </div>
            <div class="p-1 description-product">
                <div class="d-flex flex-column">
                    <span class="font-sm text-capitalize line-normal">{{$p->product->name}}</span>
                </div>
            </div>
        </a>
        @endforeach
    </div>
    </div>
    @endif
    <div class=" mt-3">
        <div class="d-flex px-3 align-items-center">
            <h2 class="font-lg m-0">{{Str::title('Brand pilihan')}}</h2>
        </div>

        <div class="section-category pl-3 pe-2">
            <div class="container-card">
                @foreach($brand as $b)
                <a href="{{route('web.category.show_brand', $b->slug)}}" class="card-brand text-dark">
                    <div class="container-img-card-brand">
                        <img class="category-img" src="{{asset('storage/'.$b->photo)}}" alt="">
                    </div>
                    <span>{{$b->name}}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>
    @section('custom_js')
    @endsection
</x-web-layout>