<x-web-layout title="Buka Toko" keyword="Bajaga Online Store">
    <div class="post d-flex flex-column-fluid pt-4" id="kt_post">
        <div id="kt_content_container" class="container-xxl">
            <div class="card">
                <div class="card-header border-0 pt-3">
                    <div class="card-toolbar">
                        <div class="d-flex justify-content-center">
                            Halo {{Auth::user()->name}}, Masukan Detail Toko Anda!
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <form id="form_input">
                        <div class="row">
                            <div class="col-lg-12 mt-2">
                                <input type="hidden" name="users_id" id="users_id" value="{{Auth::user()->id}}">
                                <label for="name" class="required fs-6 fw-bold mb-1">Nama Toko</label>
                                <input type="text" id="name" class="form-control" name="name" placeholder="Masukkan nama toko...">
                            </div>
                            <div class="col-lg-4 mt-2">
                                <label for="province" class="required fs-6 fw-bold mb-1">Provinsi</label>
                                <select class="form-control" name="province_id" id="province" class="form-control" required></select>                
                            </div>
                            <div class="col-lg-4 mt-2">
                                <label class="required fs-6 fw-bold mb-1">Kota</label>
                                <select class="form-control" id="city" name="city_id"></select>
                            </div>
                            <div class="col-lg-4 mt-2">
                                <label class="required fs-6 fw-bold mb-1">Kecamatan</label>
                                <select class="form-control" id="subdistrict" name="subdistrict_id"></select>
                            </div>
                            <div class="col-lg-12 mt-2">
                                <label class="required fs-6 fw-bold mb-1">Alamat Toko</label>
                                <textarea name="address" class="form-control" id="address" cols="30" rows="10"></textarea>
                            </div>
                            <div class="min-w-150px mt-10 text-end mt-2">
                                <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.open-store.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @section('custom_js')
        <script type="text/javascript">
        get_province();
        function get_province(){
            $.post('{{route('web.regional.province')}}', {}, function(result) {
                $("#province").html(result);
            }, "html");
        }
        $("#province").change(function(){
            $.post('{{route('web.regional.city')}}', {province : $("#province").val()}, function(result) {
                $("#city").html(result);
            }, "html");
        });
        $("#city").change(function(){
            $.post('{{route('web.regional.subdistrict')}}', {city : $("#city").val()}, function(result) {
                $("#subdistrict").html(result);
            }, "html");
        });
        </script>
    @endsection
</x-web-layout>