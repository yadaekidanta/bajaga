@foreach($collections as $product)
    <div class="section-card-vertical" style="margin: 1rem 0.3rem;">
        <input type="hidden" value="{{$product->created_at}}" id="created_at">
        <input type="hidden" value="{{$product->price}}" id="item_price">
        <div class="container-card shadow">
            <div class="container-img position-relative">
                <img src="{{asset($product->image)}}" alt="">
                <div class="d-flex gap-2 align-items-center position-absolute left-10" style="bottom: 7px;">
                    <a href="javascript:void(0);" onclick="load_input('{{route('web.product.edit',$product->id)}}')" class="btn btn-dark btn-sm p-1 me-2"><i class="icon-pencil-alt"></i></a>
                </div>
            </div>
            <div class="p-1 description-product">
                <div class="d-flex flex-column">
                    <span class="font-sm text-capitalize line-normal product-title">{{$product->name}}</span>
                    <span class="font-medium font-bold">Rp {{number_format($product->price)}}</span>
                    <div class="d-flex align-items-center gap-1">
                        <img class="logo-sm" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                        <span class="font-sm">{{$product->product_store->city->name}}</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endforeach