@foreach ($collection as $product)
                    <div class="card p-2 shadow-none border mt-3">
                        <div class="d-flex gap-3 align-items-start">
                            <div class="w-3em h-3em rounded-circle border overflow-hidden">
                                <img src="{{asset('storage/'.$product->photo)}}" class="w-100 h-100 object-fit-cover" alt="image product">
                            </div>
                            <span class="font-bold">{{$product->name}}</span>
        
                        </div>
                        <div class="w-100 mt-2 ">    
                            @foreach ($product->product_reviews()->get() as $ulasan)
                                @if (!$ulasan->to_id)
                                    <div class="card p-2 {{ !$ulasan->read_at ?'bg-aliceblue': ''}} shadow-none border">
                                        {{-- icon star --}}
                                        <div class="d-flex gap-2 align-items-center">
                                            @for ($i = 0 ; $i < intval($ulasan->rate); $i++)
                                                <i class="icon-star3 text-emas font-icon-size-1"></i>
                                            @endfor
                                            @for ($i = intval($ulasan->rate); $i < 5; $i++)
                                                <i class="icon-star-empty text-secondary font-icon-size-1"></i>
                                            @endfor
                                        </div>
                                        {{-- icon star --}}
                                        
                                        <span>{{$ulasan->review}}</span>
        
                                        @if (!$ulasan->read_at)
                                            <span class="d-flex justify-content-end font-bold text-primary" role="button" onclick="handle_open_modal('{{route('web.store.review.addModalReview', $ulasan->id)}}', '#modalListResult', '#contentListResult')">Balas</span>
                                        @endif
                                    </div>
                                @else
                                    <div class="w-100 d-flex justify-content-end">
                                        <div class="card p-2 {{ !$ulasan->read_at ?'bg-aliceblue': ''}} w-90% shadow-none border">
                                            <span>{{$ulasan->review}}</span>
                                        </div>
                                    </div>
                                @endif
                                <hr>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                @endforeach