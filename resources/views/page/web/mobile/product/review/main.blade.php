<x-web-layout title="Ulasan" keyword="Bajaga Store">
    <div class="bg-white pt-3 container-custom">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item px-2" role="presentation">
                <button class="nav-link font-bold active" id="semua-tab" data-bs-toggle="tab" data-bs-target="#semua" type="button" role="tab" aria-controls="semua" aria-selected="true">Semua Ulasan</button>
            </li>
            <li class="nav-item px-2" role="presentation">
                <button class="nav-link font-bold" id="baru-tab" data-bs-toggle="tab" data-bs-target="#baru" type="button" role="tab" aria-controls="baru" aria-selected="false">Menunggu Dibalas</button>
            </li>
        </ul>
        <div class="tab-content w-100 h-screen" id="myTabContent">
            <div class="tab-pane fade px-3 mt-3 w-100 h-100 show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div id="list_result"></div>
                <div class="ajax-load d-none d-flex justify-content-center w-100 mt-5">
                    <div class="spinner-grow text-primary" role="status">
                    </div>
                </div>

                
                {{-- <div class="d-flex flex-column justify-content-center align-items-center w-100 mt-10em">
                    <h3 class="text-center m-0">Belum ada barang untuk kamu ulas</h3>
                    <span class="text-muted text-center">Saatnya belanja dan kasih ulasan, yuk!</span>
                </div> --}}

            </div>
            <div class="tab-pane fade px-3 mt-3 w-100 h-100" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                
                {{-- <div class="d-flex flex-column justify-content-center align-items-center w-100 mt-10em">
                    <h3 class="text-center m-0">Belum ada riwayat ulasan</h3>
                    <span class="text-muted text-center">Saatnya kamu belanja dan kasih ulasanmu ke jutaan orang!</span>
                </div> --}}

            </div>
        </div>
    </div>
    <form id="content_filter" class="d-none">
        <input type="hidden" id="filter_input" name="filter">
    </form>
    @section('custom_js')
    <script>
        load_list(1)

        var tabEl = document.querySelectorAll('button[data-bs-toggle="tab"]')
        tabEl.forEach(element => {
            element.addEventListener('shown.bs.tab', function (event) {
                // resetState()
                let target = $(event.target).attr("data-bs-target")
                if (target == "#semua") {
                    $("#filter_input").val('')
                    load_list(1)
                }else if(target == "#baru"){
                    $("#filter_input").val('Baru')
                    load_list(1)
                }
            })
        });
    </script>
    @endsection
</x-web-layout>