<x-web-layout title="Kelola Produk" keyword="Bajaga Online Store">
    <div id="content_list">
        <div class="container-custom bg-white bg-dark-mode py-3 pb-5">
            <div class="d-flex justify-content-between px-3">
                <div class="d-flex gap-3">
                    <div class="w-20 h-20 border overflow-hidden">
                        <img class="object-fit-cover w-100 h-100" src="{{asset( $available->photo != null ? 'storage/'.$available->photo : 'img/store/default.png')}}" alt="">
                    </div>
                    <div class="d-flex flex-column">
                        <h3 class="font-bold m-0">{{Str::limit($available->name, 18)}}</h3>
                        <span>{{Str::limit($available->city->name, 22)}}</span>
                        <div class="mt-2">
                            <span class="font-sm">Buka mulai:</span>
                            {{$available->start_at ? $available->start_at->format('H:i') : '..:..'}} -  {{$available->end_at ? $available->end_at->format('H:i') : '..:..'}}</span>
                        </div>
                    </div>
                </div>

                <a href="{{route('web.store.settings')}}">
                    <svg class="gir-setting" fill="#595a5cb0" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M24 13.616v-3.232c-1.651-.587-2.694-.752-3.219-2.019v-.001c-.527-1.271.1-2.134.847-3.707l-2.285-2.285c-1.561.742-2.433 1.375-3.707.847h-.001c-1.269-.526-1.435-1.576-2.019-3.219h-3.232c-.582 1.635-.749 2.692-2.019 3.219h-.001c-1.271.528-2.132-.098-3.707-.847l-2.285 2.285c.745 1.568 1.375 2.434.847 3.707-.527 1.271-1.584 1.438-3.219 2.02v3.232c1.632.58 2.692.749 3.219 2.019.53 1.282-.114 2.166-.847 3.707l2.285 2.286c1.562-.743 2.434-1.375 3.707-.847h.001c1.27.526 1.436 1.579 2.019 3.219h3.232c.582-1.636.75-2.69 2.027-3.222h.001c1.262-.524 2.12.101 3.698.851l2.285-2.286c-.744-1.563-1.375-2.433-.848-3.706.527-1.271 1.588-1.44 3.221-2.021zm-12 2.384c-2.209 0-4-1.791-4-4s1.791-4 4-4 4 1.791 4 4-1.791 4-4 4z"/></svg>
                </a>
            </div>

            <!-- Points -->
            <div class="px-3 mt-3">
                <div class="my-2 border bg-white card-dark-mode rounded-lg p-2 d-flex justify-content-around gap-3">
                    <div class="d-flex flex-column align-items-center">
                        <svg class="icon-size-3" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M10 9v-1.098l1.047-4.902h1.905l1.048 4.9v1.098c0 1.067-.933 2.002-2 2.002s-2-.933-2-2zm5 0c0 1.067.934 2 2.001 2s1.999-.833 1.999-1.9v-1.098l-2.996-5.002h-1.943l.939 4.902v1.098zm-10 .068c0 1.067.933 1.932 2 1.932s2-.865 2-1.932v-1.097l.939-4.971h-1.943l-2.996 4.971v1.097zm-4 2.932h22v12h-22v-12zm2 8h18v-6h-18v6zm1-10.932v-1.097l2.887-4.971h-2.014l-4.873 4.971v1.098c0 1.066.933 1.931 2 1.931s2-.865 2-1.932zm15.127-6.068h-2.014l2.887 4.902v1.098c0 1.067.933 2 2 2s2-.865 2-1.932v-1.097l-4.873-4.971zm-.127-3h-14v2h14v-2z"/></svg>
                        <span class="font-bold font-thin font-sm m-0">Poin Toko</span>
                        <span class="font-bold font-sm -mt-1">100</span>
                    </div>

                    <a href="{{route('web.store.paystore')}}" class="d-flex text-dark flex-column align-items-center">
                        <svg class="icon-size-3" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M0 8v-3c0-1.105.895-2 2-2h20c1.104 0 2 .895 2 2v3h-24zm24 3v8c0 1.104-.896 2-2 2h-20c-1.105 0-2-.896-2-2v-8h24zm-15 6h-6v1h6v-1zm3-2h-9v1h9v-1zm9 0h-3v1h3v-1z"/></svg>

                        <span class="font-bold font-thin font-sm m-0">Saldo</span>
                        <span class="font-bold font-sm -mt-1">Rp {{number_format($balance)}}</span>
                    </a>
                </div>
            </div>
            <!-- /Points -->

            <div class="mt-2">
                <ul class="nav nav-tabs d-flex align-items-center justify-content-around" id="myTab" role="tablist">
                    <li class="nav-item px-2" role="presentation">
                        <button class="nav-link active font-bold padding-tab-navs-icon" id="btn-store" data-bs-toggle="tab" data-bs-target="#store-nav" type="button" role="tab" aria-controls="store-nav" aria-selected="true">
                            <!-- fill -->
                            <svg class="store-fill" xmlns="http://www.w3.org/2000/svg" width="20" height="17.844" viewBox="0 0 20 17.844"><path d="M2.1,10.05A4.072,4.072,0,0,1,2,9.6L3.8,4.3A1.6,1.6,0,0,1,5.7,3.2H18.3a1.955,1.955,0,0,1,1.9,1.3c.6,1.8,1.7,4.9,1.8,5.1a4.331,4.331,0,0,0-.1.5,1.69,1.69,0,0,1-.2.5,3.208,3.208,0,0,1-.3.5l-.4.4a2.716,2.716,0,0,1-1.7.6,2.628,2.628,0,0,1-2.7-2.7,1.421,1.421,0,0,0-.2-.6.56.56,0,0,1-.082-.109c-.033-.053-.056-.091-.118-.091l-.1-.1a.1.1,0,0,0-.1-.1.367.367,0,0,0-.3-.1.355.355,0,0,0-.15.05.355.355,0,0,1-.15.05.1.1,0,0,1-.1.1.349.349,0,0,0-.2.1l-.1.1a.1.1,0,0,0-.1.1.764.764,0,0,0-.2.5A2.628,2.628,0,0,1,12,12,2.476,2.476,0,0,1,9.3,9.3a.754.754,0,0,0-.2-.453C9.07,8.8,9.037,8.755,9,8.7l-.1-.1-.1-.1a.13.13,0,0,1-.1-.05.13.13,0,0,0-.1-.05.1.1,0,0,0-.1-.1.367.367,0,0,0-.3-.1.355.355,0,0,0-.15.05.355.355,0,0,1-.15.05l-.1.1-.1.1a.559.559,0,0,1-.109.082c-.053.033-.091.056-.091.118a1.421,1.421,0,0,0-.2.6A2.628,2.628,0,0,1,4.6,12a2.716,2.716,0,0,1-1.7-.6L2.5,11a3.208,3.208,0,0,1-.3-.5A4.072,4.072,0,0,0,2.1,10.05ZM15.6,12.3A4.983,4.983,0,0,0,19.3,14a4.671,4.671,0,0,0,1.7-.3V19a1.89,1.89,0,0,1-2,2H15V19a1.89,1.89,0,0,0-2-2H11a1.89,1.89,0,0,0-2,2v2H5a1.89,1.89,0,0,1-2-2V13.7a5.079,5.079,0,0,0,1.7.3,4.611,4.611,0,0,0,3.6-1.7A4.385,4.385,0,0,0,12,14,4.611,4.611,0,0,0,15.6,12.3Z" transform="translate(-2 -3.156)" fill="#15314f" fill-rule="evenodd"/></svg>
                            <!-- fill -->

                        </button>
                    </li>
                    <!-- <li class="nav-item px-2" role="presentation">
                        <button class="nav-link font-bold padding-tab-navs-icon" id="btn-auction" data-bs-toggle="tab" data-bs-target="#auction-nav" type="button" role="tab" aria-controls="auction-nav" aria-selected="false">
                            <svg class="store-fill" width="20" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M20.9999 7C20.991 6.88416 20.9675 6.76991 20.9299 6.66C20.928 6.63336 20.928 6.60664 20.9299 6.58C20.9113 6.54422 20.8946 6.5075 20.8799 6.47L19.1799 3.87C19.1799 3.81 19.0899 3.76 19.0499 3.71C18.849 3.49255 18.6066 3.31765 18.3369 3.19569C18.0672 3.07372 17.7758 3.00718 17.4799 3H6.47985C5.91652 3.03262 5.38701 3.27949 4.99985 3.69C4.95112 3.73871 4.90756 3.79233 4.86985 3.85L3.16985 6.45C3.15509 6.4875 3.1384 6.52422 3.11985 6.56C3.12166 6.58664 3.12166 6.61336 3.11985 6.64C3.05724 6.75124 3.01651 6.87344 2.99985 7V19C2.99004 19.2653 3.03507 19.5297 3.1321 19.7768C3.22914 20.0239 3.37611 20.2483 3.56382 20.436C3.75153 20.6237 3.97594 20.7707 4.22303 20.8678C4.47012 20.9648 4.73457 21.0098 4.99985 21H18.9999C19.2651 21.0098 19.5296 20.9648 19.7767 20.8678C20.0238 20.7707 20.2482 20.6237 20.4359 20.436C20.6236 20.2483 20.7706 20.0239 20.8676 19.7768C20.9646 19.5297 21.0097 19.2653 20.9999 19V7ZM6.44985 5.05C6.44985 5.05 6.50985 5.05 6.44985 5.05H17.4199L17.5099 5.11L18.1299 6.11H5.82985L6.44985 5.05ZM9.99985 8H13.9999V9.61L12.2999 9.05C12.0923 8.97989 11.8674 8.97989 11.6599 9.05L9.99985 9.61V8ZM18.9999 19H4.99985V8H7.99985V11C7.99944 11.1578 8.03638 11.3134 8.10764 11.4542C8.1789 11.595 8.28246 11.7169 8.40985 11.81C8.5326 11.9098 8.67727 11.9791 8.83199 12.0121C8.98671 12.0451 9.14705 12.041 9.29985 12L11.9799 11.1L14.6599 12C14.7726 12.0163 14.8871 12.0163 14.9999 12C15.2089 12.0029 15.413 11.9361 15.5799 11.81C15.7091 11.718 15.8146 11.5965 15.8876 11.4557C15.9606 11.3148 15.9991 11.1587 15.9999 11V8H18.9999V19Z"/></svg>
                        </button>
                    </li> -->
                    <li class="nav-item px-2" role="presentation">
                        <button class="nav-link font-bold padding-tab-navs-icon" id="btn-etalase" data-bs-toggle="tab" data-bs-target="#etalase-nav" type="button" role="tab" aria-controls="etalase-nav" aria-selected="false">
                            <!-- etalase fill -->
                            <svg class="etalase-fill" xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20"><path d="M16.258,0h-14a2.006,2.006,0,0,0-2,2V18a2.006,2.006,0,0,0,2,2h14a2.006,2.006,0,0,0,2-2V2A2.006,2.006,0,0,0,16.258,0Zm-5,17h-4a1,1,0,0,1,0-2h4a.945.945,0,0,1,1,1A1,1,0,0,1,11.258,17Zm0-6h-4a.945.945,0,0,1-1-1,.945.945,0,0,1,1-1h4a.945.945,0,0,1,1,1A1,1,0,0,1,11.258,11Zm0-6h-4a.945.945,0,0,1-1-1,.945.945,0,0,1,1-1h4a.945.945,0,0,1,1,1A1,1,0,0,1,11.258,5Z" transform="translate(-0.258)" fill="#15314f"/></svg>
                            <!-- etalase fill -->
                        </button>
                    </li>
                </ul>
                <div class="tab-content w-100" id="myTabContent">
                    <div class="tab-pane fade show active mt-3 px-3" id="store-nav" role="tabpanel" aria-labelledby="store-nav-tab">
                        
                        <div class="w-full my-1 d-flex justify-content-between">
                            <h2 class="font-lg m-0">Semua Produk</h2>
                            <span class="text-primary" onclick="load_input('{{route('web.product.create')}}')">Tambah Produk</span>
                        </div>

                        <!-- filter -->
                        <div class="d-flex gap-3 my-3">
                            <span class="font-bold">Filter</span>
                            <div class="rounded-lg border px-1 d-flex gap-2 align-items-center" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                                <span class="status-filter hover-opacity-65">Semua status</span>
                                
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                        <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                                    </defs>
                                    <g fill="none" fill-rule="evenodd">
                                        <path d="M0 24h24V0H0z"/>
                                        <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                                    </g>
                                </svg>

                            </div>
                        </div>
                        <!-- /filter -->
                        
                        <div id="list_result" class="d-flex flex-wrap justify-content-between position-relative h-100"></div>
                        <div class="ajax-load text-center mt-3" style="display:none">
                            <p class="text-primary font-bold">Load More Product...</p>
                        </div>

                        
                    </div>

                    <!-- <div class="tab-pane fade show mt-3 px-3" id="auction-nav" role="tabpanel" aria-labelledby="auction-nav-tab">
                        <div class="w-full my-1 d-flex">
                            <h2 class="font-lg m-0">Semua Produk lelang</h2>
                        </div>

                        <div id="list_result_auction" class="d-flex flex-wrap position-relative h-100"></div>
                        <div class="ajax-load text-center mt-3" style="display:none">
                            <p class="text-primary font-bold">Load More Product...</p>
                        </div>
                    </div> -->

                    <div class="tab-pane fade show mt-3 px-3" id="etalase-nav" role="tabpanel" aria-labelledby="etalase-nav-tab">
                        <div class="d-flex align-items-center justify-content-between">
                            <h3 class="m-0">Atur Etalase</h3>
                            <a href="{{route('web.store.settings.product.list_etalase')}}">Tambah Etalase</a>
                        </div>

                        <div>
                            @foreach($etalase as $et)
                                <a href="{{route('web.etalase', $et->id)}}?store_id={{$available->id}}" class="d-flex align-items-center text-dark gap-3 mt-2">
                                    <div class="w-10 h-10 border">
                                        <img src="{{asset('img/product/'.$et->thumbnail)}}" alt="image etalase">
                                    </div>
                                    <span class="font-bold">{{$et->name}}</span>
            
                                </a>
                                <hr class="m-0 mt-2">
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <div id="content_input"></div>

    <!-- canvas filter -->
    <div class="offcanvas offcanvas-bottom h-50" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small position-relative pb-5">
            <form id="content_filter">
                <input type="hidden" name="keywords" id="keywords">
                <!-- filter sort -->
                <!-- <div>
                    <h3 class="font-bold m-0">Urutkan</h3>
                    <div class="d-flex flex-wrap align-item-center gap-2 mt-1 container-sort-filter">
                        <span data-sort-by="new_item" class="px-2 py-1 d-flex align-item-center justify-center border border-primary text-primary rounded-pill">
                            Terbaru
                        </span>
                        <span data-sort-by="name" class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                            Nama
                        </span>
                        <span data-sort-by="price_hl" class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                            Harga Tertinggi
                        </span>
                        <span data-sort-by="price_lh" class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                            Harga Terendah
                        </span>
                    </div>
                </div> -->
                <!-- /filter sort -->

                <!-- filter Categories -->
                <div class="mt-3">
                    <h3 class="font-bold m-0">Kategori</h3>
                    <div class="d-flex flex-wrap align-item-center gap-2 mt-1 container-categories">
                        @foreach($categories as $category)
                            <span data-filter-by="{{$category->id}}" onclick="filter(this)" class="px-2 py-1 d-flex align-item-center justify-content-center border  rounded-pill">
                                {{$category->title}}
                            </span>
                        @endforeach
                    </div>
                </div>
                <!-- /filter Categories -->
                {{-- <input type="text" name="keywords" id="keywords" class="d-none"> --}}
            </form>
        </div>
    </div>
    <!-- /canvas filter -->

    

    <!-- canvas menu right -->
    <div class="offcanvas offcanvas-end w-100" tabindex="-1" id="canvasMenuStore" aria-labelledby="canvasMenuStoreLabel">
        <div class="offcanvas-header">
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body px-0 small position-relative">
            <!-- saldo toko -->
            <div class="px-3">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-bold font-lg">Saldo Toko</span>
                </div>

                <div class="d-flex align-items-center mt-2 gap-2 w-100">
                    <a href="{{route('web.store.paystore')}}" class="d-flex m-0 gap-2 hover-alice align-items-center justify-content-between w-100 p-1 py-3 rounded border">
                        <span>Rp{{number_format($balance)}}</span>
                        <span class="d-flex gap-2 align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><g transform="translate(512 0) scale(-1 1)"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="48" d="M328 112L184 256l144 144"/></g></svg>
                        </span>
                    </a>
                </div>
            </div>
            <!-- /saldo toko -->
            <hr class="px-3 my-4">
            <!-- riwayat penjualan -->
            <div class="px-3">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-bold font-lg">PENJUALAN</span>
                    <a href="{{route('web.store.history.transaction')}}" class="font-sm">Lihat Riwayat</a>
                </div>

                <div class="d-flex align-items-center mt-2 gap-2">
                    <a href="{{route('web.store.history.transaction')}}?filter=Tertunda" class="d-flex gap-2 hover-alice text-dark align-items-center p-1 rounded border">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" viewBox="0 0 18.003 18.001"><path d="M21,7a1.376,1.376,0,0,0-.07-.34.562.562,0,0,1,0-.08,1.143,1.143,0,0,1-.05-.11l-1.7-2.6c0-.06-.09-.11-.13-.16A2.21,2.21,0,0,0,17.48,3h-11A2.21,2.21,0,0,0,5,3.69a1,1,0,0,0-.13.16l-1.7,2.6a1.17,1.17,0,0,1-.05.11.59.59,0,0,1,0,.08A1,1,0,0,0,3,7V19a1.93,1.93,0,0,0,2,2H19a1.93,1.93,0,0,0,2-2ZM6.45,5.05H17.42l.09.06.62,1H5.83ZM10,8h4V9.61l-1.7-.56a1,1,0,0,0-.64,0L10,9.61Zm9,11H5V8H8v3a1.04,1.04,0,0,0,1.3,1l2.68-.9,2.68.9A1.188,1.188,0,0,0,15,12a.94.94,0,0,0,.58-.19A1,1,0,0,0,16,11V8h3Z" transform="translate(-2.999 -3)"/></svg>
                        Pesanan Baru
                    </a>
                    <a href="{{route('web.store.history.transaction')}}?filter=Dipesan" class="d-flex gap-2 hover-alice text-dark align-items-center p-1 rounded border">
                        <i class="icon-shipping-fast" style="font-size: 24px;"></i>
                        Siap dikirim
                    </a>
                </div>
            </div>
            <!-- /riwayat penjualan -->
            <hr class="px-3 my-4">
            <!-- produk -->
            <div class="px-3">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-bold font-lg">PRODUK</span>
                    <span class="font-sm text-primary" onclick="load_input('{{route('web.product.create')}}')" data-bs-dismiss="offcanvas">Tambah Produk</span>
                </div>

                <a href="{{route('web.store.settings.product')}}" class="d-flex hover-opacity-65 align-items-center justify-content-between mt-2">
                    <div>
                        <div class="font-lg font-bold text-dark">Daftar Produkmu</div>
                        <div class="font-sm text-dark">{{$count_product}} produk</div>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><g transform="translate(512 0) scale(-1 1)"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="48" d="M328 112L184 256l144 144"/></g></svg>
                </a>
            </div>
            <!-- /produk -->
            <hr class="px-3 my-4">
            <!-- kata pembeli -->
            <div class="px-3">
                <a href="{{route('web.chat')}}" class="d-flex text-dark hover-opacity-65 align-items-center gap-2 mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 3c5.514 0 10 3.685 10 8.213 0 5.04-5.146 8.159-9.913 8.159-2.027 0-3.548-.439-4.548-.712l-4.004 1.196 1.252-2.9c-.952-1-2.787-2.588-2.787-5.743 0-4.528 4.486-8.213 10-8.213zm0-2c-6.628 0-12 4.573-12 10.213 0 2.39.932 4.591 2.427 6.164l-2.427 5.623 7.563-2.26c1.585.434 3.101.632 4.523.632 7.098.001 11.914-4.931 11.914-10.159 0-5.64-5.372-10.213-12-10.213z"/></svg>
                    <span>Pesan</span>
                </a>
            </div>
            <!-- /kata pembeli -->
            <hr class="px-3 my-4">
            <!-- kata pembeli -->
            <div class="px-3">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-bold font-lg">KATA PEMBELI</span>
                </div>

                <a href="{{route('web.store.review.menu')}}" class="d-flex hover-opacity-65 text-dark align-items-center gap-2 mt-2">
                    <svg class="unf-icon" viewBox="0 0 24 24" width="24" height="24" fill="black" style="display: inline-block; vertical-align: middle;"><path d="M7.62 20.74A2.381 2.381 0 0 1 5.27 18l.64-3.73-2.71-2.69a2.39 2.39 0 0 1 1.32-4.07L8.27 7 10 3.57a2.38 2.38 0 0 1 4.27 0L15.9 7l3.74.54A2.39 2.39 0 0 1 21 11.58l-2.72 2.64.61 3.78a2.37 2.37 0 0 1-.89 2.29 2.41 2.41 0 0 1-2.52.18l-3.35-1.76-3.4 1.76a2.42 2.42 0 0 1-1.11.27Zm4.46-17a.87.87 0 0 0-.79.5l-2 4.1L4.74 9a.89.89 0 0 0-.49 1.51l3.27 3.19-.77 4.51a.87.87 0 0 0 .35.86.89.89 0 0 0 .94.07l4-2.13 4 2.13a.89.89 0 0 0 1.29-.93l-.78-4.51 3.28-3.19a.89.89 0 0 0-.4-1.51l-4.53-.66-2-4.1a.88.88 0 0 0-.82-.5Z"></path></svg>
                    Ulasan
                </a>
                {{-- <a href="{{route('web.store.review', 'diskusi')}}" class="d-flex hover-opacity-65 text-dark align-items-center gap-2 mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="black" viewBox="0 0 24 24"><path d="M22 3v13h-11.643l-4.357 3.105v-3.105h-4v-13h20zm2-2h-24v16.981h4v5.019l7-5.019h13v-16.981z"/></svg>
                    Diskusi
                </a> --}}
            </div>
            <!-- /kata pembeli -->
            <hr class="px-3 my-4">
        </div>
    </div>
    <!-- /canvas menu right -->

    @section('custom_js')
    <script>
        load_list(1);

        $(".container-categories").on('click', function (event) {
            $target = $(event.target);
            $('.container-categories > span').removeClass('border-primary text-primary')
            $target.addClass('border-primary text-primary');
            [].slice.call($(this).children()).forEach(element => {
                if ($(element).hasClass('border-primary')) {
                    let dataFilter = $(element).attr('data-filter-by')
                    $("#keywords").val(dataFilter)
                    load_list(1)
                }
            });
            return false;
        });

        //function for Scroll Event
        var pages = 1;
        $(window).scroll(function(){
            // console.log($(window).scrollTop());
            if($(window).scrollTop() + $(window).height() >= $(document).height()){
                pages++;
                load_more(pages);
            }
        });

        function load_auction(page){
            $.get('/store/auction?page=' + page, $('#content_filter').serialize(), function(result) {
                console.log(result);
                $('#list_result_auction').html(result);
            }, "html");
        }

        $("#btn-auction").click(function() {
            if ($(this).hasClass("active")) {
                load_auction(1)
            }
        })

        $(".gir-setting").click(function(){
            $(this).addClass("animate-rotate")
        })
        
        function filter(el) {
            let filter = $(el).attr("data-filter-by")
            $("#keywords").val(filter)
            load_list(1)
        }
        
    </script>
    @endsection
</x-web-layout>