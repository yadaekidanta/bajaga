@foreach($collection as $product)
    <div class="w-47%">
        <div class="section-card-vertical h-100">
            <input type="hidden" value="{{$product->created_at}}" id="created_at">
            <input type="hidden" value="{{$product->price}}" id="item_price">
            <a href="{{route('web.product.show', $product->slug)}}">
                <div class="container-card shadow">
                    <div class="container-img position-relative">
                        <img src="{{asset($product->image)}}" alt="">
                        <div class="d-flex gap-2 align-items-center position-absolute left-10" style="bottom: 7px;">
                            <a href="javascript:;" onclick="load_input('{{route('web.product.edit',$product->id)}}')" class="btn btn-dark btn-sm p-1 me-2"><i class="icon-pencil-alt"></i></a>
                            <a href="javascript:void(0);" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('web.product.destroy',$product->id)}}', true);" class="btn p-1 btn-sm btn-dark me-2"><i class="icon-trash"></i></a>
                        </div>
                    </div>
                    <div class="p-1 card-dark-mode description-product">
                        <div class="d-flex flex-column">
                            <span class="font-sm text-capitalize line-normal product-title">{!! Str::limit($product->name, 18, ' ...') !!}</span>
                            <span class="font-medium font-bold">Rp {{number_format($product->price)}}</span>
                            <div class="d-flex mt-2 gap-1">
                                <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                <span style="margin-top: -5px !important;">
                                    <span class="text-capitalize line-normal">{{$product->product_store->name}}</span>
                                    <span class="font-sm">{{\Str::limit($product->product_store->city->name, 14)}}</span>
                                </span>
                            </div>
                        </div>
                    </div>
        
                </div>
            </a>
        </div>
    </div>
@endforeach