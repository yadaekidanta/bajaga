@foreach($collections as $col)
    @if(count($col->sale_detail()->get()) > 1)
        <div class="card text-black p-2 mt-2 success item w-100">
            <!-- header -->
            <a href="{{route('web.transaction.detail', $col->id)}}" class="d-flex justify-content-between align-items-center">
                <div class="d-flex gap-2 align-items-center">
                    <i class="icon-bag font-xl text-primary"></i>
                    <div class="d-flex flex-column">
                        <span class="font-bold font-sm text-dark">Belanja</span>
                        <span class="font-thin font-xs -mt-1 text-dark">252525</span>
                    </div>
                </div>
        
                <div class="d-flex gap-2 align-items-center">
                    <span class="badge bg-primary">{{$col->st}}</span>
                </div>
            </a>
            <!-- header -->
        
            <hr class="my-2">
        
            <!-- body -->
            <div class="">
                <div class="d-flex align-items-center gap-2 card-sale">
                    <div class="w-10 h-10 border overflow-hidden object-fit-cover">
                        <img src="{{asset('storage/'.$col->sale_detail()->get()[0]->product->photo)}}" alt="image product" class="object-fit-cover w-100 h-100">
                    </div>
                    <div class="w-100 px-1" id="collapse" data-collapse-target="collapse-{{$col->id}}">

                        <div class="font-bold font-sm text-dark">{{count($col->sale_detail()->get())}} Item</div>

                        <div class="d-flex align-items-center justify-content-between">
                            <span class="font-sm text-secondary">Detail Produk</span>
            
                            <svg id="arrow" class="rotate-90" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                <defs>
                                    <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                                </defs>
                                <g fill="none" fill-rule="evenodd">
                                    <path d="M0 24h24V0H0z"/>
                                    <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>

                <!-- detail more item -->
                <div id="collapse-{{$col->id}}" class="collapse mt-3">
                    @foreach($col->sale_detail()->get() as $product)
                        <div class="d-flex align-items-center gap-2 mt-3">
                            <div class="w-10 h-10 border overflow-hidden object-fit-cover">
                                <img src="{{asset('storage/'.$product->product->photo)}}" alt="image product" class="object-fit-cover w-100 h-100">
                            </div>
                            <div class="">
                                <span class="font-bold">{{$product->product->name}}</span>
                                <span class="font-sm d-block">Rp{{number_format($product->price - $product->disc_price)}} x {{$product->qty}}</span>
                            </div>
                        </div>
                    @endforeach
                    <hr class="mb-2">
                </div>
                <!-- detail more item -->
                
                <div class="mt-2 d-flex justify-content-between align-items-center">
                    <a href="{{route('web.transaction.detail', $col->id)}}" class="d-flex flex-column">
                        <span class="font-xs text-dark">Total Belanja</span>
                        <span class="font-sm font-bold text-dark -mt-1">Rp {{number_format(255555)}}</span>
                    </a>
                    @if($col->payment_st == "Lunas" && $col->st == "Dipesan")
                        <button class="btn btn-sm btn-outline-primary" id="atur-pengiriman" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasPengiriman" aria-controls="offcanvasPengiriman">Atur Pengiriman</button>
                        <input type="hidden" value="{{$col->courier}}">
                        <input type="hidden" value="{{$col->id}}">
                        <input type="hidden" value="{{$col->code_courier}}">
                    @endif
                </div>
            </div>
            <!-- body -->
        </div>
    @else
        @foreach($col->sale_detail()->get() as $product)
        <div class="card text-black p-2 mt-2 success item w-100">
            <!-- header -->
            <a href="{{route('web.transaction.detail', $col->id)}}" class="d-flex justify-content-between align-items-center">
                <div class="d-flex gap-2 align-items-center">
                    <i class="icon-bag font-xl text-primary"></i>
                    <div class="d-flex flex-column">
                        <span class="font-bold font-sm text-dark">Belanja</span>
                        <span class="font-thin font-xs -mt-1 text-dark">{{$col->date}}</span>
                    </div>
                </div>

                <div class="d-flex gap-2 align-items-center">
                    <span class="badge bg-primary">{{$col->st}}</span>
                </div>
            </a>
            <!-- header -->

            <hr class="my-2">

            <!-- body -->
            <div class="">
                <div class="d-flex align-items-center gap-2">
                    <div class="w-10 h-10 border overflow-hidden object-fit-cover">
                        <img src="{{asset('storage/'.$product->product->photo)}}" alt="image product" class="object-fit-cover w-100 h-100">
                    </div>
                    <div>
                        <h4 class="m-0 font-bold"><span class="text-dark text-capitalize">[{{$product->product->condition}}]</span> {{$product->product->name}}</h4>
                        <span class="font-xs -mt-0.8">{{$product->qty}} Barang</span>
                    </div>
                </div>
                
                <div class="mt-2 d-flex justify-content-between align-items-center">
                    <a href="{{route('web.transaction.detail', $col->id)}}" class="d-flex flex-column text-dark">
                        <span class="font-xs">Total Belanja</span>
                        <span class="font-sm font-bold -mt-1">Rp {{number_format((floatval($product->price) + floatval($product->shipping_price)) - floatval($product->disc_price))}}</span>
                    </a>
                    @if($col->payment_st == "Lunas" && $col->st == "Dipesan")
                        <button class="btn btn-sm btn-outline-primary" id="atur-pengiriman" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasPengiriman" aria-controls="offcanvasPengiriman">Atur Pengiriman</button>
                        <input type="hidden" value="{{$col->courier}}">
                        <input type="hidden" value="{{$col->id}}">
                        <input type="hidden" value="{{$col->code_courier}}">
                    @endif
                </div>
            </div>
            <!-- body -->
        </div>
        @endforeach
    @endif

@endforeach

@if (count($collections))
    <script>
        $(".card-sale").click(function(){
            $("#"+$(this).find("#collapse").attr("data-collapse-target")).toggleClass("show")
            $(this).find("#arrow").toggleClass("rotate-90")
        })

        $("#atur-pengiriman").click(function(){
            let courier = $(this).next().val()
            let id_sale = $(this).next().next().val()
            let code_courier = $(this).next().next().next().val()
            console.log(code_courier);
            $("#courier_canvas").val(courier)
            $("#id_sale").val(id_sale)
            $("#code_courier").val(code_courier)
            // console.log(courier);
        })
    </script>
@endif