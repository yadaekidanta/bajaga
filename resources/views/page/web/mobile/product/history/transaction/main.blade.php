<x-web-layout title="Penjualan" keyword="Bajaga Store">
    <div class="bg-white pt-2 pb-5 container-custom">
        <!-- heading -->
        <div class="px-3">
            <!-- search -->
            <form class="m-0">
                <div class="position-relative w-100">
                    <input id="search_input" type="text" onkeyup="search()" class="form-control w-100 px-4" placeholder="Cari nama pembeli, atau resi">
    
                    <!-- icon search -->
                    <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                        <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                            <g fill="none" fill-rule="evenodd">
                                <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                                <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                            </g>
                        </svg>
                    </div>
                    <!-- icon search -->
    
                </div>
            </form>
            <!-- search -->
    
            <!-- filter -->
            <div class="d-flex gap-3 mt-2">
                <div class="d-flex align-item-center gap-2 container-sort-filter overflow-scroll">
                    <span data-filter="Semua" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                        Semua Pesanan
                    </span>
                    <span data-filter="Dipesan" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                        Siap Dikirim
                    </span>
                    <span data-filter="Dikirim" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                        Dalam Pengirman
                    </span>
                    <span data-filter="Diterima" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                        Pesanan Selesai
                    </span>
                </div>
                <form id="content_filter" class="d-none">
                    <input type="hidden" id="filter_input" name="filter">
                </form>
            </div>
            <!-- /filter -->
        </div>
        <!-- /heading -->

        <hr class="m-0">

        <!-- body -->
        <div class="mt-3 px-3 ">
            <div class="" id="list_result"></div>
            <div class="ajax-load  d-flex justify-content-center w-100 mt-5">
                <div class="spinner-grow text-secondary" role="status">
                </div>
            </div>
        </div>
        <!-- body -->
    </div>

    <div class="offcanvas offcanvas-bottom h-29vh" tabindex="-1" id="canvasConfirmastionResi" aria-labelledby="canvasConfirmastionResiLabel">
        <form action="">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="canvasConfirmastionResiLabel">Konfirmasi Resi</h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body small">
                    <input type="number" class="form-control">
                    <div class="position-absolute bottom-0 w-100 left-0 p-2">
                        <button class="btn btn-primary w-100">Konfirmasi</button>
                    </div>
            </div>
        </form>
    </div>

    <!-- tinggal bikinin canvas di ubah harga dan stok -->

    <!-- canvas for atur pengirimana -->
    <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasPengiriman" aria-labelledby="offcanvasPengirimanLabel">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasPengirimanLabel">Pengiriman</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small">
            <form id="form_input" class="m-0">
                <input type="hidden" id="id_sale" name="id_sale" class="form-control">
                <input type="text" id="courier_canvas" disabled name="courier" class="form-control">
                <input type="text" id="no_resi" name="no_resi" class="form-control mt-2" placeholder="Masukkan no resi">
                <input type="hidden" id="code_courier" name="code_courier" class="form-control mt-2" placeholder="Masukkan no resi">
            </form>
        </div>
        <div class="offcanvas-footer small px-3 py-2">
            <button class="btn btn-primary w-100" id="btn-simpan" onclick="handle_save('#btn-simpan', '#form_input', '{{route('web.store.tracking_number')}}', 'POST')">Simpan</button>
        </div>
    </div>
    <!-- canvas for atur pengirimana -->
    
    @section('custom_js')
    <script>
        localStorage.setItem("page_infinate", 1)
        if (url.searchParams.get("filter")) {
            $("#filter_input").val(url.searchParams.get("filter"))
            load_list(1)
        }else{
            load_list(1)
        }



        $("#reset-filter").click(function(){
            $('.container-sort-filter > span').removeClass('border-primary text-primary')
            $('.container-status-pesanan-filter > span').removeClass('border-primary text-primary')
            $('.container-courier-filter > span').removeClass('border-primary text-primary')
        })

        $(".container-sort-filter").on('click', function (event) {
            // alert()
            $target = $(event.target);
            localStorage.setItem("page_infinate", 1)
            $('.container-sort-filter > span').removeClass('border-primary text-primary')
            if ($target.attr("data-filter") == "Semua") {
                $("#filter_input").val('')
            }else{
                $("#filter_input").val($target.attr("data-filter"))
            }
            load_list(1)
            $target.addClass('border-primary text-primary');
        });

        $(".container-status-pesanan-filter").on('click', function (event) {
            $target = $(event.target);
            $('.container-status-pesanan-filter > span').removeClass('border-primary text-primary')
            $target.addClass('border-primary text-primary');
        });

        $(".container-courier-filter").on('click', function (event) {
            $target = $(event.target);
            $('.container-courier-filter > span').removeClass('border-primary text-primary')
            $target.addClass('border-primary text-primary');
        });

        function search(){
            let filter = $("#search_input").val()
            // console.log(filter);
            $.get('?search='+filter, function(result) {
                $('#list_result').html(result);
            })
        }

        $(document).ready(function(){
            let options = {
                root: null,
                rootMargin: '10px',
                threshold: 0.5
            }

            const observer = new IntersectionObserver(handleIntersect, options)
            observer.observe(document.querySelector(".ajax-load"))
        })
        
        function handleIntersect(entries){
            if (entries[0].isIntersecting) {
                // alert()
                localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                load_more(parseInt(localStorage.getItem("page_infinate")));
            }
        }
    </script>
    @endsection
    
</x-web-layout>

