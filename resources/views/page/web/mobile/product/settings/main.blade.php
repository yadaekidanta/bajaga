<x-web-layout title="Pengaturan Toko" keyword="Bajaga Store">
    <div class="container-custom bg-dark-mode bg-white py-3">
        <!-- profile toko -->
        <div class="px-3 d-flex gap-1 flex-column">
            <div class="d-flex mb-1 gap-2 align-items-center">
                <svg class="icon-size-19px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10 9v-1.098l1.047-4.902h1.905l1.048 4.9v1.098c0 1.067-.933 2.002-2 2.002s-2-.933-2-2zm5 0c0 1.067.934 2 2.001 2s1.999-.833 1.999-1.9v-1.098l-2.996-5.002h-1.943l.939 4.902v1.098zm-10 .068c0 1.067.933 1.932 2 1.932s2-.865 2-1.932v-1.097l.939-4.971h-1.943l-2.996 4.971v1.097zm-4 2.932h22v12h-22v-12zm2 8h18v-6h-18v6zm1-10.932v-1.097l2.887-4.971h-2.014l-4.873 4.971v1.098c0 1.066.933 1.931 2 1.931s2-.865 2-1.932zm15.127-6.068h-2.014l2.887 4.902v1.098c0 1.067.933 2 2 2s2-.865 2-1.932v-1.097l-4.873-4.971zm-.127-3h-14v2h14v-2z"/></svg>
                <h3 class="m-0">Profil Toko</h3>
            </div>
            <a href="{{route('web.store.settings.profile')}}" class="text-dark">Ubah profil toko</a>
            <hr class="my-3">
        </div>
        <!-- profile toko -->

        <!-- produk toko -->
        <div class="px-3 d-flex gap-1 flex-column">
            <div class="d-flex mb-1 gap-2 align-items-center">                
                
                <svg width="19" height="17" viewBox="0 0 19 17" fill="black" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_4_10)">
                    <path d="M3.3322 14.8904C3.5373 14.8904 3.70399 14.7367 3.70399 14.5469V8.62351C3.70399 8.43464 3.53734 8.28003 3.3322 8.28003H1.91388C1.70744 8.28003 1.54211 8.43464 1.54211 8.62351V14.5469C1.54211 14.7367 1.7074 14.8904 1.91388 14.8904H3.3322Z"/>
                    <path d="M18.4179 15.4671H0.606913C0.404222 15.4671 0.23645 15.621 0.23645 15.8112V16.7039C0.23645 16.8947 0.404222 17.0481 0.606913 17.0481H18.4179C18.623 17.0481 18.7874 16.8948 18.7874 16.7039V15.8112C18.7874 15.621 18.623 15.4671 18.4179 15.4671Z"/>
                    <path d="M7.9275 14.8904C8.13052 14.8904 8.29696 14.7367 8.29696 14.5469V8.62351C8.29696 8.43464 8.13052 8.28003 7.9275 8.28003H6.50558C6.30274 8.28003 6.13745 8.43464 6.13745 8.62351V14.5469C6.13745 14.7367 6.30274 14.8904 6.50558 14.8904H7.9275Z"/>
                    <path d="M17.1134 14.8904C17.3164 14.8904 17.4838 14.7367 17.4838 14.5469V8.62351C17.4838 8.43464 17.3163 8.28003 17.1134 8.28003H15.6912C15.4875 8.28003 15.3222 8.43464 15.3222 8.62351V14.5469C15.3222 14.7367 15.4875 14.8904 15.6912 14.8904H17.1134Z"/>
                    <path d="M18.5298 6.34357L10.9378 0.499839C10.1532 -0.102607 8.87162 -0.102607 8.08694 0.499839L0.496516 6.34357C-0.289275 6.94888 -0.0966402 7.44106 0.921678 7.44106H18.1052C19.1226 7.44106 19.3141 6.94888 18.5298 6.34357ZM9.51246 6.01934C8.71632 6.01934 8.07102 5.4179 8.07102 4.67782C8.07102 3.93761 8.71632 3.33834 9.51246 3.33834C10.3106 3.33834 10.954 3.93761 10.954 4.67782C10.954 5.4179 10.3106 6.01934 9.51246 6.01934Z"/>
                    <path d="M12.5193 14.8904C12.7245 14.8904 12.8898 14.7367 12.8898 14.5469V8.62351C12.8898 8.43464 12.7245 8.28003 12.5193 8.28003H11.0998C10.8942 8.28003 10.7289 8.43464 10.7289 8.62351V14.5469C10.7289 14.7367 10.8942 14.8904 11.0998 14.8904H12.5193Z"/>
                    </g>
                    <defs>
                    <clipPath id="clip0_4_10">
                    <rect width="19" height="17" fill="white"/>
                    </clipPath>
                    </defs>
                </svg>


                <h3 class="m-0">Rekening Bank</h3>
            </div>
            <a href="{{route('web.profile.bank_account')}}" class="text-dark">Tambah rekening bank</a>
            <hr class="my-3">
        </div>
        <!-- produk toko -->

        <!-- produk toko -->
        <div class="px-3 d-flex gap-1 flex-column">
            <div class="d-flex mb-1 gap-2 align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon-size-19px" viewBox="0 0 18.003 18.001"><path d="M21,7a1.376,1.376,0,0,0-.07-.34.562.562,0,0,1,0-.08,1.143,1.143,0,0,1-.05-.11l-1.7-2.6c0-.06-.09-.11-.13-.16A2.21,2.21,0,0,0,17.48,3h-11A2.21,2.21,0,0,0,5,3.69a1,1,0,0,0-.13.16l-1.7,2.6a1.17,1.17,0,0,1-.05.11.59.59,0,0,1,0,.08A1,1,0,0,0,3,7V19a1.93,1.93,0,0,0,2,2H19a1.93,1.93,0,0,0,2-2ZM6.45,5.05H17.42l.09.06.62,1H5.83ZM10,8h4V9.61l-1.7-.56a1,1,0,0,0-.64,0L10,9.61Zm9,11H5V8H8v3a1.04,1.04,0,0,0,1.3,1l2.68-.9,2.68.9A1.188,1.188,0,0,0,15,12a.94.94,0,0,0,.58-.19A1,1,0,0,0,16,11V8h3Z" transform="translate(-2.999 -3)"/></svg>
                <h3 class="m-0">Produk Toko</h3>
            </div>
            <a href="{{route('web.store.settings.product')}}" class="text-dark">Lihat daftar produk</a>
            <a href="{{route('web.store.settings.product.list_etalase')}}" class="text-dark">Tambah dan ubah etalase</a>
            <hr class="my-3">
        </div>
        <!-- produk toko -->

        <!-- pengiriman toko -->
        <div class="px-3 d-flex gap-1 flex-column">
            <div class="d-flex mb-1 gap-2 align-items-center">
                <i class="icon-shipping-fast" style="font-size: 19px;"></i>
                <h3 class="m-0">Layanan toko</h3>
            </div>
            <a class="text-dark" href="{{route('web.store.settings.shipping.address')}}">Lokasi toko</a>
            <span class="text-dark" type="button" data-bs-toggle="offcanvas" data-bs-target="#canvasOprational" aria-controls="canvasOprational">Atur jam operasional toko</span>
            {{-- <a href="{{route('web.store.settings.shipping.services')}}" class="text-dark">Atur layanan pengiriman</a> --}}
        </div>
        <!-- pengiriman toko -->
    </div>

    {{-- canvas oprasional --}}
    <div class="offcanvas offcanvas-bottom" tabindex="-1" id="canvasOprational" aria-labelledby="canvasOprationalLabel">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="canvasOprationalLabel">Atur jam oprasional toko</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small">
            <form id="form_input">
                <div class="d-flex gap-2 align-item-center">
                    <input type="time" class="form-control w-100" name="start_at" id="start_at">
                    <input type="time" class="form-control w-100" name="end_at" id="end_at">
                </div>
            </form>
        </div>
        <div class="offcanvas-footer small px-3 py-2">
            <button class="btn btn-primary w-100" id="btn-simpan" onclick="asd('#btn-simpan', '#form_input', '{{route('web.store.settings.oprational')}}', 'POST')">Simpan</button>
        </div>
    </div>
    {{-- canvas oprasional --}}
    
    @section('custom_js')
        <script>
            function asd(tombol, form, url, method){
                // alert()
                $(tombol).html(`
                    <div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>
                `)
                handle_save(tombol, form, url, method, function(){
                    $(tombol).html("simpan")
                })
            }
        </script>
    @endsection
    
</x-web-layout>