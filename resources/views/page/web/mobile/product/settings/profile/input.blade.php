<x-web-layout title="Ubah Profil Toko" keyword="Bajaga Store">
    <div class="bg-white bg-dark-mode pt-5 container-custom px-3">
        <div id="load_result">
            <form id="form_input">
                <div class="d-flex flex-column align-items-center">
                    <div class="profile-avatar d-flex align-items-center justify-content-center overflow-hidden">
                        <img id="preview_avatar" class="w-100 h-100 object-fit-cover" src="{{asset( $store->photo ? 'storage/'.$store->photo : 'img/store/default.png')}}" alt="avatar user">
                    </div>
                    <div class="mt-2">
                        <label for="image" class="text-primary">Ubah Foto Toko</label>
                        <input type="file" name="photo" id="image" class="d-none">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mt-3">
                        <label class="required fs-6 fw-bold mb-1">Nama Toko</label>
                        <input type="text" class="form-control" name="name" placeholder="Masukkan nama toko" value="{{$store->name}}">
                    </div>
                    <div class="col-lg-6 mt-2">
                        <label for="desc" class="required fs-6 fw-bold mb-1">Catatan toko</label>
                        <textarea name="desc" class="form-control" id="desc" cols="30" rows="5">{{$store->desc}}</textarea>
                    </div>
                </div>
                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.store.settings.profile')}}','PATCH', true);" class="btn mt-2 btn-primary w-100">Simpan</button>
            </form>
        </div>
    </div>

    @section('custom_js')
    <script>
        $("#image").change(function(){
            let filesAmount = this.files.length
            let reader = new FileReader();
            reader.onload = (e) => { 
                $('img#preview_avatar').attr('src', e.target.result); 
            }

            reader.readAsDataURL(this.files[0]); 
        })
    </script>
    @endsection
</x-web-layout>
