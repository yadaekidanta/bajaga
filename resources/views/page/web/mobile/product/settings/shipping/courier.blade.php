<x-web-layout title="Lokasi" keyword="Bajaga Store">
    <!-- list of courier -->
    <div class="container-custom d-flex flex-column gap-3 bg-white py-3 px-3" id="container-courier">
        <form id="form_input">
            @foreach($collections as $collection)
            <div class="card">
                <div class="card-body d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center gap-2">
                        <div class="w-20 h-10 border overflow-hidden">
                            <img src="{{asset('img/courier/'.$collection->name.'.png')}}" alt="" class="object-fit-cover w-100 h-100">
                        </div>
                        <span class="font-bold font-lg">{{$collection->name}}</span>
                    </div>
                    <div class="m-0">
                        <input style="width: 22px; height: 22px;" {{ $collection->status ? 'checked' : ''}} type="checkbox" name="si_cepat_reguler" id="si_cepat_reguler" class="form-check-input m-0 input-ongkir">
                    </div>
                </div>
            </div>
            @endforeach
        </form>
    </div>
    <!-- list of courier -->


    @section("custom_js")
    <script>
        $(".card").click(function (){
            $(this).find(".input-ongkir").prop("checked", true)
        })
    </script>
    @endsection
</x-web-layout>