<div class="card">
    <div class="card-body">
        <div class="d-flex justify-content-between">
            <div class="">
                <div class="font-bold font-lg">{{$address->name}}</div>
                <input type="hidden" id="city_id_tmp" value="{{$address->city_id}}">
                <input type="hidden" id="subdistrict_id_tmp" value="{{$address->subdistrict_id}}">
                <div class="d-flex flex-column text-box mt-1" data-maxlength="25">
                        <span class="font-sm text-secondary">{{$address->address}}</span>
                        <span class="font-sm text-secondary text-capitalize">
                            {{$address->subdistrict->name}}, {{$address->city->name}}, {{$address->province->name}}</span> 
                </div>
            </div>
            <div class="pl-3">
                <svg role="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" xmlns="http://www.w3.org/2000/svg" fill="#8f8f8f" width="18" viewBox="0 0 24 24"><path d="M14.078 7.061l2.861 2.862-10.799 10.798-3.584.723.724-3.585 10.798-10.798zm0-2.829l-12.64 12.64-1.438 7.128 7.127-1.438 12.642-12.64-5.691-5.69zm7.105 4.277l2.817-2.82-5.691-5.689-2.816 2.817 5.69 5.692z"/></svg>
            </div>
        </div>
        <hr>
        <div class="d-flex gap-3 align-items-center">
            <svg xmlns="http://www.w3.org/2000/svg" fill="8f8f8f" width="20" viewBox="0 0 24 24"><path d="M5 3.461c0 .978.001 16.224 0 17.213-.002 2.214 3.508 3.326 7.014 3.326 3.495 0 6.986-1.105 6.986-3.326v-17.213c0-2.348-3.371-3.461-6.805-3.461-3.563 0-7.195 1.199-7.195 3.461zm7-1.461c.276 0 .5.223.5.5 0 .276-.224.5-.5.5s-.5-.224-.5-.5c0-.277.224-.5.5-.5zm0 20c-.552 0-1-.448-1-1 0-.553.448-1 1-1s1 .447 1 1c0 .552-.448 1-1 1zm5-3h-10v-15h10v15z"/></svg>
            <span class="col-10">{{$address->user->phone}}</span>
        </div>
        <hr>
        <div class="d-flex gap-3 align-items-center">
            <svg class="" xmlns="http://www.w3.org/2000/svg" fill="8f8f8f" width="20" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm21.518 2l-9.518 7.713-9.518-7.713h19.036zm-19.518 14v-11.817l10 8.104 10-8.104v11.817h-20z"/></svg>
            <span class="col-10">{{$address->user->email}}</span>
        </div>
    </div>
</div>