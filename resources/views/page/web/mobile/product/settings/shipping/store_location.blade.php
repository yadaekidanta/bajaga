<x-web-layout title="Lokasi" keyword="Bajaga Store">
    <div id="list_result" class="container-custom bg-white bg-dark-mode py-3 px-3">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div class="">
                        <div class="font-bold font-lg">{{$address->name}}</div>
                        <input type="hidden" id="city_id_tmp" value="{{$address->city_id}}">
                        <input type="hidden" id="subdistrict_id_tmp" value="{{$address->subdistrict_id}}">
                        <div class="d-flex flex-column text-box mt-1" data-maxlength="25">
                                <span class="font-sm text-secondary">{{$address->address}}</span>
                                <span class="font-sm text-secondary text-capitalize">
                                    {{$address->subdistrict->name}}, {{$address->city->name}}, {{$address->province->name}}</span> 
                        </div>
                    </div>
                    <div class="pl-3">
                        <svg role="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" xmlns="http://www.w3.org/2000/svg" fill="#8f8f8f" width="18" viewBox="0 0 24 24"><path d="M14.078 7.061l2.861 2.862-10.799 10.798-3.584.723.724-3.585 10.798-10.798zm0-2.829l-12.64 12.64-1.438 7.128 7.127-1.438 12.642-12.64-5.691-5.69zm7.105 4.277l2.817-2.82-5.691-5.689-2.816 2.817 5.69 5.692z"/></svg>
                    </div>
                </div>
                <hr>
                <div class="d-flex gap-3 align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="8f8f8f" width="20" viewBox="0 0 24 24"><path d="M5 3.461c0 .978.001 16.224 0 17.213-.002 2.214 3.508 3.326 7.014 3.326 3.495 0 6.986-1.105 6.986-3.326v-17.213c0-2.348-3.371-3.461-6.805-3.461-3.563 0-7.195 1.199-7.195 3.461zm7-1.461c.276 0 .5.223.5.5 0 .276-.224.5-.5.5s-.5-.224-.5-.5c0-.277.224-.5.5-.5zm0 20c-.552 0-1-.448-1-1 0-.553.448-1 1-1s1 .447 1 1c0 .552-.448 1-1 1zm5-3h-10v-15h10v15z"/></svg>
                    <span class="col-10">{{$address->user->phone}}</span>
                </div>
                <hr>
                <div class="d-flex gap-3 align-items-center">
                    <svg class="" xmlns="http://www.w3.org/2000/svg" fill="8f8f8f" width="20" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm21.518 2l-9.518 7.713-9.518-7.713h19.036zm-19.518 14v-11.817l10 8.104 10-8.104v11.817h-20z"/></svg>
                    <span class="col-10">{{$address->user->email}}</span>
                </div>
            </div>
        </div>
    </div>

    <!-- canvas update -->
    <div class="offcanvas offcanvas-bottom h-75" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasBottomLabel">Offcanvas bottom</h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body small mb-5 pb-3">
        <form id="form_input">
            <div class="col-lg-4">
                <label for="province" class="required fw-bold mb-1">Provinsi</label>
                <select class="form-control" name="province_id" id="province" class="form-control" required>
                    @foreach($provinsi as $p)
                        <option value="{{$p->id}}" {{ $address->province_id == $p->id ? 'selected': ''}}>{{$p->name}}</option>
                    @endforeach
                </select>                
            </div>
            <div class="col-lg-4">
                <label class="required fw-bold mb-1">Kota</label>
                <select class="form-control" id="city" name="city_id"></select>
            </div>
            <div class="col-lg-4">
                <label class="required fw-bold mb-1">Kecamatan</label>
                <select class="form-control" id="subdistrict" name="subdistrict_id"></select>
            </div>
            <div class="col-lg-12">
                <label class="required fw-bold mb-1">Alamat Toko</label>
                <textarea name="address" class="form-control" id="address" cols="30" rows="5">{{$address->address}}</textarea>
            </div>
            <div class="min-w-150px mt-10 text-end position-absolute left-0 bottom-0 p-1 bg-white w-100">
                <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.store.settings.shipping.address.update', $address->id)}}','PATCH');" class="btn btn-primary w-100">Simpan</button>
            </div>
        </form>
    </div>
    </div>
    <!-- /canvas update -->

    @section('custom_js')
    <script>
        if ($("#province:selected")) {
            $.ajax({
                type: "POST",
                url: "{{route('web.regional.city')}}",
                data: {province : $("#province").val()},
                success: function(response){
                    $("#city").html(response);
                    Array.from($("#city").children()).forEach(element => {
                        if (element.value == $("#city_id_tmp").val()) {
                            element.selected = true
                        }
                    });

                    if ($("#city:selected")) {
                        $.ajax({
                            type: "POST",
                            url: "{{route('web.regional.subdistrict')}}",
                            data: {city : $("#city").val()},
                            success: function(response){
                                $("#subdistrict").html(response);
                                Array.from($("#subdistrict").children()).forEach(element => {
                                    // console.log(element.value == $("#subdistrict_id_tmp").val(), element.value, $("#subdistrict_id_tmp").val());
                                    if (element.value == $("#subdistrict_id_tmp").val()) {
                                        element.selected = true
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    </script>
    @endsection
    
</x-web-layout>