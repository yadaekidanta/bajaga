<x-web-layout title="Daftar Etalase" keyword="Bajaga Store">
    <div class="bg-white bg-dark-mode pb-5 pt-2 container-custom">
        <!-- heading -->
        <div class="px-3">
            <!-- search -->
            <form id="content_filter">
                <div class="position-relative w-100">
                    <input onkeyup="load_list(1)" type="text" class="form-control w-100 px-4" placeholder="Cari Etalase" name="keywords">
    
                    <!-- icon search -->
                    <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                        <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                            <g fill="none" fill-rule="evenodd">
                                <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                                <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                            </g>
                        </svg>
                    </div>
                    <!-- icon search -->
    
                </div>
            </form>
            <!-- search -->

            <button class="btn btn-outline-primary w-100" data-bs-toggle="offcanvas" data-bs-target="#canvasTambahEtalase" aria-controls="canvasTambahEtalase">Tambah Etalase</button>
        </div>
        <!-- /heading -->

        <!-- body -->
        <!-- list of product -->
        <hr>
        <div class="px-3">
            <div>
                <div id="list_result"></div>
            </div>
        </div>
        <!-- list of product -->
        <!-- body -->
    </div>

    <!-- canvas tambah etalase -->
    <div class="offcanvas offcanvas-bottom" tabindex="-1" id="canvasTambahEtalase" aria-labelledby="canvasTambahEtalaseLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Yuk, tambah etalase di tokomu</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <form id="form-submit" class="needs-validation" novalidate>
            @csrf
            <div class="offcanvas-body small position-relative">
                <input name="name" type="text" class="form-control w-100" aria-describedby="nameHelp" placeholder="Nama Etalase">
                @error('name')
                    <span class="text-danger text-sm">{{$message}}</span>
                @enderror
                <div id="nameHelp" class="form-text">Nama etalase yang sesuai kategori produk lebih mudah dicari pembeli</div>
            </div>
            <div class="position-absolute bottom-0 left-0 w-100 p-1 shadow">
                <button id="btn-submit" onclick="handle_upload('#btn-submit', '#form-submit', '{{route('web.store.settings.product.etalase.store')}}', 'POST', null, null, 'Simpan')" class="btn btn-primary w-100" type="submit">Simpan</button>
            </div>
        </form>
    </div>
    <!-- /canvas tambah etalase -->

    <!-- tinggal bikinin canvas di ubah harga dan stok -->

    @section('custom_js')
    <script>
        load_list(1);
        $(".decrement").on('click', function (event) {
            $target = $(event.target);
            let count_item = parseInt($target.next().html())
            if ( count_item > 1) {
                $target.next().html(count_item - 1)
            }
        });

        $(".increment").on('click', function (event) {
            $target = $(event.target);
            let count_item = parseInt($target.prev().html())
            $target.prev().html(count_item + 1)
        });
    </script>
    @endsection
    
</x-web-layout>

