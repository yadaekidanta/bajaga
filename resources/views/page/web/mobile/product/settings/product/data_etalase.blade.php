@foreach($collections as $collection)
    <div class="d-flex align-items-center justify-content-between mt-2">
        <a href="{{route('web.etalase', $item->id)}}?store_id={{Auth::user()->store()->id}}" class="d-flex col-10 align-items-center gap-3">
            <div class="w-10 h-10 border">
                <img src="{{asset('img/product/'.$collection->thumbnail)}}" alt="image etalase">
            </div>
            <span class="font-bold">{{$collection->name}}</span>
        </a>
        <div class="">
            <svg onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('web.store.settings.product.etalase.destroy', $collection->id)}}', true, null, null, null, 'dak-mode');" width="24" height="24" fill="#8f8f8f" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M19 24h-14c-1.104 0-2-.896-2-2v-17h-1v-2h6v-1.5c0-.827.673-1.5 1.5-1.5h5c.825 0 1.5.671 1.5 1.5v1.5h6v2h-1v17c0 1.104-.896 2-2 2zm0-19h-14v16.5c0 .276.224.5.5.5h13c.276 0 .5-.224.5-.5v-16.5zm-9 4c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6 0c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm-2-7h-4v1h4v-1z"/></svg>
        </div>
    </div>
    <hr class="m-0 mt-2">
@endforeach