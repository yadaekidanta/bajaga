@foreach($collections as $product)
<div class="mt-2">
    <div class="d-flex gap-2">
        <div class="d-flex justify-content-center align-items-center w-20 h-20 rounded border">
            <img src="{{asset('storage/'.$product->photo)}}" class="object-fit-cover max-w-100 h-100" alt="Image Produk">
        </div>
        <div class="d-flex flex-column">
            <span class="font-bold font-lg">{{$product->name}}</span>
            <span class="font-thin">{{number_format($product->price)}}</span>
            <div class="d-flex align-items-center gap-2">
                <span class="text-sm">Stok: {{$product->stock}}</span>
            </div>
        </div>
    </div>

    <div class="d-flex align-items-center mt-2 gap-2">
        <input type="hidden" value="{{$product->qty}}">
        <input type="hidden" value="{{$product->stock}}">
        <input type="hidden" value="{{$product->id}}">
        <input type="hidden" value="{{$product->price}}">
        <button class="btn btn-outline-primary btn-ubah-harga btn-sm w-100"  data-bs-toggle="offcanvas" data-bs-target="#canvasUbahHarga" aria-controls="canvasUbahHarga">Ubah Harga</button>
        <button class="btn btn-outline-primary btn-ubah-stok btn-sm w-100"  data-bs-toggle="offcanvas" data-bs-target="#canvasUbahStok" aria-controls="canvasUbahStok">Ubah Stok</button>
    </div>
</div>
@endforeach

@if (count($collections) > 0)
    <script>
        

        $(".btn-ubah-harga").click(function() {
            let el = $(this)
            let price = el.prev().val()
            let id_product = el.prev().prev().val()
            $("#price").val(formatRupiah(price))
            $("#id_product").val(id_product)
        })

        // canvasUbahHarga

        $(".btn-ubah-stok").click(function() {
            let el = $(this)
            let stock = el.prev().prev().prev().prev().val()
            let qty = el.prev().prev().prev().prev().prev().val()
            let id_product = el.prev().prev().prev().val()
            $("#stok_count").text(stock)
            $("#stok_input").val(stock)
            $("#id_product_to_stock").val(id_product)
            $("#qty").val(qty)
        })
    </script>
    
@endif
