<x-web-layout title="Daftar Produk" keyword="Bajaga Store">
    <div class="bg-white bg-dark-mode pt-2 pb-5 container-custom">
        <!-- heading -->
        <div class="px-3">
            <!-- search -->
            <form id="content_filter">
                <div class="position-relative w-100">
                    <input type="text" onkeyup="load_list(1)" class="form-control w-100 px-4" name="keywords" placeholder="Cari Produk">
                    <!-- icon search -->
                    <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                        <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                            <g fill="none" fill-rule="evenodd">
                                <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                                <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                            </g>
                        </svg>
                    </div>
                    <!-- icon search -->
                </div>
            </form>
            <!-- search -->
        </div>
        <!-- /heading -->

        <!-- body -->
        <!-- list of product -->
        <hr>

        <div class="px-3">
            <div id="list_result"></div>
            <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                <div class="spinner-grow text-secondary" role="status">
                </div>
            </div>
        </div>
        <!-- list of product -->
        <!-- body -->
    </div>

    <!-- canvas filter -->
    <div class="offcanvas offcanvas-bottom h-90" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small position-relative">
            <!-- filter sort -->
            <div>
                <h3 class="font-bold m-0">Urutkan</h3>
                <div class="d-flex align-item-center gap-2 mt-1 container-sort-filter">
                    <span class="px-2 py-1 d-flex align-item-center justify-center border border-primary text-primary rounded-pill">
                        Terbaru
                    </span>
                    <span class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                        Harga Tertinggi
                    </span>
                    <span class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                        Harga Terendah
                    </span>
                </div>
            </div>
            <!-- /filter sort -->

            <!-- filter price -->
            <div class="mt-2">
                <h3 class="font-bold">Urutkan</h3>
                <div class="d-flex align-items-center gap-2">
                    <div class="position-relative">
                        <input type="text" name="minimum" class="form-control minimum-filter ps-4" placeholder="Terendah">
                        <div class="position-absolute d-flex align-items-center justify-content-center left-1 top-0 h-100 ps-1">
                            <span class="font-bold">Rp</span>
                        </div>
                    </div>
                    <div class="position-relative">
                        <input type="text" class="form-control maximum-filter ps-4" placeholder="Terendah">
                        <div class="position-absolute d-flex align-items-center justify-content-center left-1 top-0 h-100 ps-1">
                            <span class="font-bold">Rp</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /filter price -->

            <!-- filter Categories -->
            <div class="mt-3">
                <h3 class="font-bold m-0">Kategori</h3>
                <div class="d-flex align-item-center gap-2 mt-1 container-categories">
                    <span class="px-2 py-1 d-flex align-item-center justify-center border  rounded-pill">
                        Aksesoris
                    </span>
                    <span class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                        CD Game
                    </span>
                </div>
            </div>
            <!-- /filter Categories -->
            
        </div>
        <div class="position-absolute bottom-0 left-0 w-100 p-1 shadow">
            <button class="btn btn-primary w-100">Tampilkan 550 Produk</button>
        </div>
    </div>
    <!-- /canvas filter -->

    <!-- canvas ubah harga -->
    <div class="offcanvas offcanvas-bottom h-29vh" tabindex="-1" id="canvasUbahHarga" aria-labelledby="canvasUbahHargaLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Atur Harga</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <form id="form_input">
            <div class="offcanvas-body small position-relative">
                <div class="position-relative">
                    <input type="hidden" name="id_product" id="id_product">
                    <input id="price" type="text" name="price" class="form-control maximum-filter ps-4 w-100" placeholder="Min. Rp 100">
                    <div class="position-absolute d-flex align-items-center justify-content-center left-1 top-0 h-100 ps-1">
                        <span class="font-bold">Rp</span>
                    </div>
                </div>
                @error('price')
                    <span class="text-danger text-sm">{{$message}}</span>
                @enderror
            </div>
            <div class="position-absolute bottom-0 left-0 w-100 p-1 shadow">
                <button id="btn_submit" onclick="ajaxUpdate('#btn_submit', '#form_input', '{{route('web.store.settings.product.price')}}', 'PATCH', 'price')" class="btn btn-primary w-100" type="submit">Simpan</button>
            </div>
        </form>
    </div>
    <!-- /canvas ubah harga -->

    <!-- canvas ubah stok -->
    <div class="offcanvas offcanvas-bottom h-29vh" tabindex="-1" id="canvasUbahStok" aria-labelledby="canvasUbahStokLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <form id="form_input_stock">
            <div class="offcanvas-body small position-relative">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-lg">Jumlah Stok</span>
                    <input type="hidden" id="stok_input" name="stock"> 
                    <input type="hidden" id="qty"> <!-- tidak untuk di store -->
                    <input type="hidden" id="id_product_to_stock" name="id_product"> 
                    <!-- counter item -->
                    <div class="d-flex justify-content-between px-1 align-items-center gap-3 border border-dark rounded">
                        <i class="icon-line-minus decrement font-lg"></i>
                        <span class="font-lg" id="stok_count">1</span>
                        <i class="icon-line-plus increment font-lg"></i>
                    </div>
                    <!-- counter item -->
                </div>
                @error('stok')
                    <span class="text-danger text-sm">{{$message}}</span>
                @enderror
            </div>
            <div class="position-absolute bottom-0 left-0 w-100 p-1 shadow">
                <button class="btn btn-primary w-100" type="submit" id="btn_submit_stock" onclick="ajaxUpdate('#btn_submit_stock', '#form_input_stock', '{{route('web.store.settings.product.stock')}}', 'PATCH', 'stock')">Simpan</button>
            </div>
        </form>
    </div>
    <!-- /canvas ubah stok -->

    @section('custom_js')
    <script>
        localStorage.setItem("page_infinate", 1)
        load_list(1)

        $(".decrement").on('click', function (event) {
            $target = $(event.target);
            let count_item = parseInt($target.next().html())
            if ( count_item > 1) {
                $target.next().html(count_item - 1)
            }
            $("#stok_input").val(parseInt($("#stok_count").text()))
        });

        $(".increment").on('click', function (event) {
            $target = $(event.target);
            let count_item = parseInt($target.prev().html())
            let qty = $("#qty").val();
            if (count_item > qty) {
                $target.prev().html(count_item + 1)
                $("#stok_input").val(parseInt($("#stok_count").text()))
            }
        });

        function ajaxUpdate(tombol, form, url, method, state){
            $(tombol).submit(function() {
                return false;
            });
            
            let data = $(form).serialize();
            
            $(tombol).prop("disabled", true);
            
            // console.log(data);
            $.ajax({
                type: method,
                url: url,
                data: data,
                dataType: 'json',
                processData: false,
                beforeSend: function() {
                    if (state == "price") {
                        let splitData = this.data.split('&price=')
                        let bid = splitData[1].replaceAll('.', '')
                        this.data = `${splitData[0]}&price=${bid}`
                    }
                },
                success: function(response) {
                        console.log(response);
                        if (response.alert == "success") {
                            Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                            $(tombol).prop("disabled", false);
                            load_list(1)
                        } else {
                            Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                            $(tombol).prop("disabled", false);
                        }
                    },

                error: function (response) {
                    Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    $(tombol).prop("disabled", false);
                }
            });
        }

        
        $(document).ready(function(){
            let options = {
                root: null,
                rootMargin: '10px',
                threshold: 0.5
            }

            const observer = new IntersectionObserver(handleIntersect, options)
            observer.observe(document.querySelector(".ajax-load"))
        })
        
        function handleIntersect(entries){
            if (entries[0].isIntersecting) {
                // alert()
                localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                load_more(parseInt(localStorage.getItem("page_infinate")));
            }
        }
    </script>
    @endsection
    
</x-web-layout>

