<x-web-layout title="Detail Produk" keyword="Bajaga Store">
    <div style="margin-top: -1.6em;">
        <div id="carousel" class="carousel slide" data-bs-ride="carousel">
            <input type="hidden" id="id_product" value="{{$product->id}}">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active carousel-image-custom">
                    <img src="{{$product->image}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item carousel-image-custom">
                    <img src="{{$product->image}}" class="d-block w-100" alt="...">
                </div>
            </div>
        </div>

        <div class="px-3 bg-white py-2">
            <div class="d-flex justify-content-between align-items-center">
                <!-- Product Single - Price
                ============================================= -->
                @if ($varian->count() > 0)
                <div class="font-xl font-bold">Rp. {{number_format($varian_low->price)}}
                    @if($varian_high->id != $varian_low->id)
                    - {{number_format($varian_high->price)}}
                    @endif
                </div><!-- Product Single - Price End -->
                @else
                <div class="font-xl font-bold">Rp. {{number_format($product->price)}}</div><!-- Product Single - Price End -->
                @endif

                <form id="form_wishlist" class="m-0">
                    <input type="number" class="d-none" value="{{$product->id}}" name="id_product">
                    <!-- icon love whitelist -->
                    <!-- no-fill -->
                    <svg role="button" class="whitelist-ouline {{ $wishlist ? 'd-none' : '' }}" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 21.593c-5.63-5.539-11-10.297-11-14.402 0-3.791 3.068-5.191 5.281-5.191 1.312 0 4.151.501 5.719 4.457 1.59-3.968 4.464-4.447 5.726-4.447 2.54 0 5.274 1.621 5.274 5.181 0 4.069-5.136 8.625-11 14.402m5.726-20.583c-2.203 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248-3.183 0-6.281 2.187-6.281 6.191 0 4.661 5.571 9.429 12 15.809 6.43-6.38 12-11.148 12-15.809 0-4.011-3.095-6.181-6.274-6.181"/></svg>
                    <!-- /no-fill -->
    
                    <!-- fill -->
                    <svg role="button" class="whitelist-fill {{ $wishlist ? '' : 'd-none' }}" fill="red" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>
                    <!-- fill -->
                    <!-- /icon love whitelist -->
                </form>
            </div>

            <span class="mt-3">{{$product->name}}</span>
            <!-- <div class="px-1 py-1 d-flex align-items-center gap-2 rounded border mt-3" style="width: fit-content;">
                <i class="icon-star3 text-warning"></i>
                <span>0.0</span>
            </div> -->
        </div>

        <!-- varian -->
        <!-- <div class="px-3 bg-white py-3 mt-2">
            <div class="d-flex align-item-center gap-3">
                <b class="font-bold">Pilih varian:</b>
                <span>3 warna</span>
            </div>

            <div class="d-flex align-item-center gap-2 mt-1 container-varians">
                <span class="px-2 py-1 d-flex align-item-center justify-center border border-primary text-primary rounded-pill">
                    Neon biru merah
                </span>
                <span class="px-2 py-1 d-flex align-item-center justify-center border rounded-pill">
                    Abu-abu
                </span>
            </div>
        </div> -->
        <!-- / varian -->


        <div class="px-3 bg-white py-3 mt-2">
            <div class="d-flex justify-content-between">
                <a href="{{route('web.store', $product->product_store->id)}}" class="d-flex gap-3">
                    <div class="image-toko">
                        <img src="{{asset( $product->photo != null ? 'storage/'.$product->photo : 'img/store/default.png')}}" alt="gambar toko">
                    </div>
    
                    <div class="d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center gap-2">
                            <img class="logo-sm" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                            <h4 class="font-bold m-0">{{$product->product_store->name}}</h4>
                        </div>
                        <span class="m-0 text-dark">{{$product->product_store->city->name}}</span>
                        <!-- rating toko -->
                        <div class="d-flex align-items-center gap-2 mt-1">
                            <i class="icon-star3 text-emas"></i>
                            <span class="m-0 text-dark">{{floatval($rating)}}</span>
                            <span class="font-sm text-dark">rata-rata ulasan</span>
                        </div>
                        <!-- /rating toko -->
                    </div>
                </a>

                <a href="{{route('web.chat.detail', $product->product_store->user->id)}}" class="btn btn-outline-primary btn-sm">
                    Chat
                </a>
            </div>


        </div>

        <div class="px-3 bg-white py-3 mt-2">
            <h4 class="font-bold m-0">Detail Produk</h4>
            <div class="mt-1">
                {{$product->name}}
            </div>

            <div class="mt-1">
                <div class="row p-1">
                    <span class="col-5 p-0">Kondisi</span>
                    <span class="col">{{$product->condition}}</span>
                    <hr class="m-0">
                </div>
                <div class="row p-1">
                    <span class="col-5 p-0">Stok</span>
                    <span class="col">{{$product->stock}}</span>
                    <hr class="m-0">
                </div>
                <div class="row p-1">
                    <span class="col-5 p-0">Berat</span>
                    <span class="col">{{$product->weight}}</span>
                    <hr class="m-0">
                </div>
                <div class="row p-1">
                    <span class="col-5 p-0">Kategori</span>
                    <span class="col">{{$product->product_category->title}}</span>
                    <hr class="m-0">
                </div>
            </div>

            <div class="mt-2">
                <h4 class="font-bold m-0">Detail Produk</h4>
                <span class="mt-2">{{$product->desc}}</span>
            </div>
        </div>

        <div class="bg-white py-3 mt-2">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item px-2" role="presentation">
                    <button class="nav-link active font-bold" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">
                        Ulasan 
                        <span class="badge ms-1 badge-primary">{{$review_count <= 9 ? $review_count : "9+"}}</span></button>
                </li>
                <li class="nav-item px-2" role="presentation">
                    <button class="nav-link font-bold" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Diskusi</button>
                </li>
            </ul>
            <div class="tab-content w-100 h-100" id="myTabContent">
                <div class="tab-pane fade show active px-3 mt-3 w-100 h-100" id="home" role="tabpanel" aria-labelledby="home-tab">
                    @if($review_count > 0)
                        @foreach($review as $rvw)
                            @if (!$rvw->to_id)
                                <div class="d-flex gap-3">
                                    <!-- img user -->
                                    <div class="w-10 h-10 border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                                        <img src="{{asset($rvw->user_review->avatar ? 'storage/'. $rvw->user_review->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
                                    </div>
                                    <!-- img user -->
                    
                                    <div class="w-90%">
                                        <span class="font-bold">{{$rvw->user_review->name}}</span>
                                        <div class="d-flex align-item-center">
                                            @for ($i = 0; $i < intval($rating); $i++)
                                                <i class="icon-star3 text-emas"></i>
                                            @endfor
                                            @for ($i = intval($rating); $i < 5; $i++)
                                                <i class="icon-star-empty"></i>
                                            @endfor
                                            
                                        </div>
                                        <span class="d-block p-1 bg-comment text-light rounded w-100 bg-gray font-sm text-break">{{$rvw->review}}</span>
                                    </div>
                                </div>
                            @else
                                <div class="w-100 d-flex gap-3 justify-content-end mt-3">
                                    <div class="d-flex gap-3" style="width: 80%">
                                        <!-- img user -->
                                        <div class="w-10 h-10 border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                                            <img class="w-100 h-100 object-fit-cover" src="{{asset($product->product_store->photo ? 'storage/'. $product->product_store->photo : 'storage/avatars/avatar1.png')}}" alt="image user">
                                        </div>
                                        <!-- img user -->
                        
                                        <div class="w-100">
                                            <span class="font-bold">{{$product->product_store->name}}</span>
                                            <div class="badge bg-primary">Penjual</div>
                                            <span class="d-block p-1 bg-comment text-light rounded w-100 bg-gray font-sm text-break">{{$rvw->review}}</span>
                                        </div>
                                    </div>
                                </div>
                                
                            @endif
                            <hr>
                        @endforeach
                    @else
                        <div class="d-flex flex-column justify-content-center align-items-center w-100 h-100">
                            <span class="text-muted text-center">Saatnya belanja dan kasih ulasan, yuk!</span>
                        </div>
                    @endif

                </div>
                <div class="tab-pane fade px-3 mt-3 w-100 h-100" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    
                    <div class="d-flex flex-column justify-content-center align-items-center w-100 h-100">
                        <span class="text-muted text-center">Saatnya kamu belanja dan kasih ulasanmu ke jutaan orang!</span>
                    </div>

                </div>
            </div>
        </div>

        <div class="px-3 bg-white py-3 mt-2 mb-3">
            <div class="d-flex justify-content-between align-items-center">
                <h2 class="font-lg m-0">Produk pilihan untukmu</h2>
                <a href="{{route('web.catalog.products')}}" class="link">Lihat semua</a>
            </div>

            <div class="section-slide mt-2 pe-2">
                @foreach($product_pilihan as $p)
                <div class="container-card text-dark shadow">
                    <div class="container-img">
                        <a href="{{route('web.product.show', $p->slug)}}">
                            <img src="{{asset('storage/'.$p->photo)}}" alt="">
                        </a>
                    </div>
                    <div class="p-1 description-product">
                        <div class="d-flex flex-column">
                            <a href="{{route('web.product.show', $p->slug)}}" class="font-sm 
                            text-capitalize line-normal text-dark "><span class="act-dark">{{Str::limit($p->name, 18)}}</span></a>
                            <a href="{{route('web.product.show', $p->slug)}}" class="font-medium text-dark font-bold">
                                <span class="act-dark">
                                    Rp {{number_format($p->price)}}</a>
                                </span>
                            <a href="{{route('web.store', $p->product_store->id)}}" class="text-dark d-flex mt-2 gap-1">
                                    <img class="logo-sm m-0" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                    <span style="margin-top: -5px !important;">
                                        <span class="text-capitalize line-normal">{{$p->product_store->name}}</span>
                                        <span class="font-sm">{{\Str::limit($p->product_store->city->name, 14)}}</span>
                                    </span>
                            </a>
                        </div>
    
                    </div>
    
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- form to add cart -->
    <form class="d-none" id="form_cart">
        <input type="hidden" id="product_id" name="product" value="{{$product->id}}">
        <input type="hidden" id="store_id" name="store" value="{{$product->store_id}}">
        <input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="qty d-none" />
    </form>
    <!-- form to add cart -->

    @section('custom_js')
    <script>
        $(".whitelist-ouline").click(function(){

            let data = $("#form_wishlist").serialize();
            $.ajax({
                type: 'POST',
                url: "{{route('web.wishlist.store')}}",
                data: data,
                success: function(response){
                    $(".whitelist-ouline").addClass('d-none');
                    $(".whitelist-fill").removeClass('d-none');
                },
                error: response => {
                    console.log(response);
                }
            })
        })
        $(".whitelist-fill").click(function(){
            // alert('sad')
            let data = $("#form_wishlist").serialize();
            $.ajax({
                type: 'DELETE',
                url: "{{route('web.wishlist.destroy', $product->id)}}",
                success: function(response){
                    $(".whitelist-fill").addClass('d-none');
                    $(".whitelist-ouline").removeClass('d-none');
                },
                error: response => {
                    console.log(response);
                }
            })
        })

        function beli(tombol, url, method){
            
            let item = $("#id_product").val()
            // console.log(JSON.stringify(item));
            $(tombol).submit(function() {
                return false;
            });

            $(tombol).html(`<div class="spinner-border spinner-border-sm text-primary spinner" role="status"></div>`)
            
            $(tombol).prop("disabled", true);

            $.ajax({
                type: method,
                url: url,
                data: {data: item},
                dataType: 'json',
                error: function(response) {
                    Swal.fire({ 
                        text: response.responseJSON.message,
                        icon: "error", 
                        showDenyButton: true,
                        showCancelButton: false,
                        confirmButtonText: "Tambahkan",
                        denyButtonText: "Tidak"})
                        .then(result => {
                            if (result.isConfirmed) {
                                location.href = "{{route('web.user-address.create')}}"
                            }
                        })
                        $(tombol).submit(function() {
                            return true;
                        });

                        // console.log(arrayItems);
                        
                        $(tombol).prop("disabled", false);
                        $(tombol).html(`Beli langsung`)
                },
                success: function(response){
                    // console.log(response);
                    if (response.alert == "success") {
                        location.href = response.redirect 
                    }
                    $(tombol).prop("disabled", false);
                    $(tombol).html(`Beli langsung`)
                    toggleLoading($(tombol), false)
                }
            });
        }

        // $(".container-varians").on('click', function (event) {
        //     $target = $(event.target);
        //     $('.container-varians > span').removeClass('border-primary text-primary')
        //     $target.addClass('border-primary text-primary');
        // });
        
    </script>
    @endsection
</x-web-layout>