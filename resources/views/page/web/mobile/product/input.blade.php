<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0">
                <div class="card-title d-flex justify-content-between align-items-center">
                    <h4 class="m-0">
                        @if($product->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Produk
                    </h4>
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-6 mt-3">
                            <label class="required fs-6 fw-bold mb-1">Nama</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$product->name}}">
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label class="required fs-6 fw-bold mb-1">Harga</label>
                            <input type="text" class="form-control" name="price" id="price" placeholder="Masukkan Harga..." value="{{$product->price}}">
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="condition" class="required fs-6 fw-bold mb-1">Stok</label>
                            <input type="tel" class="form-control" id="stok" name="stok" placeholder="Masukkan Jumlah Peringatan..." value="{{$product->stock}}">
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="condition" class="required fs-6 fw-bold mb-1">Jumlah Peringatan</label>
                            <input type="text" class="form-control" name="alert_quantity" placeholder="Masukkan Jumlah Peringatan..." value="{{$product->alert_quantity}}">
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label class="required fs-6 fw-bold mb-1">Kondisi</label>
                            <select data-control="select2" data-placeholder="Pilih Kondisi" name="condition" class="form-select form-select-solid">
                                <option SELECTED DISABLED>Pilih Kondisi</option>
                                <option value="baru"{{$product->condition=="baru"?"selected":""}}>Baru</option>
                                <option value="bekas"{{$product->condition=="bekas"?"selected":""}}>Bekas</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 mt-3">
                            <label for="unit" class="required fs-6 fw-bold mb-1">Satuan</label>
                            <div class="input-group">
                                <select data-control="select2" data-placeholder="Pilih Satuan" id="unit_id" name="product_unit_id" class="form-select form-select-solid">
                                    <option value="" SELECTED DISABLED >Pilih Satuan</option>
                                    @foreach ($satuan as $item)
                                    <option value="{{$item->id}}"{{$item->id==$product->product_unit_id?"selected":""}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="unit" class="required fs-6 fw-bold mb-1">Merek</label>
                            <div class="input-group">
                                <select data-control="select2" data-placeholder="Pilih Merek" id="brand_id" name="product_brand_id" class="form-select form-select-solid">
                                    <option value="" SELECTED DISABLED >Pilih Merek</option>
                                    @foreach ($merek as $item)
                                    <option value="{{$item->id}}"{{$item->id==$product->product_brand_id?"selected":""}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="unit" class="required fs-6 fw-bold mb-1">Garansi</label>
                            <select data-control="select2" data-placeholder="Pilih Garansi" id="warranty_id" name="product_warranty_id" class="form-select form-select-solid">
                                <option value="" SELECTED DISABLED >Pilih Merek</option>
                                @foreach ($garansi as $item)
                                <option value="{{$item->id}}"{{$item->id==$product->product_warranty_id?"selected":""}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="unit" class="required fs-6 fw-bold mb-1">Kategori</label>
                            <div class="input-group">
                                <select data-control="select2" data-placeholder="Pilih Kategori" id="category_id" name="product_category_id" class="form-select form-select-solid">
                                    <option value="" SELECTED DISABLED>Pilih Kategori</option>
                                    @foreach ($kategori as $item)
                                        @if($item->product_category_id == 0)
                                            <option value="{{$item->id}}"{{$item->id==$product->product_category_id?"selected":""}}>{{$item->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="subcategory_id" class="required fs-6 fw-bold mb-1">Sub Kategori</label>
                            <select data-control="select2" data-placeholder="Pilih Kategori terlebih dahulu" id="subcategory_id" name="product_subcategory_id" class="form-select form-select-solid">
                                <option value="" SELECTED DISABLED>Pilih Sub Kategori</option>
                                @foreach ($kategori as $item)
                                    @if($item->product_category_id > 0)
                                        <option value="{{$item->id}}"{{$item->id==$product->product_subcategory_id?"selected":""}}>{{$item->title}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="user_display_id" class="required fs-6 fw-bold mb-1">Etalase</label>
                            <select data-control="select2" data-placeholder="Pilih Etalase terlebih dahulu" id="user_display_id" name="user_display_id" class="form-select form-select-solid">
                                <option value="" SELECTED DISABLED>Pilih Etalase</option>
                                @foreach ($etalase as $item)
                                    <option value="{{$item->id}}"{{$item->id == $product->user_display_id?"selected":""}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="condition" class="required fs-6 fw-bold mb-1">Deskripsi Produk</label>
                            <textarea name="desc" cols="30" rows="5" class="form-control">{{$product->desc}}</textarea>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <label for="condition" class="required fs-6 fw-bold mb-1">Berat Produk</label>
                            <input type="text" class="form-control" name="weight" placeholder="Masukkan Berat..." value="{{$product->weight}}">
                        </div>
                        @if($product->image)
                        <div class="col-lg-6 mt-3">
                            <label for="condition" class="required fs-6 fw-bold mb-1">Gambar Produk</label>
                            <br>
                            <div class="overflow-hidden w-21.2em">
                                <img class="w-100 h-100 object-fit-cover" id="preview_avatar" src="{{$product->image}}" alt="">
                            </div>
                            <div class="">
                                <input type="file" id="image" class="form-control" name="photo" placeholder="Gambar Produk...." value="{{$product->photo}}">
                            </div>
                        </div>
                        @else
                        <div class="col-lg-6 mt-3">
                            <label for="condition" class="required fs-6 fw-bold mb-1">Gambar Produk</label>
                            <input type="file" class="form-control" name="photo" placeholder="Gambar Produk...." value="{{$product->photo}}">
                        </div>
                        @endif
                        <div class="col-lg-6 mt-3">
                            <label for="condition" class="required fs-6 fw-bold mb-1">Video Produk</label>
                            <input type="text" class="form-control" name="video_url" placeholder="URL Video Produk...." value="{{$product->video_url}}">
                        </div>
                        <div class="w-100 mt-5 text-end">
                            @if ($product->id)
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.product.update',$product->id)}}','PATCH');" class="btn btn-primary w-100">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.product.store')}}','POST');" class="btn btn-primary w-100">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    @if($product->id)
    $("#category_id").val('{{$product->product_category_id}}').trigger('change');
    @endif
    obj_select('unit_id','Pilih Satuan');
    obj_select('brand_id','Pilih Merek');
    obj_select('category_id','Pilih Kategori');
    obj_select("subcategory_id","Pilih Sub Kategori");
    obj_select("user_display_id","Pilih Etalase");
    @if($product->product_category_id)
    $("#category_id").select2().select2("val", '{{$product->product_category_id}}');
    setTimeout(function(){ 
        $('#category_id').trigger('change');
        setTimeout(function(){ 
            $('#subcategory_id').val('{{$product->product_subcategory_id}}');
            $('#subcategory_id').trigger('change');
        }, 2000);
    }, 1000);
    @endif
    $("#category_id").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.product-category.get_list_sub')}}",
            data: {category : $("#category_id").val()},
            success: function(response){
                $("#subcategory_id").html(response);
            }
        });
    });
    number_only('price');

    $("#image").change(function(){
        let filesAmount = this.files.length
        let reader = new FileReader();
        reader.onload = (e) => { 
            $('img#preview_avatar').attr('src', e.target.result); 
        }

        reader.readAsDataURL(this.files[0]); 
    })
</script>
