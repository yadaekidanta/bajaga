<x-web-layout title="Detail Pesanan" keyword="Bajaga Store">
    <div class="container-custom">
        <div class="py-3 bg-white px-3">
            <div class="d-flex justify-content-between align-items-center">
                <span class="font-bold text-capitalize">{{$sale->st}}</span>
                <a href="{{route('web.shipping.detail', $sale->id)}}" class="font-bold">Lihat Detail</a>
            </div>
            
            <hr class="my-2">

            @if($sale->st != "Tertunda" && $sale->payment_st == "Lunas" && $sale->st != "Dipesan")
                <div class="d-flex justify-content-between align-items-center mt-1">
                    <div class="d-flex gap-2 align-items-center">
                        <span>{{$sale->code}}</span>
                        <svg class="unf-icon" viewBox="0 0 24 24" width="16" height="16" fill="var(--color-icon-enabled, #2E3137)" style="display: inline-block; vertical-align: middle;"><path fill-rule="evenodd" clip-rule="evenodd" d="M16.53 2.466a.76.76 0 0 1 .22.534.75.75 0 0 1-.75.75H6c-.19 0-.25.07-.25.25v14a.75.75 0 1 1-1.5 0V4A1.7 1.7 0 0 1 6 2.24h10c.2.003.39.084.53.226ZM9 5.24h9A1.7 1.7 0 0 1 19.75 7v13A1.69 1.69 0 0 1 18 21.74H9A1.69 1.69 0 0 1 7.25 20V7A1.7 1.7 0 0 1 9 5.24Zm9 15.01c.19 0 .25-.06.25-.25V7c0-.18-.06-.25-.25-.25H9c-.19 0-.25.07-.25.25v13c0 .19.06.25.25.25h9ZM16 9.24h-5a.75.75 0 1 0 0 1.5h5a.75.75 0 1 0 0-1.5Zm-5 4h3a.75.75 0 1 1 0 1.5h-3a.75.75 0 1 1 0-1.5Z"></path></svg>
                    </div>
                    <a href="{{route('web.invoice')}}?id_invoice={{$sale->code}}" class="font-bold ">Lihat Invoice</a>
                </div>
            @endif

            <div class="d-flex justify-content-between align-items-center mt-1">
                <span >Tanggal Pembelian</span>
                <!-- <span >01 Januari 2021, 12:58 WIB</span> -->
                <span>{{$sale->date}}</span>
            </div>

        </div>

        <div class="py-3 bg-white px-3 mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <span class="font-bold">Detail Produk</span>
                <a href="{{route('web.store', $sale->store->id)}}" class="font-bold d-flex align-items-center gap-2">
                    <span>{{$sale->store->name}}</span>
                    <svg class="unf-icon" viewBox="0 0 24 24" width="16" height="16" fill="var(--color-icon-enabled, #2E3137)" style="display: inline-block; vertical-align: middle;"><path d="M9.5 17.75a.75.75 0 0 1-.5-1.28L13.44 12 9 7.53a.75.75 0 0 1 1-1.06l5 5a.75.75 0 0 1 0 1.06l-5 5a.74.74 0 0 1-.5.22Z"></path></svg>
                </a>
            </div>

                @php
                    $price = 0;
                @endphp
            @foreach($sale->sale_detail()->get() as $saleDetail)
                @php
                    $price += $saleDetail->price;
                @endphp
                <div class="card border p-2 mt-1">
                    <div class="d-flex align-items-center gap-3">
                        <div class="w-10 h-10 border overflow-hidden">
                            <img src="{{asset('storage/'.$saleDetail->product->photo)}}" alt="image product" class="object-fit-cover w-100 h-100">
                        </div>
                        <div>
                            <h4 class="m-0 font-bold">{{$saleDetail->product->name}}</h4>
                            <span class="font-sm -mt-0.8">{{$saleDetail->qty}} x Rp{{number_format($saleDetail->price)}}</span>
                        </div>
                    </div>
                    <hr class="my-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="">
                            <span class="font-sm">Total Belanja</span>
                            <span class="d-block">
                                @if($saleDetail->disc_price)
                                    <span class="font-bold font-sm -mt-1">
                                        <del>
                                            Rp{{number_format($saleDetail->subtotal)}}
                                        </del>
                                    </span>
                                    <span class="font-bold font-sm -mt-1">
                                        <mark>
                                            Rp{{number_format(($saleDetail->subtotal - $saleDetail->disc_price))}}
                                        </mark>
                                    </span>
                                @else
                                    <span class="font-bold font-sm -mt-1">
                                        Rp{{number_format($saleDetail->subtotal)}}
                                    </span>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="w-100 d-flex justify-content-end mt-2">
                @if($sale->st == 'Selesai' && $sale->store_id != Auth::user()->store->id)
                    <a href="{{route('web.product.show', $saleDetail->product->slug)}}" class="btn w-100 btn-primary">Beli Lagi</a>
                @elseif($sale->payment_st == "Lunas" && $sale->st == "Dikirim")
                    <a href="{{route('web.shipping.detail', $sale->id)}}" class="btn btn-outline-primary w-100">Lacak</a>
                @elseif($sale->payment_st == "Belum lunas" && $sale->store_id != Auth::user()->store->id)
                    <button class="btn btn-primary w-100">Bayar Sekarang</button>
                @elseif($sale->st == "Diterima" && $sale->store_id != Auth::user()->store->id)
                    <button class="btn btn-primary w-100" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasUlasan" aria-controls="offcanvasUlasan">Beri Ulasan</button>
                @else
                    @if($sale->store_id != Auth::user()->store->id)
                        <a href="{{route('web.chat.detail', $sale->store->user->id)}}"  class="btn btn-outline-primary w-100">Chat</a>    
                    @endif
                @endif
            </div>

        </div>

        <div class="py-3 bg-white px-3 mt-2">
            <span class="font-bold">Info Pengiriman</span>
            @if($sale->st == 'Dikirim')
                <div class="d-flex align-items-center mt-1">
                    <span class="text-secondary w-28">Kurir</span>
                    <span>{{$sale->courier}}</span>
                </div>
                <div class="d-flex align-items-center">
                    <div class="text-secondary w-28 d-flex align-items-center gap-2">
                        <span>No Resi</span>
                        <svg class="unf-icon" viewBox="0 0 24 24" width="16" height="16" fill="var(--color-icon-enabled, #2E3137)" style="display: inline-block; vertical-align: middle;"><path fill-rule="evenodd" clip-rule="evenodd" d="M16.53 2.466a.76.76 0 0 1 .22.534.75.75 0 0 1-.75.75H6c-.19 0-.25.07-.25.25v14a.75.75 0 1 1-1.5 0V4A1.7 1.7 0 0 1 6 2.24h10c.2.003.39.084.53.226ZM9 5.24h9A1.7 1.7 0 0 1 19.75 7v13A1.69 1.69 0 0 1 18 21.74H9A1.69 1.69 0 0 1 7.25 20V7A1.7 1.7 0 0 1 9 5.24Zm9 15.01c.19 0 .25-.06.25-.25V7c0-.18-.06-.25-.25-.25H9c-.19 0-.25.07-.25.25v13c0 .19.06.25.25.25h9ZM16 9.24h-5a.75.75 0 1 0 0 1.5h5a.75.75 0 1 0 0-1.5Zm-5 4h3a.75.75 0 1 1 0 1.5h-3a.75.75 0 1 1 0-1.5Z"></path></svg>
                    </div>
                    <span>{{$sale->no_resi}}</span>
                </div>
            @endif
            <div class="d-flex align-items-start">
                <div class="text-secondary w-28 d-flex align-items-center gap-2">
                    <span>Alamat</span>
                </div>
                <div class="col">
                    <p class="font-bold m-0">{{$sale->customer_name}}</p>
                    <p class="m-0">{{$sale->customer_phone}}</p>
                    <p class="m-0">{{$sale->shipping_detail}}</p>
                </div>
                    
            </div>
        </div>

        <div class="py-3 bg-white px-3 mt-2">
            <span class="font-bold">Rincian Pembayaran</span>
            <div class="d-flex align-items-center justify-content-between mt-1">
                <span class="text-secondary">Metode Pembayaran</span>
                <span class="font-sm">{{$sale->payment_method}}({{$sale->payment_channel}})</span>
            </div>
            <hr class="my-2">
            <div class="d-flex align-items-center justify-content-between">
                <span class="text-secondary">Total Harga ({{count($sale->sale_detail()->get())}} Barang)</span>
                <span>Rp{{number_format($price)}}</span>
            </div>
            <div class="d-flex align-items-center justify-content-between">
                <span class="text-secondary">Total Ongkos Kirim</span>
                <span>Rp{{number_format($sale->shipping_price)}}</span>
            </div>
            @if($sale->disc_price)
            <div class="d-flex align-items-center justify-content-between">
                <span class="text-secondary">Diskon</span>
                <span>- Rp{{number_format($sale->disc_price)}}</span>
            </div>
            @endif
            <hr class="my-2">
            <div class="d-flex align-items-center justify-content-between">
                <span class="font-bold">Total Belanja</span>
                <span>Rp{{number_format($sale->grand_total)}}</span>
            </div>

        </div>
    </div> 

    <div class="offcanvas offcanvas-bottom h-55%" tabindex="-1" id="offcanvasUlasan" aria-labelledby="offcanvasUlasanLabel">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasUlasanLabel">Beri Ulasan</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small">
            <!-- icon bintang -->
            <div class="d-flex w-100 d-flex align-items-center justify-content-center flex-column gap-2 mt-2">
                <div class="w-100 d-flex align-items-center justify-content-center gap-3">
                    <i class="icon-star-empty icons font-icon-size-4" data-value="1"></i>
                    <i class="icon-star-empty icons font-icon-size-4" data-value="2"></i>
                    <i class="icon-star-empty icons font-icon-size-4" data-value="3"></i>
                    <i class="icon-star-empty icons font-icon-size-4" data-value="4"></i>
                    <i class="icon-star-empty icons font-icon-size-4" data-value="5"></i>
                </div>
                <span class="mt-1">Berikan penilainmu</span>
            </div>
            <hr class="m-0">
            <!-- icon bintang -->
            <form id="form_input" class="m-0 mt-2">
                <textarea type="text" name="review" class="form-control" rows="5"></textarea>
                <input type="hidden" name="id_sale" value="{{$sale->id}}" class="form-control">
                <input type="hidden" name="rate" id="rate_input" class="form-control">
            </form>
        </div>
        <div class="offcanvas-footer px-3 py-2 small">
            <button type="button" id="btn-kirim" onclick="handle_save_local('#btn-kirim', '#form_input', '{{route('web.review')}}', 'POST')" class="btn btn-primary w-100">Kirim</button>
        </div>
    </div>

    @section("custom_js")
    <script>
        let rating_point = 0;
        function fillTheStar(num){
            // lakukan reset
            for (let index = 0; index < 5; index++) {
                let check = $(".icons")[index].classList.contains("icon-star-empty")
                if (!check) {
                    $(".icons")[index].classList.add("icon-star-empty")
                    $(".icons")[index].classList.remove("icon-star3")
                    $(".icons")[index].classList.remove("text-emas")
                }
            }
            // lakukan lakukan isi star
            // alert(num)
            for (let index = 0; index < num; index++) {
                // console.log($(".icons")[index]);
                $(".icons")[index].classList.add("icon-star3")
                $(".icons")[index].classList.add("text-emas")
                $(".icons")[index].classList.remove("icon-star-empty")
            }
        }

        $(".icons").click(function(){
            let rate = parseInt($(this).attr("data-value"))
            // rating_point = rate
            fillTheStar(rate)
            $("#rate_input").val(rate)
            // console.log($(".icons"));
        })
        function handle_save_local(tombol, form, url, method){
            handle_save(tombol, form, url, method, function(){
                location.reload()
            })
        }
    </script>
    @endsection
</x-web-layout>