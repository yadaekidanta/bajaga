<x-web-layout title="Daftar Transaksi" keyword="Bajaga Store">
    <div class="container-custom px-3 pt-2 pb-5 bg-white">
        <!-- search -->
        <form class="m-0">
            <div class="position-relative w-100">
                <input id="search_input" type="text" onkeyup="search()" class="form-control w-100 px-4" placeholder="Cari nama pembeli, atau resi">

                <!-- icon search -->
                <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                    <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd">
                            <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                            <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                        </g>
                    </svg>
                </div>
                <!-- icon search -->

            </div>
        </form>
        <!-- search -->
        <form class="d-none" id="">
            <input type="text">
        </form>

        <!-- filter -->
        <div class="d-flex gap-3 mt-2">
            <div class="d-flex align-item-center gap-2 container-sort-filter overflow-scroll">
                <span data-filter="Semua" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                    Semua Pesanan
                </span>
                <span data-filter="Dipesan" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                    Siap Dikirim
                </span>
                <span data-filter="Dikirim" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                    Dalam Pengirman
                </span>
                <span data-filter="Diterima" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                    Pesanan Selesai
                </span>
            </div>
            <form id="content_filter" class="d-none">
                <input type="hidden" id="filter_input" name="filter">
            </form>
        </div>
        <!-- /filter -->

        <hr class="mt-0">

        <!-- list transaction -->
        <div id="list_result">
        </div>
        <div class="ajax-load d-flex justify-content-center w-100 mt-5">
            <div class="spinner-grow text-secondary" role="status">
            </div>
        </div>
        <!-- list transaction -->
    </div>

    @section('custom_js')
    <script>
        localStorage.setItem("page_infinate", 1)
        load_list(1)
        // $(document).ready(function () {
        //     $(".offcanvas-body").on("click", "div", function(){
        //         var filterValue = $(this).attr('data-filter');
        //         $('#list_result').isotope({ transitionDuration: '0.65s',filter: filterValue });
        //         // console.log(filterValue);
        //         $('.offcanvas-body div input').prop("checked", false)
        //         $(this).children("input").prop("checked", true)
        //         $(".status-filter").html($(this).children("span").text())
        //     })
        // })
        $(".container-sort-filter").on('click', function (event) {
            // alert()
            $target = $(event.target);
            localStorage.setItem("page_infinate", 1)
            $('.container-sort-filter > span').removeClass('border-primary text-primary')
            if ($target.attr("data-filter") == "Semua") {
                $("#filter_input").val('')
            }else{
                $("#filter_input").val($target.attr("data-filter"))
            }
            load_list(1)
            $target.addClass('border-primary text-primary');
        });

        function search(){
            let filter = $("#search_input").val()
            // console.log(filter);
            $.get('?search='+filter, function(result) {
                $('#list_result').html(result);
            })
        }

        $(document).ready(function(){
            let options = {
                root: null,
                rootMargin: '10px',
                threshold: 0.5
            }

            const observer = new IntersectionObserver(handleIntersect, options)
            observer.observe(document.querySelector(".ajax-load"))
        })
        
        function handleIntersect(entries){
            if (entries[0].isIntersecting) {
                // alert()
                localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                load_more(parseInt(localStorage.getItem("page_infinate")));
            }
        }
        
        function search(){
            let filter = $("#search_input").val()
            // console.log(filter);
            $.get('?search='+filter, function(result) {
                $('#list_result').html(result);
            })
        }
    </script>
    @endsection
</x-web-layout>