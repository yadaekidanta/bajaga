<x-web-layout title="" keyword="Bajaga Store">
    <div class="container-custom d-flex justify-content-center pb-10em flex-column align-items-center h-screen px-3 bg-white">
        <div class="mb-5 pb-3 d-flex gap-5 align-items-center justify-content-between flex-column">
            <div class="">
                <img src="{{asset('img/other/cart-done.png')}}" class="w-14em ms-3" alt="image card done">
            </div>
            <div class="d-flex flex-column align-items-center" id="info">
                <span class="text-center">Pembayaran Berhasil</span>
                <span class="d-block text-center">Selanjutnya. <a href="{{route('web.transaction.checkout')}}">lihat status</a> pengiriman barang</span>
            </div>
        </div>
    </div>

    @section("custom_js")
    <script>
    </script>
    @endsection
</x-web-layout>