<x-web-layout title="Daftar Transaksi" keyword="Bajaga Store">
    <div class="container-custom bg-white pb-5">
        <div class="d-flex align-items-center px-3 justify-content-between pt-2">
            <img src="{{$data['merchant_profile_picture_url']}}" class="w-4em" alt="image-bajaga">

            <div class="d-flex flex-column align-items-end">
                <h3>INVOICE</h3>
                <span class="d-block text-primary font-sm">{{$data["id"]}}</span>
            </div>
        </div>

        <hr>

        <div class="mt-3 px-3">
            <!-- diterbitkan -->
                <h4 class="m-0 font-bold mb-1">DITERBITKAN OLEH:</h4>
                <div>
                    <span class="font-bold font-sm">{{$data["merchant_name"]}}</span>
                </div>
            <!-- /diterbitkan -->
        </div>
        <div class="mt-3 px-3">
            <!-- untuk -->
                <h4 class="m-0 font-bold mb-1">UNTUK</h4>
                <div>
                    <div class="d-flex ">
                        <div class="font-sm w-9.5em">Pembeli</div>
                        <div class="font-sm font-bold">: {{$sale->customer_name}}</div>
                    </div>
                    <div class="d-flex ">
                        <div class="font-sm w-9.5em">Tanggal Pembelian</div>
                        <div class="font-sm font-bold">: {{$sale->date}}</div>
                    </div>
                    <div class="d-flex ">
                        <div class="font-sm min-w-9.5em">Alamat pengiriman</div>
                        <div class="font-sm font-bold w-100">
                            <div class="font-bold font-sm">: {{$sale->customer_name}}</div>
                            <div class="font-sm">{{$sale->customer_phone}}</div>
                            <div class="font-bold font-sm d-blok">{{$sale->shipping_detail}}</div>
                        </div>
                    </div>
                </div>
            <!-- /untuk -->
        </div>

        <div class="mt-2">
            <hr class="m-0">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">INFO PRODUK</th>
                        <th scope="col">JUMLAH</th>
                        <th scope="col">HARGA SATUAN</th>
                        <th scope="col">TOTAL HARGA</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sale->sale_detail()->get() as $product)
                        <tr>
                            <th class="text-primary">
                                <span class="font-bold">{{Str::limit($product->product->name, 14)}}</span>
                                <div class="mt-1">
                                    <span class="font-sm font-thin text-secondary">Berat</span>
                                    <span class="font-sm text-dark">{{$product->product->weight}}</span>
                                </div>
                            </th>
                            <td>{{$product->qty}}</td>
                            <td>Rp{{number_format(($product->price / $product->qty))}}</td>
                            <td>Rp{{number_format($product->price)}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="mt-3">
            <div class="px-3">
                <div class="d-flex justify-content-between">
                    <span class="font-sm">Total Harga ({{count($sale->sale_detail()->get())}} barang)</span>
                    <span class="font-sm">Rp{{number_format($sale->total)}}</span>
                </div>
            </div>
            <div class="px-3">
                <div class="d-flex justify-content-between">
                    <span class="font-sm">Total Ongkos Kirim</span>
                    <span class="font-sm">Rp{{number_format($sale->shipping_price)}}</span>
                </div>
            </div>
            <div class="px-3">
                <div class="d-flex justify-content-between">
                    <span class="font-sm">Diskon</span>
                    <span class="font-sm">-Rp{{number_format($sale->disc_price)}}</span>
                </div>
            </div>
            <hr>
            <div class="px-3">
                <div class="d-flex justify-content-between">
                    <span class="   font-bold d-block">Total Belanja</span>
                    <span class="font-sm">Rp{{number_format($sale->grand_total)}}</span>
                </div>
            </div>
        </div>
        <hr>
        <div class="d-flex justify-content-between px-3">
            <div>
                <span class="font-sm font-thin">Kurir :</span>
                <span class="font-sm font-bold d-block">{{$sale->courier}}</span>
            </div>
            <div class="d-flex flex-column align-items-end">
                <span class="font-sm font-thin">Metode Pembayaran:</span>
                <span class="font-sm font-bold d-block">{{$data["payment_method"]}} ({{$data["payment_channel"]}})</span>
            </div>
        </div>
    </div>
</x-web-layout>