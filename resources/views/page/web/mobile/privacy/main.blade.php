<x-web-layout title="Syarat & Ketentuan" keyword="Bajaga Store">
    <div class="bg-white px-3 pt-3 pb-5" style="margin-top: -1.6em;">
        <h1 class="mb-3">Kebijakan Privasi</h1>
        <p>
            Adanya Kebijakan Privasi ini adalah komitmen nyata dari Bajaga untuk menghargai dan melindungi setiap data atau informasi pribadi Pengguna situs www.bajaga.com, situs-situs turunannya, serta aplikasi gawai Bajaga (selanjutnya disebut sebagai "Situs"). 
        </p>
        <p>
            Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs Bajaga sebagaimana tercantum dalam Syarat & Ketentuan dan informasi lain yang tercantum di Situs) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pengiriman, pembukaan, penyimpanan, perubahan, penghapusan dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang mengidentifikasikan atau dapat digunakan untuk mengidentifikasi Pengguna yang Pengguna berikan kepada Bajaga atau yang Bajaga kumpulkan dari Pengguna maupun pihak ketiga (selanjutnya disebut sebagai "Data Pribadi").
        </p>
        <p>
            Dengan mengklik “Daftar” (Register) atau pernyataan serupa yang tersedia di laman pendaftaran Situs, Pengguna menyatakan bahwa setiap Data Pribadi Pengguna merupakan data yang benar dan sah, Pengguna mengakui bahwa ia telah diberitahukan dan memahami ketentuan Kebijakan Privasi ini serta Pengguna memberikan persetujuan kepada Bajaga untuk memperoleh, mengumpulkan, mengolah, menganalisis, menampilkan, mengirimkan, membuka, menyimpan, mengubah, menghapus, mengelola dan/atau mempergunakan data tersebut untuk tujuan sebagaimana tercantum dalam Kebijakan Privasi.
        </p>
        <h3>A. Perolehan dan Pengumpulan Data Pribadi Pengguna</h3>
        <p>
            Bajaga mengumpulkan Data Pribadi Pengguna dengan tujuan untuk memproses transaksi Pengguna, mengelola dan memperlancar proses penggunaan Situs, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan perundang-undangan yang berlaku. Adapun data Pengguna yang dikumpulkan adalah sebagai berikut:
        </p>
        <ol>
            <li>
                Data yang diserahkan secara mandiri oleh Pengguna, termasuk namun tidak terbatas pada data yang diserahkan pada saat Pengguna:
            </li>
        </ol>
    </div>
</x-web-layout>