@foreach($address as $a)
    @if($a->is_use == 1)
        <div id="data_address">
            <input type="hidden" id="dest_subdistrict_is_use" value="{{$address_use->subdistrict_id}}">
            <input type="hidden" id="fullname" value="{{$address_use->users->name}}">
            <input type="hidden" id="phoneNumber" value="{{$address_use->users->phone}}">
            <input type="hidden" value="{{$address_use->subdistrict->name}}, {{$address_use->city->name}}, {{$address_use->province->name}}, ID {{$address_use->postcode}}" id="shipping_detail">
            <div>
                <span class="font-sm font-bold">{{$a->users->name}}</span> <span class="font-sm">({{$a->users->phone}})</span>
            </div>
            <span class="font-sm text-secondary">{{$a->desc}}</span>
            <span class="d-block font-sm text-secondary text-capitalize">{{$a->subdistrict->name}}, {{$a->city->name}}, {{$a->province->name}}, ID {{$a->postcode}}</span>    
    
            <div class="w-100 d-flex justify-content-end mt-2">
                <button class="btn btn-outline-primary btn-sm" data-bs-toggle="offcanvas" data-bs-target="#canvasOtherAddress" aria-controls="canvasOtherAddress">Pilih Alamat Lain</button>
            </div>
        </div>
    @endif
@endforeach