<x-web-layout title="Pengiriman" keyword="Bajaga Store">
    <div class="container-custom">
        <div class="px-3 pt-2 pb-2 bg-white">
            <h3 class="m-0">Alamat Pengiriman</h3>
        </div>
        <hr class="m-0">
        <!-- adress -->
        <div class="bg-white px-3 py-2 pb-3">
            <div id="content_list">
                <div id="list_result">
                    <div id="data_address">
                        <input type="hidden" id="dest_subdistrict_is_use" value="{{$address_use->subdistrict_id}}">
                        <input type="hidden" id="fullname" value="{{$address_use->users->name}}">
                        <input type="hidden" id="phoneNumber" value="{{$address_use->users->phone}}">
                        <input type="hidden" value="{{$address_use->subdistrict->name}}, {{$address_use->city->name}}, {{$address_use->province->name}}, ID {{$address_use->postcode}}" id="shipping_detail">
                        <div>
                            <span class="font-sm font-bold">{{$address_use->users->name}}</span> <span class="font-sm">({{$address_use->users->phone}})</span>
                        </div>
                        <span class="font-sm text-secondary">{{$address_use->desc}}</span>
                        <span class="d-block font-sm text-secondary text-capitalize">{{$address_use->subdistrict->name}}, {{$address_use->city->name}}, {{$address_use->province->name}}, ID {{$address_use->postcode}}</span>    
                
                        <div class="w-100 d-flex justify-content-end mt-2">
                            <button class="btn btn-outline-primary btn-sm" data-bs-toggle="offcanvas" data-bs-target="#canvasOtherAddress" aria-controls="canvasOtherAddress">Pilih Alamat Lain</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /adress -->

        <!-- info email user -->
        <input type="hidden" value="{{$address_use->users->email}}" id="email">
        <!-- info email user -->

       <!-- product -->
       <div class="d-flex flex-column gap-2">
       @if (session()->has('data'))
            @foreach (session('data')['store_id'] as $toko)

                <div class="container-store">
                    <!-- item -->
                    @php
                    $user_id = Auth::user()->id;
                    $store = \App\Models\UserStore::where('id', $toko)->first();
                    $carts = \App\Models\Cart::whereIn("product_id", session('data')['data_product'])->where('user_id', $user_id)->where('store_id', $toko)->get();
                    $product = [];
                    if (count($carts) == 0) {
                        $product = \App\Models\Product::where("id", session('data')['data_product'][0])->first();
                        // dd($product, session('data')['data_product'][0]);
                    }
                    @endphp
                    <input type="hidden" value="{{$store->id}}" id="store_id">
                    <div class="py-3 bg-white mt-2">
                        <div class="px-3 d-flex align-items-center gap-3">
                            <div class="d-flex flex-column justify-content-between">
                                <span class="font-bold">{{$store->name}}</span>
                                <span class="font-thin font-sm">{{$store->city->name}}</span>
                            </div>
                        </div>

                        @if (count($carts) > 0)
                            @foreach($carts as $st)
                            <div class="px-3 d-flex gap-3 mt-3 item-product">
                                <input type="hidden" value="{{$st->product->weight}}" id="weight">
                                <input type="hidden" value="{{$st->qty}}" id="qty">
                                <input type="hidden" value="{{$st->product->stock}}" id="stock">
                                <input type="hidden" value="{{$st->product->price}}" id="price_product_to">
                                <input type="hidden" value="{{$st->id}}" id="id_cart">
                                <input type="hidden" value="{{$st->product->id}}" id="id_product">
                                <input type="hidden" value="{{$st->product->id}}" id="origin">
                                <div class="d-flex gap-3">
                                    <div class="border w-20 h-20">
                                        <img src="{{asset('storage/'.$st->product->photo)}}" class="object-fit-cover w-100 h-100">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <span class="font-medium font-bold">{{$st->product->name}}</span>
                                        <span class="font-sm font-thin">{{$st->qty}} Barang</span>
                                        <span class="m-0 font-bold">Rp {{number_format($st->product->price)}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="px-3 d-flex gap-3 mt-3 item-product">
                                <input type="hidden" value="{{$product->weight}}" id="weight">
                                <input type="hidden" value="1" id="qty">
                                <input type="hidden" value="{{$product->stock}}" id="stock">
                                <input type="hidden" value="{{$product->price}}" id="price_product_to">
                                <input type="hidden" value="{{$product->id}}" id="id_product">
                                <input type="hidden" value="{{$product->id}}" id="origin">
                                <div class="d-flex gap-3">
                                    <div class="border w-20 h-20">
                                        <img src="{{asset('storage/'.$product->photo)}}" class="object-fit-cover w-100 h-100">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <span class="font-medium font-bold">{{$product->name}}</span>
                                        <span class="font-sm font-thin">1 Barang</span>
                                        <span class="m-0 font-bold">Rp {{number_format($product->price)}}</span>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <!-- shipping choice -->
                        <div class="w-full px-3 mt-3" role="button">
                            <div class="border rounded px-2 p-1 bg-white d-flex align-items-center opacity-65 justify-content-between canvasShipping">
                                @if (count($carts) > 0)
                                    <input type="hidden" value="{{$carts[0]->cart_store->subdistrict_id}}" id="origin">
                                @else
                                    <input type="hidden" value="{{$product->product_store->subdistrict_id}}" id="origin">
                                    
                                @endif
                                <div class="d-flex align-items-center gap-2">
                                    <i class="icon-shipping-fast"></i>
                                    <span class="font-bold d-flex align-items-center gap-3">
                                        <span id="text-btn-shipping-{{$toko}}">Pilih Pengiriman</span> 
                                        <div class="spinner-border spinner-border-sm text-primary spinner" role="status">
                                        </div>
                                    </span>
                                </div>

                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 40 40"><g fill="none" fill-rule="evenodd"><circle cx="24" cy="24" r="24"/><path stroke="#6c727c" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" d="M16.53 27.47l7.202-7.202m-7.202-7.2l7.202 7.203"/></g></svg>
                            </div>
                        </div>
                        <!-- shipping choice -->
                    </div>
                </div>
            @endforeach
       @endif
       </div>
       <!-- /product -->

        <!-- kupon -->
        <div class="px-3 py-3 bg-white mt-2">
            <!-- shipping choice -->
            <div class="w-full" role="button">
                <div class="border rounded p-1 d-flex align-items-center justify-content-between" id="container-btn-cupon" data-bs-toggle="offcanvas" data-bs-target="#canvasCupon" aria-controls="canvasCupon">
                    <div class="d-flex align-items-center gap-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#3E74FD" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd"><path d="M0 0h24v24H0z"/><path fill="#3E74FD" fill-rule="nonzero" d="M10.194 2.665a2.783 2.783 0 0 1 3.611 0l.838.713c.446.38 1 .61 1.584.657l1.098.087a2.783 2.783 0 0 1 2.552 2.553l.088 1.097c.047.585.276 1.138.656 1.585l.714.837a2.781 2.781 0 0 1 0 3.61l-.714.838c-.38.447-.61 1-.656 1.585l-.088 1.097a2.783 2.783 0 0 1-2.552 2.553l-1.098.087a2.793 2.793 0 0 0-1.584.656l-.838.714a2.781 2.781 0 0 1-3.61 0l-.838-.714c-.446-.38-1-.61-1.584-.656l-1.097-.087a2.785 2.785 0 0 1-2.554-2.553l-.087-1.097a2.782 2.782 0 0 0-.656-1.585l-.714-.838a2.781 2.781 0 0 1 0-3.61l.714-.837c.38-.447.61-1 .656-1.585l.087-1.097a2.786 2.786 0 0 1 2.554-2.553l1.097-.087a2.79 2.79 0 0 0 1.584-.657zm5.571 5.57a.797.797 0 0 0-1.13 0l-6.4 6.4a.797.797 0 0 0 0 1.13.798.798 0 0 0 1.131.001l6.4-6.4a.8.8 0 0 0 0-1.131zm-1.194 4.908a1.428 1.428 0 1 0 0 2.855 1.428 1.428 0 0 0 0-2.855zM9.43 8a1.429 1.429 0 1 0-.002 2.858A1.429 1.429 0 0 0 9.43 8z"/></g></svg>
                        <span class="font-bold text-cupon">Makin hemat pakai promo</span>
                    </div>

                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 40 40"><g fill="none" fill-rule="evenodd"><circle cx="24" cy="24" r="24"/><path stroke="#6c727c" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" d="M16.53 27.47l7.202-7.202m-7.202-7.2l7.202 7.203"/></g></svg>
                </div>
            </div>
            <!-- shipping choice -->
        </div>
        <!-- /kupon -->

        <!-- total price -->
        <div class="px-3 py-3 bg-white mt-2 mb-4">
                <div>
                    <span class="font-medium font-bold">Ringkasan belanja</span>
                    <div class="mt-1 d-flex justify-content-between">
                        <span class="font-thin" id="total_product"></span>
                        <span class="font-thin" id="total_price">Rp ..</span>
                    </div>
                    <div class="d-flex justify-content-between">
                        <span class="font-thin">Total Ongkos Kirim</span>
                        <span class="font-thin" id="total_ongkir">Rp ..</span>
                    </div>
                    <div class="info-diskon"></div>
                </div>
                <hr>
                <div class="d-flex justify-content-between">
                    <span class="font-bold">Total Tagihan</span>
                    <span class="font-thin" id="total_bill">Rp ..</span>
                </div>
            
            <div class="w-full mt-4">
                <button disabled type="submit" class="btn btn-primary w-full" onclick="checkout()" id="btn-paid">Pilih Pembayaran</button>
            </div>
        </div>
        <!-- /total price -->

        <!-- canvas cupon -->
        <div class="offcanvas offcanvas-end" tabindex="-1" id="canvasCupon" aria-labelledby="canvasCuponLabel">
            <div class="offcanvas-header">
                <h3 id="offcanvasRightLabel">Pilih Promo</h3>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <!-- list cupon -->
                <div class="cupon-container">
                    @php
                        $i = 0;
                    @endphp
                    @foreach($cupons as $cupon)
                    <div onclick="use_cupon(`{{$cupon->type}}`, `{{$cupon->type == 'harga' ? $cupon->harga : $cupon->persentase}}`, '{{$cupon->name}}', this)" id="cupon-card item-{{++$i}}" class="card my-3 cupon-card" data-target="parent-card">
                        <div data-target="parent-header" class="card-header bg-header-opacity">
                            <h4 class="font-bold">{{$cupon->name}}</h4>
                            <a href="{{route('web.discount.detail', $cupon->id)}}" class="font-sm ">Lihat detail</a>
                        </div>
                        <div data-target="parent-body" class="card-body">
                            <!-- info -->
                            <div class="">
                                {{$cupon->desc}}
                            </div>
                            <div data-target="parent-info" class="d-flex align-items-center gap-2">
                                <i class="icon-clock1"></i>
                                <span>{{$cupon->end_at}}</span>
                            </div>
                            <!-- info -->
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- list cupon -->
            </div>
        </div>
        <!-- /canvas cupon -->

        <!-- canvas shipping -->
        <div class="offcanvas h-100 offcanvas-bottom" tabindex="-1" id="canvasShipping" aria-labelledby="canvasCupon">
            <div class="offcanvas-header">
                <h2 class="offcanvas-title font-bold " id="canvasShippingLabel">Pilih Pengiriman</h2>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body small p-0">
                    <!-- TODO: suruh BE siapin data jasa kirim yang di sediakan seller -->
                    <!-- list pilihan shipping -->
                    <div class="d-flex flex-column container-item-courier">
                    </div>
                </div>
                <!-- list pilihan shipping -->
            </div>
        </div>
        <!-- /canvas shipping -->

        <!-- canvas address -->
        <div class="offcanvas h-100 offcanvas-bottom" tabindex="-1" id="canvasOtherAddress" aria-labelledby="canvasCupon">
            <div class="offcanvas-header">
                <h2 class="offcanvas-title font-bold " id="canvasOtherAddressLabel">Pilih Alamat Lain</h2>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body small">

                <form action="">
                    <div class="position-relative w-100">
                        <input type="text" class="form-control w-100 px-4" placeholder="Cari Alamat">

                        <!-- icon search -->
                        <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                            <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                                <g fill="none" fill-rule="evenodd">
                                    <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                                    <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                                </g>
                            </svg>
                        </div>
                        <!-- icon search -->

                    </div>
                </form>

                <!-- card list address -->
                <div class="d-flex flex-column gap-3">
                    @foreach($address as $a)
                        <div class="card">
                            <div class="card-body row align-items-center">
                                <div class="col-10">
                                    <div>
                                        <span class="font-sm">Osyi</span>
                                        <span class="font-sm text-primary"></span>
                                    </div>
            
                                    <div class="d-flex flex-column text-box mt-1" data-maxlength="25">
                                        <span class="font-sm text-secondary">{{$a->users->phone}}</span>
                                        <span class="font-sm text-secondary">{{$a->users->address}}</span>
                                        <span class="font-sm text-secondary text-capitalize">{{$a->subdistrict->name}}, {{$a->city->name}}, {{$a->province->name}}, ID {{$a->postcode}}</span> 
                                    </div>
                                </div>
            
                                <div class="col h-100">
                                    <div class="d-flex align-items-center h-100">
                                        <input type="radio" name="alamat" class="form-custom-radio is_use" {{$a->is_use == 1 ? 'checked': ''}}>
                                        <input type="hidden" value="{{$a->id}}">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-center">
                                    <!-- TODO: suruh backend untuk ambil data address user sesuai dari usernya, dan kasi paggination ganti parameter ke dua menggunakan id dari alamatnya  -->
                                    <a href="{{route('web.user-address.edit', $user->id)}}" class="text-black font-sm">Ubah Alamat</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- /card list address -->


            </div>
        </div>
        <!-- /canvas address -->

    </div>

    @section("custom_js")
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
        let bill_data = {
            product: [],
            diskon: 0,
            total_item: 0,
            customer_id : '{{Auth::user()->id}}',
            customer_name : '{{Auth::user()->name}}',
            customer_phone : '{{Auth::user()->phone}}',
        }
        let type_cupon = null;
        let allow_to_choice_courier = false;

        

        let item_collections = [].slice.call(document.getElementsByClassName('item-product'))
        let container_store = [].slice.call(document.getElementsByClassName('container-store'))
        let is_checked_count_all_item = 0

        function reload_item(){
            let data_store = []
            container_store.forEach(data_container => {
                let data_item = []
                let store_id = parseInt($(data_container).find("#store_id")[0].value)
                let shipping_detail = $("#shipping_detail").val()
                let shipping_price = 0
                let item = [].slice.call($(data_container).find('.item-product'))
                // console.log(item[0]);
                item.forEach(item => {
                    bill_data.total_item++
                    let price = parseInt($(item).find("#price_product_to")[0].value)
                    let product_id = parseInt($(item).find("#id_product")[0].value)
                    let qty = parseInt($(item).children("#qty")[0].value)
                    data_item.push({
                        price,
                        qty,
                        product_id
                    })
                });
                
                bill_data.product.push({data:data_item, store_id, shipping_detail, shipping_price})
            });
            $("#total_product").html(`Total Harga (${item_collections.length} Barang)`)
        }
        reload_item()

        function reload_price()
        {
            let price = 0;
            let bill = 0;
            let ongkir = 0;
            bill_data.product.forEach(prod => {

                let price_per_store = 0                
                prod["diskon_per_store"] = 0
                prod["bill_per_store"] = 0

                prod.data.forEach(itm => {
                    price += (itm.price * itm.qty)
                    bill += (itm.price * itm.qty)
                    
                    // add subtotal per product in per store
                    itm['subtotal'] = 0
                    itm['shipping_price'] = 0
                    itm['diskon_per_product'] = 0
                    itm.subtotal = (itm.price * itm.qty) + (prod.shipping_price / parseInt(prod.data.length))
                    itm.shipping_price = (prod.shipping_price / parseInt(prod.data.length))
                    if (type_cupon == null) {
                        itm.diskon_per_product = 0
                    }else if(type_cupon == "harga"){
                        itm.diskon_per_product = (bill_data.diskon / parseInt(bill_data.total_item))
                    }else{
                        itm.diskon_per_product = ((itm.price * itm.qty) + (prod.shipping_price / parseInt(prod.data.length))) * bill_data.diskon
                    }
                    price_per_store += (itm.price * itm.qty)
                    prod.diskon_per_store += itm.diskon_per_product
                    prod.bill_per_store += itm.subtotal
                });
                // bill per store
                if (type_cupon != null) {
                    prod.bill_per_store -= prod.diskon_per_store
                }
                // end of bill per store

                // set diskon per product
                // prod.data.forEach(itm => {
                //     itm['diskon_per_product'] = 0
                //     itm.diskon_per_product = prod.diskon_per_store / parseInt(prod.data.length)
                // })
                // end of set diskon per product

                prod["price_per_store"] = price_per_store
                ongkir += prod.shipping_price
            });
            // console.log(bill_data);

            bill += ongkir
            
            if (type_cupon == "harga") {
                bill -= bill_data.diskon
            }else if(type_cupon){
                bill *= bill_data.diskon
            }

            bill_data.total_bill = bill
            bill_data.total_price = price
            
            // console.log(bill_data);
            $("#total_price").html(`Rp ${formatRupiah(price.toString())}`)
            $("#total_bill").html(`Rp ${formatRupiah(bill.toString())}`)
            $("#total_ongkir").html(`Rp ${formatRupiah(ongkir.toString())}`)
        }

        reload_price()

        $(".subtotal-trigger").on('click', function (event) {
            $target = $(event.target);
            // console.log($target.parent()[0]);
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function use_cupon(type, discount, name, element)
        {
            // total_harga = total_harga_tmp
            if(element.classList.contains('border')){
                name = "Makin hemat pakai promo"
                bill_data.diskon = 0
                type_cupon = null
                $("#container-discount").remove()
                $("#container-btn-cupon").removeClass('bg-aliceblue')
            }else{
                $("#container-btn-cupon").addClass('bg-aliceblue')
                name = limit_text(name)
                if (type == "harga") {
                    type_cupon = "harga"
                    bill_data.diskon = parseInt(discount)
                    // total_diskon = parseInt(discount) // old function
                    
                    $(".info-diskon").html(`
                        <div class="d-flex justify-content-between" id="container-discount">
                            <span class="font-thin">Diskon</span>
                            <span class="font-thin">Rp ${formatRupiah(discount.toString())}</span>
                        </div>
                    `)
                } else {
                    type_cupon = "diskon"
                    bill_data.diskon = parseInt(discount) / 100
                    $(".info-diskon").html(`
                        <div class="d-flex justify-content-between" id="container-discount">
                            <span class="font-thin">Diskon</span>
                            <span class="font-thin">${discount}%</span>
                        </div>
                    `)
                }
            }

            reload_price()
            $(".text-cupon").text(`${limit_text(name, 30)}`)
        }

        $(".cupon-container").on('click', function (event) {
            
            $target = $(event.target);
            if ($target.parent().parent().parent().hasClass('border border-primary')) {
                $('.cupon-container > *').removeClass('border border-primary')
            }else if($target.parent().parent().hasClass('border border-primary')){
                $('.cupon-container > *').removeClass('border border-primary')
            }else if($target.parent().hasClass('border border-primary')){
                $('.cupon-container > *').removeClass('border border-primary')
            }else{
                
                $('.cupon-container > *').removeClass('border border-primary')
                if ($target.parent().attr("data-target") == "parent-info") {
                    $target.parent().parent().parent().addClass('border border-primary');
                }else if($target.parent().attr("data-target") == "parent-body"){
                    $target.parent().parent().addClass('border border-primary');
                }else if($target.parent().attr("data-target") == "parent-header"){
                    $target.parent().parent().addClass('border border-primary');
                }else{
                    $target.parent().addClass('border border-primary');
                }
            }
            // console.log($target);
        });

        $(".is_use").change(function(){
            // console.log($(".is_use:checked").next().val())
            // let base_url =
            $.ajax({
                type: 'PATCH',
                url: `{{env('APP_URL')}}/user-address/update/${$(".is_use:checked").next().val()}`,
                dataType: 'json',
                processData: false,
                success: function(response){
                    if(response.alert == 'success'){
                        Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                        get_info_store()
                        load_list(1)
                    }
                }
            })
        })

        load_list(1)

        // get ongkir
        let collections_ongkir = []
        get_info_store()
        function get_info_store(){
            let array_store = container_store.map(element => {
                let stores = [];
                // ambil id-id store
                stores.push($(element).children("#store_id")[0].value)
                $(element).children(".item-product")
                // get total weight
                let arrayItem = [].slice.call($(element).find(".item-product"))
                let totalWeight = 0;
                arrayItem.forEach(ip => {
                    totalWeight += parseInt($(ip).children("#weight")[0].value)
                });
                stores.push(totalWeight)
                return stores
            });

            // hit api ongkir, (store_id, destination, weight)
            let counter = 0
            // console.log(array_store.length);
            check_ongkir(array_store, function() {
                counter++
                if ( counter == array_store.length ) {
                    // console.log(collections_ongkir);
                    $(".canvasShipping").removeClass("opacity-65")
                    $(".spinner").addClass("d-none")
                    allow_to_choice_courier = true
                }
            })

            
        }

        function check_ongkir(array_store, cb){
            // get destination, destinationType, 
            let destination = $("#dest_subdistrict_is_use").val()
            let destinationType = "subdistrict"
            let collections = []
            array_store.forEach(store_info => {
                var settings = {
                    "url": `{{env('APP_URL')}}/api/rajaongkir/cost?store_id=${store_info[0]}&destination=${destination}&destinationType=${destinationType}&weight=${store_info[1]}`,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "key": '{{ env("RAJAONGKIR_KEY") }}'
                    },
                };

                // console.log(settings);
    
                $.ajax(settings).done(function (response) {
                    let data = {"store_id": 0, "data": []}
                    data.store_id = store_info[0]
                    response.forEach(collection => {
                        data.data.push(collection)
                    });
                    collections_ongkir.push(data)
                    cb()
                });
            });
        }

        let list_courier_item = [].slice.call(document.getElementsByClassName("item-courier"))
        // ongkir section
        $(".canvasShipping").click(function(){
            let countainer = $(".container-item-courier")
            if (countainer.children() != undefined) {
                countainer.children().remove()
            }
            if (allow_to_choice_courier) {
                let store_id = $(this).parent().parent().prev()[0].value
                let collections = collections_ongkir.filter( data => {
                    if (data.store_id == store_id) {
                        
                        data.data.forEach(element => {
                            // console.log(element.code);
                            let name = element.name
                            let costs = element.costs
                            let code_courier = element.code
                            let id = 0
                            costs.forEach(data_costs => {
                                let cost = data_costs.cost[0].value
                                let etd = data_costs.cost[0].etd
                                let html = `
                                 <div class="d-flex flex-column item-courier-choice px-3 pt-3" onclick="choiceOngkir(${cost}, this, ${store_id})">
                                     <input type="hidden" value="${cost}" id="cost">
                                     <input type="hidden" value="${code_courier}" id="code_courier">
                                     <span class="font-bold font-lg" id="name">${name}</span>
                                     <span>Rp ${formatRupiah(cost.toString())}</span>
                                     <span class="font-sm mb-1">Estimasi ${etd}</span>
                                     <hr class="m-0 mt-2">
                                </div>
                                `
                                countainer.append(html)
                            });
                        });
                        $("#canvasShipping").offcanvas("show")
                    }
                })
            }

        })

        function choiceOngkir(cost, el, store_id){
            $(".container-item-courier > div").removeClass("hover-click-alice")
            $(el).addClass("hover-click-alice")
            let name = $(el).children("#name")[0].textContent
            let code_courier = $(el).find("#code_courier")[0].value
            $("#text-btn-shipping-"+store_id).text(limit_text(name, 30))
            // $("#text-btn-shipping-"+store_id).addClass('bg-aliceblue')
            

            $("#btn-paid").removeAttr("disabled")
            $("#canvasShipping").offcanvas("hide")
            // set ongkir
            bill_data.product.forEach(prod => {
               if (prod.store_id == store_id) {
                    prod.shipping_price = cost
                    prod.code_courier = code_courier
                    prod.courier = name
               }

                //    check if all shipping was choice 
                if (prod.shipping_price == 0) {
                    $("#btn-paid").attr('disabled','disabled')
                }
            });
            // total_ongkir = cost
            // console.log(bill_data);
            reload_price()
        }

        function checkout()
        {
            // set loader animation in btn pilih pembayaran
            $("#btn-paid").html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)

            $("#btn-paid").attr('disabled','disabled')

            let email = $("#email").val()
            let name = $("#fullname").val()
            let description_checkout = `${name} melakukan pembayaran`
            let phone = $("#phoneNumber").val()
            let amount = bill_data.total_bill
            
            var settings = {
                "url": "{{env('APP_URL')}}/api/xendit/invoice",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    amount,
                    "description": description_checkout,
                    "payer_email": email,
                    "should_send_email": true,
                    "customer" : {
                        "given_names" : name,
                        "email" : email,
                        "mobile_number" : phone
                    }
                }),
            };
    
            $.ajax(settings).done(function (response) {
                
                // ambil id store
                let store_id = bill_data.product.map(item_prod => {
                    return item_prod.store_id
                });
                let payment_st = 'Belum lunas'
                if (response.status == 'PAID') {
                    payment_st = 'Lunas'
                }
                bill_data['payment_st'] = payment_st,
                bill_data['code'] = response.id,
                bill_data['disc_price'] = type_cupon == "harga" ? bill_data.total_price - bill_data.diskon : bill_data.total_price * bill_data.diskon,
                bill_data['date'] = response.created,
                bill_data['note'] = `${name} melakukan pembayaran`
                bill_data['type'] = `online`
                bill_data['st'] = `Dipesan`
                // console.log(bill_data);
                // store transaction user
                $.ajax({
                    url: '{{route('web.transaction.invoice')}}',
                    method: "POST",
                    header: {
                        "Content-Type": "application/json"
                    },
                    data: bill_data
                }).done( data => {
                    // console.log(data);
                    $("#btn-paid").html(`Pilih Pembayaran`)
                    $("#btn-paid").attr('disabled','disabled')
                    if (data.status == 'success') {
                        location.href = response.invoice_url
                    }else{
                        error_toastr(data.message);
                    }
                }).fail( err => {
                    $("#btn-paid").html(`Pilih Pembayaran`)
                    $("#btn-paid").attr('disabled','disabled')
                    error_toastr(response);
                })
            }).fail(function (response){
                $("#btn-paid").attr('disabled','disabled')
                $("#btn-paid").html(`Pilih Pembayaran`)
                error_toastr(response);
            })

        }


    </script>
    @endsection
</x-web-layout>