@foreach($userList as $listMessage)
    @if($listMessage["from_id"] == Auth::user()->id)
        <a href="{{route('web.chat.detail', $listMessage->to_id)}}" class="">
            <div class="d-flex hover-opacity-65 text-body px-3 py-3 align-items-center justify-content-between {{$listMessage->counter_read_null > 0 ? 'bg-aliceblue' : ''}}">
                <div class="d-flex align-items-center gap-3">
                    <!-- img user -->
                    <div class="w-15 h-15 border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                        <img src="{{asset($listMessage->to->avatar ? 'storage/'. $listMessage->to->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
                    </div>
                    <!-- img user -->
    
                    <div>
                        <span class="font-lg font-bold">{{Str::limit($listMessage->to->name, 15)}}</span>
                        <span class="d-block font-sm">{{Str::limit($listMessage->body, 25)}}</span>
                    </div>
                </div>
    
                <div class="d-flex align-items-center flex-column">
                    @if(strtotime($listMessage->created_at) < (time()-(60*60*24)))
                        <span class="text-primary">Yesterday</span>
                    @else
                        <span class="text-primary">{{$listMessage->created_at->format('H:i')}}</span>
                    @endif
                </div>
                
            </div>
            <hr class="m-0">
        </a>
    @else
        <a href="{{route('web.chat.detail', $listMessage->from_id)}}" class="">
            <div class="d-flex hover-opacity-65 text-body align-items-center justify-content-between {{$listMessage->counter_read_null > 0 ? 'bg-aliceblue' : ''}} px-3 py-3">
                <div class="d-flex align-items-center gap-3">
                    <!-- img user -->
                    <div class="w-15 h-15 border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                        <img src="{{asset($listMessage->from->avatar ? 'storage/'. $listMessage->from->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
                    </div>
                    <!-- img user -->
    
                    <div>
                        <span class="font-lg font-bold">{{Str::limit($listMessage->from->name, 15)}}</span>
                        <span class="d-block font-sm">{{Str::limit($listMessage->body, 25)}}</span>
                    </div>
                </div>
    
                <div class="d-flex align-items-center flex-column">
                    @if(strtotime($listMessage->created_at) < (time()-(60*60*24)))
                        <span class="text-primary">Yesterday</span>
                    @else
                        <span class="text-primary">{{$listMessage->created_at->format('H:i')}}</span>
                    @endif
                </div>
            </div>
            <hr class="m-0">
        </a>
    @endif
@endforeach


<script>
    // $(document).ready(function() {
        // alert(($('html').height()))
    // });
</script>