<x-web-layout title="Pesan" keyword="Bajaga Store">
    <div class="bg-white pt-3 container-custom pb-5">
        <!-- chat -->
        <form action="" class="m-0 px-3 d-none">
            <div class="position-relative w-100">
                <input type="text" class="form-control w-100 px-4" placeholder="Cari.." onkeyup="load_list(1)">

                <!-- icon search -->
                <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                    <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd">
                            <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                            <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                        </g>
                    </svg>
                </div>
                <!-- icon search -->

            </div>
        </form>
        <!-- chat -->

        <!-- filter -->
        <div class="d-flex gap-3 mb-2 px-3 mt-2 d-none">
            <span class="font-bold">Filter</span>
            <div class="rounded-lg border px-1 d-flex gap-2 align-items-center" data-bs-toggle="offcanvas" data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom">
                <span class="status-filter">Semua status</span>
                
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                    <defs>
                        <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                    </defs>
                    <g fill="none" fill-rule="evenodd">
                        <path d="M0 24h24V0H0z"/>
                        <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                    </g>
                </svg>

            </div>
        </div>
        <!-- /filter -->

        <hr class="mb-0 mt-2 d-none">
        
        <!-- list chat -->
        <div id="list_result">
            
        </div>
        <!-- /list chat -->


        <!-- canvas filter -->
        <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
            <div class="offcanvas-header">
                <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Mau lihat status apa?</h3>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body small">
                <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter="*">
                    <span class="font-bold font-lg">Semua</span>
                    <input type="radio" name="all" class="form-custom-radio" checked>
                </div>
                
                <hr class="m-0">
                
                <div class="d-flex justify-content-between align-items-center input-group py-4" data-filter=".belum">
                    <span class="font-bold font-lg">Belum Dibaca</span>
                    <input type="radio" name="belum" class="form-custom-radio">
                </div>
            </div>
        </div>
        <!-- /canvas filter -->
    </div>

    @section('custom_js')
    <script>
        load_list(1);
        hideFooter()
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        setInterval(() => {
            load_list(1);
        }, 3000);
    </script>
    @endsection
</x-web-layout>