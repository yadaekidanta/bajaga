<input type="hidden" value="{{$user->id}}" id="to_id">
@foreach($collection as $message)
    @if($message->from_id == $user->id)
        <!-- dia -->
        <div class="d-flex mt-3 gap-2">
            <!-- img user -->
            <div class="w-10 h-10 border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                <img src="{{asset($message->from->avatar ? 'storage/'. $message->from->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
            </div>
            <!-- img user -->

            <div class="card-chat-reverse text-break">
                {{$message->body}}
            </div>

        </div>
        <!-- /dia -->
    @else
        <!-- aku -->
        <div class="d-flex justify-content-end gap-2 mt-3">
            <div class="card-chat text-break">
                {{$message->body}}
            </div>
            <!-- img user -->
            <div class="w-10 h-10 border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                <img src="{{asset($message->from->avatar ? 'storage/'. $message->from->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
            </div>
            <!-- img user -->

        </div>
        <!-- /aku -->
    @endif
@endforeach

<script>
    $("#btn_id_input").val($("#to_id").val())
</script>