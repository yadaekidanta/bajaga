<x-web-layout title="{{$userAddress ? 'Ubah' : 'Detail Alamat'}}" keyword="Bajaga Store">
    <div class="bg-white pt-3 container-custom">
        <form id="form_address" class="mb-4">
            @if($userAddress)
                <input type="hidden" id="city_id_tmp" value="{{$userAddress->city_id}}">
                <input type="hidden" id="subdistrict_id_tmp" value="{{$userAddress->subdistrict_id}}">
            @endif
            <div class="px-3 d-flex flex-column gap-3">
                <div class="form-group m-0">
                    <label for="address" class="m-0">Alamat</label>
                    <input required type="text" id="address" name="address" class="form-control" value="{{$userAddress ? $userAddress->address: ''}}">
                    @error('address')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                
                <div class="form-group m-0">
                    <label for="deskripsi" class="m-0">Deskripsi</label>
                    <textarea required type="text" id="deskripsi" name="deskripsi" class="form-control">{{$userAddress ? $userAddress->desc : ''}}</textarea>
                    @error('deskripsi')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group m-0">
                    <label for="postcode" class="m-0">Kode pos</label>
                    <input required type="text" id="postcode" name="postcode" class="form-control" value="{{$userAddress ? $userAddress->postcode: ''}}">
                    @error('postcode')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group m-0">
                    <label for="province" class="m-0">Provinsi</label>
                    <select required class="select2 form-control" id="province" style="width: 100%;" name="province">
                        @if($userAddress)
                            @foreach($provinsi as $p)
                                <option value="{{$p->id}}" {{ $userAddress->province_id == $p->id ? 'selected': ''}}>{{$p->name}}</option>
                            @endforeach
                        @else
                            @foreach($provinsi as $p)
                                <option value="{{$p->id}}">{{$p->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    @error('province')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group m-0">
                    <label for="city" class="m-0">Kota / Kabupaten</label>
                    <select required class="select2 form-control" id="city" style="width: 100%;" name="city">
                    </select>
                    @error('city')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group m-0">
                    <label for="subdistrict" class="m-0">Kecamatan</label>
                    <select required class="select2 form-control" id="subdistrict" style="width: 100%;" name="subdistrict">
                    </select>
                    @error('subdistrict')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                @if($userAddress)
                    <button id="submit" onclick="handle_save('#submit', '#form_address', '{{route('web.user-address.update', $userAddress->id)}}', 'PATCH', null, 'Simpan')" class="btn btn-primary mt-3">Simpan</button>
                @else
                    <button id="submit" onclick="handle_save('#submit', '#form_address', '{{route('web.user-address.store')}}', 'POST', null,  'Simpan')" class="btn btn-primary mt-3">Simpan</button>
                @endif

            </div>
        </form>
    </div>
    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $("#submit").prop("disabled", true)

        $(document).ready(function() {
            $('.select2').select2({
                width: 'resolve',
                height: 'resolve',
            });
        });
        if ($("#province:selected")) {
            // console.log($("#province").val());
            $.ajax({
                type: "POST",
                url: "{{route('web.regional.city')}}",
                data: {province : $("#province").val()},
                success: function(response){
                    $("#city").html(response);
                    Array.from($("#city").children()).forEach(element => {
                        if (element.value == $("#city_id_tmp").val()) {
                            element.selected = true
                        }
                    });

                    if ($("#city:selected")) {
                        $.ajax({
                            type: "POST",
                            url: "{{route('web.regional.subdistrict')}}",
                            data: {city : $("#city").val()},
                            success: function(response){
                                $("#subdistrict").html(response);
                                Array.from($("#subdistrict").children()).forEach(element => {
                                    // console.log(element.value == $("#subdistrict_id_tmp").val(), element.value, $("#subdistrict_id_tmp").val());
                                    if (element.value == $("#subdistrict_id_tmp").val()) {
                                        element.selected = true
                                    }
                                });
                                $("#submit").prop("disabled", false)
                            }
                        });
                    }
                }
            });
        }
        $("#province").change(function(){
            $.ajax({
                type: "POST",
                url: "{{route('web.regional.city')}}",
                data: {province : $("#province").val()},
                success: function(response){
                    $("#city").html(response);
                }
            });
        });
        
        $("#city").change(function(){
            $.ajax({
                type: "POST",
                url: "{{route('web.regional.subdistrict')}}",
                data: {city : $("#city").val()},
                success: function(response){
                    $("#subdistrict").html(response);
                }
            });
        });

        // window.localStorage.setItem('pathBack', '');

        
    </script>
    @endsection
</x-web-layout>