@foreach($address as $a)
    <div class="card">
        <div class="card-body hover-bg-aliceblue row align-items-center" onclick="is_use('{{route('web.user-address.is_use', $a->id)}}', this)" role="button">
            <div class="col-10">
                <div>
                    <span class="font-sm">{{$user->name}}</span>
                    @if ($a->is_use == 1)
                        <span class="font-sm text-primary">[Utama]</span>
                    @endif
                </div>

                <div class="d-flex flex-column text-box mt-1" data-maxlength="25">
                    <span class="font-sm text-secondary">{{$a->users->phone}}</span>
                    <span class="font-sm text-secondary">{{$a->users->address}}</span>
                    <span class="font-sm text-secondary text-capitalize">{{$a->subdistrict->name}}, {{$a->city->name}}, {{$a->province->name}}</span> 
                </div>
            </div>

            <div class="col h-100">
                <div class="d-flex align-items-center h-100">
                    <input type="radio" name="alamat" class="form-custom-radio is_use" {{$a->is_use == 1 ? 'checked': ''}}>
                    <input type="hidden" value="{{$a->id}}">
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-center">
                <a href="{{route('web.user-address.edit', $a->id)}}" class="text-black font-sm">Ubah Alamat</a>
            </div>
        </div>
    </div>
    @endforeach