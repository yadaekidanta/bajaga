<x-web-layout title="Profile" keyword="Bajaga Store">
    <div class="container-custom px-3 py-2 bg-white">
        <div class="d-flex flex-column gap-2">
            
            <a href="{{route('web.user-address.create')}}" class="px-3 py-2 border rounded d-flex justify-content-between align-items-center bg-white hover-bg-aliceblue">
                <span class="font-sm">Tambah alamat baru</span>
                <i class="icon-plus"></i>
            </a>
    
            <!-- card list address -->
            <div class="d-flex flex-column gap-3" id="list_result">
            </div>
            <!-- /card list address -->
        </div>
    </div>
    @section('custom_js')
    <script>
        load_list(1)
        let que = url.search.split('from=')[1]
        que = que == 'home' || que == undefined ? '/' : `/${que}`

        function changeUse(){
            console.log();
        }

        function is_use(url, el) {
            $.ajax({
                type: 'PATCH',
                url: url,
                dataType: 'json',
                processData: false,
                success: function(response){
                    // console.log(response);
                    if(response.alert == 'success'){
                        Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    }
                    load_custom('#list_result_address', '{{route('web.user-address.getDataAddress')}}', 1)
                    // $(el).find(".is_use").prop("checked", true)
                    load_list(1)
                },
                error: function(response) {
                    Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    load_list(1)
                }
            })
        }

    </script>
    @endsection
</x-web-layout>