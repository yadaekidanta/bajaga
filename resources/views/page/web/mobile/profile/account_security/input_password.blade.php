<x-web-layout title="Detail Alamat" keyword="Bajaga Store">
    <div class="container-custom bg-white pt-3">
        @if(session()->has('success'))
            <div class="alert alert-success mx-3 mb-3">
                {{session()->get('success')}}
            </div>
        @endif
        <div class="d-flex flex-column mb-4 px-3">
            <h2>Kata Sandi Baru</h2>
    
            <span class="mt-2 text-secondary">Buat kata sandi yang kuat untuk akun dengan email {{$user->email}}</span>
    
            <form action="{{route('web.profile.account_security.password')}}" method="POST" class="mt-5">
                @csrf
                <div class="d-flex flex-column gap-3">
                    <div class="gap-4 form-group">
                        <label for="password">Sandi Baru</label>
                        <div class="position-relative">
                            <input type="password" id="password" aria-describedby="passwordHelp" name="password" class="form-control">
                            <svg class="position-absolute eye-hidden-password" style="top: 0.5em; right: 1em;opacity: .4;" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M11.885 14.988l3.104-3.098.011.11c0 1.654-1.346 3-3 3l-.115-.012zm8.048-8.032l-3.274 3.268c.212.554.341 1.149.341 1.776 0 2.757-2.243 5-5 5-.631 0-1.229-.13-1.785-.344l-2.377 2.372c1.276.588 2.671.972 4.177.972 7.733 0 11.985-8.449 11.985-8.449s-1.415-2.478-4.067-4.595zm1.431-3.536l-18.619 18.58-1.382-1.422 3.455-3.447c-3.022-2.45-4.818-5.58-4.818-5.58s4.446-7.551 12.015-7.551c1.825 0 3.456.426 4.886 1.075l3.081-3.075 1.382 1.42zm-13.751 10.922l1.519-1.515c-.077-.264-.132-.538-.132-.827 0-1.654 1.346-3 3-3 .291 0 .567.055.833.134l1.518-1.515c-.704-.382-1.496-.619-2.351-.619-2.757 0-5 2.243-5 5 0 .852.235 1.641.613 2.342z"/></svg>
                            <svg class="position-absolute eye-show-password d-none" style="top: 0.5em; right: 1em;opacity: .4;" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15 12c0 1.654-1.346 3-3 3s-3-1.346-3-3 1.346-3 3-3 3 1.346 3 3zm9-.449s-4.252 8.449-11.985 8.449c-7.18 0-12.015-8.449-12.015-8.449s4.446-7.551 12.015-7.551c7.694 0 11.985 7.551 11.985 7.551zm-7 .449c0-2.757-2.243-5-5-5s-5 2.243-5 5 2.243 5 5 5 5-2.243 5-5z"/></svg>
                        </div>
                        <small id="passwordHelp" class="form-text text-muted">Minimum 8 karakter</small>
                    </div>
    
                    <div class="gap-4 form-group">
                        <label for="confirmation_password">Ketik Ulang Sandi</label>
                        <div class="position-relative">
                            <input type="password" id="confirmation_password" name="password_confirmation " class="form-control">
                            <svg class="position-absolute eye-hidden-confirmation-password" style="top: 0.5em; right: 1em;opacity: .4;" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M11.885 14.988l3.104-3.098.011.11c0 1.654-1.346 3-3 3l-.115-.012zm8.048-8.032l-3.274 3.268c.212.554.341 1.149.341 1.776 0 2.757-2.243 5-5 5-.631 0-1.229-.13-1.785-.344l-2.377 2.372c1.276.588 2.671.972 4.177.972 7.733 0 11.985-8.449 11.985-8.449s-1.415-2.478-4.067-4.595zm1.431-3.536l-18.619 18.58-1.382-1.422 3.455-3.447c-3.022-2.45-4.818-5.58-4.818-5.58s4.446-7.551 12.015-7.551c1.825 0 3.456.426 4.886 1.075l3.081-3.075 1.382 1.42zm-13.751 10.922l1.519-1.515c-.077-.264-.132-.538-.132-.827 0-1.654 1.346-3 3-3 .291 0 .567.055.833.134l1.518-1.515c-.704-.382-1.496-.619-2.351-.619-2.757 0-5 2.243-5 5 0 .852.235 1.641.613 2.342z"/></svg>
                            <svg class="position-absolute eye-show-confirmation-password d-none" style="top: 0.5em; right: 1em;opacity: .4;" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15 12c0 1.654-1.346 3-3 3s-3-1.346-3-3 1.346-3 3-3 3 1.346 3 3zm9-.449s-4.252 8.449-11.985 8.449c-7.18 0-12.015-8.449-12.015-8.449s4.446-7.551 12.015-7.551c7.694 0 11.985 7.551 11.985 7.551zm-7 .449c0-2.757-2.243-5-5-5s-5 2.243-5 5 2.243 5 5 5 5-2.243 5-5z"/></svg>
                        </div>
                    </div>
    
                    <button type="submit" class="btn btn-primary mt-3">Simpan</button>
                </div>
            </form>
        </div>
    </div>

    @section('custom_js')
    <script>
        $('.eye-hidden-password').click(()=>{
            $('.eye-show-password').removeClass('d-none')
            $('.eye-hidden-password').addClass('d-none')
            $('#password').after(`<input type="text" id="password" name="password" class="form-control" value="${$('#password').val()}">`)
            $('#password').remove()
        })

        $('.eye-show-password').click(()=>{
            $('.eye-hidden-password').removeClass('d-none')
            $('.eye-show-password').addClass('d-none')
            $('#password').after(`<input type="password" id="password" name="password" class="form-control" value="${$('#password').val()}">`)
            $('#password').remove()
        })

        $('.eye-hidden-confirmation-password').click(()=>{
            $('.eye-show-confirmation-password').removeClass('d-none')
            $('.eye-hidden-confirmation-password').addClass('d-none')
            $('#confirmation_password').after(`<input type="text" id="confirmation_password" name="password_confirmation " class="form-control" value="${$('#confirmation_password').val()}">`)
            $('#confirmation_password').remove()
        })

        $('.eye-show-confirmation-password').click(()=>{
            $('.eye-hidden-confirmation-password').removeClass('d-none')
            $('.eye-show-confirmation-password').addClass('d-none')
            $('#confirmation_password').after(`<input type="password" id="confirmation_password" name="password_confirmation " class="form-control" value="${$('#confirmation_password').val()}">`)
            $('#confirmation_password').remove()
        })
    </script>
    @endsection

</x-web-layout>