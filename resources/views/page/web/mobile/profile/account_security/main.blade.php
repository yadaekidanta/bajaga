<x-web-layout title="Keamanan Akun" keyword="Bajaga Store">
    <div class="container-custom bg-white pt-3">
        <div class="d-flex flex-column px-3">
            <a href="{{route('web.profile.account_security.password')}}" class="d-flex align-items-center justify-content-between">
                <span class="text-secondary">Ubah Kata Sandi</span>
                <svg width="18" height="18" viewBox="0 0 8 14" xmlns="http://www.w3.org/2000/svg"><path fill="#BDBDBD" d="M0 2L1.5.5 8 7l-6.5 6.5L0 12l5-5z" fill-rule="evenodd"/></svg>
            </a>
        </div>
    </div>
</x-web-layout>