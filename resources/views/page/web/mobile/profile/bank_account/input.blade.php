<x-web-layout title="Profile" keyword="Bajaga Store">
        <div class="container-custom bg-dark-mode px-3 py-3 bg-white">
            <div class="mt-3">
                <form id="form_input">
                    <div data-bs-toggle="offcanvas" data-bs-target="#canvasListBank" aria-controls="canvasListBank" role="button">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="m-0 font-thin mb-1" id="type-bank-title">Pilih nama bank</h3>
                            <ion-icon style="transform: rotate(270deg);font-size: 24px;" name="chevron-back-outline"></ion-icon>
                        </div>
                        <hr class="m-0">
                        <input type="hidden" class="bank" name="bank" required>
                        <input type="hidden" class="code_bank" name="code_bank" required>
                    </div>

                    <div>
                        <div class="d-flex gap-2 flex-column mt-2">
                            <div class="form-group m-0">
                                <input type="text" autocomplete="off" autofocus placeholder="Nama pemilik" name="account_holder_name" class="form-control w-100 py-2">
                            </div>
                            <div class="form-group m-0 mt-1">
                                <input type="number" placeholder="Nomor Rekening" name="account_number" id="account_number" class="form-control w-100 py-2">
                            </div>
                            <div class="form-group m-0 mt-1">
                                <input type="text" id="branch_name" name="branch_name" placeholder="Nama cabang" class="form-control w-100 py-2">
                            </div>
                            <input type="hidden" id="bank_id_to" name="bank_id">
                            <div class="form-group m-0 mt-2">
                            <button type="button" id="btn_submit" onclick="handle_save('#btn_submit', '#form_input', '{{route('web.profile.bank_account.store')}}', 'POST')" class="btn btn-outline-primary w-100">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- canvas list bank -->
        <div class="offcanvas offcanvas-bottom h-90" tabindex="-1" id="canvasListBank" aria-labelledby="canvasListBankLabel">
            <div class="offcanvas-header">
                <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Pilih nama bank</h3>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body small">
                <!-- sampe mau bikin pencarian di filter -->
                @php
                $list_bank = [
                    'BCA (PT. BCA (BANK CENTRAL ASIA) TBK)',
                    'BNI (PT. BANK NEGARA INDONESIA (BNI) (PERSERO))',
                    'BRI (PT. BANK RAKYAT INDONESIA (BRI) (PERSERO))',
                    'CIMB NIAGA (PT. BANK CIMB NIAGA TBK)',
                    'CITIBANK (CITIBANK NA)',
                    'BANK DANAMON (PT. BANK DANAMON INDONESIA TBK)',
                    'BANK MANDIRI (PT. BANK MANDIRI)',
                    'BANGKOK BANK (BANGKOK BANK PUBLIC CO. LTD)',
                    'HANA BANK (BANK HANA)',
                    'BJB SYARIAH (BANK JABAR BANTEN SYARIAH)',
                    'JATIM UUS (BANK JATIM UNIT USAHA SYARIAH)',
                    'MNC BANK (BANK MNC INTERNASIONAL)',
                    'BOA (BANK OF AMERICA NA)',
                    'BOC (BANK OF CHINA LIMITED)',
                    'BANK SHINHAN INDONESIA',
                    'BPD JATIM (BPD JATIM)',
                    'BPD KALSEL (BPD KALIMANTAN SELATAN)',
                    'BPD KALSEL UUS (BPD KALIMANTAN SELATAN UUS)',
                    'BPD KALTIM UUS (BPD KALTIM UUS)',
                    'BPD LAMPUNG (BPD LAMPUNG)',
                    'BPD NTT (BPD NUSA TENGGARA TIMUR)',
                    'BPD SUMBAR (BPD SUMATERA BARAT (BANK NAGARI))',
                    'BPD SUMBAR UUS (BPD SUMATERA BARAT (NAGARI) UUS)',
                    'BPD SUMSEL DAN BABEL',
                    'BPD DIY (BPD YOGYAKARTA)',
                    'DGZ (DEUTSCHE BANK AG)',
                    'HSBC UUS (HSBC UNIT USAHA SYARIAH)',
                    'EXIMBANK (INDONESIA EXIMBANK)',
                    'CHASE BANK (JPMORGAN CHASE BANK NA)',
                    'BANK AGRONIAGA (PT. AGRONIAGA BANK)',
                    'PT.ANGLOMAS INTERNATIONAL BANK',
                    'ANZ PANIN BANK (PT. ANZ PANIN BANK)',
                    'ARTAJASA (PT. ARTAJASA PEMBAYARAN ELEKTRONIK)',
                    'BANK AGRIS (PT. BANK AGRIS)',
                    'BANK ANDARA (PT. BANK ANDARA)',
                    'BAD (PT. BANK ANTAR DAERAH)',
                    'BANK ARTHA GRAHA (PT. BANK ARTHA GRAHA INT TBK)',
                    'BCA SYARIAH (PT. BANK BCA SYARIAH)',
                    'BANK BISNIS (PT. BANK BISNIS INTERNATIONAL)',
                    'BPD ACEH UUS (PT. BANK BPD ACEH UUS)',
                    'BBA (PT. BANK BUMI ARTA)',
                    'BANK CAPITAL (PT. BANK CAPITAL INDONESIA)',
                    'PT. BANK CHINATRUST INDONESIA',
                    'CIMB NIAGA UUS (PT. BANK CIMB NIAGA TBK UUS)',
                    'PT. BANK COMMONWEALTH',
                    'BANK DANAMON UUS (PT. BANK DANAMON IND. UU SYARIAH)',
                    'DBS (PT. BANK DBS INDONESIA)',
                    'PT. BANK DIPO INTERNATIONAL',
                    'BANK DKI (PT. BANK DKI)',
                    'DKI UUS (PT. BANK DKI UNIT USAHA SYARIAH)',
                    'BANK EKONOMI (PT. BANK EKONOMI RAHARJA)',
                    'PT. BANK FAMA INTERNATIONAL',
                    'BANK GANESHA (PT. BANK GANESHA)',
                    'BHI (PT. BANK HARDA INTERNASIONAL)',
                    'ICBC (PT. BANK ICBC INDONESIA)',
                    'BANK INA (PT. BANK INA PERDANA)',
                    'BANK INDEX (PT. BANK INDEX SELINDO)',
                    'BJB (PT. BANK JABAR DAN BANTEN)',
                    'JAGO (PT. Bank Jago Tbk)',
                    'BJJ (PT. BANK JASA JAKARTA)',
                    'BANK KALBAR UUS (PT. BANK KALBAR UUS)',
                    'BANK KEB (PT. BANK KEB INDONESIA)',
                    'BANK KESAWAN (PT. BANK KESAWAN)',
                    'SEABANK (PT. BANK KESEJAHTERAAN EKONOMI (SeaBank))',
                    'PT. BANK LIMAN INTERNATIONAL',
                    'BANK MASPION (PT. BANK MASPION INDONESIA)',
                    'BANK MAYAPADA (PT. BANK MAYAPADA INTERNASIONAL T)',
                    'MAYBANK (PT BANK MAYBANK INDONESIA TBK)',
                    'MAYBANK SYARIAH (PT. BANK MAYBANK SYARIAH INDONESIA)',
                    'BANK MAYORA (PT. BANK MAYORA INDONESIA)',
                    'BANK MEGA (PT. BANK MEGA TBK)',
                    'PT. BANK MESTIKA DHARMA',
                    'BANK MITRANIAGA (PT. BANK MITRANIAGA)',
                    'MIZUHO BANK (PT. BANK MIZUHO INDONESIA)',
                    'BANK MUAMALAT (PT. BANK MUAMALAT INDONESIA)',
                    'BANK MAS (PT. BANK MULTIARTA SENTOSA)',
                    'BANK MUTIARA (PT. BANK MUTIARA TBK)',
                    'NOBU BANK (PT. BANK NATIONALNOBU)',
                    'NTB UUS (PT. BANK NTB UNIT USAHA SYARIAH)',
                    'PT. BANK NUSANTARA PARAHYANGAN',
                    'OCBC NISP (PT. BANK OCBC NISP TBK)',
                    'PT. BANK OCBC NISP TBK SYARIAH',
                    'PANIN BANK (PT. BANK PAN INDONESIA TBK (PANIN))',
                    'PT. BANK PANIN SYARIAH',
                    'BPD JAMBI (PT. BANK PEMBANGUNAN DAERAH JAMBI)',
                    'BPD PAPUA (PT. BANK PEMBANGUNAN DAERAH PAPUA)',
                    'BANK PERMATA (PT. BANK PERMATA TBK)',
                    'PT. BANK PERMATA TBK SYARIAH',
                    'BANK PUNDI (PT. BANK PUNDI INDONESIA)',
                    'RABOBANK (PT. BANK RABOBANK INTERNATIONAL IND)',
                    'PT. BANK RESONA PERDANIA',
                    'PT. BANK ROYAL INDONESIA',
                    'BANK SAHABAT (PT. BANK SAHABAT PURBA DANARTA)',
                    'BANK SAMPOERNA (PT. BANK SAHABAT SAMPOERNA)',
                    'SBI (PT. BANK SBI INDONESIA)',
                    'PT. BANK SINAR HARAPAN BALI',
                    'BANK SINARMAS (PT. BANK SINARMAS)',
                    'PT. BANK SINARMAS SYARIAH',
                    'BANK SULSELBAR (PT. BANK SULSELBAR)',
                    'PT. BANK SULSELBAR SYARIAH',
                    'BANK SULUT (PT. BANK SULUT)',
                    'SMBCI (PT. BANK SUMITOMO MITSUI INDONESIA)',
                    'BPD SUMUT (PT. BANK SUMUT (BPD SUMUT))',
                    'BANK SWADESI (PT. BANK SWADESI TBK)',
                    'PT. BANK SYARIAH BUKOPIN',
                    'BSM (PT. BANK SYARIAH INDONESIA)',
                    'BSI (PT. BANK SYARIAH INDONESIA)',
                    'BSMI (PT. BANK SYARIAH MEGA INDONESIA)',
                    'BTN (PT. BANK TABUNGAN NEGARA (BTN) (PERSERO))',
                    'BTPN (PT. BANK TABUNGAN PENSIUNAN NASIONAL)',
                    'BTPN SYARIAH (PT. BANK TABUNGAN PENSIUNAN NASIONAL (BTPN) SYARIAH)',
                    'UOB INDONESIA (PT. BANK UOB INDONESIA)',
                    'BANK VICTORIA (PT. BANK VICTORIA INTERNATIONAL)',
                    'PT. BANK VICTORIA SYARIAH',
                    'BANK WINDU (PT. BANK WINDU KENTJANA INTERNATIONAL TBK)',
                    'BWS (PT. BANK WOORI INDONESIA)',
                    'PT. BANK YUDHA BHAKTI',
                    'BANK SAUDARA (PT. B. HIMPUNAN SAUDARA 1906 TBK)',
                    'BII (PT. BII TBK. SYARIAH)',
                    'PT. BNP PARIBAS INDONESIA',
                    'BPD ACEH (PT. BPD ACEH)',
                    'BPD BALI (PT. BPD BALI)',
                    'BPD BENGKULU (PT. BPD BENGKULU)',
                    'BPD DIY SYARIAH (PT. BPD DIY SYARIAH)',
                    'BPD JATENG (PT. BPD JAWA TENGAH)',
                    'BPD JATENG (PT. BPD JAWA TENGAH UUS)',
                    'BPD KALBAR (PT. BPD KALIMANTAN BARAT)',
                    'BPD KALTIM (PT. BPD KALIMANTAN TIMUR)',
                    'BPD KALTENG (PT. BPD KALTENG)',
                    'BPD MALUKU (PT. BPD MALUKU)',
                    'BPD NTB (PT. BPD NUSA TENGGARA BARAT)',
                    'BPD RIAU (PT. BPD RIAU)',
                    'BPD RIAU UUS (PT. BPD RIAU UNIT USAHA SYARIAH)',
                    'BPD SULTENG (PT.BPD SULAWESI TENGAH)',
                    'BPD SULTRA (PT. BPD SULAWESI TENGGARA)',
                    'PT. BPD SUMSEL DAN BABEL SYARIAH',
                    'PT. BPD SUMUT SYARIAH',
                    'BTN UUS (PT. BTN (PERSERO) UUS)',
                    'BUKOPIN (PT. BUKOPIN)',
                    'PT. CENTRATAMA NASIONAL BANK',
                    'KSEI (PT. KUSTODIAN SENTRAL EFEK INDONESIA)',
                    'PT. PRIMA MASTER BANK',
                    'SCB (STANDARD CHARTERED BANK)',
                    'THE BOT MITSUBISHI UFJ LTD',
                    'HSBC (THE HONGKONG AND SHANGHAI BANK (HSBC))',
                    'RBS (THE ROYAL BANK OF SCOTLAND N.V.)' 
                ];

                $list_available = [
                    [
                        "name" => "Bank Central Asia",
                        "code" => "BCA",
                        "is_activated" => false
                    ],
                    [
                        "name" => "Bank Negara Indonesia",
                        "code" => "BNI",
                        "is_activated" => true
                    ],
                    [
                        "name" => "Bank Mandiri",
                        "code" => "MANDIRI",
                        "is_activated" => true
                    ],
                    [
                        "name" => "Bank Permata",
                        "code" => "PERMATA",
                        "is_activated" => true
                    ],
                    [
                        "name" => "Bank Sahabat Sampoerna",
                        "code" => "SAHABAT_SAMPOERNA",
                        "is_activated" => false
                    ],
                    [
                        "name" => "Bank Rakyat Indonesia",
                        "code" => "BRI",
                        "is_activated" => true
                    ],
                    [
                        "name" => "Bank CIMB Niaga",
                        "code" => "CIMB",
                        "is_activated" => false
                    ],
                    [
                        "name" => "Bank Syariah Indonesia",
                        "code" => "BSI",
                        "is_activated" => true
                    ],
                    [
                        "name" => "Bank Jabar Banten",
                        "code" => "BJB",
                        "is_activated" => true
                    ]
                ];
                @endphp
                @foreach($master as $bank)
                <div class="d-flex justify-content-between align-items-center input-group py-4 hover-opacity-65" data-filter="*">
                    <span class="font-bold font-lg">{{Str::limit($bank['name'], 30)}}</span>
                    <input type="radio" name="all" class="form-custom-radio">
                    <input type="hidden" id="code" name="code" value="{{$bank['code']}}">
                    <input type="hidden" id="bank_id" name="bank_id" value="{{$bank['id']}}">
                </div>
                <hr class="m-0">
                @endforeach
            </div>
        </div>
    @section('custom_js')
    <script>
        $(document).ready(function () {
            $(".offcanvas-body").on("click", "div", function(){
                $('.offcanvas-body div input').prop("checked", false)
                $(this).children("input").prop("checked", true)
                $("#type-bank-title").html($(this).children("span").text())
                $("#canvasListBank").offcanvas('hide');
                
                let bank_name = $(this).children("span").text()
                let code =  $(this).find("#code").val()
                let bank_id =  $(this).find("#bank_id").val()
                $(".bank").val(bank_name)
                $(".code_bank").val(code)
                $("#bank_id_to").val(bank_id)
                
            });
        })
    </script>
    @endsection
    <!-- /canvas list bank -->
</x-web-layout>