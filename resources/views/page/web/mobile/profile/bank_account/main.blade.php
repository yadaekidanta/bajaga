<x-web-layout title="Profile" keyword="Bajaga Store">
    <div class="container-custom px-3 py-3 bg-dark-mode bg-white">
        <!-- list bank account -->
        <div id="list_result">
        </div>
        <!-- list bank account -->
    </div>


    <!-- canvas syarat dan ketentuan -->
    <div class="offcanvas offcanvas-bottom h-90" tabindex="-1" id="canvasInfoBank" aria-labelledby="canvasInfoBankLabel">
        <div class="offcanvas-header">
            <h3 class="offcanvas-title font-bold" id="offcanvasBottomLabel">Syarat & Ketentuan</h3>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body small">
            <span>Syarat dan Ketentuan Pendaftaran Rekening Bank di Bajaga :</span>
            <ol>
                <li>Setiap Pengguna hanya dapat memiliki 3 (tiga) rekening Bank yang terdaftar pada akun Bajaga Pengguna. Pengguna memahami dan menyetujui bahwa Pengguna bertanggung jawab penuh atas penggunaan rekening Bank yang didaftarkan tersebut.</li>
                <li>Apabila Pengguna melakukan perubahan akun rekening Bank pada akun Bajaga Pengguna, maka rekening Bank utama tidak dapat diubah karena alasan keamanan.</li>
                <li>Pendaftaran akun Bank dapat digunakan untuk melakukan penarikan dana dari saldo refund, saldo penghasilan, dan/atau saldo mitra Pengguna.</li>
                <li>Bajaga berhak untuk melakukan pembatasan Pendaftaran Rekening Bank, pembekuan dan/atau penghapusan akun Bank Pengguna sesuai dengan kebijakan yang ditentukan oleh Bajaga, dalam hal ditemukan adanya dugaan pelanggaran terhadap syarat dan ketentuan Bajaga, kecurangan, manipulasi atau kejahatan yang dilakukan oleh Pengguna pada Situs/Aplikasi.</li>
            </ol>
        </div>
    </div>
    <!-- /canvas syarat dan ketentuan -->

    @section("custom_js")
    <script>
        load_list(1)
    </script>
    @endsection
</x-web-layout>