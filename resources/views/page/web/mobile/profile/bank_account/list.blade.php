@if (count($collections) == 0)
    <span class="text-primary text-center w-100 d-flex justify-content-between mt-4">Akun rekening masih kosong, ayok tambahkan supaya transaksi lebih mudah!</span>
@else
    @foreach($collections as $collection)
        <div class="">
            <div class="d-flex justify-content-between align-items-center">
                <div class="col-8 p-0">
                    <span class="font-bold">{{$collection->banks->name}}</span>
                    <span class="font-sm d-block">{{$collection->account_number}}</span>
                    <span class="font-sm d-block">a/n Sdr {{$collection->account_holder_name}}</span>
                    <span class="font-sm d-block">Kantor Cabang {{$collection->branch_name}}</span>
                </div>
                <div class="">
                    <img src="{{asset('storage/'.$collection->banks->thumbnail)}}" class="w-15" alt="">
                </div>
            </div>

            <div class="w-100 d-flex justify-content-end">
                <button class="btn btn-sm btn-outline-secondary" onclick="handle_confirm('Anda yakin, hapus akun bank?', 'Yes', 'No', 'POST', '{{route('web.profile.bank_account.destroy', $collection->id)}}', true)">Hapus</button>
            </div>

            <hr>
        </div>
    @endforeach
@endif