<x-web-layout title="Profile" keyword="Bajaga Store">
    <div class="px-3">
        <div class="d-flex justify-content-between">
            <div class="d-flex gap-3">
                <div class="profile-avatar">
                    <img class="w-full" src="{{asset($user->avatar != null ? 'storage/'.$user->avatar : 'img/avatars/admin.png')}}" alt="avatar user">
                </div>
        
                <div>
                    <h2 class="font-lg font-bold m-0 mb-1">{{Str::limit($user->name, 20)}}</h2>
                    <h3 class="font-sm m-0 font-thin">{{$user->phone}}</h3>
                    <h3 class="font-sm m-0 font-thin">{{Str::limit($user->email)}}</h3>
                </div>
            </div>

            <a href="{{route('web.profile.edit', $user->id)}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M14.078 7.061l2.861 2.862-10.799 10.798-3.584.723.724-3.585 10.798-10.798zm0-2.829l-12.64 12.64-1.438 7.128 7.127-1.438 12.642-12.64-5.691-5.69zm7.105 4.277l2.817-2.82-5.691-5.689-2.816 2.817 5.69 5.692z"/></svg>
            </a>
        </div>
    
        <!-- Points -->
        <div>
            <h2 class="font-lg mt-3">Poin Toko & Poin User</h2>
            <div class="my-2 shadow card-dark-mode bg-white rounded-lg p-2 d-flex justify-content-around gap-3">
                    @if($available == 2)
                        <a href="{{route('web.open-store.index')}}" class="d-flex align-items-center gap-2 flex-column">
                            <svg class="icon-size-3" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M10 9v-1.098l1.047-4.902h1.905l1.048 4.9v1.098c0 1.067-.933 2.002-2 2.002s-2-.933-2-2zm5 0c0 1.067.934 2 2.001 2s1.999-.833 1.999-1.9v-1.098l-2.996-5.002h-1.943l.939 4.902v1.098zm-10 .068c0 1.067.933 1.932 2 1.932s2-.865 2-1.932v-1.097l.939-4.971h-1.943l-2.996 4.971v1.097zm-4 2.932h22v12h-22v-12zm2 8h18v-6h-18v6zm1-10.932v-1.097l2.887-4.971h-2.014l-4.873 4.971v1.098c0 1.066.933 1.931 2 1.931s2-.865 2-1.932zm15.127-6.068h-2.014l2.887 4.902v1.098c0 1.067.933 2 2 2s2-.865 2-1.932v-1.097l-4.873-4.971zm-.127-3h-14v2h14v-2z"/></svg>
                            <div class="d-flex flex-column align-items-center text-dark">
                                <span class="font-bold font-thin font-sm m-0">Poin Toko</span>
                                <span class="font-bold font-sm -mt-1">{{$point_store}}</span>
                            </div>
                        </a>
                    @elseif($available == 0)
                        <a href="{{route('web.auth.index')}}" class="d-flex align-items-center gap-2 flex-column">
                            <ion-icon name="log-in-outline" style="font-size: 30px;"></ion-icon>
                            <span class="font-bold font-thin font-sm m-0 text-black">Masuk / Daftar</span>
                        </a>
                    @else
                        <a href="{{route('web.open-store.index')}}" class="d-flex align-items-center gap-2 flex-column">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon-size-3" fill="#4879D3" viewBox="0 0 24 24"><path d="M21 13v10h-21v-19h12v2h-10v15h17v-8h2zm3-12h-10.988l4.035 4-6.977 7.07 2.828 2.828 6.977-7.07 4.125 4.172v-11z"/></svg>
                            <div class="d-flex flex-column align-items-center">
                                <span class="font-bold font-thin font-sm m-0 text-black">Buka Toko</span>
                            </div>
                        </a>
                    @endif

        
                    <div class="d-flex align-items-center gap-2 flex-column">
                        <svg class="icon-size-3" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.326 18.266l-4.326-2.314-4.326 2.313.863-4.829-3.537-3.399 4.86-.671 2.14-4.415 2.14 4.415 4.86.671-3.537 3.4.863 4.829z"/></svg>
        
                        <div class="d-flex flex-column align-items-center">
                            <span class="font-bold font-thin font-sm m-0">Poin User</span>
                            <span class="font-bold font-sm -mt-1">{{$point_user}}</span>
                        </div>
                    </div>
            </div>
        </div>
        <!-- /Points -->

        <!-- Kupon Saya -->
        <div class="mb-3">
            <h2 class="font-lg mt-3">Kupon Saya</h2>
            <a href="{{route('web.discount')}}" class="my-2 card-dark-mode shadow bg-white rounded-lg p-2 d-flex align-items-center gap-3">
                    <img class="icon-size-5" src="{{asset('img/other/MyCoupons.png')}}" alt="">
                    <!-- <span class="font-bold font-thin font-sm m-0">Kupon Saya</span> -->
                    <div class="vl"></div>
                    <span class="font-bold font-medium">{{$count_cupon}} Kupon Tersedia</span>
            </a>
        </div>
        <!-- /Member Silver -->
    </div>

    <div class="px-3 bg-white card-dark-mode py-3 mb-3">
        <h2 class="font-lg">Pengaturan Akun</h2>

        <a href="{{route('web.profile.address')}}" class="d-flex align-items-center gap-3 mt-3">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 24 24"><path d="M10 9v-1.098l1.047-4.902h1.905l1.048 4.9v1.098c0 1.067-.933 2.002-2 2.002s-2-.933-2-2zm5 0c0 1.067.934 2 2.001 2s1.999-.833 1.999-1.9v-1.098l-2.996-5.002h-1.943l.939 4.902v1.098zm-10 .068c0 1.067.933 1.932 2 1.932s2-.865 2-1.932v-1.097l.939-4.971h-1.943l-2.996 4.971v1.097zm-4 2.932h22v12h-22v-12zm2 8h18v-6h-18v6zm1-10.932v-1.097l2.887-4.971h-2.014l-4.873 4.971v1.098c0 1.066.933 1.931 2 1.931s2-.865 2-1.932zm15.127-6.068h-2.014l2.887 4.902v1.098c0 1.067.933 2 2 2s2-.865 2-1.932v-1.097l-4.873-4.971zm-.127-3h-14v2h14v-2z"/></svg>
            </div>

            <div class="d-flex flex-column">
                <span class="font-bold font-medium m-0 text-body">Daftar Alamat</span>
                <span class="text-secondary font-sm -mt-0.5">Atur alamat pengiriman belanjaan</span>
            </div>
        </a>

        <a href="{{route('web.profile.bank_account')}}" class="d-flex align-items-center gap-3 mt-2">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" class="unf-icon" viewBox="0 0 24 24" width="21" height="21" fill="#525A67" style="vertical-align:middle" display="inline-block"><path fill-rule="evenodd" clip-rule="evenodd" d="M19 13V5.21C19 2.67 14 2 11 2s-8 .67-8 3.21v13.58C3 21 7 22 11 22c.53 0 1 0 1.57-.05A5.51 5.51 0 0 0 19 13zm-8-9c3.46 0 5.46.79 5.93 1.21-.47.43-2.47 1.21-5.93 1.21-3.46 0-5.49-.78-5.95-1.21C5.51 4.79 7.52 4 11 4zM5 18.72v-2.21c1.694.643 3.488.981 5.3 1 0 .866.206 1.719.6 2.49-3.62 0-5.62-.89-5.9-1.28zm0-4.53c.29.39 2.223 1.217 5.67 1.27a5.54 5.54 0 0 1 1.98-2.44c-.54.06-1.1.06-1.65.06a16.27 16.27 0 0 1-6-1v2.11zm9.5 5.11l-.8-1.2h-.4v1.2h-1v-3.8h1.8a1.69 1.69 0 0 1 1.2.4 1.222 1.222 0 0 1 .3.9 1.21 1.21 0 0 1-.8 1.2l.9 1.3h-1.2zM11 11.05c-3.67 0-5.7-.88-6-1.29V7.45a17.08 17.08 0 0 0 6 1 17.12 17.12 0 0 0 6-1v2.31c-.32.41-2.35 1.29-6 1.29zm7 8.35a1.27 1.27 0 0 1-.9-.4v1.2h-1v-3.8h1v.4a1.15 1.15 0 0 1 .9-.5 1.381 1.381 0 0 1 1.3 1.5c-.02 1-.62 1.6-1.3 1.6zm-4-3h-.7v1h.7c.4 0 .6-.2.6-.5s-.22-.5-.6-.5zm3.26.863a.71.71 0 0 1 .42-.063.67.67 0 0 1 .6.7.71.71 0 1 1-1.02-.637z"/></svg>
            </div>

            <div class="d-flex flex-column">
                <span class="font-bold font-medium m-0 text-body">Rekening Bank</span>
                <span class="text-secondary font-sm -mt-0.5">Tarik Saldo Bajaga ke rekening tujuan</span>
            </div>
        </a>

        <a href="{{route('web.profile.account_security')}}" class="d-flex align-items-center gap-3 mt-2">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" class="unf-icon" viewBox="0 0 24 24" width="21" height="21" fill="#525A67" style="vertical-align:middle" display="inline-block"><path fill-rule="evenodd" clip-rule="evenodd" d="M17 7h1a1.94 1.94 0 0 1 2 2v10a1.929 1.929 0 0 1-2 2H6a1.93 1.93 0 0 1-2-2V9a1.94 1.94 0 0 1 2-2h1V6a4 4 0 0 1 4-4h2a4 4 0 0 1 4 4v1zM9.586 4.586A2 2 0 0 0 9 6v1h6V6a2 2 0 0 0-2-2h-2a2 2 0 0 0-1.414.586zM6 19h12V9H6v10zm4.785-7.096A2 2 0 0 1 12 11.49v.01a2 2 0 0 1 1 3.73V16a1 1 0 0 1-2 0v-.78a2 2 0 0 1-.215-3.316z"/></svg>
            </div>

            <div class="d-flex flex-column">
                <span class="font-bold font-medium m-0 text-body">Keamanan Akun</span>
                <span class="text-secondary font-sm -mt-0.5">Kata sandi</span>
            </div>
        </a>
    </div>

    <div class="px-3 bg-white card-dark-mode py-3 mb-3">
        <h2 class="font-lg">Seputar Bajaga</h2>

        <a href="{{route('web.about')}}" class="d-flex align-items-center gap-3 mt-3">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" class="favicon-mobile-profile" width="19.938" height="30.694" viewBox="0 0 19.938 30.694"><g transform="translate(-94.875 506.107)"><path d="M103.81-505.757a5.407,5.407,0,0,0-2.079.943,6.632,6.632,0,0,0-1.9,2.375l-.133.332-1.517.018c-1.5.018-1.529.018-1.855.175a2,2,0,0,0-1.118,1.36c-.085.326-.091,1.5-.079,10.605l.018,10.237.175.375a2.122,2.122,0,0,0,1.039,1.027,2.381,2.381,0,0,0,.985.181l.653.036.036.393a2.36,2.36,0,0,0,1.3,1.867l.344.169H110.01l.344-.169a2.36,2.36,0,0,0,1.3-1.867l.036-.393.653-.036a2.351,2.351,0,0,0,1.009-.193,2.039,2.039,0,0,0,1.021-1.027l.169-.363.018-10.237c.012-9.1.006-10.279-.079-10.605a2,2,0,0,0-1.118-1.36c-.326-.157-.357-.157-1.843-.175l-1.511-.018-.23-.459a5.424,5.424,0,0,0-1.251-1.71,5.158,5.158,0,0,0-2.671-1.475A6.3,6.3,0,0,0,103.81-505.757Zm1.752,1.994a3.2,3.2,0,0,1,1.583.937,5.02,5.02,0,0,1,.532.586l.073.139h-5.813l.109-.187a4.513,4.513,0,0,1,.532-.592A3.117,3.117,0,0,1,105.563-503.763Zm-6.224,2.913a5.988,5.988,0,0,0-.079,1c-.006.562.012.665.121.816a1,1,0,0,0,1.692-.024,1.449,1.449,0,0,0,.163-.725,6.98,6.98,0,0,1,.115-.852l.073-.314.163.157a1.808,1.808,0,0,1,.115,2.188,1.81,1.81,0,0,1-3.028-.091,1.89,1.89,0,0,1-.109-1.644c.121-.284.749-.961.822-.888A1.155,1.155,0,0,1,99.338-500.85Zm11.47.03a1.8,1.8,0,0,1,.139,2.212,1.8,1.8,0,0,1-2.786.211,1.781,1.781,0,0,1-.133-2.333.765.765,0,0,1,.2-.224,3.571,3.571,0,0,1,.218,1.118c.066.677.314,1.021.828,1.142a1.1,1.1,0,0,0,.834-.248c.369-.314.429-.695.266-1.68-.048-.3-.091-.568-.091-.592C110.282-501.285,110.578-501.061,110.808-500.82Zm-7.947,4.23a3.91,3.91,0,0,0,1.861.429,3.865,3.865,0,0,0,2.689-1l.393-.363v.181a3.334,3.334,0,0,1-.381,1.178,3.519,3.519,0,0,1-1.112,1.027,4.2,4.2,0,0,1-1.589.4,4.2,4.2,0,0,1-1.589-.4,3.259,3.259,0,0,1-1.112-1.051,3.061,3.061,0,0,1-.381-1.19c0-.127.036-.109.387.212A4.272,4.272,0,0,0,102.861-496.59Zm2.6,3.559a3.449,3.449,0,0,1,2.272,1.819l.163.344h.894c.961.006,1.178.06,1.474.381.332.357.32.079.32,6.708,0,5.831-.006,6.134-.115,6.363a1.27,1.27,0,0,1-.628.628c-.236.091-9.759.091-10,0a1.27,1.27,0,0,1-.628-.628c-.109-.23-.115-.532-.115-6.363,0-6.629-.012-6.351.32-6.708.3-.32.514-.375,1.456-.375h.882l.145-.29A3.248,3.248,0,0,1,105.46-493.03Z" transform="translate(0 0)" fill="#fff" stroke="#000" stroke-width="0.5"/><path d="M230.082-274.743a2.281,2.281,0,0,0-.864.532c-.477.471-.562.441,1.348.441,1.62,0,1.692-.006,1.638-.109a2.905,2.905,0,0,0-.864-.743A2.309,2.309,0,0,0,230.082-274.743Z" transform="translate(-125.722 -217.091)" stroke="#707070" stroke-width="0.1"/><path d="M193.584-248.635a1.039,1.039,0,0,0,.78,1.752.991.991,0,0,0,1.027-1.058c0-.26-.157-.653-.278-.689-.073-.024-.145.3-.145.635a.662.662,0,0,1-.743.695.707.707,0,0,1-.314-.212.656.656,0,0,1-.151-.52,3.088,3.088,0,0,1,.054-.574C193.88-248.877,193.831-248.889,193.584-248.635Z" transform="translate(-92.239 -241.488)" stroke="#707070" stroke-width="0.1"/><path d="M285.577-248.776a2.918,2.918,0,0,1,.054.55.54.54,0,0,1-.175.5.589.589,0,0,1-.822.03c-.157-.157-.187-.248-.242-.78-.024-.187-.066-.344-.1-.344-.133,0-.3.4-.3.713a.987.987,0,0,0,1.027,1.051,1.031,1.031,0,0,0,.767-1.752.973.973,0,0,0-.23-.193C285.541-249,285.547-248.9,285.577-248.776Z" transform="translate(-177.464 -241.317)" stroke="#707070" stroke-width="0.1"/><path d="M222-186.868v3.432l.29-.042a2.161,2.161,0,0,0,.592-.187l.308-.151.332.151a2.473,2.473,0,0,0,3.378-2.665,2.48,2.48,0,0,0-1.414-1.843,1.664,1.664,0,0,0-.979-.193c-.3,0-.6.012-.665.024-.109.024-.127-.018-.157-.4a1.532,1.532,0,0,0-1.408-1.511L222-190.3Zm2.967.369a.771.771,0,0,1-.363,1.323.958.958,0,0,1-.8-.344,1.075,1.075,0,0,1,.03-.864A.763.763,0,0,1,224.967-186.5Z" transform="translate(-119.211 -296.47)" stroke="#707070" stroke-width="0.1"/></g></svg>
                <img src="http://127.0.0.1:8000/img/favicon.png" alt="image" class="imaged favicon-mobile-profile w24">
            </div>

            <span class="font-bold font-medium m-0 text-body">Kenali Bajaga</span>
        </a>

        <a href="{{route('web.terms')}}" class="d-flex align-items-center gap-3 mt-3">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" class="unf-icon" viewBox="0 0 24 24" width="21" height="21" fill="#525A67" style="vertical-align:middle" display="inline-block"><path fill-rule="evenodd" clip-rule="evenodd" d="M16 4h2a1.93 1.93 0 0 1 2 2v14.1a1.9 1.9 0 0 1-1.92 1.9H5.88A1.9 1.9 0 0 1 4 20.1V5.9A1.9 1.9 0 0 1 5.88 4H8V3a.94.94 0 0 1 1-1h6a.94.94 0 0 1 1 1v1zM6 6v14h12V6H6zm3 2h6a1 1 0 1 1 0 2H9a1 1 0 0 1 0-2zm6 4H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2zm-6 4h3a1 1 0 0 1 0 2H9a1 1 0 0 1 0-2z"/></svg>
            </div>

            <span class="font-bold font-medium m-0 text-body">Syarat dan Ketentuan</span>
        </a>

        <a href="{{route('web.privacy')}}" class="d-flex align-items-center gap-3 mt-3">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" class="unf-icon" viewBox="0 0 24 24" width="21" height="21" fill="#525A67" style="vertical-align:middle" display="inline-block"><path fill-rule="evenodd" clip-rule="evenodd" d="M16.7 4.1c.7.7 1.3 1.2 2.4 1.2 1.1 0 1.9.9 1.9 2.1v3.5c0 5.5-3.4 8.4-6.8 10.4-.5.5-1.2.7-2.2.7-.921 0-1.588-.34-2-.55l-.1-.05C6.3 19.3 3 16.6 3 10.9V7.4c0-1 .7-2.1 1.9-2.1.8 0 1.4-.4 2.2-1.2l.194-.16C8.354 3.066 9.645 2 12 2c2.5 0 3.7 1.1 4.7 2.1zm-3.6 15.5c4.2-2.5 5.9-5 5.9-8.7V7.3c-1.72 0-2.62-.82-3.443-1.568A25.666 25.666 0 0 0 15.3 5.5C14.5 4.7 13.7 4 12 4c-1.8 0-2.6.7-3.5 1.6l-.02.018C7.584 6.414 6.587 7.3 5 7.3v3.6c0 4.4 2.2 6.6 5.9 8.7.4.3.8.4 1.1.4.5 0 1-.3 1.1-.4zm1.4-9.6h.5c.5 0 1 .4 1 1v3c0 1.1-.9 2-2 2h-4c-1.1 0-2-.9-2-2v-3c0-.6.4-1 1-1h.5v-.5C9.5 8.1 10.6 7 12 7s2.5 1.1 2.5 2.5v.5zM12 9c-.3 0-.5.2-.5.5v.5h1v-.5c0-.3-.2-.5-.5-.5zm-2 3v2h4v-2h-4z"/></svg>
            </div>

            <span class="font-bold font-medium m-0 text-body">Kebijakan Privasi</span>
        </a>
    </div>

    <div class="px-3 bg-white py-3 card-dark-mode mb-3">
        <a href="{{route('web.auth.logout')}}" class="d-flex align-items-center gap-3">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" class="unf-icon" viewBox="0 0 24 24" width="21" height="21" fill="#525A67" style="vertical-align:middle" display="inline-block"><path fill-rule="evenodd" clip-rule="evenodd" d="M5 21h7.5a1.929 1.929 0 0 0 2-2v-2a1 1 0 0 0-2 0v2H5V5h7.5v2a1 1 0 0 0 2 0V5a1.93 1.93 0 0 0-2-2H5a1.93 1.93 0 0 0-2 2v14a1.93 1.93 0 0 0 2 2zM17.71 7.29l4 4a1.001 1.001 0 0 1 0 1.42l-4 4a.999.999 0 0 1-1.42 0 1 1 0 0 1 0-1.42l2.3-2.29H9a1 1 0 1 1 0-2h9.59l-2.3-2.29a1.004 1.004 0 1 1 1.42-1.42z"/></svg>
            </div>

            <span class="font-bold font-medium m-0 text-body">Keluar Akun</span>
        </a>
    </div>
    @section('custom_js')
    <script>
        $("a").click(function(){
            window.localStorage.setItem('pathBack', 'profile')
        })
    </script>
    @endsection
</x-web-layout>