<x-web-layout title="Profile" class="bg-white" keyword="Bajaga Store">
    <div class="bg-white pt-3 bg-dark-mode container-custom">
        <form id="form_input" class="mb-4">
            @csrf
            <div class="d-flex flex-column align-items-center">
                <div class="profile-avatar">
                    <img id="preview_avatar" class="w-full" src="{{asset($user->avatar != null ? 'storage/'.$user->avatar : 'img/avatars/admin.png')}}" alt="avatar user">
                </div>
                <div class="mt-2">
                    <label for="image" class="text-primary">Ubah Foto Profil</label>
                    <input type="file" name="image" id="image" class="d-none form-control">
                </div>
            </div>
    
            <hr>
            <div class="px-3 d-flex flex-column gap-2">
                <h2 class="font-lg">Info Profil</h2>
                <div class="d-flex align-items-center gap-4 form-group">
                    <label for="name" class="label-form-profile">Nama</label>
                    <input type="text" id="name" name="name" class="form-control" value="{{$user->name}}">
                </div>
    
                <div class="d-flex align-items-center gap-4 form-group">
                    <label for="username" class="label-form-profile">Username</label>
                    <input type="text" id="username" name="username" class="form-control" value="{{$user->username}}">
                </div>
    
            </div>
            <hr>
            
            <div class="px-3 d-flex flex-column gap-2">
                <h2 class="font-lg">Info Pribadi</h2>
    
                <div class="d-flex align-items-center gap-4 form-group">
                    <label for="nik" class="label-form-profile">Nik</label>
                    <input type="text" id="nik" name="nik" class="form-control" placeholder="" value="{{$user->nik}}">
                </div>
    
                <div class="d-flex align-items-center gap-4 form-group">
                    <label for="email" class="label-form-profile">Email</label>
                    <input type="email" id="email" name="email" class="form-control" value="{{$user->email}}">
                </div>
    
                <div class="d-flex align-items-center gap-4 form-group">
                    <label for="phone" class="label-form-profile">Nomor HP</label>
                    <input type="text" id="phone" name="phone" class="form-control" value="{{$user->phone}}">
                </div>
                <button onclick="handle_upload('#submit', '#form_input', '{{route('web.profile.update', $user->id)}}', 'PATCH', null, null, 'Simpan')" id="submit" class="btn btn-primary mt-3">Simpan</button>
            </div>
    
        </form>
    </div>
    @section('custom_js')
    <script>
        $("#image").change(function(){
            let filesAmount = this.files.length
            let reader = new FileReader();
            reader.onload = (e) => { 
                $('img#preview_avatar').attr('src', e.target.result); 
            }

            reader.readAsDataURL(this.files[0]); 
        })
    </script>
    @endsection
</x-web-layout>

<!-- sampai update data user -->