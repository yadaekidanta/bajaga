<x-web-layout title="Masuk / Daftar" keyword="Bajaga Store">
    <div class="login-form mt-1">
        <div class="bg-dark-mode-2 section p-0">
            <img src="{{asset('img/favicon.png')}}" alt="image" class="form-image" style="width:20%;">
        </div>
        <div class="section bg-dark-mode-2 mt-1 p-0">
            <h1 class="m-0">{{config('app.name')}}</h1>
            <h4 class="m-0" id="title_auth"></h4>
        </div>
        <div class="mt-1 mb-5 px-4">
            <div id="main_auth">
                <a href="javascript:;" onclick="content_auth('mail_login','Masuk dengan email');" class="btn btn-info btn-block me-1 mt-5 mb-1">Masuk dengan email</a><br>
                <a href="javascript:;" onclick="content_auth('phone_login','Masuk dengan no HP');" class="btn btn-dark btn-block me-1 mb-1">Masuk dengan no HP</a><br>
                <a href="javascript:;" onclick="content_auth('register_page','Daftar sekarang');" class="btn btn-secondary btn-block me-1 mb-1">Daftar sekarang</a>
            </div>
            <div id="mail_login">
                <form id="mail_login_form">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="email" name="email" class="form-control" id="email1" placeholder="Email">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="password" name="password" class="form-control" id="password1" placeholder="Password" autocomplete="off">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-links mt-2">
                        <div>
                            <a href="javascript:;" onclick="content_auth('register_page','Daftar sekarang');" class="text-muted">Daftar sekarang</a>
                        </div>
                        <div><a href="javascript:;" onclick="content_auth('forgot_page','Atur ulang kata sandi');" class="text-muted">Lupa kata sandi?</a></div>
                    </div>
                    <div class="form-button-group">
                        <button type="button" class="btn btn-primary btn-block btn-lg" id="tombol_login_mail" onclick="handle_post('#tombol_login_mail','#mail_login_form','{{route('web.auth.login')}}', 'Masuk');">Masuk</button>
                    </div>
                </form>
            </div>
            <div id="phone_login">
                <form id="phone_form">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="tel" class="form-control" id="login_phone" name="login_phone" maxlength="13" placeholder="08123456789">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-links mt-2">
                        <div><a href="javascript:;" onclick="content_auth('main_auth','Selamat datang');" class="text-muted">Kembali</a></div>
                    </div>
                    <div class="form-button-group">
                        <button type="button" class="btn btn-primary btn-block btn-lg">Lanjut</button>
                    </div>
                </form>
            </div>
            <div id="register_page" style="padding-bottom: 3em !important">
                <form id="register_form">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="tel" class="form-control" id="phone" name="phone" placeholder="08123456789">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="email" class="form-control" id="email" name="email" placeholder="email@bajaga.com">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Kata sandi" autocomplete="off">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-links mt-2">
                        <div><a href="javascript:;" onclick="content_auth('main_auth','Selamat datang');" class="text-muted">Sudah punya akun?</a></div>
                    </div>
                    <div class="form-button-group">
                        <button type="button" class="btn btn-primary btn-block btn-lg" id="tombol_register" onclick="handle_post('#tombol_register','#register_form','{{route('web.auth.register')}}','Daftar');">Daftar</button><br><br>
                    </div>
                </form>
            </div>
            <div id="forgot_page">
                <form id="forgot_form">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="email" class="form-control" id="email1" placeholder="Email">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-links mt-2">
                        <div><a href="javascript:;" onclick="content_auth('main_auth','Selamat datang');" class="text-muted">Sudah ingat kata sandi?</a></div>
                    </div>
                    <div class="form-button-group">
                        <button type="button" class="btn btn-primary btn-block btn-lg">Lanjut</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('custom_js')
    <script>
        content_auth('main_auth','Selamat datang');
    </script>
    @endsection
</x-web-layout>