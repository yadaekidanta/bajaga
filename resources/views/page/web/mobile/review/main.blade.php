<x-web-layout title="Ulasan" keyword="Bajaga Store">
    <div class="bg-white pt-3 container-custom">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item px-2" role="presentation">
                <button class="nav-link active font-bold" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Menunggu Diulas</button>
            </li>
            <li class="nav-item px-2" role="presentation">
                <button class="nav-link font-bold" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Riwayat</button>
            </li>
        </ul>
        <div class="tab-content w-100 h-screen" id="myTabContent">
            <div class="tab-pane fade show active px-3 mt-3 w-100 h-100" id="home" role="tabpanel" aria-labelledby="home-tab">

                <div class="d-flex flex-column justify-content-center align-items-center w-100 h-100">
                    <h3 class="text-center m-0">Belum ada barang untuk kamu ulas</h3>
                    <span class="text-muted text-center">Saatnya belanja dan kasih ulasan, yuk!</span>
                </div>

            </div>
            <div class="tab-pane fade px-3 mt-3 w-100 h-100" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                
                <div class="d-flex flex-column justify-content-center align-items-center w-100 h-100">
                    <h3 class="text-center m-0">Belum ada riwayat ulasan</h3>
                    <span class="text-muted text-center">Saatnya kamu belanja dan kasih ulasanmu ke jutaan orang!</span>
                </div>

            </div>
        </div>
    </div>
    @section('custom_js')
    <script>
    </script>
    @endsection
</x-web-layout>