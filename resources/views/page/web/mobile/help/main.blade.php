<x-web-layout title="Bajaga Care" keyword="Bajaga Online Store">
    <section id="page-title" style="background-image: url('http://127.0.0.1:8000/img/bg/faq_header.svg');margin-top: -29px;">
        <div class="container clearfix">
            <h1 id="title_greet"></h1>
            <h1>Ada yang bisa kami bantu?</h1>
            {{-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">FAQs</li>
            </ol> --}}
        </div>
    </section>
    <section id="content" style="padding-bottom: 20em">
        <div class="">
            <div class="container clearfix">

                <div class="row gutter-40">
                    <!-- Post Content ============================================= -->
                    <div class="d-flex gap-3 mt-2">
                        <ul class="gap-2 container-sort-filter overflow-scroll grid-filter customjs justify-content-start p-0 mb-3" style="flex-wrap: nowrap">
                            <li class="activeFilter">
                                <a data-filter="all" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                                Semua
                                </a>
                            </li>
                            @foreach ($categories as $item)
                            <li>
                                <a data-filter=".faq-{{\Str::slug($item->name)}}" class="py-1 pill-nav d-flex align-item-center justify-content-center border rounded-pill">
                                    {{$item->name}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="0 d-none">
                        <div class="grid-filter-wrap">
                            <ul class="grid-filter style-4 customjs">
                                <li class="activeFilter">
                                    <a href="javascript:;" data-filter="all">Semua</a>
                                </li>
                                @foreach ($categories as $item)
                                    <li>
                                        <a href="javascript:;" data-filter=".faq-{{$item->name}}">{{$item->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="postcontent mt-2 col-lg-6">
                        <div id="faqs" class="faqs">
                            @foreach ($faqs as $item)
                                <div class="toggle faq faq-{{\Str::slug($item->category->name)}}">
                                    <div class="toggle-header">
                                        <div class="toggle-icon">
                                            <i class="toggle-closed icon-question-sign"></i>
                                            <i class="toggle-open icon-question-sign"></i>
                                        </div>
                                        <div class="toggle-title">
                                            {{$item->question}}
                                        </div>
                                    </div>
                                    <div class="toggle-content">{{$item->answer}}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- .postcontent end -->
                    <!-- Sidebar ============================================= -->
                    <div class="col-lg-3"></div>
                </div>

            </div>
        </div>
    </section>
    @section('custom_js')
    <script>
        jQuery(document).ready(function($){
            var $faqItems = $('#faqs .faq');
			if( window.location.hash != '' ) {
				var getFaqFilterHash = window.location.hash;
				var hashFaqFilter = getFaqFilterHash.split('#');
				if( $faqItems.hasClass( hashFaqFilter[1] ) ) {
					$('.grid-filter li').removeClass('activeFilter');
					$( '[data-filter=".'+ hashFaqFilter[1] +'"]' ).parent('li').addClass('activeFilter');
					var hashFaqSelector = '.' + hashFaqFilter[1];
					$faqItems.css('display', 'none');
					if( hashFaqSelector != 'all' ) {
						$( hashFaqSelector ).fadeIn(500);
					} else {
						$faqItems.fadeIn(500);
					}
				}
			}

			$('.grid-filter a').on( 'click', function(){
				$('.grid-filter li').removeClass('activeFilter');
				$(this).parent('li').addClass('activeFilter');
				var faqSelector = $(this).attr('data-filter');
				$faqItems.css('display', 'none');
				if( faqSelector != 'all' ) {
					$( faqSelector ).fadeIn(500);
				} else {
					$faqItems.fadeIn(500);
				}
				return false;
		   });
		});
	</script>
    @endsection
</x-web-layout>