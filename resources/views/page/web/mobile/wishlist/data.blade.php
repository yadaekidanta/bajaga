@foreach($collections as $collection)
            <div class="card w-100">
                <div class="card-body">
                    <a href="{{route('web.product.show', $collection->products->slug)}}" class="text-dark d-flex gap-3">
                        <div class="w-20 h-20 border overflow-hidden d-flex justify-content-center align-items-center">
                            <img src="{{$collection->products->photo != null ? 'storage/'.$collection->products->photo : 'img/product/a31.jpg' }}" class="w-100 h-100 object-fit-cover" alt="image product">
                        </div>
    
                        <div class="">
                            <span>{{$collection->products->name}}</span>
                            <div class="font-bold">Rp {{number_format($collection->products->price)}}</div>
                            <div class="mt-1 d-flex gap-1 align-items-center">
                                <img src="{{asset('img/other/OS-Badge-80.png')}}" alt="badge store" style="width: 21px;">
                                <span>{{$collection->products->product_store->name}}</span>
                            </div>
                        </div>
                    </a>

                    <div class="mt-2">
                        <form id="form_cart" class="d-flex align-items-center gap-2 m-0">
                            <button type="button" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('web.wishlist.destroy', $collection->id_product)}}', true);" class="btn btn-outline-secondary btn-sm p-0 p-1">
                                <i class="icon-trashcan" style="font-size: 2em;"></i>
                            </button>
                            <input type="hidden" id="product_id" name="product" value="{{$collection->id_product}}">
                            <input type="hidden" id="store_id" name="store" value="{{$collection->products->store_id}}">
                            <input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="qty d-none" />
                            <button id="tombol_cart" onclick="add_cart('#tombol_cart','#form_cart','{{route('web.cart.add')}}','POST');" class="btn btn-outline-primary w-100 btn-sm">
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" fill="#1E74FD" viewBox="0 0 24 24"><path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/></svg>
                                <span class="ms-1">Keranjang</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach