<x-web-layout title="Wishlist" keyword="Bajaga Store">
    <div class="container-custom bg-white px-3 pb-5">
        <h3 class="m-0 font-bold pt-2" id="count-wishlist"></h3>
        
        <div id="info-wishlist" class="container-custom px-3 d-flex {{count($collections) == 0 ? '' : 'd-none'}} flex-column gap-3 justify-content-center align-items-center">
            <span class="text-center">Belum ada barang di wishlist mu, saatnya mencari barang!</span>
            <div class="px-2 w-full">
                <a href="{{route('web.catalog.products')}}" class="btn btn-primary w-full">Cari Barang</a>
            </div>
        </div>

        <div class="d-flex flex-column gap-3 mt-3"
         id="list_result">
            @foreach($collections as $collection)
            <div class="card w-100">
                <div class="card-body">
                    <a href="{{route('web.product.show', $collection->products->slug)}}" class="text-dark d-flex gap-3">
                        <div class="w-20 h-20 border overflow-hidden d-flex justify-content-center align-items-center">
                            <img src="{{$collection->products->photo != null ? 'storage/'.$collection->products->photo : 'img/product/a31.jpg' }}" class="w-100 h-100 object-fit-cover" alt="image product">
                        </div>
    
                        <div class="">
                            <span>{{$collection->products->name}}</span>
                            <div class="font-bold">Rp {{number_format($collection->products->price)}}</div>
                            <div class="mt-1 d-flex gap-1 align-items-center">
                                <img src="{{asset('img/other/OS-Badge-80.png')}}" alt="badge store" style="width: 21px;">
                                <span>{{$collection->products->product_store->name}}</span>
                            </div>
                        </div>
                    </a>

                    <div class="mt-2">
                        <form id="form_cart" class="d-flex align-items-center gap-2 m-0">
                            <button type="button" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('web.wishlist.destroy', $collection->id_product)}}', true);" class="btn btn-outline-secondary btn-sm p-0 p-1">
                                <i class="icon-trashcan" style="font-size: 2em;"></i>
                            </button>
                            <input type="hidden" id="product_id" name="product" value="{{$collection->id_product}}">
                            <input type="hidden" id="store_id" name="store" value="{{$collection->products->store_id}}">
                            <input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="qty d-none" />
                            <button id="tombol_cart" onclick="add_cart('#tombol_cart','#form_cart','{{route('web.cart.add')}}','POST');" class="btn btn-outline-primary w-100 btn-sm">
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" fill="#1E74FD" viewBox="0 0 24 24"><path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/></svg>
                                <span class="ms-1">Keranjang</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @section('custom_js')
    <script>
        $(document).ready(function(){
            load_list(1);
            countWl()
        })
    </script>
    @endsection
</x-web-layout>