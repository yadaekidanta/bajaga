<x-web-layout title="Syarat & Ketentuan" keyword="Bajaga Store">
    <div class="bg-white px-3 pt-3 pb-5" style="margin-top: -1.6em;">
        <h1 class="mb-3">Syarat & Ketentuan</h1>

        <p>
            Selamat datang di www.bajaga.com.
        </p>
        <p>
            Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh PT. Bajaga terkait penggunaan situs www.bajaga.com. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.
        </p>
        <p>
            Dengan mendaftar dan/atau menggunakan situs www.bajaga.com, maka pengguna dianggap telah membaca, mengerti, memahami dan menyetujui semua isi dalam Syarat & ketentuan. Syarat & ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan PT.Tokopedia. Jika pengguna tidak menyetujui salah satu, pesebagian, atau seluruh isi Syarat & ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di www.bajaga.com.
        </p>
    </div>
</x-web-layout>