{{-- left --}}
<div class="col-8">
    <div class="pt-2 pb-2" style="margin-top: -1.6em;">
        <div class="d-flex gap-3 align-items-center">
            <input id="check_all" type="checkbox" class="check_all big-checkbox">
            <label for="check_all" class="text-capitalize m-0">Pilih Semua Barang</label>
        </div>
    </div>

    <div id="content_list" class="mt-2">
        @foreach ($store as $toko)
            <div class="mb-3" id="list-product">
                @php
                $carts = \App\Models\Cart::where('store_id',$toko->store_id)->where('user_id',$user_id)->get();
                
                @endphp
                <div class="py-2">
                    <hr class="m-0 mt-2 mb-4">
                    {{-- header store --}}
                    <div class="d-flex align-items-start gap-3 header-store">
                        <input id="check_all_store" type="checkbox" class="check_all_store big-checkbox">
                        <div class="d-flex flex-column justify-content-between">
                            <div class="font-bold">
                                <img style="width: 18px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                <label style="margin: 0" for="check_all_store" class="">{{Str::limit($carts[0]->cart_store->name, 20)}}</label>
                            </div>
                            <div class="font-bold">
                                <img style="width: 18px" src="{{asset('img/other/badge-ijo.png')}}" alt="">
                                <label for="check_all_store" class="text-xs font-thin" style="margin: 0">Dilayani bajaga</label>
                            </div>
                        </div>
                    </div>
                    {{-- header store --}}
                    @foreach($carts as $st)
                    
                    {{-- product --}}
                    <div class="d-flex gap-3 mt-2 item-product">
                        <input type="hidden" value="{{$st->product->stock}}" id="stock">
                        <input type="hidden" value="{{$st->product->price}}" id="price_product_to">
                        <input type="hidden" value="{{$st->id}}" id="id_cart">
                        <input type="hidden" value="{{$st->product->id}}" id="id_product">
                        <input id="check_all_store_item" type="checkbox" class="check_all_store_item big-checkbox">
                        <div class="w-100">
                            <div class="d-flex gap-3">
                                <div class="border w-5rem h-5rem overflow-hidden align-items-center justify-content-center d-flex">
                                    <img src="{{asset($st->product != null ? 'storage/'.$st->product->photo : 'img/product/all-product.png')}}" class="object-fit-cover w-100 h-100" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="font-medium">{{Str::limit($st->product->name, 20)}}</span>
                                    <span class="m-0 font-bold font-medium">Rp {{number_format($st->product->price)}}</span>
                                </div>
                            </div>

                            <div class="px-3 d-flex gap-3 mt-3 w-100 align-items-center justify-content-end">
                                <div class="d-flex gap-4 align-items-center">
                                    <svg onclick="destroy({{$st->product->id}}, this)" class="cursor-pointer hover-opacity-65" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M20 5.09h-4.95v-.1A3.025 3.025 0 1 0 9 5v.1H4a1 1 0 0 0 0 2h1v13A1.84 1.84 0 0 0 6.92 22h10.16A1.9 1.9 0 0 0 19 20.09V7.1h1a1 1 0 1 0 0-2v-.01zM11 5a1 1 0 0 1 2 0v.1h-2V5zM7 20V7.1h10V20H7zm3-2a1 1 0 0 0 1-1v-6.9a1 1 0 0 0-2 0V17a1 1 0 0 0 1 1zm4.767-.293A1 1 0 0 1 13.06 17v-6.9a1 1 0 1 1 2 0V17a1 1 0 0 1-.293.707z" fill="#8D96AA"/></svg>
                                    <div class="d-flex justify-content-between px-1 align-items-center gap-3 border rounded">
                                        <i class="icon-line-minus decrement cursor-pointer hover-opacity-65"></i>
                                        <span id="qty_count">{{$st->qty}}</span>
                                        <i class="icon-line-plus increment cursor-pointer hover-opacity-65"></i>
                                    </div>
                                    <!-- counter item -->
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- product --}}

                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
    <!-- list -->
</div>
{{-- left --}}

{{-- right --}}
{{-- <div class="position-fixed right-0 h-screen pe-5" style="width: 30%"> --}}
<div class="card col p-3 me-5 h-100">
    <div class="d-flex mt-2 justify-content-between align-items-center">
        <h5 class="m-0">Total Harga</h5>
        <span class="text-secondary text-sm" id="total_price">-</span>
    </div>
    <button type="button" style="margin-top: 1em !important" class=" w-100 d-block btn btn-primary m-0" id="btn-beli" onclick="beli('#btn-beli', '{{route('web.transaction')}}', 'POST', 'Beli')">Bayar</button>
</div>
{{-- </div> --}}
{{-- right --}}

<script>
    $(".check_all").change(function() {
        if ($(".check_all")[0].checked == true) {
            $(".check_all_store").prop('checked', true)
            $(".check_all_store_item").prop('checked', true)
        } else {
            $(".check_all_store").prop('checked', false)
            $(".check_all_store_item").prop('checked', false)
        }
        reload_price()
        if (getListItemChecked().length == 0) {
            $("#btn-beli").prop("disabled", true)
        }else{
            $("#btn-beli").prop("disabled", false)
        }
    })

    let item_collections = [].slice.call(document.getElementsByClassName('item-product'))
    let is_checked_count_all_item = 0
    function reload_price()
    {
        total_harga = 0
        is_checked_count_all_item = 0
        item_collections.forEach(element => {
            
            if ($(element).children(".check_all_store_item")[0].checked) {
                let price = $(element).children("#price_product_to")[0].value;
                let qty = $(element).find("#qty_count")[0].textContent
                total_harga += (parseInt(qty) * parseInt(price))
                is_checked_count_all_item += 1
                $("#button-cupon").attr("data-bs-toggle", "offcanvas")
                $("#button-cupon").removeClass("opacity-65")
            }
        });
        if (item_collections.length == is_checked_count_all_item) {
            $(".check_all").prop('checked', true)
        }else if(is_checked_count_all_item == 0){
            $("#button-cupon").removeAttr("data-bs-toggle", "offcanvas")
            $("#button-cupon").addClass("opacity-65")
            name = "Makin hemat pakai promo"
            $(".text-cupon").text(`${name}`)
        }
        else{
            $(".check_all").prop('checked', false)
        }
        total_harga_tmp = total_harga
        // console.log(total_harga);
        // console.log(parseInt($("#qty_count").text())+'asd', $("#price_product_to").val());
        $("#total_price").html(`Rp ${formatRupiah(total_harga.toString())}`)
    }

    reload_price()

    // console.log();
    $(".check_all_store_item").change(function(){
        let list_item = [].slice.call($(this).parent().parent().children(".item-product"))
        let is_checked_count = 0
        list_item.forEach(element => {
            if ($(element).children(".check_all_store_item")[0].checked) {
                is_checked_count++
            }
        });
        if (is_checked_count == list_item.length) {            
            $(this).parent().parent().children(".header-store").children(".check_all_store")[0].checked = true;
        }else{
            $(this).parent().parent().children(".header-store").children(".check_all_store")[0].checked = false;
        }
        reload_price()
        if (getListItemChecked().length == 0) {
            $("#btn-beli").prop("disabled", true)
        }else{
            $("#btn-beli").prop("disabled", false)
        }
    })

    $(".check_all_store").change(function(){
        
        if ($(this)[0].checked == true) {
            var arr = [].slice.call($(this).parent().parent().children(".item-product"));
            arr.forEach(element => {
                // console.log(element);
                $(element).children(".check_all_store_item").prop('checked', true)
            });
        } else {
            $(".check_all").prop('checked', false)
            var arr = [].slice.call($(this).parent().parent().children(".item-product"));
            arr.forEach(element => {
                // console.log(element);
                $(element).children(".check_all_store_item").prop('checked', false)
            });
        }
        reload_price()
        if (getListItemChecked().length == 0) {
            $("#btn-beli").prop("disabled", true)
        }else{
            $("#btn-beli").prop("disabled", false)
        }
    })
    $(".increment").click(function() {
         
        let qty = parseInt($(this).prev("#qty_count")[0].textContent)
        let content = $(this).prev("#qty_count")[0]
        console.log(qty + 1);
        let container_item = $(this).parent().parent().parent().parent().parent()
        let stock = container_item.find("#stock")[0].value
        // console.log(stock);
        // console.log(qty < stock, qty , stock);
        let id = container_item.find("#id_cart")[0].value
        $.ajax({
            type: "PATCH",
            url: `/cart/increase/${id}`,
            dataType: 'json',
            success: function(response) { 
                if (response.alert == "success") {
                    
                    if (qty < stock) {
                        content.textContent++
                        reload_price()
                    }
                    // console.log(response);
                } else {
                    error_toastr(response.message);
                } 
            }
        });
    })

    function removeElement(root, element)
    {
        let sum_of_product = root.children(".item-product").length
        console.log(sum_of_product);
        if (sum_of_product == 1) {
            root.parent().remove()
        }else{
            element.remove()
        }
    }

    function ajaxDecrease(id, callback){
        $.ajax({
            type: "PATCH",
            url: `/cart/decrease/${id}`,
            dataType: 'json',
            success: function(response) { 
                if (response.alert == "success") {
                    callback(true)
                } else {
                    error_toastr(response.message);
                    callback(false)
                } 
            }
        });
    }

    $(".decrement").click(function() {
        let qty = parseInt($(this).next("#qty_count")[0].textContent)
        let content = $(this).next("#qty_count")[0]
        // console.log(qty + 1);
        let item_container = $(this).parent().parent().parent().parent().parent()
        let stock = parseInt(item_container.find("#stock")[0].value)
        let id = item_container.find("#id_cart")[0].value
        let store_container = $(this).parent().parent().parent().parent().parent().parent()
        if (qty == 1) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        // do it ajax
                        let decreasing = ajaxDecrease(id, function(cond){
                            if (cond) {
                                removeElement(store_container, item_container)
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )
                                if (item_collections.length == 1) {
                                    total_harga = 0
                                    $("#total_price").html(`Rp ${formatRupiah(total_harga.toString())}`)
                                    $(".check_all").prop('checked', false)
                                } 

                            }
                        })
                    }
            })
        }else{
            ajaxDecrease(id, function(cond){
                if (cond) {
                    content.textContent--
                    reload_price()
                }
            })
        }
    })

    function destroy(id, el) {
        let state = false
        let container_store = $(el).parent().parent().parent().parent().parent()
        let container = $(el).parent().parent().parent().parent()
        console.log(container_store, container);
        // doing ajax
        if (!state) {
            state = true
            let url = '{{route('web.cart.destroy')}}' + `?id=${id}`
            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: 'json',
                error: function(response) {
                    Swal.fire({ 
                        text: response.responseJSON.message,
                        icon: "error", 
                        showDenyButton: true,
                        showCancelButton: false,
                        confirmButtonText: "Tambahkan",
                        denyButtonText: "Tidak"})
                    state = false
                },
                success: function(response){
                    // console.log(response);
                    removeElement(container_store, container)
                    state = false
                    Swal.fire({ 
                        text: response.message,
                        icon: "success", 
                        confirmButtonText: "Oke mengerti"})
                }
            });
        }
    }

    function getListItemChecked(){
        let arrayItems = [];
        item_collections.forEach(element => {
            if ($(element).children(".check_all_store_item")[0].checked) {
                let id = $(element).children("#id_product")[0].value
                arrayItems.push(id)
            }
        });

        // console.log(arrayItems.length);

        return arrayItems
    }

    if (getListItemChecked().length == 0) {
        $("#btn-beli").prop("disabled", true)
    }else{
        $("#btn-beli").prop("disabled", false)
    }

    function beli(tombol, url, method, text_button = null){ 
        // alert()
        let arrayItems = getListItemChecked()
        if (arrayItems.langth == 0) {
            return false
        }

        if (text_button) {
            $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)
        }

        $(tombol).submit(function() {
            return false;
        });
        
        $(tombol).prop("disabled", true);

        $.ajax({
            type: method,
            url: url,
            data: {data: arrayItems},
            dataType: 'json',
            error: function(response) {
                Swal.fire({ 
                    text: response.responseJSON.message,
                    icon: "error", 
                    showDenyButton: true,
                    showCancelButton: false,
                    confirmButtonText: "Tambahkan",
                    denyButtonText: "Tidak"})
                    .then(result => {
                        if (result.isConfirmed) {
                            location.href = "{{route('web.profile')}}"
                        }
                    })
                    $(tombol).submit(function() {
                        return true;
                    });

                    // console.log(arrayItems);
                    
                    $(tombol).prop("disabled", false);
                    if (text_button) {
                        $(tombol).html(text_button)
                    }
            },
            success: function(response){
                // console.log(response);
                if (response.alert == "success") {
                    location.href = response.redirect 
                }

                if (text_button) {
                    $(tombol).html(text_button)
                }
            }
        });
    }
</script>