<x-web-layout title="CheckOut" keyword="Bajaga Online Store">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div id="content_list">
                    <div id="list_result"></div>
                </div>
            </div>
        </div>
    </section>
    @section('custom_js')
    <script>
        payment_content('order_content');
        load_list(1);
    </script>
    @endsection
</x-web-layout>
