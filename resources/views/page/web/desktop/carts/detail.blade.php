<x-web-layout title="CheckOut" keyword="Bajaga Online Store">
    <section id="content" class="px-5em">
        <div class="content-wrap pb-10em p-0 pt-5">
            <div class="container clearfix">
                <h4>Keranjang</h4>
                <div style="margin-top: 2em !important" id="list_result" class="d-flex gap-4">
                </div>
            </div>
        </div>
    </section>
    @section('custom_js')
    <script>
        load_list(1);

        let total_harga = 0;
        let total_harga_tmp = 0;

        var pages = 1;
        // $(document).ready(function(){
        //     let options = {
        //         root: null,
        //         rootMargin: '300px',
        //         threshold: 0.5
        //     }
        //     let optionsSide = {
        //         root: null,
        //         rootMargin: '300px',
        //         threshold: 0.5
        //     }

        //     const observer = new IntersectionObserver(handleIntersect, options)
        //     observer.observe(document.querySelector("footer"))
        // })

        // function handleIntersect(entries){
        //     if (entries[0].isIntersecting) {
        //         ++pages
        //         // load_more(pages);
        //     }
        // }
    </script>
    @endsection
</x-web-layout>
