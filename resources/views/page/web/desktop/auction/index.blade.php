<x-web-layout title="Selamat datang" keyword="Bajaga Online Store">
  <section id="content">
    <div class="container clearfix">
      <div class="row gutter-40 col-mb-80">
        <!-- Post Content ============================================= -->
        <div class="postcontent col-lg-9 order-lg-last">
          <!-- Shop ============================================= -->
          <div
            id="shop"
            class="shop row grid-container gutter-20"
            data-layout="fitRows"
          >
            @foreach ($products as $item)
            <div class="product col-md-4 col-sm-6 sf-dress">
              <div class="grid-inner">
                <div class="product-image">
                  <a href="{{ route('web.auction.show', $item->id) }}">
                    <img src="{{$item->product_store->image}}" alt="{{$item->product_store->name}}"/>
                  </a>
                  <a href="{{ route('web.auction.show', $item->id) }}">
                    <img src="{{$item->product_store->image}}" alt="{{$item->product_store->name}}"/>
                  </a>
                </div>
                <div class="product-desc">
                  <div class="product-title">
                    <h3>
                      <a href="{{ route('web.auction.show', $item->id) }}">{{$item->product_store->name}}</a>
                    </h3>
                  </div>
                  <div class="product-price">Rp. {{$item->price}}</div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          <!-- #shop end -->
        </div>
        <!-- .postcontent end -->

        <!-- Sidebar ============================================= -->
        <div class="sidebar col-lg-3">
          <div class="sidebar-widgets-wrap">
            <div class="widget widget-filter-links">
              <h4>Select Category</h4>
              <ul
                class="custom-filter ps-2"
                data-container="#shop"
                data-active-class="active-filter"
              >
                <li class="widget-filter-reset active-filter">
                  <a href="#" data-filter="*">Clear</a>
                </li>
                @foreach ($list_category as $item)
                <li>
                  <a href="#" data-filter=".{{$item->slug}}"
                    >{{$item->title}}</a
                  >
                </li>
                @endforeach
              </ul>
            </div>

            <div class="widget widget-filter-links">
              <h4>Sort By</h4>
              <ul class="shop-sorting ps-2">
                <li class="widget-filter-reset active-filter">
                  <a href="#" data-sort-by="original-order">Clear</a>
                </li>
                <li><a href="#" data-sort-by="name">Name</a></li>
                <li>
                  <a href="#" data-sort-by="price_lh">Price: Low to High</a>
                </li>
                <li>
                  <a href="#" data-sort-by="price_hl">Price: High to Low</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- .sidebar end -->
      </div>
    </div>
  </section>
</x-web-layout>
