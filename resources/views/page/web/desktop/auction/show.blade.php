<x-web-layout title="Lelang" keyword="Bajaga Online Store">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  <section id="content">
    <div class="container clearfix">
      <div class="content-wrap">
        <div id="content_main">
          <div class="row shop-categories clearfix">
            <div class="col-lg-8 mb-0">
              <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                </div>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="{{ $productLelang->product_store->image }}" class="d-block w-100" alt="{{ $productLelang->product_store->name }}">
                  </div>
                  <div class="carousel-item">
                    <iframe src="{{ $productLelang->product_store->video_url }}" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div class="col-lg-4 bg-light overflow-auto mb-0" style="max-height: 736px;">
              <div class="p-3">
                <h4>{{ $productLelang->product_store->name }}</h4>
                <p class="mb-0">
                  {{ $productLelang->product_store->desc }}
                </p>
                <h5>
                  Harga : {{ $productLelang->price }}
                </h5>
                <p style="font-size: 14px;">
                  Tanggal Mulai - Berakhir : <br/><span class="fst-italic">{{ $productLelang->start }} - {{ $productLelang->end }}</span>
                </p>
                <div id="list_result"></div>
              </div>
            </div>
          </div>
        </div>
        @auth
          <form id="form_input">
            <div class="row">
              <div class="offset-lg-8 col-lg-4 bg-light">
                <div class="row p-3">
                  <div class="col-2 text-center">
                    <img src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png" class="img-fluid rounded-circle mx-auto" style="max-width: 32px;">
                  </div>
                  <div class="col-10 ps-0">
                    <div class="input-group">
                      <input type="text" name="bid" id="bid" class="form-control rounded-pill" placeholder="Tulis Bid..." required style="z-index: 1;" value="{{ old('bid') }}">
                      <input type="hidden" name="product_id" id="product_id" value="{{ $productLelang->id }}">
                      <input type="hidden" name="price" id="price" value="{{ $productLelang->price }}">
                      <button id="tombol_kirim" onclick="handle_save('#tombol_kirim','#form_input','{{ route('web.auction.store') }}','POST');" class="btn btn-default position-absolute end-0" style="z-index: 2;">
                        <i class="fas fa-paper-plane"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        @endauth
      </div>
    </div>
  </section>
  @section('custom_js')
  <script>
    load_list(1);
    setInterval(function(){
      load_list(1);
    }, 5000);
  </script>
  @endsection
</x-web-layout>
