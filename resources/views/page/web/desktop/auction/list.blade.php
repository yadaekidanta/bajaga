<div class="row" style="overflow-y: scroll; height:400px;">
@foreach ($collection as $data)
    <div class="col-2">
    <img src="{{ $data->users->avatar == null ? 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png' : $data->users->avatar }}" class="img-fluid rounded-circle mx-auto" style="max-width: 32px;">
    </div>
    <div class="col-10 ps-0">
    <p class="mb-0">
        {{ $data->users->name }}<br/>
        {{ number_format($data->bid) }}
    </p>
    </div>
@endforeach
</div>