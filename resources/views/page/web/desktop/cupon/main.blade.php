<x-web-layout title="Wishlist" keyword="Bajaga Online Store">
    <section id="content">
            <div class="container clearfix">

                <div class="row gutter-40 col-mb-80">
                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent col-lg-12 mt-5 order-lg-last" style="margin-bottom: 19em">

                        <h2 class="mb-0 mt-5">Daftar Diskon</h2>
                        <p class="text-mutted">Temukan diskon menarik hanya di sini!</p>
                        
                        <!-- Shop
                        ============================================= -->
                        <div class="row grid-container gutter-20" data-layout="fitRows">
                            <div class="row gap-3 mt-5 ms-2 grid-6 mb-3" id="list_result">
                                @foreach($collections as $collection)
                                    <a href="{{route('web.discount.detail', $collection->id)}}" class="card p-0 text-dark shadow-cupon w-30%">
                                        <div class="card-body p-0 position-relative bg-warning h-10">
                                            <img src="{{asset('storage/'.$collection->thumbnail)}}" class="w-100 h-100 object-fit-cover" alt="">
                                        </div>
                                        <div class="card-footer px-2 pb-2 h-8em">
                                            <h5 class="m-0">{{Str::limit($collection->name, 14)}}</h5>

                                            <div class="d-flex mt-2 justify-content-between align-items-start text-muted">
                                                <div class="d-flex align-items-center gap-2">
                                                    <!-- <div class="w-5 h-5"> -->
                                                        <img src="{{asset('img/other/timer.png')}}" class="w-1.5em h-1.5em" alt="image time">
                                                    <!-- </div> -->
                                                    <div class="d-flex flex-column">
                                                        <span class="text-xs">Berlaku hingga</span>
                                                        <span class="text-xs">{{$collection->end_at}}</span>
                                                    </div>
                                                </div>

                                                <div class="d-flex gap-2 align-items-center">
                                                    <i class="icon-money-bill-wave text-success"></i>
                                                    <div class="d-flex flex-column">
                                                        @if($collection->type == "harga")
                                                            <span class="text-xs">Diskon</span>
                                                            <span class="text-xs font-bold">Rp{{number_format($collection->harga)}}</span>
                                                        @else
                                                            <span class="text-xs">Diskon</span>
                                                            <span class="text-xs font-bold">{{$collection->persentase}}%</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <button class="btn btn-primary w-100 mt-3 text-sm font-bold">Lihat Detail</button>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                            <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                                <div class="spinner-grow text-secondary" role="status">
                                </div>
                            </div>
                        </div><!-- #shop end -->

                    </div><!-- .postcontent end -->

                    <!-- Sidebar -->
                    <div class="sidebar col-lg-3 d-none">
                        <div class="sidebar-widgets-wrap">

                            <div class="widget widget-filter-links">

                                <h4>Select Category</h4>
                                <ul class="custom-filter ps-2" data-container="#shop" data-active-class="active-filter">
                                    <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Clear</a></li>
                                    @foreach ($list_category as $item)
                                    <li><a href="#" data-filter=".{{$item->slug}}">{{$item->title}}</a></li>
                                    @endforeach
                                </ul>

                            </div>

                            <div class="widget widget-filter-links">

                                <h4>Sort By</h4>
                                <ul class="sorting ps-2">
                                    <li class="widget-filter-reset active-filter">
                                        <a href="#" data-sort-by="original-order">Clear</a></li>
                                    <li>
                                        <a href="#" data-sort-by="name">Name</a></li>
                                    <li>
                                        <a href="#" data-sort-by="price_lh">Price: Low to High</a></li>
                                    <li>
                                        <a href="#" data-sort-by="price_hl">Price: High to Low</a></li>
                                </ul>

                            </div>

                        </div>
                    </div>
                    <!-- .sidebar end -->
                </div>

            </div>
    </section>
    @section('custom_js')
        <script>
            // load_list(1)

            //function for Scroll Event
            var pages = 1;
            $(document).ready(function(){
                let options = {
                    root: null,
                    rootMargin: '300px',
                    threshold: 0.5
                }

                const observer = new IntersectionObserver(handleIntersect, options)
                observer.observe(document.querySelector("footer"))
            })

            function handleIntersect(entries){
                if (entries[0].isIntersecting) {
                    ++pages
                    load_more(pages);
                }
            }
        </script>
    @endsection
</x-web-layout>