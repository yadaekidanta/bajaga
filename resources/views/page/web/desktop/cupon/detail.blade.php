<x-web-layout title="Wishlist" keyword="Bajaga Online Store">
    <section id="content">
            <div class="container pt-2em px-8em clearfix">
                <div class="row gutter-40 col-mb-80 mb-5">
                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent order-lg-last mt-3 mb-5">
                        <div class="w-100 card">
                            <div class="h-18em w-100 overflow-hidden">
                                <img src="{{asset('storage/'.$discount->thumbnail)}}" class="card-img-top w-100 h-100 object-fit-cover" alt="image discount">
                            </div>
                            <div class="card-body p-0">
                                <h4 class="p-3 m-0">{{$discount->name}}</h4>

                                <hr class="hr-list-bg m-0">

                                <div class="d-flex p-3 gap-3 w-100 align-items-start text-muted">
                                    <div class="d-flex align-items-center gap-2">
                                        <!-- <div class="w-5 h-5"> -->
                                            <img src="{{asset('img/other/timer.png')}}" class="w-1.5em h-1.5em" alt="image time">
                                        <!-- </div> -->
                                        <div class="d-flex flex-column">
                                            <span class="text-xs">Berlaku hingga</span>
                                            <span class="text-xs">{{$discount->end_at}}</span>
                                        </div>
                                    </div>

                                    <div class="d-flex gap-2 align-items-center">
                                        <i class="icon-money-bill-wave text-success"></i>
                                        <div class="d-flex flex-column">
                                            @if($discount->type == "harga")
                                                <span class="text-xs">Diskon</span>
                                                <span class="text-xs font-bold">Rp{{number_format($discount->harga)}}</span>
                                            @else
                                                <span class="text-xs">Diskon</span>
                                                <span class="text-xs font-bold">{{$discount->persentase}}%</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <hr class="hr-list-bg m-0">

                                <div class="p-3">
                                    <h5>Deskripsi</h5>

                                    <p class="text-break">
                                        {{$discount->desc}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</x-web-layout>