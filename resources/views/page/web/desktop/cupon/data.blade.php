@foreach($collections as $collection)
    <a href="{{route('web.discount.detail', $collection->id)}}" class="card p-0 text-dark shadow-cupon w-30%">
        <div class="card-body p-0 position-relative bg-warning h-10">
            <img src="{{asset('storage/'.$collection->thumbnail)}}" class="w-100 h-100 object-fit-cover" alt="">
        </div>
        <div class="card-footer px-2 pb-2 h-8em">
            <h5 class="m-0">{{Str::limit($collection->name, 14)}}</h5>

            <div class="d-flex mt-2 justify-content-between align-items-start text-muted">
                <div class="d-flex align-items-center gap-2">
                    <!-- <div class="w-5 h-5"> -->
                        <img src="{{asset('img/other/timer.png')}}" class="w-1.5em h-1.5em" alt="image time">
                    <!-- </div> -->
                    <div class="d-flex flex-column">
                        <span class="text-xs">Berlaku hingga</span>
                        <span class="text-xs">{{$collection->end_at}}</span>
                    </div>
                </div>

                <div class="d-flex gap-2 align-items-center">
                    <i class="icon-money-bill-wave text-success"></i>
                    <div class="d-flex flex-column">
                        @if($collection->type == "harga")
                            <span class="text-xs">Diskon</span>
                            <span class="text-xs font-bold">Rp{{number_format($collection->harga)}}</span>
                        @else
                            <span class="text-xs">Diskon</span>
                            <span class="text-xs font-bold">{{$collection->persentase}}%</span>
                        @endif
                    </div>
                </div>
            </div>

            <button class="btn btn-primary w-100 mt-3 text-sm font-bold">Lihat Detail</button>
        </div>
    </a>
@endforeach