<x-web-layout title="Toko Favorit" keyword="Bajaga Store">
    <div class="container-custom pt-3em px-5em pb-10em bg-white pt-2">
        {{-- <form id="content_filter">
            <div class="input-group mb-3">
                <input type="text" class="form-control" name="keywords" placeholder="Cari toko favorit" aria-label="Cari toko favorit" aria-describedby="button-addon2">
                <button onclick="load_list(1)" class="btn shadow-none btn-outline-secondary" type="button" id="button-addon2">Button</button>
            </div>
        </form> --}}
        <div class="d-flex gap-3 align-items-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="#444444"><g fill="none"><path d="M0 0h24v24H0z"/><path d="M17 11.12a3.978 3.978 0 0 1-2.5.88 3.978 3.978 0 0 1-2.5-.88 3.978 3.978 0 0 1-2.5.88 3.978 3.978 0 0 1-2.5-.88 3.978 3.978 0 0 1-2.5.88c-.171 0-.334-.029-.5-.05V19a2 2 0 0 0 2 2h3v-4a2 2 0 0 1 2-2h2a2 2 0 0 1 2 2v4h3a2 2 0 0 0 2-2v-7.05c-.166.021-.329.05-.5.05a3.978 3.978 0 0 1-2.5-.88m4.55-3.87l-.45-.75L19 3H5L2.9 6.5l-.45.75L2 8a2.5 2.5 0 0 0 2.5 2.5c.559 0 1.07-.189 1.487-.5A2.49 2.49 0 0 0 7 8a2.49 2.49 0 0 0 1.014 2c.416.311.928.5 1.486.5.559 0 1.07-.189 1.487-.5A2.49 2.49 0 0 0 12 8a2.49 2.49 0 0 0 1.014 2c.416.311.928.5 1.486.5.559 0 1.07-.189 1.487-.5A2.49 2.49 0 0 0 17 8a2.49 2.49 0 0 0 1.014 2 2.473 2.473 0 0 0 1.986.449A2.5 2.5 0 0 0 22 8l-.45-.75z" fill="#444444"/><path fill="#444444" stroke="white" d="M17 11.5S22 10 22 6c0-2-1.5-3-2.5-3C17 3 17 5 17 5s0-2-2.5-2c-1 0-2.5 1-2.5 3 0 4 5 5.5 5 5.5"/><path fill="#444444" d="M17 11.5S22 10 22 6c0-2-1.5-3-2.5-3C17 3 17 5 17 5s0-2-2.5-2c-1 0-2.5 1-2.5 3 0 4 5 5.5 5 5.5"/></g></svg>
            <h3 class="m-0">Toko Favorit</h3>
        </div>
        <div class="d-flex flex-wrap gap-3 mt-4"
         id="list_result">
            @foreach ($store as $key => $item)
                <div class="card w-24%">
                    <div class="card-header justify-content-between bg-white d-flex align-items-center gap-3">
                        <div class=" d-flex align-items-center gap-3">
                            {{-- image store --}}
                            <div class="w-3em h-3em rounded-circle overflow-hidden">
                                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->photo ? $item->photo : 'store/default.png'))}}" alt="image store">
                            </div>
                            {{-- image store --}}
                            <div>
                                <div class="d-flex align-items-center">
                                    <img style="width: 15px; height: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                    <span class="d-block text-sm">{{$item->name}}</span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <i class="icon-location"></i>
                                    <span class="d-block text-sm">{{Str::limit($item->city->name, 10)}}</span>
                                </div>
                            </div>                            
                        </div>

                        <button type="button" id="follow_store" onclick="follow('{{route('web.store.favorite.follow').'?store_id='.$item->id}}', this)" class="btn btn-sm btn-primary text-sm">Follow</button>

                    </div>
                    <div class="card-body p-0 h-8em overflow-hidden">
                        <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->photo ? $item->photo : 'store/default.png'))}}" alt="image store">
                    </div>
                </div>
            @endforeach
            @foreach ($storeFavorite as $key => $item)
                <div class="card w-24%">
                    <div class="card-header justify-content-between bg-white d-flex align-items-center gap-3">
                        <div class=" d-flex align-items-center gap-3">
                            {{-- image store --}}
                            <div class="w-3em h-3em rounded-circle overflow-hidden">
                                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->store->photo ? $item->store->photo : 'store/default.png'))}}" alt="image store">
                            </div>
                            {{-- image store --}}
                            <div>
                                <div class="d-flex align-items-center">
                                    <img style="width: 15px; height: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                    <span class="d-block text-sm">{{$item->store->name}}</span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <i class="icon-location"></i>
                                    <span class="d-block text-sm">{{Str::limit($item->store->city->name, 10)}}</span>
                                </div>
                            </div>                            
                        </div>

                        <button type="button" id="unfollow_store" onclick="unfollow('{{route('web.store.favorite.unfollow').'?store_id='.$item->store->id}}', this)" class="btn btn-sm btn-outline-primary text-sm">Unfollow</button>

                    </div>
                    <div class="card-body p-0 h-8em overflow-hidden">
                        <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->store->photo ? $item->store->photo : 'store/default.png'))}}" alt="image store">
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    @section('custom_js')
        <script>
            // load_list(1)
            // follow and unfollow store
            function follow(url, el){
                // alert(param)
                $(el).prop("disabled", true)
                $.ajax({
                    type: 'GET',
                    url,
                    success: function(response){
                        $(el).prop("disabled", false)
                        // console.log(response);
                        $("#unfollow_store").removeClass("d-none")
                        $("#follow_store").addClass("d-none")
                        load_list(1)
                    },
                    error: (response) => {
                        $(el).prop("disabled", false)
                        // load_list(1)
                    }
                })
            }
            function unfollow(url, el){
                $(el).prop("disabled", true)
                $.ajax({
                    type: 'DELETE',
                    url,
                    success: function(response){
                        $(el).prop("disabled", false)
                        $("#follow_store").removeClass("d-none")
                        $("#unfollow_store").addClass("d-none")
                        load_list(1)
                    },
                    error: (response) => {
                        $(el).prop("disabled", false)
                        // load_list(1)
                    }
                })
            }
            // follow and unfollow store
        </script>
    @endsection
</x-web-layout>