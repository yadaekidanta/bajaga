@foreach ($store as $key => $item)
                <div class="card w-24%">
                    <div class="card-header justify-content-between bg-white d-flex align-items-center gap-3">
                        <div class=" d-flex align-items-center gap-3">
                            {{-- image store --}}
                            <div class="w-3em h-3em rounded-circle overflow-hidden">
                                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->photo ? $item->photo : 'store/default.png'))}}" alt="image store">
                            </div>
                            {{-- image store --}}
                            <div>
                                <div class="d-flex align-items-center">
                                    <img style="width: 15px; height: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                    <span class="d-block text-sm">{{$item->name}}</span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <i class="icon-location"></i>
                                    <span class="d-block text-sm">{{Str::limit($item->city->name, 10)}}</span>
                                </div>
                            </div>                            
                        </div>

                        <button type="button" id="follow_store" onclick="follow('{{route('web.store.favorite.follow').'?store_id='.$item->id}}', this)" class="btn btn-sm btn-primary text-sm">Follow</button>

                    </div>
                    <div class="card-body p-0 h-8em overflow-hidden">
                        <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->photo ? $item->photo : 'store/default.png'))}}" alt="image store">
                    </div>
                </div>
            @endforeach
            @foreach ($storeFavorite as $key => $item)
                <div class="card w-24%">
                    <div class="card-header justify-content-between bg-white d-flex align-items-center gap-3">
                        <div class=" d-flex align-items-center gap-3">
                            {{-- image store --}}
                            <div class="w-3em h-3em rounded-circle overflow-hidden">
                                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->store->photo ? $item->store->photo : 'store/default.png'))}}" alt="image store">
                            </div>
                            {{-- image store --}}
                            <div>
                                <div class="d-flex align-items-center">
                                    <img style="width: 15px; height: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                    <span class="d-block text-sm">{{$item->store->name}}</span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <i class="icon-location"></i>
                                    <span class="d-block text-sm">{{Str::limit($item->store->city->name, 10)}}</span>
                                </div>
                            </div>                            
                        </div>

                        <button type="button" id="unfollow_store" onclick="unfollow('{{route('web.store.favorite.unfollow').'?store_id='.$item->store->id}}', this)" class="btn btn-sm btn-outline-primary text-sm">Unfollow</button>

                    </div>
                    <div class="card-body p-0 h-8em overflow-hidden">
                        <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'. ($item->store->photo ? $item->store->photo : 'store/default.png'))}}" alt="image store">
                    </div>
                </div>
            @endforeach