<x-web-layout title="Saldo Saya" keyword="Bajaga Online Store">
    <div id="content_list">
            <br>
            <div class="px-5 m-0 mb-12em" id="kt_content_container">
                <div class="row gap-4 clearfix">
                    
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="d-flex position-relative gap-5">
                                <div class="d-flex gap-4">
                                    <div class="w-6em h-6em border overflow-hidden rounded-circle">
                                        <img src="{{asset($store->photo ? 'storage/'.$store->photo : 'storage/store/default.png')}}" class="w-100 h-100 object-fit-cover" alt="image store">
                                    </div>

                                    <div class="">
                                        <div class="d-flex gap-2 align-items-center">
                                            <h3 class="m-0">{{$store->name}}</h3>
                                            <img style="width: 24px; height: 24px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                        </div>

                                        <div class="d-flex align-items-center gap-3 mt-3">
                                            <div>
                                                <span class="">Buka Toko :</span>
                                                <span class="">{{$store->start_at ? $store->start_at->format('H:i') : '..:..'}} -  {{$store->end_at ? $store->end_at->format('H:i') : '..:..'}}</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-2">
                                                <i class="icon-location"></i>
                                                <span>{{Str::limit($store->city->name, 25)}}</span>
                                            </div>
                                        </div>

                                        <div class="mt-2 d-flex align-items-center gap-2 mt-3">
                                            <button type="button" id="follow_store" onclick="follow('{{route('web.store.favorite.follow').'?store_id='.$store->id}}', this)" class="btn btn-sm btn-primary {{ $state_follow ?'d-none': ''}} text-sm px-5">Follow</button>
                                            <button type="button" id="unfollow_store" onclick="unfollow('{{route('web.store.favorite.unfollow').'?store_id='.$store->id}}', this)" class="btn btn-sm btn-outline-primary {{ $state_follow ? '': 'd-none'}} text-sm px-5">Unfollow</button>
                                            <a href="{{route('web.chat')}}?id={{$store->user->id}}" class="btn btn-sm btn-primary text-sm px-5">Chat Penjual</a>
                                        </div>
                                    </div>

                                </div>

                                <div>
                                    <div>Nilai Kualitas Produk</div>
                                    <div class="d-flex gap-2 align-items-center">
                                        <h3 class="m-0">{{$rating}}</h3>
                                        <div class="d-flex gap-2 align-items-center">
                                            @for ($i = 0 ; $i < intval($rating); $i++)
                                                <i class="icon-star3 text-emas font-icon-size-2"></i>
                                            @endfor
                                            @for ($i = intval($rating); $i < 5; $i++)
                                                <i class="icon-star-empty text-secondary font-icon-size-2"></i>
                                            @endfor
                                        </div>
                                        {{-- <div class="product-rating d-flex flex-column">
                                            <input id="input-15" class="rating" value="{{$rating}}" data-size="sm" data-readonly="true">
                                        </div><!-- Product Single - Rating End --> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- content --}}
                    <div>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item px-3" role="presentation">
                              <button class="nav-link active" id="produk-tab" data-bs-toggle="tab" data-bs-target="#produk" type="button" role="tab" aria-controls="produk" aria-selected="true">Produk</button>
                            </li>
                            <li class="nav-item px-3" role="presentation">
                              <button class="nav-link" id="ulasan-tab" data-bs-toggle="tab" data-bs-target="#ulasan" type="button" role="tab" aria-controls="ulasan" aria-selected="false">Ulasan</button>
                            </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="produk" role="tabpanel" aria-labelledby="produk-tab">
                                <div class="row gap-4 mt-3 p-4">
                                    <div class="sidebar h-100 col-lg-2 p-0 py-2 card shadow-sm">
                                        <div class="sidebar-widgets-wrap">
                
                                            <div class="widget widget-filter-links">
                
                                                <h4 class="px-3 pt-2">Etalase</h4>
                                                <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
                                                    @foreach ($etalase as $item)
                                                    <li>
                                                        <a href="{{route('web.etalase', $item->id)}}?store_id={{$store->id}}" class="px-3 py-1 cursor-pointer hover-opacity-65" data-filter=".{{$item->id}}">
                                                            {{$item->name}}
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                
                                            </div>
                
                                        </div>
                                    </div><!-- .sidebar end -->
        
                                    <div class="col-lg-9 ms-4">
                                        {{-- sort --}}
                                        {{-- <div class="w-100 d-flex justify-content-end">
                                            <div class="btn-group">
                                                <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                  Urutkan   
                                                </button>
                                                <ul class="dropdown-menu">
                                                  ...
                                                </ul>
                                            </div>
                                        </div> --}}
                                        {{-- sort --}}
        
                                        {{-- daftar produk --}}
                                        <div class=" mt-5 ms-4">
                                            <div class="row gap-3 p-3" id="list_result"></div>
                                            <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                                                <div class="spinner-grow text-secondary" role="status">
                                                </div>
                                            </div>
                                        </div>
                                        {{-- daftar produk --}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="ulasan" role="tabpanel" aria-labelledby="ulasan-tab">
                                
                                <div id="list_result">  
                                    @foreach ($collection_review as $key => $product)
                                        <div class="card p-3 mt-3">
                                            <div class="d-flex gap-3 align-items-start">
                                                <div class="col-lg-2">
                                                    <div class="w-8em h-8em border overflow-hidden rounded">
                                                        <img src="{{asset('storage/'.$product->photo)}}" class="w-100 h-100 object-fit-cover" alt="image product">
                                                    </div>
    
                                                    <h5 class="m-0 mt-3">{{Str::limit($product->name, 15)}}</h5>

                                                </div>

                                                <div class="col-10 pe-3">

                                                    {{-- list ulasan --}}
                                                    @foreach ($product->product_reviews()->get() as $ulasan)
                                                        @if (!$ulasan->to_id)
                                                            <div class="d-flex gap-3">
                                                                @php
                                                                    $user = Auth::user();
                                                                @endphp
                                                                <div class="w-3em h-3em border overflow-hidden rounded-circle">
                                                                    <img src="{{asset('storage/'.$user->avatar)}}" class="w-100 h-100 object-fit-cover" alt="image store">
                                                                </div>
                                                                <div class="w-100">
                                                                    <div class="d-flex gap-2 align-items-center">
                                                                        <h5 class="mb-3">{{$user->name}}</h5>
                                                                        <span class="badge bg-primary text-sm">Pembeli</span>
                                                                    </div>
                                                                    <div class="rounded border {{!$ulasan->read_at ? 'bg-aliceblue' : ''}} w-100 p-3 position-relative">
                                                                        {{-- icon star --}}
                                                                        <div class="d-flex gap-2 align-items-center">
                                                                            @for ($i = 0 ; $i < intval($ulasan->rate); $i++)
                                                                                <i class="icon-star3 text-emas font-icon-size-1"></i>
                                                                            @endfor
                                                                            @for ($i = intval($ulasan->rate); $i < 5; $i++)
                                                                                <i class="icon-star-empty text-secondary font-icon-size-1"></i>
                                                                            @endfor
                                                                        </div>
                                                                        <span class="mt-3">
                                                                            {{$ulasan->review}}
                                                                        </span>
        
                                                                        @if (!$ulasan->read_at)
                                                                            <div class="d-flex justify-content-end mt-3">
                                                                                <span class="text-primary hover-opacity-65 font-bold" role="button" onclick="handle_open_modal('{{route('web.store.review.addModalReview', $ulasan->id)}}', '#modalListResult', '#contentListResult')">Balas</span>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        @else
                                                        <div class="w-100 d-flex gap-3 justify-content-end mt-3">
                                                            <div class="w-3em h-3em border overflow-hidden rounded-circle">
                                                                <img src="{{asset('storage/'.$store->photo)}}" class="w-100 h-100 object-fit-cover" alt="image store">
                                                            </div>
                                                            
                                                            <div class="w-80%">
                                                                <div class="d-flex gap-2 align-items-center">
                                                                    <h5 class="mb-3">{{$store->name}}</h5>
                                                                    <span class="badge bg-primary text-sm">Pembeli</span>
                                                                </div>
                                                                <div class="rounded border {{!$ulasan->read_at ? 'bg-aliceblue' : ''}} w-100 p-3 position-relative">
                                                                    <span class="mt-3">
                                                                        {{$ulasan->review}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            
                                                        @endif
                                                    @endforeach
                                                    {{-- list ulasan --}}

                                                </div>
                                            </div>
                                        </div>   
                                    @endforeach
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    {{-- content --}}
                    
                </div>
            </div>
    </div>
    @section('custom_js')
        <script>
            load_list(1)
            localStorage.setItem("page_infinate", 1)

            // follow and unfollow store
            function follow(url, el){
                // alert(param)
                $(el).prop("disabled", true)
                $.ajax({
                    type: 'GET',
                    url,
                    success: function(response){
                        $(el).prop("disabled", false)
                        // console.log(response);
                        $("#unfollow_store").removeClass("d-none")
                        $("#follow_store").addClass("d-none")
                        // load_list(1)
                    },
                    error: (response) => {
                        $(el).prop("disabled", false)
                        // load_list(1)
                    }
                })
            }
            function unfollow(url, el){
                $(el).prop("disabled", true)
                $.ajax({
                    type: 'DELETE',
                    url,
                    success: function(response){
                        $(el).prop("disabled", false)
                        $("#follow_store").removeClass("d-none")
                        $("#unfollow_store").addClass("d-none")
                    },
                    error: (response) => {
                        $(el).prop("disabled", false)
                        // load_list(1)
                    }
                })
            }
            // follow and unfollow store

            $(document).ready(function(){
                let options = {
                    root: null,
                    rootMargin: '50px',
                    threshold: 0.5
                }

                const observer = new IntersectionObserver(handleIntersect, options)
                observer.observe(document.querySelector(".ajax-load"))
            })
            
            function handleIntersect(entries){
                if (entries[0].isIntersecting) {
                    localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                    // console.log(pages);
                    load_more(parseInt(localStorage.getItem("page_infinate")));
                }
            }
        </script>
    @endsection
</x-web-layout>