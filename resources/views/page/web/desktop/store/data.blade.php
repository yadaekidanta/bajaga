@foreach ($collections as $item)
    <div style="width: 10em; height: 17em;" class="rounded overflow-hidden position-relative px-0 d-flex flex-column shadow-sm cursor-pointer card-product">
        <a href="{{route('web.product.show', $item->slug)}}" class="position-relative d-flex align-items-center justify-content-between overflow-hidden w-100" style="height: 12em">
            <img class="w-100 h-100 object-fit-cover" src="{{$item->image}}" alt="">
        </a>
        <div class="px-2 py-2 position-relative">
            <a href="{{route('web.product.show', $item->slug)}}" class="text-dark">{{Str::limit($item->name, 14)}}</a>
            <a href="{{route('web.product.show', $item->slug)}}" class="text-lg font-bold mt-2 d-block text-dark">Rp {{number_format($item->price)}}</a>
            <a href="{{route('web.store', $item->product_store->id)}}" class="d-flex mt-1 align-items-center gap-3">
                <img style="width: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                <span class="text-secondary store-text">{{Str::limit($item->product_store->name, 14)}}</span>
            </a>
            <a href="{{route('web.product.show', $item->slug)}}" class="d-flex align-items-center mt-1 text-dark gap-1">
                <i class="icon-star2 text-emas"></i>
                <span class="text-sm">4.8</span> | 
                <span class="text-sm">Terjual 10rb+</span>
            </a>
        </div>
    </div>
@endforeach