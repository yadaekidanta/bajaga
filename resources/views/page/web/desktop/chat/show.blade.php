<x-web-layout title="Bajaga Store" keyword="Bajaga Store">
    <div id="content_list" class="pb-5 bg-white">
        <div class="pt-3 pb-5 container-custom px-3">
            <div id="list_result"></div>
        </div>
    </div>
    @section('custom_js')
<script>
    load_list(1);

    hideFooter()
    
    
    setInterval(() => {
        load_list(1);
    }, 3000);

    function send_chat(tombol, form, url, method){
        // $(document).one('submit', form, function(e) {
            $(tombol).submit(function() {
                return false;
            });
            // let data = new FormData(this);
            let data = $(form).serialize();
            console.log(data);
            // data.append('_method', method);
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                resetForm: true,
                processData: false,
                dataType: 'json',
                beforeSend: function() {

                },
                success: function(response) {
                    // console.log(response);
                    if (response.alert == "success") {
                        $(form)[0].reset();
                        load_list(1);
                    } else {
                        error_toastr(response.message);
                        setTimeout(function() {
                            $(tombol).prop("disabled", false);
                        }, 2000);
                    }
                },
            });
            return false;
        // });
    }
</script>
@endsection
</x-web-layout>