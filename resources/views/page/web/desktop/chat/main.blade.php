<x-web-layout title="Ulasan" keyword="Bajaga Online Store">
    <div id="content_list">
            <br>
            <div class="px-5 m-0" id="kt_content_container">
                <div class="row clearfix gap-4">
                     {{-- sidebar --}}
                     <div  class="w-16em">
                        @include('page.web.desktop.components.sidebar')
                    </div>
                    {{-- sidebar --}}
    
                    {{-- content --}}
                    <div class="col-sm-9 position-relative h-29rem col-md-9 card p-0 overflow-hidden shadow-sm">
                        <div class="d-flex w-100 h-100">
                            {{-- side menu --}}
                            <div class="h-100 col-md-4 bg-white shadow-sm overflow-y-scroll">

                                <h3 class="m-0 p-3">Chat</h3>

                                {{-- search --}}
                                {{-- <div class="mb-3 px-3">
                                    <form class="m-0">
                                        <div class="input-group">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="icon-search1"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon1">
                                        </div>
                                    </form>
                                </div> --}}
                                {{-- search --}}
                                
                                {{-- daftar orang --}}
                                <div id="list_result">
                                </div>
                                {{-- daftar orang --}}
                                
                            </div>
                            {{-- side menu --}}

                            {{-- content chat --}}
                            <div class="h-100 col-md-8 position-relative" id="list_detail_chat">
                                {{-- spinner --}}
                                <div class="ajax-load-detail w-100 h-100 d-flex align-items-center justify-content-center d-none">
                                    <div class="spinner-border text-primary" role="status">
                                    </div>
                                </div>
                                {{-- spinner --}}
                            </div>
                            
                            {{-- content chat --}}
                        </div>
                    </div>
                    {{-- content --}}
                </div>
            </div>
    </div>
    {{-- <div id="content_input"></div> --}}
    @section('custom_js')
    <script type="text/javascript">
        load_list(1);
        
        try {
            const urlParams = new URLSearchParams(params);
            const id = urlParams.get('id')
            if (id) {
                detailChat(`chat/${id}`)
            }
        } catch (error) {
            
        }

        function toTheBottom(){
            // alert()
            var objDiv = document.getElementById("scrollView");
            console.log(objDiv.scrollHeight);
            // console.log($(objDiv).find("list_detail_chat")[0].scrollHeight);
            objDiv.scrollTop = objDiv.scrollHeight;
        }

        setInterval(() => {
            load_list(1);
        }, 5000);

        function detailChat(url) {
            // alert(url)
            $(".ajax-load-detail").toggleClass("d-none")
            load_custom("#list_detail_chat", url, 1, function(){
                $(".ajax-load-detail").toggleClass("d-none")
                setTimeout(() => {
                    toTheBottom()
                }, 50);
            })
        }

        function send_chat(tombol, form, url, method, url_list){
                // alert(url_list)
                $(tombol).submit(function() {
                    return false;
                });
                $(tombol).prop("disabled", true);
                let data = $(form).serialize();
                // console.log(data);
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    resetForm: true,
                    processData: false,
                    dataType: 'json',
                    beforeSend: function() {

                    },
                    success: function(response) {
                        // console.log(url_list);
                        if (response.alert == "success") {
                            $(form)[0].reset();
                            load_custom("#list_detail_chat", url_list, 1, function() {
                                setTimeout(() => {
                                    toTheBottom()
                                }, 50);
                            })
                            load_list(1);
                        } else {
                            error_toastr(response.message);
                            setTimeout(function() {
                                $(tombol).prop("disabled", false);
                            }, 2000);
                        }
                        $(tombol).prop("disabled", false);
                    },
                });
                return false;
            // });
        }
    </script>
    @endsection
</x-web-layout>