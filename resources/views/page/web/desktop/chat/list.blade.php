@foreach($userList as $listMessage)
{{-- {{dd($userList)}} --}}
    @if($listMessage["from_id"] == Auth::user()->id)
        <div class="cursor-pointer">
            <div onclick="detailChat('{{route('web.chat.detail', $listMessage->to_id)}}')" class="d-flex hover-opacity-65 text-body px-3 py-3 align-items-center justify-content-between {{$listMessage->counter_read_null > 0 ? 'bg-aliceblue' : ''}}">
                <div class="d-flex align-items-center gap-3">
                    <!-- img user -->
                    <div class="w-3em h-3em border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                        <img class="w-100 h-100 object-fit-cover" src="{{asset($listMessage->to->avatar ? 'storage/'. $listMessage->to->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
                    </div>
                    <!-- img user -->
    
                    <div>
                        <span class="font-bold">{{Str::limit($listMessage->to->name, 14)}}</span>
                        <span class="d-block text-sm">{{Str::limit($listMessage->body, 18)}}</span>
                    </div>
                </div>
    
                <div class="d-flex align-items-center flex-column">
                    @if(strtotime($listMessage->created_at) < (time()-(60*60*24)))
                        @if (strtotime($listMessage->created_at) < (time()-(60*60*24*2)))
                            @if (strtotime($listMessage->created_at) < (time()-(60*60*24*7)))
                                <span class="text-primary text-xs">{{$listMessage->created_at->format('y/n/j')}}</span>
                            @else
                                <span class="text-primary text-xs">{{$listMessage->created_at->format('D')}}</span>
                            @endif
                        @else
                            <span class="text-primary text-xs">Yesterday</span>
                        @endif
                    @else
                        <span class="text-primary text-xs">{{$listMessage->created_at->format('H:i')}}</span>
                    @endif
                </div>
                
            </div>
            <hr class="m-0">
        </a>
    @else
        <div class="cursor-pointer">
            <div onclick="detailChat('{{route('web.chat.detail', $listMessage->from_id)}}')" class="d-flex hover-opacity-65 px-3 py-3 text-body align-items-center justify-content-between {{$listMessage->counter_read_null > 0 ? 'bg-aliceblue' : ''}} px-3 py-3">
                <div class="d-flex align-items-center gap-3">
                    <!-- img user -->
                    <div class="w-3em h-3em border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                        <img class="w-100 h-100 object-fit-cover" src="{{asset($listMessage->from->avatar ? 'storage/'. $listMessage->from->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
                    </div>
                    <!-- img user -->
    
                    <div>
                        <span class="font-lg font-bold">{{Str::limit($listMessage->from->name, 12)}}</span>
                        <span class="d-block text-sm">{{Str::limit($listMessage->body, 25)}}</span>
                    </div>
                </div>
    
                <div class="d-flex align-items-center flex-column">
                    @if(strtotime($listMessage->created_at) < (time()-(60*60*24)))
                        @if (strtotime($listMessage->created_at) < (time()-(60*60*24*2)))
                            @if (strtotime($listMessage->created_at) < (time()-(60*60*24*7)))
                                <span class="text-primary text-xs">{{$listMessage->created_at->format('y/n/j')}}</span>
                            @else
                                <span class="text-primary text-xs">{{$listMessage->created_at->format('D')}}</span>
                            @endif
                        @else
                            <span class="text-primary text-xs">Yesterday</span>
                        @endif
                    @else
                        <span class="text-primary text-xs">{{$listMessage->created_at->format('H:i')}}</span>
                    @endif
                </div>
            </div>
            <hr class="m-0">
        </a>
    @endif
@endforeach