<div class="h-100 pb-5em scroll-custom-2 overflow-y-scroll px-3" id="scrollView">
    <input type="hidden" value="{{$user->id}}" id="to_id">
    @foreach($collection as $message)
        @if($message->from_id == $user->id)
            <!-- dia -->
            <div class="d-flex mt-3 gap-2">
                <!-- img user -->
                <div class="w-3em h-3em border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                    <img class="w-100 h-100 object-fit-cover" src="{{asset($message->from->avatar ? 'storage/'. $message->from->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
                </div>
                <!-- img user -->

                <div class="card-chat-reverse text-break">
                    {{$message->body}}
                </div>

            </div>
            <!-- /dia -->
        @else
            <!-- aku -->
            <div class="d-flex justify-content-end gap-2 mt-3">
                <div class="card-chat text-break">
                    {{$message->body}}
                </div>
                <!-- img user -->
                <div class="w-3em h-3em border rounded-full overflow-hidden d-flex align-items-center justify-content-center">
                    <img class="w-100 h-100 object-fit-cover" src="{{asset($message->from->avatar ? 'storage/'. $message->from->avatar : 'storage/avatars/avatar1.png')}}" alt="image user">
                </div>
                <!-- img user -->

            </div>
            <!-- /aku -->
        @endif
    @endforeach
    <div class="scrollingContainer"></div>
</div>
<div class="position-absolute bottom-0 left-0 w-100 p-2 bg-white shadow">
    <form id="form-chat" class="m-0">
        <div class="position-relative w-100">
            <div class="input-group">
                <input type="text" name="body" class="form-control" placeholder="Type you're message" aria-label="Type you're message" aria-describedby="button-addon2">
                <input  type="hidden" id="btn_id_input" name="to_id" value="{{$user->id}}" class="form-control">
                <button class="btn btn-outline-secondary" type="button" id="button-addon2" id="btn-send" onclick="send_chat('#btn-send', '#form-chat', '{{route('web.chat')}}', 'POST', '{{route('web.chat.detail', $user->id)}}')">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M24 0l-6 22-8.129-7.239 7.802-8.234-10.458 7.227-7.215-1.754 24-12zm-15 16.668v7.332l3.258-4.431-3.258-2.901z"/></svg>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    // function toTheBottom(){
    //     var objDiv = document.getElementById("scrollView");
    //     // console.log($(objDiv).find("list_detail_chat")[0].scrollHeight);
    //     objDiv.scrollTop = objDiv.scrollHeight;
    // }
    
    $("#btn_id_input").val($("#to_id").val())
</script>