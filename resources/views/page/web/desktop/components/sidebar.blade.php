@php
    $url = request()->path()
@endphp

<div class="position-fixed overflow-x-hidden overflow-y-scroll h-100 pb-10em p-0 py-3 card shadow-sm w-inherit">
    <div class="d-flex gap-3 px-3">
        <div class="w-3rem h-3rem d-flex align-items-center justify-content-center rounded-circle overflow-hidden border-2">
            <img class="w-100 h-100 object-fit-cover" src="{{asset($user->avatar != null ? 'storage/'.$user->avatar : 'img/avatars/admin.png')}}" alt="">
        </div>

        <span class="font-bold" id="name_user">{{Str::limit($user->name, 17)}}</span>
    </div>
    <hr>

    <div class="d-flex align-items-center justify-content-between gap-3 px-3">
        <div class="d-flex align-items-center gap-3">
            <svg class="icon-size-2" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.326 18.266l-4.326-2.314-4.326 2.313.863-4.829-3.537-3.399 4.86-.671 2.14-4.415 2.14 4.415 4.86.671-3.537 3.4.863 4.829z"/></svg>

            <span class="font-thin text-sm">Point User</span>
        </div>
        <span class="font-thin text-sm">{{$point_user}}</span>
    </div>

    @if($available == 2)
    <a href="{{route('web.open-store.index')}}" class="d-flex text-dark hover-opacity-65 align-items-center mt-3 justify-content-between gap-3 px-3">
        <div class="d-flex align-items-center gap-3">
            <svg class="icon-size-2" xmlns="http://www.w3.org/2000/svg" fill="#4879D3" viewBox="0 0 24 24"><path d="M10 9v-1.098l1.047-4.902h1.905l1.048 4.9v1.098c0 1.067-.933 2.002-2 2.002s-2-.933-2-2zm5 0c0 1.067.934 2 2.001 2s1.999-.833 1.999-1.9v-1.098l-2.996-5.002h-1.943l.939 4.902v1.098zm-10 .068c0 1.067.933 1.932 2 1.932s2-.865 2-1.932v-1.097l.939-4.971h-1.943l-2.996 4.971v1.097zm-4 2.932h22v12h-22v-12zm2 8h18v-6h-18v6zm1-10.932v-1.097l2.887-4.971h-2.014l-4.873 4.971v1.098c0 1.066.933 1.931 2 1.931s2-.865 2-1.932zm15.127-6.068h-2.014l2.887 4.902v1.098c0 1.067.933 2 2 2s2-.865 2-1.932v-1.097l-4.873-4.971zm-.127-3h-14v2h14v-2z"/></svg>

            <span class="font-thin text-sm">Point Toko</span>
        </div>
        <span class="font-thin text-sm">{{$point_store}}</span>
    </a>
    @else
    <a href="{{route('web.open-store.index')}}" class="d-flex text-dark hover-opacity-65 align-items-center mt-3 justify-content-between gap-3 px-3">
        <div class="d-flex align-items-center gap-3">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon-size-2" fill="#4879D3" viewBox="0 0 24 24"><path d="M21 13v10h-21v-19h12v2h-10v15h17v-8h2zm3-12h-10.988l4.035 4-6.977 7.07 2.828 2.828 6.977-7.07 4.125 4.172v-11z"/></svg>                                
            
            <span class="font-thin text-sm">Buka Toko</span>
        </div>
    </a>
    @endif

    <a href="{{route('web.discount')}}" class="d-flex text-dark hover-opacity-65 align-items-center mt-3 justify-content-between gap-3 px-3">
        <div class="d-flex align-items-center gap-3">
            <img class="icon-size-2" src="{{asset('img/other/MyCoupons.png')}}" alt ="">
            <span class="font-thin text-sm">Kupon Saya</span>
        </div>
        <span class="font-thin text-sm">{{$count_cupon}}</span>
    </a>

    <div class="accordion" id="accordionPanelsStayOpenExample">
        <div class="accordion-item" style="border: 0; margin-top: 1em">
            <h2 class="accordion-header" style="padding: 0 6px;" id="panelKotakMasuk-headingOne">
            <button class="accordion-button font-bold text-sm" type="button" data-bs-toggle="collapse" data-bs-target="#panelKotakMasuk-collapseOne" aria-expanded="true" aria-controls="panelKotakMasuk-collapseOne" style="border: 0;padding: 7px 19px;">
                Kotak Masuk
            </button>
            </h2>
            <div id="panelKotakMasuk-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelKotakMasuk-headingOne">
            <div class="accordion-body">
                <a href="{{route('web.chat')}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                <span>Chat</span>
                </a>
                <a href="{{route('web.store.review.menu')}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                <span>Ulasan</span>
                </a>
            </div>
            </div>
        </div>

        <div class="accordion-item" style="border: 0; margin-top: 2px">
            <h2 class="accordion-header font-bold" style="padding: 0 6px;"  id="panelPembelian-headingTwo">
                <button style="border: 0;padding: 7px 19px;" class="accordion-button collapsed text-sm font-bold" type="button" data-bs-toggle="collapse" data-bs-target="#panelPembelian-collapseTwo" aria-expanded="false" aria-controls="panelPembelian-collapseTwo">
                Pembelian
                </button>
            </h2>
            <div id="panelPembelian-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelPembelian-headingTwo">
                <div class="accordion-body">
                    {{-- <a href="{{}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                        <span>Menunggu Pembayaran</span>
                    </a> --}}
                    <a href="{{route('web.transaction.checkout')}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                        <span>Daftar Transaksi</span>
                    </a>
                </div>
            </div>
        </div>
        
        <div class="accordion-item" style="border: 0; margin-top: 2px">
            <h2 class="accordion-header font-bold" style="padding: 0 6px;"  id="panelProfile-headingTwo">
                <button style="border: 0;padding: 7px 19px;" class="accordion-button collapsed text-sm font-bold" type="button" data-bs-toggle="collapse" data-bs-target="#panelProfile-collapseTwo" aria-expanded="false" aria-controls="panelProfile-collapseTwo">
                Profile Saya
                </button>
            </h2>
            <div id="panelProfile-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelProfile-headingTwo">
                <div class="accordion-body">
                    <a href="{{route('web.wishlist')}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                        <span>Wishlist</span>
                    </a>
                    <a href="{{route('web.profile')}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                        <span>Pengaturan</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{-- sidebar --}}
</div>