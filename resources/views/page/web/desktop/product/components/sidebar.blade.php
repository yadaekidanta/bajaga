@php
    $url = request()->path()
@endphp

<div class="position-fixed overflow-x-hidden overflow-y-scroll h-100 pb-4 pt-3 p-0 card shadow-sm w-inherit">
    <div class="d-flex gap-3 px-3">
        <div class="w-3rem h-3rem d-flex align-items-center justify-content-center rounded-circle overflow-hidden border-2">
            <img id="image-store-sidebar" class="w-100 h-100 object-fit-cover" src="{{asset($available->photo != null ? 'storage/'.$available->photo : 'img/avatars/admin.png')}}" alt="">
        </div>
        <div class="">
            <span class="font-bold text-sm" id="store-name">{{Str::limit($available->name, 17)}}</span>
        </div>
    </div>
    
    <div class="d-flex align-items-center mt-3 justify-content-between gap-3 px-3">
        <span class="badge bg-primary">Toko Buka</span>
        <span class="font-thin text-sm" id="open-store-info">
            {{$available->start_at ? $available->start_at->format('H:i') : '..:..'}} -  {{$available->end_at ? $available->end_at->format('H:i') : '..:..'}}
        </span>
    </div>
    
    <div class="d-flex align-items-center mt-3 justify-content-between gap-3 px-3">
        <span class="text-sm">Saldo</span>
        <span class="font-bold text-sm">Rp{{number_format($balance)}}</span>
    </div>
    <div class="d-flex align-items-center mt-3 justify-content-between gap-3 px-3">
        <span class="text-sm">Poin Toko</span>
        <span class="font-bold text-sm">{{$point_store}}</span>
    </div>
    <hr>

    {{-- home --}}
    <a href="{{route('web.product.index')}}" id="home-btn-side" class="text-dark position-relative hover-bg-aliceblue">
        {{-- pointer --}}
        @if ($url == 'product')
            <div class="pointer-card-container">
                <div class="pointer-card-sm bg-primary"></div> 
            </div>
        @endif
        {{-- pointer --}}
        
        <div class="px-3 py-2 d-flex align-items-center gap-3">
            <i class="icon-line-home {{ $url == 'product' ?  'text-primary' : ''}} text-lg"></i>
            <span class="{{ $url == 'product' ?  'text-primary' : ''}} font-bold">Home</span>
        </div>
    </a>
    {{-- home --}}

    {{-- chat --}}
    <a href="{{route('web.chat')}}" id="chat-btn-side" class="text-dark position-relative hover-bg-aliceblue">
        {{-- pointer --}}
        @if ($url == 'chat')
            <div class="pointer-card-container">
                <div class="pointer-card-sm bg-primary"></div> 
            </div>
        @endif
        {{-- pointer --}}

        <div class="px-3 py-2 d-flex align-items-center gap-3">
            <i class="icon-chat {{ $url == 'chat' ?  'text-primary' : ''}} text-lg"></i>
            <span class="{{ $url == 'chat' ?  'text-primary' : ''}} font-bold">Chat</span>
        </div>
    </a>
    {{-- chat --}}

    {{-- ulasan --}}
    <a href="{{route('web.store.review.menu')}}" id="ulasan-btn-side" class="text-dark position-relative hover-bg-aliceblue">
        {{-- pointer --}}
        @if ($url == 'store/review/menu')
            <div class="pointer-card-container">
                <div class="pointer-card-sm bg-primary"></div> 
            </div>
        @endif
        {{-- pointer --}}

        <div class="px-3 py-2 d-flex align-items-center gap-3">
            <i class="icon-comment2 text-lg {{ $url == 'store/review/menu' ?  'text-primary' : ''}}"></i>
            <span class="font-bold {{ $url == 'store/review/menu' ?  'text-primary' : ''}}">Ulasan</span>
        </div>
    </a>
    {{-- ulasan --}}

    {{-- ulasan --}}
    <a href="{{route('web.store.discuss.menu')}}" id="ulasan-btn-side" class="text-dark position-relative hover-bg-aliceblue">
        {{-- pointer --}}
        @if ($url == 'store/discuss/menu')
            <div class="pointer-card-container">
                <div class="pointer-card-sm bg-primary"></div> 
            </div>
        @endif
        {{-- pointer --}}

        <div class="px-3 py-2 d-flex align-items-center gap-3">
            <i class="icon-comment-alt2 text-lg {{ $url == 'store/discuss/menu' ?  'text-primary' : ''}}"></i>
            <span class="font-bold {{ $url == 'store/discuss/menu' ?  'text-primary' : ''}}">Diskusi</span>
        </div>
    </a>
    {{-- ulasan --}}

    {{-- pesanan --}}
    <a href="{{route('web.store.paystore')}}" id="pesanan-btn-side" class="text-dark position-relative hover-bg-aliceblue">
        {{-- pointer --}}
        @if ($url == 'store/paystore')
            <div class="pointer-card-container">
                <div class="pointer-card-sm bg-primary"></div> 
            </div>
        @endif
        {{-- pointer --}}

        <div class="px-3 py-2 d-flex align-items-center gap-3">
            <i class="icon-credit-card1 text-lg {{ $url == 'store/paystore' ?  'text-primary' : ''}}"></i>
            <span class="font-bold {{ $url == 'store/paystore' ?  'text-primary' : ''}}">Saldo Saya</span>
        </div>
    </a>
    {{-- pesanan --}}

    {{-- pesanan --}}
    <a href="{{route('web.store.history.transaction')}}" id="pesanan-btn-side" class="text-dark position-relative hover-bg-aliceblue">
        {{-- pointer --}}
        @if ($url == 'store/history/transaction')
            <div class="pointer-card-container">
                <div class="pointer-card-sm bg-primary"></div> 
            </div>
        @endif
        {{-- pointer --}}

        <div class="px-3 py-2 d-flex align-items-center gap-3">
            <i class="icon-note text-lg {{ $url == 'store/history/transaction' ?  'text-primary' : ''}}"></i>
            <span class="font-bold {{ $url == 'store/history/transaction' ?  'text-primary' : ''}}">Pesanan</span>
        </div>
    </a>
    {{-- pesanan --}}

    {{-- pengaturan --}}
    <a href="{{route('web.store.settings')}}" id="pengaturan-btn-side" class="text-dark position-relative hover-bg-aliceblue">
        @if ($url == 'store/settings')
            <div class="pointer-card-container">
                <div class="pointer-card-sm bg-primary"></div> 
            </div>
        @endif

        <div class="px-3 py-2 d-flex align-items-center gap-3">
            <i class="icon-line2-settings text-lg {{ $url == 'store/settings' ?  'text-primary' : ''}}"></i>
            <span class="font-bold {{ $url == 'store/settings' ?  'text-primary' : ''}}">Pengaturan</span>
        </div>
    </a>
    {{-- pengaturan --}}

    <div class="accordion" id="accordionPanelsStayOpenExample">
        <div class="accordion-item" style="border: 0; margin-top: 1em; padding-bottom: 7em">
            <h2 class="accordion-header" style="padding: 0 6px;" id="panelProduk-headingOne">
                <button class="accordion-button font-bold text-sm" type="button" data-bs-toggle="collapse" data-bs-target="#panelProduk-collapseOne" aria-expanded="true" aria-controls="panelProduk-collapseOne" style="border: 0;padding: 11px 19px;">
                Produk
                </button>
            </h2>
            <div id="panelProduk-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelProduk-headingOne">
                <div class="accordion-body">
                    <a href="{{route('web.store.settings.product')}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                        <span>Tambah Produk</span>
                    </a>
                    <a href="{{route('web.store.settings.product')}}" class="d-flex hover-bg-aliceblue rounded cursor-pointer align-items-center justify-content-between" style="padding: 5px 10px">
                        <span>Daftar Produk</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>