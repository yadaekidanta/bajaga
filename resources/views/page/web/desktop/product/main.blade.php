<x-web-layout title="Profile" keyword="Bajaga Online Store">
    <div id="content_list">
            <br>
            <div class="px-5 m-0" id="kt_content_container">
                <div class="row clearfix gap-4">
                    {{-- sidebar --}}
                    <div  class="w-16em">
                        @include('page.web.desktop.product.components.sidebar')
                    </div>
                    {{-- sidebar --}}

                    {{-- content --}}
                    <div class="col-sm-9 col-md-9 card p-4 pb-5 shadow-sm">
                        <h4 class="font-bold mb-3">Penting hari ini</h4>
                        <span class="text-secondary">Aktivitas yang perlu kamu pantau untuk jaga kepuasan pembeli</span>
                        {{-- {{dd($master["pesanan baru"])}} --}}

                        <div class="row gap-2 p-2">
                            <a href="{{route('web.store.history.transaction')}}" class="w-23% p-3 card shadow-sm cursor-pointer hover-bg-aliceblue">
                                <span>Pesanan baru</span>
                                <h2 class="m-0 mt-3">{{$master["pesanan baru"]}}</h2>
                            </a>
                            <a href="{{route('web.store.history.transaction')}}" class="w-23% p-3 card shadow-sm cursor-pointer hover-bg-aliceblue">
                                <span>Siap dikirim</span>
                                <h2 class="m-0 mt-3">{{$master["siap dikirim"]}}</h2>
                            </a>
                            <a href="{{route('web.chat')}}" class="w-23% p-3 card shadow-sm cursor-pointer hover-bg-aliceblue">
                                <span>Chat Baru</span>
                                <h2 class="m-0 mt-3">{{$chat_baru}}</h2>
                            </a>
                            <a href="{{route('web.store.review.menu')}}" class="w-23% p-3 card shadow-sm cursor-pointer hover-bg-aliceblue">
                                <span>Ulasan baru</span>
                                <h2 class="m-0 mt-3">{{$ulasan_baru}}</h2>
                            </a>
                        </div>

                        {{-- etalase --}}
                        <div class=" mt-4">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="font-bold mb-3">Daftar Etalase</h4>
                                <span class="font-bold hover-opacity-65 cursor-pointer text-primary mb-3" onclick="handle_open_modal('{{route('web.store.settings.product.etalase.add_modal')}}', '#modalListResult', '#contentListResult')">Tambah Etalase</span>
                            </div>

                            {{-- crousal --}}
                            <div>
                                <div class="owl-carousel product-carousel carousel-widget" data-margin="5" data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-md="2" data-items-lg="5" data-items-xl="5" id="list_result_etalase">
                                    @foreach ($etalase as $item)
                                    <div class="oc-item hover-opacity-65 cursor-pointer">
                                        <div class="p-3 card align-items-center justify-content-center w-10em shadow-sm position-relative z-10">
                                            <a href="{{route('web.etalase', $item->id)}}?store_id={{$available->id}}" class="w-5em h-5em">
                                                <img src="{{asset('img/product/'.$item->thumbnail)}}" alt="image etalase">
                                            </a>
                                            <a href="" class="font-bold text-dark">{{Str::limit($item->name, 10)}}</a>

                                            <div class="position-absolute z-20 top-0 right-0 p-2">
                                                <button onclick="handle_confirm_costum('Anda yakin?', 'Yes', 'No', 'DELETE', '{{route('web.store.settings.product.etalase.destroy', $item->id)}}', true, 'Perubahan ini tidak bisa dikemblaikan!')" class="btn btn-sm btn-outline-danger">
                                                    <i class="icon-trash2"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            {{-- crousal --}}

                        </div>
                        {{-- etalase --}}

                        {{-- daftar produk --}}
                        <div class=" mt-5">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="font-bold mb-3">Daftar Produk</h4>
                                <span class="font-bold hover-opacity-65 cursor-pointer text-primary mb-3" onclick="handle_open_modal('{{route('web.product.add_modal')}}', '#modalListResult', '#contentListResult')">Tambah Produk</span>
                            </div>

                            {{-- list produk --}}
                            <div class="row gap-3 p-3" id="list_result"></div>
                            <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                                <div class="spinner-grow text-secondary" role="status">
                                </div>
                            </div>
                            {{-- list produk --}}
                        </div>
                        {{-- daftar produk --}}

                    </div>
                    {{-- content --}}
                </div>
            </div>
    </div>
    
    @section('custom_js')
    <script>

        
        load_list(1);
        localStorage.setItem("page_infinate", 1)

        function handle_confirm_costum(title, confirm_title, deny_title, method, route, idtmp = null, text) {
            handle_confirm(title, confirm_title, deny_title, method, route, true, function() {
                location.reload()
            }, text)
        }

        $(document).ready(function(){
            let options = {
                root: null,
                rootMargin: '50px',
                threshold: 0.5
            }

            const observer = new IntersectionObserver(handleIntersect, options)
            observer.observe(document.querySelector(".ajax-load"))
        })
        
        function handleIntersect(entries){
            if (entries[0].isIntersecting) {
                localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                // console.log(pages);
                load_more(parseInt(localStorage.getItem("page_infinate")));
            }
        }
    </script>
    @endsection
</x-web-layout>