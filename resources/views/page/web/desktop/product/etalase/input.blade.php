<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Tambah Etalase</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <form id="form_input" class="m-0">
        <div class="d-flex gap-2 flex-column">
            <div class="form-group m-0">
                <label for="etalase" class="form-label m-0">Nama etalase</label>
                <input type="text" id="etalase" autocomplete="off"  name="name" class="form-control w-100 py-2">
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button type="button" id="btn_submit" onclick="handle_save_custom('#btn_submit', '#form_input', '{{route('web.store.settings.product.etalase.store')}}', 'POST')" class="btn btn-primary">Save changes</button>
</div>

{{-- @if (count($master)) --}}
    <script>
        
        function handle_save_custom(tombol, form, url, method){
            handle_save(tombol, form, url, method, function() {
                $(tombol).html('Save changes')
                location.reload();
            }, null, null, true)
        }
    </script>
{{-- @endif --}}
