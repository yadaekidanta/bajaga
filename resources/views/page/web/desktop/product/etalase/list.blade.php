@foreach ($etalase as $item)
<a href="" class="oc-item hover-opacity-65 cursor-pointer">
    <div class="p-3 card align-items-center justify-content-center position-relative w-10em shadow-sm">
        <div class="w-5em h-5em">
            <img src="{{asset('img/product/'.$item->thumbnail)}}" alt="image etalase">
        </div>
        <span class="font-bold text-dark">{{$item->name}}</span>

        <div class="position-absolute top-0 right-0">
            <button class="btn btn-sm btn-outline-danger">
                <i class="icon-trash2 text-danger"></i>
            </button>
        </div>
    </div>
</a>
@endforeach