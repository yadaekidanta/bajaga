<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Tambah Rekening Bank</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="p-4 modal-body">
    <form id="form_input">
        <div class="d-flex gap-2 flex-column mt-2">
            <div class="form-group m-0 mt-3">
                <label for="courier" class="form-label m-0">Kurir</label>
                <input type="text" id="courier" disabled name="courier" value="{{$courier}}" class="form-control w-100 py-2">
                <input type="hidden" id="id_sale" name="id_sale" value="{{$id_sale}}" class="form-control w-100 py-2">
                <input type="hidden" id="code_courier" name="code_courier" value="{{$code_courier}}" class="form-control w-100 py-2">
            </div>
            <div class="form-group m-0 mt-3">
                <label for="no_resi" class="form-label m-0">No Resi</label>
                <input type="text" id="no_resi" autocomplete="off"  name="no_resi" class="form-control w-100 py-2">
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button type="button" id="btn_submit" onclick="handle_save_custom('#btn_submit', '#form_input', '{{route('web.store.tracking_number')}}', 'POST')" class="btn btn-primary">Save changes</button>
</div>

@if ($courier)
    <script>
        
        function handle_save_custom(tombol, form, url, method){
            handle_save(tombol, form, url, method, function() {
                $(tombol).html('Save changes')
                load_list(1)
                // load_custom('#list_result_bank', '{{route('web.profile.bank_account.getDataBankUser')}}', 1)
            })
        }
    </script>
@endif
