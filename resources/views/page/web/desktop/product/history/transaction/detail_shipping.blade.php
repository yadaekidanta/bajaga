<div class="px-3" id="timeline">
    @for($i = 0; $i < count($response["manifest"]); $i++)
    <div class="d-flex gap-3 mt-3">
        <div class="bullet-tracker"></div>
        <div style="width: 90%;margin-top: -2px;">
            <span class="font-bold">{{$response["manifest"][$i]["manifest_description"]}}</span>
            <span class="font-sm">( {{$response["manifest"][$i]["city_name"]}} )</span>
            <span class="d-block font-sm">
                {{$response["manifest"][$i]["manifest_date"]}} {{$response["manifest"][$i]["manifest_time"]}}
            </span>
        </div>
    </div>
    @endfor
    <div class="d-flex gap-3 mt-3">
        <div class="bullet-tracker"></div>
        <div style="width: 90%;margin-top: -2px;">
            <span class="font-bold">Bejaga - {{$sale->updated_at}}</span>
            <span class="d-block font-sm">
                Pembayaran sudah Diverifikasi.
                <br>
                Pembayaran telah diterima Bajaga dan pesanan Anda sudah diteruskan ke penjual.
            </span>
        </div>
    </div>
</div>
