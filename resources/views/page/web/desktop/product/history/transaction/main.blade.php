<x-web-layout title="Profile" keyword="Bajaga Online Store">
    <div id="content_list">
            <br>
            <div class="px-5 m-0" id="kt_content_container">
                <div class="row clearfix gap-4">
                    {{-- sidebar --}}
                    <div  class="w-16em">
                        @include('page.web.desktop.product.components.sidebar')
                    </div>
                    {{-- sidebar --}}
                    {{-- content --}}
                    <div class="col-sm-9 col-md-9 card p-4 pb-5 shadow-sm min-h-29em">
                     
                        <div class="">
                            <ul class="nav nav-tabs d-flex justify-content-between" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="semua-tab" data-bs-toggle="tab" data-bs-target="#semua" type="button" role="tab" aria-controls="semua" aria-selected="true">Semua Pesanan</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="baru-tab" data-bs-toggle="tab" data-bs-target="#baru" type="button" role="tab" aria-controls="baru" aria-selected="false">Pesanan Baru</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="siap-tab" data-bs-toggle="tab" data-bs-target="#siap" type="button" role="tab" aria-controls="siap" aria-selected="false">Siap Dikirim</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="delivery-tab" data-bs-toggle="tab" data-bs-target="#delivery" type="button" role="tab" aria-controls="delivery" aria-selected="false">Dalam Pengirman</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="done-tab" data-bs-toggle="tab" data-bs-target="#done" type="button" role="tab" aria-controls="done" aria-selected="false">Pesanan Selesai</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div id="list_result">  
                                </div>
                                <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                                    <div class="spinner-grow text-primary" role="status">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row gap-3 p-3" id="list_result"></div> --}}
                    </div>
                    {{-- content --}}
                </div>
            </div>
            <form id="content_filter" class="d-none">
                <input type="hidden" id="filter_input" name="filter">
            </form>
    </div>


    @section('custom_js')
        <script>
            // let state = "semua"
            localStorage.setItem("page_infinate", 1)
            load_list(1)
            

            $(document).ready(function(){
                let options = {
                    root: null,
                    rootMargin: '50px',
                    threshold: 0.5
                }

                const observer = new IntersectionObserver(handleIntersect, options)
                observer.observe(document.querySelector(".ajax-load"))
            })
            
            function handleIntersect(entries){
                if (entries[0].isIntersecting) {
                    localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                    // console.log(pages);
                    load_more(parseInt(localStorage.getItem("page_infinate")));
                }
            }

            function resetState(){
                $(".ajax-load").html(`<div class="spinner-grow text-primary" role="status"></div>`)
                localStorage.setItem("page_infinate", 1)
            }


            var tabEl = document.querySelectorAll('button[data-bs-toggle="tab"]')
            tabEl.forEach(element => {
                element.addEventListener('shown.bs.tab', function (event) {
                    resetState()
                    let target = $(event.target).attr("data-bs-target")
                    if (target == "#semua") {
                        // doit ajax
                        // $("#filter_input").val()
                        $("#filter_input").val('')
                        load_list(1)
                    }else if(target == "#baru"){
                        $("#filter_input").val('Tertunda')
                        load_list(1)
                    }else if(target == "#siap"){
                        $("#filter_input").val('Dipesan')
                        load_list(1)
                    }else if(target == "#delivery"){
                        $("#filter_input").val('Dikirim')
                        load_list(1)
                    }else if(target == "#done"){
                        $("#filter_input").val('Selesai')
                        load_list(1)
                    }
                    // console.log(target);
                    // event.target // newly activated tab
                    // event.relatedTarget // previous active tab
                })
            });
        </script>
    @endsection
</x-web-layout>