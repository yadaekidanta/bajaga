<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Detail Transaksi</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="p-0 modal-body h-33em">
    
    <div class="d-flex h-100">
        <div class="col-md-8 overflow-y-scroll">

            {{-- section 1 --}}
            <div class="p-4">

                <div class="d-flex justify-content-between">
                    <span class="font-bold">Selesai</span>
                    <div role="button" data-bs-toggle="collapse" data-bs-target="#collapseDetailTrack" aria-expanded="false" aria-controls="collapseDetailTrack" class="text-primary">
                        Lihat Detail
                    </div>
                </div>
                <div class="collapse" id="collapseDetailTrack">
                    <div class="card card-body">
                        <div id="result_timeline">
                            <div class="w-100 h-100 d-flex justify-content-center mb-5 mt-5">
                                <div class="spinner-border spinner-border-sm text-primary spinner" role="status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="">

                <div class="d-flex justify-content-between align-item-center">
                    <span class="text-secondary text-sm">No. Invoice</span>
                    <span class="text-primary font-bold text-sm">{{$sale->code}}</span>
                </div>

                <div class="d-flex mt-3 justify-content-between align-items-center">
                    <span class="text-secondary text-sm">Tanggal Pembelian</span>
                    <span class="text-primary font-bold text-sm">{{$sale->created_at}}</span>
                </div>
            </div>
            {{-- section 1 --}}
            
            <hr class="h-7px m-0">
            
            {{-- section 2 --}}
            <div class="p-4">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-bold">Detail Produk</span>
                    <a href="{{route('web.store', $sale->store->id)}}" class="d-flex gap-2 align-item-center">
                        <img style="width: 19px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="image badge">
                        <span class="text-primary font-bold text-sm">{{$sale->store->name}}</span>
                        
                    </a>
                </div>
                @php
                    $price = 0;
                @endphp
                @foreach ($sale->sale_detail()->get() as $item)
                    @php
                        $price += $item->price;
                    @endphp
                    <div class="card p-3 mt-3">

                        <div class="d-flex">
                            {{-- left --}}
                            <div class="col-lg-8 d-flex gap-3">
                                <div class="w-4rem h-4rem d-flex rounded overflow-hidden justify-content-center align-items-center">
                                    <img src="{{asset('storage/'.$item->product->photo)}}" class="object-fit-cover w-100 h-100" alt="image product">
                                </div>
                                <div>
                                    <span class="font-bold ">
                                        {{Str::limit($item->product->name, 15)}}
                                    </span>
                                    <span class=" text-secondary d-block">{{$item->qty}} x Rp{{number_format($item->price)}}</span>
                                </div>
                            </div>
                            {{-- left --}}
    
                            {{-- right --}}
                            <div class="col-lg-4 d-flex flex-column align-items-end">
                                <span>Total Harga</span>
                                <span class="text-danger font-bold">Rp{{ number_format($item->qty * $item->price) }}</span>
    
                                @if (Auth::user()->store)
                                    @if($sale->st == 'Selesai' && $sale->store_id != Auth::user()->store->id)
                                        <a href="{{route('web.product.show', $item->product->slug)}}" class="btn w-100 btn-primary mt-4 btn-sm">Beli Lagi</a>
                                    @elseif($sale->payment_st == "Lunas" && $sale->st == "Dikirim")
                                        <button type="button" data-bs-toggle="collapse" data-bs-target="#collapseDetailTrack" aria-expanded="false" aria-controls="collapseDetailTrack" class="btn btn-outline-primary w-100 mt-4 btn-sm">Lacak</button>
                                    @elseif($sale->payment_st == "Belum lunas" && $sale->store_id != Auth::user()->store->id)
                                        <button class="btn btn-primary w-100  mt-4 btn-sm">Bayar Sekarang</button>
                                    @elseif($sale->st == "Diterima" && $sale->store_id != Auth::user()->store->id)
                                        <button class="btn btn-primary w-100  mt-4 btn-sm" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasUlasan" aria-controls="offcanvasUlasan">Beri Ulasan</button>
                                    @else
                                        @if($sale->store_id != Auth::user()->store->id)
                                            <a href="{{route('web.chat.detail', $sale->store->user->id)}}"  class="btn btn-outline-primary w-100  mt-4 btn-sm">Chat</a>    
                                        @endif
                                    @endif
                                    
                                @else
                                    @if($sale->st == 'Selesai')
                                        <a href="{{route('web.product.show', $item->product->slug)}}" class="btn w-100 btn-primary mt-4 btn-sm">Beli Lagi</a>
                                    @elseif($sale->payment_st == "Lunas" && $sale->st == "Dikirim")
                                        <button type="button" data-bs-toggle="collapse" data-bs-target="#collapseDetailTrack" aria-expanded="false" aria-controls="collapseDetailTrack" class="btn btn-outline-primary w-100 mt-4 btn-sm">Lacak</button>
                                    @elseif($sale->payment_st == "Belum lunas")
                                        <button class="btn btn-primary w-100  mt-4 btn-sm">Bayar Sekarang</button>
                                    @elseif($sale->st == "Diterima")
                                        <button class="btn btn-primary w-100  mt-4 btn-sm" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasUlasan" aria-controls="offcanvasUlasan">Beri Ulasan</button>
                                    @else
                                        @if($sale->store_id != Auth::user()->store->id)
                                            <a href="{{route('web.chat.detail', $sale->store->user->id)}}"  class="btn btn-outline-primary w-100  mt-4 btn-sm">Chat</a>    
                                        @endif
                                    @endif
                                @endif
                                
                            </div>
                            {{-- right --}}
                        </div>
                    </div>
                @endforeach
            </div>
            {{-- section 2 --}}

            <hr class="h-7px m-0">

            {{-- section 3 --}}
            <div class="p-4">
                <span class="font-bold">Info Pengiriman</span>
                @if($sale->st == 'Dikirim' || $sale->st == 'Sampai' || $sale->st == 'Selesai')
                    <div class="d-flex align-items-center mt-1">
                        <span class="text-secondary w-7rem">Kurir</span>
                        <span>{{$sale->courier}}</span>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="text-secondary w-7rem d-flex align-items-center gap-2">
                            <span>No Resi</span>
                            <svg class="unf-icon" viewBox="0 0 24 24" width="16" height="16" fill="var(--color-icon-enabled, #2E3137)" style="display: inline-block; vertical-align: middle;"><path fill-rule="evenodd" clip-rule="evenodd" d="M16.53 2.466a.76.76 0 0 1 .22.534.75.75 0 0 1-.75.75H6c-.19 0-.25.07-.25.25v14a.75.75 0 1 1-1.5 0V4A1.7 1.7 0 0 1 6 2.24h10c.2.003.39.084.53.226ZM9 5.24h9A1.7 1.7 0 0 1 19.75 7v13A1.69 1.69 0 0 1 18 21.74H9A1.69 1.69 0 0 1 7.25 20V7A1.7 1.7 0 0 1 9 5.24Zm9 15.01c.19 0 .25-.06.25-.25V7c0-.18-.06-.25-.25-.25H9c-.19 0-.25.07-.25.25v13c0 .19.06.25.25.25h9ZM16 9.24h-5a.75.75 0 1 0 0 1.5h5a.75.75 0 1 0 0-1.5Zm-5 4h3a.75.75 0 1 1 0 1.5h-3a.75.75 0 1 1 0-1.5Z"></path></svg>
                        </div>
                        <span>{{$sale->no_resi}}</span>
                    </div>
                @endif
                <div class="d-flex align-items-start mt-3">
                    <div class="text-secondary w-7rem d-flex align-items-center gap-2">
                        <span class="text-secondary text-sm">Alamat</span>
                    </div>
                    <div class="col">
                        <p class="font-bold m-0 text-sm">{{$sale->customer_name}}</p>
                        <p class="m-0 text-sm">{{$sale->customer_phone}}</p>
                        <p class="m-0 text-sm">{{$sale->shipping_detail}}</p>
                    </div>
                        
                </div>
            </div>
            {{-- section 3 --}}
            
            <hr class="h-7px m-0">

            {{-- section 4 --}}
            <div class="p-4">
                <div class="font-bold">Rincian Pembayaran</div>

                <div class="d-flex align-items-center justify-content-between mt-3">
                    <span class="text-secondary text-sm">Metode Pembayaran</span>
                    <span class="text-secondary text-sm">{{$sale->payment_method}}({{$sale->payment_channel}})</span>
                </div>
                <hr class="my-2">
                <div class="d-flex align-items-center justify-content-between">
                    <span class="text-secondary text-sm">Total Harga ({{count($sale->sale_detail()->get())}} Barang)</span>
                    <span>Rp{{number_format($price)}}</span>
                </div>
                <div class="d-flex align-items-center mt-2 justify-content-between">
                    <span class="text-secondary text-sm">Total Ongkos Kirim</span>
                    <span class="text-secondary text-sm">Rp{{number_format($sale->shipping_price)}}</span>
                </div>
                @if($sale->disc_price)
                <div class="d-flex align-items-center justify-content-between">
                    <span class="text-secondary text-sm">Diskon</span>
                    <span class="text-secondary text-sm">- Rp{{number_format($sale->disc_price)}}</span>
                </div>
                @endif
                <hr class="my-2">
                <div class="d-flex align-items-center justify-content-between">
                    <span class="font-bold">Total Belanja</span>
                    <span>Rp{{number_format($sale->grand_total)}}</span>
                </div>


            </div>
            {{-- section 4 --}}
            
        </div>
        {{-- sidebar --}}
        <div class="h-100 col-md-4 p-3 overflow-hidden position-relative">
            <button class="btn btn-outline-secondary w-100">Chat Penjual</button>
            <div class="w-100 overflow-hidden position-absolute bottom-0 left-0">
                <img src="{{asset('img/other/character-2.png')}}" class="w-18em" alt="image character">
            </div>
        </div>
        {{-- sidebar --}}
    </div>

</div>

<script>
    var myCollapsible = document.getElementById('collapseDetailTrack')
    myCollapsible.addEventListener('show.bs.collapse', function () {
        // console.log("{{route('web.shipping.get_detail', $sale->id)}}");
        load_custom("#result_timeline", "{{route('web.shipping.get_detail', $sale->id)}}", 1)
    })
</script>