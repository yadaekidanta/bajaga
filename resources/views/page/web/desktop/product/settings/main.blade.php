<x-web-layout title="Profile" keyword="Bajaga Online Store">
    <div id="content_list">
            <br>
            <div class="px-5 m-0" id="kt_content_container">
                <div class="row clearfix gap-4">

                    {{-- sidebar --}}
                    <div  class="w-16em">
                        @include('page.web.desktop.product.components.sidebar')
                    </div>
                    {{-- sidebar --}}
                    {{-- content --}}
                    <div class="col-sm-9 col-md-9 card p-4 pb-5 shadow-sm min-h-29em">
                        
                        <div class="" id="list_result">
                            
                        </div>
                        {{-- <div class="row gap-3 p-3" id="list_result"></div> --}}
                    </div>
                    {{-- content --}}
                    
                </div>
            </div>
            <form id="content_filter" class="d-none">
                <input type="hidden" id="filter_input" name="filter">
            </form>
    </div>
    @section('custom_js')
        <script>
            // let state = "semua"
            localStorage.setItem("page_infinate", 1)
            load_list(1)
        </script>
    @endsection
</x-web-layout>