{{-- name store --}}
<div class="d-flex align-items-center gap-2">
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M21.1 6.5l.45.75L22 8a2.5 2.5 0 0 1-2.5 2.5c-.558 0-1.07-.189-1.486-.5A2.49 2.49 0 0 1 17 8a2.49 2.49 0 0 1-1.013 2c-.416.311-.928.5-1.487.5-.558 0-1.07-.189-1.486-.5A2.49 2.49 0 0 1 12 8a2.49 2.49 0 0 1-1.013 2c-.416.311-.928.5-1.487.5-.558 0-1.07-.189-1.486-.5A2.49 2.49 0 0 1 7 8a2.49 2.49 0 0 1-1.013 2A2.473 2.473 0 0 1 4 10.449 2.5 2.5 0 0 1 2 8l.45-.75.451-.75L5 3h14l2.1 3.5zM14.5 12c.945 0 1.814-.329 2.5-.88.686.551 1.555.88 2.5.88.137 0 .27-.019.402-.037l.098-.014V19a2 2 0 0 1-2 2h-3v-4a2 2 0 0 0-2-2h-2a2 2 0 0 0-2 2v4H6a2 2 0 0 1-2-2v-7.05l.098.013c.133.018.265.037.402.037.945 0 1.814-.329 2.5-.88.686.551 1.555.88 2.5.88.945 0 1.814-.329 2.5-.88.686.551 1.555.88 2.5.88z" fill="#6C727C"/></svg>
    <span class="font-bold text-secondary">{{$available->name}}</span>
</div>
{{-- name store --}}

<form id="form_input" action="">
    {{-- gambar toko --}}
    <div class="mt-4 d-flex flex-column align-items-center">
            <div class="mt-4 d-flex flex-column align-items-center">
                <div class="w-13rem h-13rem overflow-hidden d-flex align-items-center justify-content-center border rounded">
                    <img id="image-preview" src="{{asset('storage/'.$available->photo)}}" class="w-100 h-100 object-fit-cover" alt="image store">
                </div>
                <label for="photo-input" class="btn btn-outline-primary text-capitalized mt-3">Pilih Foto</label>
                <input type="file" class="d-none" name="photo" id="photo-input">
            </div>
    </div>
    {{-- gambar toko --}}

    {{-- informasi toko --}}
    <div class="">
        <span class="font-bold">Informasi Toko</span>

        <div class="d-flex gap-4 mt-3">
            {{-- <div class="col">
                <div class="card p-3 shadow-sm h-13em justify-content-between">
                    <div class="d-flex flex-column">
                        <span class="font-bold text-sm">Nama Toko</span>
                        <span class="text-sm">{{$available->name}}</span>
                        <span class="font-bold text-sm mt-3">Domain Toko</span>
                        <span class="text-sm">{{env('APP_URL')}}/store/ <span class="font-bold text-sm">{{$available->name}}</span> </span>
                    </div>
            
                    <button class="btn btn-outline-secondary text-sm">Ubah</button>
                </div>
            </div> --}}
            <div class="col">
                <input type="text" value="{{$available->name}}" name="name" id="name" placeholder="Nama Toko anda" class="form-control">
                <textarea name="desc" id="desc" rows="5" placeholder="Deskripsi toko" class="form-control h-100 my-3">{{$available->desc}}</textarea>
            </div>

            <div class="d-flex justify-content-end position-absolute top-0 right-0 p-3">
                <button class="btn btn-primary text-sm px-5" id="tombol_simpan" onclick="handle_upload_custom('#tombol_simpan','#form_input','{{route('web.store.settings.profile')}}','PATCH', true);">Simpan</button>
            </div>
        </div>
    </div>
    {{-- informasi toko --}}

    {{-- status toko --}}
    <div class="mt-6em">
        <span class="font-bold">Atur jadwal buka toko</span>
        <span class="text-secondary font-thin d-block mt-3">Tentukan hari dan jam berapa tokomu bisa melayani pembeli. Kamu masih bisa menerima pesanan dari pembeli meskipun telah melawati jadwal buka tokomu.</span>

        <div class="d-flex gap-3">
            <div class="form-group m-0">
                <label for="start-at-input" class="text-capitalized">Jam Buka</label>
                <input type="time" class="form-control" name="start_at" id="start-at-input" value="{{$available->start_at}}">
            </div>
            <div class="form-group m-0">
                <label for="end-at-input" class="text-capitalized">Jam Tutup</label>
                <input type="time" class="form-control" name="end_at" id="end-at-input" value="{{$available->end_at}}">
            </div>
        </div>
    </div>
    {{-- status toko --}}

</form>
<script>
    function handle_upload_custom(tombol, form, url, method, idTmp = null, cb = null, text_button = null){
        handle_upload(tombol, form, url, method, true, function(){
            if ($("#photo-input")[0].files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                    $('#image-preview').attr('src', e.target.result);
                    $('#image-store-sidebar').attr('src', e.target.result);
                }
                reader.readAsDataURL($("#photo-input")[0].files[0]);
            }
            $("#store-name").html($("#name").val())
            $("#open-store-info").html($("#start-at-input").val() + "-" + $("#end-at-input").val())
        }, 'Simpan')
    }
</script>