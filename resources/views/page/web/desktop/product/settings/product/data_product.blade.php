@foreach ($collections as $item)
    <div class="d-flex gap-2 rounded">
        <div class="col-4">
            <div class="w-100 h-8em overflow-hidden rounded">
                <img src="{{asset('storage/'.$item->photo)}}" alt="image product" class="object-fit-cover w-100 h-100">
            </div>
        </div>

        <div style="width: 1px;" class="bg-dark"></div>
        
        <div class="col-6">
            <div class="mt-1 text-lg font-bold">{{$item->name}}</div>
            <div class="mt-1 font-semibold">Rp{{number_format($item->price)}}</div>
            <div class="mt-1 font-thin">Stok : {{$item->stock}}</div>
        </div>

        <div class="col-2">
            <button class="btn btn-sm w-100 btn-primary" onclick="handle_open_modal('{{route('web.store.settings.product.price.update_modal', $item->id)}}', '#modalListResult', '#contentListResult')">Ubah Harga</button>

            <button class="btn btn-sm w-100 btn-warning d-block mt-2" onclick="handle_open_modal('{{route('web.store.settings.product.stok.update_modal', $item->id)}}', '#modalListResult', '#contentListResult')">Ubah Stok</button>
        </div>
    </div>
@endforeach