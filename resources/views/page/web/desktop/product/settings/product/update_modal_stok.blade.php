<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Update Address</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="p-4 modal-body">
    <form id="form_input_stock">
        <div class="d-flex justify-content-between align-items-center">
            <span class="font-lg">Jumlah Stok</span>
            <input type="hidden" id="stok_input" name="stock" value="{{$product->stock}}"> 
            <input type="hidden" id="qty"> <!-- tidak untuk di store -->
            <input type="hidden" id="id_product_to_stock" name="id_product" value="{{$product->id}}"> 
            <!-- counter item -->
            <div class="d-flex justify-content-between px-1 align-items-center gap-3 border border-dark rounded">
                <i class="icon-line-minus decrement font-lg cursor-pointer hover-opacity-65"></i>
                <span class="font-lg" id="stok_count">{{$product->stock}}</span>
                <i class="icon-line-plus increment font-lg cursor-pointer hover-opacity-65"></i>
            </div>
            <!-- counter item -->
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button class="btn btn-primary" type="submit" id="btn_submit_stock" onclick="ajaxUpdate('#btn_submit_stock', '#form_input_stock', '{{route('web.store.settings.product.stock')}}', 'PATCH', 'stock')">Simpan</button>
</div>

{{-- @if() --}}
<script>
    $(".decrement").on('click', function (event) {
        $target = $(event.target);
        let count_item = parseInt($target.next().html())
        if ( count_item > 1) {
            $target.next().html(count_item - 1)
        }
        $("#stok_input").val(parseInt($("#stok_count").text()))
    });

    $(".increment").on('click', function (event) {
        $target = $(event.target);
        let count_item = parseInt($target.prev().html())
        let qty = $("#qty").val();
        if (count_item > qty) {
            $target.prev().html(count_item + 1)
            $("#stok_input").val(parseInt($("#stok_count").text()))
        }
    });
    function ajaxUpdate(tombol, form, url, method, state){
            $(tombol).submit(function() {
                return false;
            });
            
            let data = $(form).serialize();
            
            $(tombol).prop("disabled", true);
            
            // console.log(data);
            $.ajax({
                type: method,
                url: url,
                data: data,
                dataType: 'json',
                processData: false,
                success: function(response) {
                        console.log(response);
                        if (response.alert == "success") {
                            Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                            $(tombol).prop("disabled", false);
                            load_list(1)
                        } else {
                            Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                            $(tombol).prop("disabled", false);
                        }
                    },

                error: function (response) {
                    Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    $(tombol).prop("disabled", false);
                }
            });
        }
</script>
{{-- @endif --}}