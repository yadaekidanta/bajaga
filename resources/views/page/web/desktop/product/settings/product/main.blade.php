<x-web-layout title="Profile" keyword="Bajaga Online Store">
    <div id="content_list">
            <br>
            <div class="px-5 m-0" id="kt_content_container">
                <div class="row clearfix gap-4">
                    {{-- sidebar --}}
                    <div  class="w-16em">
                        @include('page.web.desktop.product.components.sidebar')
                    </div>
                    {{-- sidebar --}}

                    {{-- content --}}
                    <div class="col-sm-9 col-md-9 card p-4 pb-5 shadow-sm">
                        <div class="d-flex justify-content-between align-items-start">
                            <div>
                                <h4 class="font-bold mb-3">Daftar produk</h4>
                                <span class="text-secondary">Selalu cek stok barang kamu supaya pelayanan tokomu semakin baik.</span>
                            </div>
    
                            <div>
                                <button class="btn btn-sm btn-primary px-4 h-100" onclick="handle_open_modal('{{route('web.product.add_modal')}}', '#modalListResult', '#contentListResult')">Tambah Produk</button>

                            </div>
                        </div>
                        
                        <div class="w-100">
                            {{-- sort --}}
                            {{-- <div class="w-100 d-flex justify-content-end">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                      Urutkan
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li>
                                            <span onclick="filter('semua')" data-target="semua" class="dropdown-item cursor-pointer semua" href="#">Semua</span>
                                        </li>
                                        <li>
                                            <span onclick="filter('tranding')" data-target="tranding" class="dropdown-item cursor-pointer tranding" href="#">Paling laku</span>
                                        </li>
                                        <li>
                                            <span onclick="filter('stok')" data-target="stok" class="dropdown-item cursor-pointer stok" href="#">Stok mau habis</span>
                                        </li>
                                    </ul>
                                </div>
                            </div> --}}
                            {{-- sort --}}

                            {{-- daftar produk --}}
                            <div class="mt-5">
                                <div class="row gap-3 p-3" id="list_result">
                                    
                                </div>
                                <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                                    <div class="spinner-grow text-secondary" role="status">
                                    </div>
                                </div>
                            </div>
                            {{-- daftar produk --}}
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <form id="content_filter" class="d-none">
        <input type="text" name="keyword" id="filter_input">
    </form>

    @section('custom_js')
        <script>
            // let state = "semua"
            localStorage.setItem("page_infinate", 1)
            load_list(1)
            

            $(document).ready(function(){
                let options = {
                    root: null,
                    rootMargin: '50px',
                    threshold: 0.5
                }

                const observer = new IntersectionObserver(handleIntersect, options)
                observer.observe(document.querySelector(".ajax-load"))
            })
            
            function handleIntersect(entries){
                if (entries[0].isIntersecting) {
                    localStorage.setItem("page_infinate", parseInt(localStorage.getItem("page_infinate")) + 1)
                    // console.log(pages);
                    load_more(parseInt(localStorage.getItem("page_infinate")));
                }
            }

            function resetState(){
                $(".ajax-load").html(`<div class="spinner-grow text-primary" role="status"></div>`)
                localStorage.setItem("page_infinate", 1)
            }


            var tabEl = document.querySelectorAll('button[data-bs-toggle="dropdown"]')
            tabEl.forEach(element => {
                element.addEventListener('shown.bs.dropdown', function (event) {
                    // alert()
                    // resetState()
                    let target = $(event.target).attr("data-bs-target")
                    if (target == "#semua") {
                        // doit ajax
                        // $("#filter_input").val()
                        $("#filter_input").val('')
                        load_list(1)
                    }else if(target == "#baru"){
                        $("#filter_input").val('Tertunda')
                        load_list(1)
                    }else if(target == "#siap"){
                        $("#filter_input").val('Dipesan')
                        load_list(1)
                    }else if(target == "#delivery"){
                        $("#filter_input").val('Dikirim')
                        load_list(1)
                    }else if(target == "#done"){
                        $("#filter_input").val('Selesai')
                        load_list(1)
                    }
                    // console.log(target);
                    // event.target // newly activated tab
                    // event.relatedTarget // previous active tab
                })
            });

            // function filter(f){
            //     if (f == "semua") {
            //             // doit ajax
            //             // $("#filter_input").val()
            //         $("#filter_input").val('')
            //         load_list(1)
            //     }else if(f == "tranding"){
            //         $("#filter_input").val('tranding')
            //         load_list(1)
            //     }else if(f == "stok"){
            //         $("#filter_input").val('stok')
            //         load_list(1)
            //     }
            // }
        </script>
    @endsection
</x-web-layout>