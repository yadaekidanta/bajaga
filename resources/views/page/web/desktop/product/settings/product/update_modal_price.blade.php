<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Update Address</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="p-4 modal-body">
    <form id="form_input_price">
        <div class="position-relative">
            <input type="hidden" name="id_product" id="id_product" value="{{$product->id}}">
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">@</span>
                <input id="price" type="text" name="price" class="form-control maximum-filter " placeholder="Min. Rp 100" aria-describedby="basic-addon1" value="{{number_format($product->price)}}">
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button id="btn_submit" onclick="ajaxUpdate('#btn_submit', '#form_input_price', '{{route('web.store.settings.product.price')}}', 'PATCH', 'price')" class="btn btn-primary" type="submit">Simpan</button>
</div>

{{-- @if() --}}
<script>
    number_only('price');
    ribuan('price');
    function ajaxUpdate(tombol, form, url, method, state){
            $(tombol).submit(function() {
                return false;
            });
            
            let data = $(form).serialize();
            
            $(tombol).prop("disabled", true);
            
            // console.log(data);
            $.ajax({
                type: method,
                url: url,
                data: data,
                dataType: 'json',
                processData: false,
                beforeSend: function() {
                    if (state == "price") {
                        let splitData = this.data.split('&price=')
                        let bid = splitData[1].replaceAll('.', '')
                        this.data = `${splitData[0]}&price=${bid}`
                    }
                },
                success: function(response) {
                        console.log(response);
                        if (response.alert == "success") {
                            Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                            $(tombol).prop("disabled", false);
                            load_list(1)
                        } else {
                            Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                            $(tombol).prop("disabled", false);
                        }
                    },

                error: function (response) {
                    Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    $(tombol).prop("disabled", false);
                }
            });
        }
</script>
{{-- @endif --}}