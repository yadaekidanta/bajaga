<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Tambah Etalase</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<form id="form_input">
<div class="modal-body">
        <div>
            <div class="mb-3">
                <label class="required fs-6 fw-bold mb-2">Nama</label>
                <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$product->name}}">
            </div>
            <div class="mb-3">
                <label class="required fs-6 fw-bold mb-2">Harga</label>
                <input type="text" class="form-control" name="price" id="price" placeholder="Masukkan Harga..." value="{{$product->price}}">
            </div>
            <div class="mb-3">
                <label for="condition" class="required form-label">Stok</label>
                <input type="tel" class="form-control" id="stok" name="stok" placeholder="Masukkan Jumlah Peringatan..." value="{{$product->stock}}">
            </div>
            <div class="mb-3">
                <label for="condition" class="required form-label">Jumlah Peringatan</label>
                <input type="text" class="form-control" name="alert_quantity" placeholder="Masukkan Jumlah Peringatan..." value="{{$product->alert_quantity}}">
            </div>
            <div class="mb-3">
                <label class="required fs-6 fw-bold mb-2">Kondisi</label>
                <select data-control="select2" data-placeholder="Pilih Kondisi" name="condition" class="form-select form-select-solid">
                    <option SELECTED DISABLED>Pilih Kondisi</option>
                    <option value="baru"{{$product->condition=="baru"?"selected":""}}>Baru</option>
                    <option value="bekas"{{$product->condition=="bekas"?"selected":""}}>Bekas</option>
                </select>
            </div>
        </div>
        <div>
            <div class="mb-3">
                <label for="unit" class="required form-label">Satuan</label>
                <div class="input-group">
                    <select data-control="select2" data-placeholder="Pilih Satuan" id="unit_id" name="product_unit_id" class="form-select form-select-solid">
                        <option value="" SELECTED DISABLED >Pilih Satuan</option>
                        @foreach ($satuan as $item)
                            <option value="{{$item->id}}" {{$item->id==$product->product_unit_id?"selected":""}}>{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <label for="unit" class="required form-label">Merek</label>
                <div class="input-group">
                    <select data-control="select2" data-placeholder="Pilih Merek" id="brand_id" name="product_brand_id" class="form-select form-select-solid">
                        <option value="" SELECTED DISABLED >Pilih Merek</option>
                        @foreach ($merek as $item)
                        <option value="{{$item->id}}"{{$item->id==$product->product_brand_id?"selected":""}}>{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <label for="unit" class="required form-label">Garansi</label>
                <select data-control="select2" data-placeholder="Pilih Garansi" id="warranty_id" name="product_warranty_id" class="form-select form-select-solid">
                    <option value="" SELECTED DISABLED >Pilih Merek</option>
                    @foreach ($garansi as $item)
                    <option value="{{$item->id}}"{{$item->id==$product->product_warranty_id?"selected":""}}>{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="unit" class="required form-label">Kategori</label>
                <div class="input-group">
                    <select data-control="select2" data-placeholder="Pilih Kategori" id="category_id" name="product_category_id" class="form-select form-select-solid">
                        <option value="" SELECTED DISABLED>Pilih Kategori</option>
                        @foreach ($kategori as $item)
                        <option value="{{$item->id}}"{{$item->id==$product->product_category_id?"selected":""}}>{{$item->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <label for="subcategory_id" class="form-label">Sub Kategori</label>
                <select data-control="select2" data-placeholder="Pilih Kategori terlebih dahulu" id="subcategory_id" name="product_subcategory_id" class="form-select form-select-solid"></select>
            </div>
            <div class="mb-3">
                <label for="unit" class="required form-label">Etalase</label>
                <div class="input-group">
                    <select data-control="select2" data-placeholder="Pilih Etalase" id="user_display_id" name="user_display_id" class="form-select form-select-solid">
                        <option value="" SELECTED DISABLED>Pilih Etalase</option>
                        @foreach ($etalase as $item)
                            <option value="{{$item->id}}"{{$item->id==$product->user_display_id?"selected":""}}>{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <label for="condition" class="required form-label">Deskripsi Produk</label>
                <textarea name="desc" cols="30" rows="5" class="form-control">{{$product->desc}}</textarea>
            </div>
            <div class="mb-3">
                <label for="condition" class="required form-label">Berat Produk</label>
                <input type="text" class="form-control" name="weight" placeholder="Masukkan Berat..." value="{{$product->weight}}">
            </div>
            @if($product->image)
            <div class="mb-3">
                <label for="condition" class="required form-label">Gambar Produk</label>
                <br>
                <div class="d-flex justify-content-center my-3">
                    <img style="width: 45%" src="{{$product->image}}" alt="">
                </div>
                <div class="mb-3">
                    <input type="file" class="form-control" name="photo" placeholder="Gambar Produk...." value="{{$product->photo}}">
                </div>
            </div>
            @else
            <div class="mb-3">
                <label for="condition" class="required form-label">Gambar Produk</label>
                <input type="file" class="form-control" name="photo" placeholder="Gambar Produk...." value="{{$product->photo}}">
            </div>
            @endif
            <div>
                <label for="condition" class="required form-label">Video Produk</label>
                <input type="text" class="form-control" name="video_url" placeholder="URL Video Produk...." value="{{$product->video_url}}">
            </div>
            <div class="min-w-150px mt-10 text-end">
                {{-- @if ($product->id)
                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.product.update',$product->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                @else
                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.product.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                @endif --}}
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button id="btn_submit" onclick="handle_upload_custom('#btn_submit','#form_input','{{route('web.product.update', $product->id)}}','PATCH', true, null, 'Save changes');" class="btn btn-primary">Save changes</button>
    </div>
</form>
<script>
    function handle_upload(tombol, form, url, method, idTmp = null, cb = null, text_button = null) {
        handle_upload(tombol, form, url, method, true, null, text_button)
    }
    number_only('price');
    ribuan('price');
    // @if($product->id)
    //     $("#category_id").val('{{$product->product_category_id}}').trigger('change');
    // @endif
    // // obj_select('unit_id','Pilih Satuan');
    // // obj_select('brand_id','Pilih Merek');
    // // obj_select('category_id','Pilih Kategori');
    // // obj_select("subcategory_id","Pilih Sub Kategori");
    // @if($product->product_category_id)
    //     $("#category_id").select2().select2("val", '{{$product->product_category_id}}');
    //     setTimeout(function(){ 
    //         $('#category_id').trigger('change');
    //         setTimeout(function(){ 
    //             $('#subcategory_id').val('{{$product->product_subcategory_id}}');
    //             $('#subcategory_id').trigger('change');
    //         }, 2000);
    //     }, 1000);
    // @endif
    // $("#category_id").change(function(){
    //     $.ajax({
    //         type: "POST",
    //         url: "{{route('web.product-category.get_list_sub')}}",
    //         data: {category : $("#category_id").val()},
    //         success: function(response){
    //             $("#subcategory_id").html(response);
    //         }
    //     });
    // });
    // number_only('price');
</script>
