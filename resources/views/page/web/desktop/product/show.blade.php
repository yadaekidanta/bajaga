<x-web-layout title="Selamat datang" keyword="Bajaga Online Store">
    <section id="content">

        <div class="content-wrap pb-10em">
            		<!-- Page Title
		============================================= -->
            <div class="container clearfix">


                <div class="single-product">
                    <div class="product">
                        <div class="row gutter-40">

                            <div class="col-md-6">

                                <!-- Product Single - Gallery
                                ============================================= -->
                                <div class="product-image">
                                    <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                        <div class="flexslider">
                                            <div class="slider-wrap" data-lightbox="gallery">
                                                <div class="slide" data-thumb="{{$product->image}}"><a href="{{$product->image}}" title="{{$product->name}}" data-lightbox="gallery-item"><img src="{{$product->image}}" alt="{{$product->name}}"></a></div>
                                                <div class="slide" data-thumb="{{$product->image}}"><a href="{{$product->image}}" title="{{$product->name}}" data-lightbox="gallery-item"><img src="{{$product->image}}" alt="{{$product->name}}"></a></div>
                                                <div class="slide" data-thumb="{{$product->image}}"><a href="{{$product->image}}" title="{{$product->name}}" data-lightbox="gallery-item"><img src="{{$product->image}}" alt="{{$product->name}}"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Product Single - Gallery End -->

                            </div>

                            <div class="col-md-6 product-desc">

                                <h1>{{$product->name}}</h1>

                                <div class="d-flex align-items-center justify-content-between">

                                    <!-- Product Single - Price
                                    ============================================= -->
                                    @if ($varian->count() > 0)
                                    <div class="product-price">Rp. {{number_format($varian_low->price)}}
                                        @if($varian_high->id != $varian_low->id)
                                        - {{number_format($varian_high->price)}}
                                        @endif
                                    </div><!-- Product Single - Price End -->
                                    @else
                                    <div class="product-price">Rp. {{number_format($product->price)}}</div><!-- Product Single - Price End -->
                                    @endif

                                    <!-- Product Single - Rating
                                    ============================================= -->
                                    <div class="d-flex align-items-center gap-2">
                                        <div class="product-rating d-flex flex-column">
                                            <input id="input-15" class="rating" value="{{$rating}}" data-size="sm" data-readonly="true">
                                        </div><!-- Product Single - Rating End -->
                                        @auth
                                            <form id="form_wishlist" class="m-0">
                                                <input type="number" class="d-none" value="{{$product->id}}" name="id_product">
                                                <!-- icon love whitelist -->
                                                <!-- no-fill -->
                                                <svg role="button" class="whitelist-ouline {{ $wishlist ? 'd-none' : '' }}" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 21.593c-5.63-5.539-11-10.297-11-14.402 0-3.791 3.068-5.191 5.281-5.191 1.312 0 4.151.501 5.719 4.457 1.59-3.968 4.464-4.447 5.726-4.447 2.54 0 5.274 1.621 5.274 5.181 0 4.069-5.136 8.625-11 14.402m5.726-20.583c-2.203 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248-3.183 0-6.281 2.187-6.281 6.191 0 4.661 5.571 9.429 12 15.809 6.43-6.38 12-11.148 12-15.809 0-4.011-3.095-6.181-6.274-6.181"/></svg>
                                                <!-- /no-fill -->
                                
                                                <!-- fill -->
                                                <svg role="button" class="whitelist-fill {{ $wishlist ? '' : 'd-none' }}" fill="red" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>
                                                <!-- fill -->
                                                <!-- /icon love whitelist -->
                                            </form>
                                        @endauth
                                        @guest
                                            <!-- no-fill -->
                                            <a href="#modal-auth" data-lightbox="inline">
                                                <svg role="button" class="" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 21.593c-5.63-5.539-11-10.297-11-14.402 0-3.791 3.068-5.191 5.281-5.191 1.312 0 4.151.501 5.719 4.457 1.59-3.968 4.464-4.447 5.726-4.447 2.54 0 5.274 1.621 5.274 5.181 0 4.069-5.136 8.625-11 14.402m5.726-20.583c-2.203 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248-3.183 0-6.281 2.187-6.281 6.191 0 4.661 5.571 9.429 12 15.809 6.43-6.38 12-11.148 12-15.809 0-4.011-3.095-6.181-6.274-6.181"/></svg>
                                            </a>
                                            <!-- /no-fill -->
                                        @endguest
                                    </div>

                                </div>

                                <div class="line"></div>

                                <!-- Product Single - Quantity & Cart Button
                                ============================================= -->
                                <form id="form_cart">
                                    <div class="cart mb-0 d-flex justify-content-between align-items-center">
                                        <input type="hidden" id="product_id" name="product" value="{{$product->id}}">
                                        <input type="hidden" id="store_id" name="store" value="{{$product->store_id}}">
                                        <div class="quantity clearfix">
                                            <input type="button" value="-" class="minus">
                                            <input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="qty" />
                                            <input type="button" value="+" class="plus">
                                        </div>
                                        @auth
                                            <button id="tombol_cart" onclick="add_cart('#tombol_cart','#form_cart','{{route('web.cart.add')}}','POST');" class="add-to-cart button m-0">Tambah ke Keranjang</button>   
                                        @endauth
                                        @guest
                                            <a href="#modal-auth" data-lightbox="inline" type="button" class="button m-0">Tambah ke Keranjang</a>   
                                        @endguest
                                    </div>
                                    <div class="line"></div>
                                    @if ($varian->count() > 0)
                                    <div class="row align-items-center">
                                        <div class="col-sm-12">
                                            <h5 class="fw-medium mb-3">Pilih Varian:</h5>
                                            <div role="group">
                                                @foreach ($varian as $item)
                                                    <input class="btn-check varian" type="radio" name="varian" id="varian-{{$item->id}}" autocomplete="off" data-stok="{{$item->stock}}" data-harga="{{$item->price}}" value="{{$item->id}}">
                                                    <label for="varian-{{$item->id}}" class="btn btn-sm btn-outline-secondary fw-normal ls0 nott">{{$item->name}}</label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="line"></div>
                                    @endif
                                </form><!-- Product Single - Quantity & Cart Button End -->

                                <!-- Product Single - Short Description
                                ============================================= -->
                                <p>{{$product->desc}}</p>
                                <p>
                                  @if(session('cart'))
                                    @foreach(session('cart') as $id => $details)
                                                <p>{{ $details['name'] }}</p>
                                                <span class="price text-info"> ${{ $details['price'] }} <br/> Quantity:{{ $details['quantity'] }}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                              </p>

                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                        <span class="text-muted">Kategori:</span><span class="text-dark fw-semibold">{{$product->product_category->title}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                        <span class="text-muted">Berat:</span><span class="text-dark fw-semibold">{{$product->weight}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                        <span class="text-muted">Stok:</span><span class="text-dark fw-semibold product_stock">{{$product->stock}} {{$product->product_unit->name}}</span>
                                    </li>
                                    <li class="list-group-item d-flex flex-column gap-3 justify-content-between px-0">
                                        <span class="text-muted">Kebijakan Pengembalian:</span><span class="text-dark fw-semibold">{{$product->product_warranty->description}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                        <span class="text-muted">SKU:</span><span class="text-dark fw-semibold">{{$product->sku}}</span>
                                    </li>
                                </ul>

                                <!-- Product Single - Share
                                ============================================= -->
                                <div class="si-share d-flex justify-content-between align-items-center mt-4">
                                    <span>Bagikan:</span>
                                    <div>
                                        <a href="#" class="social-icon si-borderless si-facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-pinterest">
                                            <i class="icon-pinterest"></i>
                                            <i class="icon-pinterest"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-gplus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-rss">
                                            <i class="icon-rss"></i>
                                            <i class="icon-rss"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-email3">
                                            <i class="icon-email3"></i>
                                            <i class="icon-email3"></i>
                                        </a>
                                    </div>
                                </div><!-- Product Single - Share End -->

                            </div>

                            <div class="w-100"></div>

                            <div class="col-12 mt-5">

                                <div class="tabs clearfix mb-0" id="tab-1">

                                    <ul class="tab-nav clearfix">
                                        <li><a href="#tabs-1"><i class="icon-align-justify2"></i><span class="d-none d-md-inline-block"> Deskripsi</span></a></li>
                                        <li><a href="#tabs-2"><i class="icon-info-sign"></i><span class="d-none d-md-inline-block"> Informasi Tambahan</span></a></li>
                                        <li><a href="#tabs-3"><i class="icon-star3"></i><span class="d-none d-md-inline-block"> Ulasan ({{$review_count}})</span></a></li>
                                        <li><a href="#tabs-4"><i class="icon-star3"></i><span class="d-none d-md-inline-block"> Diskusi</span></a></li>
                                    </ul>

                                    <div class="tab-container">

                                        <div class="tab-content clearfix" id="tabs-1">
                                            <p>{{$product->desc}}
                                        </div>
                                        <div class="tab-content clearfix" id="tabs-2">

                                            <table class="table table-striped table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td>Ukuran</td>
                                                        <td>{{$product->product_category->title}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Berat</td>
                                                        <td>{{$product->weight}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Stok</td>
                                                        <td>{{$product->stock}} {{$product->product_unit->name}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Garansi</td>
                                                        <td>{{$product->product_warranty->description}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                        <div class="tab-content clearfix" id="tabs-3">

                                            <div id="reviews" class="clearfix">

                                                <ol class="commentlist clearfix">
                                                    @foreach ($review as $item)
                                                    {{-- {{$item}} --}}
                                                    @if (!$item->to_id)
                                                        <li class="" id="li-comment-1">
                                                            <div class="d-flex gap-3 align-items-start">
                                                                <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                                                                    <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'.$item->user_review->avatar)}}" alt="image user">
                                                                </div>

                                                                <div class="w-100">
                                                                    <div>{{$item->user_review->name}}</div>
                                                                    <div class="text-sm text-secondary">{{$item->created_at->format('j F Y h:i:sa')}}</div>
                                                                    <div class="mt-3 p-3 card w-100">
                                                                        {{$item->review}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @else
                                                        <div class="w-100 d-flex gap-3 justify-content-end mt-3">
                                                            <li class="w-80% m-0" id="li-comment-1">
                                                                <div class="d-flex gap-3 align-items-start">
                                                                    <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                                                                        <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'.$product->product_store->photo)}}" alt="image user">
                                                                    </div>
    
                                                                    <div class="w-100">
                                                                        <div class="d-flex gap-3 align-item-center">
                                                                            <span class="font-bold text-primary">{{$product->product_store->name}}</span>
                                                                            <span class="font-bold text-sm badge bg-primary" style="margin-top: 3px">Penjual</span>
                                                                        </div>
                                                                        <div class="text-sm text-secondary">{{$item->created_at->format('j F Y h:i:sa')}}</div>
                                                                        <div class="mt-3 p-3 card bg-aliceblue w-100">
                                                                            {{$item->review}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </div>
                                                    @endif
                                                    @endforeach
                                                </ol>

                                                <!-- Modal Reviews
                                                ============================================= -->
                                                {{-- <a href="#" data-bs-toggle="modal" data-bs-target="#reviewFormModal" class="button button-3d m-0 float-end">Tambah Ulasan</a>

                                                <div class="modal fade" id="reviewFormModal" tabindex="-1" role="dialog" aria-labelledby="reviewFormModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="reviewFormModalLabel">Kirim Ulasan</h4>
                                                                <button type="button" class="btn-close btn-sm" data-bs-dismiss="modal" aria-hidden="true"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="row mb-0" id="template-reviewform" name="template-reviewform" action="#" method="post">

                                                                    <div class="col-6 mb-3">
                                                                        <label for="template-reviewform-name">Nama <small>*</small></label>
                                                                        <div class="input-group">
                                                                            <div class="input-group-text"><i class="icon-user"></i></div>
                                                                            <input type="text" id="template-reviewform-name" name="template-reviewform-name" value="" class="form-control required" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-6 mb-3">
                                                                        <label for="template-reviewform-email">Email <small>*</small></label>
                                                                        <div class="input-group">
                                                                            <div class="input-group-text">@</div>
                                                                            <input type="email" id="template-reviewform-email" name="template-reviewform-email" value="" class="required email form-control" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="w-100"></div>

                                                                    <div class="col-12 mb-3">
                                                                        <label for="template-reviewform-rating">Rating</label>
                                                                        <select id="template-reviewform-rating" name="template-reviewform-rating" class="form-select">
                                                                            <option value="">-- Pilih Rating --</option>
                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>
                                                                            <option value="5">5</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="w-100"></div>

                                                                    <div class="col-12 mb-3">
                                                                        <label for="template-reviewform-comment">Comment <small>*</small></label>
                                                                        <textarea class="required form-control" id="template-reviewform-comment" name="template-reviewform-comment" rows="6" cols="30"></textarea>
                                                                    </div>

                                                                    <div class="col-12">
                                                                        <button class="button button-3d m-0" type="submit" id="template-reviewform-submit" name="template-reviewform-submit" value="submit">Kirim Ulasan</button>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal --> --}}
                                                <!-- Modal Reviews End -->

                                            </div>

                                        </div>
                                        <div class="tab-content clearfix" id="tabs-4">
                                            <div class="p-3 border d-flex align-items-center justify-content-between">
                                                <div class="d-flex gap-3 align-items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd"><path d="M0 0h24v24H0z"/><path fill="#EAEAEA" d="M12 2v20C6.477 22 2 17.523 2 12S6.477 2 12 2z"/><path stroke="#999" stroke-linecap="round" stroke-linejoin="round" d="M19.144 19a9.97 9.97 0 0 1-7.142 3 9.97 9.97 0 0 1-7.141-3M4.86 5a9.97 9.97 0 0 1 7.142-3 9.97 9.97 0 0 1 7.142 3"/><path d="M7.135 14.44c.214.145.446.285.699.421.253.136.42.244.502.325.082.08.123.194.123.342 0 .105-.049.21-.145.315a.461.461 0 0 1-.352.157c-.112 0-.247-.036-.406-.108a4.576 4.576 0 0 1-.56-.315 16.62 16.62 0 0 1-.707-.482c-.476.242-1.06.364-1.751.364-.561 0-1.064-.09-1.508-.268a2.966 2.966 0 0 1-1.12-.773 3.33 3.33 0 0 1-.681-1.198A4.797 4.797 0 0 1 1 11.71c0-.555.08-1.063.239-1.525.159-.463.39-.857.69-1.181a2.99 2.99 0 0 1 1.103-.746A3.968 3.968 0 0 1 4.508 8c.748 0 1.39.152 1.926.455A2.95 2.95 0 0 1 7.652 9.75c.275.56.413 1.216.413 1.97 0 1.145-.31 2.052-.93 2.721zm-1.146-.797a2.23 2.23 0 0 0 .45-.826c.097-.318.145-.687.145-1.107 0-.528-.085-.986-.256-1.373-.17-.387-.414-.68-.73-.878a2.008 2.008 0 0 0-1.09-.298c-.292 0-.562.055-.81.165-.247.11-.46.27-.639.48-.179.21-.32.478-.423.804a3.635 3.635 0 0 0-.155 1.1c0 .83.193 1.468.58 1.916.388.448.876.672 1.467.672.242 0 .492-.051.748-.153a3.813 3.813 0 0 0-.579-.344 3.705 3.705 0 0 1-.474-.266.31.31 0 0 1-.128-.266.34.34 0 0 1 .123-.26.391.391 0 0 1 .27-.114c.299 0 .799.25 1.5.748zm4.365-3.337c0-.241.063-.46.189-.659.125-.198.31-.355.553-.472a1.97 1.97 0 0 1 .86-.175c.326 0 .608.06.847.183.238.121.416.28.534.478.118.197.177.401.177.614 0 .29-.096.543-.288.758-.191.215-.474.434-.847.657.13.153.25.296.362.431.11.135.218.26.322.377.104.117.198.216.284.3.039-.073.099-.21.18-.409.082-.2.159-.343.23-.431a.365.365 0 0 1 .301-.132.46.46 0 0 1 .323.13c.093.087.14.192.14.317 0 .114-.04.275-.12.484-.081.208-.198.44-.35.693.095.083.247.209.456.377.209.169.337.283.385.344a.382.382 0 0 1 .072.243.45.45 0 0 1-.146.34.461.461 0 0 1-.325.138.62.62 0 0 1-.353-.116 14.079 14.079 0 0 1-.727-.572 2.692 2.692 0 0 1-.523.39c-.18.1-.373.175-.579.224-.206.05-.436.074-.69.074-.324 0-.615-.047-.872-.142a1.862 1.862 0 0 1-.644-.382 1.576 1.576 0 0 1-.38-.535 1.516 1.516 0 0 1-.125-.596c0-.195.031-.373.093-.535.062-.162.15-.308.263-.44.112-.13.251-.254.416-.372.164-.118.355-.233.573-.344a4.224 4.224 0 0 1-.441-.651c-.1-.19-.15-.387-.15-.589zm1.1 1.862c-.264.153-.462.305-.593.458a.81.81 0 0 0-.196.545.84.84 0 0 0 .072.342 1.013 1.013 0 0 0 .53.516c.122.05.25.074.385.074.132 0 .26-.02.385-.062.124-.041.242-.1.354-.177a2.79 2.79 0 0 0 .346-.285 8.582 8.582 0 0 1-.622-.632 31.794 31.794 0 0 1-.661-.78zm.396-1.065c.265-.169.456-.31.574-.426a.563.563 0 0 0 .177-.418.528.528 0 0 0-.177-.404.63.63 0 0 0-.445-.163.636.636 0 0 0-.45.167.519.519 0 0 0-.18.393c0 .077.023.16.068.25.045.09.102.178.17.266.067.089.155.2.263.335zm9.474 3.889l-.37-.97H17.81l-.369.991c-.144.387-.267.648-.37.784-.101.135-.268.203-.5.203a.735.735 0 0 1-.523-.216.663.663 0 0 1-.227-.491c0-.106.018-.215.053-.327.035-.113.093-.27.174-.47l1.979-5.024.203-.52c.08-.202.164-.37.253-.503.09-.134.208-.242.354-.325.146-.083.326-.124.54-.124.219 0 .4.041.547.124.146.083.264.19.353.32.09.13.166.27.227.419.062.15.14.349.235.599l2.021 4.992c.159.38.238.656.238.828 0 .18-.075.344-.224.494a.737.737 0 0 1-.541.224.648.648 0 0 1-.538-.245 1.65 1.65 0 0 1-.193-.349 20.408 20.408 0 0 1-.177-.414zm-3.103-2.148h2.311l-1.166-3.192-1.145 3.192z" fill="#999"/></g></svg>
                                                    <span>Ada pertanyaan? <b>Diskusikan dengan penjual atau pengguna lain</b></span>
                                                </div>

                                                <a href="#form_comment" class="btn btn-primary">Tulis Pertanyaan</a>
                                            </div>

                                            <div id="list_result_discuss"></div>

                                            <div class="card mt-4">
                                                <form id="form_comment" class="m-0">
                                                    <div class="card-body">
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        <textarea name="comment" id="comment" rows="5" class="form-control" required placeholder="Apa yang ingin anda tanyakan mengenai produk ini?"></textarea>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="d-flex justify-content-end mt-3">
                                                            <button type="button" id="btn_submit_form_comment" onclick="handle_save('#btn_submit_form_comment', '#form_comment', '{{route('web.discuss')}}', 'POST', null, 'Kirim', null, true)" class="btn btn-primary px-5">Kirim</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="line"></div>
                @if($collection->count() > 0)
                <div class="w-100">

                    <h4>Produk Terkait</h4>

                    <div class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-md="2" data-items-lg="3" data-items-xl="4">
                        @foreach ($collection as $item)
                        <div class="oc-item">
                            <div style="height: 21em;" class="rounded mb-4 overflow-hidden px-0 d-flex flex-column shadow cursor-pointer card-product">
                                <a href="{{route('web.product.show', $item->slug)}}" class="position-relative d-flex align-items-center justify-content-between overflow-hidden w-100" style="height: 12em">
                                    <img class="w-100 h-100 object-fit-cover" src="{{$item->image}}" alt="">
                                </a>
                                <div class="px-3 py-2">
                                    <a href="{{route('web.product.show', $item->slug)}}" class="text-dark">{{Str::limit($item->name, 18)}}</a>
                                    <a href="{{route('web.product.show', $item->slug)}}" class="text-lg font-bold mt-2 d-block text-dark">Rp {{number_format($item->price)}}</a>
                                    <a href="{{route('web.store', $item->product_store->id)}}" class="d-flex mt-1 align-items-center gap-2">
                                        <img style="width: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                        <span class="text-secondary city-text">{{Str::limit($item->product_store->city->name, 18)}}</span>
                                        <span class="text-secondary store-text d-none">{{Str::limit($item->product_store->name, 18)}}</span>
                                    </a>
                                    <a href="{{route('web.product.show', $item->slug)}}" class="d-flex mt-1 text-dark gap-1">
                                        <i class="icon-star3 text-emas"></i>
                                        <span>{{$rating}}</span> | 
                                        <span>Terjual 10rb+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
    @section('custom_js')
    <script>
        // list_result_discuss
        load_custom("#list_result_discuss", "{{route('web.discuss.get', $product->id)}}", 1, () => {
            // do something
        }, null)
        $('input:radio[name="varian"]').change(function() {
            $("#product_id").val($(this).val());
            $(".product_stock").html(format_ribuan($(this).attr('data-stok')));
            $(".product-price").html('Rp. '+format_ribuan($(this).attr('data-harga')));
        });

        $(".whitelist-ouline").click(function(){
            let data = $("#form_wishlist").serialize();
            $.ajax({
                type: 'POST',
                url: "{{route('web.wishlist.store')}}",
                data: data,
                success: function(response){
                    $(".whitelist-ouline").addClass('d-none');
                    $(".whitelist-fill").removeClass('d-none');
                },
                error: response => {
                    if (response.status == 401) {
                        location.href = '{{route("web.auth.index")}}'
                    }
                }
            })
        })
        $(".whitelist-fill").click(function(){
            let data = $("#form_wishlist").serialize();
            $.ajax({
                type: 'DELETE',
                url: "{{route('web.wishlist.destroy', $product->id)}}",
                success: function(response){
                    $(".whitelist-fill").addClass('d-none');
                    $(".whitelist-ouline").removeClass('d-none');
                },
                error: response => {
                    if (response.status == 401) {
                        location.href = '{{route("web.auth.index")}}'
                    }
                }
            })
        })

        $(".card-product").on("mouseleave", function() {
                $(this).find(".store-text")[0].classList.add("d-none")
                $(this).find(".city-text")[0].classList.remove("d-none")
                // console.log($(this).find("..city-text"));
            })

            $(".card-product").on("mouseover", function() {
                $(this).find(".store-text")[0].classList.remove("d-none")
                $(this).find(".city-text")[0].classList.add("d-none")
            })

    </script>
    @endsection
</x-web-layout>
