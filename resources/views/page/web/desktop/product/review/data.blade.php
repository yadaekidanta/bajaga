@foreach ($collection as $key => $product)
    <div class="card p-3 mt-3">
        <div class="d-flex gap-3 align-items-start">
            <div class="w-3em h-3em border overflow-hidden rounded-circle">
                <img src="{{asset('storage/'.$product->photo)}}" class="w-100 h-100 object-fit-cover" alt="image product">
            </div>

            <div class="w-100">
                <h5 class="m-0 mb-3">{{$product->name}}</h5>

                {{-- list ulasan --}}
                @foreach ($product->product_reviews()->get() as $ulasan)
                    @if (!$ulasan->to_id)
                        <div class="rounded border {{!$ulasan->read_at ? 'bg-aliceblue' : ''}} w-100 p-3 position-relative">
                            {{-- icon star --}}
                            <div class="d-flex gap-2 align-items-center">
                                @for ($i = 0 ; $i < intval($ulasan->rate); $i++)
                                    <i class="icon-star3 text-emas font-icon-size-1"></i>
                                @endfor
                                @for ($i = intval($ulasan->rate); $i < 5; $i++)
                                    <i class="icon-star-empty text-secondary font-icon-size-1"></i>
                                @endfor
                            </div>
                            <span class="mt-3">
                                {{$ulasan->review}}
                            </span>

                            @if (!$ulasan->read_at)
                                <div class="d-flex justify-content-end mt-3">
                                    <span class="text-primary hover-opacity-65 font-bold" role="button" onclick="handle_open_modal('{{route('web.store.review.addModalReview', $ulasan->id)}}', '#modalListResult', '#contentListResult')">Balas</span>
                                </div>
                            @endif
                        </div>
                    @else
                    <div class="w-100 d-flex gap-3 justify-content-end mt-3">
                        <div class="w-3em h-3em border overflow-hidden rounded-circle">
                            <img src="{{asset('storage/'.Auth::user()->store->photo)}}" class="w-100 h-100 object-fit-cover" alt="image store">
                        </div>
                        
                        <div class="w-80%">
                            <h5 class="mb-3">{{Auth::user()->store->name}}</h5>
                            <div class="rounded border {{!$ulasan->read_at ? 'bg-aliceblue' : ''}} w-100 p-3 position-relative">
                                <span class="mt-3">
                                    {{$ulasan->review}}
                                </span>
                            </div>
                        </div>
                    </div>
                        
                    @endif
                @endforeach
                {{-- list ulasan --}}

            </div>
        </div>
    </div>   
@endforeach