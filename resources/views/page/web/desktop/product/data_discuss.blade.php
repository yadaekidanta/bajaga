<div class="card mt-4">
    <div class="card-header">
        <div class="d-flex gap-3">
            <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/')}}" alt="image user">
            </div>

            <div class="w-100">
                <div class="d-flex align-items-center">
                    <span class="font-bold">Mahrum</span>
                    <div class="text-sm text-mutted text-secondary mt-1">(123)</div>
                </div>
                <div>apaan ni</div>
            </div>
        </div>
    </div>
    <div class="card-body" style="padding-left: 5em !important">
        <div class="d-flex gap-3">
            <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/')}}" alt="image user">
            </div>

            <div class="w-100">
                <div class="d-flex align-items-center">
                    <span class="font-bold">Mahrum</span>
                    <span class="badge bg-primary mx-2">Penjual</span>
                    <div class="text-sm text-mutted text-secondary mt-1">(123)</div>
                </div>
                <div>Ini adalah barang!</div>
            </div>
        </div>

        @auth
            <div class="d-flex gap-3 mt-4">
                <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                    <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'.Auth::user()->avatar)}}" alt="image user">
                </div>
                
                <div class="w-100">
                    <form id="form_comment_reply_1">
                        <textarea name="comment" id="comment" rows="5" class="form-control" required placeholder="Isi komentar disini..."></textarea>
                        <div class="d-flex justify-content-end mt-3">
                            <button class="btn btn-primary px-5">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        @endauth
        @guest
            <div class="d-flex gap-3 mt-4">
                <span>Untuk dapat ikut diskusi anda harus <a href="{{route('web.auth.index')}}" class="text-primary">masuk dulu</a>!</span>
            </div>
        @endguest
    </div>
</div>