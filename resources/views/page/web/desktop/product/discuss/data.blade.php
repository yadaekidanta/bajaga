@if (count($collection) > 0)    
    @foreach ($collection as $key => $item)
        <div class="card mt-4">
            <div class="card-header">
                <div class="d-flex gap-3">
                    <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                        <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'.$item->user->avatar)}}" alt="image user">
                    </div>

                    <div class="w-100">
                        <div class="d-flex align-items-center">
                            <span class="font-bold">{{$item->user->name}}</span>
                            <div class="text-sm text-mutted text-secondary mt-1">({{$item->created_at->format('j F Y h:i:sa')}})</div>
                        </div>
                        <div>{{$item->comment}}</div>
                    </div>
                </div>
            </div>
            <div class="card-body {{!$item->read_at ? 'bg-aliceblue':''}}" style="padding-left: 5em !important">
                @php
                    $replies = \App\Models\ProductDiscuss::where("to_id", $item->id)->get();
                @endphp
                @foreach ($replies as $item_inner)
                    <div class="d-flex gap-3 mt-3">
                        @php
                            $status = false;
                            if ($item_inner->product->product_store->user->id == $item_inner->user_id) {
                                $status = true;
                            }
                        @endphp
                        <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                            @if ($status)
                                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'.$item_inner->product->product_store->photo)}}" alt="image user">
                            @else
                                <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'.$item_inner->user->avatar)}}" alt="image user">
                            @endif
                        </div>

                        <div class="w-100">
                            <div class="d-flex align-item_inners-center">
                                @if ($status)
                                    <span class="font-bold">{{$item_inner->product->product_store->name}}</span>
                                    <span class="badge bg-primary mx-2" style="margin-top: 3px">Penjual</span>
                                @else
                                    <span class="font-bold">{{$item_inner->user->name}}</span>
                                @endif
                                <div class="text-sm text-mutted text-secondary mt-1">({{$item_inner->created_at->format('j F Y h:i:sa')}})</div>
                            </div>
                            <div>{{$item_inner->comment}}</div>
                        </div>
                    </div>
                @endforeach

                @auth
                    <div class="d-flex gap-3 mt-4">
                        <div class="w-3rem h-3rem overflow-hidden rounded-circle border">
                            <img class="w-100 h-100 object-fit-cover" src="{{asset('storage/'.Auth::user()->avatar)}}" alt="image user">
                        </div>
                        
                        <div class="w-100">
                            <form id="form_comment_reply_{{$key}}">
                                <input type="hidden" name="product_id" value="{{$item->product_id}}">
                                <input type="hidden" name="to_id" value="{{$item->id}}">
                                <textarea name="comment" id="comment" rows="5" class="form-control" required placeholder="Isi komentar disini..."></textarea>
                                <div class="d-flex justify-content-end mt-3">
                                    <button class="btn btn-primary px-5" id="btn_submit_form_comment_{{$key}}" onclick="handle_save('#btn_submit_form_comment_{{$key}}', '#form_comment_reply_{{$key}}', '{{route('web.discuss')}}', 'POST', function(){
                                        load_custom('#list_result_discuss', '{{route('web.discuss.get', $item->product_id)}}', 1, () => {}, null)
                                    }, 'Kirim', null, true)">Kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endauth
                @guest
                    <div class="d-flex gap-3 mt-4">
                        <span>Untuk dapat ikut diskusi anda harus <a href="{{route('web.auth.index')}}" class="text-primary">masuk dulu</a>!</span>
                    </div>
                @endguest
            </div>
        </div>
    @endforeach
@endif
