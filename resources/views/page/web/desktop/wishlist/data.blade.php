@if (count($collections) > 0)
    {{-- {{dd($collections)}} --}}
    @foreach ($collections as $item)
        <div style="width: 12em; height: 20em;" class="rounded overflow-hidden px-0 d-flex flex-column shadow cursor-pointer card-product">
            <a href="{{route('web.product.show', $item->products->slug)}}" class="position-relative d-flex align-items-center justify-content-between overflow-hidden w-100" style="height: 12em">
                <img class="w-100 h-100 object-fit-cover" src="{{$item->products->image}}" alt="">
            </a>
            <div class="px-2 py-2">
                <a href="{{route('web.product.show', $item->products->slug)}}" class="text-dark">{{Str::limit($item->products->name, 18)}}</a>
                <a href="{{route('web.product.show', $item->products->slug)}}" class="text-lg font-bold mt-2 d-block text-dark">Rp {{number_format($item->products->price)}}</a>
                <a href="{{route('web.store', $item->products->product_store->id)}}" class="d-flex mt-1 align-items-center gap-3">
                    <img style="width: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                    <span class="text-secondary city-text">{{Str::limit($item->products->product_store->city->name, 14)}}</span>
                    <span class="text-secondary store-text d-none">{{Str::limit($item->products->product_store->name, 14)}}</span>
                </a>
                <form id="form_cart">
                    <input type="hidden" id="product_id" name="product" value="{{$item->products->id}}" class="m-0 d-none">
                    <input type="hidden" id="store_id" name="store" value="{{$item->products->product_store->id}}">
                    <input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="qty d-none" />
                </form>
                <button onclick="add_cart('#tombol_cart','#form_cart','{{route('web.cart.add')}}','POST');" id="tombol_cart" type="button" class="mt-3 w-100 gap-1 btn btn-sm btn-outline-primary">Tambah ke keranjang</button>
            </div>
        </div>
    @endforeach
    
    <script>
        $(".card-product").on("mouseleave", function() {
                    $(this).find(".store-text")[0].classList.add("d-none")
                    $(this).find(".city-text")[0].classList.remove("d-none")
                    // console.log($(this).find("..city-text"));
                })

                $(".card-product").on("mouseover", function() {
                    $(this).find(".store-text")[0].classList.remove("d-none")
                    $(this).find(".city-text")[0].classList.add("d-none")
                })
    </script>
@endif