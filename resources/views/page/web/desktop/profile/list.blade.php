<div class="container clearfix">
    <h3>Profile Anda</h3>
        <form id="profile-form" name="profile-form" class="row mb-0" action="#" method="post">
            <div class="col-md-6 form-grup">
                <img src="{{ asset('storage/'.$user->avatar) }}" alt="">
            </div>
            <div class="col-md-12 form-group">
                <label for="profile-form-nik">NIK : </label>
                    <input type="text" id="profile-form-name" name="profile-form-name" value="{{ $user->nik }}" class="sm-form-control" readonly>
            </div>
            <div class="col-md-12 form-group">
                <label for="profile-form-name">Nama : </label>
                <input type="text" id="profile-form-name" name="profile-form-name" value="{{ $user->name }}" class="sm-form-control" readonly>
            </div>
            <div class="col-12 form-group">
                <label for="profile-form-email">Alamat Email:</label>
                <input type="email" id="profile-form-email" name="profile-form-email" value="{{ $user->email }}" class="sm-form-control" readonly>
            </div>
            <div class="col-12 form-group">
                <label for="profile-form-phone">No HP:</label>
                <input type="text" id="profile-form-phone" name="profile-form-phone" value="{{ $user->phone }}" class="sm-form-control" readonly>
            </div>
            <div class="col-12 form-group">
                <label for="profile-form-name">Nama Pengguna : </label>
                <input type="text" id="profile-form-username" name="profile-form-username" value="{{ $user->username }}" class="sm-form-control" readonly>
            </div>
        </form>
        <a href="javascript:;" onclick="load_input('{{route('web.profile.edit',$user->id)}}');" class="button button-3d float-end">Edit</a>
            </div>
        <br><br>
        <h4>Alamat Anda</h4>
        @foreach($address as $item)
            <label class="custom_radio">
                <input type="radio" name="demo" class="card-input-element d-none" id="demo1" {{$item->is_use == 1 ? 'checked' : ''}} onclick="handle_check('{{ route('web.profile.updateAddress',$item->id)}}')">
                <div class="card card-header d-flex flex-row justify-content-between align-items-center">
                    {{$item->desc}}
                </div>
                <div class="card card-body d-flex flex-row justify-content-between align-items-center">
                {{$item->address}} <br> {{$item->subdistrict->name}}, {{$item->city->name}} <br> {{$item->province->name}} {{$item->postcode}}
                </div>
            </label>
        @endforeach
    </div>
</section>
