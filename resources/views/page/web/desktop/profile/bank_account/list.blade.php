@foreach ($collections as $item)
    <div class="card p-4 mt-3 shadow-sm">
        <div class="d-flex align-items-center w-100 justify-content-between">
            <div class="d-flex align-items-center gap-3 w-100">
                {{-- img bank --}}
                <div>
                    <img src="{{asset('storage/'.$item->banks->thumbnail)}}" class="w-8em" alt="">
                </div>
                {{-- img bank --}}

                <div>
                    <div class="text-sm font-thin">{{$item->banks->name}}</div>
                    <div class="font-thin">{{$item->account_number}}</div>
                    <div class="font-thin"><span class="text-sm font-thin">a.n </span> <span class="font-bold">Sdr {{$item->account_holder_name}}</span></div>
                </div>
            </div>
            <div>
                <button class="btn btn-sm btn-outline-secondary" onclick="handle_confirm_costum('Anda yakin?', 'Yes', 'No', 'POST', '{{route('web.profile.bank_account.destroy', $item->id)}}', true, 'Perubahan ini tidak bisa dikemblaikan!')">Hapus</button>
            </div>
        </div>
    </div>
@endforeach