<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Tambah Rekening Bank</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="p-4 modal-body">
    <form id="form_input">
        <div data-bs-toggle="offcanvas" data-bs-target="#canvasListBank" aria-controls="canvasListBank" role="button">
            <label for="province" class="form-label m-0">Pilih Bank</label>
            <select autofocus required class="select2 form-control w-100 cursor-pointer" id="bank_id" name="bank_id">
                @foreach($master as $bank)
                    <option value="{{$bank->id}}">{{$bank->name}}</option>
                @endforeach
                <input type="hidden" class="bank" name="bank" value="{{$bank->name}}">
            </select>
        </div>

        <div>
            <div class="d-flex gap-2 flex-column mt-2">
                <div class="form-group m-0 mt-3">
                    <label for="account_holder_name" class="form-label m-0">Nama pemilik</label>
                    <input type="text" id="account_holder_name" autocomplete="off"  name="account_holder_name" class="form-control w-100 py-2">
                </div>
                <div class="form-group m-0 mt-2">
                    <label for="account_number" class="form-label m-0">Nomor Rekening</label>
                    <input type="number" name="account_number" id="account_number" class="form-control w-100 py-2">
                </div>
                <div class="form-group m-0 mt-2">
                    <label for="branch_name" class="form-label m-0">Nama cabang</label>
                    <input type="text" id="branch_name" name="branch_name" class="form-control w-100 py-2">
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button type="button" id="btn_submit" onclick="handle_save_custom('#btn_submit', '#form_input', '{{route('web.profile.bank_account.store')}}', 'POST')" class="btn btn-primary">Save changes</button>
</div>

@if (count($master))
    <script>
        
        function handle_save_custom(tombol, form, url, method){
            handle_save(tombol, form, url, method, function() {
                $(tombol).html('Save changes')
                load_custom('#list_result_bank', '{{route('web.profile.bank_account.getDataBankUser')}}', 1)
            })
        }
    </script>
@endif
