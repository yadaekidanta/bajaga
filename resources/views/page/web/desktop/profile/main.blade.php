<x-web-layout title="Profile" keyword="Bajaga Online Store">
    <div id="content_list">
            <br>
            <div class="px-5 m-0" id="kt_content_container">
                <div class="row clearfix gap-4">
                    {{-- sidebar --}}
                    <div  class="w-16em">
                        @include('page.web.desktop.components.sidebar')
                    </div>
                    {{-- sidebar --}}

                    {{-- content --}}
                    <div class="col-sm-9 col-md-9 min-h-29em card p-2 pb-5 shadow-sm">
                        <ul class="nav nav-tabs d-flex justify-content-around" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                              <button class="nav-link active" id="biodata-tab" data-bs-toggle="tab" data-bs-target="#biodata" type="button" role="tab" aria-controls="biodata" aria-selected="true">Biodata Diri</button>
                            </li>
                            <li class="nav-item" role="presentation">
                              <button class="nav-link" id="alamat-tab" data-bs-toggle="tab" data-bs-target="#alamat" type="button" role="tab" aria-controls="alamat" aria-selected="false">Daftar Alamat</button>
                            </li>
                            <li class="nav-item" role="presentation">
                              <button class="nav-link" id="rekening-tab" data-bs-toggle="tab" data-bs-target="#rekening" type="button" role="tab" aria-controls="rekening" aria-selected="false">Rekening Bank</button>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            {{-- biodata diri section --}}
                            <div class="tab-pane fade show active" id="biodata" role="tabpanel" aria-labelledby="biodata-tab">
                                <div class="d-flex">
                                    {{-- left --}}
                                    <div class="p-3 gap-3" style="width: 21em">
                                        <div>
                                            <div class="p-3 card shadow-sm">
                                                <div class="h-16em overflow-hidden rounded">
                                                    <img class="w-100 h-100 object-fit-cover" id="foto-preview" src="{{asset($user->avatar != null ? 'storage/'.$user->avatar : 'img/avatars/admin.png')}}" alt="">
                                                </div>
                                                
                                                <form class="d-none" id="form_upload_foto">
                                                    <input type="file" name="avatar" id="avatar" class="d-none" onchange="handle_upload_custom('#btn-upload-foto', '#form_upload_foto', '{{route('web.profile.update.photo', $user->id)}}', 'PATCH', true, 'Pilih Foto')">
                                                </form>
        
                                                <label class="btn btn-outline-primary w-100 mt-3" for="avatar" id="btn-upload-foto">Pilih Foto</label>
        
                                                <p class="text-xs text-secondary mt-3 mb-2">Besar file: maksimum 10.000.000 bytes (10 Megabytes). Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
                                            </div>
    
                                            {{-- ubah sandi --}}
                                            <div>
                                                <button type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-outline-primary d-flex align-items-center position-relative w-100 mt-3" id="btn_ubah_kata_sandi">
                                                    <span class="text-center w-100">Ubah Kata Sandi</span>    
                                                </button>
                                            </div>
                                            {{-- ubah sandi --}}
                                        </div>
                                    </div>
                                    {{-- left --}}

                                    {{-- right --}}
                                    <div class="p-3 d-flex gap-4 justify-content-between">
                                        <div id="list_result_biodata">
                                            <div>
                                                <span class="text-md font-bold">Ubah Biodata Diri</span>
                                                
                                                {{-- name --}}
                                                <div class="d-flex align-items-center mt-3">
                                                    <span class="w-10em">Nama</span>
                                                    <span>{{$user->name}}</span>
                                                </div>
                                                {{-- name --}}
        
                                                {{-- nik --}}
                                                <div class="d-flex align-items-center mt-3">
                                                    <span class="w-10em">NIK</span>
                                                    <span>{{$user->nik}}</span>
                                                </div>
                                                {{-- nik --}}
                                            </div>
    
                                            <div class="mt-5">
                                                <span class="text-md font-bold">Ubah Kontak</span>
                                                
                                                {{-- name --}}
                                                <div class="d-flex align-items-center mt-3">
                                                    <span class="w-10em">Email</span>
                                                    <span>{{$user->email}}</span>
                                                </div>
                                                {{-- name --}}

                                                {{-- username --}}
                                                <div class="d-flex align-items-center mt-3">
                                                    <span class="w-10em">Username</span>
                                                    <span>{{$user->username}}</span>
                                                </div>
                                                {{-- username --}}
        
                                                {{-- name --}}
                                                <div class="d-flex align-items-center mt-3">
                                                    <span class="w-10em">Nomor HP</span>
                                                    <span>{{$user->phone}}</span>
                                                </div>
                                                {{-- name --}}
                                            </div>
                                        </div>

                                        <div>
                                            <button class="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#editProfileModal">Edit Profile</button>
                                        </div>
                                    </div>
                                    {{-- right --}}

                                </div>
                            </div>
                            {{-- biodata diri section --}}

                            {{-- alamat section --}}
                            <div class="tab-pane fade" id="alamat" role="tabpanel" aria-labelledby="alamat-tab">
                                <div class="p-3">
                                    <div class="d-flex justify-content-between">
                                        {{-- serach --}}
                                        <div class="input-group w-30%">
                                            <input type="text" class="form-control" placeholder="Cari alamat atau nama penerima" aria-label="Cari alamat atau nama penerima" aria-describedby="basic-addon2">
                                            <span class="input-group-text" id="basic-addon2">
                                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M0 0h24v24H0z"/><path d="M10.503 15.188a4.692 4.692 0 0 1-4.69-4.686 4.692 4.692 0 0 1 4.684-4.69 4.693 4.693 0 0 1 4.69 4.686 4.693 4.693 0 0 1-4.684 4.69m10.092 3.42l-3.206-3.203a1.378 1.378 0 0 0-.908-.393A7.46 7.46 0 0 0 18 10.495 7.5 7.5 0 1 0 10.504 18a7.46 7.46 0 0 0 4.516-1.524c.019.33.14.655.394.908l3.206 3.201a1.396 1.396 0 0 0 1.976 0 1.396 1.396 0 0 0-.001-1.977" fill="#9FA6B0" fill-rule="nonzero"/></g></svg>
                                            </span>
                                        </div>
                                        {{-- serach --}}

                                        {{-- btn tambah --}}
                                        <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#modalAddress">Tambah Alamat Baru</button>
                                        {{-- btn tambah --}}
                                    </div>
                                </div>
                                
                                <div id="list_result_address" class="px-3 mt-4">
                                </div>
                            </div>
                            {{-- alamat section --}}

                            {{-- rekening section --}}
                            <div class="tab-pane fade" id="rekening" role="tabpanel" aria-labelledby="rekening-tab">
                                <div class="p-3">
                                    <div class="d-flex justify-content-between align-items-end">
                                        <div>
                                            <h3 class="font-bold mb-3">Daftar Rekening Bank</h3>
                                            <ul class="list-unstyled m-0">
                                                <li>Akun Rekening Bank yang aktif maksimal berjumlah 3 (tiga) buah.</li>
                                            </ul>
                                        </div>

                                        <button class="btn btn-primary" onclick="handle_open_modal('{{route('web.profile.bank_account.add_modal')}}', '#modalListResult', '#contentListResult')">Tambah Rekening</button>
                                    </div>
                                    
                                    
                                    {{-- card bank --}}
                                    <div id="list_result_bank" class="mt-3">
                                        @foreach ($bank as $item)
                                            <div class="card p-4 mt-3 shadow-sm">
                                                <div class="d-flex align-items-center w-100 justify-content-between">
                                                    <div class="d-flex align-items-center gap-3 w-100">
                                                        {{-- img bank --}}
                                                        <div>
                                                            <img src="{{asset('storage/'.$item->banks->thumbnail)}}" class="w-8em" alt="">
                                                        </div>
                                                        {{-- img bank --}}
    
                                                        <div>
                                                            <div class="text-sm font-thin">{{$item->banks->name}}</div>
                                                            <div class="font-thin">{{$item->account_number}}</div>
                                                            <div class="font-thin"><span class="text-sm font-thin">a.n </span> <span class="font-bold">Sdr {{$item->account_holder_name}}</span></div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <button class="btn btn-sm btn-outline-secondary" onclick="handle_confirm_costum('Anda yakin?', 'Yes', 'No', 'POST', '{{route('web.profile.bank_account.destroy', $item->id)}}', true, 'Perubahan ini tidak bisa dikemblaikan!')">Hapus</button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    {{-- card bank --}}
                                </div>
                            </div>
                            {{-- rekening section --}}
                          </div>
                    </div>
                    {{-- content --}}

                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ubah sandi -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ubah Sandi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="modal-body">
                <form id="form_input_sandi">
                    <div class="mb-3">
                        <label for="password" class="form-label">New Password</label>
                        <input type="text" name="password" class="form-control" id="password">
                    </div>
                    <div class="mb-3">
                        <label for="password_confirmation" class="form-label">Confirmation Password</label>
                        <input type="text" name="password_confirmation" class="form-control" id="password_confirmation">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="btn_form_modal" onclick="handle_save('#btn_form_modal', '#form_input_sandi', '{{route('web.profile.account_security.password', $user->id)}}', 'POST', true, 'Save changes')" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </div>
    </div>
    {{-- modal ubah sandi --}}

    <!-- Modal Edit profile -->
    <div class="modal fade" id="editProfileModal" tabindex="-1" aria-labelledby="editProfileModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="editProfileModalLabel">Edit Profile</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="modal-body">
                <form id="form_input_profile">
                    <div class="mb-3">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{$user->name}}">
                    </div>
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" name="username" class="form-control" id="username" value="{{$user->username}}">
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" value="{{$user->email}}" name="email" class="form-control" id="email">
                    </div>
                    <div class="mb-3">
                        <label for="nik" class="form-label">Nik</label>
                        <input type="text" name="nik" class="form-control" id="nik" value="{{$user->nik}}">
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" name="phone" class="form-control" id="phone" value="{{$user->phone}}">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="btn_form_profile" onclick="handle_save_custom('#btn_form_profile', '#form_input_profile', '{{route('web.profile.update', $user->id)}}', 'PATCH', 'Save changes')" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </div>
    </div>
    {{-- modal Edit profile --}}

    <!-- Modal Alamat -->
    <div class="modal fade" id="modalAddress" tabindex="-1" aria-labelledby="modalAddressLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modalAddressLabel">Ubah Sandi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="modal-body">
                <form id="form_input_address">
                    <div class="mb-3">
                        <label for="address" class="form-label">Address</label>
                        <input type="text" name="address" class="form-control" id="address">
                    </div>
                    <div class="mb-3">
                        <label for="deskripsi" class="form-label">Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsi" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="postcode" class="form-label">Kode pos</label>
                        <input type="text" name="postcode" class="form-control" id="postcode">
                    </div>
                    <div class="mb-3">
                        <label for="province" class="m-0">Provinsi</label>
                        <select required class="select2 form-control" id="province" style="width: 100%;" name="province">
                            @foreach($provinsi as $p)
                                <option value="{{$p->id}}">{{$p->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="city" class="m-0">Kota / Kabupaten</label>
                        <select required class="select2 form-control" id="city" style="width: 100%;" name="city">
                        </select>
                    </div>
    
                    <div class="mb-3">
                        <label for="subdistrict" class="m-0">Kecamatan</label>
                        <select required class="select2 form-control" id="subdistrict" style="width: 100%;" name="subdistrict">
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="btn_form_modal_address" onclick="handle_save_custom('#btn_form_modal_address', '#form_input_address', '{{route('web.user-address.store')}}', 'POST', 'Save changes')" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </div>
    </div>
    {{-- modal alamat --}}

    {{-- <div id="content_input"></div> --}}
    @section('custom_js')
    <script type="text/javascript">

        // load_list(1);
        load_custom('#list_result_address', '{{route('web.user-address.getDataAddress')}}', 1)
        // load_custom('#list_result_biodata', '{{route('web.profile.getDataBiodata')}}', 1)

        function handle_upload_custom(tombol, form, url, method, idTmp = null, text_button = null){
            // alert()
            
            let formData = new FormData($(form)[0])
            let file = $("#avatar")[0].files[0]
            formData.append('formData', file);
            formData.append('_method', method);
            $(tombol).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',
                resetForm: true,
                dataType: 'json',
                beforeSend: function() {
                    $(tombol).html(`<div class="spinner-border spinner-border-sm text-primary spinner" role="status"></div>`)
                },
                error: function(response) {
                    console.log(response);
                    if (text_button) {
                        Swal.fire({ text: response.responseJSON.message, icon: "error", confirmButtonText: "Ok, Mengerti!"});
                        $(tombol).html(text_button);
                    }
                },
                success: function(response) {
                    if (response.alert == "success") {
                        Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                        if (idTmp == null) {
                            $(form)[0].reset();
                        }
                        if (text_button) {
                            $(tombol).html(text_button);
                        }
                        setTimeout(function() {
                            if(response.redirect == "input"){
                                load_input(response.route);
                            }
                            if(!response.redirect){
                                $(tombol).prop("disabled", false);
                                $(tombol).removeAttr("data-kt-indicator");
                                main_content('content_list');
                                load_list(1);
                            }
                        }, 2000);
                    } else {
                        Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                        setTimeout(function() {
                            $(tombol).prop("disabled", false);
                            $(tombol).removeAttr("data-kt-indicator");
                        }, 2000);
                    }
                    // $("#btn-paid").html(text_tombol)

                    // change the picture

                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#foto-preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL($("#avatar")[0].files[0]);
                },
            });
            // return false;
        }

        function handle_save_custom(tombol, form, url, method, text_button){
            $(tombol).prop("disabled", true)
            $(tombol).html(loading_animate("white"))
            // console.log(loading_animate("primary"));
            handle_save(tombol, form, url, method, function() {
                $(tombol).html(text_button)
                try {
                    let name_value = $(form).find("#name")[0].value
                    $("#name_user").html(limit_text(name_value, 17))
                } catch (error) {
                    
                }
                // console.log(name_value);
                $(tombol).prop("disabled", false)
                load_custom('#list_result_biodata', '{{route('web.profile.getDataBiodata')}}', 1)
                load_custom('#list_result_address', '{{route('web.user-address.getDataAddress')}}', 1)
            }, text_button, true)
        }

        function handle_confirm_costum(title, confirm_title, deny_title, method, route, idtmp = null, text) {
            handle_confirm(title, confirm_title, deny_title, method, route, true, function(params) {
                load_custom('#list_result_bank', '{{route('web.profile.bank_account.getDataBankUser')}}', 1)
            }, text)
        }

        $("#province").change(function(){
            $.ajax({
                type: "POST",
                url: "{{route('web.regional.city')}}",
                data: {province : $("#province").val()},
                success: function(response){
                    $("#city").html(response);
                }
            });
        });

        
        $("#city").change(function(){
            $.ajax({
                type: "POST",
                url: "{{route('web.regional.subdistrict')}}",
                data: {city : $("#city").val()},
                success: function(response){
                    $("#subdistrict").html(response);
                }
            });
        });

        function destroy(url){
            Swal.fire({ 
                title: 'Anda Yakin?',
                text: "Anda tidak akan dapat mengembalikan perubahan ini!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Iya, Hapus!',
                denyButtonText: 'Batal!'
            }).then(result => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        success: function (response) {
                            Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                            load_custom('#list_result_address', '{{route('web.user-address.getDataAddress')}}', 1)
                        },

                        error: function (response) {
                            // console.log(response);
                            Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                        }
                    })
                }
            })
            
        }
    </script>
    @endsection
</x-web-layout>