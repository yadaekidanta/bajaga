<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Update Address</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="p-4 modal-body">
    <form id="form_input_address_update">
        <div class="mb-3">
            <label for="address" class="form-label">Address</label>
            <input type="text" name="address" class="form-control" id="address_update" value="{{$userAddress->address}}">
        </div>
        <div class="mb-3">
            <label for="deskripsi" class="form-label">Deskripsi</label>
            <textarea name="deskripsi" id="deskripsi_update" rows="5" class="form-control">{{$userAddress->desc}}</textarea>
        </div>
        <div class="mb-3">
            <label for="postcode" class="form-label">Kode pos</label>
            <input type="text" value="{{$userAddress->postcode}}" name="postcode" class="form-control" id="postcode_update">
        </div>
        <div class="mb-3">
            <label for="province" class="m-0">Provinsi</label>
            <select required class="select2 form-control" id="province_update" style="width: 100%;" name="province">
                @foreach($provinsi as $p)
                    <option value="{{$p->id}}">{{$p->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="city" class="m-0">Kota / Kabupaten</label>
            <select required class="select2 form-control" id="city_update" style="width: 100%;" name="city">
            </select>
        </div>
    
        <div class="mb-3">
            <label for="subdistrict" class="m-0">Kecamatan</label>
            <select required class="select2 form-control" id="subdistrict_update" style="width: 100%;" name="subdistrict">
            </select>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button type="button" id="btn_form_modal" onclick="handle_save_custom('#btn_form_modal', '#form_input_address_update', '{{route('web.user-address.update', $userAddress->id)}}', 'PATCH')" class="btn btn-primary">Save changes</button>
</div>

@if($userAddress->province_id)
<script>
    $('#province_update').val('{{$userAddress->province_id}}');
    setTimeout(function(){ 
        $('#province_update').trigger('change');
        setTimeout(function(){ 
            $('#city_update').val('{{$userAddress->city_id}}');
            $('#city_update').trigger('change');
            setTimeout(function(){ 
                $('#subdistrict_update').val('{{$userAddress->subdistrict_id}}');
                $('#subdistrict_update').trigger('change');
            }, 1200);
            setTimeout(function(){ 
                $('#postcode').val('{{$userAddress->postcode}}');
            }, 1200);
        }, 1200);
    }, 500);

    $("#province_update").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.regional.city')}}",
            data: {province : $("#province_update").val()},
            success: function(response){
                $("#city_update").html(response);
            }
        });
    });
    $("#city_update").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.regional.subdistrict')}}",
            data: {city : $("#city_update").val()},
            success: function(response){
                $("#subdistrict_update").html(response);
            }
        });
    });
    
    function handle_save_custom(tombol, form, url, method){
        handle_save(tombol, form, url, method, function() {
            load_custom('#list_result_address', '{{route('web.user-address.getDataAddress')}}', 1)
        })
    }
</script>
@endif