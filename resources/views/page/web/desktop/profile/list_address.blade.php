@if (count($address) > 0)
    
    @foreach ($address as $item)
        <div class="w-100 card border mt-3 shadow-sm {{$item->is_use == 1 ? 'bg-aliceblue' : ''}} border-primary py-3 px-4 position-relative">
            <div class="pointer-card bg-primary"></div>
            <span class="font-bold">{{$user->name}}</span>
            <span class="d-block mt-2">({{$user->phone}})</span>
            <span class="d-block">{{$item->province->name}}, {{$item->city->name}}, {{$item->subdistrict->name}}, {{$item->postcode}}</span>
            <span class="d-block">{{Str::limit($item->desc, 20)}}</span>

            <div class="mt-3 ">
                <span class="text-primary cursor-pointer hover-opacity-65 font-bold me-3 btn-ubah-alamat" onclick="handle_open_modal('{{route('web.user-address.edit_modal', $item->id)}}', '#modalListResult', '#contentListResult')">Ubah Alamat</span>
                @if ($item->is_use == 0)
                
                    <span class="text-primary cursor-pointer hover-opacity-65 font-bold me-3" onclick="is_use('{{route('web.user-address.is_use', $item->id)}}')">
                        Jadikan alamat utama
                    </span>
                    
                    <span class="text-primary hover-opacity-65 cursor-pointer font-bold" onclick="destroy('{{route('web.user-address.destroy', $item->id)}}')">Hapus</span>

                @endif
            </div>
        </div>
    @endforeach

    <script>
        function is_use(url) {
            $.ajax({
                type: 'PATCH',
                url: url,
                dataType: 'json',
                processData: false,
                success: function(response){
                    // console.log(response);
                    if(response.alert == 'success'){
                        Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    }
                    load_custom('#list_result_address', '{{route('web.user-address.getDataAddress')}}', 1)
                },
                error: function(response) {
                    Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                }
            })
        }
    </script>

@endif
