<div>
    <span class="text-md font-bold">Ubah Biodata Diri</span>
    
    {{-- name --}}
    <div class="d-flex align-items-center mt-3">
        <span class="w-10em">Nama</span>
        <span>{{$user->name}}</span>
    </div>
    {{-- name --}}

    {{-- nik --}}
    <div class="d-flex align-items-center mt-3">
        <span class="w-10em">Nik</span>
        <span>{{$user->nik}}</span>
    </div>
    {{-- nik --}}
</div>

<div class="mt-5">
    <span class="text-md font-bold">Ubah Kontak</span>
    
    {{-- name --}}
    <div class="d-flex align-items-center mt-3">
        <span class="w-10em">Email</span>
        <span>{{$user->email}}</span>
    </div>
    {{-- name --}}

    {{-- username --}}
    <div class="d-flex align-items-center mt-3">
        <span class="w-10em">Username</span>
        <span>{{$user->username}}</span>
    </div>
    {{-- username --}}

    {{-- name --}}
    <div class="d-flex align-items-center mt-3">
        <span class="w-10em">Nomor HP</span>
        <span>{{$user->phone}}</span>
    </div>
    {{-- name --}}
</div>