<x-web-layout title="Selamat datang" keyword="Bajaga Online Store">
    <section id="content">       
            <div class="container clearfix">

                <div class="row gutter-40 col-mb-80">
                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent col-lg-9 order-lg-last">

                        <!-- Shop
                        ============================================= -->
                        <div class="row grid-container gutter-20" data-layout="fitRows">
                            <div class="row gap-3 mt-5 ms-2 grid-6 mb-3" id="list_result">
                            </div>
                            <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                                <div class="spinner-grow text-secondary" role="status">
                                </div>
                            </div>
                        </div><!-- #shop end -->

                    </div><!-- .postcontent end -->

                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar col-lg-3 pt-5">
                        <div class="sidebar-widgets-wrap">

                            <div class="widget widget-filter-links">
                                <h4 class="px-2">Urutkan</h4>
                                <ul class="custom-filter ps-2" data-container="#shop" data-active-class="active-filter">
                                    <li class="p-2 rounded bg-aliceblue">
                                        <a class="hover-opacity-65 w-100 cursor-pointer" onclick="filter_product('semua', this)" data-filter="semua">Tampilkan Semua
                                        </a>
                                    </li>
                                    <li class="p-2 rounded">
                                        <a class="hover-opacity-65 w-100 cursor-pointer" onclick="filter_product('min', this)" data-filter="semua">Harga Terendah
                                        </a>
                                    </li>
                                    <li class="p-2 rounded">
                                        <a class="hover-opacity-65 w-100 cursor-pointer" onclick="filter_product('max', this)" data-filter="semua">Harga Tertinggi
                                        </a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                    </div><!-- .sidebar end -->
                </div>
                <form id="content_filter" class="d-none">
                    <input type="hidden" id="filter_input" name="filter">
                </form>

            </div>
    </section>

    @section('custom_js')
        <script>
            load_list(1)
            $(".card-product").on("mouseleave", function() {
                $(this).find(".store-text")[0].classList.add("d-none")
                $(this).find(".city-text")[0].classList.remove("d-none")
                // console.log($(this).find("..city-text"));
            })

            $(".card-product").on("mouseover", function() {
                $(this).find(".store-text")[0].classList.remove("d-none")
                $(this).find(".city-text")[0].classList.add("d-none")
            })

            //function for Scroll Event
            var pages = 1;
            $(document).ready(function(){
                let options = {
                    root: null,
                    rootMargin: '300px',
                    threshold: 0.5
                }

                const observer = new IntersectionObserver(handleIntersect, options)
                observer.observe(document.querySelector("footer"))
            })

            function handleIntersect(entries){
                if (entries[0].isIntersecting) {
                    ++pages
                    load_more(pages);
                }
            }

            function filter_product(id, el){
                if (id == "semua") {
                    pages = 1
                    $("#filter_input").val("")
                }else{
                    $("#filter_input").val(id)
                }
                // console.log($(el).parent());
                $(".custom-filter > *").removeClass("bg-aliceblue")
                $(el).parent()[0].classList.add("bg-aliceblue")
                state_filter = true
                // console.log($("#filter_input_category").val());
                load_list(1)
            }
        </script>
    @endsection
</x-web-layout>