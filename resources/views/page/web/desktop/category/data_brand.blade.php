@if (count($products) > 0)
    @foreach ($products as $item)
        <div style="width: 12em; height: 20em;" class="rounded overflow-hidden px-0 d-flex flex-column shadow cursor-pointer card-product">
            <a href="{{route('web.product.show', $item->slug)}}" class="position-relative d-flex align-items-center justify-content-between overflow-hidden w-100" style="height: 12em">
                <img class="w-100 h-100 object-fit-cover" src="{{$item->image}}" alt="">
            </a>
            <div class="px-2 py-2">
                <a href="{{route('web.product.show', $item->slug)}}" class="text-dark">{{Str::limit($item->name, 18)}}</a>
                <a href="{{route('web.product.show', $item->slug)}}" class="text-lg font-bold mt-2 d-block text-dark">Rp {{number_format($item->price)}}</a>
                <a href="{{route('web.store', $item->product_store->id)}}" class="d-flex mt-1 align-items-center gap-3">
                    <img style="width: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                    <span class="text-secondary city-text">{{Str::limit($item->product_store->city->name, 14)}}</span>
                    <span class="text-secondary store-text d-none">{{Str::limit($item->product_store->name, 14)}}</span>
                </a>
                <a href="{{route('web.product.show', $item->slug)}}" class="d-flex mt-1 text-dark gap-1">
                    <i class="icon-star3 text-emas"></i>
                    <span>{{$item->rating}}</span> | 
                    <span>Terjual 10rb+</span>
                </a>
            </div>
        </div>
    @endforeach
    
    <script>
        $(".card-product").on("mouseleave", function() {
                    $(this).find(".store-text")[0].classList.add("d-none")
                    $(this).find(".city-text")[0].classList.remove("d-none")
                    // console.log($(this).find("..city-text"));
                })

                $(".card-product").on("mouseover", function() {
                    $(this).find(".store-text")[0].classList.remove("d-none")
                    $(this).find(".city-text")[0].classList.add("d-none")
                })
    </script>
@endif