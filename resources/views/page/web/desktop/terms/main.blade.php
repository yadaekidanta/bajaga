<x-web-layout title="Syarat & Ketentuan" keyword="Bajaga Online Store">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row clearfix">
                    <div class="col-md-3">
                        <div class="list-group">
                            <a href="{{route('web.about')}}" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Tentang Bajaga</div></a>
                            <a href="{{route('web.privacy')}}" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Kebijakan Privasi</div></a>
                            <a href="{{route('web.terms')}}" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Syarat & Ketentuan</div></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <h1>Syarat & Ketentuan</h1>
                        <p>
                            Selamat datang di www.bajaga.com.
                        </p>
                        <p>
                            Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh PT. Bajaga terkait penggunaan situs www.bajaga.com. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.
                        </p>
                        <p>
                            Dengan mendaftar dan/atau menggunakan situs www.bajaga.com, maka pengguna dianggap telah membaca, mengerti, memahami dan menyetujui semua isi dalam Syarat & ketentuan. Syarat & ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan PT.Tokopedia. Jika pengguna tidak menyetujui salah satu, pesebagian, atau seluruh isi Syarat & ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di www.bajaga.com.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>