<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<x-web-layout title="Selamat datang" keyword="Bajaga Online Store">
    <section id="content">
        <div class="content-wrap">
            <div class="container mb-5">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        @foreach ($banner as $key => $item)
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$key}}" class="{{ $key == 0 ? 'active': ''}}" aria-current="true" aria-label="Slide {{$key}}"></button>
                        @endforeach
                    </div>
                    <div class="carousel-inner">
                        @foreach ($banner as $key => $item)
                        <a href="{{$item->url}}" class="carousel-item rounded-13px overflow-hidden {{ $key == 0 ? 'active': ''}}">
                          <img src="{{asset('storage/'.$item->photo)}}" class="d-block w-100" alt="...">
                        </a>
                        @endforeach
                    </div>
                  </div>
            </div>

            <div class="container clearfix mt-2">
                <!-- Shop Categories ============================================= -->
                <div class="fancy-title title-border title-center mb-4">
                    <h4>Kategori Pilihan</h4>
                </div>
                @php
                    // dd($category[0]->image);
                    $categories = $category->chunk(6);
                @endphp
                <div id="carouselExampleControls" class="carousel position-relative slide h-29em card p-3" data-interval="false">
                    <div class="carousel-inner">
                        @foreach ($categories as $key => $category)
                        <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
                            <div class="row shop-categories clearfix">
                                @foreach ($category as $item)
                                {{-- {{dd($item)}} --}}
                                <div class="col-lg-4">
                                    <a href="{{route('web.category.show',[$item['slug']])}}" style="background: url('{{$item->image}}') no-repeat right center; background-size: cover;">
                                        <div class="vertical-middle dark center">
                                            <div class="heading-block m-0 border-0">
                                                <h3 class="nott fw-semibold ls0">{{$item['title']}}</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            </div>      
                        </div>
                        @endforeach
                    </div>
                    <div class="position-absolute h-100 d-flex align-items-center left-0 top-0">
                        <button class="carousel-control-prev position-relative bg-secondary p-2 rounded-circle" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev" style="height: 50px; width: 50px;">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                    </div>
                    <div class="position-absolute h-100 d-flex align-items-center right-0 me-3 top-0">
                        <button class="carousel-control-next position-relative bg-secondary p-2 rounded-circle" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next" style="height: 50px; width: 50px;">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>


                @php
                    $subcategories = $subCategory->chunk(5);
                @endphp
                
                <div class="fancy-title title-border title-center mb-4 mt-5">
                    <h4>Cari kategori apa?</h4>
                </div>
                <div class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-md="2" data-items-lg="6" data-items-xl="6">
                        @foreach ($subcategories as $key => $subcategory)
                            {{-- <div class="d-flex gap-3"> --}}
                                @foreach ($subcategory as $item)
                                <div class="oc-item">
                                    <a href="{{route('web.category.show_subcategory', $item->slug)}}" class="px-2 py-1 rounded border hover-bg-aliceblue card-subcategory">
                                        <img class="w-2em" src="{{asset('storage/'. $item->photo)}}" alt="">
                                        <div class="">{{$item->title}}</div>
                                    </a>
                                </div>
                                @endforeach
                            {{-- </div> --}}
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- New Arrival Section ============================================= -->
            

            <div class="clear"></div>

            <div class="container clearfix mb-5">
                <div class="fancy-title title-border topmargin-sm mb-4 title-center">
                    <h4>Lagi trending, nih</h4>
                </div>
                <div class="row gap-3 ms-2 grid-6">
                    @foreach ($trending as $item)
                        @php
                            $rating = \App\Models\ProductReview::where('product_id','=',$item->product_id)->avg('rate');
                            $sold = \App\Models\SaleDetail::where('product_id','=',$item->product_id)->get()->count();
                            // dd($rating);
                        @endphp
                        <div style="width: 13em; height: 20em;" class="rounded overflow-hidden px-0 d-flex flex-column shadow cursor-pointer card-product">
                            <a href="{{route('web.product.show', $item->product->slug)}}" class="position-relative d-flex align-items-center justify-content-between overflow-hidden w-100" style="height: 12em">
                                <img class="w-100 h-100 object-fit-cover" src="{{$item->product->image}}" alt="">
                            </a>
                            <div class="px-2 py-2">
                                <a href="{{route('web.product.show', $item->product->slug)}}" class="text-dark">{{Str::limit($item->product->name, 18)}}</a>
                                <a href="{{route('web.product.show', $item->product->slug)}}" class="text-lg font-bold mt-2 d-block text-dark">Rp {{number_format($item->product->price)}}</a>
                                <a href="{{route('web.store', $item->product->product_store->id)}}" class="d-flex mt-1 align-items-center gap-3">
                                    <img style="width: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                    <span class="text-secondary city-text">{{Str::limit($item->product->product_store->city->name, 18)}}</span>
                                    <span class="text-secondary store-text d-none">{{Str::limit($item->product->product_store->name, 18)}}</span>
                                </a>
                                <a href="{{route('web.product.show', $item->product->slug)}}" class="d-flex mt-1 text-dark gap-1">
                                    <i class="icon-star3 text-emas"></i>
                                    <span>{{$rating ? $rating : 0}}</span> | 
                                    <span>Terjual {{$sold > 1000 ? '10rb+' : number_format($sold)}}</span>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="fancy-title title-border title-center mb-4">
                            <h4>Brand <span class="text-danger">pilihan</span></h4>
                        </div>

                        {{-- brand list --}}
                        <div class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-md="2" data-items-lg="7" data-items-xl="7">
                            @foreach ($brand as $item)
                            <div class="oc-item">
                                <a href="{{route('web.category.show_brand', $item->slug)}}" class="px-2 hover-bg-aliceblue py-1 rounded border card-brandd card overflow-hidden">
                                    <img src="{{asset('storage/'. $item->photo)}}" alt="">
                                    <span class="mt-2 text-center">{{Str::limit($item->name, 15)}}</span>
                                </a>
                            </div>
                            @endforeach
                        </div>
                        {{-- <div class="d-flex align-items-center gap-3 ">
                            @foreach ($brand as $item)
                                <a href="{{route('web.category.show_brand', $item->slug)}}" class="px-2 hover-bg-aliceblue py-1 rounded border card-brandd card overflow-hidden">
                                    <img src="{{asset('storage/'. $item->photo)}}" alt="">
                                    <span class="mt-2 text-center">{{$item->name}}</span>
                                </a>
                            @endforeach
                            <a href="#" class="px-2 py-1 hover-bg-aliceblue cursor-pointer d-flex align-items-center justify-content-center  rounded border card-brandd card overflow-hidden">
                                <span class="text-center">Lihat semua</span>
                            </a>
                        </div> --}}
                        {{-- /brand list --}}
                    </div>
                </div>
            </div>

            {{-- product pilihan untukmu --}}
            <div class="container clearfix mb-5">
                <div class="fancy-title title-border topmargin-sm mb-4 title-center">
                    <h4>Product pilihan untukmu!</h4>
                </div>
                <div class="row gap-3 ms-2 grid-6 mb-3" id="list_result">
                </div>
                <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                    <div class="spinner-grow text-secondary" role="status">
                    </div>
                </div>
            </div>
            {{-- /product pilihan untukmu --}}
        </div>
    </section>

    @section('custom_js')
        <script>
            load_list(1)
            $(".card-product").on("mouseleave", function() {
                $(this).find(".store-text")[0].classList.add("d-none")
                $(this).find(".city-text")[0].classList.remove("d-none")
                // console.log($(this).find("..city-text"));
            })

            $(".card-product").on("mouseover", function() {
                $(this).find(".store-text")[0].classList.remove("d-none")
                $(this).find(".city-text")[0].classList.add("d-none")
            })

            //function for Scroll Event
            var pages = 1;
            $(document).ready(function(){
                let options = {
                    root: null,
                    rootMargin: '50px',
                    threshold: 0.5
                }

                const observer = new IntersectionObserver(handleIntersect, options)
                observer.observe(document.querySelector(".ajax-load"))
            })

            function handleIntersect(entries){
                if (entries[0].isIntersecting) {
                    ++pages
                    load_more(pages);
                }
            }

            
        </script>
    @endsection
</x-web-layout>