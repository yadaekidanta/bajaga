<x-web-layout title="Buka Toko" keyword="Bajaga Online Store">
    <div class="post d-flex flex-column-fluid pt-4" id="kt_post">
        <div id="kt_content_container" class="container-xxl mb-12em">
            <div class="d-flex align-items-center">
                <div class="col-6 d-flex justify-content-center">
                    <img src="{{asset('img/other/open-store.png')}}" class="w-26em" alt="image open store">
                </div>
                <div class="col-5">
                    <div class="card">
                        <div class="card-header border-0 pt-3">
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-center">
                                    Halo {{Auth::user()->name}}, Masukan Detail Toko Anda!
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <form id="form_input">
                                <div class="row">
                                    <div class="col-lg-12 mb-3 mt-4">
                                        <input type="hidden" name="users_id" id="users_id" value="{{Auth::user()->id}}">
                                        <label class="required cursor-pointer hover-opacity-65 fw-bold mb-2">Nama Toko</label>
                                        <input type="text" class="form-control" name="name" placeholder="Masukkan nama toko...">
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="province">Provinsi</label>
                                        <select name="province_id" id="province" class="form-control cursor-pointer hover-opacity-65" required></select>                
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="required cursor-pointer hover-opacity-65 fw-bold mb-2">Kota</label>
                                        <select class="form-control cursor-pointer hover-opacity-65"  id="city" name="city_id"></select>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="required cursor-pointer hover-opacity-65 fw-bold mb-2">Kecamatan</label>
                                        <select class="form-control cursor-pointer hover-opacity-65" id="subdistrict" name="subdistrict_id"></select>
                                    </div>
                                    <div class="col-lg-12 mt-3">
                                        <label class="required cursor-pointer hover-opacity-65 fw-bold mb-2">Alamat Toko</label>
                                        <textarea name="address" class="form-control hover-opacity-65" id="address" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="w-100 mt-4 text-end">
                                        <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.open-store.store')}}','POST', null, 'Simpan');" class="btn w-100 btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('custom_js')
        <script type="text/javascript">
        $("#tombol_simpan").prop("disabled", true)
        get_province();
        function get_province(){
            $.post('{{route('web.regional.province')}}', {}, function(result) {
                $("#province").html(result);
            }, "html");
        }
        $("#province").change(function(){
            $.post('{{route('web.regional.city')}}', {province : $("#province").val()}, function(result) {
                $("#city").html(result);
            }, "html");
        });
        $("#city").change(function(){
            $.post('{{route('web.regional.subdistrict')}}', {city : $("#city").val()}, function(result) {
                $("#subdistrict").html(result);
                $("#tombol_simpan").prop("disabled", false)
            }, "html");
        });
        </script>
    @endsection
</x-web-layout>