{{-- card list product --}}
@if(count($item->sale_detail()->get()) > 1)
    <div class="card-transaction mt-5">
        <div class="card p-3 min-h-13em position-relative">
            
            {{-- info status --}}
            <div class="position-absolute -top-15px">
                <span class="badge bg-primary">{{$item->st}}</span>
            </div>
            {{-- info status --}}

            <div class="d-flex h-100 justify-content-between">

                <div class="d-flex col-lg-4 justify-content-between flex-column">
                    <div class="d-flex gap-4">
                        {{-- image product --}}
                        <div class="w-4em h-4em border rounded overflow-hidden d-flex align-item-center justify-content-center">
                            <img class="object-fit-cover w-100 h-100" src="{{asset('storage/'.$item->sale_detail()->get()[0]->product->photo)}}" alt="image product">
                        </div>
                        {{-- image product --}}


                        <div>
                            <div class="w-100 collapse-custom hover-opacity-65 cursor-pointer" id="collapse" data-collapse-target="#collapse-{{$item->id}}">
                                <div class="font-bold text-sm text-dark">{{count($item->sale_detail()->get())}} Item</div>
                                <div class="d-flex align-items-center gap-4">
                                    <span class="text-sm text-secondary">Detail Produk</span>
                    
                                    <svg id="arrow" class="rotate-90" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                        <defs>
                                            <path id="a" d="M8.761 8.172L12.59 12 8.76 15.827c-.508.509-.521 1.326-.032 1.815.49.49 1.306.475 1.814-.033l4.719-4.718c.49-.49.49-1.292 0-1.782L10.543 6.39c-.508-.508-1.324-.521-1.814-.032-.49.49-.476 1.306.032 1.814"/>
                                        </defs>
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M0 24h24V0H0z"/>
                                            <use fill="#6C727C" transform="matrix(0 1 1 0 0 0)" xlink:href="#a"/>
                                        </g>
                                    </svg>
                                </div>
                            </div>

                            
                            {{-- detail more item --}}
                            <div id="collapse-{{$item->id}}" class="collapse collapse-{{$item->id}}">
                            @foreach ($item->sale_detail()->get() as $key => $product)
                                @if ($key < 2)
                                    <div class="mt-2">
                                        <div class="d-flex justify-content-between">
                                            <span class="font-bold text-sm">{{Str::limit($product->product->name, 15)}}</span>
                                            <span class="text-sm">Rp{{number_format($product->price - $product->disc_price)}}</span>
                                        </div>
                                        <span class="text-secondary d-block text-sm font-thin">{{$product->qty}} barang</span>
                                    </div>
                                @endif

                                @if ($key < 1)
                                <script>
                                    $(".collapse-custom").click(function(){
                                                // alert()
                                                let target = $(this).attr("data-collapse-target")
                                                console.log(target);
                                                $(target).toggleClass("show")
                                                // console.log($(target));
                                                $(target).prev().find("#arrow")[0].classList.toggle("rotate-90")
                                            })
                                </script>
                                @endif
                            @endforeach
                            </div>
                            {{-- detail more item --}}
                            <div>
                                <span class="text-primary font-bold">{{$product->product->name}}</span>
                                <span class="text-secondary d-block text-sm font-thin">{{$product->qty}} barang</span>
                                <span onclick="handle_open_modal('{{route('web.store.history.transaction.getDetailModal', $item->id)}}', '#modalListResultLarge', '#contentListResultLarge')" class="text-sm cursor-pointer mt-3 text-primary hover-opacity-65">Lihat detail pesanan</span>
                            </div>
                        </div>

                    </div>
                    @if($item->payment_st == "Lunas" && $item->st == "Dikirim")
                        <button type="button" onclick="handle_open_modal('{{route('web.store.history.transaction.getDetailModal', $item->id)}}', '#modalListResultLarge', '#contentListResultLarge')" class="btn btn-outline-primary w-100 mt-4 btn-sm">Lihat Detail</button>
                    @elseif($item->payment_st == "Belum lunas" && $item->customer_id == Auth::user()->id)
                        <button onclick="checkout('{{$item->code}}', this)" class="btn btn-primary w-100  mt-4 btn-sm">Bayar Sekarang</button>
                    @elseif($item->st == "Diterima" && $item->customer_id == Auth::user()->id)
                        <button class="btn btn-primary w-100  mt-4 btn-sm" type="button" onclick="handle_open_modal('{{route('web.transaction.checkout.getModalUlasan', $item->id)}}', '#modalListResultLarge', '#contentListResultLarge')">Beri Ulasan</button>
                    @else
                        @if($item->customer_id == Auth::user()->id)
                            <a href="{{route('web.chat')}}?id={{$item->store->user->id}}"  class="btn btn-outline-primary w-100  mt-4 btn-sm">Chat</a>    
                        @endif
                    @endif
                </div>

                {{-- address --}}
                <div class="d-flex col-lg-3 gap-3">
                    <div class="d-flex gap-2">
                        <div>
                            <div class="text-sm font-bold">Alamat</div>
                            <div class="text-sm">{{$item->customer->name}}</div>
                            <div class="text-sm">{{$item->customer_phone}}</div>
                            <div class="text-sm text-break">{{$item->shipping_detail}}</div>
                        </div>
                    </div>
                </div>
                {{-- address --}}

                {{-- kurir --}}
                <div class="d-flex col-lg-2 gap-3">
                    <div class="d-flex gap-2">
                        <div>
                            <div class="text-sm font-bold">Kurir</div>
                            <div class="text-sm text-break">{{Str::limit($item->courier, 10)}}</div>
                            <div class="text-sm text-secondary">(Rp{{number_format($item->shipping_price)}})</div>
                        </div>
                    </div>
                </div>
                {{-- kurir --}}

                {{-- total harga --}}
                <div class="d-flex gap-3">
                    <div class="d-flex gap-2">
                        <div>
                            <div class="text-sm font-bold">Total Harga</div>
                            <h4 class="font-bold text-danger">Rp {{number_format($item->grand_total)}}</h4>
                        </div>
                    </div>
                </div>
                {{-- total harga --}}

                
            </div>
        </div>
    </div>
@else
    @foreach ($item->sale_detail()->get() as $product)
        <div class="card-transaction mt-5">
            <div class="card p-3 h-13em position-relative">
                
                {{-- info status --}}
                <div class="position-absolute -top-15px">
                    <span class="badge bg-primary">{{$item->st}}</span>
                </div>
                {{-- info status --}}

                <div class="d-flex h-100 justify-content-between">

                    <div class="d-flex col-lg-4 justify-content-between flex-column">
                        <div class="d-flex gap-4">
                            {{-- image product --}}
                            <div class="w-4em h-4em border rounded overflow-hidden d-flex align-item-center justify-content-center">
                                <img class="object-fit-cover w-100 h-100" src="{{asset('storage/'.$product->product->photo)}}" alt="image product">
                            </div>
                            {{-- image product --}}

                            <div>
                                <span class="text-primary font-bold">{{$product->product->name}}</span>
                                <span class="text-secondary d-block text-sm font-thin">{{$product->qty}} barang</span>
                                <span onclick="handle_open_modal('{{route('web.store.history.transaction.getDetailModal', $item->id)}}', '#modalListResultLarge', '#contentListResultLarge')" class="text-sm cursor-pointer mt-3 text-primary hover-opacity-65">Lihat detail pesanan</span>
                            </div>
                        </div>

                        @if($item->st == 'Selesai' && $item->customer_id == Auth::user()->id)
                            <a href="{{route('web.product.show', $product->product->slug)}}" class="btn w-100 btn-primary mt-4 btn-sm">Beli Lagi</a>
                        @elseif($item->payment_st == "Lunas" && $item->st == "Dikirim")
                            <button type="button" onclick="handle_open_modal('{{route('web.store.history.transaction.getDetailModal', $item->id)}}', '#modalListResultLarge', '#contentListResultLarge')" class="btn btn-outline-primary w-100 mt-4 btn-sm">Lihat Detail</button>
                        @elseif($item->payment_st == "Belum lunas" && $item->customer_id == Auth::user()->id)
                            <button onclick="checkout('{{$item->code}}', this)" class="btn btn-primary w-100  mt-4 btn-sm">Bayar Sekarang</button>
                        @elseif($item->st == "Diterima" && $item->customer_id == Auth::user()->id)
                            <button class="btn btn-primary w-100  mt-4 btn-sm" type="button" onclick="handle_open_modal('{{route('web.transaction.checkout.getModalUlasan', $item->id)}}', '#modalListResultLarge', '#contentListResultLarge')">Beri Ulasan</button>
                        @else
                            @if($item->customer_id == Auth::user()->id)
                                <a href="{{route('web.chat')}}?id={{$item->store->user->id}}"  class="btn btn-outline-primary w-100  mt-4 btn-sm">Chat</a>    
                            @endif
                        @endif
                    </div>

                    {{-- address --}}
                    <div class="d-flex col-lg-3 gap-3">
                        <div class="d-flex gap-2">
                            <div>
                                <div class="text-sm font-bold">Alamat</div>
                                <div class="text-sm">{{$item->customer->name}}</div>
                                <div class="text-sm">{{$item->customer_phone}}</div>
                                <div class="text-sm text-break">{{$item->shipping_detail}}</div>
                            </div>
                        </div>
                    </div>
                    {{-- address --}}

                    {{-- kurir --}}
                    <div class="d-flex col-lg-2 gap-3">
                        <div class="d-flex gap-2">
                            <div>
                                <div class="text-sm font-bold">Kurir</div>
                                <div class="text-sm text-break">{{Str::limit($item->courier, 10)}}</div>
                                <div class="text-sm text-secondary">(Rp{{number_format($item->shipping_price)}})</div>
                            </div>
                        </div>
                    </div>
                    {{-- kurir --}}

                    {{-- total harga --}}
                    <div class="d-flex gap-3">
                        <div class="d-flex gap-2">
                            <div>
                                <div class="text-sm font-bold">Total Harga</div>
                                <h4 class="font-bold text-danger">Rp {{number_format($item->grand_total)}}</h4>
                            </div>
                        </div>
                    </div>
                    {{-- total harga --}}

                    
                </div>
            </div>
        </div>
    @endforeach
@endif
{{-- card list product --}}