<x-web-layout title="" keyword="Bajaga Store">
    <div class="container-custom d-flex justify-content-center pb-10em flex-column align-items-center h-screen px-3 bg-white">
        <div class="mb-5 d-flex gap-5 align-items-center justify-content-between flex-column">
            <div class="">
                <img src="{{asset('img/other/snap-error.png')}}" class="w-10em ms-3" alt="image card done">
            </div>
            <div class="d-flex flex-column align-items-center" id="info">
                <span class="text-center font-xl font-bold mb-2">Oops! snap</span>
                <span class="d-block text-center">Ada yang salah.</span>
                <span class="d-block text-center">Transaksi anda gagal, temukan produk <a href="{{route('web.catalog.products')}}">lainnya</a></span>
            </div>
        </div>
    </div>

    @section("custom_js")
    <script>
    </script>
    @endsection
</x-web-layout>