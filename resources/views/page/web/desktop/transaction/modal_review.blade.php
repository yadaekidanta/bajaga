<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Kirim Ulasanmu</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<form id="form_input">
    <div class="modal-body">
        <!-- icon bintang -->
        <div class="d-flex w-100 d-flex align-items-center justify-content-center flex-column gap-2 mt-2">
            <div class="w-100 d-flex align-items-center justify-content-center gap-3">
                <i class="icon-star-empty cursor-pointer hover-opacity-65 icons font-icon-size-4" data-value="1"></i>
                <i class="icon-star-empty cursor-pointer hover-opacity-65 icons font-icon-size-4" data-value="2"></i>
                <i class="icon-star-empty cursor-pointer hover-opacity-65 icons font-icon-size-4" data-value="3"></i>
                <i class="icon-star-empty cursor-pointer hover-opacity-65 icons font-icon-size-4" data-value="4"></i>
                <i class="icon-star-empty cursor-pointer hover-opacity-65 icons font-icon-size-4" data-value="5"></i>
            </div>
            <span class="mt-1">Berikan penilainmu</span>
        </div>
        <hr class="m-0">
        <!-- icon bintang -->
        <form id="form_input" class="m-0 mt-2">
            <textarea type="text" name="review" class="form-control" rows="5"></textarea>
            <input type="hidden" name="id_sale" value="{{$sale->id}}" class="form-control">
            <input type="hidden" name="rate" id="rate_input" class="form-control">
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button id="btn-kirim" onclick="handle_save_local('#btn-kirim', '#form_input', '{{route('web.review')}}', 'POST')" type="button" class="btn btn-primary">Kirim</button>
    </div>
</form>

<script>
    let rating_point = 0;
    function fillTheStar(num){
        // lakukan reset
        for (let index = 0; index < 5; index++) {
            let check = $(".icons")[index].classList.contains("icon-star-empty")
            if (!check) {
                $(".icons")[index].classList.add("icon-star-empty")
                $(".icons")[index].classList.remove("icon-star3")
                $(".icons")[index].classList.remove("text-emas")
            }
        }
        // lakukan lakukan isi star
        // alert(num)
        for (let index = 0; index < num; index++) {
            // console.log($(".icons")[index]);
            $(".icons")[index].classList.add("icon-star3")
            $(".icons")[index].classList.add("text-emas")
            $(".icons")[index].classList.remove("icon-star-empty")
        }
    }

    $(".icons").click(function(){
        let rate = parseInt($(this).attr("data-value"))
        // rating_point = rate
        fillTheStar(rate)
        $("#rate_input").val(rate)
        // console.log($(".icons"));
    })
    function handle_save_local(tombol, form, url, method){
        handle_save(tombol, form, url, method, function(){
            location.reload()
        })
    }
</script>