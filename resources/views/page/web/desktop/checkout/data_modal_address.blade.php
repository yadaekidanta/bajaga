<div class="modal-header">
    <h5 class="modal-title" id="modalTitleLabel">Tambah Etalase</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<form id="form_input">
    <div class="modal-body">
        <form action="">
            <div class="position-relative w-100">
                <input type="text" class="form-control w-100 px-4" placeholder="Cari Alamat">

                <!-- icon search -->
                <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                    <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd">
                            <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                            <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                        </g>
                    </svg>
                </div>
                <!-- icon search -->

            </div>
        </form>

        <!-- card list address -->
        <div class="d-flex flex-column gap-3">
            @foreach($address as $a)
                <div class="card cursor-pointer {{ $a->is_use == 1 ? 'bg-aliceblue' : ''}}" role="button" onclick="is_use('{{route('web.user-address.is_use', $a->id)}}')">
                    <div class="card-body row align-items-center">
                        <div class="col-10">
                            <div>
                                <span class="font-sm">Osyi</span>
                                <span class="font-sm text-primary"></span>
                            </div>
    
                            <div class="d-flex flex-column text-box mt-1" data-maxlength="25">
                                <span class="font-sm text-secondary">{{$a->users->phone}}</span>
                                <span class="font-sm text-secondary">{{$a->users->address}}</span>
                                <span class="font-sm text-secondary text-capitalize">{{$a->subdistrict->name}}, {{$a->city->name}}, {{$a->province->name}}, ID {{$a->postcode}}</span> 
                            </div>
                        </div>
    
                        <div class="col h-100">
                            <div class="d-flex align-items-center h-100">
                                <input type="radio" name="alamat" class="form-custom-radio is_use" {{$a->is_use == 1 ? 'checked': ''}}>
                                <input type="hidden" value="{{$a->id}}">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center">
                            <!-- TODO: suruh backend untuk ambil data address user sesuai dari usernya, dan kasi paggination ganti parameter ke dua menggunakan id dari alamatnya  -->
                            <a href="{{route('web.user-address.edit', $user->id)}}" class="text-black font-sm">Ubah Alamat</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- /card list address -->
    </div>
</form>