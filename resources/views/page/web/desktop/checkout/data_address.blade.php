@foreach($address as $a)
    @if($a->is_use == 1)
        <div id="data_address">
            <hr class="m-0 mb-2">
            <input type="hidden" id="dest_subdistrict_is_use" value="{{$address_use->subdistrict_id}}">
            <input type="hidden" id="fullname" value="{{$address_use->users->name}}">
            <input type="hidden" id="phoneNumber" value="{{$address_use->users->phone}}">
            <input type="hidden" value="{{$address_use->subdistrict->name}}, {{$address_use->city->name}}, {{$address_use->province->name}}, ID {{$address_use->postcode}}" id="shipping_detail">
            <div>
                <span class="text-sm font-bold">{{$address_use->users->name}}</span> <span class="font-sm">({{$address_use->users->phone}})</span>
            </div>
            <span class="text-sm text-secondary">{{$address_use->desc}}</span>
            <span class="d-block text-sm text-secondary text-capitalize">{{$address_use->subdistrict->name}}, {{$address_use->city->name}}, {{$address_use->province->name}}, ID {{$address_use->postcode}}</span>    

            <hr>

            <div class="w-100 d-flex mt-2">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="handle_open_modal('{{route('web.checkout.modalUserAddress')}}', '#modalListResult', '#contentListResult')">Pilih Alamat Lain</button>
            </div>
            <hr class="hr-list-bg m-0 mb-4 mt-3">
        </div>
    @endif
@endforeach