@foreach ($store as $toko)
    <div class="container-store">
        <!-- item -->
        @php
        $user_id = Auth::user()->id;
        $carts = \App\Models\Cart::where('store_id',$toko->store_id)->where('user_id',$user_id)->get();
        @endphp
        <input type="hidden" value="{{$toko->store_id}}" id="store_id">
        <div class="py-3 bg-white mt-2">
            <div class="px-3 d-flex align-items-center gap-3">
                <div class="d-flex flex-column justify-content-between">
                    <span class="font-bold">{{$carts[0]->cart_store->name}}</span>
                    <span class="font-thin font-sm">{{$carts[0]->cart_store->city->name}}</span>
                </div>
            </div>

            @foreach($carts as $st)
            <div class="px-3 d-flex gap-3 mt-2 item-product">
                <input type="hidden" value="{{$st->product->stock}}" id="stock">
                <input type="hidden" value="{{$st->product->price}}" id="price_product_to">
                <input type="hidden" value="{{$st->id}}" id="id_cart">
                <input type="hidden" value="{{$st->product->id}}" id="id_product">
                <div class="d-flex gap-3">
                    <div class="border w-20 h-20">
                        <img src="" alt="">
                    </div>
                    <div class="d-flex flex-column">
                        <span class="font-medium font-bold">Nintendo</span>
                        <span class="font-sm font-thin -mt-1">Neon biru merah</span>
                        <span class="font-sm font-thin -mt-1">1 Barang</span>
                        <h3 class="m-0 font-bold">Rp 4.050.000</h3>
                    </div>
                </div>
            </div>
            @endforeach

            <!-- shipping choice -->
            <div class="w-full px-3 mt-3" role="button">
                <div class="border rounded px-2 p-1 bg-white d-flex align-items-center justify-content-between" data-bs-toggle="offcanvas" data-bs-target="#canvasShipping" aria-controls="canvasShipping">
                    <div class="d-flex align-items-center gap-2">
                        <i class="icon-shipping-fast"></i>
                        <span class="font-bold">Pilih Pengiriman</span>
                    </div>

                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 40 40"><g fill="none" fill-rule="evenodd"><circle cx="24" cy="24" r="24"/><path stroke="#6c727c" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" d="M16.53 27.47l7.202-7.202m-7.202-7.2l7.202 7.203"/></g></svg>
                </div>
            </div>
            <!-- shipping choice -->
        </div>

        <!-- subtotal -->
        <!-- <div class="px-3 py-2 bg-white mt-1">
            <a class="d-flex justify-content-between subtotal-trigger" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <span class="font-bold">Subtotal</span>
                <div class="d-flex align-items-center gap-2">
                    <h4 class="m-0">Rp4.100.600</h4>
                    <svg width="8" height="5" viewBox="0 0 8 5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M8.71 12.29L11.3 9.7a.996.996 0 0 1 1.41 0l2.59 2.59c.63.63.181 1.71-.71 1.71H9.41c-.89 0-1.33-1.08-.7-1.71" id="a"/></defs><g transform="rotate(-180 8 7)" fill="none" fill-rule="evenodd"><path d="M0 24h24V0H0z"/><mask id="b" fill="#fff"><use xlink:href="#a"/></mask><use fill="#9FA6B0" xlink:href="#a"/><g mask="url(#b)" fill="#6C727C" fill-rule="nonzero"><path d="M1 1h22v22H1z"/></g></g></svg>
                </div>
            </a> -->
            <!-- list subtotal detail -->
            <!-- <div class="mt-1 collapse" id="collapseExample">
                <div class="d-flex flex-column gap-1">
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="font-sm">Harga (1 Barang)</span>
                        <span class="font-sm">Rp4.050.000</span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between ">
                        <span class="font-sm">Harga (1 Barang)</span>
                        <span class="font-sm">Rp4.050.000</span>
                    </div>
                </div>
            </div> -->
            <!-- list subtotal detail -->
        <!-- </div> -->
        <!-- subtotal -->
        <!-- /item -->
    </div>
@endforeach