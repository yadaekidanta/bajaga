<div class="card">
    <div class="card-body">
        <form id="form_alamat">
            <div class="row col-mb-50 gutter-50">
                <div class="col-lg-12">
                    <h3>Alamat Pengiriman</h3>
                    <div class="row mb-0">
                        <div class="col-12 form-group">
                            <label for="address">Alamat <small>*</small></label>
                            <textarea id="address" class="sm-form-control" name="address" rows="6" cols="30"></textarea>
                        </div>
                        <div class="col-6 form-group">
                            <label for="province">Provinsi <small>*</small></label>
                            <select class="form-control" name="province" id="province" class="form-control" required>
                                <option value="">Pilih Provinsi</option>
                            @foreach ($provinsi as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="col-6 form-group">
                            <label for="city">Kota <small>*</small></label>
                            <select class="form-control" name="city" id="city" class="form-control" required>
                                <option value="">Pilih Provinsi Terlebih Dahulu</option>
                            </select>
                        </div>
                        <div class="col-6 form-group">
                            <label for="subdistrict">Kecamatan <small>*</small></label>
                            <select class="form-control" name="subdistrict" id="subdistrict" class="form-control" required>
                                <option value="">Pilih Kota Terlebih Dahulu</option>
                            </select>
                        </div>
                        <div class="col-6 form-group">
                            <label for="postcode">Kode Pos <small>*</small></label>
                            <input type="text" id="postcode" maxlength="6" name="postcode" value="{{Auth::user()->postcode}}" class="sm-form-control" />
                        </div>
                        <div class="col-12 form-group">
                            <label for="desc">Deskripsi Alamat <small>*</small></label>
                            <input type="text" id="desc" name="deskripsi" value="{{Auth::user()->desc}}" class="sm-form-control" />
                        </div>
                    </div>
                    <button id="tombol_simpan" class="button button-3d mt-2 mt-sm-0 me-0" onclick="handle_save_modal('#tombol_simpan','#form_alamat','{{route('web.user-address.store')}}','POST','#modalListResult');">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $("#province").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.regional.city')}}",
            data: {province : $("#province").val()},
            success: function(response){
                $("#city").html(response);
            }
        });
    });
    $("#city").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.regional.subdistrict')}}",
            data: {city : $("#city").val()},
            success: function(response){
                $("#subdistrict").html(response);
            }
        });
    });
</script>