<x-auth-layout title="Reset Password">
    <div class="w-lg-550px bg-white rounded shadow-sm p-10 p-lg-15 mx-auto">
        <form class="form w-100" novalidate="novalidate" id="kt_new_password_form">
            <div class="text-center mb-10">
                <h1 class="text-dark mb-3">Setup New Password</h1>
                <div class="text-gray-400 fw-bold fs-4">Already have reset your password ?
                <a href="{{route('web.auth.index')}}" class="link-primary fw-bolder">Sign in here</a></div>
            </div>
            <input class="form-control form-control-lg form-control-solid" type="hidden" placeholder="" id="token" autocomplete="off" value="{{$token}}" />
            <div class="mb-10 fv-row" data-kt-password-meter="true">
                <div class="mb-1">
                    <label class="form-label fw-bolder text-dark fs-6">Password</label>
                    <div class="position-relative mb-3">
                        <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" id="password" name="password" autocomplete="off" />
                        <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                            <i class="bi bi-eye-slash fs-2"></i>
                            <i class="bi bi-eye fs-2 d-none"></i>
                        </span>
                    </div>
                    <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                    </div>
                </div>
                <div class="text-muted">Use 8 or more characters with a mix of letters, numbers &amp; symbols.</div>
            </div>
            <div class="fv-row mb-10">
                <label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
                <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" id="pc" name="password_confirmation" autocomplete="off" />
            </div>
            <div class="fv-row mb-10">
                <div class="form-check form-check-custom form-check-solid form-check-inline">
                    <input class="form-check-input" type="checkbox" name="toc" value="1" />
                    <label class="form-check-label fw-bold text-gray-700 fs-6">I Agree &amp;
                    <a href="#" class="ms-1 link-primary">Terms and conditions</a>.</label>
                </div>
            </div>
            <div class="text-center">
                <button type="button" id="kt_new_password_submit" class="btn btn-lg btn-primary fw-bolder">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </form>
    </div>
    @section('custom_js')
        <script type="text/javascript">
            "use strict";
            var KTPasswordResetNewPassword = (function () {
                var e,
                    t,
                    r,
                    o,
                    s = function () {
                        return 100 === o.getScore();
                    };
                return {
                    init: function () {
                        (e = document.querySelector("#kt_new_password_form")),
                            (t = document.querySelector("#kt_new_password_submit")),
                            (o = KTPasswordMeter.getInstance(e.querySelector('[data-kt-password-meter="true"]'))),
                            (r = FormValidation.formValidation(e, {
                                fields: {
                                    password: {
                                        validators: {
                                            notEmpty: { message: "The password is required" },
                                            callback: {
                                                message: "Please enter valid password",
                                                callback: function (e) {
                                                    // if (e.value.length > 0) return s();
                                                },
                                            },
                                        },
                                    },
                                    "password_confirmation": {
                                        validators: {
                                            notEmpty: { message: "The password confirmation is required" },
                                            identical: {
                                                compare: function () {
                                                    return e.querySelector('[name="password"]').value;
                                                },
                                                message: "The password and its confirm are not the same",
                                            },
                                        },
                                    },
                                    toc: { validators: { notEmpty: { message: "You must accept the terms and conditions" } } },
                                },
                                plugins: { trigger: new FormValidation.plugins.Trigger({ event: { password: !1 } }), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }) },
                            })),
                            t.addEventListener("click", function (s) {
                                s.preventDefault(),
                                r.revalidateField("password"),
                                r.validate().then(function (r) {
                                    var reset_data = {
                                        password: $("#password").val(),
                                        password_confirmation: $("#pc").val(),
                                        token: $("#token").val()
                                    };
                                    t.setAttribute("data-kt-indicator", "on"),(t.disabled = !0);
                                    if(r == "Valid"){
                                        $.ajax({
                                            type: "POST",
                                            url: "{{route('web.auth.reset')}}",
                                            data: reset_data,
                                            dataType: 'json',
                                            success: function (response) {
                                                if (response.alert=="success") {
                                                    setTimeout(function () {
                                                        t.removeAttribute("data-kt-indicator"),
                                                            (t.disabled = !1),
                                                            Swal.fire({ 
                                                                text: response.message, 
                                                                icon: "success", 
                                                                buttonsStyling: !1, 
                                                                confirmButtonText: "Ok, got it!", 
                                                                customClass: { confirmButton: "btn btn-primary" } })
                                                                .then(function (t) {
                                                                if (t.isConfirmed) {
                                                                    location.href = '{{route('web.auth.password_changed')}}'
                                                                }
                                                            });
                                                    }, 2e3);
                                                } else {
                                                    setTimeout(function () {
                                                        t.removeAttribute("data-kt-indicator"),
                                                            (t.disabled = !1),
                                                            Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, got it!", customClass: { confirmButton: "btn btn-primary" } }).then(function (t) {
                                                                // 
                                                            });
                                                    }, 2e3);
                                                }
                                            }
                                        });
                                    }else{
                                        t.removeAttribute("data-kt-indicator"),(t.disabled = !1);
                                        Swal.fire({
                                            text: "Sorry, looks like there are some errors detected, please try again.",
                                            icon: "error",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: { confirmButton: "btn btn-primary" },
                                        });
                                    }
                                });
                            }),
                            e.querySelector('input[name="password"]').addEventListener("input", function () {
                                this.value.length > 0 && r.updateFieldStatus("password", "NotValidated");
                            });
                    },
                };
            })();
            KTUtil.onDOMContentLoaded(function () {
                KTPasswordResetNewPassword.init();
            });
        </script>
    @endsection
</x-auth-layout>