<x-auth-layout title="Login">
    <div class="pt-lg-10">
        <h1 class="fw-bolder fs-2qx text-gray-800 mb-7">Password is changed</h1>
        <div class="fw-bold fs-3 text-muted mb-15">Your password is successfully changed. Please Sign
        <br />in to your account.</div>
        <div class="text-center">
            <a href="{{route('web.auth.index')}}" class="btn btn-primary btn-lg fw-bolder">Sign In</a>
        </div>
    </div>
</x-auth-layout>