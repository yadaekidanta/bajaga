<x-auth-layout title="Login">
    <div class="d-flex flex-center flex-column flex-column-fluid">
        <!--begin::Wrapper-->
        <div id="page_login" class="w-lg-500px p-10 p-lg-15 mx-auto">
            <!--begin::Form-->
            <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form">
                <!--begin::Heading-->
                <div class="text-center mb-10">
                    <!--begin::Title-->
                    <h1 class="text-dark mb-3">Sign In to {{config('app.name')}}</h1>
                    <!--end::Title-->
                    <!--begin::Link-->
                    <div class="text-gray-400 fw-bold fs-4">Belum punya akun?
                    <a href="{{route('web.auth.get_register')}}" class="link-primary fw-bolder">Register</a></div>
                    <!--end::Link-->
                </div>
                <!--begin::Heading-->
                <!--begin::Input group-->
                <div class="fv-row mb-7">
                    <!--begin::Label-->
                    <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                    <!--end::Label-->
                    <!--begin::Input-->
                    <input class="form-control form-control-lg form-control-solid" type="text" name="email" autocomplete="off" />
                    <!--end::Input-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="fv-row mb-7">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-stack mb-2">
                        <!--begin::Label-->
                        <label class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                        <!--end::Label-->
                    </div>
                    <!--end::Wrapper-->
                    <!--begin::Input-->
                    <input class="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="off" />
                    <!--end::Input-->
                    
                    <div class="d-flex justify-content-end mt-3">
                        <!--begin::Link-->
                        <a href="javascript:;" onclick="auth_content('page_forgot');" class="link-primary fs-6 fw-bolder">Forgot Password ?</a>
                        <!--end::Link-->
                    </div>
                </div>
                <!--end::Input group-->
                <!--begin::Actions-->
                <div class="text-center">
                    <!--begin::Submit button-->
                    <button type="submit" id="kt_sign_in_submit" onclick="handle_post('#kt_sign_in_submit','#kt_sign_in_form','{{route('web.auth.login')}}','Sign in');" class="btn btn-lg btn-primary w-100 mb-5">
                        Sign in
                    </button>
                    <!--end::Submit button-->
                    <!--begin::Separator-->
                    {{-- <div class="text-center text-muted text-uppercase fw-bolder mb-5">or</div> --}}
                    <!--end::Separator-->
                    <!--begin::Google link-->
                    {{-- <a href="#" class="btn btn-flex flex-center btn-light btn-lg w-100 mb-5">
                    <img alt="Logo" src="assets/media/svg/brand-logos/google-icon.svg" class="h-20px me-3" />Continue with Google</a> --}}
                    <!--end::Google link-->
                    <!--begin::Google link-->
                    {{-- <a href="#" class="btn btn-flex flex-center btn-light btn-lg w-100 mb-5">
                    <img alt="Logo" src="assets/media/svg/brand-logos/facebook-4.svg" class="h-20px me-3" />Continue with Facebook</a> --}}
                    <!--end::Google link-->
                    <!--begin::Google link-->
                    {{-- <a href="#" class="btn btn-flex flex-center btn-light btn-lg w-100">
                    <img alt="Logo" src="assets/media/svg/brand-logos/apple-black.svg" class="h-20px me-3" />Continue with Apple</a> --}}
                    <!--end::Google link-->
                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>
        <!--end::Wrapper-->

        <div id="page_forgot" style="display:none;">

            <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                <form class="form w-100" novalidate="novalidate" id="form_forgot">
                    <div class="text-center mb-10">
                        <h1 class="text-dark mb-3">Trouble Logging In?</h1>
                        <div class="text-gray-400 fw-bold fs-4">Enter you email and we'll send you link to reset your password</div>
                    </div>

                    <div class="fv-row mb-7">
                        <label class="form-label fw-bolder text-dark fs-6">Email</label>
                        <input class="form-control form-control-lg form-control-solid" type="email" id="email_forgot" name="email" autocomplete="off" data-forgot="1" />
                    </div>
                    <div class="text-center">
                        <button id="tombol_forgot" onclick="handle_post('#tombol_forgot','#form_forgot','{{route('web.auth.forgot')}}','POST');" class="btn btn-lg btn-primary w-100 mb-5">Send</button>

                        <a href="javascript:;" onclick="auth_content('page_login');" class="btn btn-lg btn-light-primary w-100 mb-5">Cancel</a>

                    </div>

                </form>

            </div>

        </div>
    </div>
</x-auth-layout>