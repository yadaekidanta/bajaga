<x-mail-layout title="Change Password">
    <div class="pt-lg-10">
        <h1 class="fw-bolder fs-2qx text-gray-800 mb-7">Account Deactivation successfully </h1>
        <div class="fw-bold fs-3 text-muted mb-15">Your account is successfully deactivated.</div>
        <div class="text-center">
            <a href="{{route('web.auth.index')}}" class="btn btn-primary btn-lg fw-bolder">Sign In</a>
        </div>
    </div>
</x-mail-layout>