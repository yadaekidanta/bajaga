<x-web-layout title="Etalase" keyword="Bajaga Online Store">
    <section id="content">
            <div class="container clearfix">

                <div class="row gutter-40 col-mb-80">
                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent col-lg-9 order-lg-last">

                        <!-- Shop
                        ============================================= -->
                        <div class="row grid-container gutter-20" data-layout="fitRows">
                            <div class="row gap-3 mt-5 ms-2 grid-6 mb-3" id="list_result">
                                @if (count($collections) > 0)
                                @foreach ($collections as $item)
                                    <div style="width: 12em; height: 20em;" class="rounded overflow-hidden px-0 d-flex flex-column shadow cursor-pointer card-product">
                                        <a href="{{route('web.product.show', $item->slug)}}" class="position-relative d-flex align-items-center justify-content-between overflow-hidden w-100" style="height: 12em">
                                            <img class="w-100 h-100 object-fit-cover" src="{{$item->image}}" alt="">
                                        </a>
                                        <div class="px-2 py-2">
                                            <a href="{{route('web.product.show', $item->slug)}}" class="text-dark">{{Str::limit($item->name, 18)}}</a>
                                            <a href="{{route('web.product.show', $item->slug)}}" class="text-lg font-bold mt-2 d-block text-dark">Rp {{number_format($item->price)}}</a>
                                            <a href="{{route('web.store', $item->product_store->id)}}" class="d-flex mt-1 align-items-center gap-3">
                                                <img style="width: 15px" src="{{asset('img/other/OS-Badge-80.png')}}" alt="">
                                                <span class="text-secondary city-text">{{Str::limit($item->product_store->city->name, 14)}}</span>
                                                <span class="text-secondary store-text d-none">{{Str::limit($item->product_store->name, 14)}}</span>
                                            </a>
                                            <a href="{{route('web.product.show', $item->slug)}}" class="d-flex mt-1 text-dark gap-1">
                                                <i class="icon-star3 text-emas"></i>
                                                <span>{{$item->rating}}</span> | 
                                                <span>Terjual 10rb+</span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                                
                                <script>
                                    $(".card-product").on("mouseleave", function() {
                                                $(this).find(".store-text")[0].classList.add("d-none")
                                                $(this).find(".city-text")[0].classList.remove("d-none")
                                                // console.log($(this).find("..city-text"));
                                            })

                                            $(".card-product").on("mouseover", function() {
                                                $(this).find(".store-text")[0].classList.remove("d-none")
                                                $(this).find(".city-text")[0].classList.add("d-none")
                                            })
                                </script>
                            @endif
                            </div>
                            <div class="ajax-load d-flex justify-content-center w-100 mt-5">
                                <div class="spinner-grow text-secondary" role="status">
                                </div>
                            </div>
                        </div><!-- #shop end -->

                    </div><!-- .postcontent end -->

                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar col-lg-3 pt-5">
                        <div class="sidebar-widgets-wrap">

                            <div class="widget widget-filter-links">
                                <h4 class="px-2">Select Category</h4>
                                <ul class="custom-filter ps-2" data-container="#shop" data-active-class="active-filter">
                                    <li class="p-2 rounded bg-aliceblue">
                                        <a class="hover-opacity-65 w-100 cursor-pointer" onclick="filter_product('semua', this)" data-filter="semua">Tampilkan Semua
                                        </a>
                                    </li>
                                    @foreach ($list_category as $item)
                                    <li class="p-2 rounded"><a class="hover-opacity-65 cursor-pointer font-thin" onclick="filter_product('{{$item->id}}', this)" data-filter=".{{$item->slug}}">{{$item->title}}</a> </li>
                                    @endforeach
                                </ul>

                            </div>

                            <div class="widget widget-filter-links">

                                <h4>Sort By</h4>
                                <ul class="sorting ps-2">
                                    <li class="widget-filter-reset active-filter">
                                        <a href="#" data-sort-by="original-order">Clear</a></li>
                                    <li>
                                        <a href="#" data-sort-by="name">Name</a></li>
                                    <li>
                                        <a href="#" data-sort-by="price_lh">Price: Low to High</a></li>
                                    <li>
                                        <a href="#" data-sort-by="price_hl">Price: High to Low</a></li>
                                </ul>

                            </div>

                        </div>
                    </div><!-- .sidebar end -->
                </div>

            </div>
            <form id="content_filter_category" class="d-none">
                <input type="hidden" id="filter_input_category" name="category">
                <input type="hidden" id="filter_input_store_id" name="store_id">
            </form>
    </section>
    @section('custom_js')
        <script>
            let urlParams = new URLSearchParams(params);
            $("#filter_input_store_id").val(urlParams.get("store_id"))
            $(".card-product").on("mouseleave", function() {
                $(this).find(".store-text")[0].classList.add("d-none")
                $(this).find(".city-text")[0].classList.remove("d-none")
                // console.log($(this).find("..city-text"));
            })

            $(".card-product").on("mouseover", function() {
                $(this).find(".store-text")[0].classList.remove("d-none")
                $(this).find(".city-text")[0].classList.add("d-none")
            })

            //function for Scroll Event
            var pages = 1;
            $(document).ready(function(){
                let options = {
                    root: null,
                    rootMargin: '350px',
                    threshold: 0.5
                }

                const observer = new IntersectionObserver(handleIntersect, options)
                observer.observe(document.querySelector("footer"))
            })

            function handleIntersect(entries){
                if (entries[0].isIntersecting) {
                    ++pages
                    load_more(pages);
                }
            }

            function filter_product(id, el){
                if (id == "semua") {
                    pages = 1
                    $("#filter_input_category").val("")
                }else{
                    $("#filter_input_category").val(id)
                }
                // console.log($(el).parent());
                $(".custom-filter > *").removeClass("bg-aliceblue")
                $(el).parent()[0].classList.add("bg-aliceblue")
                state_filter = true
                // console.log($("#filter_input_category").val());
                load_custom('#list_result', '', 1, function(result) {
                    if(result == ""){
                        $("#list_result").html("")
                        $('.ajax-load').html("No more Items Found!");
                        return;
                    }
                }, '#content_filter_category')
            }
        </script>
    @endsection
</x-web-layout>