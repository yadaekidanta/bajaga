<!DOCTYPE html>
<html>
<head>
  <title>Barcode</title>
</head>
<body>

  <div class="container">
    <div class="row">
      <div class="col-lg-12" style="margin-top: 15px ">
        <div class="pull-left">
          <h2>Barcode Produk Anda</h2>
        </div>

      </div>
    </div><br>

    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Barcode</th>
        </tr>
      </thead>
      @php
      $i = 1;
      $var = '';
      if ($tipe == "QRCODE"){
        $var = 'DNS2D';
      }else{
        $var = 'DNS1D';
      }
      @endphp
      <tbody>
      {{-- @foreach($data as $item) --}}
      @for($j=0;$j < $qty;$j++)
      @php
      $sku = $product->id.$j;
      @endphp
      <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $product->name }}</td>
        <td>{!! $var::getBarcodeHTML($sku, $tipe) !!}</td>

      </tr>
      @endfor
      {{-- @endforeach --}}
    </tbody>
    </table>
  </div>
</body>
</html>