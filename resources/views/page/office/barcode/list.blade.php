<form id="form_input">
    <div class="row">
        <div class="col-lg-4">
            <label for="unit" class="required form-label">Nama Barang</label>
            <select data-control="select2" data-placeholder="Nama Barang" id="name" name="name" class="form-select form-select-solid">
                <option value="" SELECTED DISABLED >Pilih Barang</option>
                @foreach ($product as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="required fs-6 fw-bold mb-2">Total Barang</label>
            <input type="text" class="form-control" name="qty" placeholder="Masukkan Total Barang..." value="">
        </div>
        <div class="col-lg-4">
            <label for="unit" class="required form-label">Tipe Barcode</label>
            <select data-control="select2" data-placeholder="Nama Barang" id="tipe" name="tipe" class="form-select form-select-solid">
                <option value="" SELECTED DISABLED>Pilih Tipe Barcode</option>
                <option value="QRCODE">QRCODE</option>
                <option value="PHARMA">PHARMA</option>
                <option value="PHARMA2T">PHARMA2T</option>
                <option value="CODABAR">CODABAR</option>
                <option value="KIX">KIX</option>
                <option value="RMS4CC">RMS4CC</option>
                <option value="UPCA">UPCA</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="min-w-150px mt-10 text-end">
            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.barcode.request_download_pdf')}}','POST');" class="btn btn-sm btn-primary">Cetak Barcode</button>
        </div>
    </div>
</form>
{{-- <script>
    // @if($product)
    // $('#name').val('{{$product->name}}');
    // setTimeout(function(){ 
    //     $('#sku').trigger('change');
    //     setTimeout(function(){ 
    //         $('#kelas').val('{{$product->sku}}');
    //     }, 2000);
    // }, 1000);
    // @endif
    // $("#name").change(function(){
    //     $.ajax({
    //         type: "POST",
    //         url: '#',
    //         data: {pelajaran : $("#name").val()},
    //         success: function(response){
    //             $("#kelas").html(response);
    //         }
    //     });
    // });
</script> --}}