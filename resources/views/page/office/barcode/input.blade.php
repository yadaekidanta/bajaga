<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        Ubah Data Lelang Produk
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Nama Barang</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="" readonly>
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Tanggal Mulai Lelang</label>
                            <input type="text" class="form-control" id="tanggal" name="start" placeholder="Masukkan Tanggal..." value="" readonly>
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Tanggal Berhenti Lelang</label>
                            <input type="text" class="form-control" id="end" name="end" placeholder="Masukkan Tanggal..." value="" readonly>
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Harga Lelang</label>
                            <input type="text" class="form-control" name="price" placeholder="Masukkan Harga Lelang..." value="">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Total</label>
                            <input type="text" class="form-control" name="qty" placeholder="Masukkan Total Produk Yang Dilelang..." value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="min-w-150px mt-10 text-end">
                            <button id="tombol_simpan"  class="btn btn-sm btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    obj_date('tanggal');
    obj_date('end');
    @if($data->id)
    $("#category_id").val('{{$data->product_category_id}}').trigger('change');
    @endif
    obj_select('unit_id','Pilih Satuan');
    obj_select('brand_id','Pilih Merek');
    obj_select('category_id','Pilih Kategori');
    obj_select('warranty_id','Pilih Garansi');
    obj_select('business_location','Pilih Lokasi Bisnis');
    obj_select("subcategory_id","Pilih Sub Kategori");

    @if($data->product_category_id)
    $("#category_id").select2().select2("val", '{{$data->product_category_id}}');
    setTimeout(function(){ 
        $('#category_id').trigger('change');
        setTimeout(function(){ 
            $('#subcategory_id').val('{{$data->product_subcategory_id}}');
            $('#subcategory_id').trigger('change');
        }, 2000);
    }, 1000);
    @endif
    $("#category_id").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.product-category.get_list_sub')}}",
            data: {category : $("#category_id").val()},
            success: function(response){
                $("#subcategory_id").html(response);
            }
        });
    });
</script>
