<div class="modal-header">
    <h5 class="modal-title">Tambah data Kategori</h5>
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <i class="las la-times"></i>
    </div>
</div>
<div class="modal-body">
    <form id="form_input_modal">
        <div class="row">
            <div class="col-lg-4">
                <label class="required fs-6 fw-bold mb-2">Nama</label>
                <input type="text" class="form-control" name="title" placeholder="Masukkan Nama..." value="{{$data->title}}">
            </div>
            <div class="col-lg-4">
                <label class="fs-6 fw-bold mb-2">Kategori</label>
                <select class="form-select" id="category" name="category">
                    <option SELECTED DISABLED>Pilih Kategori</option>
                    @foreach ($category as $item)
                    <option value="{{$item->id}}" {{$data->product_category_id == $item->id ? 'selected' : ''}}>{{$item->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <label class="fs-6 fw-bold mb-2">Foto</label>
                <input type="file" class="form-control" name="photo" placeholder="Foto...." value="{{$data->photo}}">
            </div>
            <div class="min-w-150px mt-10 text-end">
                <button id="tombol_simpan_modal" onclick="handle_save_modal('#tombol_simpan_modal','#form_input_modal','{{route('office.product-category.store')}}','POST','#modalListResult');" class="btn btn-sm btn-primary">Simpan</button>
            </div>
        </div>
    </form>
</div>