@if($data->id)
                <div class="card mt-4">
                    <div class="card-body pt-2 mt-5">
                        <div class="mb-5">
                            <h3 class="m-0">Kode</h3>
                            <h5 class="d-block">tgl</h5>
                        </div>
                        <div class="d-flex h-100">
                            <div class="col-4">
                                <div class="">
                                    <div class="">
                                        <h5 class="d-block m-0 font-thin">Nama Supplier</h5>
                                        <h6 class="d-block m-0">osi</h6>
                                    </div>
                                    <div class="mt-2">
                                        <h5 class="d-block m-0 font-thin">Email</h5>
                                        <h5 class="d-block m-0">osi</h5>
                                    </div>
                                    <div class="mt-2">
                                        <h5 class="d-block m-0 font-thin">Telp</h5>
                                        <h5 class="d-block m-0">osi</h5>
                                    </div>
                                    <div class="mt-2">
                                        <h5 class="d-block m-0 font-thin">Alamat</h5>
                                        <h5 class="d-block m-0">osi</h5>
                                    </div>
                                    
                                </div>
                            </div>  

                            {{-- vr --}}
                            <div class="bg-secondary mx-5" style="width: 3px;"></div>
                            {{-- vr --}}
                            
                            <div class="col-4">
                                <div class="d-flex align-items-center gap-2 mb-5">
                                    <h5 class="font-thin m-0 w-12em">Status Transaksi</h5>
                                    <span class="badge bg-warning">Tertunda</span>
                                </div>
                                <div class="d-flex align-items-center gap-2">
                                    <h5 class="font-thin m-0 w-12em">Status Pembayaran</h5>
                                    <span class="badge bg-danger">Belumn Lunas</span>
                                </div>
                            </div>

                            {{-- vr --}}
                            <div class="bg-secondary mx-5" style="width: 3px;"></div>
                            {{-- vr --}}

                            <div class="col-2">
                                <div class="mb-5">
                                    <h5 class="font-thin m-0">Grand Total</h5>
                                    <h3 class="m-0">Rp{{number_format(4000)}}</h3>
                                </div>
                                <div class="">
                                    <h5 class="font-thin m-0">Total Pembayaran</h5>
                                    <h3 class="m-0">Rp{{number_format(4000)}}</h3>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>            
            @endif