<div class="post d-flex flex-column-fluid" id="kt_post">
  <div id="kt_content_container" class="container-xxl">
    <div class="card">
      <div class="card-header border-0 pt-6">
        <div class="card-title">
          <h6>
            Detail Bid Produk
          </h6>
        </div>
        <div class="card-toolbar">
          <div class="d-flex justify-content-end">
            <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
          </div>
        </div>
      </div>
      <div class="card-body pt-0">
        @foreach ($bids as $key => $data)
          <div class="row mb-5">
            <div class="col-1 text-center">
              <img src="{{ $data->users->avatar == null ? 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png' : $data->users->avatar }}" class="img-fluid rounded-circle mx-auto" style="max-width: 32px;">
            </div>
            <div class="col-9 ps-0">
              <p class="mb-0">
                {{ $data->users->name }}<br/>
                {{ $data->bid }}
              </p>
            </div>
            @if (++$key == 1)
              <div class="col-2">
                <a href="{{ route('office.productLelang.bid_send_mail', $data->users->id) }}" class="btn btn-sm btn-primary">Kirim Email</a>
              </div>
            @endif
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
