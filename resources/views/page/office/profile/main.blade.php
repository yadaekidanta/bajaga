<x-office-layout title="Pembelian">
    <div id="content_list">
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="card mb-5 mb-xxl-8 mt-10">
                    <form id="form-profile">
                        <div class="card-body pb-0">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label for="username">Password Baru</label>
                                    <input class="form-control" type="password" name="password_baru" id="password_baru">
                                </div>
                                <div class="col-lg-12 mt-4">
                                    <label for="username">Konfirmasi Password Baru</label>
                                    <input class="form-control" type="password" name="kpassword_baru" id="kpassword_baru">
                                </div>
                            </div>
                            <div class="min-w-150px mt-10 mb-5 text-end">
                                <button class="btn btn-primary" id="tombol_simpan" onclick="handle_save_password('#tombol_simpan','#form-profile','{{route('office.profile.cpassword')}}','POST');">Kirim</button>
                            </div>
                        </div>
                    </form>
                    <form id="kt_forms_widget_1_form" class="d-none ql-quil ql-quil-plain pb-3">
                        <div id="kt_forms_widget_1_editor" class="py-6"></div>
                        <div class="separator"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-office-layout>