<x-office-layout title="Pembelian">
    <div id="content_list">
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="card">
                    <form id="content_filter">
                        <div class="card-header border-0 pt-6">
                            <div class="card-title d-none">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                        </svg>
                                    </span>
                                    <input type="text" name="keywords" onkeyup="load_list(1);" class="form-control form-control-solid w-250px ps-15" placeholder="Cari data..." />
                                </div>
                            </div>
                            <div class="card-toolbar d-none">
                                <button type="button" class="btn btn-light-primary me-3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="black" />
                                        </svg>
                                    </span>
                                    Filter
                                </button>
                                <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true" id="kt-toolbar-filter">
                                    <div class="px-7 py-5">
                                        <div class="fs-4 text-dark fw-bolder">Filter</div>
                                    </div>
                                    <div class="separator border-gray-200"></div>
                                    <div class="px-7 py-5">
                                        <div class="mb-10">
                                            <label class="form-label fs-5 fw-bold mb-3">Status:</label>
                                            <select name="tipe" class="form-select form-select-solid fw-bolder" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" data-kt-customer-table-filter="tipe" data-dropdown-parent="#kt-toolbar-filter">
                                                <option value="all">Semua</option>
                                                <option value="Pending">Tertunda</option>
                                                <option value="Ordered">Dipesan</option>
                                                <option value="Received">Diterima</option>
                                            </select>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="button" onclick="load_list(1);" class="btn btn-primary" data-kt-menu-dismiss="true" data-kt-customer-table-filter="filter">Apply</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 gy-5">
                                <thead class="px-3">
                                    <tr>
                                        <th colspan="2" class="text-center bg-aliceblue2 font-bold">Aktiva</th>
                                        <th colspan="2" class="text-center bg-aliceblue2 font-bold">Pasiva</th>
                                    </tr>
                                </thead>
                                    <tr>
                                        <td class="bg-aliceblue font-bold">Aktiva Lancar</td>
                                        <td class="bg-aliceblue font-bold"></td>
                                        <td class="bg-aliceblue font-bold">Kewajiban Lancar</td>
                                        <td class="bg-aliceblue font-bold"></td>
                                    </tr>
                                <tbody class="px-3">
                                    <tr>
                                        <td>Kas</td>
                                        <td>Rp{{number_format($kas)}}</td>
                                        <td>Hutang Usaha</td>
                                        <td>Rp{{number_format($utang)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Bank</td>
                                        <td>Rp{{number_format($bank)}}</td>
                                        <td>Pinjaman Bank</td>
                                        <td>Rp{{number_format($pinjaman_bank)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Piutang Penjualan</td>
                                        <td>Rp{{number_format($piutang_penjualan)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Piutang Ragu Ragu</td>
                                        <td>Rp{{number_format($piutang_ragu_ragu)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Persediaan Barang</td>
                                        <td>Rp{{number_format($persedian_barang)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="bg-aliceblue font-bold">Total Aktiva Lancar</td>
                                        @php
                                            $total_aktiva_lancar = $kas + $bank + $piutang_penjualan + $piutang_ragu_ragu + $persedian_barang;
                                            $total_kewajiban_lancar = $pinjaman_bank + $utang;
                                        @endphp
                                        <td class="bg-aliceblue font-bold">Rp{{number_format($total_aktiva_lancar)}}</td>
                                        <td class="bg-aliceblue font-bold">Total Kewajiban Lancar</td>
                                        <td class="bg-aliceblue font-bold">Rp{{number_format($total_kewajiban_lancar)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Aktiva Tetap</td>
                                        <td></td>
                                        <td>Kewajiban Tetap</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Asset</td>
                                        <td>Rp{{number_format($asset)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr class="bg-aliceblue font-bold">
                                        <td>Total Aktiva Tetap</td>
                                        @php
                                            $total_aktiva_tetap = $asset;
                                            $total_kewajiban_tetap = 0;
                                            $total_kewajiban = $total_kewajiban_tetap + $total_kewajiban_lancar;
                                        @endphp
                                        <td>Rp{{number_format($total_aktiva_tetap)}}</td>
                                        <td>Total Kewajiban Tetap</td>
                                        <td>Rp{{number_format($total_kewajiban_tetap)}}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="bg-aliceblue font-bold">Total Kewajiban</td>
                                        <td class="bg-aliceblue font-bold">Rp{{$total_kewajiban}}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Modal</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Modal</td>
                                        <td>Rp{{number_format($modal)}}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="bg-aliceblue font-bold">Total Modal</td>
                                        <td class="bg-aliceblue font-bold">Rp{{number_format($modal)}}</td>
                                    </tr>
                                    <tr class="bg-aliceblue2 font-bold">
                                        <td>Total Aktiva</td>
                                        <td>Rp{{number_format($total_aktiva_tetap + $total_aktiva_lancar)}}</td>
                                        <td>Total Pasiva</td>
                                        <td>Rp{{number_format($total_kewajiban + $modal)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-office-layout>