<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Produk
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">SKU</label>
                            <input type="text" class="form-control" name="sku" placeholder="Masukkan SKU..." value="{{$data->sku}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Kondisi</label>
                            <select data-control="select2" data-placeholder="Pilih Kondisi" name="condition" class="form-select form-select-solid">
                                <option SELECTED DISABLED>Pilih Kondisi</option>
                                <option value="baru"{{$data->condition=="baru"?"selected":""}}>Baru</option>
                                <option value="bekas"{{$data->condition=="bekas"?"selected":""}}>Bekas</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="required form-label">Harga</label>
                            <input type="text" class="form-control" id="harga" name="harga" placeholder="Masukkan Jumlah Peringatan..." value="{{$data->price}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="required form-label">Stok</label>
                            <input type="tel" class="form-control" id="stok" name="stok" placeholder="Masukkan Jumlah Peringatan..." value="{{$data->stock}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 mb-5">
                            <label for="unit" class="required form-label">Satuan</label>
                            <div class="input-group">
                                <select data-control="select2" data-placeholder="Pilih Satuan" id="unit_id" name="product_unit_id" class="form-select form-select-solid">
                                    <option value="" SELECTED DISABLED >Pilih Satuan</option>
                                    @foreach ($satuan as $item)
                                    <option value="{{$item->id}}"{{$item->id==$data->product_unit_id?"selected":""}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <a href="javascript:;" onclick="handle_open_modal('{{route('office.product-unit.input_modal')}}','#modalListResult','#contentListResult');">
                                            <i class="las la-plus"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="unit" class="required form-label">Merek</label>
                            <div class="input-group">
                                <select data-control="select2" data-placeholder="Pilih Merek" id="brand_id" name="product_brand_id" class="form-select form-select-solid">
                                    <option value="" SELECTED DISABLED >Pilih Merek</option>
                                    @foreach ($merek as $item)
                                    <option value="{{$item->id}}"{{$item->id==$data->product_brand_id?"selected":""}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <a href="javascript:;" onclick="handle_open_modal('{{route('office.product-brand.input_modal')}}','#modalListResult','#contentListResult');">
                                            <i class="las la-plus"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="unit" class="required form-label">Kategori</label>
                            <div class="input-group">
                                <select data-control="select2" data-placeholder="Pilih Kategori" id="category_id" name="product_category_id" class="form-select form-select-solid">
                                    <option value="" SELECTED DISABLED>Pilih Kategori</option>
                                    @foreach ($kategori as $item)
                                    <option value="{{$item->id}}"{{$item->id==$data->product_category_id?"selected":""}}>{{$item->title}}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <a href="javascript:;" onclick="handle_open_modal('{{route('office.product-category.input_modal')}}','#modalListResult','#contentListResult');">
                                            <i class="las la-plus"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="subcategory_id" class="form-label">Sub Kategori</label>
                            <select data-control="select2" data-placeholder="Pilih Kategori terlebih dahulu" id="subcategory_id" name="product_subcategory_id" class="form-select form-select-solid">
                                <option value="" SELECTED DISABLED>Pilih SubKategori</option>
                                @foreach ($subkategori as $item)
                                <option value="{{$item->id}}"{{ $item->id == $data->product_subcategory_id ? "selected" : "" }}>{{$item->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="required form-label">Deskripsi Produk</label>
                            <textarea name="desc" cols="30" rows="5" class="form-control">{{$data->desc}}</textarea>
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="required form-label">Berat Produk</label>
                            <input type="text" class="form-control" name="weight" placeholder="Masukkan Berat..." value="{{$data->weight}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="required form-label">Gambar Produk</label>
                            <input type="file" class="form-control" name="photo" placeholder="Gambar Produk...." value="{{$data->photo}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="required form-label">Video Produk</label>
                            <input type="text" class="form-control" name="video_url" placeholder="Gambar Produk...." value="{{$data->video_url}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="form-label">Brosur</label>
                            <input type="file" class="form-control" name="brocure" placeholder="brocure...." value="{{$data->brocure}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="unit" class="required form-label">Garansi</label>
                            <select data-control="select2" data-placeholder="Pilih Garansi" id="warranty_id" name="product_warranty_id" class="form-select form-select-solid">
                                <option value="" SELECTED DISABLED >Pilih Garansi</option>
                                @foreach ($garansi as $item)
                                <option value="{{$item->id}}"{{$item->id==$data->product_warranty_id?"selected":""}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="unit" class="required form-label">Lokasi Bisnis</label>
                            <select data-control="select2" data-placeholder="Pilih Lokasi Bisnis" id="business_location" name="business_location" class="form-select form-select-solid">
                                <option value="" SELECTED DISABLED >Pilih Lokasi</option>
                                @foreach ($lokasi as $item)
                                <option value="{{$item->id}}"{{$item->id==$data->business_location_id?"selected":""}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="required form-label">Jumlah Peringatan</label>
                            <input type="text" class="form-control" name="alert_quantity" placeholder="Masukkan Jumlah Peringatan..." value="{{$data->alert_quantity}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="form-label">Custom Field 1</label>
                            <input type="text" class="form-control" name="custom_field_1" placeholder="Custom Field..." value="{{$data->custom_field_1}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="form-label">Custom Field 2</label>
                            <input type="text" class="form-control" name="custom_field_2" placeholder="Custom Field..." value="{{$data->custom_field_2}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="form-label">Custom Field 3</label>
                            <input type="text" class="form-control" name="custom_field_3" placeholder="Custom Field..." value="{{$data->custom_field_3}}">
                        </div>
                        <div class="col-lg-6 mb-5">
                            <label for="condition" class="form-label">Custom Field 4</label>
                            <input type="text" class="form-control" name="custom_field_4" placeholder="Custom Field..." value="{{$data->custom_field_4}}">
                        </div>
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.product.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.product.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    @if($data->id)
    $("#category_id").val('{{$data->product_category_id}}').trigger('change');
    @endif
    obj_select('unit_id','Pilih Satuan');
    obj_select('brand_id','Pilih Merek');
    obj_select('category_id','Pilih Kategori');
    obj_select('warranty_id','Pilih Garansi');
    obj_select('business_location','Pilih Lokasi Bisnis');
    obj_select("subcategory_id","Pilih Sub Kategori", '100%');
    ribuan('harga');
    number_only('stok');
    // get_unit();
    // get_brand();
    // get_category();
    // get_warranty();
    // get_business();

    // function get_unit(){
    //     $.post('{{route('office.product-unit.get_list')}}', {}, function(result) {
    //         $("#unit_id").html(result);
    //     }, "html");
    // }
    // function get_brand(){
    //     $.post('{{route('office.product-brand.get_list')}}', {}, function(result) {
    //         $("#brand_id").html(result);
    //     }, "html");
    // }
    // function get_category(){
    //     $.post('{{route('office.product-category.get_list')}}', {}, function(result) {
    //         $("#category_id").html(result);
    //     }, "html");
    // }
    // function get_warranty(){
    //     $.post('{{route('office.product-warranty.get_list')}}', {}, function(result) {
    //         $("#warranty_id").html(result);
    //     }, "html");
    // }
    // function get_business(){
    //     $.post('{{route('office.business-location.get_list')}}', {}, function(result) {
    //         $("#business_location").html(result);
    //     }, "html");
    // }
    @if($data->product_category_id)
    $("#category_id").select2().select2("val", '{{$data->product_category_id}}');
    setTimeout(function(){ 
        $('#category_id').trigger('change');
        setTimeout(function(){ 
            $('#subcategory_id').val('{{$data->product_subcategory_id}}');
            $('#subcategory_id').trigger('change');
        }, 2000);
    }, 1000);
    @endif
    $("#category_id").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.product-category.get_list_sub')}}",
            data: {category : $("#category_id").val()},
            success: function(response){
                $("#subcategory_id").html(response);
            }
        });
    });
</script>
