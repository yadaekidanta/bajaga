<div class="modal-header">
    <h5 class="modal-title">Daftar Barang</h5>

    <!--begin::Close-->
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <i class="las la-times"></i>
    </div>
    <!--end::Close-->
</div>
<div class="modal-body">
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr>
                <th>Gambar</th>
                <th>Kode Barang</th>
                <th>Nama Produk</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if ($collection->count() > 0)
            @foreach($collection as $item)
            <tr>
                <td><img src="{{$item->image}}" alt="test" height="30px"></td>
                <td>{{ $item->sku }}</td>
                <td>{{ $item->name }}</td>
                <td>
                    <a href="javascript:;" onclick="getBarang('{{$item->sku}}');" class="btn btn-light" data-bs-dismiss="modal">
                        <i class="las la-check"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4" class="text-center">Tidak Data</td>
            </tr>
        @endif
        </tbody>
    </table>
    {{-- {{$collection->links('theme.office.pagination')}} --}}
</div>
{{-- <div class="modal-footer">
    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
</div> --}}