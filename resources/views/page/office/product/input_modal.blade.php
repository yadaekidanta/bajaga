<div class="modal-header">
    <h5 class="modal-title">Tambah Barang</h5>
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <i class="las la-times"></i>
    </div>
</div>
<div class="modal-body">
    <form id="form_input_modal">
        <div class="row">
            <div class="col-lg-4">
                <label class="required fs-6 fw-bold mb-2">Nama</label>
                <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
            </div>
            <div class="col-lg-4">
                <label class="required fs-6 fw-bold mb-2">SKU</label>
                <input type="text" class="form-control" name="sku" placeholder="Masukkan SKU..." value="{{$data->sku}}">
            </div>
            <div class="col-lg-4">
                <label class="required fs-6 fw-bold mb-2">Kondisi</label>
                <select data-control="select2" data-placeholder="Pilih Kondisi" name="condition" class="form-select form-select-solid">
                    <option SELECTED DISABLED>Pilih Kondisi</option>
                    <option value="baru"{{$data->condition=="baru"?"selected":""}}>Baru</option>
                    <option value="bekas"{{$data->condition=="bekas"?"selected":""}}>Bekas</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 mb-5">
                <label for="unit" class="required form-label">Satuan</label>
                <div class="input-group">
                    <select data-control="select2" data-placeholder="Pilih Satuan" id="unit_id" name="product_unit_id" class="form-select form-select-solid"></select>
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <a href="javascript:;" onclick="handle_open_modal('{{route('office.product-unit.input_modal')}}','#modalListResult','#contentListResult');">
                                <i class="las la-plus"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-5">
                <label for="unit" class="required form-label">Merek</label>
                <div class="input-group">
                    <select data-control="select2" data-placeholder="Pilih Merek" id="brand_id" name="product_brand_id" class="form-select form-select-solid"></select>
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <a href="javascript:;" onclick="handle_open_modal('{{route('office.product-brand.input_modal')}}','#modalListResult','#contentListResult');">
                                <i class="las la-plus"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-5">
                <label for="unit" class="required form-label">Kategori</label>
                <div class="input-group">
                    <select data-control="select2" data-placeholder="Pilih Kategori" id="category_id" name="product_category_id" class="form-select form-select-solid"></select>
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <a href="javascript:;" onclick="handle_open_modal('{{route('office.product-brand.input_modal')}}','#modalListResult','#contentListResult');">
                                <i class="las la-plus"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-5">
                <label for="subcategory_id" class="form-label">Sub Kategori</label>
                <select data-control="select2" data-placeholder="Pilih Kategori terlebih dahulu" id="subcategory_id" name="product_subcategory_id" class="form-select form-select-solid"></select>
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="required form-label">Deskripsi Produk</label>
                <textarea name="desc" cols="30" rows="5" class="form-control">{{$data->desc}}</textarea>
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="required form-label">Berat Produk</label>
                <input type="text" class="form-control" name="weight" placeholder="Masukkan Berat..." value="{{$data->weight}}">
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="required form-label">Gambar Produk</label>
                <input type="file" class="form-control" name="photo" placeholder="Gambar Produk...." value="{{$data->photo}}">
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="form-label">Brosur</label>
                <input type="file" class="form-control" name="brocure" placeholder="brocure...." value="{{$data->brocure}}">
            </div>
            <div class="col-lg-4 mb-5">
                <label for="unit" class="required form-label">Garansi</label>
                <select data-control="select2" data-placeholder="Pilih Garansi" id="warranty_id" name="product_warranty_id" class="form-select form-select-solid"></select>
            </div>
            <div class="col-lg-4 mb-5">
                <label for="unit" class="required form-label">Lokasi Bisnis</label>
                <select data-control="select2" data-placeholder="Pilih Lokasi Bisnis" id="business_location" name="business_location" class="form-select form-select-solid"></select>
            </div>
            <div class="col-lg-4 mb-5">
                <label for="condition" class="required form-label">Jumlah Peringatan</label>
                <input type="text" class="form-control" name="alert_quantity" placeholder="Masukkan Jumlah Peringatan..." value="{{$data->alert_quantity}}">
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="form-label">Custom Field 1</label>
                <input type="text" class="form-control" name="custom_field_1" placeholder="Custom Field..." value="{{$data->custom_field_1}}">
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="form-label">Custom Field 2</label>
                <input type="text" class="form-control" name="custom_field_2" placeholder="Custom Field..." value="{{$data->custom_field_2}}">
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="form-label">Custom Field 3</label>
                <input type="text" class="form-control" name="custom_field_3" placeholder="Custom Field..." value="{{$data->custom_field_3}}">
            </div>
            <div class="col-lg-6 mb-5">
                <label for="condition" class="form-label">Custom Field 4</label>
                <input type="text" class="form-control" name="custom_field_4" placeholder="Custom Field..." value="{{$data->custom_field_4}}">
            </div>
            <div class="min-w-150px mt-10 text-end">
                <button id="tombol_simpan_modal" onclick="handle_upload_modal('#tombol_simpan_modal','#form_input_modal','{{route('office.product.store')}}','POST');" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </form>
</div>
<script>
    $('#unit_id').select2({
        placeholder: "Pilih Satuan",
        width:'90%',
        language: {
            // You can find all of the options in the language files provided in the
            // build. They all must be functions that return the string that should be
            // displayed.
            "searching": function(){
                return "Harap tunggu";
            },
            "noResults": function(){
                return "Data Tidak ditemukan";
            },
            "inputTooShort": function () {
                return "Anda harus memasukkan setidaknya 1 karakter";
            }
        },
        // minimumInputLength: 1,
        ajax: {
            method:'POST',
            url: "{{route('office.product-unit.get_list')}}",
            data: function (params) {
                var query = {
                    search: params.term
                }
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name + ' | ' + item.shortname,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    $('#brand_id').select2({
        placeholder: "Pilih Satuan",
        width:'90%',
        language: {
            // You can find all of the options in the language files provided in the
            // build. They all must be functions that return the string that should be
            // displayed.
            "searching": function(){
                return "Harap tunggu";
            },
            "noResults": function(){
                return "Data Tidak ditemukan";
            },
            "inputTooShort": function () {
                return "Anda harus memasukkan setidaknya 1 karakter";
            }
        },
        // minimumInputLength: 1,
        ajax: {
            method:'POST',
            url: "{{route('office.product-brand.get_list')}}",
            data: function (params) {
                var query = {
                    search: params.term
                }
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    $("#category_id").val('{{$data->product_category_id}}').trigger('change');
    $('#category_id').select2({
        placeholder: "Pilih Kategori",
        width:'90%',
        language: {
            // You can find all of the options in the language files provided in the
            // build. They all must be functions that return the string that should be
            // displayed.
            "searching": function(){
                return "Harap tunggu";
            },
            "noResults": function(){
                return "Data Tidak ditemukan";
            },
            "inputTooShort": function () {
                return "Anda harus memasukkan setidaknya 1 karakter";
            }
        },
        // minimumInputLength: 1,
        ajax: {
            method:'POST',
            url: "{{route('office.product-category.get_list')}}",
            data: function (params) {
                var query = {
                    search: params.term
                }
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.title,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    
    $('#warranty_id').select2({
        placeholder: "Pilih Garansi",
        width:'100%',
        language: {
            // You can find all of the options in the language files provided in the
            // build. They all must be functions that return the string that should be
            // displayed.
            "searching": function(){
                return "Harap tunggu";
            },
            "noResults": function(){
                return "Data Tidak ditemukan";
            },
            "inputTooShort": function () {
                return "Anda harus memasukkan setidaknya 1 karakter";
            }
        },
        // minimumInputLength: 1,
        ajax: {
            method:'POST',
            url: "{{route('office.product-warranty.get_list')}}",
            data: function (params) {
                var query = {
                    search: params.term
                }
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name + ' | ' + item.duration + ' ' + item.duration_type,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    $('#business_location').select2({
        placeholder: "Pilih Lokasi Bisnis",
        width:'100%',
        language: {
            // You can find all of the options in the language files provided in the
            // build. They all must be functions that return the string that should be
            // displayed.
            "searching": function(){
                return "Harap tunggu";
            },
            "noResults": function(){
                return "Data Tidak ditemukan";
            },
            "inputTooShort": function () {
                return "Anda harus memasukkan setidaknya 1 karakter";
            }
        },
        // minimumInputLength: 1,
        ajax: {
            method:'POST',
            url: "{{route('office.business-location.get_list')}}",
            data: function (params) {
                var query = {
                    search: params.term
                }
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    obj_select("subcategory_id","Pilih Sub Kategori");
    $("#category_id").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.product-category.get_list_sub')}}",
            data: {category : $("#category_id").val()},
            success: function(response){
                $("#subcategory_id").html(response);
            }
        });
    });
</script>
{{-- <div class="modal-footer">
    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
</div> --}}