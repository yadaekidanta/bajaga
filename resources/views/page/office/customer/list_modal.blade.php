<div class="modal-header header-bg">
    <h2 class="">List Pelanggan</h2>
    <div class="btn btn-sm btn-icon btn-color-white btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y m-5">
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                <th class="min-w-125px">Kode pelanggan</th>
                <th class="min-w-125px">Nama Lengkap</th>
                <th class="min-w-125px">Email</th>
                <th class="min-w-125px">No Telpon</th>
                <th class="min-w-125px">Tipe</th>
                <th class="text-end min-w-70px">Aksi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @if ($collection->count() > 0)
                @foreach ($collection as $item)
                <tr>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->code}}</a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->name}}</a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->email}}</a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->phone}}</a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->type}}</a>
                    </td>
                    <td class="text-end">
                        <a href="javascript:;" onclick="getClient('{{$item->code}}');" class="menu-link px-3">Pilih</a>
                    </td>
                    {{-- <td>
                        {{$item->created_at->diffForHumans()}} <br>
                        {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}} <br>
                        Jam {{$item->created_at->isoFormat('h A')}}
                        @if($item->created_at->format('i') > 0 )
                            lebih {{$item->created_at->format('i')}} Menit
                        @endif
                    </td> --}}
                </tr>
                @endforeach
            @else
            <tr>
                <td colspan="5" class="text-center">
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">Tidak ada data</a>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
</div>