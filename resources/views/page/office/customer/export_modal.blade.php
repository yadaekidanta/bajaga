<div class="modal-header">
    <h2 class="fw-bolder">Export Customers</h2>
    <div id="kt_customers_export_close" class="btn btn-icon btn-sm btn-active-icon-primary">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
    <form id="kt_customers_export_form" class="form" action="#">
        <div class="fv-row mb-10">
            <label class="fs-5 fw-bold form-label mb-5">Select Date Range:</label>
            <input class="form-control form-control-solid" placeholder="Pick a date" name="date" />
        </div>
        <div class="fv-row mb-10">
            <label class="fs-5 fw-bold form-label mb-5">Select Export Format:</label>
            <select data-control="select2" data-placeholder="Select a format" data-hide-search="true" name="format" class="form-select form-select-solid">
                <option value="excell">Excel</option>
                <option value="pdf">PDF</option>
            </select>
        </div>
        <div class="text-center">
            <button type="reset" id="kt_customers_export_cancel" class="btn btn-light me-3">Discard</button>
            <button type="submit" id="kt_customers_export_submit" class="btn btn-primary">
                <span class="indicator-label">Submit</span>
                <span class="indicator-progress">Please wait...
                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
            </button>
        </div>
    </form>
</div>