<div class="content d-flex flex-column flex-column-fluid" style="margin-top:-5%;">
    <div class="post d-flex flex-column-fluid mt-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body pt-0 mt-5">
                            <h6 style="font-size:100%;">{{$purchaseReturn->code}}</h6>
                            <h6 style="font-size:100%;">{{$purchaseReturn->date->format("j F Y")}}</h6>
                            <h6 style="font-size:100%;">{{$purchaseReturn->purchase->supplier->name}}</h6>
                            <p>
                                {{$purchaseReturn->purchase->supplier->email}} <br>
                                {{$purchaseReturn->purchase->supplier->phone}} <br>
                                {{$purchaseReturn->purchase->supplier->address}}
                            </p>
                            <h6>Sub Total : {{number_format($purchaseReturn->subtotal_detil->subtotal)}}</h6>
                            <h6>Total Pembayaran : {{number_format($purchaseReturn->total_paid)}}</h6>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <a href="javascript:;" onclick="load_list(1);" class="btn btn-icon btn-primary me-1" title="Kembali">
                                    <i class="las la-undo fs-3"></i>
                                </a>
                                @if ($purchaseReturn->st == "Tertunda")
                                <a title="Perbarui" data-bs-toggle="tooltip" data-bs-placement="top" href="javascript:;" onclick="load_input('{{route('office.purchase-return.edit',$purchaseReturn->id)}}');" class="btn btn-icon btn-warning me-1"><i class="las la-edit fs-3"></i></a>
                                <a title="Hapus" data-bs-toggle="tooltip" data-bs-placement="top" href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('office.purchase-return.destroy',$purchaseReturn->id)}}');" class="btn btn-icon btn-danger me-1"><i class="las la-trash fs-3"></i></a>
                                <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('office.purchase-return.process',$purchaseReturn->id)}}');" class="btn btn-icon btn-success" title="Proses">
                                    <i class="las la-check fs-3"></i>
                                </a>
                                @elseif($purchaseReturn->st == "Diproses")
                                <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('office.purchase-return.delivered',$purchaseReturn->id)}}');" class="btn btn-icon btn-success" title="Diterima">
                                    <i class="las la-people-carry fs-3"></i>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card mt-3">
                        <div class="card-body pt-0 mt-5">
                            <h6 style="font-size:100%;">Tambah Pembayaran</h6>
                            <form id="form_input_pembayaran">
                                <div class="row">
                                    <div class="col-lg-12 p-0 mt-3">
                                        <div class="form-group">
                                            <label class="required fs-6 fw-bold mb-2">Akun</label>
                                            <div class="input-group">
                                                <input type="hidden" class="form-control" id="id_purchase_return" name="id_purchase_return" placeholder="Akun" value="{{$purchaseReturn->id}}">
                                                <input type="hidden" class="form-control" id="account_id" name="account_id" placeholder="Akun" readonly>
                                                <input type="text" class="form-control" id="account_name" name="account_name" placeholder="Akun" readonly>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text border-radius-custom">
                                                        <a href="javascript:;" onclick="handle_open_modal('{{route('office.account.list_modal')}}','#modalListResult','#contentListResult');">
                                                            <i class="las la-search"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                            {{-- <span class="form-text text-muted">Some help content goes here</span> --}}
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-0 mt-3">
                                        <label class="fs-6 fw-bold mb-2">Tanggal Pembayaran</label>
                                        <input type="text" class="form-control" id="payment_date" name="payment_date" readonly>
                                    </div>
                                    <div class="col-lg-12 p-0 mt-3">
                                        <label class="fs-6 fw-bold mb-2">Jumlah Pembayaran</label>
                                        <input type="text" class="form-control" id="paid_amount" name="paid_amount" placeholder="">
                                    </div>
                                    <div class="col-lg-12 p-0 mt-3">
                                        <label class="fs-6 fw-bold mb-2">Catatan</label>
                                        <textarea class="form-control" name="catatan" id="catatan"></textarea>
                                    </div>
                                    @if ($purchaseReturn->st != "Tertunda")
                                        @if ($purchaseReturn->payment_st == "Belum lunas")
                                        <button id="tombol_simpan_pembayaran" onclick="handle_save('#tombol_simpan_pembayaran','#form_input_pembayaran','{{route('office.purchase-return_payment.store')}}','POST');" class="btn btn-sm btn-success mt-5">Simpan Pembayaran</button>
                                        @endif
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-body pt-0 mt-5">
                            <form id="form_input_barang">
                                <div class="row">
                                    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Tambah Barang</h1>
                                    <div class="col-lg-4 mt-3">
                                        <div class="form-group">
                                            <label class="required fs-6 fw-bold mb-2">Kode Barang</label>
                                            <div class="input-group cursor-pointer">
                                                <div class="input-group-prepend hover-opacity-65" onclick="handle_open_modal('{{route('office.product.list_modal')}}','#modalListResult','#contentListResult');">
                                                    <span class="input-group-text border-radius-custom2">
                                                        <a href="javascript:;">
                                                            <i class="las la-search"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                                <input type="hidden" class="form-control" id="purchase_return_id" name="purchase_return_id" value="{{$purchaseReturn->id}}">
                                                <input type="hidden" class="form-control" id="barang" name="barang">
                                                <input type="text" class="form-control" id="code_product" onkeyup="if(event.keyCode == 13) getBarang(this.value); return false;" placeholder="Kode Barang">
                                                <div class="input-group-prepend hover-opacity-65 d-none" onclick="handle_open_modal('{{route('office.product.input_modal')}}','#modalListResult','#contentListResult');">
                                                    <span class="input-group-text border-radius-custom">
                                                        <a href="javascript:;">
                                                            <i class="las la-plus"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                            {{-- <span class="form-text text-muted">Some help content goes here</span> --}}
                                        </div>
                                    </div>
                                    <div class="col-lg-4 mt-3">
                                        <label class="fs-6 fw-bold mb-2">Nama Barang</label>
                                        <input type="text" class="form-control" id="name" placeholder="" readonly>
                                    </div>
                                    <div class="col-lg-4 mt-3">
                                        <label class="required fs-6 fw-bold mb-2">Harga Barang</label>
                                        <input type="tel" class="form-control" id="price" name="price" placeholder="">
                                    </div>
                                    <div class="col-lg-4 mt-3">
                                        <label class="required fs-6 fw-bold mb-2">Jumlah Barang</label>
                                        <input type="tel" class="form-control" id="qty" name="qty" placeholder="">
                                    </div>
                                    <div class="min-w-150px text-end">
                                        @if ($purchaseReturn->st == "Tertunda")
                                            <button id="tombol_simpan_barang" style="margin-top:-70px;" onclick="handle_save('#tombol_simpan_barang','#form_input_barang','{{route('office.purchase-return_detail.store')}}','POST');" class="btn btn-primary">Tambah barang</button>
                                        @else
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-body pt-0">
                            <h1 class="d-flex align-items-center text-dark fw-bolder my-1 mt-5 fs-3">Daftar Barang</h1>
                            <table class="table align-middle table-row-dashed fs-6 gy-5">
                                <thead>
                                    <tr>
                                        <th>Barang</th>
                                        <th>Harga Beli</th>
                                        <th>Jumlah</th>
                                        <th>Sub Total</th>
                                        @if ($purchaseReturn->st == "Tertunda")
                                        <th>Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($purchaseReturn->purchase_detil as $item)
                                    <tr>
                                        <td>
                                            @if($item->product_id)
                                            {{ $item->product->name }}
                                            @endif
                                        </td>
                                        <td>{{ number_format($item->price) }}</td>
                                        <td>{{ number_format($item->qty) }}</td>
                                        <td>{{ number_format($item->subtotal) }}</td>
                                        @if ($purchaseReturn->st == "Tertunda")
                                        <td>
                                            <a title="Hapus" data-bs-toggle="tooltip" data-bs-placement="top" href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('office.purchase-return_detail.destroy',$item->id)}}');" class="btn btn-icon btn-danger"><i class="las la-trash fs-2"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @if ($purchaseReturn->st != "Tertunda")
                    <div class="card mt-5">
                        <div class="card-body pt-0">
                            <h1 class="d-flex align-items-center text-dark fw-bolder my-1 mt-5 fs-3">Daftar Pembayaran</h1>
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5">
                                    <thead>
                                        <tr>
                                            <th>Akun</th>
                                            <th>Tanggal</th>
                                            <th>Jumlah</th>
                                            <th>Catatan</th>
                                            @if ($purchaseReturn->st == "Diproses")
                                            <th>Aksi</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($purchaseReturn->purchase_payment as $item)
                                        <tr>
                                            <td>
                                                @if($item->account_id)
                                                {{ $item->account->code }} - {{ $item->account->name }}
                                                @endif
                                            </td>
                                            <td>{{ $item->date->format('j F Y') }}</td>
                                            <td>{{ number_format($item->total) }}</td>
                                            <td>{{ $item->note }}</td>
                                            <td>
                                                <a title="Hapus" data-bs-toggle="tooltip" data-bs-placement="top" href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('office.purchase-return_payment.destroy',$item->id)}}');" class="btn btn-icon btn-danger"><i class="las la-trash fs-2"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    obj_date('payment_date');
    number_only('price');
    ribuan('price');
    number_only('qty');
    ribuan('qty');
    number_only('paid_amount');
    ribuan('paid_amount');
    function getBarang(code){
        $.post("{{route('office.product.get')}}", {code:code}, function(result) {
            if (result.alert == "success") {
                $("#barang").val(result.data.id);
                $("#code_product").val(result.data.sku);
                $("#name").val(result.data.name);
                $("#price").val(format_ribuan(result.data.price));
            }else{
                error_toastr(result.message);
            }
        }, "json");
    }
    function getAccount(id){
        $.post("{{route('office.account.get')}}", {id:id}, function(result) {
            if (result.alert == "success") {
                $("#account_id").val(result.data.id);
                $("#account_name").val(result.data.name);
            }else{
                error_toastr(result.message);
            }
        }, "json");
    }
</script>