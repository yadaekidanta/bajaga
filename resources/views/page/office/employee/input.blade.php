<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Pegawai
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">NIK</label>
                            <input type="text" class="form-control" name="nik" placeholder="Masukkan NIK..." value="{{$data->nik}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">No Telpon</label>
                            <input type="tel" class="form-control" name="phone" maxlength="15" placeholder="Masukkan No Telpon..." value="{{$data->phone}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Foto</label>
                            <input type="file" class="form-control" name="photo" maxlength="15" placeholder="Masukkan Foto..." value="{{$data->photo}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Masukkan Email..." value="{{$data->email}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Jabatan</label>
                            <select name="role" id="" class="form-control">
                                <option value="owner" {{$data->role=="owner"?"selected":""}}>Owner</option>
                                <option value="pegawai" {{$data->role=="pegawai"?"selected":""}}>Pegawai</option>
                            </select>
                        </div>
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.employee.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.employee.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>