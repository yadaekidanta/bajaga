<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr>
            <th>Kode Penjualan</th>
            <th>Tanggal</th>
            <th>Kode</th>
            <th>Status Transaksi</th>
            <th>Status Pembayaran</th>
            <th>Grand Total</th>
            <th>Total Pembayaran</th>
            <th>Dibuat oleh</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @if ($collection->count() > 0)
        @foreach($collection as $item)
        <tr>
            <td>{{ $item->sale->code }}</td>
            <td>{{ $item->created_at->format('j F Y') }}</td>
            <td>{{ $item->code }}</td>
            <td>{{ $item->st }}</td>
            <td>{{ $item->payment_st }}</td>
            <td>{{ number_format($item->grand_total) }}</td>
            <td>{{ number_format($item->total_paid) }}</td>
            <td>{{ $item->employee->name}}</td>
            <td>
                <a title="Detil Penjualan" data-bs-toggle="tooltip" data-bs-placement="top" href="javascript:;" onclick="load_input('{{route('office.sale-return.show',$item->id)}}');" class="btn btn-icon btn-info"><i class="las la-eye fs-2"></i></a>
                @if ($item->st == "Pending")
                <a title="Perbarui" data-bs-toggle="tooltip" data-bs-placement="top" href="javascript:;" onclick="load_input('{{route('office.sale-return.edit',$item->id)}}');" class="btn btn-icon btn-warning"><i class="las la-edit fs-2"></i></a>
                <a title="Hapus" data-bs-toggle="tooltip" data-bs-placement="top" href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('office.sale-return.destroy',$item->id)}}');" class="btn btn-icon btn-danger"><i class="las la-trash fs-2"></i></a>
                @endif
            </td>
        </tr>
        @endforeach
    @else
        <tr>
            <td colspan="10" class="text-center">Tidak Data</td>
        </tr>
    @endif
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}