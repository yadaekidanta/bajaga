<div class="modal-body">
    <h5 class="modal-title">Daftar Satuan</h5>
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                <th class="min-w-125px">Nama</th>
                <th class="text-end min-w-70px">Aksi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @foreach ($collection as $item)
            <tr>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->name}} | {{ $item->shortname }}</a>
                </td>
                <td class="text-end">
                    <a href="javascript:;" onclick="getUnit('{{$item->id}}');" class="menu-link px-3">
                        <i class="las la-check"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>