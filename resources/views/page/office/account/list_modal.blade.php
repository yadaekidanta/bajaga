<div class="modal-header">
    <h5 class="modal-title">Daftar Akun</h5>

    <!--begin::Close-->
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <i class="las la-times"></i>
    </div>
    <!--end::Close-->
</div>
<div class="modal-body">
    <div class="table-responsive">
        <table class="table align-middle table-row-dashed fs-6 gy-5">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>Tipe</th>
                    <th>Saldo</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            @if ($collection->count() > 0)
                @foreach($collection as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->account_type->parent_account->name }} - {{ $item->account_type->name }}</td>
                    <td>Rp {{ number_format($item->balance) }}</td>
                    <td>
                        <a href="javascript:;" onclick="getAccount('{{$item->id}}');" class="btn btn-icon btn-success" data-bs-dismiss="modal">
                            <i class="las la-check"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7" class="text-center">Tidak ada Data</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    {{-- {{$collection->links('theme.office.pagination')}} --}}
</div>
{{-- <div class="modal-footer">
    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
</div> --}}