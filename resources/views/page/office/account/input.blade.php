<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Akun
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Kode</label>
                            <input type="text" class="form-control" name="code" maxlength="20" placeholder="Masukkan Kode..." value="{{$data->code}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Tipe Akun</label>
                            <select class="form-select" id="account_type" name="account_type">
                                <option SELECTED DISABLED>Pilih Tipe Akun</option>
                                @foreach ($account_type as $item)
                                    <option value="{{$item->id}}" {{$data->account_type_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                    @foreach ($item->account_type as $type)
                                        <option value="{{$type->id}}" {{$data->account_type_id == $type->id ? 'selected' : ''}}>-- {{$type->name}}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6">
                            @if(!$data->id)
                            <label class="required fs-6 fw-bold mb-2">Saldo</label>
                            <input type="tel" class="form-control" id="balance" name="balance" placeholder="Masukkan Saldo..." value="{{number_format($data->balance)}}">
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Label</label>
                            <input type="text" class="form-control" name="label" placeholder="Masukkan Label..." value="{{$data->label}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Isi</label>
                            <input type="text" class="form-control" name="value" placeholder="Masukkan Isi..." value="{{$data->value}}">
                        </div>
                        <div class="col-lg-12">
                            <label class="fs-6 fw-bold mb-2">Catatan</label>
                            <textarea class="form-control" name="note">{{$data->note}}</textarea>
                        </div>
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.account.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.account.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    obj_select('account_type','Pilih Tipe Akun');
    number_only('balance');
    ribuan('balance');
</script>