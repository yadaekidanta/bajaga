<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Produk
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">SKU</label>
                            <input type="text" class="form-control" name="sku" readonly value="{{$product->sku}}">
                            <input type="hidden" class="form-control" name="condition" readonly value="{{$product->condition}}">
                            <input type="hidden" class="form-control" name="product_unit_id" readonly value="{{$product->product_unit_id}}">
                            <input type="hidden" class="form-control" name="product_brand_id" readonly value="{{$product->product_brand_id}}">
                            <input type="hidden" class="form-control" name="product_category_id" readonly value="{{$product->product_category_id}}">
                            <input type="hidden" class="form-control" name="product_subcategory_id" readonly value="{{$product->product_subcategory_id}}">
                            <input type="hidden" class="form-control" name="desc" readonly value="{{$product->desc}}">
                            <input type="hidden" class="form-control" name="weight" readonly value="{{$product->weight}}">
                            <input type="hidden" class="form-control" name="video_url" readonly value="{{$product->video_url}}">
                            <input type="hidden" class="form-control" name="brocure" readonly value="{{$product->brocure}}">
                            <input type="hidden" class="form-control" name="product_warranty_id" readonly value="{{$product->product_warranty_id}}">
                            <input type="hidden" class="form-control" name="business_location" readonly value="{{$product->business_location_id}}">
                            <input type="hidden" class="form-control" name="alert_quantity" readonly value="{{$product->alert_quantity}}">
                            <input type="hidden" class="form-control" name="custom_field_1" readonly value="{{$product->custom_field_1}}">
                            <input type="hidden" class="form-control" name="custom_field_2" readonly value="{{$product->custom_field_2}}">
                            <input type="hidden" class="form-control" name="custom_field_3" readonly value="{{$product->custom_field_3}}">
                            <input type="hidden" class="form-control" name="custom_field_4" readonly value="{{$product->custom_field_4}}">
                        </div>
                        <div class="col-lg-4 mb-5">
                            <label for="condition" class="required form-label">Harga Produk</label>
                            <input type="text" class="form-control" name="price" placeholder="Harga Produk...." value="{{$data->price}}">
                        </div>
                        <div class="col-lg-4 mb-5">
                            <label for="condition" class="required form-label">Stok</label>
                            <input type="text" class="form-control" name="stock" placeholder="Stok...." value="{{$data->stock}}">
                        </div>
                        <div class="col-lg-4 mb-5">
                            <label for="condition" class="required form-label">Gambar Produk</label>
                            <input type="file" class="form-control" name="photo" placeholder="Gambar Produk...." value="{{$data->photo}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.product-varian.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.product-varian.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>