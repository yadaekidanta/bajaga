<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Supplier
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Kode Lokasi</label>
                            <input type="text" class="form-control" name="code" placeholder="Masukkan kode lokasi..." value="{{$data->code}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan Nama..." value="{{$data->name}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">No HP</label>
                            <input type="tel" class="form-control" name="phone" maxlength="15" placeholder="Masukkan No HP..." value="{{$data->phone}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Masukkan Email..." value="{{$data->email}}">
                        </div>
                        <div class="col-lg-12">
                            <label class="required fs-6 fw-bold mb-2">Alamat</label>
                            <textarea type="text" class="form-control" name="address" id="address" placeholder="Masukkan Alamat..." >{{$data->address}}</textarea>
                        </div>
                        <div class="col-lg-4">
                            <label for="province">Provinsi</label>
                            <select class="form-control" name="province_id" id="province" class="form-control" required></select>                
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Kota</label>
                            <select class="form-control" id="city" name="city_id"></select>
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Kode Pos</label>
                            <input type="tel" class="form-control" maxlength="6" name="postcode" placeholder="Masukkan Kode Pos..." value="{{$data->postcode}}">
                        </div>
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.business-location.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.business-location.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    obj_select('province');
    obj_select('city');
    obj_autosize('address');
    get_province();
    function get_province(){
        $.get('{{route('office.regional.province')}}', {}, function(result) {
            $("#province").html(result);
        }, "html");
    }
    $("#province").change(function(){
        $.get('{{route('office.regional.city')}}', {province : $("#province").val()}, function(result) {
            $("#city").html(result);
        }, "html");
    });
</script>