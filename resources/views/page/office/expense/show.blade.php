<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        @if ($data->id)
        <div class="card mt-3">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        Data pembayaran {{$data->code}}
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <table class="table align-middle table-row-dashed fs-6 gy-5">
                    <thead>
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                            <th class="min-w-125px">Tanggal</th>
                            <th class="min-w-125px">Akun</th>
                            <th class="min-w-125px">Jumlah Pembayaran</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($data->expense_payment->sum('amount')>0)
                            @foreach ($data->expense_payment as $item)
                            <tr>
                                <td>{{$item->date->isoFormat('dddd,')}} {{$item->date->format('j F Y')}} <br></td>
                                <td>{{$item->account->account_type->name}} - {{$item->account->name}}</td>
                                <td>{{number_format($item->amount)}}</td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>
</div>