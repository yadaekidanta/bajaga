<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            @if ($data->id)
            <div class="col-6">
            @else
            <div class="col-12">
            @endif
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h6>
                                @if ($data->id)
                                    Ubah
                                @else
                                    Tambah
                                @endif
                                data Pengeluaran
                            </h6>
                        </div>
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end">
                                <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form id="form_input">
                            @if ($data->id)
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label class="required fs-6 fw-bold mb-2">Jenis Pengeluaran</label>
                                        <select data-control="select2" data-placeholder="Pilih Jenis Pengeluaran" id="expense_category_id" name="expense_category_id" class="form-select form-select-solid">
                                            <option value="" selected disabled>Pilih Jenis Pengeluaran</option>
                                            @foreach($category as $ctgr)
                                            <option value="{{$ctgr->id}}" {{$ctgr->id == $data->expense_category_id ? 'selected="selected"' : ''}}>{{$ctgr->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                                        <input type="text" class="form-control" id="date" name="date" placeholder="Masukkan Tanggal..." value="{{$data->date}}">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="required fs-6 fw-bold mb-2">Jumlah</label>
                                        <input type="tel" class="form-control" id="amount" name="amount" placeholder="Masukkan Jumlah..." value="{{number_format($data->amount)}}">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="fs-6 fw-bold mb-2">Dokumen</label>
                                        <div class="input-group">
                                            <input type="file" class="form-control" name="document">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <a href="{{route('office.expense.download',$data->id)}}" title="Unduh">
                                                        <i class="las la-download"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <label class="fs-6 fw-bold mb-2">Catatan</label>
                                        <textarea class="form-control" id="note" name="note">{{$data->note}}</textarea>
                                    </div>
                                    <div class="min-w-150px mt-10 text-end">
                                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.expense.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label class="required fs-6 fw-bold mb-2">Jenis Pengeluaran</label>
                                        <select data-control="select2" data-placeholder="Pilih Jenis Pengeluaran" id="expense_category_id" name="expense_category_id" class="form-select form-select-solid">
                                            <option value="" selected disabled>Pilih Jenis Pengeluaran</option>
                                            @foreach($category as $ctgr)
                                            <option value="{{$ctgr->id}}">{{$ctgr->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                                        <input type="text" class="form-control" id="date" name="date" placeholder="Masukkan Tanggal..." value="{{$data->date}}">
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="required fs-6 fw-bold mb-2">Jumlah</label>
                                        <input type="tel" class="form-control" id="amount" name="amount" placeholder="Masukkan Jumlah..." value="{{number_format($data->amount)}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label class="fs-6 fw-bold mb-2">Dokumen</label>
                                        <div class="input-group">
                                            <input type="file" class="form-control" name="document">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <label class="fs-6 fw-bold mb-2">Catatan</label>
                                        <textarea class="form-control" id="note" name="note">{{$data->note}}</textarea>
                                    </div>
                                    <div class="min-w-150px mt-10 text-end">
                                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.expense.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                                    </div>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
            @if ($data->id)
            <div class="col-6">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h6>
                                Tambah pembayaran {{$data->code}}
                            </h6>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form id="form_input_payment">
                            <div class="row">
                                <input name="expense_payment_id" value="{{$data->id}}" type="hidden">
                                <div class="col-lg-12">
                                    <label class="required fs-6 fw-bold mb-2">Akun</label>
                                    <select data-control="select2" data-placeholder="Pilih Akun" id="account_id" name="account_id" class="form-select form-select-solid"></select>
                                </div>
                                <div class="col-lg-12">
                                    <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                                    <input type="text" class="form-control" id="payment_date" name="payment_date" placeholder="Masukkan Tanggal...">
                                </div>
                                <div class="col-lg-12">
                                    <label class="required fs-6 fw-bold mb-2">Jumlah</label>
                                    <input type="tel" class="form-control" id="paid_amount" name="paid_amount" placeholder="Masukkan Jumlah...">
                                </div>
                                <div class="min-w-150px mt-10 text-end">
                                    <button id="tombol_simpan_payment" onclick="handle_upload('#tombol_simpan_payment','#form_input_payment','{{route('office.expense-payment.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 gy-5">
                                <thead>
                                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                        <th class="min-w-125px">Tanggal</th>
                                        <th class="min-w-125px">Akun</th>
                                        <th class="min-w-125px">Jumlah Pembayaran</th>
                                        <th class="text-end min-w-70px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data->expense_payment as $item)
                                    <tr>
                                        <td>{{$item->date->isoFormat('dddd,')}} {{$item->date->format('j F Y')}} <br></td>
                                        <td>{{$item->account->account_type->name}} - {{$item->account->name}}</td>
                                        <td>Rp. {{number_format($item->amount)}}</td>
                                        <td>
                                            <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('office.expense-payment.destroy',$item->id)}}');" class="menu-link px-3">Hapus</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<script>
    obj_date('date');
    obj_date('payment_date');
    number_only('amount');
    ribuan('amount');
    number_only('paid_amount');
    ribuan('paid_amount');
    obj_autosize('note');
    obj_select('expense_category_id','Pilih Jenis Pengeluaran');
    obj_select('account_id','Pilih ');
    @if($data->id)
        get_account();
        function get_account(){
            $.post('{{route('office.account.get_list')}}', {}, function(result) {
                $("#account_id").html(result);
            }, "html");
        }
    @endif
</script>