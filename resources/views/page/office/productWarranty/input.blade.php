<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Garansi
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Deskripsi</label>
                            <input type="text" class="form-control" name="description" placeholder="Masukkan Deskripsi..." value="{{$data->description}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Durasi</label>
                            <input type="tel" class="form-control" maxlength="4" name="duration" id="duration" placeholder="Masukkan Durasi..." value="{{$data->duration}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Tipe Durasi</label>
                            <select name="duration_type" class="form-control">
                                <option value="Hari" {{$data->duration_type=="Hari"?"selected":""}}>Hari</option>
                                <option value="Bulan" {{$data->duration_type=="Bulan"?"selected":""}}>Bulan</option>
                                <option value="Tahun" {{$data->duration_type=="Tahun"?"selected":""}}>Tahun</option>
                            </select>
                        </div>
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.product-warranty.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.product-warranty.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    number_only('duration');
</script>