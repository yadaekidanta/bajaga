<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr>
            <th class="text-center">Total Paid</th>
            <th class="text-center">Payment Status</th>
            <th class="text-center">date</th>
            <th class="text-center">Created By</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($collection as $item)
            <tr>
                <td class="text-center">Rp {{number_format($item->total_paid)}}</td>
                <td class="text-center">{{$item->payment_st}}</td>
                <td class="text-center">{{$item->date->format('j F Y')}}</td>
                <td class="text-center">{{$item->employee->name}}</td>
            </tr>
        @endforeach

    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}