<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data Diskon
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <form id="form_input">
                <div class="card-body pt-0">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Kode</label>
                            <input type="text" name="code" id="code" value="{{$data->code}}" class="form-control" placeholder="Masukan Kode">
                        </div>

                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" name="name" id="name" value="{{$data->name}}" class="form-control" placeholder="Masukan Nama">
                        </div>
                        
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Deskripsi</label>
                            <input type="text" name="desc" id="desc" value="{{$data->desc}}" class="form-control" placeholder="Masukan Deskripsi">
                        </div>
                       
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Tipe</label>
                            <select name="type" id="type" class="form-control">
                                <option value="" selected disabled>Pilih Tipe</option>
                                <option value="persentase" {{$data->type == "persentase" ? 'selected' : ''}}>Persentase</option>
                                <option value="harga" {{$data->type == "harga" ? 'selected' : ''}}>Harga</option>
                            </select>
                        </div>

                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Nilai</label>
                            <input type="tel" name="nilai" id="nilai" value="{{$data->type == "persentase" ? number_format($data->persentase) : number_format($data->price)}}" class="form-control" placeholder="Masukan Nilai">
                        </div>

                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Masa berlaku awal</label>
                            <input type="text" readonly name="start" id="start" value="{{$data->start_at}}" class="form-control" placeholder="Masukan Masa berlaku awal">
                        </div>

                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Masa berlaku akhir</label>
                            <input type="text" readonly name="end" id="end" value="{{$data->end_at}}" class="form-control" placeholder="Masukan Masa berlaku akhir">
                        </div>

                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Status</label>
                            <select name="st" id="st" class="form-control">
                                <option value="" selected disabled>Pilih Status</option>
                                <option value="aktif" {{$data->st == "aktif" ? 'selected' : ''}}>Aktif</option>
                                <option value="kadaluarsa" {{$data->st == "kadaluarsa" ? 'selected' : ''}}>Kadaluarsa</option>
                                <option value="belum aktif" {{$data->st == "belum aktif" ? 'selected' : ''}}>Belum Aktif</option>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2" for="thumbnail">Thumbnail</label>
                            <input type="file" name="thumbnail" id="thumbnail" value="{{$data->thumbnail}}" class="form-control" placeholder="Pilih thumbnail">
                        </div>

                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.discount.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.discount.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    obj_date_time('start');
    obj_date_time('end');
    ribuan('nilai');
    number_only('nilai');
</script>