<header id="header" class="full-header header-size-md">
    <div id="header-wrap">
        <div class="container">
            <div class="header-row justify-content-lg-between">
                <!-- Logo ============================================= -->
                <div id="logo" class="me-lg-4">
                    <a href="{{route('web.home')}}" class="standard-logo">
                        <img src="{{asset('img/favicon.png')}}" alt="{{config('app.name')}}">
                    </a>
                    <a href="{{route('web.home')}}" class="retina-logo">
                        <img src="{{asset('img/favicon.png')}}" alt="{{config('app.name')}}">
                    </a>
                </div>
                <!-- #logo end -->
                <div class="header-misc">
                    <!-- Top Search ============================================= -->
                    @guest
                    <div id="top-account">
                        <a href="#modal-auth" data-lightbox="inline" ><i class="icon-line2-user me-1 position-relative" style="top: 1px;"></i><span class="d-none d-sm-inline-block font-primary fw-medium">Masuk / Daftar</span></a>
                    </div>
                    @endguest
                    <!-- #top-search end -->
                    <!-- Top Cart ============================================= -->
                    @auth
                    <div id="top-account">
                        <div class="dropdown mx-3 me-lg-0">
                            <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="icon-user"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item text-start" href="{{route('web.profile')}}">Profil</a>
                                @if(\App\Models\UserStore::where('users_id',Auth::user()->id)->first())
                                <a class="dropdown-item text-start" href="{{route('web.product.index')}}">Lihat Toko</a>
                                @else
                                <a class="dropdown-item text-start" href="{{route('web.open-store.index')}}">Buka Toko</a>
                                @endif
                                <a class="dropdown-item text-start" href="{{route('web.store.favorite')}}">Toko Favorit</a>
                                <a class="dropdown-item text-start" href="{{route('web.transaction.checkout')}}">Transaksi Saya</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-start" href="{{route('web.auth.logout')}}">Keluar <i class="icon-signout"></i></a>
                            </ul>
                        </div>
                    </div>
                    <div id="top-cart" class="header-misc-icon d-none d-sm-block">
                        <a href="javascript:;" onclick="tombol_cart();" id="top-cart-trigger">
                            <i class="icon-line-bag"></i>
                            <span class="top-cart-number"></span>
                        </a>
                        <div class="top-cart-content w-22em">
                            <div class="top-cart-title">
                                <h4>Keranjang Belanja</h4>
                            </div>
                            <div class="top-cart-items" style="height: 200px;overflow-y: scroll;"></div>
                            <div class="top-cart-action">
                                <span class="top-checkout-price" style="font-size: 100%;"></span>
                                <a href="{{route('web.cart.detail')}}" class="button button-small m-0" style="font-size: 60%;">Lihat Keranjang</a>
                            </div>
                        </div>
                    </div>
                    <div id="top-notification" class="header-misc-icon d-none d-sm-block">
                        <a href="javascript:;" onclick="tombol_notif();" id="top-notification-trigger">
                            <i class="icon-bell"></i>
                            <span id="top-notification-number" class="top-notification-number"></span>
                        </a>
                        <div class="top-notification-content w-22em">
                            <div class="top-notification-title">
                                <h4>Notifikasi</h4>
                            </div>
                            <div id="notification_items" class="top-notification-items" style="height: 200px;overflow-y: scroll;"></div>
                            {{-- <div class="top-notification-action">
                                <a href="javascript:;" class="button button-3d button-small m-0" style="font-size: 60%;">Lihat Selengkapnya</a>
                            </div> --}}
                        </div>
                    </div>

                    <div id="top-notification" class="header-misc-icon d-none d-sm-block">
                        <a href="{{route('web.chat')}}" id="top-notification-trigger">
                            <svg width="24" height="24" y="24" xmlns="http://www.w3.org/2000/svg"><defs><path d="M12 14l10-4v8a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2v-8l10 4zm0-2L2 8V6c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v2l-10 4z" id="c"/></defs><g fill="none" fill-rule="evenodd"><path d="M0 0h24v24H0z"/><mask id="d" fill="#fff"><use xlink:href="#c"/></mask><use fill="#9FA6B0" xlink:href="#c"/><g mask="url(#d)" fill="#6C727C" fill-rule="nonzero"><path d="M1 1h22v22H1z"/></g></g></svg>
                            <span id="top-chat-number" class="top-chat-number"></span>
                        </a>
                        <div class="top-notification-content w-22em">
                            <div class="top-notification-title">
                                <h4>Notifikasi</h4>
                            </div>
                            <div id="notification_items" class="top-notification-items" style="height: 200px;overflow-y: scroll;"></div>
                            {{-- <div class="top-notification-action">
                                <a href="javascript:;" class="button button-3d button-small m-0" style="font-size: 60%;">Lihat Selengkapnya</a>
                            </div> --}}
                        </div>
                    </div>

                    @endauth
                    <!-- #top-cart end -->
                    <!-- Top Search ============================================= -->
                    {{-- <div id="top-search" class="header-misc-icon">
                        <a href="javascript:;" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
                    </div> --}}
                    <!-- #top-search end -->
                </div>
                <div id="primary-menu-trigger">
                    <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                </div>
                <!-- Primary Navigation ============================================= -->
                <nav class="primary-menu with-arrows me-lg-auto">
                    <ul class="menu-container">
                        <li class="menu-item"><a class="menu-link" href="{{ route('web.home') }}"><div>Home</div></a></li>
                        <li class="menu-item"><a class="menu-link" href="{{ route('web.auction') }}"><div>Lelang</div></a></li>
                        <li class="menu-item mega-menu">
                            <a class="menu-link" href="javascript:;">
                                <div>Kategori</div>
                            </a>
                            <div class="mega-menu-content mega-menu-style-2">
                                <div class="container">
                                    <div class="row">
                                        @php
                                            $category = \App\Models\ProductCategory::where('product_category_id','=','0')->where('show_on_home','=','y')->limit(3)->get();
                                        @endphp

                                        @foreach ($category as $item)
                                        <ul class="sub-menu-container mega-menu-column border-start-0 col-lg-1-5">
                                            <li class="menu-item mega-menu-title">
                                                <a class="menu-link" href="{{route('web.category.show_brand',[$item->slug])}}">
                                                    <div>{{$item->title}}</div>
                                                </a>
                                                <ul class="sub-menu-container">
                                                    @foreach ($item->subcategory as $subcategory)
                                                    <li class="menu-item">
                                                        <a class="menu-link" href="{{route('web.category.show_brand',[$subcategory->slug])}}">
                                                            <div>{{$subcategory->title}}</div>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                    @if ($item->subcategory->count()>0)
                                                    <li class="menu-item">
                                                        <a class="menu-link" href="{{route('web.category.show_brand',[$subcategory->slug])}}">
                                                            <div>Lihat Semua <i class="icon-angle-right"></i></div>
                                                        </a>
                                                    </li>
                                                    @endif
                                                </ul>
                                            </li>
                                        </ul>
                                        @endforeach

                                        <ul class="sub-menu-container mega-menu-column col-lg-1-5 border-start-0">
                                            <li class="card p-0 bg-transparent border-0 mega-menu-title">
                                                <a class="menu-link" href="javascript:;">
                                                    <div>Brand Pilihan</div>
                                                </a>

                                                <ul class="clients-grid grid-6 grid-sm-3 grid-md-4 grid-lg-4 mb-0">
                                                    @php
                                                    $brand = \App\Models\ProductBrand::limit(8)->get();
                                                    @endphp
                                                    @foreach ($brand as $item)
                                                    <li class="grid-item">
                                                        <a href="{{route('web.category.show_brand',[$item->slug])}}"><img src="{{$item->image}}" alt="Brand"></a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="menu-item w-33em ms-3">
                            <form id="form-search" class="m-0">
                                <div class="input-group input-group-sm">
                                        <input type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" id="search-input-header" aria-describedby="button-addon2">
                                        <button class="btn btn-outline-secondary box-shadow-none" onclick="search_header()" type="button" id="button-addon2">
                                            <i class="icon-line-search"></i>
                                        </button>
                                </div>
                            </form>
                        </li>
                    </ul>
                </nav>
                <!-- #primary-menu end -->
                {{-- <form class="top-search-form" id="search_filter" action="{{route('web.searching')}}">
                    <div class="input-group mb-3">
                        <input type="text" name="keyword" class="form-control" value="" placeholder="Cari barang impianmu..." autocomplete="off">
                        <button type="submit" class="btn btn-outline-secondary" type="button" id="button-addon2">Button</button>
                    </div>
                </form> --}}
            </div>
        </div>
    </div>
    <div class="header-wrap-clone"></div>
</header>
