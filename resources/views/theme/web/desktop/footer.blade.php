<footer id="footer" class="bg-transparent border-0">
    <div class="container clearfix">
        <!-- Footer Widgets ============================================= -->
        <div class="footer-widgets-wrap pb-3 border-bottom clearfix">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-6">
                    <div class="widget clearfix">
                        <h4 class="ls0 mb-3 nott">Bajaga</h4>
                        <ul class="list-unstyled iconlist ms-0">
                            <li><a href="{{route('web.about')}}">Tentang Bajaga</a></li>
                            <li><a href="javascript:;">Hak Kekayaan Intelektual</a></li>
                            <li><a href="javascript:;">BajagaPoints</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6 d-none">
                    <div class="widget clearfix">
                        <h4 class="ls0 mb-3 nott">Beli</h4>
                        <ul class="list-unstyled iconlist ms-0">
                            <li><a href="javascript:;">Barang</a></li>
                        </ul>
                    </div>
                    <div class="widget clearfix">
                        <h4 class="ls0 mb-3 nott">Jual</h4>
                        <ul class="list-unstyled iconlist ms-0">
                            <li><a href="javascript:;">Barang</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-2">
                    <div class="widget clearfix">
                        <h4 class="ls0 mb-3 nott">Bantuan dan Panduan</h4>
                        <ul class="list-unstyled iconlist ms-0">
                            <li><a href="{{route('web.help')}}">Bajaga Care</a></li>
                            <li><a href="{{route('web.terms')}}">Syarat dan Ketentuan</a></li>
                            <li><a href="{{route('web.privacy')}}">Kebijakan Privasi</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="widget clearfix">
                        <img src="{{asset('img/footer.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- .footer-widgets-wrap end -->
    </div>
    <!-- Copyrights ============================================= -->
    <div id="copyrights" class="bg-transparent">
        <div class="container clearfix">
            <div class="row justify-content-between align-items-center">
                <div class="col-md-6">
                    &copy; 2022, {{config('app.name')}}.
                    {{-- <br> --}}
                    {{-- <div class="copyright-links"><a href="javascript:;">Terms of Use</a> / <a href="javascript:;">Privacy Policy</a></div> --}}
                </div>
                {{-- <div class="col-md-6 d-md-flex flex-md-column align-items-md-end mt-4 mt-md-0">
                    <div class="copyrights-menu copyright-links clearfix">
                        <a href="javascript:;">About</a>/<a href="javascript:;">Features</a>/<a href="javascript:;">FAQs</a>/<a href="javascript:;">Contact</a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    <!-- #copyrights end -->
</footer>