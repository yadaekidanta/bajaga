@php
$banner = \App\Models\Banner::get();
@endphp
@if ($banner->count()>0)
<section id="slider" class="slider-element swiper_wrapper" data-autoplay="6000" data-speed="800" data-loop="true" data-grab="true" data-effect="fade" data-arrow="false" style="height: 600px;">
    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">            
            @foreach ($banner as $item)
            <div class="swiper-slide dark">
                <div class="container">
                    <div class="slider-caption slider-caption-center">
                        {{-- <div>
                            <h2 class="bottommargin-sm text-white">New Arrivals / 18</h2>
                            <a href="{{$item->url}}" class="button bg-white text-dark button-light">Shop Womenswear</a>
                        </div> --}}
                    </div>
                </div>
                <a href="{{$item->url}}"><div class="swiper-slide-bg" style="background-image: url('{{$item->image}}'); background-position: center 40%;"></div></a>
            </div>
            @endforeach
        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>
@endif