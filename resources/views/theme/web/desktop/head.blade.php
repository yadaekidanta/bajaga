<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="{{config('app.name')}}" />
	<meta name="keyword" content="{{$keyword ?? config('app.name')}}" />
	<!-- Stylesheets ============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Montserrat:300,400,500,600,700|Merriweather:300,400,300i,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/swiper.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/components/select-boxes.css')}}" type="text/css" />
	<!-- shop Demo Specific Stylesheet -->
	<link rel="stylesheet" href="{{asset('semicolon/shop/shop.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/shop/css/fonts.css')}}" type="text/css" />
	<!-- / -->
	<link rel="stylesheet" href="{{asset('semicolon/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/magnific-popup.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/custom.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/components/bs-rating.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/toastr.css')}}" type="text/css" />

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="{{asset('semicolon/css/colors.php?color=000000')}}" type="text/css" />
	<!-- Document Title ============================================= -->
	<title>{{config('app.name') . ': ' .$title ?? config('app.name')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" />
	<style>
	.product img {
		width:  300px;
		height: 300px;
		object-fit: cover;
	}
	label.custom_radio {
	width: 100%;
	font-size: 1rem;
	}
	
	.card-input-element+.card {
		height: calc(36px + 2*1rem);
		color: var(--primary);
		-webkit-box-shadow: none;
		box-shadow: none;
		border: 2px solid transparent;
		border-radius: 4px;
	}
	
	.card-input-element+.card:hover {
		cursor: pointer;
	}
	
	.card-input-element:checked+.card {
		border: 2px solid var(--primary);
		-webkit-transition: border .3s;
		-o-transition: border .3s;
		transition: border .3s;
  	}
  
	  .card-input-element:checked+.card::after {
		content: "\e92b";
		color: #AFB8EA;
		font-family: "lined-icons";
		font-size: 24px;
		-webkit-animation-name: fadeInCheckbox;
		animation-name: fadeInCheckbox;
		-webkit-animation-duration: .5s;
		animation-duration: .5s;
		-webkit-animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
		animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
	}
  
	@-webkit-keyframes fadeInCheckbox {
		from {
		opacity: 0;
		-webkit-transform: rotateZ(-20deg);
		}
		to {
		opacity: 1;
		-webkit-transform: rotateZ(0deg);
		}
	}
	
	@keyframes fadeInCheckbox {
		from {
		opacity: 0;
		transform: rotateZ(-20deg);
		}
		to {
		opacity: 1;
		transform: rotateZ(0deg);
		}
	}
	</style>
</head>