@php
$uri = request()->path();

@endphp
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
@include('theme.web.desktop.head')
@yield('custom_css')
<body class="stretched {{$uri == 'store/paystore' ? 'overflow-hidden' : ''}}">
	<!-- Document Wrapper ============================================= -->
	<div id="wrapper" class="clearfix">
        @include('theme.web.desktop.modal')
		<!-- Top Bar ============================================= -->
		@include('theme.web.desktop.topbar')
		<!-- Header ============================================= -->
		@include('theme.web.desktop.header')
        <!-- #header end -->
		<!-- Slider ============================================= -->
		@if (request()->is('/'))
		@include('theme.web.desktop.slider')
		@endif
        <!-- #Slider End -->
		<!-- Content ============================================= -->
		{{$slot}}
        <!-- #content end -->
		<!-- Footer ============================================= -->
		@if (!preg_match("/product\/.*|store\/history\/transaction|store\/settings|profile|chat|transaction\/checkout|store\/paystore|store\/review\/menu|product|store\/discuss\/menu/", $uri))
			@include('theme.web.desktop.footer');
		@endif
        <!-- #footer end -->
	</div>
    <!-- #wrapper end -->
	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-line-arrow-up"></div>
	<!-- JavaScripts ============================================= -->
	@include('theme.web.desktop.js')
	@yield('custom_js')
	@yield('custom_js_auth_modal')
</body>
</html>