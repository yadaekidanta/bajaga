<div id="top-bar" class="dark" style="background-color: #a3a5a7;">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-12 col-lg-auto">
                <p class="mb-0 d-flex justify-content-center justify-content-lg-start py-3 py-lg-0">
                    <strong></strong>
                </p>
            </div>
            <div class="col-12 col-lg-auto d-none d-lg-flex">
                <!-- Top Links ============================================= -->
                <div class="top-links">
                    <ul class="top-links-container">
                        <li class="top-links-item"><a href="{{route('web.about')}}">Tentang Bajaga</a></li>
                        <li class="top-links-item"><a href="{{route('web.help')}}">Bajaga Care</a></li>
                        <li class="top-links-item">
                            <a href="javascript:;">ID</a>
                            <ul class="top-links-sub-menu">
                                <li class="top-links-item"><a href="javascript:;"><img src="{{asset('img/flags/indonesia.svg')}}" alt="Indonesia"> ID</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- .top-links end -->
                <!-- Top Social ============================================= -->
                <ul id="top-social" class="d-none">
                    <li><a href="javascript:;" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                    <li><a href="javascript:;" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                    <li><a href="tel:+1.11.85412542" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+1.11.85412542</span></a></li>
                    <li><a href="mailto:info@canvas.com" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">info@canvas.com</span></a></li>
                </ul>
                <!-- #top-social end -->
            </div>
        </div>
    </div>
</div>