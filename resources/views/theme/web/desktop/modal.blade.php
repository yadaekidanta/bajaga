<div class="modal fade" id="modalListResult" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="contentListResult"></div>
    </div>
</div>
{{-- <div class="modal-on-load" data-target="#myModal1"></div> --}}

<!-- On LOad Modal -->
<div class="modal1 mfp-hide subscribe-widget mx-auto" id="myModal1" style="max-width: 750px;">
    <div class="row justify-content-center bg-white align-items-center" style="min-height: 380px;">
        <div class="col-md-5 p-0">
            <div style="background: url('images/modals/modal1.jpg') no-repeat center right; background-size: cover;  min-height: 380px;"></div>
        </div>
        <div class="col-md-7 bg-white p-4">
            <div class="heading-block border-bottom-0 mb-3">
                <h3 class="font-secondary nott ">Join Our Newsletter &amp; Get <span class="text-danger">40%</span> Off your First Order</h3>
                <span>Get Latest Fashion Updates &amp; Offers</span>
            </div>
            <div class="widget-subscribe-form-result"></div>
            <form class="widget-subscribe-form2 mb-2" action="include/subscribe.php" method="post">
                <input type="email" id="widget-subscribe-form2-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email Address..">
                <div class="d-flex justify-content-between align-items-center mt-1">
                    <button class="button button-dark  bg-dark text-white ms-0" type="submit">Subscribe</button>
                    <a href="javascript:;" class="btn-link" onClick="$.magnificPopup.close();return false;">Don't Show me</a>
                </div>
            </form>
            <small class="mb-0 fst-italic text-black-50">*We also hate Spam &amp; Junk Emails.</small>
        </div>
    </div>
</div>

<!-- Login Modal -->
<div class="modal1 mfp-hide" id="modal-auth">
    <div class="card mx-auto" style="max-width: 540px;">
        <div class="card-header py-3 bg-transparent center">
            <h3 class="mb-0 fw-normal" id="title_auth"></h3>
        </div>
        <div class="card-body mx-auto py-5" style="max-width: 70%;">
            <div id="main_auth">
                <a href="javascript:;" onclick="content_auth('mail_login','Masuk dengan email');" class="button button-large w-100 si-colored si-facebook nott fw-normal ls0 center m-0 mb-3">
                    <i class="icon-envelope"></i> Masuk dengan email
                </a>
                <a href="javascript:;" onclick="content_auth('phone_login','Masuk dengan no HP');" class="button button-large w-100 si-colored si-instagram nott fw-normal ls0 center m-0 mb-3">
                    <i class="icon-phone"></i> Masuk dengan no HP
                </a>
                <div class="card-footer py-4 center">
                    <p class="mb-0">Tidak punya akun? <a href="javascript:;" onclick="content_auth('register_page','Daftar sekarang');"><u>Daftar sekarang</u></a></p>
                </div>
            </div>
            {{-- <div class="divider divider-center"><span class="position-relative" style="top: -2px">OR</span></div> --}}
            <div id="mail_login">
                <form id="mail_login_form" class="mb-0 row">
                    <div class="col-12">
                        <input type="text" id="login_email" name="email" class="form-control not-dark" placeholder="email@bajaga.com" />
                    </div>
                    <div class="col-12 mt-4">
                        <input type="password" id="login_password" name="password" class="form-control not-dark" placeholder="Kata sandi" />
                    </div>
                    <div class="col-12">
                        <a href="javascript:;" class="float-end text-dark fw-light mt-2" onclick="content_auth('forgot_page','Atur ulang kata sandi');">Lupa kata sandi?</a>
                    </div>
                    <div class="col-12 mt-4">
                        <button type="button" class="button w-100 m-0" id="tombol_login_mail" onclick="handle_post('#tombol_login_mail','#mail_login_form','{{route('web.auth.login')}}','Masuk');">Masuk</button>
                    </div>
                    <div class="col-12 mt-4">
                        <a href="javascript:;" class="button center w-100 m-0" onclick="content_auth('main_auth','{{config('app.name')}}');">Kembali</a>
                    </div>
                </form>
            </div>
            <div id="phone_login">
                <form id="phone_login_form" class="mb-0 row">
                    <div class="col-12">
                        <input type="tel" id="login_phone" name="phone" class="form-control not-dark" placeholder="08123456789" />
                    </div>
                    <div class="col-12 mt-4">
                        <button type="button" class="button w-100 m-0" id="tombol_login_phone" onclick="handle_post('#tombol_login_phone','#phone_login_form','{{route('web.auth.login')}}','POST');">Masuk</button>
                    </div>
                    <div class="col-12 mt-4">
                        <a href="javascript:;" class="button center w-100 m-0" onclick="content_auth('main_auth','{{config('app.name')}}');">Kembali</a>
                    </div>
                </form>
            </div>
            <div id="register_page">
                <form id="register_form" class="mb-0 row">
                    <div class="col-12">
                        <input type="text" id="register_name" name="name" class="form-control not-dark" placeholder="Nama Lengkap" />
                    </div>
                    <div class="col-12 mt-4">
                        <input type="tel" id="register_phone" name="phone" class="form-control not-dark" placeholder="08123456789" />
                    </div>
                    <div class="col-12 mt-4">
                        <input type="email" id="register_email" name="email" class="form-control not-dark" placeholder="email@bajaga.com" />
                    </div>
                    <div class="col-12 mt-4">
                        <input type="password" id="register_password" name="password" class="form-control not-dark" placeholder="Kata sandi" />
                    </div>
                    <div class="col-12">
                        <a href="javascript:;" class="float-end text-dark fw-light mt-2" onclick="content_auth('main_auth','{{config('app.name')}}');">Sudah punya akun?</a>
                    </div>
                    <div class="col-12 mt-4">
                        <button type="button" class="button w-100 m-0" id="tombol_register" onclick="handle_post('#tombol_register','#register_form','{{route('web.auth.register')}}','POST');">Daftar</button>
                    </div>
                    <div class="col-12 mt-4">
                        <a href="javascript:;" class="button center w-100 m-0" onclick="content_auth('main_auth','{{config('app.name')}}');">Kembali</a>
                    </div>
                </form>
            </div>
            <div id="forgot_page">
                <form id="forgot_form" class="mb-0 row">
                    <div class="col-12">
                        <input type="email" id="forgot_email" name="email" class="form-control not-dark" placeholder="email@bajaga.com" />
                    </div>
                    <div class="col-12">
                        <a href="javascript:;" onclick="content_auth('mail_login','Masuk dengan email');" class="float-end text-dark fw-light mt-2">Sudah ingat kata sandi?</a>
                    </div>
                    <div class="col-12 mt-4">
                        <button type="button" class="button w-100 m-0" id="tombol_forgot" onclick="handle_post('#tombol_forgot','#forgot_form','{{route('web.auth.forgot')}}','Lanjut');">Lanjut</button>
                    </div>
                    <div class="col-12 mt-4">
                        <a href="javascript:;" class="button center w-100 m-0" onclick="content_auth('main_auth','{{config('app.name')}}');">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('custom_js_auth_modal')
        @guest
        <script>
            content_auth('main_auth','{{config('app.name')}}');
        </script>
        @endguest
    @endsection
</div>

{{-- modal lg --}}
<div class="modal fade" id="modalListResultLarge" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="contentListResultLarge"></div>
    </div>
</div>
{{-- modal lg --}}