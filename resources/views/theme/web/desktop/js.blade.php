<script src="{{asset('semicolon/js/jquery.js')}}"></script>
<script src="{{asset('semicolon/js/plugins.min.js')}}"></script>

<!-- Footer Scripts ============================================= -->
<script src="{{asset('js/components/star-rating.js')}}"></script>
<script src="{{asset('semicolon/js/functions.js')}}"></script>
<script src="{{asset('js/toastr.js')}}"></script>
<script src="{{asset('js/sweetalert.js')}}"></script>
<script src="{{asset('js/method.js')}}"></script>
@auth
<script src="{{asset('js/plugin.js')}}"></script>
<script>
    localStorage.setItem("route_cart", "{{route('web.cart')}}");
    localStorage.setItem("route_counter_notif", "{{route('web.counter_notif')}}");
    localStorage.setItem("route_notification", "{{route('web.notification')}}");
</script>
<script src="{{asset('js/cart.js')}}"></script>
<script src="{{asset('js/notif.js')}}"></script>
<script src="{{asset('js/select-boxes.js')}}"></script>
<script src="{{asset('semicolon/js/components/select-boxes.js')}}"></script>
<script src="{{asset('semicolon/js/components/selectsplitter.js')}}"></script>
@endauth
@guest
<script src="{{asset('js/auth.js')}}"></script>
@endguest
<script>
    var url_string = window.location.href
    var url = new URL(url_string);
    let params = url.search

    function search_header(){
        let filter = $("#search-input-header").val()
        location.href = '{{route('web.catalog.products')}}'+`?keyword=${filter}`
            // console.log(filter);
        
        // $.get('?search='+filter, function(result) {
        //     $('#list_result').html(result);
        // })
    }

    
    // jQuery(document).ready( function($){
    //     $(window).on( 'pluginIsotopeReady', function(){
    //         $('#shop').isotope({
    //             transitionDuration: '0.65s',
    //             getSortData: {
    //                 name: '.product-title',
    //                 price_lh: function( itemElem ) {
    //                     if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
    //                         var price = $(itemElem).find('.product-price ins').text();
    //                     } else {
    //                         var price = $(itemElem).find('.product-price').text();
    //                     }

    //                     priceNum = price.split("Rp. ");

    //                     return parseFloat( priceNum[1] );
    //                 },
    //                 price_hl: function( itemElem ) {
    //                     if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
    //                         var price = $(itemElem).find('.product-price ins').text();
    //                     } else {
    //                         var price = $(itemElem).find('.product-price').text();
    //                     }

    //                     priceNum = price.split("Rp. ");

    //                     return parseFloat( priceNum[1] );
    //                 }
    //             },
    //             sortAscending: {
    //                 name: true,
    //                 price_lh: true,
    //                 price_hl: false
    //             }
    //         });

    //         $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
    //             var element = $(this),
    //                 elementFilter = element.children('a').attr('data-filter'),
    //                 elementFilterContainer = element.parents('.custom-filter').attr('data-container');

    //             elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

    //             element.append('<span>'+ elementFilterCount +'</span>');

    //         });

    //         $('.shop-sorting li').click( function() {
    //             $('.shop-sorting').find('li').removeClass( 'active-filter' );
    //             $(this).addClass( 'active-filter' );
    //             var sortByValue = $(this).find('a').attr('data-sort-by');
    //             $('#shop').isotope({ sortBy: sortByValue });
    //             return false;
    //         });
    //     });
    // });
</script>
<!-- ADD-ONS JS FILES -->