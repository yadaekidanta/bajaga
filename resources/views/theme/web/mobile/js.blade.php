<!-- Bootstrap -->
<script src="{{asset('mobile/js/lib/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('semicolon/js/plugins.min.js')}}"></script>
<script src="{{asset('semicolon/js/functions.js')}}"></script>
<!-- Ionicons -->
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<!-- Splide -->
<script src="{{asset('mobile/js/plugins/splide/splide.min.js')}}"></script>
<!-- ProgressBar js -->
<script src="{{asset('mobile/js/plugins/progressbar-js/progressbar.min.js')}}"></script>
<!-- Base Js File -->
<!-- <script src="{{asset('semicolon/js/functions.js')}}"></script> -->

<script src="{{asset('js/toastr.js')}}"></script>
<script src="{{asset('js/sweetalert.js')}}"></script>
<script src="{{asset('js/method.js')}}"></script>
<script src="{{asset('mobile/js/base.js')}}"></script>

@auth
<script src="{{asset('js/plugin.js')}}"></script>
<script>
    localStorage.setItem("route_cart", "{{route('web.cart')}}");
    localStorage.setItem("route_counter_notif", "{{route('web.counter_notif')}}");
    localStorage.setItem("route_notification", "{{route('web.notification')}}");
</script>
<script src="{{asset('js/cart.js')}}"></script>
<script src="{{asset('js/notif.js')}}"></script>
<script src="{{asset('js/select-boxes.js')}}"></script>
<script src="{{asset('semicolon/js/components/select-boxes.js')}}"></script>
<script src="{{asset('semicolon/js/components/selectsplitter.js')}}"></script>
@endauth

@guest
<script src="{{asset('js/auth.js')}}"></script>
@endguest

<script>
    var url_string = window.location.href
    var url = new URL(url_string);
    let params = url.search
    let pathnames = url.pathname
    if (pathnames == "/home") {
        window.addEventListener('scroll', function(){
            let value = window.scrollY
            // console.log(value);
            let appHeader = document.querySelector('.appHeader')
            appHeader.classList.toggle('active-header', value > 36)
            if (!$(document.body).hasClass("dark-mode-active")) {
                document.getElementById("hamburger-nav").classList.toggle('active-header-items', value > 36)
                document.getElementById("pencarian-nav").classList.toggle('active-header-items', value > 36)
                document.getElementById("message-nav").classList.toggle('active-header-items', value > 36)
                document.getElementById("notification-nav").classList.toggle('active-header-items', value > 36)
                document.getElementById("cart-nav").classList.toggle('active-header-items', value > 36)
            }
        })
    }else if(pathnames == "/store/paystore"){
        document.querySelector('.appHeader').classList.add("bg-bajaga-primary")
    }
    else{
        document.querySelector('.appHeader').classList.add("bg-white")
        document.querySelector('.appHeader').classList.add("border")
    }
    let spl = pathnames.split("/")
    spl.pop()

    // Trigger welcome notification after 5 seconds
<<<<<<< HEAD
    // setTimeout(() => {
    //     notification('notification-welcome', 5000);
    // }, 2000);
=======
    
>>>>>>> c0bad8dc9a7f445738d489affb6c81b5f99bcd89

    // console.log(window.history);
    let pathBack = window.localStorage.getItem('pathBack');

    $('.goBacks').click(function() {
        // let ur = '{{ url()->previous() }}'
        // window.history.back()
        // history.replaceState();
        // location.reload()
    })

    function search_header(){
        let filter = $("#search-input-header").val()
        location.href = '{{route('web.catalog.products')}}'+`?keyword=${filter}`
    }

    // function func(event) {
    //     alert()
    //     console.log("before: " + window.history.length);
    //     window.history.replaceState(null, '', "./page1.html");
    //     console.log("after: " + window.history.length);
    // }
    // onload = function() { console.log("load: " + window.history.length);}


    // get data user for sidebar
    $(document).ready(function () {
        let data = getDataUser("data-user/getDataUser", callback);
        function callback(data){
            // console.log(data);

            if (data.status) {
                let data_user = data.data.user
                avatar = "avatars/avatar1.png"
                if(data_user.avatar != null) {
                    avatar = data_user.avatar
                } 
                $(".profileBox").html(`
                <div class="w-10 h-10 me-2 overflow-hidden rounded-circle border border-white">
                    <img src="{{ asset('storage/${avatar}') }}" alt="image" class="w-100 h-100 object-fit-cover">
                </div>
                <div class="d-flex flex-column gap-1">
                    <strong class="font-lg text-white">${limit_text(data_user.name, 15)}</strong>
                    <a href="{{route('web.profile')}}" class="text-light font-sm">Lihat Profile</a>
                </div>
                <a href="javascript:;" class="close-sidebar-button" data-bs-dismiss="offcanvas">
                    <ion-icon name="close"></ion-icon>
                </a>
                `)
            }
        }

        // TODO: Tambahkan nanti ini ke kondisi true diatas, ketika BE sudah siapkan data lokasi user
        // <div class="text-muted">
        //     <ion-icon name="location"></ion-icon>
        //     California
        // </div>
        
    })
</script>