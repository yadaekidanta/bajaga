@php
$uri = request()->path();
@endphp
<!doctype html>
<html lang="en">
    @include('theme.web.mobile.head')
<body>
    <!-- loader -->
    {{-- {{dd(url()->previous())}} --}}
    @include('theme.web.mobile.loader')
    <!-- * loader -->
    <!-- App Header -->
    @include('theme.web.mobile.modal')
    @include('theme.web.mobile.header')
    <!-- * App Header -->
    <!-- Search Component -->
    @include('theme.web.mobile.search')
    <!-- * Search Component -->
    <!-- App Capsule -->
    <div id="appCapsule">
        {{$slot}}
        <!-- app footer -->
        @if($uri != 'auth')
            @include('theme.web.mobile.footer')
        @endif
        <!-- * app footer -->
    </div>
    <!-- * App Capsule -->
    <!-- App Bottom Menu -->
    
    @if($uri != 'auth')
        @include('theme.web.mobile.menu')
    @endif
    <!-- * App Bottom Menu -->
    <!-- App Sidebar -->
    @include('theme.web.mobile.sidebar')
    <!-- * App Sidebar -->
    <!-- welcome notification  -->
    @include('theme.web.mobile.notification')
    <!-- * welcome notification -->
    <!-- ============== Js Files ==============  -->
    @include('theme.web.mobile.js')
    @yield('custom_js')
</body>
</html>