<div class="appFooter">
    <div class="w-full d-flex justify-content-center">
        <img src="{{asset('img/favicon.png')}}" alt="icon" class="footer-logo mb-2">
    </div>
    <div class="footer-title">
        &copy; 2022
        {{-- <span class="yearNow"></span> --}}
        {{config('app.name')}}.
    </div>
    {{-- <div>Mobilekit is PWA ready Mobile UI Kit Template.</div>
    Great way to start your mobile websites and pwa projects. --}}
    <div class="mt-2">
        <a href="#" class="btn btn-icon d-none btn-sm btn-facebook">
            <ion-icon name="logo-facebook"></ion-icon>
        </a>
        <a href="#" class="btn btn-icon d-none btn-sm btn-twitter">
            <ion-icon name="logo-twitter"></ion-icon>
        </a>
        <a href="#" class="btn btn-icon d-none btn-sm btn-linkedin">
            <ion-icon name="logo-linkedin"></ion-icon>
        </a>
        <a href="#" class="btn btn-icon d-none btn-sm btn-instagram">
            <ion-icon name="logo-instagram"></ion-icon>
        </a>
        <a href="#" class="btn btn-icon d-none btn-sm btn-whatsapp">
            <ion-icon name="logo-whatsapp"></ion-icon>
        </a>
        <a href="#" class="btn btn-icon btn-sm btn-secondary goTop">
            <ion-icon name="arrow-up-outline"></ion-icon>
        </a>
    </div>
</div>