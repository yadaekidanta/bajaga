<div class="appHeader">
    @if($uri == 'home')
        <div class="left ms-2">
            {{-- @auth --}}
            <a href="javascript:;" data-bs-toggle="offcanvas" data-bs-target="#sidebarPanel">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon-size-2" id="hamburger-nav" fill="white" width="24" height="24" viewBox="0 0 24 24"><path d="M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z"/></svg>
            </a>    
            {{-- @endauth --}}
        </div>
        <div class="pageTitle">
        </div>
        <div class="right d-flex align-items-center gap-3 me-2">
            <a href="javascript:;" data-bs-toggle="offcanvas" data-bs-target="#searchInput" aria-controls="searchInput">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon-size-2" id="pencarian-nav" width="24" fill="white" height="24" viewBox="0 0 24 24"><path d="M21.172 24l-7.387-7.387c-1.388.874-3.024 1.387-4.785 1.387-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785l7.387 7.387-2.828 2.828zm-12.172-8c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z"/></svg>
            </a>
            
            @auth
            <!-- icon pesan -->
            <a href="{{route('web.chat')}}">
                <svg class="unf-icon" viewBox="0 0 24 24" class="icon-size-2" id="message-nav" width="24" height="24" fill="white" style="display: inline-block; vertical-align: middle;"><path d="M20 4.25H4A1.76 1.76 0 0 0 2.25 6v12A1.76 1.76 0 0 0 4 19.75h16A1.76 1.76 0 0 0 21.75 18V6A1.76 1.76 0 0 0 20 4.25Zm-1 1.5-6.09 4.63a1.32 1.32 0 0 1-1.51 0L5.16 5.75H19Zm1 12.5H4a.25.25 0 0 1-.25-.25V6.57l6.71 5a2.86 2.86 0 0 0 3.29 0l6.5-4.94V18a.25.25 0 0 1-.25.25Z"></path></svg>
            </a>
            <!-- icon pesan -->

            <!-- icon notification -->
            <a href="{{route('web.notification.show')}}">
                <svg class="unf-icon" viewBox="0 0 24 24" class="icon-size-2" id="notification-nav" width="24" height="24" fill="white" style="display: inline-block; vertical-align: middle;"><path d="M21 17.25h-1.25V11a7.76 7.76 0 0 0-6.06-7.56 2.12 2.12 0 0 0 .06-.44 1.75 1.75 0 0 0-3.5 0c.005.148.025.296.06.44A7.76 7.76 0 0 0 4.25 11v6.25H3a.75.75 0 1 0 0 1.5h5.25V19a3.75 3.75 0 0 0 7.5 0v-.25H21a.75.75 0 1 0 0-1.5ZM14.25 19a2.25 2.25 0 0 1-4.5 0v-.25h4.5V19Zm.75-1.75H5.75V11a6.25 6.25 0 0 1 12.5 0v6.25H15Z"></path></svg>
            </a>
            <!-- icon notification -->

            <!-- icon cart -->
            <a href="{{route('web.cart.detail')}}">
                <svg class="unf-icon" viewBox="0 0 24 24" class="icon-size-2" id="cart-nav" width="24" height="24" fill="white" style="display: inline-block; vertical-align: middle;"><path d="M21.68 7.56a1.908 1.908 0 0 0-.35-.66 1.71 1.71 0 0 0-.58-.46 1.85 1.85 0 0 0-.75-.19H6.17a1.82 1.82 0 0 0-.57.13l-.06-.3a1.91 1.91 0 0 0-2-1.83h-1a.75.75 0 0 0 0 1.5h1c.42 0 .49.07.57.59l1.09 5.54.54 2.78A3.86 3.86 0 0 0 7 16.89a3.76 3.76 0 0 0 1.54.75A2 2 0 0 0 8 19a2 2 0 0 0 4 0 2 2 0 0 0-.46-1.25h2.88a2 2 0 1 0 3.06-.12 3.8 3.8 0 0 0 1.46-.7 3.71 3.71 0 0 0 1.32-2.1l1.47-6.46V8.3a1.68 1.68 0 0 0-.05-.74Zm-2.89 6.93a2.24 2.24 0 0 1-2.2 1.76H9.38a2.25 2.25 0 0 1-2.19-1.82L6 8.1A.62.62 0 0 1 6 8v-.14a.39.39 0 0 1 .1-.08H20a.28.28 0 0 1 .13 0 .389.389 0 0 1 .1.08c.03.03.05.069.06.11a.405.405 0 0 1 0 .11l-1.5 6.41Z"></path></svg>
            </a>
            <!-- icon cart -->
            @endauth
            
            @guest
            <!-- icon pesan -->
            <a href="{{route('web.auth.index')}}">
                <svg class="unf-icon" viewBox="0 0 24 24" class="icon-size-2" id="message-nav" width="24" height="24" fill="white" style="display: inline-block; vertical-align: middle;"><path d="M20 4.25H4A1.76 1.76 0 0 0 2.25 6v12A1.76 1.76 0 0 0 4 19.75h16A1.76 1.76 0 0 0 21.75 18V6A1.76 1.76 0 0 0 20 4.25Zm-1 1.5-6.09 4.63a1.32 1.32 0 0 1-1.51 0L5.16 5.75H19Zm1 12.5H4a.25.25 0 0 1-.25-.25V6.57l6.71 5a2.86 2.86 0 0 0 3.29 0l6.5-4.94V18a.25.25 0 0 1-.25.25Z"></path></svg>
            </a>
            <!-- icon pesan -->

            <!-- icon notification -->
            <a href="{{route('web.auth.index')}}">
                <svg class="unf-icon" viewBox="0 0 24 24" class="icon-size-2" id="notification-nav" width="24" height="24" fill="white" style="display: inline-block; vertical-align: middle;"><path d="M21 17.25h-1.25V11a7.76 7.76 0 0 0-6.06-7.56 2.12 2.12 0 0 0 .06-.44 1.75 1.75 0 0 0-3.5 0c.005.148.025.296.06.44A7.76 7.76 0 0 0 4.25 11v6.25H3a.75.75 0 1 0 0 1.5h5.25V19a3.75 3.75 0 0 0 7.5 0v-.25H21a.75.75 0 1 0 0-1.5ZM14.25 19a2.25 2.25 0 0 1-4.5 0v-.25h4.5V19Zm.75-1.75H5.75V11a6.25 6.25 0 0 1 12.5 0v6.25H15Z"></path></svg>
            </a>
            <!-- icon notification -->

            <!-- icon cart -->
            <a href="{{route('web.auth.index')}}">
                <svg class="unf-icon" viewBox="0 0 24 24" class="icon-size-2" id="cart-nav" width="24" height="24" fill="white" style="display: inline-block; vertical-align: middle;"><path d="M21.68 7.56a1.908 1.908 0 0 0-.35-.66 1.71 1.71 0 0 0-.58-.46 1.85 1.85 0 0 0-.75-.19H6.17a1.82 1.82 0 0 0-.57.13l-.06-.3a1.91 1.91 0 0 0-2-1.83h-1a.75.75 0 0 0 0 1.5h1c.42 0 .49.07.57.59l1.09 5.54.54 2.78A3.86 3.86 0 0 0 7 16.89a3.76 3.76 0 0 0 1.54.75A2 2 0 0 0 8 19a2 2 0 0 0 4 0 2 2 0 0 0-.46-1.25h2.88a2 2 0 1 0 3.06-.12 3.8 3.8 0 0 0 1.46-.7 3.71 3.71 0 0 0 1.32-2.1l1.47-6.46V8.3a1.68 1.68 0 0 0-.05-.74Zm-2.89 6.93a2.24 2.24 0 0 1-2.2 1.76H9.38a2.25 2.25 0 0 1-2.19-1.82L6 8.1A.62.62 0 0 1 6 8v-.14a.39.39 0 0 1 .1-.08H20a.28.28 0 0 1 .13 0 .389.389 0 0 1 .1.08c.03.03.05.069.06.11a.405.405 0 0 1 0 .11l-1.5 6.41Z"></path></svg>
            </a>
            <!-- icon cart -->
            @endguest
        </div>
    @elseif($uri == 'profile/bank_account')
        <div class="left">
            <a href="javascript:history.back()" class="headerButton goBacks">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">{{$title}}</div>
        <div class="right pe-1">
            <svg xmlns="http://www.w3.org/2000/svg" role="button" data-bs-toggle="offcanvas" data-bs-target="#canvasInfoBank" aria-controls="canvasInfoBank" fill="#383838" width="20" height="20" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm1 18h-2v-8h2v8zm-1-12.25c.69 0 1.25.56 1.25 1.25s-.56 1.25-1.25 1.25-1.25-.56-1.25-1.25.56-1.25 1.25-1.25z"/></svg>
        </div>
    @elseif($uri == 'store/paystore')
        <div class="left">
            <a href="javascript:history.back()" class="headerButton goBacks">
                <ion-icon name="chevron-back-outline" class="text-white"></ion-icon>
            </a>
        </div>
        <div class="pageTitle text-white">Paystore</div>
        <div class="right pe-1">
        </div>
    @elseif($uri == 'transaction/done' || $uri == 'transaction/success' || $uri == 'transaction/failed')
        <div class="left">
        </div>
        <div class="pageTitle">{{$title}}</div>
        <div class="right pe-1">
        </div>
    @elseif($uri == 'product')
        <div class="left">
            <a href="javascript:history.back()" class="headerButton goBacks">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">{{$title}}</div>
        <div class="right pe-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" data-bs-toggle="offcanvas" data-bs-target="#canvasMenuStore" aria-controls="canvasMenuStore" viewBox="0 0 24 24"><path d="M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z"/></svg>
        </div>
    @else
        <div class="left">
            <a href="javascript:history.back()" class="headerButton goBacks">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">{{$title}}</div>
        <div class="right"></div>
    @endif
</div>