<div id="notification-welcome" class="notification-box">
    <div class="notification-dialog android-style">
        <div class="notification-header">
            <div class="in">
                <img src="{{asset('img/favicon.png')}}" alt="image" class="imaged w24">
                <strong>{{config('app.name')}}</strong>
                {{-- <span>just now</span> --}}
            </div>
            <a href="javascript:;" class="close-button">
                <ion-icon name="close"></ion-icon>
            </a>
        </div>
        <div class="notification-content">
            <div class="in">
                <h3 class="subtitle">Selamat Datang</h3>
                {{-- <div class="text">
                    Mobilekit is a PWA ready Mobile UI Kit Template.
                    Great way to start your mobile websites and pwa projects.
                </div> --}}
            </div>
        </div>
    </div>
</div>