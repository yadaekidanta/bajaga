<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>{{config('app.name') . ': ' .$title ?? config('app.name')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description" content="{{config('app.name')}}">
    <meta name="author" content="{{config('app.name')}}" />
	<meta name="keyword" content="{{$keyword ?? config('app.name')}}" />
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" />
    <link rel="stylesheet" href="{{asset('semicolon/css/swiper.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/components/select-boxes.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('semicolon/css/bootstrap.css')}}" type="text/css" />
    <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('semicolon/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/toastr.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('mobile/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('semicolon/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('mobile/css/style.css')}}">
    <link rel="manifest" href="{{asset('__manifest.json')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>