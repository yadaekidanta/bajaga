<div class="offcanvas offcanvas-top h-11vh" tabindex="-1" id="searchInput" aria-labelledby="searchInputLabel">
  <div class="offcanvas-body search_padding">
    <!-- search -->
    <form id="form-search" class="m-0 mt-2">
        <div class="position-relative w-100 d-flex gap-2 align-items-center">
            <input type="text" id="search-input-header" class="form-control w-100 px-4" placeholder="Cari barang apa nih?">

            <button type="button" onclick="search_header()" class="btn btn-primary">Cari</button>
            
            <!-- icon search -->
            <div class="d-flex align-items-center justify-content-center position-absolute left-1 top-0 h-100">
                <svg xmlns="http://www.w3.org/2000/svg" class="" width="24" height="24" viewBox="0 0 24 24">
                    <g fill="none" fill-rule="evenodd">
                        <path fill-rule="nonzero" d="M0 0h24v24H0z"/>
                        <path fill="#9FA6B0" d="M11.002 14.125a3.128 3.128 0 0 1-3.127-3.123c0-1.724 1.4-3.126 3.123-3.127 1.723 0 3.127 1.4 3.127 3.123 0 1.724-1.4 3.126-3.123 3.127m6.728 2.28l-2.137-2.135a.919.919 0 0 0-.606-.262A4.974 4.974 0 0 0 16 10.997 5 5 0 1 0 11.003 16a4.974 4.974 0 0 0 3.01-1.016c.013.22.094.437.263.605l2.137 2.135a.93.93 0 1 0 1.317-1.318"/>
                    </g>
                </svg>
            </div>
            <!-- icon search -->

        </div>
    </form>
    <!-- /search -->
  </div>
</div>