<div class="appBottomMenu">
    @if($uri == 'cart/detail')
        <div class="px-3 w-100 py-2 bg-white d-flex justify-content-between align-items-center">
            <div>
                <span class="text-mutted">Total Harga</span>
                <h3 class="m-0" id="total_price">...</h3>
            </div>

            <button type="submit" class="btn btn-primary px-5" id="btn-beli" onclick="beli('#btn-beli', '{{route('web.transaction')}}', 'POST', 'Beli')">Beli</button>
        </div>
    @elseif($uri == 'transaction/detail' || $uri == 'transaction/detail/*')
        <div class="px-3 w-100 py-2 bg-white bg-dark-mode d-flex justify-content-between align-items-center">
            <button class="btn btn-primary w-full">Beri Ulasan</button>
        </div>
    @elseif($uri == 'profile/bank_account')
        <div class="px-3 w-100 py-2 bg-white bg-dark-mode d-flex justify-content-between align-items-center">
            <a href="{{route('web.profile.bank_account.add')}}" class="btn btn-primary w-full">Tambah Rekening</a>
        </div>
    @elseif($uri == 'store/settings/shipping/services')
        <div class="px-3 w-100 py-2 bg-white bg-dark-mode d-flex justify-content-between align-items-center">
            <button class="btn btn-primary w-full" id="btn-submit-courier" onclick="handle_save('#btn-submit-courier', 'form_input', '{{route('web.')}}', method)">Simpan</button>
        </div>
    @elseif(preg_match("/chat\/[1-9]/", $uri))
        <div class="px-1 w-100 py-2 bg-white bg-dark-mode">
            <form id="form-chat" class="m-0">
                <div class="d-flex gap-2 align-items-center ">
                    <input type="text" name="body" placeholder="Type you're message" class="form-control">
                    <input  type="hidden" id="btn_id_input" name="to_id" class="form-control">
                    <button id="btn-send" type="button" onclick="send_chat('#btn-send', '#form-chat', '{{route('web.chat')}}', 'POST')" class="btn btn-primary">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" fill="white" height="16" viewBox="0 0 24 24"><path d="M24 0l-6 22-8.129-7.239 7.802-8.234-10.458 7.227-7.215-1.754 24-12zm-15 16.668v7.332l3.258-4.431-3.258-2.901z"/></svg>
                    </button>
                </div>
            </form>
        </div>
    @elseif(preg_match("/products\/.+/", $uri))
        <div class="px-3 w-100 py-2 bg-white bg-dark-mode d-flex gap-2 align-items-center">

            @auth
            <button class="btn btn-outline-primary w-100" id="btn-beli" onclick="beli('#btn-beli', '{{route('web.transaction')}}', 'POST')">Beli Langsung</button>
            <button id="tombol_cart" onclick="add_cart_mobile('#tombol_cart','#form_cart','{{route('web.cart.add')}}','POST', '{{route('web.cart.detail')}}', 'Keranjang');" class="btn btn-primary w-100 d-flex align-items-center gap-1">
                <i class="icon-plus1"></i>
                <span>Keranjang</span>
            </button>
            @endauth
            
            @guest
            <a href="{{route('web.auth.index')}}" class="btn btn-outline-primary w-100" id="btn-beli">Beli Langsung</a>
            <a href="{{route('web.auth.index')}}" id="tombol_cart" class="btn btn-primary w-100 d-flex align-items-center gap-1">
                <i class="icon-plus1"></i>
                <span>Keranjang</span>
            </a>
            @endguest
        </div>
    @else
        <a href="{{route('web.home')}}" class="item {{request()->is('home') ? 'active' : 'text-dark'}}">
            <div class="col">
                <ion-icon name="home-outline"></ion-icon>
                <span class="act-dark">
                    Halaman Utama
                </span>
            </div>
        </a>
        <a href="{{route('web.auction')}}" class="item  {{request()->is('auction') || request()->is('auction/*') ? 'active' : 'text-dark'}}">
            <div class="col">
                <ion-icon name="cube-outline"></ion-icon>
                <span class="act-dark">
                    Lelang
                </span>
            </div>
        </a>
        @auth
        <a href="{{route('web.profile')}}" class="item  {{request()->is('profile/*') || request()->is('profile') ? 'active' : 'text-dark'}}">
            <div class="col">
                <ion-icon name="person-outline"></ion-icon>
                <span class="act-dark">
                    Profil
                </span>
            </div>
        </a>
        <a href="{{route('web.transaction.checkout')}}" class="item  {{request()->is('transaction/*') ? 'active' : 'text-dark'}}">
            <div class="col">
                <ion-icon name="layers-outline"></ion-icon>
                <span class="act-dark">
                    Transaksi
                </span>
            </div>
        </a>
        @endauth

        @guest
        <a href="{{route('web.auth.index')}}" class="item  {{request()->is('transaction/*') ? 'active' : 'text-dark'}}">
            <div class="col">
                <ion-icon name="layers-outline"></ion-icon>
                <span class="act-dark">
                    Transaksi
                </span>
            </div>
        </a>
        <a href="{{route('web.auth.index')}}" class="item  {{request()->is('auth') ? 'active' : 'text-dark'}}">
            <div class="col">
                <ion-icon name="log-in-outline"></ion-icon>
                <span class="act-dark">
                    Masuk / Daftar
                </span>
            </div>
        </a>
        @endguest
    @endif
</div>