load_cart(localStorage.getItem("route_cart"));
function tombol_cart(){
    load_cart(localStorage.getItem("route_cart"));
}
function load_cart(url){
    // let data = "view="+ view + "&load_keranjang=";
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        success: function (response){
            $('.top-cart-items').html(response.collection);
            $('.top-checkout-price').html('Rp. '+format_ribuan(response.total));
            $('.top-cart-number').html(format_ribuan(response.total_item) ?? 0);
            // $('#counter_cart').html(format_ribuan(response.total_item) ?? 0);
        },
    });
}
function add_cart(tombol, form, url, method){
    // alert()
    $(tombol).submit(function() {
        return false;
    });
    let data = $(form).serialize();
    $(tombol).prop("disabled", true);
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(response) {
            console.log(response.alert);
            if (response.alert == "success") {
                success_toastr(response.message);
                $(form)[0].reset();
                load_cart(localStorage.getItem("route_cart"));
            } else {
                error_toastr(response.message);
            }
            $(tombol).prop("disabled", false);
        },
        error: response => {
            if (response.status == 401) {
                location.href = '{{route("web.auth.index")}}'
            }
        }
    });
}

function add_cart_mobile(tombol, form, url, method, redirect, text_button = null){
    $(tombol).submit(function() {
        return false;
    });
    
    if (text_button) {
        $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)
    }

    let data = $(form).serialize();
    $(tombol).prop("disabled", true);
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(response) {
            // console.log(response.alert);
            if (response.alert == "success") {
                Swal.fire({ 
                    text: "Item berhasil ditambahkan ke keranjang", 
                    icon: "success", 
                    buttonsStyling: !1, 
                    confirmButtonText: "Lihat keranjang", 
                    customClass: { confirmButton: "btn btn-primary",
                    showDenyButton: true,
                    showCancelButton: false,
                    denyButtonText: "Tidak",
                 } }).then((result) => {
                    if (result.isConfirmed) location.href = redirect
                 });
                $(form)[0].reset();
                load_cart(localStorage.getItem("route_cart"));
            } else {
                error_toastr(response.message);
            }
            $(tombol).prop("disabled", false);
            toggleLoading($(tombol), false)

            if (text_button) {
                $(tombol).html(text_button)
            }
        },
        
        error: function(response) {
            Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
            $(tombol).prop("disabled", false);
            toggleLoading($(tombol), false)

            if (text_button) {
                $(tombol).html(text_button)
            }
        }
    });
}

function update_quantity(route){
    $.ajax({
        type: "PATCH",
        url: route,
        dataType: 'json',
        success: function(response) { 
            if (response.alert == "success") {
                load_cart(localStorage.getItem("route_cart"));    
            load_list(1);
            } else {
                error_toastr(response.message);
            } 
        }
    });
}
function input_quantity(route){
    let data = "qty=" + $("#qty").val();
    $.ajax({
        type: "PATCH",
        url: route,
        data: data,
        dataType: 'json',
        success: function(response) { 
            if (response.alert == "success") {
                load_cart(localStorage.getItem("route_cart"));    
            load_list(1);
            } else {
                error_toastr(response.message);
                $("#qty").val(response.qty);
            } 
        }
    });
}
$(document).on('click', '#top-cart', function(){
    load_cart(localStorage.getItem("route_cart"));
});