$("body").on("contextmenu", "img", function(e) {
    return false;
});
$('img').attr('draggable', false);
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            // load_list(1);
        }
    });
});
let page;
$(window).on('hashchange', function() {
    if (window.location.hash) {
        page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            load_list(page);
        }
    }
});
$(document).ready(function() {
    $(document).on('click', '.paginasi', function(event) {
        event.preventDefault();
        $('.paginasi').removeClass('active');
        $(this).parent('.paginasi').addClass('active');
        // var myurl = $(this).attr('href');
        page = $(this).attr('halaman').split('page=')[1];
        load_list(page);
    });
});
function main_content(obj){
    $("#content_list").hide();
    $("#content_input").hide();
    $("#content_detail").hide();
    $("#" + obj).show();
}

function hideFooter(){
    $(".appFooter").hide()
}

function load_list(page){
    // $('#list_result').html(`<div class="w-100 pt-5 mt-5 d-flex align-items-center justify-content-center"><div class="spinner-border text-primary" role="status"></div></div>`);
    $('.ajax-load').toggleClass("d-none");
    $.get('?page=' + page, $('#content_filter').serialize(), function(result) {
        // console.log(result);
        $('#list_result').html(result);
        main_content('content_list');
        // countEl(result)
        $('.ajax-load').toggleClass("d-none");
        if(result == ""){
            $('.ajax-load').html("No more Items Found!");
            $('.load-inf').html("No more Items Found!");
            return;
        }
        countWl() // for load wishlist coounter in wishlist page
    }, "html");
}

function load_custom(container, url, page, cb = null, filter = '#content_filter'){
    // alert(container, url, page)
    $.get(url+'?page=' + page, $(filter).serialize(), function(result) {
        // console.log($.parseJSON(result));
        try {
            let cond = $.parseJSON(result)
            if (cond.alert == "error") {
                if (cb) cb() 
                error_toastr(cond.message);
                $(container).html(`
                    <div class="w-100 h-100 d-flex justify-content-center mb-5 mt-5">
                        <span class="text-danger">${cond.message}</span>
                    </div>
                `);
                return
                // parseJSON
            }
        } catch (error) {
            
        }
        
        if (cb) cb(result) 
        if(result == ""){
            $('.ajax-load').html("No more Items Found!");
            $('.load-inf').html("No more Items Found!");
            return;
        }
        $(container).html(result);
    }, "html");
}

function load_more(page, customUrl = null, customContainer = "#list_result", filter = '#content_filter'){
    // alert()
    let url = '?page='
    if (customUrl) {
        url = customUrl+url 
    }
    if (customContainer) {
        customContainer = customContainer
    }
    $(".ajax-load").show();
    $(".load-inf").removeClass("d-none");
    $.get(url + page, $(filter).serialize(), function(result) {
        // alert()
        // console.log(result);
        if(result == ""){
            $('.ajax-load').html("No more Items Found!");
            $('.load-inf').html("No more Items Found!");
            return;
        }
        $('.ajax-load').hide();
        $(".load-inf").addClass("d-none");
        $(customContainer).append(result);
        main_content('content_list');
    }, "html");
}

function loading_animate(color = "primary"){
    if (color == "primary") {
        return `<div class="spinner-border spinner-border-sm text-primary spinner" role="status"></div>`
    }else if (color == "white"){
        return `<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`
    }else{
        return `<div class="spinner-border spinner-border-sm text-primary spinner" role="status"></div>`
    }
}

function load_input(url){
    $.get(url, {}, function(result) {
        // alert(result)
        // console.log(result);
        $('#content_input').html(result);
        main_content('content_input');
    }, "html");
}
function load_detail(url){
    $.get(url, {}, function(result) {
        $('#content_detail').html(result);
        main_content('content_detail');
    }, "html"); 
}
function handle_open_modal_add(modal){
    $(modal).modal('show');
}
function handle_open_modal(url,modal,content, form = null){
    
    console.log(data);
    $.ajax({
        type: "POST",
        data: data,
        url: url,
        success: function (html) {
            $(modal).modal('show');
            $(content).html(html);
        },
        error: function () {
            $(content).html('<h3>Ajax Bermasalah !!!</h3>')
        },
    });
}
function handle_confirm(title, confirm_title, deny_title, method, route, idtmp = null, cb = null, text = null, background = 'white'){
    if (background == 'dark-mode') {
        background = '#0f1c24'
    }
    Swal.fire({
        title: title,
        text: text,
        icon: 'warning',
        background,
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: confirm_title,
        denyButtonText: deny_title,
    }).then((result) => {
        if (result.isConfirmed) {
            var id = [];
            $(':checkbox:checked').each(function(i){
                id[i] = $(this).val();
            });
            if(id.length === 0 && idtmp == null){
                Swal.fire('Please Select atleast one checkbox', '', 'info')
            } else {
                $.ajax({
                    type: method,
                    url: route,
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        if(response.redirect == "input"){
                            load_input(response.route);
                        }else if(response.redirect == "reload"){
                            location.reload();
                        }else{
                            load_list(1);
                            localStorage.setItem("page_infinate", 1)
                        }
                        Swal.fire(response.message, '', response.alert)
                        if (cb) cb()
                    },
                    error: function (response) {
                        Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    }
                });
            } 
        } else if (result.isDenied) {
            Swal.fire('Konfirmasi dibatalkan', '', 'info')
        }
    });
}
function handle_check(route){
    $.ajax({
        type: "PATCH",
        url: route,
        dataType: 'json',
        success: function(response) {      
            load_list(1);
        }
    });
}
function handle_get(url){
    $.get(url, {}, function(result) {
        info_toastr(response.message);
    }, "json");
}
function handle_post(url){
    $.post(url, {}, function(result) {
        info_toastr(response.message);
    }, "json");
}

function handle_save_password(tombol, form, url, method){
    $(tombol).submit(function () {
        return false;
    });
    let data = $(form).serialize();
    $(tombol).prop("disabled", true);
    $(tombol).html("Please wait");
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {
            
        },
        success: function (response) {
            if (response.alert=="success") {
                Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                $(form)[0].reset();
                setTimeout(function () {
                    $(tombol).prop("disabled", false);
                        main_content('content_list');
                        load_list(1);
                }, 2000);
            } else {
                Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                setTimeout(function () {
                    $(tombol).prop("disabled", false);
                }, 2000);
            }
        },
    });
}

function toggleLoading(container, cond){
    // alert()
    let text_btn = container.text()
    // console.log(text_btn);
    if (cond) {
        $(container).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`);
    } else {
        $(container).html(text_btn);
    }
}

function handle_save(tombol, form, url, method, cb = null, text_button = null, no_form_reset = null, no_redirect = null){
    $(tombol).submit(function() {
        return false;
    });
    
    let nameBtn = $(tombol).text()
    
    let data = $(form).serialize();
    // console.log(data);

    if (text_button) {
        $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`);
    }

    $('input[disabled]').each( function() {
        data = data + '&' + $(this).attr('name') + '=' + $(this).val();
    });
    
    $(tombol).prop("disabled", true);
    $(tombol).attr("data-kt-indicator", "on");
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        processData: false,
        beforeSend: function() {
            if (!text_button) {
                toggleLoading($(tombol), true)
                $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`);
            }
        },
        success: function(response) {
            if (response.alert == "success") {
                Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                if (!no_form_reset) {
                    $(form)[0].reset();
                }
                if (!no_redirect) {
                    if(response.redirect == "input"){
                        load_input(response.route);
                    }else if(response.redirect == "reload"){
                        setTimeout(function() {
                            $(tombol).prop("disabled", false);
                            $(tombol).removeAttr("data-kt-indicator");
                            main_content('content_list');
                            load_list(1);
                        }, 2000);
                    }else if(response.redirect == "reload page"){
                        $(tombol).prop("disabled", false);
                        $(tombol).removeAttr("data-kt-indicator");
                    }else{
                        location.href= response.redirect;
                    }
                } else {
                    $(tombol).prop("disabled", false);
                }
            } else {
                Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                setTimeout(function() {
                    $(tombol).prop("disabled", false);
                    $(tombol).removeAttr("data-kt-indicator");
                }, 2000);
            }
            if (text_button) {
                if (text_button == 'send') {
                    $(tombol).html(`<i class="icon-line-send"></i>`);
                }else{
                    $(tombol).html(text_button);
                }
            }else{
                toggleLoading($(tombol), false)
            }
            $(".offcanvas").offcanvas("hide")
            if (cb) cb()
        },

        error: function(response) {
            // console.log();
            Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
            $(".offcanvas").offcanvas("hide")
            $(tombol).prop("disabled", false);
            if (text_button) {
                $(tombol).html(text_button);
            }else{
                toggleLoading($(tombol), false)
            }
            if (cb) cb()
        }
    });
}
function handle_save_modal(tombol, form, url, method, modal){
    $(tombol).submit(function() {
        return false;
    });
    $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)
    let data = $(form).serialize();
    let text_btn = $(tombol).text()
    $(tombol).prop("disabled", true);
    $(tombol).attr("data-kt-indicator", "on");
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {
        },
        success: function(response) {
            if (response.alert == "success") {
                Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                $(form)[0].reset();
                load_list(1);
                setTimeout(function() {
                    $(modal).modal('hide');
                    $(tombol).prop("disabled", false);
                    $(tombol).removeAttr("data-kt-indicator");
                }, 2000);
            } else {
                 Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                setTimeout(function() {
                    $(tombol).prop("disabled", false);
                    $(tombol).removeAttr("data-kt-indicator");
                }, 2000);
            }
            $(tombol).html(`${text_btn}`)
        },
    });
}
function handle_upload(tombol, form, url, method, idTmp = null, cb = null, text_button = null){
    // alert()
    $(document).one('submit', form, function(e) {
        let data = new FormData(this);
        data.append('_method', method);
        let text_tombol = $(tombol).text()
        $(tombol).prop("disabled", true);
        $(tombol).attr("data-kt-indicator", "on");
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            beforeSend: function() {
                $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)
            },
            success: function(response) {
                if (response.alert == "success") {
                    Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    if (idTmp == null) {
                        $(form)[0].reset();
                    }
                    setTimeout(function() {
                        if(response.redirect == "input"){
                            load_input(response.route);
                        }
                        if(!response.redirect){
                            // alert()
                            localStorage.setItem("page_infinate", 1);
                            $(tombol).prop("disabled", false);
                            $(tombol).removeAttr("data-kt-indicator");
                            main_content('content_list');
                            load_list(1);
                        }
                    }, 2000);
                } else {
                     Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    setTimeout(function() {
                        $(tombol).prop("disabled", false);
                        $(tombol).removeAttr("data-kt-indicator");
                    }, 2000);
                }
                if (text_button) {
                    $(tombol).html(text_button)
                }
                $("#btn-paid").html(text_tombol)
                $(tombol).prop("disabled", false);
                if (cb) cb()
            },
            error: function(response) {
                // console.log(response);
                Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                if (text_button) {
                    $(tombol).html(text_button)
                }
                $(tombol).prop("disabled", false);
                if (cb) cb()
            }
        });
        return false;
    });
}
function handle_open_modal(url,modal,content, form = null){
    // alert()
    let data = $(form).serialize();
    $.ajax({
        type: "POST",
        data: data,
        url: url,
        success: function (html) {
            $(content).html(html);
            $(modal).modal('show');
        },
        error: function () {
            $(content).html('<h3>Ajax Bermasalah !!!</h3>')
        },
    });
}
function handle_upload_modal(tombol, form, url, method, modal){
    $(document).one('submit', form, function (e) {
        let data = new FormData(this);
        $(tombol).prop("disabled", true);
        $(tombol).attr("data-kt-indicator", "on");
        $.ajax({
            type: method,
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.alert == "success") {
                    Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    $(form)[0].reset();
                    $(modal).modal('hide');
                    setTimeout(function () {
                        $(tombol).prop("disabled", false);
                        $(tombol).removeAttr("data-kt-indicator");
                    }, 2000);
                } else {
                     Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                    setTimeout(function () {
                        $(tombol).prop("disabled", false);
                        $(tombol).removeAttr("data-kt-indicator");
                    }, 2000);
                }
            },
        });
        return false;
    });
}
/* Fungsi formatRupiah */
function formatRupiah(angka){
    // console.log(angka);
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return rupiah;
}

function getDataUser(route, cb){
    $.ajax({
        type: "GET",
        url: "data-user",
        dataType: 'json',
        success: function(response) {    
            cb({
                data: response,
                status: true
            })
        },
        
        error: function(err){
            // console.log(err);
            if (err.status == 401) {
                cb({
                    data: null,
                    status: false
                })
            }
        }
    });
}

function limit_text(name, limit = 30)
{
    if (name.length > limit) {
        return name.substring(0, limit) + " ...";
    }
    return name
}
function countWl()
{
    let c = $("#list_result").children().length
    // console.log(c);
    if (parseInt(c) == 0) {
        $("#info-wishlist").removeClass("d-none")
    }else{
        $("#info-wishlist").addClass("d-none")
    }
    $("#count-wishlist").text(c + ' barang Wishlist')
}