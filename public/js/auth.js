$("body").on("contextmenu", "img", function(e) {
    return false;
});
$('img').attr('draggable', false);
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function auth_content(obj)
{
    $("#page_login").hide();
    $("#page_forgot").hide();
    $("#" + obj).show();
}
function content_auth(obj,title)
{
    $("#title_auth").html(title);
    $("#main_auth").hide();
    $("#mail_login").hide();
    $("#phone_login").hide();
    $("#register_page").hide();
    $("#forgot_page").hide();
    $("#" + obj).show();
}
$("#form_login").on('keydown', 'input', function(event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-login'));
        var val = $($this).val();
        if (val == 1) {
            $('[data-login="' + (index + 1).toString() + '"]').focus();
        } else {
            $('#tombol_login').trigger("click");
        }
    }
});
$("#email").focus();
function handle_post(tombol,form,url, text_button = null){
    $(tombol).prop("disabled", true);
    $(tombol).attr("data-kt-indicator", "on");
    if (text_button) {
        $(tombol).html(`<div class="spinner-border spinner-border-sm text-white spinner" role="status"></div>`)
    }
<<<<<<< HEAD
    $.ajax({
        method: "POST",
        url,
        data: $(form).serialize(),
        dataType: "json",
        success: function(response) {
            if (response.alert == "success") {
                Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } }).then(function() {
                    if(response.callback == "reload"){
                        location.reload();
                    }else if(response.callback == "redirect"){
                        location.href = response.redirect
                    }else{
                        content_auth(response.callback,response.title);
                    }
                });
            }else{
                Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
            }    
            $(tombol).prop("disabled", false);
            $(tombol).removeAttr("data-kt-indicator");
            
            if (text_button) {
                $(tombol).html(text_button)
            }
        },
        error: function(response) {
            Swal.fire({ text: response.responseJSON.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });  
            $(tombol).prop("disabled", false);
            $(tombol).removeAttr("data-kt-indicator");
            
            if (text_button) {
                $(tombol).html(text_button)
            }
=======
    $.post(url, $(form).serialize(), function(result) {
        if (result.alert == "success") {
            Swal.fire({ text: result.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } }).then(function() {
                if(result.callback == "reload"){
                    location.reload();
                    setTimeout(() => {
                        notification('notification-welcome', 5000);
                    }, 10000);
                }else{
                    content_auth(result.callback,result.title);
                }
            });
        }else{
            Swal.fire({ text: result.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
>>>>>>> c0bad8dc9a7f445738d489affb6c81b5f99bcd89
        }
    })
}