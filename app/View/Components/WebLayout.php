<?php

namespace App\View\Components;
use Jenssegers\Agent\Agent;
use Illuminate\View\Component;

class WebLayout extends Component
{
    public $title;
    public $keyword;
    public function __construct($title, $keyword)
    {
        $this->title = $title;
        $this->keyword = $keyword;
    }
    public function render()
    {
        $agent = new Agent();
        if($agent->isMobile()){
            return view('theme.web.mobile.main');
        }else{
            return view('theme.web.desktop.main');
        }
    }
}
