<?php

namespace App\Helpers;

use App\Models\Account;
use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Support\Str;
use App\Models\PurchasePayment;
use Illuminate\Support\Facades\DB;

class Helper{
    public static function IDGenerator($model, $prefix){
        $preff = $prefix;
		$value = '';
		$row = $model::orderBy('id','DESC')->get()->count();
		$nomor = sprintf("%03s", $row) + 1;
		$value = $preff.$nomor;
		return $value;
    }
	public static function check_stock($id_product, $qty){
		$product = Product::where('id','=',$id_product)->first();
		$stok = $product->stock;
		if($stok < $qty){
			return 0;
		}else{
			return 1;
		}
	}
	public static function increase_stock($model_detail,$param_model,$param_column,$param_detail){
		$data_detail = $model_detail::where($param_column,'=',$param_detail)->get();
		$model = $param_model::where('id','=',$param_detail)->first();
		$subtotal = 0;
		foreach($data_detail as $dd){
			$id_product = $dd->product_id;
    		$price = $dd->price;
			$subtotal += $dd->subtotal;
    		$quantity = $dd->qty;
			$product = Product::where('id','=',$id_product)->first();
			$product->price = $price;
			$product->stock = $product->stock + $quantity;
			$product->update();
		}
		$model->total = $subtotal;
		$model->grand_total = $subtotal;
		$model->update();
	}
	public static function decrease_stock($model_detail,$param_model,$param_column,$param_detail){
		$data_detail = $model_detail::where($param_column,'=',$param_detail)->get();
		$model = $param_model::where('id','=',$param_detail)->first();
		$subtotal = 0;
		foreach($data_detail as $dd){
			$id_product = $dd->product_id;
    		$price = $dd->price;
    		$quantity = $dd->qty;
			$subtotal += $dd->subtotal;
			$product = Product::where('id','=',$id_product)->first();
			$product->price = $price;
			$product->stock = $product->stock - $quantity;
			$product->update();
		}
		$model->total = $subtotal;
		$model->grand_total = $subtotal;
		$model->update();
	}
	public static function increase_balance($model_payment,$param_model , $param_column,$param_id, $account_id){
		$data_payment = $model_payment::where($param_column,'=',$param_id)->where('account_id','=',$account_id)->first();
		$purchase = $param_model::where('id','=',$param_id)->first();
		$akun = Account::where('id','=',$account_id)->first();
		$akun->balance = Str::remove('.', $akun->balance) + Str::remove('.', $data_payment->total);
		$akun->update();

		$purchase->total_paid = $purchase->total_paid - $data_payment->total;
		$purchase->update();
		$purchases = $param_model::where('id','=',$param_id)->first();
		if($purchases->total_paid < $purchases->grand_total){
			$purchases->payment_st = "Belum lunas";
		}else{
			$purchases->payment_st = "Lunas";
		}
		$purchases->update();
	}
	public static function decrease_balance($model_payment,$param_model , $param_column, $param_id, $account_id){
		$data_payment = $model_payment::where($param_column,'=',$param_id)->where('account_id','=',$account_id)->first();
		$purchase = $param_model::where('id','=',$param_id)->first();
		$akun = Account::where('id','=',$account_id)->first();
		$akun->balance = Str::remove('.', $akun->balance) - Str::remove('.', $data_payment->total);
		$akun->update();

		$purchase->total_paid = $purchase->total_paid + $data_payment->total;
		$purchase->update();
		$purchases = $param_model::where('id','=',$param_id)->first();
		if($purchases->total_paid >= $purchases->grand_total){
			$purchases->payment_st = "Lunas";
		}else{
			$purchases->payment_st = "Belum lunas";
		}
		$purchases->update();
	}
	public static function increase_balance_return($model_payment,$param_model , $param_column,$param_id, $account_id){
		// dd($model_payment,$param_model , $param_column,$param_id, $account_id);
		$data_payment = $model_payment::where($param_column,'=',$param_id)->where('account_id','=',$account_id)->first();
		$purchase = $param_model::where('id','=',$param_id)->first();
		$akun = Account::where('id','=',$account_id)->first();
		$akun->balance = Str::remove('.', $akun->balance) + Str::remove('.', $data_payment->total);
		$akun->update();

		$purchase->total_paid = $data_payment->total;
		$purchase->update();
		$purchases = $param_model::where('id','=',$param_id)->first();
		if($purchases->total_paid < $purchases->grand_total){
			$purchases->payment_st = "Belum lunas";
		}else{
			$purchases->payment_st = "Lunas";
		}
		$purchases->update();
	}
	public static function decrease_balance_return($model_payment,$param_model , $param_column, $param_id, $account_id){
		$data_payment = $model_payment::where($param_column,'=',$param_id)->where('account_id','=',$account_id)->first();
		$purchase = $param_model::where('id','=',$param_id)->first();
		$akun = Account::where('id','=',$account_id)->first();
		$akun->balance = Str::remove('.', $akun->balance) - Str::remove('.', $data_payment->total);
		$akun->update();

		$purchase->total_paid = $purchase->total_paid - $data_payment->total;
		$purchase->update();
		$purchases = $param_model::where('id','=',$param_id)->first();
		if($purchases->total_paid >= $purchases->grand_total){
			$purchases->payment_st = "Lunas";
		}else{
			$purchases->payment_st = "Belum lunas";
		}
		$purchases->update();
	}
}
