<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PurchaseReturn extends Model
{
    use HasFactory;
    public $table = "purchase_return";
    protected $casts = [
        'date' => 'date',
    ];

    public function purchase()
    {
        return $this->belongsTo(Purchase::class,'purchase_id','id');
    }
    public function purchase_detil()
    {
        return $this->hasMany(PurchaseReturnDetail::class,'purchase_return_id','id');
    }
    public function purchase_payment()
    {
        return $this->hasMany(PurchaseReturnPayment::class,'purchase_return_id','id');
    }
    public function subtotal_detil()
    {
        return $this->belongsTo(PurchaseReturnDetail::class,'id','purchase_return_id')->select(DB::raw('sum(subtotal) AS subtotal'));
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class,'created_by','id');
    }    
}