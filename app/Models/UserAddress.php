<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use HasFactory;
    protected $table = "users_address";

    public function users(){
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
    public function province(){
        return $this->belongsTo(Province::class,'province_id','id');
    }
    public function city(){
        return $this->belongsTo(City::class,'city_id','id');
    }
    public function subdistrict(){
        return $this->belongsTo(Subdistrict::class,'subdistrict_id','id');
    }
}
