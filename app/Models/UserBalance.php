<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    use HasFactory;
    protected $fillable = ["code", "user_id", "amount", "type", "status", "failure_code", "created_at", "updated_at"];
    public $table = "user_balances";
}
