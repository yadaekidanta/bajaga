<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "product";
    // public $timestamps = false;
    public function getImageAttribute()
    {
        return asset('storage/' . $this->photo);
    }
    public function product_store()
    {
        return $this->belongsTo(UserStore::class,'store_id','id');
    }
    public function product_warranty()
    {
        return $this->belongsTo(ProductWarranty::class,'product_warranty_id','id');
    }
    public function product_business()
    {
        return $this->belongsTo(BusinessLocation::class,'business_location_id','id');
    }
    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class,'product_category_id','id');
    }
    public function product_unit()
    {
        return $this->belongsTo(ProductUnit::class,'product_unit_id','id');
    }
    public function product_brand()
    {
        return $this->belongsTo(ProductBrand::class,'product_brand_id','id');
    }
    public function user_display()
    {
        return $this->belongsTo(UserDisplay::class,'user_display_id','id');
    }

    public function product_reviews()
    {
        return $this->hasMany(ProductReview::class,'product_id','id');
    }

    public function product_discuss()
    {
        return $this->hasMany(ProductDiscuss::class, 'product_id','id');
    }
    
}
