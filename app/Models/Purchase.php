<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Purchase extends Model
{
    use HasFactory;
    protected $table = "purchase";
    protected $casts = [
        'date' => 'date',
    ];
    public function business_location()
    {
        return $this->belongsTo(BusinessLocation::class,'business_location_id','id');
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplier_id','id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class,'created_by','id');
    }
    public function purchase_detil()
    {
        return $this->hasMany(PurchaseDetail::class,'purchase_id','id');
    }
    public function purchase_payment()
    {
        return $this->hasMany(PurchasePayment::class,'purchase_id','id');
    }
    public function subtotal_detil()
    {
        return $this->belongsTo(PurchaseDetail::class,'id','purchase_id')->select(DB::raw('sum(subtotal) AS subtotal'));
    }
}
