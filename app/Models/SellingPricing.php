<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellingPricing extends Model
{
    use HasFactory;
    protected $table = "selling_price_group";
    public $timestamps = false;
}
