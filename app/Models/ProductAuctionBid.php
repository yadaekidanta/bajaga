<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAuctionBid extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $table = "product_auction_bid";


    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
