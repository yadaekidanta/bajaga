<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseReturnDetail extends Model
{
    use HasFactory;
    public $table = "purchase_return_detail";
    public $timestamps = false;
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
    
    public function purchase_return()
    {
        return $this->belongsTo(PurchaseReturn::class, 'purchase_return_id','id');
    }
}
