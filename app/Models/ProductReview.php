<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    use HasFactory;
    public $table = "reviews";
    // public $timestamps = false;

    protected $fillable = ["user_id", "product_id", "review", "to_id", "read_at", "rate"];
    
    public function user_review()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
}
