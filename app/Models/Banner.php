<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $table = "banners";
    public $timestamps = false;
    public function getImageAttribute()
    {
        return asset('storage/' . $this->photo);
    }
}
