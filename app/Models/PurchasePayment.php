<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchasePayment extends Model
{
    use HasFactory;
    protected $table = "purchase_payment";
    protected $casts = [
        'date' => 'date',
    ];
    public function account()
    {
        return $this->belongsTo(Account::class,'account_id','id');
    }
}
