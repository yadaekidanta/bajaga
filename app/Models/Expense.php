<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;
    protected $table = "expense";
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    protected $casts = [
        'date' => 'date',
    ];
    public function expense_payment()
    {
        return $this->hasMany(ExpensePayment::class,'expense_id','id');
    }
    public function category()
    {
        return $this->belongsTo(ExpenseCategory::class,'expense_category_id','id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class,'created_by','id');
    }
}
