<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    public $table = "carts";

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
    public function cart_store()
    {
        return $this->belongsTo(UserStore::class,'store_id','id');
    }
}
