<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SaleReturn extends Model
{
    use HasFactory;
    public $table = "sale_return";
    protected $casts = [
        'date' => 'date',
    ];

    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');
    }
    public function sale_detil()
    {
        return $this->hasMany(SaleReturnDetail::class,'sale_return_id','id');
    }
    public function sale_payment()
    {
        return $this->hasMany(SaleReturnPayment::class,'sale_return_id','id');
    }
    public function subtotal_detil()
    {
        return $this->belongsTo(SaleReturnDetail::class,'id','sale_return_id')->select(DB::raw('sum(subtotal) AS subtotal'));
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class,'created_by','id');
    }
}
