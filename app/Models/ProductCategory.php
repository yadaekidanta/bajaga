<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;
    protected $table = "product_category";
    public $timestamps = false;
    public function getImageAttribute()
    {
        return asset('storage/' . $this->photo);
    }
    public function product_category()
    {
        return $this->hasMany(ProductCategory::class,'product_category_id','id');
    }
    public function subcategory()
    {
        return $this->hasMany(ProductCategory::class,'product_category_id','id');
    }
    public function product()
    {
        return $this->hasMany(Product::class,'product_category_id','id')->where('sku','NOT LIKE','%#%')->where('not_for_selling','n');
    }
    public function product_subcategory()
    {
        return $this->hasMany(Product::class,'product_subcategory_id','id')->where('sku','NOT LIKE','%#%')->where('not_for_selling','n');
    }
}