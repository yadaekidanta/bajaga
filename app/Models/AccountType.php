<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    use HasFactory;
    protected $table = "account_type";
    public $timestamps = false;
    public function account_type()
    {
        return $this->hasMany(AccountType::class,'account_type_id','id');
    }
    public function parent_account()
    {
        return $this->belongsTo(AccountType::class,'account_type_id','id');
    }
}