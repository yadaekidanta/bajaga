<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreFavorite extends Model
{
    use HasFactory;

    protected $fillable = ["store_id", "user_id"];

    public function store()
    {
        return $this->belongsTo(UserStore::class, "store_id", "id");
    }
}
