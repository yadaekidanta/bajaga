<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    use HasFactory;
    protected $table = "product_brand";
    public $timestamps = false;
    public function getImageAttribute()
    {
        return asset('storage/' . $this->photo);
    }
    public function product_brand()
    {
        return $this->hasMany(Product::class,'product_brand_id','id')->where('sku','NOT LIKE','%#%');
    }
}
