<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sale extends Model
{
    use HasFactory;
    protected $fillable = [
        "code",
        "customer_id",
        "date",
        "total",
        "disc_price",
        "total_paid",
        "grand_total",
        "note",
        "customer_name",
        "customer_phone",
        "payment_method",
        "courier",
        "code_courier",
        "type",
        "st",
        "no_resi",
        "payment_st",
        "store_id",
        "shipping_detail",
        "shipping_price",
        "created_by"
    ];
    protected $table = "sale";
    protected $casts = [
        'date' => 'date',
    ];
    public function store()
    {
        return $this->belongsTo(UserStore::class, 'store_id','id');
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class,'created_by','id');
    }
    public function sale_detail()
    {
        return $this->hasMany(SaleDetail::class, 'sale_id', 'id');
    }
    public function sale_payment()
    {
        return $this->hasMany(SalePayment::class,'sale_id','id');
    }
    public function subtotal_detil()
    {
        return $this->belongsTo(SaleDetail::class,'id','sale_id')->select(DB::raw('sum(subtotal) AS subtotal'));
    }
}
