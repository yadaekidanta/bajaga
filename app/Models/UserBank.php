<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function users()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }
    
    public function banks()
    {
        return $this->belongsTo(Bank::class, "bank_id", "id");
    }
}
