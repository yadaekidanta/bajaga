<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleReturnDetail extends Model
{
    use HasFactory;
    public $table = "sale_return_detail";
    public $timestamps = false;
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
