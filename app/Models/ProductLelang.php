<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLelang extends Model
{
    use HasFactory;
    protected $table = "product_auction";
    public $timestamps = false;
    public function getImageAttribute()
    {
        return asset('storage/' . $this->photo);
    }
    public function product_store()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
    public function product_business()
    {
        return $this->belongsTo(BusinessLocation::class,'business_location_id','id');
    }
    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class,'product_category_id','id');
    }
    public function product_unit()
    {
        return $this->belongsTo(ProductUnit::class,'product_unit_id','id');
    }
    public function product_brand()
    {
        return $this->belongsTo(ProductBrand::class,'product_brand_id','id');
    }

    public function bids()
    {
        return $this->hasMany(ProductAuctionBid::class, 'product_id', 'id')->orderBy('bid', 'DESC');
    }

}
