<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Employee extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $table = "employee";
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $fillable = ['password'];

    public function activities()
    {
        return $this->hasMany(EmployeActivities::class, "employee_id", "id");
    }
}
