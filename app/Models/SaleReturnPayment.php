<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleReturnPayment extends Model
{
    use HasFactory;
    public $table = "sale_return_payment";
    protected $casts = [
        'date' => 'date',
    ];
    public function account()
    {
        return $this->belongsTo(Account::class,'account_id','id');
    }
}
