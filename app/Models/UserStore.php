<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStore extends Model
{
    use HasFactory;
    protected $table = "users_store";
    protected $fillable = ["start_at", "end_at"];
    protected $dates = [
        'verified_at', 'start_at', 'end_at'
    ];
    public function city()
    {
        return $this->belongsTo(City::class,'city_id','id');
    }
    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class,'subdistrict_id','id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class,'province_id','id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'users_id','id');
    }

    public function point_store()
    {
        return $this->hasOne(StorePoint::class, "store_id", "id");
    }

    public function product()
    {
        return $this->hasMany(Product::class, 'store_id', 'id');
    }

    public function sales()
    {
        return $this->hasMany(Sale::class, "store_id", "id");
    }

    public function store_favorites()
    {
        return $this->hasMany(StoreFavorite::class, "store_id", "id");
    }
    
}
