<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_have_courier extends Model
{
    use HasFactory;
    public $table = "user_have_courier";
    
    public function store()
    {
        return $this->belongsTo(UserStore::class, "store_id", "id");
    }

    public function courier()
    {
        return $this->belongsTo(Courier::class, "courier_id", "id");
    }
    
}
