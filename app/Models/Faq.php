<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasFactory;
    protected $table = "faq";
    protected $dates = [
        'created_at'
    ];
    public $timestamps = false;
    public function category()
    {
        return $this->belongsTo(FaqCategory::class,'faq_category_id','id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class,'created_by','id');
    }
}
