<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDiscuss extends Model
{
    use HasFactory;
    protected $table = "discuss";
    protected $fillable = ["read_at"];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function product()
    {
        return $this->belongsTo(Product::class, "product_id", "id");
    }
}
