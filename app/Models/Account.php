<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;
    protected $table = "account";
    public $timestamps = false;
    protected $casts = [
        'created_at' => 'datetime',
    ];
    public function account_type()
    {
        return $this->belongsTo(AccountType::class,'account_type_id','id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class,'created_by','id');
    }
}
