<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    use HasFactory;
    public $table = "wishlist";
    protected $fillable = ["users_id", "id_product"];

    public function users()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'id_product', 'id');
    }
}
