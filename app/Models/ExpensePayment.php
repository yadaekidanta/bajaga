<?php

namespace App\Models;

use App\Models\Account;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExpensePayment extends Model
{
    use HasFactory;
    protected $table = "expense_payment";

	protected $casts = [
        'date' => 'date',
    ];
	public function account()
    {
        return $this->belongsTo(Account::class,'account_id','id');
    }
    public static function CekSaldo($akun,$jumlah){
		$account = Account::where('id','=',$akun)->first();
		$saldo = $account->balance;
		if($saldo < $jumlah){
			return 'kurang';
		}else{
			return 'cukup';
		}
	}
    public static function PenguranganSaldo($akun,$jumlah){
		$account = Account::where('id','=',$akun)->first();
		$account->balance = $account->balance - $jumlah;
		$account->update();
	}
	public static function PengembalianSaldo($akun,$jumlah){
		$account = Account::where('id','=',$akun)->first();
		$account->balance = $account->balance + $jumlah;
		$account->update();
	}
}
