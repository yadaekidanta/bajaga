<?php

namespace App\Http\Services;

use App\Models\User;
use App\Models\UserBalance;
use App\Models\UserBank;
use App\Notifications\TransactionNotification;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Xendit\Xendit;
use Illuminate\Support\Str;

class CheckoutService
{

    function __construct()
    {
        Xendit::setApiKey(env('XENDIT_KEY'));
    }

    public function createInvoice($args)
    {
        $date = new \DateTime();
        $redirectUrl = 'http://127.0.0.1:8000/home';
        $defParams = [
            'external_id' => 'bajaga-checkout-' . $date->getTimestamp(),
            'currency' => 'IDR',
            "redirect_url" => "http://127.0.0.1:8000/home",
            "success_redirect_url" => route('web.transaction.success'),
            "failure_redirect_url" => route('web.transaction.failed'),
            "customer_notification_preference" => [
                "invoice_created" => [
                    "whatsapp",
                    "sms",
                    "email"
                ],
                "invoice_reminder" => [
                    "whatsapp",
                    "sms",
                    "email"
                ],
                "invoice_paid" => [
                    "whatsapp",
                    "sms",
                    "email"
                ],
                "invoice_expired" => [
                    "whatsapp",
                    "sms",
                    "email"
                ]
            ],
        ];

        $data = json_decode(json_encode($args), true);
        $params = array_merge($defParams, $data);
        // dd($params);
        $response = [];

        try {
            $response = \Xendit\Invoice::create($params);
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function getInvoice($args)
    {
        // dd($args);
        try {
            $response = \Xendit\Invoice::retrieve($args["id"]);
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function createPayout($args)
    {
        $params = [
            'external_id' => '123123123',
            'amount' => 45000,
            'email' => 'sayidinaahmadalqososyi@gmail.com',
        ];

        $response = [];

        try {
            $response = \Xendit\Payouts::create($params);
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function createDisbursment($params)
    {
        try {
            $randomString = Str::random(40);
            
            // cek data userbank
            $user_bank = UserBank::where([
                ["user_id", $params["user_id"]],
                ["is_active", 1]
            ])->first();
            if (!$user_bank) {
                return response()->json([
                    "alert" => "Error",
                    "message" => "Tambahkan akun bank anda dan aktifkan terlebih dahulu!"
                ], 500);
            }
            // cek saldo mencukupi atau tidak
            $params['amount'] = intval(str_replace(".", "", $params['amount']));
            $balance = UserBalance::where("user_id", $params["user_id"])->sum("amount");
            // dd(intval($params["amount"]), $params["amount"] , $balance);
            if (intval($params["amount"]) > $balance) {
                return response()->json([
                    "alert" => "Error",
                    "message" => "Saldo anda tidak mencukupi!"
                ], 500);
            }else if(intval($params["amount"]) < 10000){
                return response()->json([
                    "alert" => "Error",
                    "message" => "Minimal penarikan Rp10.000!"
                ], 500);
            }

            // lakukan disbursment ke akun terkait
            $params["bank_code"] = $user_bank->banks->code;
            $params["account_holder_name"] = $user_bank->account_holder_name;
            $params["account_number"] = $user_bank->account_number;
            

            $params['external_id'] = 'bajaga-disbursment-' . (string) Str::uuid();
            $params['X-IDEMPOTENCY-KEY'] = $randomString;

            
            $response = \Xendit\Disbursements::create($params);
            // dd($response);
            if ($response) {
                UserBalance::create([
                    "code" => $response["id"],
                    "user_id" => $params["user_id"],
                    "amount" => $response["amount"],
                    "type" => "W",
                    "status" => $response["status"]
                ]);

                $user = User::where("id", $params["user_id"])->first(); 
                
                $details = [
                    'greeting' => "Hi ".$user->name,
                    'body' => 'Penarikan dana kamu masih dalam proses!'
                ];

                Notification::send($user, new TransactionNotification($user, $details));

                return response()->json([
                    "alert" => "success",
                    "message" => "Penarikan dana kamu masih dalam proses!",
                    "data" => $response
                ]);
            }

        } catch (\Throwable $e) {
            // return response()->json([
            //     "alert" => "Error",
            //     "message" => "Terjadi masalah, coba lagi"
            // ], 500);
            return response()->json([
                "alert" => "Error",
                "message" => $e->getMessage()
            ], 500);
        }
    }
}
