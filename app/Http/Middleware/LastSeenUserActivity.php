<?php

namespace App\Http\Middleware;

use App\Models\Employee;
use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class LastSeenUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::guard('web')->check()) {

            $expireTime = Carbon::now()->addMinute(1); // keep online for 1 min

            Cache::put('is_client_online'.Auth::guard('web')->user()->id, true, $expireTime);

            //Last Seen

            User::where('id', Auth::guard('web')->user()->id)->update(['last_seen' => Carbon::now()]);

        }elseif (Auth::guard('office')->check()) {

            $expireTime = Carbon::now()->addMinute(1); // keep online for 1 min

            Cache::put('is_employee_online'.Auth::guard('office')->user()->id, true, $expireTime);



            //Last Seen

            Employee::where('id', Auth::guard('office')->user()->id)->update(['last_seen' => Carbon::now()]);

        }

        return $next($request);
    }
}
