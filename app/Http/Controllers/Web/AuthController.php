<?php

namespace App\Http\Controllers\Web;

use Exception;
use App\Models\User;
use App\Helpers\Helper;
use App\Mail\ForgotMail;
use App\Mail\WelcomeMail;
use App\Traits\ReplyJson;
use App\Models\UserVerify;
use Illuminate\Support\Str;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Mail\PasswordChangeMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Notifications\PasswordChangedNotification;
use App\Notifications\RegisteredNotification;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class AuthController extends Controller
{
    use ResponseView, ReplyJson;
    public function __construct()
    {
        $this->middleware('guest')->except('do_logout');
    }
    public function index()
    {
        return $this->render_view('auth.main');
    }

    public function get_register()
    {
        return $this->render_view('auth.register');
    }

    public function do_register(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users|min:9|max:13',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
            
        }
        // dd("do");
        $user = new User;
        $user->name = Str::title($request->name);
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $generator = Helper::IDGenerator(new User,'code','CUST-');
        $user->code = $generator;
        $user->type = "Online";
        $user->save();
        $token = Str::random(64);

        UserVerify::create([
            'user_id' => $user->id,
            'token' => $token
        ]);
        Notification::send($user, new RegisteredNotification($user,$token));
        Auth::login($user);
        return response()->json([
            'alert' => 'success',
            'message' => 'Thanks for join us '. $request->fullname,
            'callback' => 'redirect',
            'redirect' => route("web.home"),
            'title' => config('app.name')
        ]);
    }

    public function do_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        if(Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return response()->json([
                'alert' => 'success',
                'message' => 'Selamat datang '. Auth::guard('web')->user()->name,
                'callback' => 'reload',
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Maaf, sepertinya ada beberapa kesalahan yang terdeteksi, silakan coba lagi.',
            ]);
        }
    }

    public function check_phone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:9|max:13',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        $check = User::where('phone',$request->phone)->get()->count();
        if($check > 0){
            return response()->json([
                'alert' => 'success',
                'message' => 'Message sent to '.$request->phone,
                'callback' => '+62'.Str::after($request->phone, '0'),
                'real_phone' => $request->phone,
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Sorry, looks like there phone number are not registered.',
            ]);
        }
    }
    public function do_login_sms(Request $request)
    {
        $check = User::where('phone','=',$request->phone)->first();
        Auth::guard('office')->login($check, true);
        return response()->json([
            'alert' => 'success',
            'message' => 'Welcome back '. Auth::guard('office')->user()->name,
            'callback' => 'reload',
        ]);
    }

    public function do_verify($token)
    {
        $verifyUser = UserVerify::where('token', $token)->first();

        $message = 'Sorry your email cannot be identified.';

        if (!is_null($verifyUser)) {
            $user = $verifyUser->user;

            if (!$user->email_verified_at) {
                $verifyUser->user->email_verified_at = date("Y-m-d H:i:s");
                $verifyUser->user->save();
                $message = "Your e-mail is verified. You can now login.";
            } else {
                $message = "Your e-mail is already verified. You can now login.";
            }
        }

        return redirect()->route('web.home')->with('message', $message);
    }

    public function do_forgot(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        $user = User::where('email',$request->email)->first();

        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        $data = array(
            'token' => $token,
            'user' => $user
        );
        Notification::send($data['user'], new ResetPasswordNotification($data));
        return response()->json([
            'alert' => 'success',
            'message' => 'We have e-mailed your password reset link!',
            'callback' => 'main_auth',
            'title' => config('app.name')
        ]);
    }
    public function reset($token)
    {
        return $this->render_view('auth.reset', compact('token'));
    }
    public function do_reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|exists:password_resets',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('token')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('token'),
                ]);
            } elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            } elseif ($errors->has('password_confirmation')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password_confirmation'),
                ]);
            }
        }

        $updatePassword = DB::table('password_resets')
            ->where([
                'token' => $request->token
            ])
            ->first();

        if (!$updatePassword) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Invalid token!',
            ]);
        }
        $user = User::where('email', $updatePassword->email)
        ->update(['password' => Hash::make($request->password)]);
        $users = User::where('email', $updatePassword->email)
        ->first();
        DB::table('password_resets')->where(['email' => $updatePassword->email])->delete();
        Mail::to($users->email)->send(new PasswordChangeMail($users));
        return response()->json([
            'alert' => 'success',
            'message' => 'Your password has been changed!',
            'callback' => 'auth',
            'title' => config('app.name')
        ]);
    }
    public function password_changed(){
        return $this->render_view('auth.password_changed');
    }

    public function resend_mail()
    {
        $users = User::where('id', Auth::user()->id)->first();
        $token = Str::random(64);
        UserVerify::create([
            'user_id' => $users->id,
            'token' => $token
        ]);
        Mail::to($users->email)->send(new WelcomeMail($users, $token));
        return response()->json([
            'alert' => 'info',
            'message' => 'We have resend activation to your email'
        ]);
    }
    public function verification()
    {
        return $this->render_view('profile.notice');
    }
    public function deactivated(User $user)
    {
        return $this->render_view('auth.deactivate');
    }

    public function do_logout()
    {
        $user = Auth::guard('web')->user();
        Auth::logout($user);
        return redirect()->route('web.home');
    }
}
