<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\ProductCategory;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class DiscountController extends Controller
{
    use ResponseView, ReplyJson;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Discount::where('name','LIKE','%'.$keywords.'%')
            ->orWhere('code','LIKE','%'.$keywords.'%')
            ->orderBy('id',"desc")
            ->paginate(10);
            return view('page.office.discount.list', compact('collection'));
        }
        return view('page.office.discount.main');
    }
    public function create()
    {
        return view('page.office.discount.input', ['data' => new Discount]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:discount',
            'name' => 'required',
            'desc' => 'required',
            'thumbnail' => 'required',
            'type' => 'required',
            'nilai' => 'required',
            'start' => 'required',
            'end' => 'required',
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }else if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }else if ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }else if ($errors->has('nilai')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }else if ($errors->has('start')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('start'),
                ]);
            }else if ($errors->has('end')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('end'),
                ]);
            }else if ($errors->has('st')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('st'),
                ]);
            }
        }
        $data = New Discount;
        if($request->type == "persentase"){
            $data->persentase = Str::remove(',',$request->nilai);
        }else{
            $data->harga = Str::remove(',',$request->nilai);
        }
        $data->code = $request->code;
        $data->name = Str::title($request->name);
        $data->desc = $request->desc;
        
        $thumbnail = request()->file('thumbnail')->store("discount");
        $data->thumbnail = $thumbnail;

        $data->type = $request->type;
        $data->start_at = $request->start;
        $data->end_at = $request->end;
        $data->st = $request->st;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Diskon tersimpan',
        ]);
    }
    public function show(Discount $discount)
    {
        //
    }
    public function edit(Discount $discount)
    {
        return view('page.office.discount.input', ['data' => $discount]);
    }
    public function update(Request $request, Discount $discount)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'name' => 'required',
            'desc' => 'required',
            'thumbnail' => 'required',
            'type' => 'required',
            'nilai' => 'required',
            'start' => 'required',
            'end' => 'required',
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }else if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }else if ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }else if ($errors->has('nilai')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }else if ($errors->has('start')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('start'),
                ]);
            }else if ($errors->has('end')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('end'),
                ]);
            }else if ($errors->has('st')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('st'),
                ]);
            }
        }
        if($request->type == "persentase"){
            $discount->persentase = Str::remove(',',$request->nilai);
        }else{
            $discount->harga = Str::remove(',',$request->nilai);
        }
        $discount->code = $request->code;
        $discount->name = Str::title($request->name);
        $discount->desc = $request->desc;

        $thumbnail = request()->file('thumbnail')->store("discount");
        $discount->thumbnail = $thumbnail;
        
        $discount->type = $request->type;
        $discount->start_at = $request->start;
        $discount->end_at = $request->end;
        $discount->st = $request->st;
        $discount->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Diskon tersimpan',
        ]);
    }
    public function destroy(Discount $discount)
    {
        $discount->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Diskon terhapus',
        ]);
    }

    public function show_all(Request $request)
    {
        $collections = Discount::where(
            [
                ["end_at", ">", now()], 
                ["st", "aktif"]
            ])->paginate(10);
        
        $list_category = ProductCategory::where('product_category_id','=',0)->get();
        if ($request->ajax()) {
            return $this->render_view('cupon.data', compact("collections"));
        }
        // dd($collection);
        return $this->render_view('cupon.main', compact("collections", "list_category"));
    }

    public function detail(Discount $discount)
    {
        return $this->render_view('cupon.detail', compact("discount"));
    }
}
