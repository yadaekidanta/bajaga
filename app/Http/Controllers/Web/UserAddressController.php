<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\Subdistrict;
use App\Models\UserAddress;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserAddressController extends Controller
{
    use ResponseView, ReplyJson;
    public function index(Request $request)
    {
        $user = Auth::user();
        $address = UserAddress::where("users_id", $user->id)->orderByDesc("is_use")->get();
        if ($request->ajax()) {
            if ($request->keyword) {
                $address = UserAddress::where([
                    ["users_id", $user->id],
                    ["phone", 'like', "%$user->id%"]
                    ])
                    ->orWhere([
                        ["users_id", $user->id],
                        ["address", 'like', "%$user->id%"]
                    ])
                    ->orderByDesc("is_use")->get();
            }
            return $this->render_view('profile.address.data', compact('user', 'address'));
        }
        return $this->render_view('profile.address.main', compact('user', 'address'));
    }

    public function create()
    {
        $user = new UserAddress;
        $userAddress = null;
        $provinsi = Province::get();
        return $this->render_view('profile.address.input', compact('user', 'provinsi', 'userAddress'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            'postcode' => 'required|numeric ',
            'deskripsi' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
           if ($errors->has('address')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }elseif($errors->has('province')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif($errors->has('city')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif($errors->has('subdistrict')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif($errors->has('postcode')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }elseif($errors->has('deskripsi')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('deskripsi'),
                ]);
            }
        }
        $useraddress = New UserAddress;
        $useraddress->users_id = Auth::user()->id;
        $useraddress->desc = $request->deskripsi;
        $useraddress->address = $request->address;
        $useraddress->province_id = $request->province;
        $useraddress->city_id = $request->city;
        $useraddress->postcode = $request->postcode;
        $useraddress->subdistrict_id = $request->subdistrict;
        $useraddress->is_use = UserAddress::where('users_id',Auth::user()->id)->get()->count() > 0 ?  '0' : '1';
        $useraddress->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Alamat tersimpan',
            "redirect" => 'reload'
        ]);
    }

    public function show(UserAddress $userAddress)
    {
        //
    }

    public function edit($id)
    {
        $userAddress = UserAddress::where('id',$id)->first();
        // dd($userAddress);
        $user = new UserAddress;
        $provinsi = Province::get();
        return $this->render_view('profile.address.input', compact('user', 'provinsi', 'userAddress'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            'postcode' => 'required',
            'deskripsi' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
           if ($errors->has('address')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }elseif($errors->has('province')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif($errors->has('city')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif($errors->has('subdistrict')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif($errors->has('postcode')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }elseif($errors->has('deskripsi')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('deskripsi'),
                ]);
            }
        }
        $userAddress = UserAddress::where("id", $id)->first();
        $userAddress->desc = $request->deskripsi;
        $userAddress->address = $request->address;
        $userAddress->province_id = $request->province;
        $userAddress->city_id = $request->city;
        $userAddress->postcode = $request->postcode;
        $userAddress->subdistrict_id = $request->subdistrict;
        $userAddress->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Alamat tersimpan',
            "redirect" => 'reload page'
        ]);
    }

    public function destroy(UserAddress $userAddress)
    {
        try {
            $userAddress->delete();
            return response()->json([
                'alert' => 'success',
                'message' => 'Alamat berhasil dihapus!',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Coba lagi!',
            ], 500);
        }
    }

    public function address_is_use(UserAddress $userAddress)
    { 
        $user = Auth::user();
        $address = UserAddress::where('users_id',$user->id)->get();
        foreach ($address as $key => $value) {
            $value->is_use = '0';
            $value->update();
        }
        $userAddress->is_use = "1";
        $userAddress->update();
        
        return response()->json([
            'alert' => 'success',
            'message' => 'Alamat berhasil digunakan',
        ]);
    }

    public function edit_modal(UserAddress $userAddress)
    {

        // $userAddress = UserAddress::where('id', $id)->first();
        // dd($userAddress);
        // $user = new UserAddress;
        $provinsi = Province::get();
        return $this->render_view('profile.input_address_modal', compact('provinsi', 'userAddress'));
    }

    public function getDataAddress()
    {
        try {
            $user = Auth::user();
            $address = UserAddress::where("users_id", $user->id)->orderBy("is_use", "DESC")->get();
            // $address->map(function($data) {
            //     $user = Auth::user();
            //     $data["name"];
            //     $data["phone"];
            //     $data->name = $user->name;
            //     $data->phone = $user->phone;
            //     $data->province = $data->province->name;
            //     $data->city = $data->city->name;
            //     $data->subdistrict = $data->subdistrict->name;
                
            //     return $data;
            // });
            return $this->render_view('profile.list_address', compact('user', 'address'));
            // return response()->json([
            //     'alert' => 'success',
            //     "data" => $address
            // ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                "message" => "Something wrong!"
            ], 500);
        }
    }
    
}
