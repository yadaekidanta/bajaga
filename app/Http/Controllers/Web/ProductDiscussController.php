<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductDiscuss;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Darryldecode\Cart\Validators\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductDiscussController extends Controller
{
    use ResponseView, ReplyJson;
    
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'comment' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('product_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_id'),
                ], 500);
            }else if ($errors->has('comment')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('comment'),
                ], 500);
            }
        }

        try {
            $discuss = new ProductDiscuss;
            if ($request->to_id) {
                $discuss->user_id = Auth::user()->id;
                $discuss->product_id = $request->product_id;
                $discuss->to_id = $request->to_id;
                $discuss->comment = $request->comment;
                $discuss->save();
                if (Auth::user()->id == Product::where("id", $request->product_id)->first()->product_store->user->id) {
                    ProductDiscuss::where("id", $request->to_id)->update([
                        "read_at" => now()
                    ]);
                }else{
                    ProductDiscuss::where("id", $request->to_id)->update([
                        "read_at" => null
                    ]);
                }
                $msg = "Berhasil membalas diskusi!";
            }else{
                $discuss->user_id = Auth::user()->id;
                $discuss->product_id = $request->product_id;
                $discuss->comment = $request->comment;
                $discuss->save();
                $msg = "Pesan anda tersampaikan!";
            }
            return response()->json([
                "alert" => "success",
                "message" => $msg,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "alert" => "error",
                "message" => $th->getMessage(),
            ], 500);
        }
    }

    public function show(Request $request)
    { 	
        $collection = ProductDiscuss::where([["product_id", $request->product_id], ["to_id", null]])->paginate(10);
        // dd($collection);
        if ($request->ajax()) {
            if ($request->filter) {
                $filter = $request->filter;
                if ($filter == "Baru") {
                    $collection = ProductDiscuss::where([["product_id", $request->product_id],["read_at", null]])->paginate(10);
                }
            }
            return $this->render_view("product.discuss.data", compact('collection'));
        } 
    }

    public function showAll(Request $request)
    { 	
        $collection = ProductDiscuss::where("to_id", null)->orderBy("read_at")->paginate(10);
        // dd($collection);
        if ($request->ajax()) {
            if ($request->filter) {
                $filter = $request->filter;
                if ($filter == "Baru") {
                    $collection = ProductDiscuss::where([["product_id", $request->product_id],["read_at", null]])->orderBy("created_at", "DESC")->paginate(10);
                }
            }
            return $this->render_view("product.discuss.data", compact('collection'));
        } 
    }
    
    
}
