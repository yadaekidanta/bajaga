<?php

namespace App\Http\Controllers\Web;

use App\Models\Bank;
use App\Models\UserBank;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserBankController extends Controller
{
    use ResponseView, ReplyJson;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_banks = UserBank::where("user_id", Auth::user()->id)->get();
        if ($request->ajax()) {
            $keywords = $request->keywords;
                $collections = UserBank::where([
                    ['account_number','like', "%$keywords%"], ["user_id", Auth::user()->id]
                ])->paginate(10);
                // dd($collections[0]->banks);
                return $this->render_view('profile.bank_account.list', compact('collections'));
        }
        return $this->render_view('profile.bank_account.main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $master = Bank::get();
        // dd($master);
        return $this->render_view('profile.bank_account.input', ['data' => new UserBank, 'master' => $master]);
    }

    public function add_modal()
    {
        $master = Bank::get();
        // dd($master);
        return $this->render_view('profile.bank_account.input', ['master' => $master]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $validator = Validator::make($request->all(), [
            'bank' => 'required',
            'account_number' => 'required|unique:user_banks',
            'branch_name' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('bank')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bank'),
                ]);
            }else if ($errors->has('account_number')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_number'),
                ]);
            }else if ($errors->has('branch_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('branch_name'),
                ]);
            }
        }
        $id = Auth::guard('web')->user()->id;
        if (UserBank::where('user_id', $id)->get()->count() >= 3) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Anda hanya bisa menambahkan 3 akun bank, di akun bajaga anda!',
            ], 500);
        }
        // tinggal save data ke db
        // dd($request);
        $bank = new UserBank;
        $user = UserBank::where('user_id', $id)->first();

        if (!$user) {
            $bank->is_active = true;
        }
        
        $bank->user_id = $id;
        $bank->bank_id = $request->bank_id;
        $bank->account_holder_name = $request->account_holder_name;
        $bank->account_number = $request->account_number;
        $bank->branch_name = $request->branch_name;
        
        $bank->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'No Rekening '. $request->account_number . ' tersimpan',
            'redirect' => "reload",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserBank  $userBank
     * @return \Illuminate\Http\Response
     */
    public function show(UserBank $userBank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserBank  $userBank
     * @return \Illuminate\Http\Response
     */
    public function edit(UserBank $userBank)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserBank  $userBank
     * @return \Illuminate\Http\Response
     */
    public function is_use(Request $request)
    {
        try {
            UserBank::where("user_id", Auth::user()->id)->update([
                "is_active" => false
            ]);

            UserBank::where("id", $request->id)->update([
                "is_active" => true
            ]);

            return response()->json([
                "alert" => "success",
                "message" => "successfully changed the active bank"
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "alert" => "error",
                "message" => "failed changed the active bank"
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserBank  $userBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserBank $userBank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserBank  $userBank
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserBank $userBank)
    {
        $userBank->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Rekening berhasil terhapus'
        ]);
    }

    public function getDataBankUser(Request $request)
    {
        $collections = UserBank::where("user_id", Auth::user()->id)->get();
        $keywords = $request->keywords;
        if ($keywords) {
            $collections = UserBank::where([
                ['account_number','like', "%$keywords%"], ["user_id", Auth::user()->id]
            ])->paginate(10);
        }
        return $this->render_view('profile.bank_account.list', compact('collections'));
    }
}
