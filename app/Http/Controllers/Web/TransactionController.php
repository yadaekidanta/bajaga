<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Helper;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Discount;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserBalance;
use App\Models\UserStore;
use App\Notifications\TransactionNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToArray;
use Xendit\Xendit;

class TransactionController extends Controller
{
    use ResponseView, ReplyJson;
    
    public function index(Request $request)
    {
        $user = Auth::guard('web')->user();
        $user_id = $user->id;
        $point_user = $user != null ? true : 0;
        if ($point_user) {
            $point_user = $user->point_user != null ? $user->point_user->point : 0 ;
        }
        $point_store = 0;
        $available = 0;
        if ($user) {
            $available = UserStore::where('users_id',$user_id)->first();
            // dd($available->point_store);
            $point_store = 0;
            if ($available) {
                if ($available->point_store) {
                    $available->point_store->point;
                }
            }
            // dd($point_store);
            if(!$available) {
                $available = 1;
            }else{
                $available = 2;
            }
        }


        $user = Auth::guard('web')->user();
        $user_id = $user->id;
        $output = '';
        $cek = \Cart::session($user_id)->isEmpty();
        // print_r($cek);exit;
        $collection = \Cart::session($user_id)->getContent();
        $subtotal = \Cart::session($user_id)->getSubTotal();
        $total = \Cart::session($user_id)->getTotal();
        $total_qty = \Cart::session($user_id)->getTotalQuantity();
        $total_item = $collection->count();
        if ($request->ajax()) {
            // dd("sad");
            if ($request->filter) {
                $filter = $request->filter;
                $collection = Sale::where([
                    ["customer_id", $user_id], ["st", $filter]
                ])->paginate(2);
                // dd($collection);
            }else if ($request->search) {
                $filter = $request->search;
                // dd($filter);
                $collection = Sale::where([
                    ["customer_id", $user_id], ["customer_name","like", "%$filter%"]
                ])->orWhere([
                    ["customer_id", $user_id], ["no_resi","like", "%$filter%"]
                ])->paginate(2);
            }else{
                // dd("sad");
                $collection = Sale::where("customer_id", $user_id)->paginate(2);
            }
            // $collection = Sale::where("customer_id", $user)->paginate(10);
            // dd($collection);
            return $this->render_view('transaction.data', compact('collection'));
        }
        $count_cupon = Discount::get()->count();
        return $this->render_view('transaction.main',compact('collection','user', 'available', 'count_cupon', 'total_item','subtotal','total_qty','total', 'point_user', 'point_store'));
    }

    public function detail(Sale $sale)
    {
        // cek apakah barang sudah sampai atau tidak
        $delivery_status = $this->detailShipping($sale)->response["delivery_status"];
        $status = $delivery_status["status"];
        $pod_date = $delivery_status["pod_date"];
        if ($status == "DELIVERED") {
            if ($sale->st == "Dikirim" || $sale->st == "Sampai") {
                // lakukan update status
                if ((strtotime($pod_date) + (60 * 60 * 24)) < time()) {
                    $sale->update([
                        "st" => "Diterima"
                    ]);
    
                    // lakukan pencairan dana
                    UserBalance::where("code", $sale->code)->update([
                        "status" => "PAID"
                    ]);
                }else{
                    if ($sale->st != "Sampai") {
                        $sale->update([
                            "st" => "Sampai"
                        ]);
                    }
                }
            }

            // cek masa ulasan apakah masih tersedia 
            if ($sale->st == "Diterima") {
                if((strtotime($pod_date) + ((60 * 60 * 24) * 2)) < time()) {
                    // jika tidak maka selesaikan status sale 
                    
                    // decresee stock barang
                    foreach ($sale->sale_detail()->get() as $key => $value) {
                        $value->product->stock -= $value->qty;
                    }
                    
                    $sale->update([
                        "st" => "Selesai"
                    ]);
                }
            }

        }
        return $this->render_view("transaction.detail", compact("sale"));
    }

    public function detailShipping(Sale $sale)
    {
        $waybill = $sale->no_resi;
        $courier = $sale->code_courier;
        

        $dataPost = [
            "waybill" => $waybill,
            "courier" => $courier,
        ];

        $response = null;
        
        try {
            $data = Http::asForm()->withHeaders([
                'key' => env('RAJAONGKIR_KEY'),
                'content-type' => 'application/x-www-form-urlencoded'
            ])->post(env('RAJAONGKIR_BASE_URL') . '/waybill', $dataPost);
                
            // return $data->json();
            $response = json_decode($data, true);
            $delivery_status = $response["rajaongkir"]["result"]["delivery_status"];
            $manifest = $response["rajaongkir"]["result"]["manifest"];
            $response = [
                "delivery_status" => $delivery_status,
                "manifest" => $manifest,
            ];
            // dd($response);
            return $this->render_view("shipping.main", compact("response", "sale"));
        } catch (\Exception $e) {
            $response = null;
        }
        // dd($response);
        return $this->render_view("shipping.main", compact("response", "sale"));
    }

    public function getDetailShipping(Sale $sale)
    {
        // dd("sad");
        $waybill = $sale->no_resi;
        $courier = $sale->code_courier;
        

        $dataPost = [
            "waybill" => $waybill,
            "courier" => $courier,
        ];

        $response = null;
        
        try {
            $data = Http::asForm()->withHeaders([
                'key' => env('RAJAONGKIR_KEY'),
                'content-type' => 'application/x-www-form-urlencoded'
            ])->post(env('RAJAONGKIR_BASE_URL') . '/waybill', $dataPost);
                
            // return $data->json();
            $response = json_decode($data, true);
            // dd($response);
            if ($response["rajaongkir"]["result"]["delivered"]) {
                // jika true maka lakukan update st pesanan sudah sampai
                $delivery_status = $response["rajaongkir"]["result"]["delivery_status"];
                $pod_date = $delivery_status["pod_date"];
                if ($sale->st == "Dikirim" || $sale->st == "Sampai") {
                    // lakukan update status
                    if ((strtotime($pod_date) + (60 * 60 * 24)) < time()) {
                        $sale->update([
                            "st" => "Diterima"
                        ]);
        
                        // lakukan pencairan dana
                        UserBalance::where("code", $sale->code)->update([
                            "status" => "PAID"
                        ]);
                    }else{
                        if ($sale->st != "Sampai") {
                            $sale->update([
                                "st" => "Sampai"
                            ]);
                        }
                    }
                }

                // cek masa ulasan apakah masih tersedia 
                if ($sale->st == "Diterima") {
                    if((strtotime($pod_date) + ((60 * 60 * 24) * 2)) < time()) {
                        // jika tidak maka selesaikan status sale 

                        // decresee stock barang
                        foreach ($sale->sale_detail()->get() as $key => $value) {
                            $value->product->stock -= $value->qty;
                        }
                        
                        $sale->update([
                            "st" => "Selesai"
                        ]);
                    }
                }
            }
            $delivery_status = $response["rajaongkir"]["result"]["delivery_status"];
            $manifest = $response["rajaongkir"]["result"]["manifest"];
            $response = [
                "delivery_status" => $delivery_status,
                "manifest" => $manifest,
            ];
            return $this->render_view("product.history.transaction.detail_shipping", compact("response", "sale"));
        } catch (\Exception $e) {
            // dd($e);
            return response()->json([
                "alert" => "error",
                "message" => "Ada yang salah, pastikan internet anda tersedia!"
            ]);
            $response = null;
        }
        // dd("asd");
        return $this->render_view("product.history.transaction.detail_shipping", compact("response", "sale"));
    }
    
    public function getModalUlasan(Sale $sale)
    {
        return $this->render_view("transaction.modal_review", compact("sale"));
    }
    

    public function transaction(Request $request)
    {
        // dd("asd");
        if (is_array($request["data"])) {
            $store = Product::whereIn("id", $request["data"])->get("store_id")->map(function($data){
                return $data["store_id"];
            })->toArray();
        }else{
            $tmp = $request->data;
            $request->data = [];
            array_push($request->data, $tmp);
            $store = [];
            $store_id = Product::where("id", $request["data"])->first()->store_id;
            array_push($store, $store_id);
        }

        
        $user_address = UserAddress::where("users_id", Auth::user()->id)->first();
        if (!$user_address) {
            return response()->json([
                "message" => "Anda belum menambahkan alamat, di akun anda",
            ])->setStatusCode(404);
        }

        // dd($request->data, $store);

        
        session(['data' => ["store_id" => array_unique($store), "data_product" => $request->data]]);
        return response()->json([
            "alert" => "success",
            "redirect" => "/checkout",
            "data" => $store
        ]);
    }

    public function invoice(Request $request)
    {
        // dd($request->product);
        $code = $request->code; 
        $customer_id = $request->customer_id;
        $customer_name  = $request->customer_name;
        $customer_phone  = $request->customer_phone;
        $date = $request->date; 
        $note = $request->note;
        $type = $request->type;
        $st = "Tertunda";
        $payment_st = $request->payment_st;
        foreach ($request->product as $store) {
            // dd($store);
            $store_id  = $store["store_id"];
            $shipping_detail  = $store["shipping_detail"];
            $shipping_price  = $store["shipping_price"];
            $courier  = $store["courier"];
            $code_courier  = $store["code_courier"];
            $disc_price = $store["diskon_per_store"];
            $total_paid = $store["bill_per_store"];
            $grand_total = $store["bill_per_store"];
            $total = $store["price_per_store"]; 
            $sale_id = 0;
            try {
                $sale_id = Sale::create([
                    "code" => $code,
                    "customer_id" => $customer_id,
                    "date" => $date,
                    "total" => $total,
                    "disc_price" => $disc_price,
                    "total_paid" => $total_paid,
                    "grand_total" => $grand_total,
                    "note" => $note,
                    "customer_name" => $customer_name,
                    "customer_phone" => $customer_phone,
                    "type" => $type,
                    "courier" => $courier,
                    "code_courier" => $code_courier,
                    "st" => $st,
                    "payment_st" => $payment_st,
                    "store_id" => $store_id,
                    "shipping_detail" => $shipping_detail,
                    "shipping_price" => $shipping_price,
                    "created_by" => 1,
                ])->id;
            } catch (\Throwable $th) {
                return response()->json([
                    'status' => 'error',
                    'message' => $th->getMessage(),
                ]);
            }
            
            $data = $store["data"];
            foreach ($data as $product) {
                // dd($product["price"]);
                $product_id  = $product["product_id"];
                $diskon_per_product  = $product["diskon_per_product"];
                $price  = $product["price"];
                $qty  = $product["qty"];
                $subtotal  = $product["subtotal"];
                try {
                    SaleDetail::create([
                        "sale_id" => $sale_id,
                        "product_id" => $product_id,
                        "disc_price" => $diskon_per_product,
                        "price" => $price,
                        "shipping_price" => $shipping_price,
                        "qty" => $qty,
                        "subtotal" => $subtotal,
                    ]);
                } catch (\Throwable $th) {
                    Sale::where("id", $sale_id)->delete();
                    
                    return response()->json([
                        'status' => 'error',
                        'message' => $th->getMessage(),
                    ]);
                }
            }

        }

        return response()->json([
            'status' => 'success',
            'message' => "success create user invoice",
        ]);

    }
    public function success()
    {
        return $this->render_view("transaction.done");
    }

    public function failed(Request $request)
    {
        return $this->render_view("transaction.failed");
    }

    public function invoice_callback(Request $request)
    {
        // return response()->json([
        //     "alert" => "success",
        //     "message" => "Success resolve invoice callback"
        // ]);

        // check header for authorization the requests
        if ($request->header("x-callback-token") != env("TOKEN_VERIFIKASI")) {
            return response()->json([
                "alert" => "Error",
                "message" => "Sorry we cann't resolve this request"
            ], 401);
        }
        
        try {
            $product_id = [];
            $customer_id = 0;
            if ($request->status == "PAID") {
                $id_invoice = $request->id;
                $transaction = Sale::where("code", $id_invoice)->get();
                foreach ($transaction as $trn) {
                    
                    // update status payment
                    $id_seller = $trn->store->user->id;
                    $paid_amount = $trn->total_paid;
                    $paid_at = $request->paid_at;
                    UserBalance::create([
                        // "id_transaction" => $id_seller,
                        "code" => $id_invoice,
                        "user_id" => $id_seller,
                        "amount" => $paid_amount,
                        "type" => "T",
                        "status" => "Diverifikasi",
                        "created_at" => $paid_at,
                        "updated_at" => $paid_at,
                    ]);
                    $trn->payment_st = "Lunas";
                    $trn->payment_method = $request->payment_method;
                    $trn->payment_channel = $request->payment_channel;
                    $trn->st = "Dipesan";
                    $trn->save();
    
                    array_push($product_id, $trn->sale_detail()->get());
                }
                // dd($transaction->customer);
                $customer_id = $transaction[0]->customer_id;
                $customer = User::where("id", $customer_id)->first();
                // dd($customer);
                $details = [
                    'greeting' => "Hi ".$customer->name,
                    'body' => 'Pesanan kamu sudah diverifikasi otomatis oleh bajaga, selanjutnya pesanan akan diteruskan ke penjual.'
                ];
                $user = $customer;

                Notification::send($user, new TransactionNotification($user, $details));
            }else{
                $id_invoice = $request->id;
                $user_balance = UserBalance::where("code", $id_invoice)->first();
                $user_balance->update([
                    "status" => $request->status,
                ]);
                $transaction = Sale::where("code", $id_invoice)->first();
                $transaction->update([
                    "st" => "Dibatalkan"
                ]);
                
                $customer_id = $transaction->customer_id;
                $customer = User::where("id", $customer_id)->first();
                // dd($customer);
                // send notification to user
                $details = [
                    'greeting' => "Hi ".$transaction->customer->name,
                    'body' => 'Pesanan kamu sudah dibatalkan otomatis oleh bajaga!'
                ];
                $user = $customer;

                Notification::send($user, new TransactionNotification($user, $details));

                return response()->json([
                    "alert" => "success",
                    "message" => "Success resolve invoice callback"
                ]);
            }

            foreach ($product_id as $prodid) {
                // delete product in the cart
                foreach ($prodid as $key) {
                    Cart::where([
                        ["user_id", $customer_id],
                        ["product_id", $key->product_id],
                    ])->delete();
                }
            }

            return response()->json([
                "alert" => "success",
                "message" => "Success resolve invoice callback"
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "message" => $th->getMessage() ? $th->getMessage() : "Internals server error"
            ], 500);
        }
        
    }

    public function invoice_detail(Request $request)
    {
        Xendit::setApiKey(env('XENDIT_KEY'));
        $id_invoice = $request->id_invoice;
        // check jika dia pemilik toko atau tidak
        // get customer id
        $id_user = Auth::user()->id;

        // cek apakah dia pembeli
        $sale = Sale::where([
            ["customer_id", $id_user],["code", $id_invoice]
        ])->first();
        
        if (!$sale) {
            // jika masuk disini maka dia penjual
            $sale = Auth::user()->store->sales->where("code", $id_invoice)->first();
            if (!$sale) {
                // jika di false maka unauthorized
                return back();
            }
        }

        try {
            // $data = null;
            $data = \Xendit\Invoice::retrieve($id_invoice);
            // dd($data);
            return $this->render_view("transaction.invoice", compact("data", "sale"));
        } catch (\Exception $e) {
            $data = null;
            // dd($e);
            return $this->render_view("transaction.invoice", compact("data", "sale"));
        }

    }
}
