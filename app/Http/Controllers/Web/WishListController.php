<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\WishList;
use Illuminate\Http\Request;
use App\Traits\ResponseView;
use App\Traits\ReplyJson;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    use ResponseView, ReplyJson;
    public function index(Request $request)
    {
        try {
            $user = Auth::user();
            $list_category = ProductCategory::where('product_category_id','=',0)->get();
            $collections = WishList::where("users_id", $user->id)->paginate(10);
            if ($request->ajax()) {
                $category_id = $request->category;
                if ($category_id) {
                    $collections = WishList::where("users_id", $user->id)->whereHas("products", function($data) use($category_id){
                        return $data->where("product_category_id", $category_id);
                    })->paginate(10);
                    // dd($collections);
                }
                return $this->render_view("wishlist.data", compact("collections")); 
            }
            // dd($collections[0]->products);
            return $this->render_view("wishlist.main", compact("collections", "list_category"));
        } catch (\Throwable $th) {
            return response()->json([
                "alert" => "error",
                "message" => $th->getMessage()
            ]);
        }
    }

    public function store(Request $request)
    {
        // return $request->id_product;
        try {
            $users_id = Auth::user()->id;
            $wl = WishList::where([
                ["users_id", $users_id],
                ["id_product", $request->id_product],
            ])->first();
            if (!$wl) {
                WishList::create([
                    "users_id" => $users_id,
                    "id_product" => $request->id_product,
                ]);
            }
    
            return response()->json([
                'alert' => 'success',
                'message' => 'Success add to wishlist',
                "redirect" => "reload"
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function destroy($id)
    {
        $wl = WishList::where([
            ['users_id', Auth::user()->id],
            ['id_product', $id],
        ])->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Success delete wishlist',
        ]);
    }
}
