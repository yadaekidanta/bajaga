<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\UserAddress;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Province;
use App\Models\UserBank;
use App\Models\UserStore;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    use ResponseView, ReplyJson;
    public function index(Request $request)
    {
        $user = Auth::user();
        $address = UserAddress::where('users_id', $user->id)->get();
        $bank = UserBank::where("user_id", Auth::user()->id)->get();
        $provinsi = Province::get();
        if($request->ajax() ){
            return $this->render_view('profile.list', compact('user','address'));
        }else{
            $point_user = $user != null ? true : 0;
            if ($point_user) {
                $point_user = $user->point_user != null ? $user->point_user->point : 0 ;
            }
            $point_store = 0;
            $available = 0;
            if ($user) {
                $available = UserStore::where('users_id',$user->id)->first();
                if ($available) {
                    if ($available->point_store) {
                        $point_store = $available->point_store->point;
                    }
                }
                // dd($point_store);
                if(!$available) {
                    $available = 1;
                }else{
                    $available = 2;
                }
            }
            // dd($available);
            $count_cupon = Discount::where(
            [
                ["end_at", ">", now()], 
                ["st", "aktif"]
            ])->get()->count();
            return $this->render_view('profile.main', compact('user', 'bank',
            "point_store", 'provinsi', 'count_cupon', 'available', 'point_user', 'address'));
        }
    }

    public function create()
    {
     // 
    }

    public function store()
    {
        
    }

    public function show()
    {
        
    }

    public function edit(User $user)
    {
        return $this->render_view('profile.input', compact('user'));
    }
    
    public function update(Request $request, User $user)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'nik' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'username' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('nik')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nik'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif($errors->has('username')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('username'),
                ]);
            }
        }
        $user->nik = $request->nik;
        $user->name = Str::title($request->name);
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->username = $request->username;

        if (request()->file('image')) {
            $file = request()->file('image')->store("avatars");
            $user->avatar = $file;
        }

        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Profile '. $request->name . ' terupdate',
            'redirect' => 'reload'
        ]);
    }

    public function update_photo(Request $request, User $user)
    {
        // dd($request->all());
        try {
            $validator = Validator::make($request->all(), [
                'avatar' => 'required|mimes:jpg,jpeg,png,PNG,JPG,JPEG |max:10096'
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                if ($errors->has('avatar')) {
                    return response()->json([
                        'alert' => 'error',
                        'message' => $errors->first('avatar'),
                    ], 500);
                }
            }
            // dd(request()->file('avatar'));
            $file = request()->file('avatar')->store("avatars");
            
            $user->avatar = $file;

            $user->update();
    
            return response()->json([
                'alert' => 'success',
                'message' => 'Foto berhasil terupdate',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => "Coba lagi",
            ], 500);
        }
    }

    public function updateAddressShow(UserAddress $user)
    {
        return $this->render_view('profile.address.input', compact('user'));
    }


    public function account_security()
    {
        return $this->render_view('profile.account_security.main');
    }

    public function account_security_password()
    {
        $user = Auth::user();
        return $this->render_view('profile.account_security.input_password', compact('user'));
    }

    public function account_security_password_store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed',
        ]);
        $user = Auth::user();
        User::where("id", $user->id)->update([
            "password" => Hash::make($request->password)
        ]);
        session()->flash('success', 'Register was successfull');
        if ($request->ajax()) {
            return response()->json([
                'alert' => 'success',
                'message' => 'Password berhasil terupdate',
                'redirect' => 'reload'
            ]);
        }
        return $this->render_view('profile.account_security.input_password', compact('user'));
    }

    public function updateAddress(UserAddress $user)
    { 
        $user->is_use = '1';
        $user->update();
        $address = UserAddress::where('users_id',Auth::user()->id)->where('id','!=',$user->id)->get();
        foreach ($address as $key => $value) {
            $value->is_use = '0';
            $value->update();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Alamat berhasil digunakan',
        ]);
    }

    public function getDataUser(){
        // return "damn";
        $user = Auth::user();
        return response()->json([
            'message' => 'Success retrieve data user',
            "user" => $user
        ]);
    }

    public function getDataBiodata() {
        // get data untuk list result di page profile.main
        $user = Auth::user();
        return $this->render_view('profile.biodata.list', compact('user'));
    }
}
