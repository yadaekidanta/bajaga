<?php

namespace App\Http\Controllers\Web;

use App\Models\Product;
use App\Traits\ReplyJson;
use App\Models\SaleDetail;
use App\Models\ProductBrand;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use App\Models\ProductLelang;
use App\Models\ProductReview;
use App\Models\ProductCategory;
use App\Models\ProductAuctionBid;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\City;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\Province;
use App\Models\Subdistrict;
use App\Models\UserAddress;
use App\Models\UserStore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WebController extends Controller
{
    use ResponseView, ReplyJson;
    public function index(Request $request)
    {
        
        $user = Auth::user();
        $banner = Banner::get();
        $point_user = $user != null ? true : 0;
        if ($point_user) {
            $point_user = $user->point_user != null ? $user->point_user->point : 0 ;
        }
        $point_store = 0;
        $available = 0;
        if ($user) {
            $available = UserStore::where('users_id',$user->id)->first();
            $point_store = $available != null && $available->point_store != null ? $available->point_store->point : 0;
            // dd($point_store);
            if(!$available) {
                $available = 1;
                $point_store = 0;
            }else{
                $available = 2;
            }
        }

        
        $category = ProductCategory::where('product_category_id',0)->get();
        $subCategory = ProductCategory::where('product_category_id','!=',0)->get();
        $brand = ProductBrand::limit(16)->get();
        $trending = SaleDetail::select('product_id')->groupBy('product_id')->orderBy(\DB::raw('count(product_id)'), 'DESC')->limit(10)->get();
        // $rating = ProductReview::where('product_id','=',$product->id)->avg('rate');
        // dd($trending);
        $product_pilihan = Product::paginate(10); // ganti menurut logika bisnis
        if ($request->ajax()) {
            return $this->render_view('home.data_product', compact('product_pilihan'));
        }
        return $this->render_view('home.main',compact('category', 'banner', 'product_pilihan','brand','subCategory','trending', 'available',  'point_user', 'point_store'));
    }
    public function about()
    {
        return $this->render_view('about.main');
    }
    public function help()
    {
        $categories = FaqCategory::get();
        $faqs = Faq::get();
        return $this->render_view('help.main', compact("categories", "faqs"));
    }
    public function terms()
    {
        return $this->render_view('terms.main');
    }
    public function privacy()
    {
        return $this->render_view('privacy.main');
    }
    public function searching(Request $request){
        return redirect()->route('web.result', ['keyword' => $request->keyword]);
    }
    public function result($keyword){
        return $this->render_view('search.main');
    }

    public function auction(Request $request)
    {
        $num_paginate = 6;
        $now = date("Y-m-d");
        $products = ProductLelang::where('end','>=',$now)->where('st', 'aktif')->paginate(6);
        $data['products'] = $products;
        $data['list_category'] = ProductCategory::where('product_category_id', 0)->get();
        // dd($data['products']);
        if ($products->count() > $num_paginate) {
            if($request->ajax()){
                $view = $this->render_view('auction.data_list_auction',compact('products'))->render();
                return response()->json(['html'=>$view]);
            }
        }
        return $this->render_view('auction.index', $data);
    }

    public function auction_store(Request $request)
    {
        $req = $request->all();
        $req['bid'] = intval(str_replace(".", "", $request->bid));
        
        // dd($req['product_id']);
        $request->request->add(['user_id' => Auth::user()->id]);
        $validator = Validator::make($req, [
            'bid' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('bid')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bid'),
                ]);
            }
        }

        if ($req['bid'] > $request->price) {
            $category = ProductAuctionBid::create([
                "product_id" => $req['product_id'],
                "user_id" => Auth::user()->id,
                "bid" => $req['bid']
            ]);
            return response()->json([
                'alert' => 'success',
                'redirect' => 'reload',
                'message' => 'Bid tersimpan',
            ]);
        } else {
            return response()->json([
                'alert' => 'info',
                'message' => 'Bid tidak boleh lebih kecil dari harga',
            ]);
        }
    }

    public function modal(){
        $provinsi = Province::get();
        $address = UserAddress::where('users_id',Auth::user()->id)->get();
        return $this->render_view('alamat.input_modal',compact('provinsi','address'));
    }

    public function auction_show(Request $request,ProductLelang $productLelang)
    {
        // dd($productLelang);
        $collection = ProductAuctionBid::where('product_id',$productLelang->product_id)->orderBy('bid', 'DESC')->get();
        $bidCount = $collection->count();
        if ($request->ajax()) {
            return $this->render_view('auction.list', compact('collection'));
        }
        return $this->render_view('auction.show', compact('productLelang', 'bidCount'));
    }
}
