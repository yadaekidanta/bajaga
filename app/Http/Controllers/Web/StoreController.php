<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Courier;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductReview;
use App\Models\Province;
use App\Models\Sale;
use App\Models\StoreFavorite;
use App\Models\User;
use App\Models\user_have_courier;
use App\Models\UserBalance;
use App\Models\UserBank;
use App\Models\UserStore;
use App\Notifications\TransactionNotification;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Darryldecode\Cart\Validators\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;

class StoreController extends Controller
{
    use ResponseView, ReplyJson;

    public function detail(UserStore $userStore, Request $request)
    {
        $collections = $userStore->product()->paginate(10); // TODO: Change this with (product terbaru, terlaris, dan collection all product)
        $store = $userStore;
        
        $user = Auth::user();
        $state_follow = false;
        if ($user) {
            $check_state = StoreFavorite::where([
                ["store_id", $userStore->id],
                ["user_id", $user->id]
            ])->first();
            if ($check_state) {
                $state_follow = true;
            } else {
                $state_follow = false;
            }
            
        }

        // buat rating dari rate per produk
        $rating = 0;
        $perPeroduct = $userStore->product()->get();
        $countProduct = count($perPeroduct); 
        foreach ($perPeroduct as $key => $value) {
            if ($value->product_reviews->avg("rate")) {
                $rating += $value->product_reviews->avg("rate");
            }else{
                $rating += 0;
                $countProduct--;
            }
        }
        
        $categories = ProductCategory::get();
        $etalase = $userStore->user->etalase;

        $collection_review = $userStore->product()->paginate(10)->filter(function($data){
            // dd(count($data->product_reviews()->get()));
            if (count($data->product_reviews()->get()) > 0) {
                return $data;
            }
        });

        $collection_review = $collection_review->take(5);


        // dd($collections);
        if ($request->ajax()) {
            $keywords = $request->keywords;
            if ($keywords) {
                $collections = Product::where('product_category_id', "$keywords") 
                ->where('store_id',$userStore->id)->paginate(6);
            }else{
                $collections = Product::where('store_id',$userStore->id)->paginate(10);
            }
            return $this->render_view("store.data", compact("collections", "store"));
        }
        return $this->render_view("store.main", compact("store" , "rating" ,"collections", "categories", "etalase", "state_follow", "collection_review"));
    }
    
    public function main_setting(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $store_id = $user->store->id;
        $available = UserStore::where('users_id',Auth::user()->id)->first();
        $wSum = UserBalance::where([
            ["user_id", $user_id], ["type", "W"] , ["status", "COMPLETED"]
        ])->sum("amount");
        $tSum = UserBalance::where([
            ["user_id", $user_id], ["type", "T"], ["status", "PAID"]
        ])->sum("amount");
            
        $balance = $tSum - $wSum;

        $point_user = $user != null ? true : 0;
            if ($point_user) {
                $point_user = $user->point_user != null ? $user->point_user->point : 0 ;
            }
            $point_store = 0;
            if ($user) {
                $available = UserStore::where('users_id',$user->id)->first();
                // dd($available->point_store);
                $point_store = 0;
                if ($available) {
                    if ($available->point_store) {
                        $point_store = $available->point_store->point;
                    }
                }
            }
        // request ajax dipakai ketika di halaman desktop, mobile tidak
        if ($request->ajax()) {
            return $this->render_view("product.settings.data", compact("available"));
        }
        return $this->render_view("product.settings.main", compact("available", "balance", "point_store"));
    }
    
    public function profile_setting()
    {
        $user_id = Auth::user()->id;
        $store = UserStore::where("users_id", $user_id)->first();
        return $this->render_view("product.settings.profile.input", compact("store"));
    }
    
    public function profile_setting_update(Request $request)
    {
        // dd($request->all());
        $store = Auth::user()->store;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }

        $store->name = $request->name;
        $store->start_at = $request->start_at;
        $store->end_at = $request->end_at;
        $store->desc = $request->desc;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("store");
            $store->photo = $photo;
        }
        $store->save();
        return response()->json([
            'alert' => 'success',
            'message' => "Berhasil perbarui data store!",
        ]);
        // return $this->render_view("product.settings.profile.input");
    }
    
    public function product_setting(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $store_id = $user->store->id;
        $wSum = UserBalance::where([
            ["user_id", $user_id], ["type", "W"] , ["status", "COMPLETED"]
        ])->sum("amount");
        $tSum = UserBalance::where([
            ["user_id", $user_id], ["type", "T"], ["status", "PAID"]
        ])->sum("amount");
            
        $balance = $tSum - $wSum;
        $point_store = 0;
        $available = UserStore::where('users_id',$user_id)->first();
        if ($available->point_store) {
            $point_store = $available->point_store->point;
        }
        $store_id = UserStore::where('users_id',Auth::user()->id)->first()->id;
        if ($request->ajax()) {
            $keywords = $request->keywords;
            if ($keywords) {
                $collections = Product::where('name', 'like',  "%$keywords%") 
                                ->where('store_id',$store_id)->paginate(6);   
            }else{
                $collections = Product::where('store_id',$store_id)->paginate(6);
            }
            return $this->render_view('product.settings.product.data_product',compact('collections'));
        }
        return $this->render_view("product.settings.product.main", compact("available", "balance", "point_store"));
    }

    public function product_setting_price(Request $request)
    {
        $this->validate($request, [
            "id_product" => "required",
            "price" => "required|numeric",
        ]);
        // dd($request->price, $request->id_product);
       $product = Product::where("id", $request->id_product)->first();
       $product->price = $request->price;
       $product->save();

       return response()->json([
            'alert' => 'success',
            'message' => "Berhasil perbarui harga produk!",
        ]);
    }

    public function product_setting_stock(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            "id_product" => "required",
            "stock" => "required",
        ]);
        // dd($request->price, $request->id_product);
       $product = Product::where("id", $request->id_product)->first();
       $product->stock = $request->stock;
       $product->save();

       return response()->json([
            'alert' => 'success',
            'message' => "Berhasil perbarui harga produk!",
        ]);
    }
    
    public function address_store(Request $request)
    {
        $address = Auth::user()->store;
        $provinsi = Province::get();
        if ($request->ajax()) {
            return $this->render_view("product.settings.shipping.data_location", compact("address", "provinsi"));
        }
        return $this->render_view("product.settings.shipping.store_location", compact("address", "provinsi"));
    }
    
    public function address_store_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'province_id' => 'required',
            'city_id' => 'required',
            'subdistrict_id' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if($errors->has('province_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province_id'),
                ]);
            }elseif($errors->has('city_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city_id'),
                ]);
            }elseif($errors->has('subdistrict_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict_id'),
                ]);
            }elseif($errors->has('address')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }
        }

        $userstore = UserStore::where("id", $id)->first();
        $userstore->province_id = $request->province_id;
        $userstore->city_id = $request->city_id;
        $userstore->subdistrict_id = $request->subdistrict_id;
        $userstore->address = $request->address;
        $userstore->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Etalase '. $request->name . ' berhasil di update',
            'redirect' => 'reload'
        ]);
    }
    
    public function shipping_services()
    {
        $collections = Courier::get();
        $store_id = Auth::user()->store->id;
        $collections->map(function($data){
            $cond = user_have_courier::where("courier_id", $data->id)->where("store_id", $data->id)->first();
            if ($cond) {
                $data["status"] = true;
            }else{
                $data["status"] = false;
            }
        });

        // dd($collections);
        return $this->render_view("product.settings.shipping.courier", compact('collections'));
    }
    
    public function shipping_services_create(Request $request)
    {
        $user_have_courier = new user_have_courier;
        $user_have_courier->store_id = Auth::guard('web')->user()->store->id;
        $user_have_courier->courier_id = $request->courier_id;
        $user_have_courier->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Etalase '. $request->name . ' tersimpan',
        ]);
    }
    
    public function oprational(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_at' => 'required',
            'end_at' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if($errors->has('start_at')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('start_at'),
                ]);
            }elseif($errors->has('end_at')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('end_at'),
                ]);
            }
        }
        $store = Auth::user()->store;
        $store->update([
            "start_at" => $request->start_at,
            "end_at" => $request->end_at
        ]);
        // dd($store);
        return response()->json([
            'alert' => 'success',
            'message' => "Berhasil update jam oprasional toko",
            'redirect' => "reload",
        ]);
    }
    
    public function history_transaction(Request $request)
    {
        // doing this in this controller
        $user = Auth::user();
        $user_id = $user->id;
        $store_id = $user->store->id;
        $wSum = UserBalance::where([
            ["user_id", $user_id], ["type", "W"] , ["status", "COMPLETED"]
        ])->sum("amount");
        $tSum = UserBalance::where([
            ["user_id", $user_id], ["type", "T"], ["status", "PAID"]
        ])->sum("amount");

        $point_user = $user != null ? true : 0;
        if ($point_user) {
            $point_user = $user->point_user != null ? $user->point_user->point : 0 ;
        }
        $point_store = 0;
        
        $balance = $tSum - $wSum;
        $available = UserStore::where('users_id',$user_id)->first();
            // $available = UserStore::where('users_id',$user->id)->first();
            // dd($available->point_store);
        $point_store = 0;
        if ($available) {
            if ($available->point_store) {
                $point_store = $available->point_store->point;
            }
        }
        if ($request->filter) {
            $filter = $request->filter;
            $collections = Sale::where([
                ["store_id", $store_id], ["st", $filter]
            ])->paginate(10);
            // dd($collections);
        }else if ($request->search) {
            $filter = $request->search;
            // dd($filter);
            $collections = Sale::where([
                ["store_id", $store_id], ["customer_name","like", "%$filter%"]
            ])->orWhere([
                ["store_id", $store_id], ["no_resi","like", "%$filter%"]
            ])->paginate(10);
        }else{
            $collections = Sale::where("store_id", $store_id)->paginate(10);
        }

        if ($request->ajax()) {
            return $this->render_view("product.history.transaction.data", compact("collections"));
        }
        return $this->render_view("product.history.transaction.main", compact("collections", "balance", "available", "point_store", "point_user", "point_store"));
    }

    public function history_transaction_getDetailModal(Sale $sale)
    {
        // dd($sale);
        return $this->render_view("product.history.transaction.detail_modal", compact("sale"));
    }
    
    
    public function review(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $available = UserStore::where('users_id',Auth::user()->id)->first();
        $wSum = UserBalance::where([
            ["user_id", $user_id], ["type", "W"] , ["status", "COMPLETED"]
        ])->sum("amount");
        $tSum = UserBalance::where([
            ["user_id", $user_id], ["type", "T"], ["status", "PAID"]
        ])->sum("amount");

        $point_store = 0;
        if ($available) {
            if ($available->point_store) {
                $point_store = $available->point_store->point;
            }
        }
            
        $balance = $tSum - $wSum;
        $collection = UserStore::where("users_id", $user_id)->first()->product()->paginate(10)->filter(function($data){
            // dd(count($data->product_reviews()->get()));
            if (count($data->product_reviews()->get()) > 0) {
                return $data;
            }
        });
        // dd($collection);
        if ($request->ajax()) {
            if ($request->filter) {
                $filter = $request->filter;
                if ($filter == "Baru") {
                    $collection = UserStore::where("users_id", $user_id)->first()->product()->paginate(10)->filter(function($data){
                        
                        foreach ($data->product_reviews()->get() as $key => $value) {
                            if ($value->read_at == null) {
                                return $data;
                            }
                        }
                    });
                }
            }
            // dd($collection);
            return $this->render_view("product.review.data", compact('collection'));
        }
        return $this->render_view("product.review.main", compact("available", "balance", "collection", "point_store"));
    }
    
    public function discuss(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $available = UserStore::where('users_id',Auth::user()->id)->first();
        $wSum = UserBalance::where([
            ["user_id", $user_id], ["type", "W"] , ["status", "COMPLETED"]
        ])->sum("amount");
        $tSum = UserBalance::where([
            ["user_id", $user_id], ["type", "T"], ["status", "PAID"]
        ])->sum("amount");

        $point_store = 0;
        if ($available) {
            if ($available->point_store) {
                $point_store = $available->point_store->point;
            }
        }
            
        $balance = $tSum - $wSum;
        
        // data discuss move to ProductDiscussController
        
        return $this->render_view("product.discuss.main", compact("available", "balance", "point_store"));
    }

    public function addModalReview(ProductReview $productReview)
    {
        return $this->render_view("product.review.add_modal_review", compact('productReview'));
    }
    
    
    public function auction()
    {
        $collections = Product::paginate(6);
        return $this->render_view("product.auction.data", compact("collections"));
    }
    
    public function paystore(Request $request)
    {
        $user = Auth::user();
        $id = $user->id;
        $user_banks = UserBank::where("user_id", $id)->get();
        $wSum = UserBalance::where([
            ["user_id", $id], ["type", "W"] , ["status", "COMPLETED"]
        ])->sum("amount");
        $tSum = UserBalance::where([
            ["user_id", $id], ["type", "T"], ["status", "PAID"]
        ])->sum("amount");

        $balance = $tSum - $wSum;

        $withdraw = UserBalance::where([
            ["user_id", $id], ["type", "W"]
        ])->paginate(10);
        $topup = UserBalance::where([
            ["user_id", $id], ["type", "T"]
        ])->paginate(10);
        
        if ($request->ajax()) {
            return $this->render_view("store.paystore.data_saldo", compact("balance"));
        }
        return $this->render_view("store.paystore.main", compact("withdraw", "topup", "balance", "user_banks", "user"));
    }
    
    public function withdraw_more_data(Request $request)
    {
        $id = Auth::user()->id;
        $withdraw = UserBalance::where([
            ["user_id", $id], ["type", "W"]
        ])->paginate(10);

        if ($request->ajax()) {
            return $this->render_view("store.paystore.data_wd", compact("withdraw"));
        }
    }

    public function topup_more_data(Request $request)
    {
        $id = Auth::user()->id;
        $topup = UserBalance::where([
            ["user_id", $id], ["type", "T"]
        ])->paginate(10);
        if ($request->ajax()) {
            return $this->render_view("store.paystore.data_tp", compact("topup"));
        }
    }
    
    public function favorite(Request $request)
    {
        
        $store = UserStore::get()->filter(function($data){
            if (count($data->store_favorites()->get()) == 0) {
                return $data;
            }
        });
        
        $store = $store->take(3);
        
        $storeFavorite = StoreFavorite::where("user_id", Auth::user()->id)->get();
        if ($request->ajax()) {
            // if ($request->keywords) {
            //     $store = UserStore::where("name", "like", $request->keywords)->get()->filter(function($data){
            //         if (count($data->store_favorites()->get()) == 0) {
            //             return $data;
            //         }
            //     });
            //     $store = $store->take(3);                
            // }
            return $this->render_view("store.favorite.data", compact("storeFavorite", "store"));
        }
        return $this->render_view("store.favorite.main", compact("storeFavorite", "store"));
    }
    
    public function add_favorite(Request $request)
    {
        try {
            StoreFavorite::create([
                "store_id" => $request->store_id,
                "user_id" => Auth::user()->id
            ]);

            $store = UserStore::get()->filter(function($data){
                if (count($data->store_favorites()->get()) == 0) {
                    return $data;
                }
            });
            $store = $store->take(3);
            
            $storeFavorite = StoreFavorite::where("user_id", Auth::user()->id)->get();
            if ($request->ajax()) {
                return $this->render_view("store.favorite.data", compact("storeFavorite"));
            }
            return $this->render_view("store.favorite.main", compact("storeFavorite", "store"));
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => $th->getMessage(),
            ], 500);
        }
    }

    public function follow(Request $request)
    {
        try {
            StoreFavorite::create([
                "store_id" => $request->store_id,
                "user_id" => Auth::user()->id
            ]);

            return response()->json([
                'alert' => 'success',
                'message' => 'Berhasil follow toko!'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => $th->getMessage(),
            ], 500);
        }
    }

    public function destory_favorite(StoreFavorite $storeFavorite, Request $request)
    {
        try {
            $storeFavorite->delete();

            $store = UserStore::get()->filter(function($data){
                if (count($data->store_favorites()->get()) == 0) {
                    return $data;
                }
            });
            $store = $store->take(3);
            
            $storeFavorite = StoreFavorite::where("user_id", Auth::user()->id)->get();
            if ($request->ajax()) {
                return $this->render_view("store.favorite.data", compact("storeFavorite", "store"));
            }
            return $this->render_view("store.favorite.main", compact("storeFavorite"));
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => $th->getMessage(),
            ], 500);
        }
    }
    public function unfollow(Request $request)
    {
        try {
            $storeFavorite = StoreFavorite::where([
                ["store_id", $request->store_id],
                ["user_id", Auth::user()->id],
            ])->first();
            $storeFavorite->delete();
            return response()->json([
                'alert' => 'success',
                'message' => 'Berhasil Unfollow Toko'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => $th->getMessage(),
            ], 500);
        }
    }


    

    public function tracking_number(Request $request)
    {
        
        $sale = Sale::where("id", $request->id_sale)->first();
        $validator = Validator::make($request->all(), [
            'no_resi' => 'required',
            'code_courier' => 'required',
        ]);

        // jne, pos, tiki, wahana, jnt, rpx, sap, sicepat, pcp, jet, dse, dan first

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('no_resi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_resi'),
                ], 500);
            }else if ($errors->has('code_courier')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code_courier'),
                ], 500);
            }
        }

        
        // check resi apakah valid atau tidak
        $dataPost = [
            "waybill" => $request->no_resi,
            "courier" => $request->code_courier,
        ];
        $data = Http::asForm()->withHeaders([
            'key' => env('RAJAONGKIR_KEY'),
            'content-type' => 'application/x-www-form-urlencoded'
        ])->post(env('RAJAONGKIR_BASE_URL') . '/waybill', $dataPost);
        $data = json_decode($data, true);
        // dd($data, $dataPost);
        if ($data["rajaongkir"]["status"]["code"] != 200) {
            return response()->json([
                'alert' => 'error',
                'message' => $data["rajaongkir"]["status"]["description"],
            ], 500);
        }
        // dd($data);

        // dd($request->all());
        $sale->update([
            "no_resi" => $request->no_resi,
            "st" => "Dikirim",
        ]);

        $user = User::where("id", $sale->customer_id)->first();

        $details = [
            'greeting' => "Hi ".$user->name,
            'body' => 'Pesanan kamu sudah dikirimkan oleh'.$sale->store->name.', lihat status pesanan anda berkala.'
        ];

        Notification::send($user, new TransactionNotification($user, $details));

        return response()->json([
            'alert' => 'success',
            'message' => "Berhasil perbarui no resi!",
            'redirect' => 'reload'
        ]);
    }

    public function add_tracking_number(Request $request)
    {
        // dd($request->all());
        $code_courier = $request->code_courier;
        $courier = $request->courier;
        $id_sale = $request->id_sale;
        return $this->render_view("product.history.transaction.modal_pengiriman", compact("code_courier", "courier", "id_sale"));
    }
}
