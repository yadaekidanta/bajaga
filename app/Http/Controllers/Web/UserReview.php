<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ProductReview;
use App\Models\Sale;
use App\Models\StorePoint;
use App\Models\UserPoint;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Carbon\Carbon;
use Darryldecode\Cart\Validators\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserReview extends Controller
{
    use ResponseView, ReplyJson;
    public function index()
    {
        // belum di get data riviews user
        return $this->render_view('review.main');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // tinggal di kasi validasi 
        $validator = Validator::make($request->all(), [
            'rate' => 'required',
            'review' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('rate')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('rate'),
                ], 500);
            }else if ($errors->has('review')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('review'),
                ], 500);
            }
        }
        try {
            // trus di save
            $products = Sale::where("id", $request->id_sale)->first();
            $user_id = Auth::user()->id;
            

            if ($request->to_id) {
                ProductReview::create([
                    "user_id" => $user_id,
                    "product_id" => $request->product_id,
                    "to_id" => $request->product_id,
                    "review" => $request->review,
                    "read_at" =>  Carbon::now(),
                    "rate" => $request->rate,
                ]);

                ProductReview::where("id", $request->to_id)->update([
                    "read_at" => Carbon::now()
                ]);
            }else{
                foreach ($products->sale_detail()->get() as $key => $value) {
                    ProductReview::create([
                        "user_id" => $user_id,
                        "product_id" => $value->product_id,
                        "review" => $request->review,
                        "rate" => $request->rate,
                    ]);
                }
                if ($products->st == "Diterima") {
                    $products->update([
                        "st" => "Selesai"
                    ]);
        
                    // add point ke user
                    UserPoint::create([
                        "user_id" => $user_id,
                        "type" => "W",
                        "point" => "30"
                    ]);
        
                    // add point ke store
                    StorePoint::create([
                        "store_id" => $products->store_id,
                        "type" => "D",
                        "point" => "30"
                    ]);
                }
            }


            // Ganti status sale invoice

            return response()->json([
                "alert" => "success",
                "message" => "Terimakasih sudah memberikan penilaiannya",
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "alert" => "error",
                "message" => "Terjadi masalah",
            ], 500);
        }
    }
}
