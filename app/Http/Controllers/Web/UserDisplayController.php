<?php

namespace App\Http\Controllers\Web;

use App\Models\Product;
use App\Traits\ReplyJson;
use App\Models\UserDisplay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Traits\ResponseView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserDisplayController extends Controller
{
    use ResponseView, ReplyJson;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use ResponseView, ReplyJson;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $user_id = Auth::user()->id;
            $keywords = $request->keywords;
                $collections = UserDisplay::where('name','like',"%$keywords%")
                ->where("user_id", $user_id)->paginate(10);
                // dd($collections);
                return $this->render_view("product.settings.product.data_etalase", compact('collections'));
        }
        return $this->render_view("product.settings.product.etalase");
    }

    public function catalog(UserDisplay $userDisplay, Request $request)
    {
        // $store_id = Auth::user()->store->id;
        // dd($request->store_id);
        $list_category = ProductCategory::where('product_category_id','=',0)->get();
        
        $collections = Product::where([
            ["user_display_id", $userDisplay->id],
            ["store_id", $request->store_id],
            ])->paginate(6);
        if ($request->keyword) {
            if ($request->category) {
                $collections = Product::where([
                    ["name", "like", "%$request->keyword%"],
                    ["product_category_id", "$request->category"],
                    ["user_display_id", $userDisplay->id]
                    ])->paginate(6);
            } else {
                $collections = Product::where([
                    ["name", "like", "%$request->keyword%"],
                    ["user_display_id", $userDisplay->id],
                    ])->paginate(6);
            }            
            // dd($collections);
        }else if($request->category){
            $collections = Product::where([
                ["product_category_id", "$request->category"],
                ["user_display_id", $userDisplay->id]
                ])->paginate(6);
        }
        // dd($collections);
        
        if($request->ajax()){
            return $this->render_view('etalase.data_main',compact('collections'));
        }
        return $this->render_view("etalase.main", compact('collections', 'list_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->render_view("product.settings.product.input_etalase");
    }

    public function add_modal()
    {
        return $this->render_view('product.etalase.input');
    }

    public function update_modal_price(Product $product)
    {
        return $this->render_view('product.settings.product.update_modal_price', compact("product"));
    }

    public function update_modal_stok(Product $product)
    {
        return $this->render_view('product.settings.product.update_modal_stok', compact("product"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $display = new UserDisplay;
        $display->user_id = Auth::guard('web')->user()->id;
        $display->name = $request->name;
        if(request()->file('thumbnail')){
            $thumbnail = request()->file('thumbnail')->store("user_display");
            $display->thumbnail = $thumbnail;
        }else{
            $list_thumb = ["all-product.png", "product-sold.png"];
            $display->thumbnail = $list_thumb[array_rand($list_thumb)];
        }
        $display->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Etalase '. $request->name . ' tersimpan'
        ]);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserDisplay  $userDisplay
     * @return \Illuminate\Http\Response
     */
    public function show(UserDisplay $userDisplay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserDisplay  $userDisplay
     * @return \Illuminate\Http\Response
     */
    public function edit(UserDisplay $userDisplay)
    {
        return $this->render_view('display.input', ['data' => $userDisplay]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserDisplay  $userDisplay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDisplay $userDisplay)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $userDisplay->user_id = Auth::guard('web')->user()->id;
        $userDisplay->name = $request->name;
        if(request()->file('thumbnail')){
            Storage::delete($userDisplay->photo);
            $thumbnail = request()->file('thumbnail')->store("user_display");
            $userDisplay->thumbnail = $thumbnail;
        }
        $userDisplay->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Etalase '. $userDisplay->name . ' terupdate',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserDisplay  $userDisplay
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDisplay $userDisplay)
    {
        $product = Product::where('user_display_id',$userDisplay->id)->get();
        foreach ($product as $key => $value) {
            $value->user_display_id = null;
            $value->update();
        }
        $userDisplay->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Etalase terhapus',
        ]);
    }

    public function getDataEtalase(Request $request)
    {
        $user_id = Auth::user()->id;
        $keywords = $request->keywords;
        if ($keywords) {
            $etalase = UserDisplay::where('name','like',"%$keywords%")
            ->where("user_id", $user_id)->get();
        }else{
            $etalase = UserDisplay::where("user_id", $user_id)->get();
        }
            
        return $this->render_view("product.etalase.list", compact('etalase'));
    }
}
