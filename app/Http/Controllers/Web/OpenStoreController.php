<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\UserStore;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Support\Facades\Auth;

class OpenStoreController extends Controller
{
    use ResponseView, ReplyJson;

    public function index(Request $request)
    {
        $available = UserStore::where('users_id',Auth::user()->id)->first();
        if(!$available)
        {
            return redirect()->route('web.open-store.create');
        }
        else
        {
            $store = UserStore::where('users_id',Auth::user()->id)->first();
            return redirect()->route('web.product.index');
        }
    }
 
    public function create()
    {
        $data = New UserStore;
        return $this->render_view('open_store.input', compact('data'));
    }

    public function store(Request $request, UserStore $userstore)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'subdistrict_id' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
           if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('province_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province_id'),
                ]);
            }elseif($errors->has('city_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city_id'),
                ]);
            }elseif($errors->has('subdistrict_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict_id'),
                ]);
            }elseif($errors->has('address')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }
        }
        $userstore = New UserStore;
        $userstore->users_id = $request->users_id;
        $userstore->name = $request->name;
        $userstore->province_id = $request->province_id;
        $userstore->city_id = $request->city_id;
        $userstore->subdistrict_id = $request->subdistrict_id;
        $userstore->address = $request->address;
        $userstore->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Toko tersimpan',
            'redirect' => route('web.open-store.index'),
        ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

}
