<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Helper;
use App\Models\Discuss;
use App\Models\Product;
use App\Models\UserStore;
use App\Traits\ReplyJson;
use App\Models\ProductUnit;
use Illuminate\Support\Str;
use App\Models\ProductBrand;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use App\Models\ProductReview;
use App\Models\ProductCategory;
use App\Models\ProductWarranty;
use App\Models\BusinessLocation;
use App\Http\Controllers\Controller;
use App\Models\ChatMessage;
use App\Models\UserBalance;
use App\Models\UserDisplay;
use App\Models\WishList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    use ResponseView, ReplyJson;

    private $master = [
        "pesanan baru" => 0,
        "siap dikirim" => 0,
        "Ulasan baru" => 0
    ];

    public function searchForId($needle, $array) {
        foreach ($array as $key => $val) {
            // var_dump($val->from_id . " : ". $val->to_id . "<br>");
            if (($val->from_id == $needle[0] && $val->to_id == $needle[1]) || ($val->from_id == $needle[1] && $val->to_id == $needle[0])) {
                return [$key, true];
            }
        }
        return null;
     }

    public function index(Request $request)
    {
        $available = UserStore::where('users_id',Auth::user()->id)->first();
        // dd($available);
        if(!$available)
        {
            return redirect()->route('web.open-store.create');
        }
        else
        {
            $available->sales()->get()->map(function($data){
                if ($data->st == "Tertunda") {
                    $this->master["pesanan baru"]++;
                }else if($data->st == "Dipesan") {
                    $this->master["siap dikirim"]++;
                }
            });
            $user = Auth::user();
            $point_user = $user != null ? true : 0;
            if ($point_user) {
                $point_user = $user->point_user != null ? $user->point_user->point : 0 ;
            }
            $point_store = 0;
            if ($available) {
                if ($available->point_store) {
                    $point_store = $available->point_store->point;
                }
            }
            // dd($this->master);
            $master = $this->master;
            $categories = ProductCategory::get();
            $count_product = Product::where('store_id',$available->id)->count();
            $user_id = Auth::user()->id;
            $wSum = UserBalance::where([
                ["user_id", $user_id], ["type", "W"] , ["status", "COMPLETED"]
            ])->sum("amount");
            $tSum = UserBalance::where([
                ["user_id", $user_id], ["type", "T"], ["status", "PAID"]
            ])->sum("amount");
                
            $balance = $tSum - $wSum;
            
            $etalase = UserDisplay::where("user_id", $user_id)->get();
            $ulasan_baru = UserStore::where("users_id", $user_id)->first()->product()->paginate(10)->filter(function($data){
                        
                foreach ($data->product_reviews()->get() as $key => $value) {
                    if ($value->read_at == null) {
                        return $data;
                    }
                }
            })->count();
            // get count chat
            $userList = [];
            $chat_baru = 0;
            $chat = ChatMessage::where("to_id", $user_id)->orWhere("from_id", $user_id)->orderBy('created_at','ASC')->get();
            foreach ($chat as $data) {
                $data["counter_read_null"] = 0;
                $index = $this->searchForId([$data->from_id, $data->to_id], $userList);
                if ($data->to_id == $user_id) {
                    if ($data->read_at == null) {
                        $chat_baru++;
                        $data->counter_read_null++;
                    }
                }
                if ($index) {
                    $userList[$index[0]] = $data;
                }else{
                    array_push($userList, $data);
                }
            }
            // dd($ulasan_baru, $chat_baru);
            if ($request->ajax()) {
                $keywords = $request->keywords;
                if ($keywords) {
                    $collection = Product::where('product_category_id', "$keywords") 
                    ->where('store_id',$available->id)->orderBy("created_at", "desc")->paginate(8);
                }else{
                    $collection = Product::where('store_id',$available->id)->orderBy("created_at", "desc")->paginate(8);
                }
                
                return $this->render_view('product.list',compact('collection', 'balance'));
            }
            return $this->render_view('product.main', compact('available', "ulasan_baru", "chat_baru", 'master', 'balance', 'categories', 'count_product', 'etalase', "point_store"));
        }
    }

    public function create()
    {
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::get();
        $satuan = ProductUnit::get();
        $garansi = ProductWarranty::get();
        $user_id = Auth::user()->id;
        $etalase = UserDisplay::where("user_id", $user_id)->get();
        return $this->render_view('product.input', ['product' => new Product,'lokasi'=>$lokasi, 'merek'=>$merek, 'kategori'=>$kategori, 'satuan'=>$satuan, 'garansi'=>$garansi, 'etalase' => $etalase]);
    }

    public function add_modal()
    {
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::where('product_category_id', 0)->get();
        $satuan = ProductUnit::get();
        // dd($satuan);
        $garansi = ProductWarranty::get();
        $user_id = Auth::user()->id;
        $etalase = UserDisplay::where("user_id", $user_id)->get();
        return $this->render_view('product.input_modal', ['product' => new Product,'lokasi'=>$lokasi, 'merek'=>$merek, 'kategori'=>$kategori, 'satuan'=>$satuan, 'garansi'=>$garansi, 'etalase' => $etalase]);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'product_unit_id' => 'required',
            'product_brand_id' => 'required',
            'product_category_id' => 'required',
            'stok' => 'required',
            'price' => 'required',
            'alert_quantity' => 'required',
            'desc' => 'required',
            'weight' => 'required',
            'condition' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('product_unit_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_unit_id'),
                ]);
            }else if ($errors->has('product_brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_brand_id'),
                ]);
            }else if ($errors->has('product_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_category_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('weight')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            }else if ($errors->has('stok')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('stok'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }else if ($errors->has('product_warranty_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_warranty_id'),
                ]);
            }else if ($errors->has('condition')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('condition'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alert_quantity'),
                ]);
            }
        }
        if ($request->sku){
            $sku = $request->sku.'-';
        }else{
            $sku = 'BRG-';
        }
        $generator=Helper::IDGenerator(new Product,'BRG-');
        $product = new Product;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("product");
            $product->photo = $photo;
        }
        if(request()->file('brocure')){
            $brocure = request()->file('brocure')->store("product_brocure");
            $product->brocure = $brocure;
        }
        $product->condition = $request->condition;
        $product->store_id = Auth::user()->store->id;
        $product->video_url = $request->video_url;
        $product->product_brand_id = $request->product_brand_id;
        $product->price = Str::remove(',', $request->price) ?: 0;
        if ($request->user_display_id) {
            $product->user_display_id = $request->user_display_id;
        }
        $product->product_warranty_id = $request->product_warranty_id;
        $product->product_category_id = $request->product_category_id;
        $product->product_subcategory_id = $request->product_subcategory_id;
        $product->product_unit_id = $request->product_unit_id;
        $product->stock = $request->stok;
        $product->alert_quantity = $request->alert_quantity;
        $product->desc = $request->desc;
        $product->weight = $request->weight;
        $product->sku=$generator;
        $product->name = Str::title($request->name);
        $product->slug = Str::slug($request->name);
        $product->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk '. $request->title . ' tersimpan'
        ]);
    }

    public function edit(Product $product)
    {
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::where('product_category_id', 0)->get();
        $satuan = ProductUnit::get();
        $garansi = ProductWarranty::get();
        $user_id = Auth::user()->id;
        $etalase = UserDisplay::where("user_id", $user_id)->get();
        return $this->render_view('product.input', compact('product','lokasi', 'merek', 'kategori', 'satuan','garansi', 'etalase'));
    }
    public function show(Product $product, Request $request)
    {
        $user = Auth::user();
        if ($user) {
            $user_id = $user->id;
            $wishlist = WishList::where("users_id", $user_id)->where("id_product", $product->id)->first();
            if ($wishlist) {
                $wishlist = true;
            }else{
                $wishlist = false;
            }
        }else{
            $wishlist = false;
        }
        $varian_low = Product::where('id','!=',$product->id)->where('sku','LIKE','%'.$product->sku.'%')->orderBy('price','ASC')->first();
        // dd($product);
        $varian_high = Product::where('id','!=',$product->id)->where('sku','LIKE','%'.$product->sku.'%')->orderBy('price','DESC')->first();
        $collection = Product::where('id','!=',$product->id)->where('sku','NOT LIKE','%#%')->where('product_category_id','=',$product->product_category_id)->where('product_subcategory_id','=',$product->product_subcategory_id)->get();
        $varian = Product::where('id','!=',$product->id)->where('sku','LIKE','%'.$product->sku.'%')->get();
        $review = ProductReview::where('product_id','=',$product->id)->get();
        $review_count = ProductReview::where([['product_id','=',$product->id], ["to_id", null]])->get()->count();
        $rating = ProductReview::where([['product_id','=',$product->id], ["to_id", null]])->avg('rate');
        $product_pilihan = Product::paginate(10); // ganti menurut logika bisnis
        
        return $this->render_view('product.show',compact('collection','product_pilihan','product','review','review_count','rating','varian','varian_low','varian_high', 'wishlist'));
    }

    public function recomendation(Request $request)
    {
        // TODO: Add some bussnies logic for product recomendation
        $product_recomendation = Product::paginate(6);
        if($request->ajax()){
            $view = $this->render_view('carts.list_product',compact('product_recomendation'))->render();
            return response()->json(['html'=>$view]);
        }
    }

    public function update_modal(Product $product)
    {
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::where('product_category_id', 0)->get();
        $satuan = ProductUnit::get();
        $garansi = ProductWarranty::get();
        $user_id = Auth::user()->id;
        $etalase = UserDisplay::where("user_id", $user_id)->get();
        return $this->render_view('product.input', compact('product','lokasi', 'merek', 'kategori', 'satuan','garansi', 'etalase'));
    }

    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'product_unit_id' => 'required',
            'product_brand_id' => 'required',
            'price' => 'required',
            'product_category_id' => 'required',
            'product_warranty_id' => 'required',
            'desc' => 'required',
            'weight' => 'required',
            'condition' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('product_unit_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_unit_id'),
                ]);
            }else if ($errors->has('product_brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_brand_id'),
                ]);
            }else if ($errors->has('product_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_category_id'),
                ]);
            }else if ($errors->has('product_warranty_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_warranty_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('stok')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('stok'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }else if ($errors->has('weight')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            }else if ($errors->has('product_warranty_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_warranty_id'),
                ]);
            }else if ($errors->has('condition')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('condition'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alert_quantity'),
                ]);
            }
        }
        if ($request->sku){
            $sku = $request->sku.'-';
        }else{
            $sku = 'BRG-';
        }
        $generator=Helper::IDGenerator(new Product,'sku',$sku);
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("product");
            $product->photo = $photo;
        }
        if(request()->file('brocure')){
            $brocure = request()->file('brocure')->store("product_brocure");
            $product->brocure = $brocure;
        }

        $product->condition = $request->condition;
        $product->video_url = $request->video_url;
        $product->product_brand_id = $request->product_brand_id;
        $product->product_category_id = $request->product_category_id;
        $product->product_warranty_id = 0;
        $product->price = $request->price;
        $product->stock = $request->stok;
        $product->alert_quantity = $request->alert_quantity;
        $product->product_subcategory_id = $request->product_subcategory_id;
        $product->product_unit_id = $request->product_unit_id;
        $product->product_warranty_id = $request->product_warranty_id;
        $product->desc = $request->desc;
        $product->weight = $request->weight;
        if ($request->user_display_id) {
            $product->user_display_id = $request->user_display_id;
        }
        $product->sku=$generator;
        $product->name = Str::title($request->name);
        $product->slug = Str::slug($request->name);
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk '. $request->title . ' tersimpan',
        ]);
    }

    public function destroy(Product $product)
    {
        $produk = Product::where('sku','like','%'.$product->sku.'%')->get();
        foreach($produk AS $prd){
            Storage::delete($prd->photo);
            Storage::delete($prd->brochure);
            $prd->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk terhapus',
        ]);

    }

    public function catalog(Request $request)
    {
        $num_paginate = 6;
        $list_category = ProductCategory::where('product_category_id','=',0)->get();
        if ($request->keyword) {
            if ($request->category) {
                $collections = Product::where([
                    ["name", "like", "%$request->keyword%"],
                    ["product_category_id", "$request->category"]
                    ])->paginate(6);
            } else {
                $collections = Product::where("name", "like", "%$request->keyword%")->paginate(6);
            }            
            // dd($collections);
        }else if($request->category){
            $collections = Product::where("product_category_id", "$request->category")->paginate(6);
        }else{
            $collections = Product::paginate(6);
        }
        
        if($request->ajax()){
            // dd($collections);
            return $this->render_view('catalog.data',compact('collections'));
        }
        return $this->render_view('catalog.main', compact('collections', 'list_category'));
    }

    public function catalog_search(Request $request)
    {
        $num_paginate = 6;
        if ($request->keyword) {
            $collections = Product::where("name", "like", "%$request->keyword%")->paginate(6);
        }else{
            $collections = Product::paginate(6);
        }
        if($request->ajax()){
            $collections = Product::where("name", "like", "%$request->keyword%")->paginate(6);
            return $this->render_view('catalog.data',compact('collections'));
        }
        return $this->render_view('catalog.main', compact('collections'));
    }
}
