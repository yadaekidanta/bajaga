<?php

namespace App\Http\Controllers\Web;

use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserBalance;

class UserBalanceController extends Controller
{
    use ResponseView, ReplyJson;
    public function index(Request $request){
        if ($request->ajax()) {
            $keywords = $request->keywords;
                $collection = UserBalance::where('name','=',$keywords)->paginate(10);
                return $this->render_view('display.list', compact('collection'));
        }
        return $this->render_view('display.main');
    }
    public function topup(){
        
    }
    public function withdraw(){

    }
}
