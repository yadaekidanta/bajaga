<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Traits\ReplyJson;
use App\Models\ChatMessage;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\UserStore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    use ResponseView, ReplyJson;

    public function searchForId($needle, $array) {
        foreach ($array as $key => $val) {
            // var_dump($val->from_id . " : ". $val->to_id . "<br>");
            if (($val->from_id == $needle[0] && $val->to_id == $needle[1]) || ($val->from_id == $needle[1] && $val->to_id == $needle[0])) {
                return [$key, true];
            }
        }
        return null;
     }
    
    public function index(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $point_user = $user != null ? true : 0;
        if ($point_user) {
            $point_user = $user->point_user != null ? $user->point_user->point : 0 ;
        }
        $point_store = 0;
        $available = 0;
        if ($user) {
            $available = UserStore::where('users_id',$user_id)->first();
            // dd($available->point_store);
            $point_store = 0;
            if ($available) {
                if ($available->point_store) {
                    $available->point_store->point;
                }
            }
            // dd($point_store);
            if(!$available) {
                $available = 1;
            }else{
                $available = 2;
            }
        }
        if ($request->ajax()) {
            // dd($user_id);
            $userList = [];
            $collection = ChatMessage::where("to_id", $user_id)->orWhere("from_id", $user_id)->orderBy('created_at','ASC')->get();
            // $i = 0;
            // $counter = 0;
            foreach ($collection as $data) {
                $data["counter_read_null"] = 0;
                $index = $this->searchForId([$data->from_id, $data->to_id], $userList);
                // var_dump($userList);
                if ($data->to_id == $user_id) {
                    if ($data->read_at == null) {
                        $data->counter_read_null++;
                    }
                }
                // if ($i++ == 1) {
                    // dd($index);
                    // dd($data->from_id, $userList, $data->from_id == $userList[0]['from_id']);
                // }
                if ($index) {
                    // var_dump('asd');
                    $userList[$index[0]] = $data;
                }else{
                    // var_dump($data->id);
                    array_push($userList, $data);
                }

            }
            $userList = collect(($userList))->sortBy(function($data){
                return $data->created_at;
            }, SORT_REGULAR, true);
            // dd();
            // $collection = ChatMessage::whereRaw('(from_id = '.Auth::guard('web')->user()->id.' OR to_id = '.Auth::guard('web')->user()->id.')')->orderBy('created_at','ASC')->get();
            return $this->render_view('chat.list',compact('userList'));
        }
        $count_cupon = Discount::get()->count();
        return $this->render_view('chat.main', compact('user', 'available', 'count_cupon', 'point_user', 'point_store'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {

        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'body' => 'required',
            'to_id' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('body')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('body'),
                ]);
            }
        }
        $data = new ChatMessage;
        $data->from_id = Auth::guard('web')->user()->id;
        $data->to_id = $request->to_id;
        $data->body = $request->body;
        $data->type = 'e';
        $data->created_at = date('Y-m-d H:i:s');
        $data->save();
        return response()->json([
            'alert' => 'success',
        ]);
    }
    public function show(Request $request, User $user)
    {
        // dd($user);
        $users = User::where('name','LIKE','%'.$request->keyword.'%')->where('id','!=', Auth::guard('web')->user()->id)->get();
        if ($request->ajax()) {
            $collection = ChatMessage::whereRaw('(from_id = '.$user->id.' AND to_id = '.Auth::guard('web')->user()->id.') OR (from_id = '.Auth::guard('web')->user()->id.' AND to_id = '.$user->id.')')->orderBy('created_at','ASC')->get();
            // dd($collection, $user->id);
            $collections = ChatMessage::where('to_id', Auth::guard('web')->user()->id)->orderBy('created_at','ASC')->get();
            foreach($collections as $item)
            {
                if($item->from_id != Auth::guard('web')->user()->id){
                    $item->read_at = date('Y-m-d H:i:s');
                    $item->update();
                }
            }
            return $this->render_view('chat.show_list',compact('collection','user'));
        }
        return $this->render_view('chat.show',compact('user','users'));
    }
    public function edit(ChatMessage $chatMessage)
    {
        //
    }
    public function update(Request $request, ChatMessage $chatMessage)
    {
        //
    }
    public function destroy(ChatMessage $chatMessage)
    {
        //
    }
    public function counter(){
        return response()->json([
            'total_notif' => ChatMessage::where('to_id', Auth::guard('web')->user()->id)->where('read_at',null)->orderBy('created_at','ASC')->get()->count(),
        ]);
    }
}
