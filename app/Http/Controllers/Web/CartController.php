<?php

namespace App\Http\Controllers\Web;

use App\Models\Cart;
use App\Models\City;
use App\Models\Product;
use App\Models\Province;
use App\Traits\ReplyJson;
use App\Models\Subdistrict;
use Illuminate\Support\Str;
use App\Traits\ResponseView;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Courier;
use App\Models\Discount;
use App\Models\UserAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    use ResponseView, ReplyJson;
    public function index(Request $request){
        $user = Auth::guard('web')->user()->id;
        $store = Cart::select('store_id')->where('user_id',$user)->groupBy('store_id')->get();
        $cart = Cart::where('user_id',$user)->get();
        $output = '';
        $subtotal = 0;
        $total = 0;
        $total_qty = 0;
        if($cart->count() > 0){
            $subtotal = \Cart::session($user)->getSubTotal();
            $total_qty = Cart::where('user_id',$user)->get()->sum('qty');
            // $cart = $cart->map(function ($array) {
            //     return collect($array)->unique('store_id')->all();
            // });
            // dd($cart);
            foreach ($store as $toko) {
                $output .= 
                // <h6 style="margin-top:-7%;">'.$toko->cart_store->city->name.'</h6>
                '
                <h6><a href="javascript:;">'.$toko->cart_store->name.'</a></h6>
                <h6 style="margin-top:-7%;"></h6>
                ';
                $carts = Cart::where('store_id',$toko->store_id)->where('user_id',$user)->get();
                foreach($carts as $item){
                    $contain = Str::contains($item->product->sku,'#');
                    if($contain == TRUE){
                        $code = Str::before($item->product->sku, '#');
                        $product = Product::where('sku',$code)->where('id','!=',$item->product_id)->first();
                        $name = $product->name. ' ' . $item->product->name;
                    }else{
                        $name = $item->product->name;
                    }
                    $total += $item->product->price * $item->qty;
                    $output .= '
                    <div class="top-cart-item mb-3" style="margin-top:-8%;">
                        <div class="top-cart-item-image">
                            <span><img src="'.$item->product->image.'" alt="" /></span>
                        </div>
                        <div class="top-cart-item-desc">
                            <div class="top-cart-item-desc-title">
                                <span>'.$name.'</span>
                                <span class="top-cart-item-price d-block">Rp '.number_format($item->product->price).'</span>
                            </div>
                            <div class="top-cart-item-quantity">x '.number_format($item->qty).'</div>
                        </div>
                    </div>
                    ';
                }
            }
        }else{
            $output .= '
            <div class="top-cart-item">
                <div class="top-cart-item-desc">
                    <div class="top-cart-item-desc-title">
                        <h5>Wah keranjang belanjaanmu kosong!</h5>
                        <span>Daripada dianggurin, isi saja dengan barang-barang menarik. Lihat-lihat dulu, siapa tahu ada yang kamu suka!</span>
                    </div>
                </div>
            </div>
            ';
        }
        return response()->json([
            'collection' => $output,
            'total_item' => $cart->count(),
            'subtotal' => number_format($subtotal),
            'total_qty' => number_format($total_qty),
            'total' => number_format($total),
        ]);
    }
    public function store(Request $request){
        $product = Product::where('id',$request->product)->first();
        // TODO: BUG In where sku like, cause ada product yg mirip prefix atau enggak akhirannya
        // $varian = Product::where('id','!=',$product->id)->where('sku','LIKE','%'.$product->sku.'%')->get();
        // $varian = Product::where('id','!=',$product->id)->where('sku',$product->sku)->get();
        // dd($varian->count());
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'quantity' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('product')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product'),
                ]);
            }elseif ($errors->has('quantity')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('quantity'),
                ]);
            }
        }
        // if($varian->count() > 0){
        //     $validator = Validator::make($request->all(), [
        //         'varian' => 'required',
        //     ]);
        //     if ($validator->fails()) {
        //         $errors = $validator->errors();
        //         if ($errors->has('varian')) {
        //             return response()->json([
        //                 'alert' => 'error',
        //                 'message' => $errors->first('varian'),
        //             ]);
        //         }
        //     }
        // }
        try {
            $user = Auth::guard('web')->user()->id;
            $cart = Cart::where('user_id',$user)->where('product_id',$request->product)->first();
            if($cart != null){
                $cart->qty = $cart->qty + $request->quantity;
                $cart->update();
            }else{
                $cart = new Cart();
                $cart->user_id = $user;
                $cart->store_id = $request->store;
                $cart->product_id = $request->product;
                $cart->qty = $request->quantity;
                $cart->save();
            }
    
            return response()->json([
                'alert' => 'success',
                'message' => 'Item Added',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => $th->getMessage(),
            ]);
        }
    }
    public function detail(Request $request){
        $cupons = Discount::where("st", "aktif")->get();
        $user = Auth::guard('web')->user();
        $user_id = $user->id;
        $address = UserAddress::where('users_id',Auth::user()->id)->get();
        $product_recomendation = Product::paginate(6);
        // dd("sad");
        if($request->ajax()){
            $city = City::get();
            $provinsi = Province::get();
            $subdistrict = Subdistrict::get();
            
            $store = Cart::select('store_id')->where('user_id',$user->id)->groupBy('store_id')->get();
            // dd($store->product);
            $output = '';
            $subtotal = 0;
            $total = 0;
            $total_qty = 0;
            return $this->render_view('carts.detail_list',compact('provinsi','city','subdistrict','user','address','store', 'user_id'));
        }
        return $this->render_view('carts.detail', compact("cupons", "address", "user", 'user_id', 'product_recomendation'));
    }
    public function delete(Request $request){
        $user = Auth::guard('web')->user()->id;
        \Cart::session($user)->remove(1);
    }
    public function empty_cart(Request $request){
        $user = Auth::guard('web')->user()->id;
        \Cart::session($user)->remove(1);
    }
    public function add_voucher(Request $request){
        $coupon101 = new \CartCondition(array(
            'name' => 'COUPON 101',
            'type' => 'coupon',
            'value' => '-5%',
        ));
    }
    public function remove_voucher(Request $request){

    }
    public function decreaseQuantity(Cart $item){
        // if($item->qty==1){
        //     $item->delete();
        // } else {
        //     $item->qty = $item->qty-1;
        //     $item->update();
        // }
        // $product = Product::where('id',$item->product_id)->first();
        // $product->stock = $product->stock+1;
        // $product->update();
        // return response()->json([
        //     'alert' => 'success',
        //     'message' => 'Item Added',
        // ]);
        if($item->qty==1){
            $item->delete();
        } else {
            $item->qty = $item->qty-1;
            $item->update();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Item Added',
        ]);
    }
    public function increaseQuantity(Cart $item){
        // dd($item);
        $product = Product::where('id',$item->product_id)->first();
        $item->qty = $item->qty+1;
        if($product->stock<$item->qty){
            return response()->json([
                'alert' => 'error',
                'message' => 'Stok Kurang dari Kuantiti',
            ]);
        } else {
        $item->update();
        $product->update();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Item Added',
        ]);
    }

    public function destroy(Request $request){
        try {
            $cart = Cart::where("id", $request->id)->first();
            // $cart->destroy();
            return response()->json([
                'alert' => 'success',
                'message' => 'Berhasil hapus item!',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Internal server error!',
            ], 500);
        }
    }

    public function updateQuantity(Cart $item, Request $request){
        $product = Product::where('id',$item->product_id)->first();
        $item->qty = $request->qty;
        if($product->stock<$item->qty){
            return response()->json([
                'alert' => 'error',
                'message' => 'Stok Kurang dari Kuantiti',
                'qty' => $item->qty,
            ]);
        } else {
        $item->update();
        $product->update();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Item Added',
            'qty' => $item->qty,
        ]);
    }
    public function checkout(Request $request){
        $user = Auth::guard('web')->user()->id;
        foreach($request->id as $id){
            $cart = Cart::where('product_id',$id)->where('user_id',$user)->first();
            $qty = $cart->qty;
        }
    }

    public function checkout_show(Request $request)
    {
        $cupons = Discount::where("st", "aktif")->get();
        $courier = Courier::limit(4)->get();
        $user = Auth::user();
        $address = UserAddress::where("users_id", $user->id)->get();
        $address_use = UserAddress::where("users_id", $user->id)->where("is_use", '1')->first();
        // dd($address_use);
        if ($request->ajax()) {
            return $this->render_view("checkout.data_address", compact('user', 'address', 'address_use')); 
        }
        return $this->render_view("checkout.main", compact('user', 'address', 'cupons', 'address_use', 'courier'));
    }
    
    public function modalUserAddress()
    {
        $user = Auth::user();
        $address = UserAddress::where("users_id", $user->id)->get();
        return $this->render_view("checkout.data_modal_address", compact("address", "user"));
    }

    public function pay(Request $request)
    {

    }
}
