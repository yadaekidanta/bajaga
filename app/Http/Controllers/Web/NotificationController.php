<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChatMessage;
use Illuminate\Support\Facades\Auth;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;

class NotificationController extends Controller
{
    use ResponseView, ReplyJson;
    public function counter(Request $request){
        $user = User::where('id','=',Auth::guard('web')->user()->id)->first();
        return response()->json([
            'total_notif' => $user->unreadNotifications->count(),
        ]);
    }

    public function searchForId($needle, $array) {
        foreach ($array as $key => $val) {
            // var_dump($val->from_id . " : ". $val->to_id . "<br>");
            if (($val->from_id == $needle[0] && $val->to_id == $needle[1]) || ($val->from_id == $needle[1] && $val->to_id == $needle[0])) {
                return [$key, true];
            }
        }
        return null;
     }

    public function counter_chat(Request $request){
        $user = Auth::user();
        $user_id = $user->id;
        $collection = ChatMessage::where("to_id", $user_id)->orWhere("from_id", $user_id)->orderBy('created_at','ASC')->get();
        // $i = 0;
        $userList = [];
        $counter = 0;
        foreach ($collection as $data) {
            $data["counter_read_null"] = 0;
            $index = $this->searchForId([$data->from_id, $data->to_id], $userList);
            // var_dump($userList);
            if ($index) {
                $userList[$index[0]] = $data;
            }else{
                array_push($userList, $data);
                // if ($data->read_at == null) {
                //     $counter++;
                // }
            }

        }
        
        $arr_tmp = [];
        $counter = 0;
        foreach ($userList as $key => $value) {
            // dd($value->from_id);
            if (!in_array($value->from_id, $arr_tmp)) {
                if (!$value->read_at) {
                    $counter++;
                    array_push($arr_tmp, $value->from_id);
                }
            }
            // var_dump($arr_tmp);
        }

        return response()->json([
            'total_notif' => $counter,
        ]);
    }
    
    public function index(Request $request){
        $output = '';
        $notif = '';
        $simbol = '';
        $user = User::where('id','=',Auth::guard('web')->user()->id)->first();
        if($user->notifications->count() > 0){
            foreach ($user->notifications as $notification) {
                if($notification->data['tipe'] == 1){
                    $simbol = '
                    <div class="top-notification-item-image">
                        <a href="javascript:;"><img src="'.asset('img/favicon.png').'" alt="'.config('app.name').'" /></a>
                    </div>
                    ';
                    $notif = '
                    <div class="top-notification-item-desc flex-column">
                        <div class="top-notification-item-desc-title">
                            <a href="javascript:;">'.$notification->data['pesan'].'</a>
                        </div>
                        <span class="text-xs text-secondary">'.$notification->created_at.'</span>
                    </div>
                    ';
                }
                $output .= '
                <div class="top-notification-item">
                    '.$simbol.'
                    '.$notif.'                            
                </div>
                ';
            }
        }else{
            $output = '
            <div class="top-notification-item">
                <div class="top-notification-item-image">
                    <a href="javascript:;"><img src="'.asset('img/favicon.png').'" alt="'.config('app.name').'" /></a>
                </div>
                <div class="top-notification-item-desc">
                    <div class="top-notification-item-desc-title">
                        <a href="javascript:;">Belum ada notifikasi</a>
                    </div>
                </div>
            </div>
            ';
        }
        $user->unreadNotifications->markAsRead();
        return response()->json([
            'collection' => $output,
            'total_notif' => $user->unreadNotifications->count(),
        ]);
    }

    public function show()
    {
        $user = Auth::user();
        $collection = $user->notifications;
        // dd($collection[0]->data["pesan"]);
        // data notifcation ambil dong
        return $this->render_view('notification.main', compact("collection"));
    }
}
