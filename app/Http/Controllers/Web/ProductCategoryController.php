<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\ProductCategory;
use App\Models\ProductBrand;
use App\Models\ProductReview;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductCategoryController extends Controller
{
    use ResponseView, ReplyJson;

    public function get_list(Request $request)
    {
        $result = ProductCategory::where('product_category_id','=','0')->get();
        $list = "<option>Pilih Kategori</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->title</option>";
        }
        return $list;
    }
    public function get_list_sub(Request $request)
    {
        $result = ProductCategory::where('product_category_id','=',$request->category)->get();
        $list = "<option>Pilih Sub Kategori</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->title</option>";
        }
        echo $list;
    }
    public function index()
    {
        return $this->render_view('category.main',compact('collection'));
    }
    public function show(ProductCategory $category, Request $request)
    {
        // dd("asd");
        $num_paginate = 10;
        $products = $category->product()->paginate($num_paginate);
        $products = $products->map(function($data){
            $reviews = $data->product_reviews()->get();
            $r = 0;
            $data["rating"] = 0;
            if (count($reviews) > 0) {
                foreach ($reviews as $key => $value) {
                    $r += $value->rate;
                    // dd($value);
                }
                $r = $r / count($reviews);
            }
            $data->rating = $r;
            return $data;
        });
        // dd($products);
        // $rating = ProductReview::where("product_id", $category->id)->avg('rate');
        $list_category = ProductCategory::where('product_category_id','=',0)->get();
        // if ($products->count() > $num_paginate) {
        if($request->ajax()){
            // dd($view);
            if ($request->filter) {
                if ($request->filter == "max") {
                    $products = Product::where("product_category_id", $category->id)
                    ->orderByRaw('CONVERT(price, INT) desc')
                    ->paginate($num_paginate);
                }else if ($request->filter == "min") {
                    $products = Product::where("product_category_id", $category->id)->orderByRaw('CONVERT(price, INT) asc')
                    ->paginate($num_paginate);
                }
            }
            return $this->render_view('category.data_main',compact('products'));
        }
        // }
        return $this->render_view('category.main',compact('products','list_category', 'category'));
    }

    public function show_subcategory(ProductCategory $subcategory, Request $request)
    {
        $num_paginate = 10;
        $products = $subcategory->product_subcategory()->paginate($num_paginate);;
        $products = $products->map(function($data){
            $reviews = $data->product_reviews()->get();
            $r = 0;
            $data["rating"] = 0;
            if (count($reviews) > 0) {
                foreach ($reviews as $key => $value) {
                    $r += $value->rate;
                    // dd($value);
                }
                $r = $r / count($reviews);
            }
            $data->rating = $r;
            return $data;
        });
        
        // $rating = ProductReview::where("product_id", $subcategory->id)->avg('rate');
        $list_category = ProductCategory::where('product_category_id','=',0)->get();
        
        if($request->ajax()){
            if ($request->filter) {
                if ($request->filter == "max") {
                    $products = Product::where("product_subcategory_id", $subcategory->id)
                    ->orderByRaw('CONVERT(price, INT) desc')
                    ->paginate($num_paginate);
                }else if ($request->filter == "min") {
                    $products = Product::where("product_subcategory_id", $subcategory->id)->orderByRaw('CONVERT(price, INT) asc')
                    ->paginate($num_paginate);
                }
            }
            return $this->render_view('category.data_subcategory', compact('products'));
        }
        return $this->render_view('category.subcategory',compact('products','list_category'));
    }
    public function show_brand(ProductBrand $brand, Request $request)
    {
        $num_paginate = 10;
        $products = $brand->product_brand()->paginate($num_paginate);
        $products = $products->map(function($data){
            $reviews = $data->product_reviews()->get();
            $r = 0;
            $data["rating"] = 0;
            if (count($reviews) > 0) {
                foreach ($reviews as $key => $value) {
                    $r += $value->rate;
                    // dd($value);
                }
                $r = $r / count($reviews);
            }
            $data->rating = $r;
            return $data;
        });
        // dd($brand);
        // $rating = ProductReview::avg('rate');
        // dd($products);
        $list_category = ProductCategory::where('product_category_id','=',0)->get();
        // if ($products->count() > $num_paginate) {
            if($request->ajax()){
                if ($request->filter) {
                    if ($request->filter == "max") {
                        $products = Product::where("product_brand_id", $brand->id)
                        ->orderByRaw('CONVERT(price, INT) desc')
                        ->paginate($num_paginate);
                    }else if ($request->filter == "min") {
                        $products = Product::where("product_brand_id", $brand->id)->orderByRaw('CONVERT(price, INT) asc')
                        ->paginate($num_paginate);
                    }
                }
                return $this->render_view('category.data_brand',compact('products'));
            }
        // }
        return $this->render_view('category.brand',compact('brand','list_category', 'products'));
    }
}
