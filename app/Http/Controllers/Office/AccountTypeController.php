<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\AccountType;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class AccountTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = AccountType::where('name','LIKE','%'.$keywords.'%')
            ->where('account_type_id',0)
            ->paginate(10);
            return view('page.office.accountType.list', compact('collection'));
        }
        return view('page.office.accountType.main');
    }
    public function create()
    {
        $parent = AccountType::where('account_type_id',0)->get();
        return view('page.office.accountType.input', ['data' => new AccountType, 'parent' => $parent]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:account_type',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $data = New AccountType;
        $data->name = Str::title($request->name);
        $data->account_type_id = $request->parent ?: 0;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tipe Akun tersimpan',
        ]);
    }
    public function show(AccountType $accountType)
    {
        //
    }
    public function edit(AccountType $accountType)
    {
        $parent = AccountType::where('account_type_id',0)->get();
        return view('page.office.accountType.input', ['data' => $accountType, 'parent' => $parent]);
    }
    public function update(Request $request, AccountType $accountType)
    {
        $validator = Validator::make($request->all(), [
            'account_type' => 'required|exists:account_type,name'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $accountType->name = Str::title($request->name);
        $accountType->account_type_id = $request->parent ?:0;
        $accountType->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tipe Akun terubah',
        ]);
    }
    public function destroy(AccountType $accountType)
    {
        AccountType::where('account_type_id', $accountType->id)->delete();
        $accountType->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tipe Akun terhapus',
        ]);
    }
}