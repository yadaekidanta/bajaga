<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\SaleDetail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SaleDetailController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'qty' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }elseif ($errors->has('qty')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $price = Str::remove(',', $request->price) ?: 0;
        $qty = Str::remove(',', $request->qty) ?: 0;

        if($price <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan harga',
            ]);
        }
        if($qty <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan jumlah',
            ]);
        }
        $check = SaleDetail::where([['sale_id', '=', $request->sale_id], ['product_id', '=', $request->barang]])->first();
        if(!$check){
            $detil = new SaleDetail;
            $detil->qty = $qty;
            $detil->subtotal = $price * $qty;
        }else{
            $detil = $check;
            $detil->qty = $check->qty + $qty;
            $detil->subtotal = $price * $detil->qty;
        }
        $cek_stok = Helper::check_stock($request->barang,$qty);
        if($cek_stok == 0){
            return response()->json([
				'alert' => 'info',
				'message' => 'Stok barang tidak cukup',
			]);
        }
        $detil->sale_id = $request->sale_id;
        $detil->product_id = $request->barang;
        $detil->price = $price;
        if(!$check){
            $detil->save();
        }else{
            $detil->update();
        }
        $sale = $detil->sale; 
        $subtotal= $sale->sale_detail()->get()->sum("subtotal");
        $sale->grand_total = $subtotal;
        $sale->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.sale.show',$request->sale_id),
        ]);
    }
    public function destroy(SaleDetail $saleDetail)
    {
        $id = $saleDetail->sale_id;
        $saleDetail->sale->grand_total -= $saleDetail->subtotal;
        $saleDetail->sale->update();
        $saleDetail->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang terhapus',
            'redirect' => 'input',
            'route' => route('office.sale.show',$id),
        ]);
    }
}
