<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\ProductBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ProductBrandController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductBrand::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.productBrand.list', compact('collection'));
        }
        return view('page.office.productBrand.main');
    }
    public function create()
    {
        return view('page.office.productBrand.input', ['data' => new ProductBrand]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_brand',
            'photo' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        $productBrand = new ProductBrand;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("product_brand");
            $productBrand->photo = $photo;
        }
        $productBrand->name = $request->name;
        $productBrand->slug = Str::slug($request->name);
        $productBrand->note = $request->note;
        $productBrand->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Merek tersimpan',
        ]);
    }
    public function show(ProductBrand $productBrand)
    {
        //
    }
    public function edit(ProductBrand $productBrand)
    {
        return view('page.office.productBrand.input', ['data' => $productBrand]);
    }
    public function update(Request $request, ProductBrand $productBrand)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $productBrand->name = $request->name;
        if(request()->file('photo')){
            Storage::delete($productBrand->photo);
            $photo = request()->file('photo')->store("product_brand");
            $productBrand->photo = $photo;
        }
        $productBrand->note = $request->note;
        $productBrand->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Merek terubah',
        ]);
    }
    public function destroy(ProductBrand $productBrand)
    {
        Storage::delete($productBrand->photo);
        $productBrand->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Merek terhapus',
        ]);
    }
    public function get(Request $request)
    {
        $merek = ProductBrand::where('id','=',$request->id)->first();
        if(!$merek){
            return response()->json([
                'alert' => 'info',
                'message' => 'Merek tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $merek,
                'message' => 'Merek ditemukan',
            ]);
        }
    }
    public function get_list(Request $request)
    {
        $result = ProductBrand::get();
        $list = "<option>Pilih Merek</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
    public function input_modal()
    {
        return view('page.office.productBrand.input_modal', ['data' => new ProductBrand]);
    }
    public function input_modal_merek()
    {
        return view('page.office.productBrand.input_modal', ['data' => new ProductBrand]);
    }
    public function input_modal_kategori()
    {
        return view('page.office.productCategory.input_modal', ['data' => new ProductBrand]);
    }
    public function input_modal_unit()
    {
        return view('page.office.productUnit.input_modal', ['data' => new ProductBrand]);
    }
    public function list_modal(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductBrand::where('name','like','%'.$keywords.'%')
            ->get();
            return view('page.office.productBrand.list_modal',compact('collection'));
        }
    }
}
