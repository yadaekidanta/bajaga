<?php

namespace App\Http\Controllers\Office;

use App\Models\Sale;
use App\Helpers\Helper;
use App\Models\Customer;
use App\Models\SaleDetail;
use App\Models\SalePayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use App\Models\User;
use App\Notifications\TransactionNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{

    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities();

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }

    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Sale::where('code','LIKE','%'.$keywords.'%')->orderBy("created_at", "desc")
            ->paginate(10);
            return view('page.office.sale.list', compact('collection'));
        }
        return view('page.office.sale.main');
    }
    
    public function create()
    {
        $customer = Customer::get();
        return view('page.office.sale.input',['data' => new Sale, 'customer' => $customer ]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'pelanggan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('pelanggan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pelanggan'),
                ]);
            }
        }
        if ($request->code){
            $code = $request->code.date("-Ym").'-';
        }else{
            $code = 'SALE'.date("-Ym").'-';
        }
        $generator = Helper::IDGenerator(new Sale,'code',$code);
        $sale = new Sale;
        $sale->date = $request->tanggal;
        $sale->code = $generator;
        $sale->customer_id = $request->customer_id;
        $sale->customer_name = $request->customer_name;
        $sale->customer_phone = $request->customer_phone;
        $sale->created_by = Auth::guard('office')->user()->id;
        $sale->type = 'Offline';
        $sale->save();
        $this->purchase_activity($sale->id, Auth::user()->id, "created", "penjualan");
        return response()->json([
            'alert' => 'success',
            'message' => 'Penjualan Tersimpan',
            'redirect' => 'input',
            'route' => route('office.sale.edit',$sale->id)
        ]);
    }
    public function show(Sale $sale)
    {
        // dd($sale);
    return view('page.office.sale.show', compact('sale'));
    }
    public function edit(Sale $sale)
    {
        $customer = Customer::get();
        return view('page.office.sale.input',['data' => $sale, 'customer' => $customer ]);
    }
    public function update(Request $request, Sale $sale)
    {
        $sale->date = $request->tanggal;
        $sale->update();
        $this->purchase_activity($sale->id, Auth::user()->id, "update", "penjualan");
    }
    public function destroy(Sale $sale)
    {
        SaleDetail::where('sale_id')->delete();
        SalePayment::where('sale_id')->delete();
        $this->purchase_activity($sale->id, Auth::user()->id, "delete", "penjualan");
        $sale->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Penjualan terhapus',
        ]);
    }
    public function process(Sale $sale)
    {
        if($sale->sale_detail->count() > 0){
            $sale->st = "Dipesan";
            $sale->update();
            $this->purchase_activity($sale->id, Auth::user()->id, "update", "penjualan");
            return response()->json([
                'alert' => 'success',
                'message' => 'Penjualan diproses',
                'redirect' => 'input',
                'route' => route('office.sale.show',$sale->id),
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Harap masukkan barang terlebih dahulu',
                'redirect' => 'input',
                'route' => route('office.sale.show',$sale->id),
            ]);
        }
    }
    public function delivered(Sale $sale)
    {
        $this->purchase_activity($sale->id, Auth::user()->id, "telah diterima", "penjualan");
        Helper::decrease_stock(new SaleDetail,new Sale,'sale_id',$sale->id);
        $sale->st = "Diterima";
        $sale->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang diterima',
            'redirect' => 'input',
            'route' => route('office.sale.show',$sale->id),
        ]);
    }

    public function add_tracking_number(Request $request)
    {
        
        $sale = Sale::where("id", $request->id_sale)->first();
        $validator = Validator::make($request->all(), [
            'no_resi' => 'required',
            'code_courier' => 'required',
        ]);

        // jne, pos, tiki, wahana, jnt, rpx, sap, sicepat, pcp, jet, dse, dan first

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('no_resi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_resi'),
                ], 500);
            }else if ($errors->has('code_courier')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code_courier'),
                ], 500);
            }
        }

        
        // check resi apakah valid atau tidak
        $dataPost = [
            "waybill" => $request->no_resi,
            "courier" => $request->code_courier,
        ];
        $data = Http::asForm()->withHeaders([
            'key' => env('RAJAONGKIR_KEY'),
            'content-type' => 'application/x-www-form-urlencoded'
        ])->post(env('RAJAONGKIR_BASE_URL') . '/waybill', $dataPost);
        $data = json_decode($data, true);
        // dd($data, $dataPost);
        if ($data["rajaongkir"]["status"]["code"] != 200) {
            return response()->json([
                'alert' => 'error',
                'message' => $data["rajaongkir"]["status"]["description"],
            ], 500);
        }
        // dd($data);

        // dd($request->all());
        $sale->update([
            "no_resi" => $request->no_resi,
            "st" => "Dikirim",
        ]);

        $user = User::where("id", $sale->customer_id)->first();

        $details = [
            'greeting' => "Hi ".$user->name,
            'body' => 'Pesanan kamu sudah dikirimkan oleh'.$sale->store->name.', lihat status pesanan anda berkala.'
        ];

        Notification::send($user, new TransactionNotification($user, $details));

        return response()->json([
            'alert' => 'success',
            'message' => "Berhasil perbarui no resi!",
            'redirect' => 'input',
            'route' => route('office.sale.edit',$sale->id)
        ]);
    }
}
