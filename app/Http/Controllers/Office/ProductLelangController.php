<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductLelang;
use App\Models\ProductBrand;
use App\Models\ProductUnit;
use App\Models\ProductWarranty;
use App\Models\ProductCategory;
use App\Models\BusinessLocation;
use App\Http\Controllers\Controller;
use App\Models\ProductVarian;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class ProductLelangController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductLelang::where('id','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.productLelang.list',compact('collection'));
        }
        return view('page.office.productLelang.main');
    }
    public function create()
    {
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::where('product_category_id', 0)->get();
        $satuan = ProductUnit::get();
        $garansi = ProductWarranty::get();
        return view('page.office.productLelang.input', ['data' => new Product, 'lokasi'=>$lokasi, 'merek'=>$merek, 'kategori'=>$kategori, 'satuan'=>$satuan, 'garansi'=>$garansi]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'price' => 'required',
            'qty' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('start')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('start'),
                ]);
            }else if ($errors->has('end')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('end'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $product = Product::where('id',$request->product_id)->first();
        $productLelang = new ProductLelang;
        $productLelang->product_id = $request->product_id;
        $productLelang->start =  $request->start;
        $productLelang->end = $request->end;
        $productLelang->price = $request->price;
        if($product->stock >= $request->qty){
            $productLelang->qty = $request->qty;
        }else if($product->stock == 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Stok produk kosong',
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Stok produk tidak memenuhi',
            ]);
        }
        $product->stock = $product->stock - $request->qty;
        $productLelang->qty_return = 0;
        $productLelang->st = "belum aktif";
        $productLelang->save();
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk Lelang'. $request->name . ' tersimpan',
        ]);
    }
    public function show()
    {
        return view('page.web.web.product.show');
    }

    public function bid_detail($id)
    {
        $data['product'] = ProductLelang::find($id);
        $data['bids'] = $data['product']->bids()->get();

        return view('page.office.productLelang.show', $data);
    }

    public function bid_send_mail($id)
    {
        $user = User::find($id);
        $details = [
            'title' => 'Mail from Bajaga',
            'body' => 'Selamat anda pemenang lelang ...'
        ];

        \Mail::to($user->email)->send(new \App\Mail\SendMail($details));
        return response()->json([
            'alert' => 'success',
            'message' => 'Email Terkirim',
        ]);
    }

    public function edit(ProductLelang $productLelang)
    {
        $product = Product::get();
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::where('product_category_id', 0)->get();
        $satuan = ProductUnit::get();
        $garansi = ProductWarranty::get();
        return view('page.office.productLelang.input', ['data' => $productLelang, 'product'=>$product,'lokasi'=>$lokasi, 'merek'=>$merek, 'kategori'=>$kategori, 'satuan'=>$satuan, 'garansi'=>$garansi]);
    }
    public function update(Request $request, ProductLelang $productLelang)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'price' => 'required',
            'qty' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('start')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('start'),
                ]);
            }else if ($errors->has('end')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('end'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $product = Product::where('id',$request->product_id)->first();
        if($product->stock >= $request->qty){
            $product->stock = $product->stock - $request->qty;
        }else if($product->stock == 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Stok produk kosong',
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Stok produk tidak memenuhi',
            ]);
        }
        $product->stock = $product->stock + $request->qty;
        $productLelang->product_id = $request->product_id;
        $productLelang->start =  $request->start;
        $productLelang->end = $request->end;
        $productLelang->price = $request->price;
        $productLelang->qty = $request->qty;
        $productLelang->qty_return = 0;
        $productLelang->st = "belum aktif";
        $productLelang->update();
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk Lelang'. $request->title . ' terubah',
        ]);
    }
    public function destroy(ProductLelang $productLelang)
    {
        $product = Product::where('id',$productLelang->product_id)->first();
        $product->stock = $product->stock + $productLelang->qty;
        $product->update();
        $productLelang->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk Lelang terhapus',
        ]);
    }
    public function active(ProductLelang $productLelang){
        if($productLelang->start==date('Y-m-d')){
            $productLelang->st = 'aktif';
            $productLelang->update();
            return response()->json([
                'alert' => 'success',
                'message' => 'Produk Lelang telah aktif',
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Tanggal Mulai Lelang Lebih Kecil Dari Tanggal Sekarang',
            ]);
        }
    }

    public function tidakTerjual(ProductLelang $productLelang){
        if($productLelang->st = 'aktif'){
            if($productLelang->qty>0){
                $product = Product::where('id',$productLelang->product_id)->first();
                $product->stock = $product->stock + $productLelang->qty;
                $productLelang->qty_return= $productLelang->qty;
                $productLelang->qty-=$productLelang->qty;
                $product->update();
                $productLelang->update();
                return response()->json([
                    'alert' => 'success',
                    'message' => 'Produk Lelang Telah Dikembalikan Keproduk',
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => 'Produk Quantity Tidak Ada',
                ]);
            }
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Produk Status Tidak Aktif',
            ]);
        }
    }

}
