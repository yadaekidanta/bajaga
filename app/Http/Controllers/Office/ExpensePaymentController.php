<?php

namespace App\Http\Controllers\Office;

use App\Models\Expense;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ExpensePayment;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ExpensePaymentController extends Controller
{
    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities();

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_id' => 'required|integer',
            'payment_date' => 'required',
            'paid_amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('account_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_id'),
                ]);
            }elseif ($errors->has('payment_date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('payment_date'),
                ]);
            }elseif ($errors->has('paid_amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('paid_amount'),
                ]);
            }
        }
        if(Str::remove(',',$request->paid_amount) <= 0){
            return response()->json([
                'alert' => 'info',
                'message' => 'Harap masukkan jumlah',
            ]);
        }
        $check_saldo = ExpensePayment::CekSaldo($request->account_id,Str::remove(',',$request->paid_amount));
        $gt = Expense::where('id','=',$request->expense_payment_id)->get()->sum('amount');
        if(Str::remove(',',$request->paid_amount) > $gt){
            return response()->json([
                'alert' => 'error',
                'message' => 'Pembayaran melebihi Rp '. number_format($gt),
            ]);
        }
        if($check_saldo == "cukup"){
            $price = Str::remove(',', $request->paid_amount) ?: 0;
            $check = ExpensePayment::where('id', '=', $request->expense_payment_id)->first();
            if(!$check){
                $payment = new ExpensePayment;
                $payment->amount = $price;
            }else{
                $payment = $check;
                $payment->amount = $payment->amount + $price;
            }
            $payment->expense_id = $request->expense_payment_id;
            $payment->account_id = $request->account_id;
            $payment->date = $request->payment_date;
            $payment->created_by = Auth::guard('office')->user()->id;
            if(!$check){
                $payment->save();
            }else{
                $payment->update();
            }
            $this->purchase_activity($payment->id, Auth::user()->id, "created", "pembayaran");
            ExpensePayment::PenguranganSaldo($request->account_id,Str::remove(',',$request->paid_amount));
            return response()->json([
                'alert' => 'success',
                'message' => 'Pembayaran berhasil ditambah',
                'redirect' => 'input',
                'route' => route('office.expense.edit',$request->expense_payment_id),
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Saldo tidak cukup',
            ]);
        }
    }
    public function destroy(ExpensePayment $expensePayment)
    {
        $this->purchase_activity($expensePayment->id, Auth::user()->id, "delete", "pembayaran");
        ExpensePayment::PengembalianSaldo($expensePayment->account_id,$expensePayment->amount);
        $expensePayment->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembelian terhapus',
            'redirect' => 'input',
            'route' => route('office.expense.edit',$expensePayment->expense_id),
        ]);
    }
}