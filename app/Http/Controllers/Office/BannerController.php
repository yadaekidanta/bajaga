<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function index(Request $request)
    {
        $collection = Banner::paginate(10);
        if ($request->ajax()) {
            $keywords = $request->keywords;
            if ($keywords) {
                dd("sad");
                $collection = Banner::where('url','LIKE','%'.$keywords.'%')
                ->paginate(10);
            }
            return view('page.office.banner.list', compact('collection'));
        }
        return view('page.office.banner.main');
    }
    public function create()
    {
        return view('page.office.banner.input', ['data' => new Banner]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo' => 'required',
            'url' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('url')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('url'),
                ]);
            }elseif($errors->has('photo')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        $banner = New Banner;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("banner");
            $banner->photo = $photo;
        }
        $banner->url = $request->url;
        $banner->active = 'n';
        $banner->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Spanduk tersimpan',
        ]);
    }
    public function show(Banner $banner)
    {
        //
    }
    public function edit(Banner $banner)
    {
        return view('page.office.banner.input', ['data' => $banner]);
    }
    public function update(Request $request, Banner $banner)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('url')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('url'),
                ]);
            }elseif($errors->has('photo')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($banner->photo);
            $photo = request()->file('photo')->store("banner");
            $banner->photo = $photo;
        }
        $banner->url = $request->url;
        $banner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Spanduk terubah',
        ]);
    }
    public function destroy(Banner $banner)
    {
        Storage::delete($banner->photo);
        $banner->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Spanduk terhapus',
        ]);
    }
    public function active(Banner $banner){
        $banner->active = 'a';
        $banner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Spanduk '. $banner->url . ' telah aktif',
        ]);
    }
    public function nonActive(Banner $banner){
        $banner->active = 'n';
        $banner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Spanduk '. $banner->url . ' tidak aktif',
        ]);
    }
}