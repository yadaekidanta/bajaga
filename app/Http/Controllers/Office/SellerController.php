<?php

namespace App\Http\Controllers\Office;

use ZipArchive;
use App\Models\UserStore;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SellerController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = UserStore::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.seller.list', compact('collection'));
        }
        return view('page.office.seller.main');
    }
    public function show(UserStore $seller)
    {
        //
    }
    public function create_reason()
    {
        return view('page.office.seller.input');
    }
    public function send_reason(Request $request){
        return response()->json([
            'alert' => 'success',
            'message' => 'Alasan penolakan telah terkirim ke alamat email : '. $request->email . '.',
        ]);
    }
    public function active(UserStore $seller){
        $seller->verified_at = date("Y-m-d H:i:s");
        $seller->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Penjual '. $seller->name . ' berhasil di verifikasi',
        ]);
    }
    public function get(Request $request)
    {
        $seller = UserStore::where('code','=',$request->code)->first();
        if(!$seller){
            return response()->json([
                'alert' => 'info',
                'message' => 'seller tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $seller,
                'message' => 'seller ditemukan',
            ]);
        }
    }
    public function download(UserStore $seller){
        if($seller->ktp != null || $seller->selfie_ktp != null){
            $files = array($seller->ktp, $seller->selfie_ktp);
            $zipname = Str::slug($seller->name).'.zip';
            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);
            foreach ($files as $file) {
                $zip->addFile($file);
            }
            $zip->close();
        }else{
            return redirect()->route('office.seller.index');
        }
        // return Storage::download($seller->ktp);
    }
}
