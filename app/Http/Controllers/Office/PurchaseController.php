<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\Purchase;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Models\PurchaseDetail;
use App\Models\PurchasePayment;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PurchaseController extends Controller
{

    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities;

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Purchase::where('code','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.purchase.list', compact('collection'));
        }
        return view('page.office.purchase.main');
    }
    public function create()
    {
        $supplier = Supplier::get();
        return view('page.office.purchase.input',['data' => new Purchase, 'supplier' => $supplier ]);
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'supplier_id' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('supplier_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('supplier_id'),
                ]);
            }
        }
        if ($request->code){
            $code = $request->code.date("-Ym").'-';
        }else{
            $code = 'PRCH'.date("-Ym").'-';
        }
        $generator = Helper::IDGenerator(new Purchase,'code',$code);
        $purchase = new Purchase;
        $purchase->date = $request->tanggal;
        $purchase->code = $generator;
        $purchase->supplier_id = $request->supplier_id;
        $purchase->business_location_id = 0;
        $purchase->created_by = Auth::guard('office')->user()->id;
        // dd($purchase, $);
        $purchase->save();

        // log aktivity e,ployee
        $this->purchase_activity($purchase->id, Auth::user()->id, "created", "pembelian");
        
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembelian Tersimpan',
            'redirect' => 'input',
            'route' => route('office.purchase.edit',$purchase->id)
        ]);
    }
    public function show(Purchase $purchase)
    {
        return view('page.office.purchase.show', compact('purchase'));
    }
    public function edit(Purchase $purchase)
    {
        $supplier = Supplier::get();
        return view('page.office.purchase.input',['data' => $purchase, 'supplier' => $supplier ]);
    }
    public function update(Request $request, Purchase $purchase)
    {
        $purchase->supplier_id = $request->supplier_id;
        $purchase->date = $request->tanggal;
        $purchase->business_location_id = 0;
        // dd($purchase, $);
        $this->purchase_activity($purchase->id, Auth::user()->id, "update", "pembelian");
        $purchase->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Berhasil update pembelian',
        ]);
    }
    public function destroy(Purchase $purchase)
    {
        PurchaseDetail::where('purchase_id')->delete();
        PurchasePayment::where('purchase_id')->delete();
        
        // log aktivity e,ployee
        $this->purchase_activity($purchase->id, Auth::user()->id, "delete", "pembelian");

        $purchase->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembelian terhapus',
        ]);
    }
    public function process(Purchase $purchase)
    {
        if($purchase->purchase_detil->count() > 0){
            $purchase->st = "Dipesan";
            $purchase->update();
            // log aktivity e,ployee
            $this->purchase_activity($purchase->id, Auth::user()->id, "process", "pembelian");
            return response()->json([
                'alert' => 'success',
                'message' => 'Pembelian diproses',
                'redirect' => 'input',
                'route' => route('office.purchase.show',$purchase->id),
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Harap masukkan barang terlebih dahulu',
                'redirect' => 'input',
                'route' => route('office.purchase.show',$purchase->id),
            ]);
        }
    }
    public function delivered(Purchase $purchase)
    {
        Helper::increase_stock(new PurchaseDetail,new Purchase,'purchase_id',$purchase->id);
        $purchase->st = "Diterima";
        $this->purchase_activity($purchase->id, Auth::user()->id, "telah diterima", "pembelian");
        $purchase->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang diterima',
            'redirect' => 'input',
            'route' => route('office.purchase.show',$purchase->id),
        ]);
    }
}
