<?php

namespace App\Http\Controllers\Office;

use App\Models\Faq;
use App\Models\FaqCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FaqCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = FaqCategory::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.faqCategory.list', compact('collection'));
        }
        return view('page.office.faqCategory.main');
    }
    public function create()
    {
        return view('page.office.faqCategory.input', ['data' => new FaqCategory]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:faq_category',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $data = New FaqCategory;
        $data->name = Str::title($request->name);
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori tersimpan',
        ]);
    }
    public function show(FaqCategory $faqCategory)
    {
        //
    }
    public function edit(FaqCategory $faqCategory)
    {
        return view('page.office.faqCategory.input', ['data' => $faqCategory]);
    }
    public function update(Request $request, FaqCategory $faqCategory)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:faq_category',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $faqCategory->name = Str::title($request->name);
        $faqCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori terubah',
        ]);
    }
    public function destroy(FaqCategory $faqCategory)
    {
        Faq::where('faq_category_id','=',$faqCategory->id)->delete();
        $faqCategory->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori '. $faqCategory->name . ' terhapus',
        ]);
    }
}
