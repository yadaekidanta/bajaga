<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\SaleReturnDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SaleReturnDetailController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barang' => 'required',
            'price' => 'required',
            'qty' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('barang')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('barang'),
                ]);
            }elseif ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }elseif ($errors->has('qty')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $price = Str::remove(',', $request->price) ?: 0;
        $qty = Str::remove(',', $request->qty) ?: 0;

        if($price <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan harga',
            ]);
        }
        if($qty <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan jumlah',
            ]);
        }
        $check = SaleReturnDetail::where('sale_return_id', '=', $request->sale_return_id)->where('product_id', '=', $request->barang)->first();
        if(!$check){
            $detil = new SaleReturnDetail;
            $detil->qty = $qty;
            $detil->subtotal = $price * $qty;
        }else{
            $detil = $check;
            $detil->qty = $check->qty + $qty;
            $detil->subtotal = $price * $detil->qty;
        }
        $detil->sale_return_id = $request->sale_return_id;
        $detil->product_id = $request->barang;
        $detil->price = $price;
        if(!$check){
            $detil->save();
        }else{
            $detil->update();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.sale-return.show',$request->sale_return_id),
        ]);
    }
    public function destroy(SaleReturnDetail $saleReturnDetail)
    {
        $saleReturnDetail->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang terhapus',
            'redirect' => 'input',
            'route' => route('office.sale-return.show',$saleReturnDetail->sale_return_id),
        ]);
    }
}
