<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\ProductUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ProductUnitController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductUnit::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.productUnit.list', compact('collection'));
        }
        return view('page.office.productUnit.main');
    }
    public function create()
    {
        return view('page.office.productUnit.input', ['data' => new ProductUnit]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_unit',
            'shortname' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('shortname')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('shortname'),
                ]);
            }
        }
        $productUnit = new ProductUnit;
        $productUnit->name = $request->name;
        $productUnit->shortname = $request->shortname;
        $productUnit->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Satuan tersimpan',
        ]);
    }
    public function show(ProductUnit $productUnit)
    {
        //
    }
    public function edit(ProductUnit $productUnit)
    {
        return view('page.office.productUnit.input', ['data' => $productUnit]);
    }
    public function update(Request $request, ProductUnit $productUnit)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_brand',
            'shortname' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('shortname')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('shortname'),
                ]);
            }
        }
        $productUnit->name = $request->name;
        $productUnit->shortname = $request->shortname;
        $productUnit->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Satuan terubah',
        ]);
    }
    public function destroy(ProductUnit $productUnit)
    {
        $productUnit->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Satuan terhapus',
        ]);
    }
    public function get(Request $request)
    {
        $satuan = ProductUnit::where('id','=',$request->id)->first();
        if(!$satuan){
            return response()->json([
                'alert' => 'info',
                'message' => 'Satuan tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $satuan,
                'message' => 'Satuan ditemukan',
            ]);
        }
    }
    public function get_list(Request $request)
    {
        $result = ProductUnit::get();
        $list = "<option>Pilih Satuan</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->name | $row->shortname</option>";
        }
        return $list;
        // $collection = ProductUnit::where('name','LIKE','%'.$request->search.'%')->get();
        // return response()->json($collection);
    }
    public function input_modal()
    {
        return view('page.office.productUnit.input_modal', ['data' => new ProductUnit]);
    }
    public function list_modal(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductUnit::where('name','like','%'.$keywords.'%')
            ->get();
            return view('page.office.productUnit.list_modal',compact('collection'));
        }
    }
}