<?php

namespace App\Http\Controllers\Office;

use App\Models\Sale;
use App\Helpers\Helper;
use App\Models\Account;
use App\Models\SalePayment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SalePaymentController extends Controller
{

    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities();

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_name' => 'required',
            'payment_date' => 'required',
            'paid_amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('account_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_name'),
                ]);
            }elseif ($errors->has('payment_date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('payment_date'),
                ]);
            }elseif ($errors->has('paid_amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('paid_amount'),
                ]);
            }
        }
        $paid_amount = Str::remove(',', $request->paid_amount) ?: 0;
        if($paid_amount <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan pembayaran',
            ]);
        }
        $sale = Sale::where('id', '=', $request->id_sale)->first();
        $gt = $sale->grand_total;
        $tp = $sale->total_paid;
        if($paid_amount > $gt){
            return response()->json([
                'alert' => 'error',
                'message' => 'Pembayaran melebihi Rp '. number_format($sale->grand_total),
            ]);
        }
        $check = SalePayment::where('sale_id', '=', $request->id_sale)->where('account_id', '=', $request->account_id)->first();
        if(!$check){
            $payment = new SalePayment;
            $payment->total = $paid_amount;
        }else{
            // dd($check->total, $paid_amount);
            $payment = $check;
            $payment->total = $check->total + $paid_amount;
            if ($payment->total > $gt) {
                return response()->json([
                    'alert' => 'error',
                    'message' => 'Pembayaran melebihi Rp'. number_format($gt).'.<br>Sisa pembayaran Rp'.number_format($tp).'!',
                ]);
            }
        }
        // $sale->payment_st = ""
        $payment->account_id = $request->account_id;
        $payment->sale_id = $request->id_sale;
        $payment->date = $request->payment_date;
        $payment->note = $request->catatan ?? '-';
        $payment->created_by = Auth::guard('office')->user()->id;
        if(!$check){
            $payment->save();
        }else{
            $payment->update();
        }
        $this->purchase_activity($payment->id, Auth::user()->id, "created", "pembayaran");
        Helper::increase_balance_return(new SalePayment, new Sale, 'sale_id',$request->id_sale, $request->account_id);
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.sale.show',$request->id_sale),
        ]);
    }
    public function destroy(SalePayment $salePayment)
    {
        $this->purchase_activity($salePayment->id, Auth::user()->id, "delete", "pembayaran");
        Helper::decrease_balance_return(new SalePayment, new Sale, 'sale_id', $salePayment->sale_id, $salePayment->account_id);
        $salePayment->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran terhapus',
            'redirect' => 'input',
            'route' => route('office.sale.show',$salePayment->sale_id),
        ]);
    }
}
