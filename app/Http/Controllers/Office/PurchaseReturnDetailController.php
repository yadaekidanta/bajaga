<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PurchaseReturnDetail;
use Illuminate\Support\Facades\Validator;

class PurchaseReturnDetailController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barang' => 'required',
            'price' => 'required',
            'qty' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('barang')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('barang'),
                ]);
            }elseif ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }elseif ($errors->has('qty')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $price = Str::remove(',', $request->price) ?: 0;
        $qty = Str::remove(',', $request->qty) ?: 0;

        if($price <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan harga',
            ]);
        }
        if($qty <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan jumlah',
            ]);
        }
        $check = PurchaseReturnDetail::where('purchase_return_id', '=', $request->purchase_return_id)->where('product_id', '=', $request->barang)->first();
        if(!$check){
            $detil = new PurchaseReturnDetail;
            $detil->qty = $qty;
            $detil->subtotal = $price * $qty;
        }else{
            $detil = $check;
            $detil->qty = $check->qty + $qty;
            $detil->subtotal = $price * $detil->qty;
        }
        $cek_stok = Helper::check_stock($request->barang,$qty);
        if($cek_stok == 0){
            return response()->json([
				'alert' => 'info',
				'message' => 'Stok barang tidak cukup',
			]);
        }
        $detil->purchase_return_id = $request->purchase_return_id;
        $detil->product_id = $request->barang;
        $detil->price = $price;
        if(!$check){
            $detil->save();
        }else{
            $detil->update();
        }
        $detil->purchase_return->grand_total = $detil->subtotal;
        $detil->purchase_return->update();
        
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.purchase-return.show',$request->purchase_return_id),
        ]);
    }
    public function destroy(PurchaseReturnDetail $purchaseReturnDetail)
    {
        $purchaseReturnDetail->purchase_return->grand_total -= $purchaseReturnDetail->subtotal;
        $purchaseReturnDetail->purchase_return->update();
        $purchaseReturnDetail->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang terhapus',
            'redirect' => 'input',
            'route' => route('office.purchase-return.show',$purchaseReturnDetail->purchase_return_id),
        ]);
    }
}