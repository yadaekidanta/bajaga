<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\Account;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PurchaseReturn;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use Illuminate\Support\Facades\Auth;
use App\Models\PurchaseReturnPayment;
use Illuminate\Support\Facades\Validator;

class PurchaseReturnPaymentController extends Controller
{

    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities();

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_name' => 'required',
            'payment_date' => 'required',
            'paid_amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('account_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_name'),
                ]);
            }elseif ($errors->has('payment_date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('payment_date'),
                ]);
            }elseif ($errors->has('paid_amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('paid_amount'),
                ]);
            }
        }
        $paid_amount = Str::remove(',', $request->paid_amount) ?: 0;
        if($paid_amount <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan pembayaran',
            ]);
        }
        $purchase = PurchaseReturn::where('id', '=', $request->id_purchase_return)->first();
        $gt = $purchase->grand_total;
        if($paid_amount > $gt){
            return response()->json([
                'alert' => 'error',
                'message' => 'Pembayaran melebihi Rp '. number_format($purchase->grand_total),
            ]);
        }
        $check = PurchaseReturnPayment::where('purchase_return_id', '=', $request->id_purchase_return)->where('account_id', '=', $request->account_id)->first();
        if(!$check){
            $payment = new PurchaseReturnPayment;
            $payment->total = $paid_amount;
        }else{
            $payment = $check;
            $payment->total = $check->total + $paid_amount;
        }
        $payment->account_id = $request->account_id;
        $payment->purchase_return_id = $request->id_purchase_return;
        $payment->date = $request->payment_date;
        $payment->note = $request->catatan ?? '-';
        $payment->created_by = Auth::guard('office')->user()->id;
        if(!$check){
            $payment->save();
        }else{
            $payment->update();
        }
        
        $this->purchase_activity($request->id_purchase, Auth::user()->id, "created", "pembayaran");
        
        Helper::increase_balance_return(new PurchaseReturnPayment, new PurchaseReturn, 'purchase_return_id',$request->id_purchase_return, $request->account_id);
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.purchase-return.show',$request->id_purchase_return),
        ]);
    }
    public function destroy(PurchaseReturnPayment $purchaseReturnPayment)
    {
        $this->purchase_activity($purchaseReturnPayment->id_purchase, Auth::user()->id, "delete", "pembayaran");
        Helper::decrease_balance_return(new PurchaseReturnPayment, new PurchaseReturn, 'purchase_return_id', $purchaseReturnPayment->purchase_return_id, $purchaseReturnPayment->account_id);
        $purchaseReturnPayment->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran terhapus',
            'redirect' => 'input',
            'route' => route('office.purchase-return.show',$purchaseReturnPayment->purchase_return_id),
        ]);
    }
}
