<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\SellingPricing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class SellingPricingController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = SellingPricing::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.sellingPricing.list', compact('collection'));
        }
        return view('page.office.sellingPricing.main');
    }
    public function create()
    {
        return view('page.office.sellingPricing.input', ['data' => new SellingPricing]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'nilai' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('type')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif($errors->has('nilai')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }
        }
        $selling = New SellingPricing;
        if($request->type == "persentase"){
            $selling->persentase = Str::remove(',',$request->nilai);
        }else{
            $selling->price = Str::remove(',',$request->nilai);
        }
        $selling->name = $request->name;
        $selling->type = $request->type;
        $selling->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kelompok Harga Jual tersimpan',
        ]);
    }
    public function show(SellingPricing $sellingPrice)
    {
        //
    }
    public function edit(SellingPricing $sellingPrice)
    {
        return view('page.office.sellingPricing.input', ['data' => $sellingPrice]);
    }
    public function update(Request $request, SellingPricing $sellingPrice)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'nilai' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('type')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif($errors->has('nilai')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }
        }
        $sellingPrice->name = $request->name;
        $sellingPrice->type = $request->type;
        if($request->type == "persentase"){
            $sellingPrice->persentase = Str::remove(',',$request->nilai);
        }else{
            $sellingPrice->price = Str::remove(',',$request->nilai);
        }
        $sellingPrice->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kelompok Harga Jual terubah',
        ]);
    }
    public function destroy(SellingPricing $sellingPrice)
    {
        $sellingPrice->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kelompok Harga Jual terhapus',
        ]);
    }
    
}