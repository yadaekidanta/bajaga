<?php

namespace App\Http\Controllers\Office;

use App\Models\BusinessLocation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class BusinessLocationController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = BusinessLocation::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.businessLocation.list', compact('collection'));
        }
        return view('page.office.businessLocation.main');
    }
    public function create()
    {
        return view('page.office.businessLocation.input', ['data' => new BusinessLocation]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'phone' => 'required',
            'email' => 'required|email|max:255',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('code')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif($errors->has('address')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }elseif($errors->has('province_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province_id'),
                ]);
            }elseif($errors->has('city_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city_id'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        $businessLocation = new BusinessLocation;
        $generator=Helper::IDGenerator(new BusinessLocation,'code','LOC-');
        $businessLocation->name = $request->name;
        $businessLocation->code = $generator;
        $businessLocation->address = $request->address;
        $businessLocation->province_id = $request->province_id;
        $businessLocation->city_id = $request->city_id;
        $businessLocation->phone = $request->phone;
        $businessLocation->email = $request->email;
        $businessLocation->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lokasi Bisnis tersimpan',
        ]);
    }
    public function show(BusinessLocation $businessLocation)
    {
        //
    }
    public function edit(BusinessLocation $businessLocation)
    {
        return view('page.office.businessLocation.input', ['data' => $businessLocation]);
    }
    public function update(Request $request, BusinessLocation $businessLocation)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'phone' => 'required',
            'email' => 'required|email|max:255',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('code')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif($errors->has('address')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }elseif($errors->has('province_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province_id'),
                ]);
            }elseif($errors->has('city_id')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city_id'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        $businessLocation->name = $request->name;
        $businessLocation->code = $request->code;
        $businessLocation->address = $request->address;
        $businessLocation->province_id = $request->province_id;
        $businessLocation->city_id = $request->city_id;
        $businessLocation->phone = $request->phone;
        $businessLocation->email = $request->email;
        $businessLocation->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lokasi Bisnis terubah',
        ]);
    }
    public function destroy(BusinessLocation $businessLocation)
    {
        $businessLocation->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lokasi Bisnis terhapus',
        ]);
    }
    public function get_list(Request $request)
    {
        $result = BusinessLocation::get();
        $list = "<option>Pilih Lokasi Bisnis</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
}
