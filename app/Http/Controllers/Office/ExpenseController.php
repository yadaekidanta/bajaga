<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\EmployeActivities;
use App\Models\ExpenseCategory;
use App\Models\ExpensePayment;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ExpenseController extends Controller
{

    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities();

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Expense::paginate(10);
            return view('page.office.expense.list', compact('collection'));
        }
        return view('page.office.expense.main');
    }
    public function create()
    {
        $category = ExpenseCategory::get();
        // dd($category);
        return view('page.office.expense.input', ['data' => new Expense, 'category' => $category]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'expense_category_id' => 'required|integer',
            'date' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('expense_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('expense_category_id'),
                ]);
            }elseif ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }
        if(Str::remove(',',$request->amount) <= 0){
            return response()->json([
                'alert' => 'info',
                'message' => 'Harap masukkan jumlah',
            ]);
        }
        $generator = Helper::IDGenerator(new Expense,'code','BP-'.date('Y/m-'));
        $expense = new Expense;
        $expense->code = $generator;
        $expense->expense_category_id = $request->expense_category_id;
        $expense->date = $request->date;
        $expense->amount = Str::remove(',',$request->amount);
        $expense->note = $request->note;
        $expense->created_at = date("Y-m-d H:i:s");
        $expense->created_by = Auth::guard('office')->user()->id;
        if(request()->file('document')){
            $document = request()->file('document')->store("expense");
            $expense->document = $document;
        }
        $expense->save();
        $this->purchase_activity($expense->id, Auth::user()->id, "created", "pengeluaran");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengeluaran tersimpan',
            'redirect' => 'input',
            'route' => route('office.expense.edit',$expense->id),
        ]);
    }
    public function show(Expense $expense)
    {
        return view('page.office.expense.show', ['data' => $expense]);
    }
    public function edit(Expense $expense)
    {
        $category = ExpenseCategory::get();
        return view('page.office.expense.input', ['data' => $expense, 'category' => $category]);
    }
    public function update(Request $request, Expense $expense)
    {
        $validator = Validator::make($request->all(), [
            'expense_category_id' => 'required|integer',
            'date' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('expense_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('expense_category_id'),
                ]);
            }elseif ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }
        $expense->expense_category_id = $request->expense_category_id;
        $expense->date = $request->date;
        $expense->amount = Str::remove(',',$request->amount);
        $expense->note = $request->note;
        if(request()->file('document')){
            Storage::delete($expense->doc);
            $document = request()->file('document')->store("expense");
            $expense->document = $document;
        }
        $expense->update();
        $this->purchase_activity($expense->id, Auth::user()->id, "update", "pengeluaran");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengeluaran terubah',
            'redirect' => 'input',
            'route' => route('office.expense.edit',$expense->id),
        ]);
    }
    public function destroy(Expense $expense)
    {
        $expense_payment = ExpensePayment::where('expense_id','=',$expense->id)->first();
        ExpensePayment::PengembalianSaldo($expense_payment->account_id,$expense_payment->amount);
        if($expense->doc){
            Storage::delete($expense->doc);
        }
        $this->purchase_activity($expense->id, Auth::user()->id, "delete", "pengeluaran");
        $expense->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengeluaran terhapus',
        ]);
    }
    public function download(Expense $expense){
        return Storage::download($expense->document);
    }
}
