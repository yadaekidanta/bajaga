<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\Account;
use App\Models\Purchase;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PurchasePayment;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PurchasePaymentController extends Controller
{

    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities();

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_name' => 'required',
            'payment_date' => 'required',
            'paid_amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('account_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_name'),
                ]);
            }elseif ($errors->has('payment_date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('payment_date'),
                ]);
            }elseif ($errors->has('paid_amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('paid_amount'),
                ]);
            }
        }
        $paid_amount = Str::remove(',', $request->paid_amount) ?: 0;
        if($paid_amount <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan pembayaran',
            ]);
        }
        
        $purchase = Purchase::where('id', '=', $request->id_purchase)->first();
        $gt = $purchase->grand_total;
        if($paid_amount > $gt){
            return response()->json([
                'alert' => 'error',
                'message' => 'Pembayaran melebihi Rp '. number_format($purchase->grand_total),
            ]);
        }
        $check = PurchasePayment::where('purchase_id', '=', $request->id_purchase)->where('account_id', '=', $request->account_id)->first();
        if(!$check){
            $payment = new PurchasePayment;
            $payment->total = $paid_amount;
        }else{
            $payment = $check;
            $payment->total = $check->total + $paid_amount;
        }
        $account = Account::where('id','=',$request->account_id)->first();
        if($account->balance < $paid_amount){
            return response()->json([
                'alert' => 'error',
                'message' => 'Saldo tersisa Rp ' . number_format($account->balance),
            ]);
        }
        $payment->account_id = $request->account_id;
        $payment->purchase_id = $request->id_purchase;
        $payment->date = $request->payment_date;
        $payment->note = $request->catatan ?? '-';
        $payment->created_by = Auth::guard('office')->user()->id;
        if(!$check){
            $payment->save();
        }else{
            $payment->update();
        }

        $this->purchase_activity($request->id_purchase, Auth::user()->id, "created", "pembayaran");
        
        Helper::decrease_balance(new PurchasePayment, new Purchase, 'purchase_id',$request->id_purchase, $request->account_id);
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.purchase.show',$request->id_purchase),
        ]);
    }
    public function destroy(PurchasePayment $purchasePayment)
    {
        $this->purchase_activity($purchasePayment->purchase_id, Auth::user()->id, "delete", "pembayaran");

        Helper::increase_balance(new PurchasePayment, new Purchase, 'purchase_id', $purchasePayment->purchase_id, $purchasePayment->account_id);
        $purchasePayment->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran terhapus',
            'redirect' => 'input',
            'route' => route('office.purchase.show',$purchasePayment->purchase_id),
        ]);
    }
}