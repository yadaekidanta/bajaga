<?php

namespace App\Http\Controllers\Office;

use App\Models\ProductWarranty;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ProductWarrantyController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductWarranty::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.productWarranty.list', compact('collection'));
        }
        return view('page.office.productWarranty.main');
    }
    public function create()
    {
        return view('page.office.productWarranty.input', ['data' => new ProductWarranty]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_warranty',
            'description' => 'required',
            'duration' => 'required',
            'duration_type' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('description')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif($errors->has('duration')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('duration'),
                ]);
            }elseif($errors->has('duration_type')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('duration_type'),
                ]);
            }
        }
        $productWarranty = new ProductWarranty;
        $productWarranty->name = $request->name;
        $productWarranty->description = $request->description;
        $productWarranty->duration = $request->duration;
        $productWarranty->duration_type = $request->duration_type;
        $productWarranty->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Garansi tersimpan',
        ]);
    }
    public function show(ProductWarranty $productWarranty)
    {
        //
    }
    public function edit(ProductWarranty $productWarranty)
    {
        return view('page.office.productWarranty.input', ['data' => $productWarranty]);
    }
    public function update(Request $request, ProductWarranty $productWarranty)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'duration' => 'required',
            'duration_type' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('description')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif($errors->has('duration')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('duration'),
                ]);
            }elseif($errors->has('duration_type')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('duration_type'),
                ]);
            }
        }
        $productWarranty->name = $request->name;
        $productWarranty->description = $request->description;
        $productWarranty->duration = $request->duration;
        $productWarranty->duration_type = $request->duration_type;
        $productWarranty->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Garansi terubah',
        ]);
    }
    public function destroy(ProductWarranty $productWarranty)
    {
        $productWarranty->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Garansi terhapus',
        ]);
    }
    public function get_list(Request $request)
    {
        $result = ProductWarranty::get();
        $list = "<option>Pilih Garansi</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->name | $row->duration $row->duration_type</option>";
        }
        return $list;
    }
}
