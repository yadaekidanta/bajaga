<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\AccountType;
use App\Models\ExpensePayment;
use App\Models\Purchase;
use App\Models\PurchasePayment;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnPayment;
use App\Models\Sale;
use App\Models\SalePayment;
use App\Models\SaleReturn;
use App\Models\SaleReturnPayment;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function balance_sheet(Request $request)
    {
        $pendapatan = Account::where('account_type_id', 1)->sum("balance");
        $utang = Account::where('account_type_id', 2)->sum("balance");
        $modal = Account::where('account_type_id', 3)->sum("balance");
        $biaya = Account::where('account_type_id', 4)->sum("balance");
        $asset = Account::where('account_type_id', 5)->sum("balance");
        $pinjaman_bank = Account::where('account_type_id', 6)->sum("balance");
        $kas = Account::where('account_type_id', 7)->sum("balance");
        $persedian_barang = Account::where('account_type_id', 8)->sum("balance");
        $piutang_penjualan = Account::where('account_type_id', 9)->sum("balance");
        $bank = Account::where('account_type_id', 10)->sum("balance");
        $piutang_ragu_ragu = Account::where('account_type_id', 11)->sum("balance");
        $pendapatan = Account::where('account_type_id', 12)->sum("balance");

        return view('page.office.balanceSheet.main', compact('pendapatan', 'utang', 'modal', 'biaya', 'asset', 'pinjaman_bank', 'kas', 'persedian_barang', 'piutang_penjualan', 'bank', 'piutang_ragu_ragu', 'pendapatan'));
    }
    public function trial_balance(Request $request)
    {
        if ($request->ajax()) {
            return view('page.office.trailBalance.list');
        }
        return view('page.office.trailBalance.main');
    }
    public function cash_flow(Request $request)
    {
        $sale = Sale::whereHas("sale_payment", function ($query) {
            return $query->where('account_id', '=', 1);
        })->select("id", "total_paid", "payment_st", "date", "created_by");
        $sale_return = SaleReturn::whereHas("sale_payment", function ($query) {
            return $query->where('account_id', '=', 1);
        })->select("id", "total_paid", "payment_st", "date", "created_by");
        $purchase = Purchase::whereHas("purchase_payment", function ($query) {
            return $query->where('account_id', '=', 1);
        })->select("id", "total_paid", "payment_st", "date", "created_by");
        $purchase_return = PurchaseReturn::whereHas("purchase_payment", function ($query) {
            return $query->where('account_id', '=', 1);
        })->select("id", "total_paid", "payment_st", "date", "created_by");
        $expanse = ExpensePayment::where("account_id", 1)->select("id", "amount", "amount", "date", "created_by");
        $collection = $sale
        ->unionAll($sale_return)
        ->unionAll($purchase)
        ->unionAll($purchase_return)
        // ->unionAll($expanse)
        ->paginate(10);

        // dd($collection);

        if ($request->ajax()) {
            return view('page.office.cashFlow.list', compact("collection"));
        }
        return view('page.office.cashFlow.main');
        // 
    }
    public function payment_account_report(Request $request)
    {
        if ($request->ajax()) {
            return view('page.office.paymentAccountReport.list');
        }
        return view('page.office.paymentAccountReport.main');
    }
}
