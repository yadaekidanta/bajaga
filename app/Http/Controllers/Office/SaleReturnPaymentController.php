<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\Account;
use App\Models\SaleReturn;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\SaleReturnPayment;
use App\Http\Controllers\Controller;
use App\Models\Sale;
use App\Models\User;
use App\Notifications\TransactionNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class SaleReturnPaymentController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_name' => 'required',
            'payment_date' => 'required',
            'paid_amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('account_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_name'),
                ]);
            }elseif ($errors->has('payment_date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('payment_date'),
                ]);
            }elseif ($errors->has('paid_amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('paid_amount'),
                ]);
            }
        }
        $paid_amount = Str::remove(',', $request->paid_amount) ?: 0;
        if($paid_amount <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan pembayaran',
            ]);
        }
        $purchase = SaleReturn::where('id', '=', $request->id_sale_return)->first();
        $gt = $purchase->grand_total;
        if($paid_amount > $gt){
            return response()->json([
                'alert' => 'error',
                'message' => 'Pembayaran melebihi Rp '. number_format($purchase->grand_total),
            ]);
        }
        $check = SaleReturnPayment::where('sale_return_id', '=', $request->id_sale_return)->where('account_id', '=', $request->account_id)->first();
        if(!$check){
            $payment = new SaleReturnPayment;
            $payment->total = $paid_amount;
        }else{
            $payment = $check;
            $payment->total = $check->total + $paid_amount;
        }
        $account = Account::where('id','=',$request->account_id)->first();
        if($account->balance < $paid_amount){
            return response()->json([
                'alert' => 'error',
                'message' => 'Saldo tersisa Rp ' . number_format($account->balance),
            ]);
        }
        $payment->account_id = $request->account_id;
        $payment->sale_return_id = $request->id_sale_return;
        $payment->date = $request->payment_date;
        $payment->note = $request->catatan ?? '-';
        $payment->created_by = Auth::guard('office')->user()->id;
        if(!$check){
            $payment->save();
        }else{
            $payment->update();
        }
        Helper::decrease_balance(new SaleReturnPayment, new SaleReturn, 'sale_return_id',$request->id_sale_return, $request->account_id);
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.sale-return.show',$request->id_sale_return),
        ]);
    }
    public function destroy(SaleReturnPayment $saleReturnPayment)
    {
        Helper::increase_balance(new SaleReturnPayment, new SaleReturn, 'sale_return_id', $saleReturnPayment->sale_return_id, $saleReturnPayment->account_id);
        $saleReturnPayment->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran terhapus',
            'redirect' => 'input',
            'route' => route('office.sale-return.show',$saleReturnPayment->sale_return_id),
        ]);
    }
}
