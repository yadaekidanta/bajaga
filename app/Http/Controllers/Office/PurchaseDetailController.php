<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Purchase;
use Illuminate\Http\Request;
use App\Models\PurchaseDetail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
class PurchaseDetailController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'price' => 'required',
            'qty' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('product_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_id'),
                ]);
            }elseif ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }elseif ($errors->has('qty')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $price = Str::remove(',', $request->price) ?: 0;
        $qty = Str::remove(',', $request->qty) ?: 0;

        if($price <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan harga',
            ]);
        }
        if($qty <= 0){
            return response()->json([
                'alert' => 'error',
                'message' => 'Harap masukkan jumlah',
            ]);
        }
        $check = PurchaseDetail::where('purchase_id', '=', $request->purchase_id)->where('product_id', '=', $request->product_id)->first();
        if(!$check){
            $detil = new PurchaseDetail;
            $detil->qty = $qty;
            $detil->subtotal = $price * $qty;
        }else{
            $detil = $check;
            $detil->qty = $check->qty + $qty;
            $detil->subtotal = $price * $detil->qty;
        }
        $detil->purchase_id = $request->purchase_id;
        $detil->product_id = $request->product_id;
        $detil->price = $price;
        if(!$check){
            $detil->save();
        }else{
            $detil->update();
        }
        $detil->purchase->grand_total = $detil->subtotal;
        $detil->purchase->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang berhasil ditambah',
            'redirect' => 'input',
            'route' => route('office.purchase.show',$request->purchase_id),
        ]);
    }
    public function destroy(PurchaseDetail $purchaseDetail)
    {
        $purchaseDetail->purchase->grand_total = $purchaseDetail->subtotal;
        $purchaseDetail->purchase->update();
        $purchaseDetail->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang terhapus',
            'redirect' => 'input',
            'route' => route('office.purchase.show',$purchaseDetail->purchase_id),
        ]);
    }
}
