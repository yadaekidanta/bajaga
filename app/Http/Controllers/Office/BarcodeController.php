<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductLelang;
use App\Models\ProductUnit;
use App\Models\ProductWarranty;
use App\Models\ProductCategory;
use App\Models\BusinessLocation;
use App\Http\Controllers\Controller;
use App\Models\ProductVarian;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PDF;

class BarcodeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $product = Product::get();
            $productlelang = ProductLelang::get();
            $lokasi = BusinessLocation::get();
            $merek = ProductBrand::get();
            $kategori = ProductCategory::where('product_category_id', 0)->get();
            $satuan = ProductUnit::get();
            $garansi = ProductWarranty::get();
            return view('page.office.barcode.list', ['product' => $product, 'lokasi'=>$lokasi, 'merek'=>$merek, 'kategori'=>$kategori, 'satuan'=>$satuan, 'garansi'=>$garansi,'productlelang'=>$productlelang]);
        }
        return view('page.office.barcode.main');
    }
    public function create()
    {
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::where('product_category_id', 0)->get();
        $satuan = ProductUnit::get();
        $garansi = ProductWarranty::get();
        return view('page.office.barcode.input', ['data' => new Product, 'lokasi'=>$lokasi, 'merek'=>$merek, 'kategori'=>$kategori, 'satuan'=>$satuan, 'garansi'=>$garansi]);
    }
    public function store(Request $request)
    {
        // request()->validate([
        //     'name' => 'required',
        //     'qty' => 'required',
        // ]);
          
        // $data = 
        // [
        //     'name' => $request->name,
        //     'qty' => $request->qty
        // ];
        // $pdf = PDF::loadView('page.office.barcode.pdf', $data);
        // return $pdf->download('Barcode.pdf');
    }
    public function show()
    {
        //
    }
    public function edit(Product $product)
    {
        $lokasi = BusinessLocation::get();
        $merek = ProductBrand::get();
        $kategori = ProductCategory::where('product_category_id', 0)->get();
        $satuan = ProductUnit::get();
        $garansi = ProductWarranty::get();
        return view('page.office.product.input', ['data' => $product, 'lokasi'=>$lokasi, 'merek'=>$merek, 'kategori'=>$kategori, 'satuan'=>$satuan, 'garansi'=>$garansi]);
    }
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'product_unit_id' => 'required',
            'product_brand_id' => 'required',
            'product_category_id' => 'required',
            'product_warranty_id' => 'required',
            'desc' => 'required',
            'weight' => 'required',
            'business_location' => 'required',
            'alert_quantity' => 'required',
            'condition' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('product_unit_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_unit_id'),
                ]);
            }else if ($errors->has('product_brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_brand_id'),
                ]);
            }else if ($errors->has('product_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_category_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('weight')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            }else if ($errors->has('business_location')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('business_location'),
                ]);
            }else if ($errors->has('product_warranty_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_warranty_id'),
                ]);
            }else if ($errors->has('condition')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('condition'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alert_quantity'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($product->photo);
            $photo = request()->file('photo')->store("product");
            $product->photo = $photo;
        }
        if(request()->file('brocure')){
            Storage::delete($product->brocure);
            $brocure = request()->file('brocure')->store("product_brocure");
            $product->brocure = $brocure;
        }
        $product->condition = $request->condition;
        $product->video_url = $request->video_url;
        $product->product_brand_id = $request->product_brand_id;
        $product->product_category_id = $request->product_category_id;
        $product->product_subcategory_id = $request->product_subcategory_id;
        $product->product_unit_id = $request->product_unit_id;
        $product->product_warranty_id = $request->product_warranty_id;
        $product->desc = $request->desc;
        $product->weight = $request->weight;
        $product->business_location_id = $request->business_location;
        $product->alert_quantity = $request->alert_quantity;
        $product->custom_field_1 = $request->custom_field_1;
        $product->custom_field_2 = $request->custom_field_2;
        $product->custom_field_3 = $request->custom_field_3;
        $product->custom_field_4 = $request->custom_field_4;
        $product->name = Str::title($request->name);
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk '. $request->title . ' terubah',
        ]);
    }
    public function destroy(Product $product)
    {
        $produk = Product::where('sku','like','%'.$product->sku.'%')->get();
        foreach($produk AS $prd){
            Storage::delete($prd->photo);
            Storage::delete($prd->brochure);
            $prd->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk terhapus',
        ]);
    }
    public function request_download_pdf(Request $request){
        request()->validate([
            'name' => 'required',
            'qty' => 'required',
        ]);
        $id = $request->name;
        $data = [
            'id' => $id,
            'qty' => $request->qty,
            'tipe' => $request->tipe
        ];
        return response()->json([
            'alert' => 'success',
            'message' => 'Barcode terbentuk',
            'redirect' => 'callback',
            'route' => route('office.barcode.pdfDownload',$data),
        ]);
    }
    
    public static function pdfDownload(Request $data){
        $product = Product::where('id',$data->id)->first();
        $pdf = PDF::loadView('page.office.barcode.pdf',['qty'=> $data->qty,'tipe'=> $data->tipe, 'product' => $product]);
        return $pdf->download('Barcode.pdf');
     }
}
