<?php

namespace App\Http\Controllers\Office;
use App\Models\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Employee::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.employee.list', compact('collection'));
        }
        return view('page.office.employee.main');
    }
    public function create()
    {
        return view('page.office.employee.input', ['data' => new Employee]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nik' => 'required',
            'name' => 'required',
            'email' => 'required|email|max:255',
            'phone' => 'required',
            'role' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif($errors->has('nik')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nik'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif($errors->has('role')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }
        }
        $employee = New Employee;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("Employee");
            $employee->photo = $photo;
        }
        $employee->nik = $request->nik;
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->role = $request->role;
        $employee->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pegawai tersimpan',
        ]);
    }
    public function show(Employee $employee)
    {
        //
    }
    public function edit(Employee $employee)
    {
        return view('page.office.employee.input', ['data' => $employee]);
    }
    public function update(Request $request, Employee $employee)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|max:255',
            'phone' => 'required',
            'role' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif($errors->has('role')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }
        }
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("Employee");
            $employee->photo = $photo;
        }
        $employee->nik = $request->nik;
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->role = $request->role;
        $employee->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pegawai terubah',
        ]);
    }
    public function destroy(Employee $employee)
    {
        $employee->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pegawai terhapus',
        ]);
    }
}
