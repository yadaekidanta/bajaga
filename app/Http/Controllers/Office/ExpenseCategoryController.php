<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ExpenseCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExpenseCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ExpenseCategory::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.expenseCategory.list', compact('collection'));
        }
        return view('page.office.expenseCategory.main');
    }
    public function create()
    {
        return view('page.office.expenseCategory.input', ['data' => new ExpenseCategory]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:expense_category',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $data = New ExpenseCategory;
        if ($request->code){
            $code = $request->code.'-';
        }else{
            $code = 'BYP-';
        }
        $generator = Helper::IDGenerator(new ExpenseCategory,'code',$code);
        $data->name = Str::title($request->name);
        $data->code = $generator;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Pengeluaran tersimpan',
        ]);
    }
    public function show(ExpenseCategory $expenseCategory)
    {
        //
    }
    public function edit(ExpenseCategory $expenseCategory)
    {
        return view('page.office.expenseCategory.input', ['data' => $expenseCategory]);
    }
    public function update(Request $request, ExpenseCategory $expenseCategory)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|exists:expense_category,name',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $expenseCategory->name = Str::title($request->name);
        $expenseCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Pengeluaran terubah',
        ]);
    }
    public function destroy(ExpenseCategory $expenseCategory)
    {
        $expenseCategory->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Pengeluaran terhapus',
        ]);
    }
    public function get_list(Request $request)
    {
        $result = ExpenseCategory::get();
        $list = "<option>Pilih Jenis Pengeluaran</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->code | $row->name</option>";
        }
        return $list;
    }
}
