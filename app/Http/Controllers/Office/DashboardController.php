<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use App\Models\Product;
use App\Models\SaleDetail;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $trending = SaleDetail::selectRaw('product_id, COUNT(product_id) AS counting')
        ->whereHas('product', function ($query) {
            return $query->where('store_id', '=', 1);
        })
        ->groupBy('product_id')
        ->orderBy(\DB::raw('count(product_id)'), 'DESC')
        ->paginate(5, ['*'], 'tranding');

        $alert_quantity = Product::whereRaw("store_id = 1 AND stock > alert_quantity")
        ->paginate(5, ['*'], 'stock');

        $employee_activity = EmployeActivities::paginate(5, ['*'], 'activity');
        
        return view('page.office.dashboard.main', compact('alert_quantity', 'trending', 'employee_activity'));
    }
    
    // public function data_stock()
    // {
    //     $alert_quantity = Product::whereRaw("store_id = 1 AND stock < alert_quantity")
    //     ->paginate(5);
    //     return view('page.office.dashboard.data_stock', compact('alert_quantity'));
    // }
    
    // public function data_tranding()
    // {
    //     $trending = SaleDetail::selectRaw('product_id, COUNT(product_id) AS counting')
    //     ->whereHas('product', function ($query) {
    //         return $query->where('store_id', '=', 1);
    //     })
    //     ->groupBy('product_id')
    //     ->orderBy(\DB::raw('count(product_id)'), 'DESC')
    //     ->paginate(5);
    //     return view('page.office.dashboard.data_stock', compact('trending'));
    // }
    
    // public function data_log()
    // {
    //     $employee_activity = EmployeActivities::simplePaginate(5);
    //     return view('page.office.dashboard.data_stock', compact('employee_activity'));
    // }
    
}
