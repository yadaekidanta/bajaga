<?php

namespace App\Http\Controllers\Office;

use App\Models\Sale;
use App\Helpers\Helper;
use App\Models\SaleReturn;
use Illuminate\Http\Request;
use App\Models\SaleReturnDetail;
use App\Models\SaleReturnPayment;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SaleReturnController extends Controller
{

    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities();

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = SaleReturn::where('code','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.saleReturn.list', compact('collection'));
        }
        return view('page.office.saleReturn.main');
    }
    public function create()
    {
        $sale = Sale::get();
        return view('page.office.saleReturn.input',['data' => new SaleReturn, 'sale' => $sale ]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'penjualan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }elseif ($errors->has('penjualan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('penjualan'),
                ]);
            }
        }
        if ($request->code){
            $code = $request->code.date("-Ym").'-';
        }else{
            $code = 'SRTRN'.date("-Ym").'-';
        }
        $generator = Helper::IDGenerator(new SaleReturn,'code',$code);
        $sale = new SaleReturn;
        $sale->date = $request->tanggal;
        $sale->code = $generator;
        $sale->sale_id = $request->penjualan;
        $sale->created_by = Auth::guard('office')->user()->id;
        $sale->save();
        $this->purchase_activity($sale->id, Auth::user()->id, "created", "return_penjualan");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengembalian Tersimpan',
            'redirect' => 'input',
            'route' => route('office.sale-return.edit',$sale->id)
        ]);
    }
    public function show(SaleReturn $saleReturn)
    {
        return view('page.office.saleReturn.show', compact('saleReturn'));
    }
    public function edit(SaleReturn $saleReturn)
    {
        $sale = Sale::get();
        return view('page.office.saleReturn.input',['data' => $saleReturn, 'sale' => $sale ]);
    }
    public function update(Request $request, SaleReturn $saleReturn)
    {
        $saleReturn->date = $request->tanggal;
        $saleReturn->update();
        $this->purchase_activity($saleReturn->id, Auth::user()->id, "update", "return_penjualan");
    }
    public function process(SaleReturn $saleReturn)
    {
        if($saleReturn->sale_detil->count() > 0){
            $saleReturn->st = "Diproses";
            $saleReturn->update();
            $this->purchase_activity($saleReturn, Auth::user()->id, "process", "return_penjualan");
            return response()->json([
                'alert' => 'success',
                'message' => 'Pengembalian diproses',
                'redirect' => 'input',
                'route' => route('office.sale-return.show',$saleReturn->id),
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Harap masukkan barang terlebih dahulu',
                'redirect' => 'input',
                'route' => route('office.sale-return.show',$saleReturn->id),
            ]);
        }
    }

    public function delivered(SaleReturn $saleReturn)
    {
        $this->purchase_activity($saleReturn, Auth::user()->id, "telah diterima", "return_penjualan");
        Helper::increase_stock(new SaleReturnDetail,new SaleReturn,'sale_return_id',$saleReturn->id);
        $saleReturn->st = "Diterima";
        $saleReturn->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang diterima',
            'redirect' => 'input',
            'route' => route('office.sale-return.show',$saleReturn->id),
        ]);
    }

    public function destroy(SaleReturn $saleReturn)
    {
        SaleReturnDetail::where('sale_id')->delete();
        SaleReturnPayment::where('sale_id')->delete();
        $saleReturn->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengembalian terhapus',
        ]);
    }
}
