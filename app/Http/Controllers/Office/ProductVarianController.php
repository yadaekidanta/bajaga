<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\Product;
use App\Http\Controllers\Controller;
use App\Models\ProductVarian;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductVarianController extends Controller
{
    public function index(Request $request, Product $product)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductVarian::
            where('sku','like','%'.$product->sku.'%')
            ->where('name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.productVarian.list',compact('collection','product'));
        }
        return view('page.office.productVarian.main',compact('product'));
    }
    public function create(Product $product)
    {
        return view('page.office.productVarian.input', ['data' => new ProductVarian , 'product' => $product]);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'product_unit_id' => 'required',
            'product_brand_id' => 'required',
            'product_category_id' => 'required',
            'product_warranty_id' => 'required',
            'desc' => 'required',
            'weight' => 'required',
            'alert_quantity' => 'required',
            'business_location' => 'required',
            'condition' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('product_unit_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_unit_id'),
                ]);
            }else if ($errors->has('product_brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_brand_id'),
                ]);
            }else if ($errors->has('product_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_category_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('weight')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            }else if ($errors->has('business_location')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('business_location'),
                ]);
            }else if ($errors->has('product_warranty_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_warranty_id'),
                ]);
            }else if ($errors->has('condition')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('condition'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alert_quantity'),
                ]);
            }
        }
        $sku = $request->sku.'#';
        $cek = Product::where('sku',$request->sku)->first();
        $generator=Helper::IDGenerator(new Product,'sku',$sku);
        $product = new Product;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("product");
            $product->photo = $photo;
        }
        if(request()->file('brocure')){
            $brocure = request()->file('brocure')->store("product_brocure");
            $product->brocure = $brocure;
        }
        
        $product->condition = $request->condition;
        $product->store_id = 1;
        $product->video_url = $request->video_url;
        $product->product_brand_id = $request->product_brand_id;
        $product->product_category_id = $request->product_category_id;
        $product->product_subcategory_id = $request->product_subcategory_id;
        $product->product_unit_id = $request->product_unit_id;
        $product->product_warranty_id = $request->product_warranty_id;
        $product->desc = $request->desc;
        $product->weight = $request->weight;
        $product->price=$request->price;
        $product->stock=$request->stock;
        $product->business_location_id = $request->business_location;
        $product->alert_quantity = $request->alert_quantity;
        $product->custom_field_1 = $request->custom_field_1;
        $product->custom_field_2 = $request->custom_field_2;
        $product->custom_field_3 = $request->custom_field_3;
        $product->custom_field_4 = $request->custom_field_4;
        $product->sku=$generator;
        $product->price=$request->price;
        $product->stock=$request->stock;
        $product->name = Str::title($request->name);
        $product->slug = Str::slug($cek->name.' '.$request->name);
        $product->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Varian Produk '. $request->title . ' tersimpan',
        ]);
    }

    public function edit(Product $product,ProductVarian $productVarian)
    {
        return view('page.office.productVarian.input', ['data' => $productVarian, 'product' => $product]);
    }
    public function update(Request $request, productVarian $productVarian)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'product_unit_id' => 'required',
            'product_brand_id' => 'required',
            'product_category_id' => 'required',
            'product_warranty_id' => 'required',
            'desc' => 'required',
            'weight' => 'required',
            'alert_quantity' => 'required',
            'business_location' => 'required',
            'condition' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('product_unit_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_unit_id'),
                ]);
            }else if ($errors->has('product_brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_brand_id'),
                ]);
            }else if ($errors->has('product_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_category_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('weight')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            }else if ($errors->has('business_location')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('business_location'),
                ]);
            }else if ($errors->has('product_warranty_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_warranty_id'),
                ]);
            }else if ($errors->has('condition')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('condition'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alert_quantity'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($productVarian->photo);
            $photo = request()->file('photo')->store("product");
            $productVarian->photo = $photo;
        }
        if(request()->file('brocure')){
            Storage::delete($productVarian->brocure);
            $brocure = request()->file('brocure')->store("product_brocure");
            $productVarian->brocure = $brocure;
        }
        $productVarian->condition = $request->condition;
        $productVarian->video_url = $request->video_url;
        $productVarian->product_brand_id = $request->product_brand_id;
        $productVarian->product_category_id = $request->product_category_id;
        $productVarian->product_subcategory_id = $request->product_subcategory_id;
        $productVarian->product_unit_id = $request->product_unit_id;
        $productVarian->product_warranty_id = $request->product_warranty_id;
        $productVarian->desc = $request->desc;
        $productVarian->weight = $request->weight;
        $productVarian->price=$request->price;
        $productVarian->stock=$request->stock;
        $productVarian->business_location_id = $request->business_location;
        $productVarian->alert_quantity = $request->alert_quantity;
        $productVarian->custom_field_1 = $request->custom_field_1;
        $productVarian->custom_field_2 = $request->custom_field_2;
        $productVarian->custom_field_3 = $request->custom_field_3;
        $productVarian->custom_field_4 = $request->custom_field_4;
        $productVarian->name = Str::title($request->name);
        $productVarian->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk '. $request->title . ' terubah',
        ]);
    }
    public function destroy(ProductVarian $productVarian)
    {
        Storage::delete($productVarian->photo);
        $productVarian->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Varian terhapus',
        ]);
    }
    public function get(Request $request)
    {
        $varian = ProductVarian::where('id','=',$request->id)->first();
        if(!$varian){
            return response()->json([
                'alert' => 'info',
                'message' => 'Varian tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $varian,
                'message' => 'Varian ditemukan',
            ]);
        }
    }
    public function get_list(Request $request)
    {
        $collection = ProductVarian::where('product_id',$request->product)->get();
        $list = "<option value=''>Pilih Varian</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        echo $list;
    }
}