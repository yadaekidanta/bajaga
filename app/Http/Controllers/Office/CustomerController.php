<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\User AS Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $tipe = $request->tipe;
            if($tipe == "all"){
                $collection = Customer::where('name','LIKE','%'.$keywords.'%')
                ->paginate(10);
            }else{
                $collection = Customer::where('type','=',$request->tipe)
                ->where('name','LIKE','%'.$keywords.'%')
                ->paginate(10);
            }
            return view('page.office.customer.list', compact('collection'));
        }
        return view('page.office.customer.main');
    }
    public function create()
    {
        return view('page.office.customer.input', ['data' => new Customer]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users|max:255',
            'phone' => 'required|unique:users',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        $customer = New Customer;
        $generator=Helper::IDGenerator(new Customer,'code','CUST-');
        $customer->code = $generator;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->type= 'Offline';
        $customer->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pelanggan tersimpan',
        ]);
    }
    public function show(Customer $customer)
    {
        //
    }
    public function edit(Customer $customer)
    {
        return view('page.office.customer.input', ['data' => $customer]);
    }
    public function update(Request $request, Customer $customer)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|max:255',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pelanggan terubah',
        ]);
    }
    public function destroy(Customer $customer)
    {
        $customer->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pelanggan terhapus',
        ]);
    }
    
    public function get(Request $request)
    {
        $customer = Customer::where('code','=',$request->code)->first();
        if(!$customer){
            return response()->json([
                'alert' => 'info',
                'message' => 'Pelanggan tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $customer,
                'message' => 'Pelanggan ditemukan',
            ]);
        }
    }
    
    public function get_list()
    {
        $collection = Customer::get();
        return view('page.office.customer.list_modal', ['collection' => $collection]);
    }
}