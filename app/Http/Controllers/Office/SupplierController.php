<?php

namespace App\Http\Controllers\Office;

use App\Models\Supplier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class SupplierController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Supplier::where('name','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.supplier.list', compact('collection'));
        }
        return view('page.office.supplier.main');
    }
    public function create()
    {
        return view('page.office.supplier.input', ['data' => new Supplier]);
    }

    
    public function getList(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            if ($keywords) {
                $collection = Supplier::where('code','LIKE','%'.$keywords.'%')
                ->paginate(10);
            }else{
                $collection = Supplier::paginate(10);
            }
            return view('page.office.supplier.list_modal', compact('collection'));
        }
    }
            
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|max:255',
            'phone' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif($errors->has('address')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }
        }
        $supplier = New Supplier;
        $generator=Helper::IDGenerator(new Supplier,'code','SUP-');
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("supplier");
            $supplier->photo = $photo;
        }
        $supplier->code = $generator;
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->phone;
        $supplier->address = $request->address;
        $supplier->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Supplier tersimpan',
        ]);
    }
    public function show(Supplier $supplier)
    {
        //
    }
    public function edit(Supplier $supplier)
    {
        return view('page.office.supplier.input', ['data' => $supplier]);
    }
    public function update(Request $request, Supplier $supplier)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|max:255',
            'phone' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif($errors->has('phone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif($errors->has('address')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }
        }
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->phone;
        $supplier->address = $request->address;
        $supplier->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Supplier terubah',
        ]);
    }
    public function destroy(Supplier $supplier)
    {
        $supplier->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Supplier terhapus',
        ]);
    }
    public function get(Request $request)
    {
        $supplier = Supplier::where('code','=',$request->code)->first();
        if(!$supplier){
            return response()->json([
                'alert' => 'info',
                'message' => 'Supplier tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $supplier,
                'message' => 'Supplier ditemukan',
            ]);
        }
    }
}
