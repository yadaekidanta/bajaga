<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = ProductCategory::where('product_category_id',0)
            ->where('title','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.productCategory.list', compact('collection'));
        }
        return view('page.office.productCategory.main');
    }
    public function create()
    {
        $category = ProductCategory::where('product_category_id','=','0')->get();
        return view('page.office.productCategory.input', ['data' => new ProductCategory, 'category' => $category]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:product_category',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }
        }
        $data = New ProductCategory;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("product_category");
            $data->photo = $photo;
        }
        $data->title = Str::title($request->title);
        $data->slug = Str::slug($request->title);
        $data->product_category_id = $request->category ?: 0;
        $data->show_on_home = 'n';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori tersimpan',
        ]);
    }
    public function show(ProductCategory $productCategory)
    {
        return view('page.office.productCategory.show', ['data' => $productCategory]);
    }
    public function edit(ProductCategory $productCategory)
    {
        $pc = ProductCategory::where('product_category_id','=','0')->get();
        return view('page.office.productCategory.input', ['data' => $productCategory, 'category' => $pc]);
    }
    public function update(Request $request, ProductCategory $productCategory)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|exists:product_category',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($productCategory->photo);
            $photo = request()->file('photo')->store("product_category");
            $productCategory->photo = $photo;
        }
        $productCategory->title = Str::title($request->title);
        $productCategory->slug = Str::slug($request->title);
        $productCategory->product_category_id = $request->category ?: 0;
        $productCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori terubah',
        ]);
    }
    public function destroy(ProductCategory $productCategory)
    {
        $data = ProductCategory::where('product_category_id', $productCategory->id)->get();
        foreach($data as $row){
            Storage::delete($row->photo);
        }
        ProductCategory::where('product_category_id', $productCategory->id)->delete();
        Storage::delete($productCategory->photo);
        $productCategory->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori terhapus',
        ]);
    }
    public function active(ProductCategory $productCategory){
        $productCategory->show_on_home = 'y';
        $productCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori '. $productCategory->title . ' telah aktif',
        ]);
    }
    public function nonActive(ProductCategory $productCategory){
        $productCategory->show_on_home = 'n';
        $productCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori '. $productCategory->title . ' tidak aktif',
        ]);
    }
    public function get_list(Request $request)
    {
        $result = ProductCategory::where('product_category_id','=','0')->get();
        $list = "<option>Pilih Kategori</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->title</option>";
        }
        return $list;
    }
    public function get_list_sub(Request $request)
    {
        $result = ProductCategory::where('product_category_id','=',$request->category)->get();
        $list = "<option>Pilih Sub Kategori</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->title</option>";
        }
        echo $list;
    }

    public function input_modal()
    {
        $category = ProductCategory::where('product_category_id','=','0')->get();
        return view('page.office.productCategory.input_modal', [
            'data' => new ProductCategory, 
            "category" => $category
        ]);
    }
}
