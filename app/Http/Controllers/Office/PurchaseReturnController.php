<?php

namespace App\Http\Controllers\Office;

use App\Helpers\Helper;
use App\Models\Purchase;
use Illuminate\Http\Request;
use App\Models\PurchaseReturn;
use App\Http\Controllers\Controller;
use App\Models\EmployeActivities;
use App\Models\PurchaseReturnDetail;
use Illuminate\Support\Facades\Auth;
use App\Models\PurchaseReturnPayment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PurchaseReturnController extends Controller
{
    public function purchase_activity($transaction_id, $employee_id, $method, $tipe)
    {
        $employe_activity = new EmployeActivities;

        $employe_activity->transaction_id = $transaction_id;
        $employe_activity->employee_id = $employee_id;
        $employe_activity->method = $method;
        $employe_activity->tipe = $tipe;

        $employe_activity->save();
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = PurchaseReturn::where('code','LIKE','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.purchaseReturn.list', compact('collection'));
        }
        return view('page.office.purchaseReturn.main');
    }
    public function create()
    {
        $purchase = Purchase::get();
        return view('page.office.purchaseReturn.input',['data' => new PurchaseReturn, 'purchase' => $purchase ]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'pembelian' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }elseif ($errors->has('pembelian')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pembelian'),
                ]);
            }
        }
        if ($request->code){
            $code = $request->code.date("-Ym").'-';
        }else{
            $code = 'PRTRN'.date("-Ym").'-';
        }
        $generator = Helper::IDGenerator(new PurchaseReturn,'code',$code);
        $purchase = new PurchaseReturn;
        $purchase->date = $request->tanggal;
        $purchase->code = $generator;
        $purchase->purchase_id = $request->pembelian;
        $purchase->created_by = Auth::guard('office')->user()->id;
        $purchase->save();
        $this->purchase_activity($purchase->id, Auth::user()->id, "created", "return_pembelian");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengembalian Tersimpan',
            'redirect' => 'input',
            'route' => route('office.purchase-return.edit',$purchase->id)
        ]);
    }
    public function show(PurchaseReturn $purchaseReturn)
    {
        return view('page.office.purchaseReturn.show', compact('purchaseReturn'));
    }
    public function edit(PurchaseReturn $purchaseReturn)
    {
        $purchase = Purchase::get();
        return view('page.office.purchaseReturn.input',['data' => $purchaseReturn, 'purchase' => $purchase ]);
    }
    public function update(Request $request, PurchaseReturn $purchaseReturn)
    {
        $purchaseReturn->date = $request->tanggal;
        $purchaseReturn->update();
        $this->purchase_activity($purchaseReturn->id, Auth::user()->id, "update", "return_pembelian");
    }
    public function process(PurchaseReturn $purchaseReturn)
    {
        if($purchaseReturn->purchase_detil->count() > 0){
            $purchaseReturn->st = "Diproses";
            $purchaseReturn->update();
            $this->purchase_activity($purchaseReturn->id, Auth::user()->id, "process", "return_pembelian");
            return response()->json([
                'alert' => 'success',
                'message' => 'Pengembalian diproses',
                'redirect' => 'input',
                'route' => route('office.purchase-return.show',$purchaseReturn->id),
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Harap masukkan barang terlebih dahulu',
                'redirect' => 'input',
                'route' => route('office.purchase-return.show',$purchaseReturn->id),
            ]);
        }
    }

    public function delivered(PurchaseReturn $purchaseReturn)
    {
        Helper::decrease_stock(new PurchaseReturnDetail,new PurchaseReturn,'purchase_return_id',$purchaseReturn->id);
        $purchaseReturn->st = "Diterima";
        $purchaseReturn->update();
        $this->purchase_activity($purchaseReturn->id, Auth::user()->id, "telah diterima", "return_pembelian");
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang diterima',
            'redirect' => 'input',
            'route' => route('office.purchase-return.show',$purchaseReturn->id),
        ]);
    }

    public function destroy(PurchaseReturn $purchaseReturn)
    {
        PurchaseReturnDetail::where('purchase_id')->delete();
        PurchaseReturnPayment::where('purchase_id')->delete();
        $this->purchase_activity($purchaseReturn->id, Auth::user()->id, "delete", "return_pembelian");
        $purchaseReturn->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengembalian terhapus',
        ]);
    }
}