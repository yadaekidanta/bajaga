<?php

namespace App\Http\Controllers\Office;

use App\Models\Account;
use App\Models\AccountType;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $tipe = $request->tipe;
            if($tipe == "all"){
                $collection = Account::where('name','LIKE','%'.$keywords.'%')
                ->paginate(10);
            }else{
                $collection = Account::where('st','=',$request->tipe)
                ->where('name','LIKE','%'.$keywords.'%')
                ->paginate(10);
            }
            return view('page.office.account.list', compact('collection'));
        }
        return view('page.office.account.main');
    }
    public function create()
    {
        $account_type = AccountType::where('account_type_id',0)->get();
        return view('page.office.account.input', ['data' => new Account, 'account_type' => $account_type]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:account,name',
            'code' => 'required|max:20|unique:account,code',
            'account_type' => 'required',
            'balance' => 'required',
            'label' => 'required',
            'value' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif ($errors->has('account_type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_type'),
                ]);
            }elseif ($errors->has('balance')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('balance'),
                ]);
            }elseif ($errors->has('label')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('label'),
                ]);
            }elseif ($errors->has('value')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('value'),
                ]);
            }
        }
        $data = new Account;
        $data->name = Str::title($request->name);
        $data->code = $request->code;
        $data->account_type_id = $request->account_type;
        $data->balance = Str::remove(',',$request->balance);
        $data->label = $request->label;
        $data->value = $request->value;
        $data->note = $request->note ?: '';
        $data->created_at = date('Y-m-d H:i:s');
        $data->created_by = Auth::guard('office')->user()->id;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun tersimpan',
        ]);
    }
    public function show(Account $account)
    {
        return view('page.office.account.show', ['data' => $account]);
    }
    public function edit(Account $account)
    {
        $account_type = AccountType::where('account_type_id',0)->get();
        return view('page.office.account.input', ['data' => $account, 'account_type' => $account_type]);
    }
    public function update(Request $request, Account $account)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|exists:account,name',
            'code' => 'required|max:20|exists:account,code',
            'account_type' => 'required',
            'label' => 'required',
            'value' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif ($errors->has('account_type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('account_type'),
                ]);
            }elseif ($errors->has('label')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('label'),
                ]);
            }elseif ($errors->has('value')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('value'),
                ]);
            }
        }
        $account->name = Str::title($request->name);
        $account->code = $request->code;
        $account->account_type_id = $request->account_type;
        $account->label = $request->label;
        $account->value = $request->value;
        $account->note = $request->note ?: '';
        $account->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun terubah',
        ]);
    }
    public function destroy(Account $account)
    {
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun tidak dapat dihapus',
        ]);
    }
    public function active(Account $account)
    {
        $account->st = 'a';
        $account->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun '. $account->name . ' diaktifkan',
        ]);
    }
    public function close(Account $account)
    {
        $account->st = 'c';
        $account->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun '. $account->name . ' ditutup',
        ]);
    }
    public function get(Request $request)
    {
        $account = Account::where('id','=',$request->id)->first();
        if(!$account){
            return response()->json([
                'alert' => 'info',
                'message' => 'Akun tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $account,
                'message' => 'Akun ditemukan',
            ]);
        }
    }
    public function get_list(Request $request)
    {
        $result = Account::get();
        $list = "<option>Pilih Akun</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->code - $row->name | Rp ".number_format($row->balance)."</option>";
        }
        return $list;
    }
    public function list_modal(Request $request)
    {
        if ($request->ajax()) {
            $collection = Account::where('st','=','a')->get();
            return view('page.office.account.list_modal',compact('collection'));
        }
    }
}
