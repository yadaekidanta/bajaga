<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Courier;
use App\Models\UserStore;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class RajaOngkirController extends Controller
{
    public function cost(Request $request)
    {
        $origin = UserStore::select("subdistrict_id")->where("id", $request->store_id)->first()->subdistrict_id;
        $originType = $request->destinationType;

        //TODO: Ganti nanti couriernya sesuai courier yang disediakan oleh store itu sendiri
        $courier = Courier::limit(6)->get();
        
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'destination' => 'required',
            'destinationType' => ['required', Rule::in(['city', 'subdistrict'])],
            'weight' => ['required', 'numeric'],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('origin')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('origin'),
                ]);
            } elseif ($errors->has('originType')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('originType'),
                ]);
            } elseif ($errors->has('destination')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('destination'),
                ]);
            } elseif ($errors->has('destinationType')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('destinationType'),
                ]);
            } elseif ($errors->has('weight')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            } elseif ($errors->has('courier')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('courier'),
                ]);
            }
        }
        try {
            $collections = [];
            foreach ($courier as $key) {
                $data = Http::asForm()->withHeaders([
                    'key' => env('RAJAONGKIR_KEY'),
                    'content-type' => 'application/x-www-form-urlencoded'
                ])->post(env('RAJAONGKIR_BASE_URL') . '/cost', [
                    'origin' => $origin,
                    'originType' => $originType,
                    'destination' => $request->destination,
                    'destinationType' => $request->destinationType,
                    'weight' => $request->weight,
                    'courier' => $key->name,
                    'length' => $request->length,
                    'width' => $request->width,
                    'height' => $request->height,
                    'diameter' => $request->diameter,
                ]);
                array_push($collections, $data->json()["rajaongkir"]["results"][0]);
            }
            // return $data->json();
            return response()->json($collections, 200);
            
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function province(Request $request)
    {
        try {
            $data = Http::withHeaders([
                'key' => env('RAJAONGKIR_KEY'),
            ])->get(env('RAJAONGKIR_BASE_URL') . '/province', [
                'id' => $request->id,
            ]);
            return response()->json($data->json(), 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function city(Request $request)
    {
        try {
            $data = Http::withHeaders([
                'key' => env('RAJAONGKIR_KEY'),
            ])->get(env('RAJAONGKIR_BASE_URL') . '/city', [
                'id' => $request->id,
                'province' => $request->province,
            ]);
            return response()->json($data->json(), 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function subdistrict(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('city')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }
        }
        try {
            $data = Http::withHeaders([
                'key' => env('RAJAONGKIR_KEY'),
            ])->get(env('RAJAONGKIR_BASE_URL') . '/subdistrict', [
                'id' => $request->id,
                'city' => $request->city,
            ]);
            return response()->json($data->json(), 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function waybill(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'no_resi' => 'required',
            'courier' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('no_resi')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('no_resi'),
                ]);
            } elseif ($errors->has('courier')) {
                return response()->json([
                    'status' => 'error',
                    'message' => $errors->first('courier'),
                ]);
            }
        }

        $dataPost = [
            "waybill" => $request->no_resi,
            "courier" => $request->courier,
        ];


        try {
            $data = Http::asForm()->withHeaders([
                'key' => env('RAJAONGKIR_KEY'),
                'content-type' => 'application/x-www-form-urlencoded'
            ])->post(env('RAJAONGKIR_BASE_URL') . '/waybill', $dataPost);
             
            // return $data->json();
            return json_decode($data, true);
            
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function trf()
    { 	 
        return env('RAJAONGKIR_KEY'); 
    }
}
