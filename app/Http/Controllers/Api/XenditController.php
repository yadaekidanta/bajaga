<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\CheckoutService as CheckoutService;
use App\Http\Services\PaymentService;
use App\Models\User;
use App\Models\UserBalance;
use App\Notifications\TransactionNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class XenditController extends Controller
{
    public function create(Request $request)
    {
        $checkoutService = new CheckoutService();

        return $checkoutService->createInvoice($request->all());
    }

    public function getInvoice(Request $request)
    {
        $checkoutService = new CheckoutService();

        return $checkoutService->getInvoice($request->all());
    }

    public function createPayout(Request $request)
    {
        $checkoutService = new CheckoutService();

        return $checkoutService->createPayout($request->all());
    }

    public function createDisbursment(Request $request)
    {
        $checkoutService = new CheckoutService();

        return $checkoutService->createDisbursment($request->all());
    }

    public function callbackDisbursment(Request $request)
    {
        // return response()->json([
        //     "alert" => "success",
        //     "message" => "Success resolve disbursment callback!",
        // ]);


        // check header for authorization the requests
        if ($request->header("x-callback-token") != env("TOKEN_VERIFIKASI")) {
            return response()->json([
                "alert" => "Error",
                "message" => "Sorry we cann't resolve this request"
            ], 401);
        }
        
        try {
            $id_disbursment = $request->id;
            $user_balance = UserBalance::where("code", $id_disbursment)->first();
            $failure_code = null;
            $user = User::where("id", $user_balance->user_id)->first();
            $details = [
                'greeting' => "Hi ".$user->name,
                'body' => ''
            ];
            if ($request->status == "FAILED") {
                $failure_code = $request->failure_code;
                $details['body'] = 'Penarikan dana kamu gagal, silahkan mencoba lagi beberapa saat!';
            }else if($request->status == "COMPLETED"){
                $details['body'] = 'Penarikan dana kamu telah berhasil!';
            }else if($request->status == "PENDING"){
                $details['body'] = 'Penarikan dana kamu masih dalam proses!';
            }
            
            $user_balance->update([
                "status" => $request->status,
                "failure_code" => $failure_code,
            ]);

            Notification::send($user, new TransactionNotification($user, $details));

            return response()->json([
                "alert" => "success",
                "message" => "Success resolve disbursment callback!",
            ]);
            // add notification after doig above

        } catch (\Throwable $th) {
            return response()->json([
                "alert" => "Error",
                "message" => $th->getMessage()
            ], 500);
        }
    }

    public function createVirtualAccount(Request $request)
    {
        $checkoutService = new PaymentService();

        return $checkoutService->createVirtualAccount($request->all());
    }
}
