<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegisteredNotification extends Notification
{
    use Queueable;
    public $user;
    public $token;
    public function __construct($user,$token)
    {
        $this->user = $user;
        $this->token = $token;
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        $token = $this->token;
        return (new MailMessage)->subject('Selamat bergabung')->from('no-reply@bajaga.online')->view('email.welcome',compact('notifiable','token'));
    }
    public function toArray($notifiable)
    {
        return [
            'tipe' => 1,
            'nama' => $this->user->name,
            'pesan' => "Hai ".$this->user->name.", selamat bergabung di ".config('app.name').". Harap lakukan verifikasi email."
        ];
    }
}
