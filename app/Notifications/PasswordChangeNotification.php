<?php



namespace App\Notifications;



use Illuminate\Bus\Queueable;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Notifications\Messages\MailMessage;

use Illuminate\Notifications\Notification;



class PasswordChangedNotification extends Notification

{

    use Queueable;

    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function via($notifiable)

    {
        return ['mail','database'];
    }

    public function toMail($notifiable)

    {
        $data = $this->user;
        return (new MailMessage)->subject('Yada Ekidanta | Password Changed')->from('noreply@yadaekidanta.com')->view('email.password_change',compact('data'));

    }

    public function toArray($notifiable)

    {

        return [
            'tipe' => 1,
            'nama' => $this->user->name,
            'pesan' => "Hi ".$this->user->name.", your password has been changed"
        ];

    }

}

