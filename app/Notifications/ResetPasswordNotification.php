<?php



namespace App\Notifications;



use Illuminate\Bus\Queueable;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Notifications\Messages\MailMessage;

use Illuminate\Notifications\Notification;



class ResetPasswordNotification extends Notification

{

    use Queueable;

    public $user;

    public function __construct($user)

    {
        $this->user = $user;
    }

    public function via($notifiable)

    {
        return ['mail','database'];
    }

    public function toMail($notifiable)
    {
        $data = $this->user;
        return (new MailMessage)->subject('Yada Ekidanta | Reset Password Confirmation')->from('noreply@yadaekidanta.com')->view('email.password_reset',compact('data'));
    }

    public function toArray($notifiable)

    {

        return [
            'tipe' => 1,
            'nama' => $notifiable->name,
            'pesan' => "Hi ".$notifiable->name.", Sorry to hear you’re having trouble logging into ".config('app.name').". We got a message that you forgot your password."
        ];

    }

}

